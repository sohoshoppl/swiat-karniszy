<div id="banners">
	<div class="item">
		<div class="icon">
			<img src="{$img_dir}banner1.jpg" alt="{l s='Darmowa dostawa'}" />
		</div>
		<div class="headtxt">
			{l s='Darmowa dostawa'}
		</div>
		<div class="subtxt">
			{l s='dla zakupów powyżej'} {convertPrice price = (Configuration::get('PS_SHIPPING_FREE_PRICE'))}
		</div>
	</div>
	
	<div class="item">
		<div class="icon">
			<img src="{$img_dir}banner2.jpg" alt="{l s='w 48h w twoim domu'}" />
		</div>
		<div class="headtxt">
			{l s='w 48h w twoim domu'}
		</div>
		<div class="subtxt">
			{l s='gwarancja szybkiej dostawy kurierskiej'}
		</div>
	</div>
{*	
	<div class="item">
		<div class="icon">
			<img src="{$img_dir}banner3.jpg" alt="{l s='30 dni na zwrot towaru'}" />
		</div>
		<div class="headtxt">
			{l s='30 dni na zwrot towaru'}
		</div>
		<div class="subtxt">
			{l s='zależy nam na Twojej satysfakcji'}
		</div>
	</div>
	*}
</div>