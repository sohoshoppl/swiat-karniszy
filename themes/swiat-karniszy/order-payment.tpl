{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$opc}

</div></div></div>
<div id="cart">
	<div class="container">
<div class="flex-center"><hr><h2 class="heading">{l s='Twój koszyk z zakupami'}</h2><hr></div>              
		<div class="row">
			<div class="col-xs-12 col-md-9">
				<div class="cart_block">
					<ul class="navigation_cart">
						<li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} third">
							<i class="icon-check"></i> {l s='Dane osobowe'} <a class="edit" href="{$link->getPageLink('identity')|escape:'html':'UTF-8'}?back={'order.php?step=1'|escape:'url'}"><i class="icon-edit"></i> {l s='edytuj'}</a>
						</li>
						<li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} third">
							<i class="icon-check"></i> {l s='Adresy'} <a class="edit" href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}"><i class="icon-edit"></i> {l s='edytuj'}</a>
						</li>
						<li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done step_done_last{else}step_todo{/if}{/if} four">
							<i class="icon-check"></i> {l s='Sposób dostawy'} <a class="edit" href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}"><i class="icon-edit"></i> {l s='edytuj'}</a>
						</li>
					</ul>
					
	{addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
	{addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
	{capture name=path}<li property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" title="{l s='Your shopping cart'}">{l s='Your shopping cart'}</a>
	<meta property="position" content></li>
	<li property="itemListElement" typeof="ListItem">
		<a property="item" typeof="WebPage" href="{$link->getPageLink('order?step=1', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}">
	    	{l s='Addresses'}
	    </a>
	<meta property="position" content>
	</li>
	
	<li property="itemListElement" typeof="ListItem">
		<a property="item" typeof="WebPage" href="{$link->getPageLink('order?step=2', true)|escape:'html':'UTF-8'}" title="{l s='Shipping method'}">
	    	{l s='Shipping method'}
	    </a>
	<meta property="position" content>
	</li>
		
	<li property="itemListElement" typeof="ListItem">{l s='Your payment method'}</li>
	{/capture}
	
        <ul>
	<h1 class="page-heading"><span>4. </span>{l s='Płatność'}</h1>
{else}
	<h1 class="page-heading step-num"><span>3</span> {l s='Please choose your payment method'}</h1>
{/if}

{if !$opc}
	{assign var='current_step' value='payment'}
	{include file="$tpl_dir./errors.tpl"}
{else}
	<div id="opc_payment_methods" class="opc-main-block">
		<div id="opc_payment_methods-overlay" class="opc-overlay" style="display: none;"></div>
{/if}

{if $advanced_payment_api}
    {include file="$tpl_dir./order-payment-advanced.tpl"}
{else}
    {include file = "$tpl_dir./order-payment-classic.tpl"}
{/if}
                                </ul>
</div>
</div>
	<div class="col-xs-12 col-md-3">
		<div id="shopping-cart-right">
			{include file="$tpl_dir./shopping-cart-right.tpl" etap_platnosci=1}
		</div>
	</div>
</div>
</div></div>
<div class="container">
	<div class="row">
		<div class="center_column col-xs-12 col-sm-12">