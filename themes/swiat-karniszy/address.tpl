{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{if $back != 'order.php?step=1'}
<li property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='My account'}">
                        {l s='My account'}
                </a>
                <meta property="position" content>
</li>
<li property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My addresses'}">
                        {l s='My addresses'}
                </a>
                <meta property="position" content>
</li>
<li property="itemListElement" typeof="ListItem">{l s='Add new address'}</li>
{else}
<li property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" title="{l s='My account'}">
                        {l s='Your cart'}
                </a>
                <meta property="position" content>
</li>
<li property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My addresses'}">
                        {l s='Order finalization'}
                </a>
                <meta property="position" content>
</li>
<li property="itemListElement" typeof="ListItem">{l s='Addresses'}</li>
{/if}
{/capture}

{if $back == 'order.php?step=1'}
</div></div></div>
<div id="cart">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-9">
                            <div class="cart_block">
                                <ul class="navigation_cart">
                                    <li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} third">
                                        <i class="icon-check"></i> {l s='Dane osobowe'}
										{if $is_logged}<a class="edit" href="{$link->getPageLink('identity')|escape:'html':'UTF-8'}?back={'order.php?step=1'|escape:'url'}"><i class="icon-edit"></i> {l s='edytuj'}</a>{/if}
                                    </li>
                                    <li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} third">
                                        <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                                            2. {l s='Adresy'}
                                        </a>
                                    </li>
                                </ul>
{/if}
{if !preg_match("/^order/", $back)}
    <div class="flex-center">
    <hr>
    <h2 class="heading">{if isset($id_address) && (isset($smarty.post.alias) || isset($address->alias))}{l s='Modify address'}{else}{l s='Dodaj adres'}{/if}</h2>  
    <hr>    
</div>
{/if}
                            <div class="{if $back == 'order.php?step=1'}padding30{else}myaccount-content{/if}">
                                {include file="$tpl_dir./errors.tpl"}
                                {*if !preg_match("/^order/", $back)}
                                    <p class="info-title">
                                        {if isset($id_address) && (isset($smarty.post.alias) || isset($address->alias))}
                                            {l s='Modify address'}
                                            {if isset($smarty.post.alias)}
                                                "{$smarty.post.alias}"
                                            {else}
                                                {if isset($address->alias)}"{$address->alias|escape:'html':'UTF-8'}"{/if}
                                            {/if}
                                        {else}
                                            {l s='To add a new address, please fill out the form below.'}
                                        {/if}
                                    </p>
                                    <!--<p class="required"><sup>*</sup>{l s='Required field'}</p>-->
                                {/if*}
                                <form action="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" method="post" class="row std{if $back == 'order.php?step=1'} row{/if}" id="add_address">
                                        <!--h3 class="page-subheading">{if isset($id_address)}{l s='Your address'}{else}{l s='New address'}{/if}</h3-->
                                    {assign var="stateExist" value=false}
                                    {assign var="postCodeExist" value=false}
                                    {assign var="dniExist" value=false}
                                    {assign var="homePhoneExist" value=false}
                                    {assign var="mobilePhoneExist" value=false}
                                    {assign var="atLeastOneExists" value=false}
                                    <div class="required form-group col-xs-12 col-lg-6"  id="adress_alias">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <label for="alias">{l s='Please assign an address title for future reference.'}</label>
                                            </div>
                                            <div class="col-xs-12 col-sm-9">
                                                <input type="text" id="alias" class="is_required validate form-control" data-validate="{$address_validation.alias.validate}" name="alias" value="{if isset($smarty.post.alias)}{$smarty.post.alias}{elseif isset($address->alias)}{$address->alias|escape:'html':'UTF-8'}{elseif !$select_address}{l s='My address'}{/if}" />
                                            </div>
                                        </div>
                                    </div>
									<div class="clearfix"></div>
                                    {foreach from=$ordered_adr_fields item=field_name}
                                        {if $field_name eq 'company'}
                                            <div class="form-group col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="company">{l s='Company'}{if isset($required_fields) && in_array($field_name, $required_fields)}{/if}</label>
                                                        <span class="option">{l s='(option)'}</span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="form-control validate" data-validate="{$address_validation.$field_name.validate}" type="text" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{else}{if isset($address->company)}{$address->company|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'vat_number'}
                                            <div id="vat_area" class="form-group col-xs-12 col-lg-6" >
                                                <div id="vat_number">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-3">
                                                            <label for="vat-number">{l s='VAT number'}{if isset($required_fields) && in_array($field_name, $required_fields)}{/if}</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-9">
                                                            <input type="text" class="form-control validate" data-validate="isNip" id="vat-number" name="vat_number" value="{if isset($smarty.post.vat_number)}{$smarty.post.vat_number}{else}{if isset($address->vat_number)}{$address->vat_number|escape:'html':'UTF-8'}{/if}{/if}" />
															<span class="vat_info">{l s='10-cio cyfrowy kod podatnika'}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'dni'}
                                            {assign var="dniExist" value=true}
                                            <div class="required form-group dni  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="dni">{l s='Identification number'}</label>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="dni" id="dni" value="{if isset($smarty.post.dni)}{$smarty.post.dni}{else}{if isset($address->dni)}{$address->dni|escape:'html':'UTF-8'}{/if}{/if}" />
                                                        <span class="form_info">{l s='DNI / NIF / NIE'}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'firstname'}
                                            <div class="required form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="firstname">{l s='First name'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="firstname" id="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{else}{if isset($address->firstname)}{$address->firstname|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'lastname'}
                                            <div class="required form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="lastname">{l s='Last name'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="lastname" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{else}{if isset($address->lastname)}{$address->lastname|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'address1'}
                                            <div class="required form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="address1">{l s='Address'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="address1" name="address1" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{else}{if isset($address->address1)}{$address->address1|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'address2'}
                                            <div class="required form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="address2">{l s='Address (Line 2)'}{if isset($required_fields) && in_array($field_name, $required_fields)}{/if}</label>
                                                        <span class="option">{l s='(option)'}</span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="address2" name="address2" value="{if isset($smarty.post.address2)}{$smarty.post.address2}{else}{if isset($address->address2)}{$address->address2|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'postcode'}
                                            {assign var="postCodeExist" value=true}
                                            <div class="required postcode form-group unvisible col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="postcode">{l s='Zip/Postal Code'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html':'UTF-8'}{/if}{/if}" />
														<span class="vat_info">5-cio cyfrowy kod w formacie dd-dddd</span>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'city'}
                                            <div class="required form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="city">{l s='City'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="city" id="city" value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{if isset($address->city)}{$address->city|escape:'html':'UTF-8'}{/if}{/if}" maxlength="64" />
                                                    </div>
                                                </div>
                                            </div>
                                            {* if customer hasn't update his layout address, country has to be verified but it's deprecated *}
                                        {/if}
                                        {if $field_name eq 'Country:name' || $field_name eq 'country' || $field_name eq 'Country:iso_code'}
                                            <div class="required form-group country_block unvisible col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="id_country">{l s='Country'}</label>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <select id="id_country" class="form-control" name="id_country">{$countries_list}</select>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'State:name'}
                                            {assign var="stateExist" value=true}
                                            <div class="required id_state form-group  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="id_state">{l s='State'}</label>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <select name="id_state" id="id_state" class="form-control">
                                                            <option value="">-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                        {if $field_name eq 'phone'}
                                            {assign var="homePhoneExist" value=true}
                                            <div class="form-group phone-number  col-xs-12 col-lg-6">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label for="phone">{l s='Home phone'}</label>
                                                        <!--<p class="required"><sup>*</sup></p>-->
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input class="{if isset($one_phone_at_least) && $one_phone_at_least}is_required{/if} validate form-control" data-validate="{$address_validation.phone.validate}" type="tel" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($address->phone)}{$address->phone|escape:'html':'UTF-8'}{/if}{/if}"  />
                                                    </div>
                                                </div>
                                            </div>
                                            {if $back != 'order.php?step=1'}<div class="clearfix"></div>{/if}
                                        {/if}
                                        {if $field_name eq 'phone_mobile'}
                                            {assign var="mobilePhoneExist" value=true}
                                            <div class="hidden {if isset($one_phone_at_least) && $one_phone_at_least}required {/if}form-group">
                                                <label for="phone_mobile">{l s='Mobile phone'}</label>
                                                <input class="validate form-control" data-validate="{$address_validation.phone_mobile.validate}" type="tel" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html':'UTF-8'}{/if}{/if}" />
                                            </div>
                                        {/if}
                                        {if ($field_name eq 'phone_mobile') || ($field_name eq 'phone_mobile') && !isset($atLeastOneExists) && isset($one_phone_at_least) && $one_phone_at_least}
                                            {assign var="atLeastOneExists" value=true}
                                            {if $back == 'order.php?step=1'}<div class="clearfix"></div>{/if}
                                            <p class="inline-infos required hidden">** {l s='You must register at least one phone number.'}</p>
                                        {/if}
                                    {/foreach}
                                    {if !$postCodeExist}
                                        <div class="required postcode form-group unvisible  col-xs-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <label for="postcode">{l s='Zip/Postal Code'}</label>
                                                    <!--<p class="required"><sup>*</sup></p>-->
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input class="is_required validate form-control" data-validate="{$address_validation.postcode.validate}" type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html':'UTF-8'}{/if}{/if}" />
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                    {if !$stateExist}
                                        <div class="required id_state form-group unvisible  col-xs-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <label for="id_state">{l s='State'}</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <select name="id_state" id="id_state" class="form-control">
                                                        <option value="">-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                    {if !$dniExist}
                                        <div class="required dni form-group unvisible  col-xs-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <label for="dni">{l s='Identification number'}</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input class="is_required form-control" data-validate="{$address_validation.dni.validate}" type="text" name="dni" id="dni" value="{if isset($smarty.post.dni)}{$smarty.post.dni}{else}{if isset($address->dni)}{$address->dni|escape:'html':'UTF-8'}{/if}{/if}" />
                                                    <span class="form_info">{l s='DNI / NIF / NIE'}</span>
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                    <div class="form-group hidden">
                                        <label for="other">{l s='Additional information'}</label>
                                        <textarea class="validate form-control" data-validate="{$address_validation.other.validate}" id="other" name="other" cols="26" rows="3" >{if isset($smarty.post.other)}{$smarty.post.other}{else}{if isset($address->other)}{$address->other|escape:'html':'UTF-8'}{/if}{/if}</textarea>
                                    </div>
                                    {if !$homePhoneExist}
                                        <div class="form-group phone-number  col-xs-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <label for="phone">{l s='Home phone'}</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <input class="{if isset($one_phone_at_least) && $one_phone_at_least}is_required{/if} validate form-control" data-validate="{$address_validation.phone.validate}" type="tel" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($address->phone)}{$address->phone|escape:'html':'UTF-8'}{/if}{/if}"  />
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                    {if $back != 'order.php?step=1'}<div class="clearfix"></div>{/if}
                                    {if !$mobilePhoneExist}
                                        <div class="hidden {if isset($one_phone_at_least) && $one_phone_at_least}required {/if}form-group">
                                            <label for="phone_mobile">{l s='Mobile phone'}</label>
                                            <input class="validate form-control" data-validate="{$address_validation.phone_mobile.validate}" type="tel" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html':'UTF-8'}{/if}{/if}" />
                                        </div>
                                    {/if}
                                    {if isset($one_phone_at_least) && $one_phone_at_least && !$atLeastOneExists}
                                        <p class="hidden inline-infos required">{l s='You must register at least one phone number.'}</p>
                                    {/if}


                                    <div class="clearfix"></div>
                                    <p class="submit2 col-xs-12">
                                        {if isset($id_address)}<input type="hidden" name="id_address" value="{$id_address|intval}" />{/if}
                                        {if isset($back)}<input type="hidden" name="back" value="{$back}" />{/if}
                                        {if isset($mod)}<input type="hidden" name="mod" value="{$mod}" />{/if}
                                        {if isset($select_address)}<input type="hidden" name="select_address" value="{$select_address|intval}" />{/if}
                                        <input type="hidden" name="token" value="{$token}" />
                                        <button type="submit" name="submitAddress" id="submitAddress" class="btn btn-default button button-medium">
                                            <span>
                                                {l s='Save'}
                                                <i class="icon-chevron-right right"></i>
                                            </span>
                                        </button>
                                    </p>
                                </form>
                            </div>
                            <ul class="footer_links clearfix{if $back == 'order.php?step=1'} hidden{/if}">
                                <li>
                                    <a class="btn btn-defaul button button-small" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}">
                                        <span><i class="icon-chevron-left"></i> {l s='Back to your addresses'}</span>
                                    </a>
                                </li>
                            </ul>

                            {if $back == 'order.php?step=1'}
                                <ul class="navigation_cart bottom">
                                    <li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done step_done_last{else}step_todo{/if}{/if} four">
                                        {if $current_step=='payment'}
                                            <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                                                <em>04.</em> {l s='Shipping'}
                                            </a>
                                        {else}
                                            <span><em>3.</em> {l s='Sposób dostawy'}</span>
                                        {/if}
                                    </li>
                                    <li id="step_end" class="{if $current_step=='payment'}step_current{else}step_todo{/if} last">
                                        <span><em>4.</em> {l s='Płatność'}</span>
                                    </li>
                                </ul>                                
                            </div>


</div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div id="shopping-cart-right">
                    {include file="$tpl_dir./shopping-cart-right.tpl"}
                </div>
            </div>
</div>
</div></div>
<div class="container">
	<div class="row">
		<div class="center_column col-xs-12 col-sm-12">
{/if}


{strip}
{if isset($smarty.post.id_state) && $smarty.post.id_state}
	{addJsDef idSelectedState=$smarty.post.id_state|intval}
{elseif isset($address->id_state) && $address->id_state}
	{addJsDef idSelectedState=$address->id_state|intval}
{else}
	{addJsDef idSelectedState=false}
{/if}
{if isset($smarty.post.id_country) && $smarty.post.id_country}
	{addJsDef idSelectedCountry=$smarty.post.id_country|intval}
{elseif isset($address->id_country) && $address->id_country}
	{addJsDef idSelectedCountry=$address->id_country|intval}
{else}
	{addJsDef idSelectedCountry=false}
{/if}
{if isset($countries)}
	{addJsDef countries=$countries}
{/if}
{if isset($vatnumber_ajax_call) && $vatnumber_ajax_call}
	{addJsDef vatnumber_ajax_call=$vatnumber_ajax_call}
{/if}
{/strip}
