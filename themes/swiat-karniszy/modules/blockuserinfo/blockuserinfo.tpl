<!-- Block user information module NAV  -->
{*if $is_logged}
    <div class="header_user_info">
        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
            <img src="{$img_dir}login.png" alt="{l s='Ulubione' mod='blockuserinfo'}">
            <span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span>
        </a>
    </div>
{/if*}
<div class="header_user_info">
    {if $is_logged}
        <a class="header-padding logout current" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Konto' mod='blockuserinfo'}">
            <img src="{$img_dir}my-aaccount.png" alt="{l s='Sign out' mod='blockuserinfo'}">
        </a>
		<ul class="toogle_content loginbox loggedul">
			<li>
				<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
					{$cookie->customer_firstname} {$cookie->customer_lastname}
				</a>
			</li>
			<li>
				<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
					{l s='Log me out' mod='blockuserinfo'}
				</a>
			</li>
		</ul>
    {else}
        <a class="header-padding login current" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
            <img src="{$img_dir}my-aaccount.png" alt="{l s='Sign in' mod='blockuserinfo'}">
        </a>
		<ul class="toogle_content loginbox">
			<li>
				<form id="login_form" class="std" method="post" action="{$link->getPageLink('authentication')}">
						<p>
							<input placeholder="{l s='Adres email'}" id="email" class="is_required validate account_input form-control" type="text" value="" name="email">
						</p>
						<p>
							<input placeholder="{l s='Hasło'}" id="passwd" class="is_required validate account_input form-control" type="password" value="" name="passwd">
						</p>
						<p class="submit">
							<input class="hidden" type="hidden" value="my-account" name="back">
							<input id="" class="transition" type="submit" value="{l s='Zaloguj się'}" name="SubmitLogin">
						</p>
				</form>
				<p class="login-block mb">
					<a class="query" href="{$link->getPageLink('password', true)|escape:'html':'UTF-8'}">{l s='Przypomnij hasło'}</a>
				</p>
				<p class="login-block">
					{l s='Nie masz jeszcze konta?'}
				</p>
				<p class="login-block">
					<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" href="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}">{l s='Zarejestruj się'}</a>
				</p>
			</li>
		</ul>
    {/if}
</div>
<!-- /Block usmodule NAV -->
