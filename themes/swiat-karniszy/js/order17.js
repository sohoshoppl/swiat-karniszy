$(document).ready(function(){
	
	$('.payment_option').each(function() {
		if ($(this).prop('checked'))
		{			
			$('#payment_button_url').attr('href', $(this).val() );
		}
	});
	
	$(document).on('click', '.payment_option', function(e){
		//$('.payment_option_content').removeClass('active');
		//$(this).parent().parent().parent().find('.payment_option_content').addClass('active');
		
		$('#payment_button_url').attr('href', $(this).val() );
	});
        
        $(document).on('click', '#payment_button_url', function(e){
            if($(this).attr('href')=='#'){
                if (!!$.prototype.fancybox){
                    $.fancybox.open(
                    [{
                        type: 'inline',
                        autoScale: true,
                        minHeight: 30,
                        content: '<p class="fancybox-error">' + msg_no_payment + '</p>'
                    }],
                    {
                            padding: 0
                    });
                }else{
                    alert(msg_no_payment);
                }
            }
        });
        
        
	
	$(document).on('click', '.delivery_options > .delivery_option tr td:nth-of-type(2),.delivery_options > .delivery_option tr td:nth-of-type(3),.delivery_options > .delivery_option tr td:nth-of-type(4)', function(e){
		$(this).parent().find('input.delivery_option_radio').prop('checked', true).trigger('click');
	});
	
	$(document).on('click', '.payment_module label', function(e){
		$(this).parent().find('input.payment_option').prop('checked', true).trigger('click');
	});
	
});