{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$opc}
        
    {assign var='current_step' value='address'}
    {capture name=path}<li property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" title="{l s='Your shopping cart'}">{l s='Your shopping cart'}</a><meta property="position" content></li>
        <li property="itemListElement" typeof="ListItem">{l s='Addresses'}</li>
    {/capture}
    {assign var="back_order_page" value="order.php"}
    </div></div></div>
    <div id="cart">
        <div class="container">
            <div class="flex-center"><hr><h2 class="heading">{l s='Twój koszyk z zakupami'}</h2><hr></div>              
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                    <div class="cart_block">
                        <ul class="navigation_cart">
                            <li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} third">
                                <i class="icon-check"></i> {l s='Dane osobowe'} <a class="edit" href="{$link->getPageLink('identity')|escape:'html':'UTF-8'}?back={'order.php?step=1'|escape:'url'}">{l s='edytuj'}</a>
                            </li>
                        </ul>
                        <ul> 
                        <h1 class="page-heading withborder"><span>2.</span>{l s='Adresy'}</h1>
                        {include file="$tpl_dir./errors.tpl"}
                        <form action="{$link->getPageLink($back_order_page, true)|escape:'html':'UTF-8'}" method="post" class="padding30 order17">
{else}
    {assign var="back_order_page" value="order-opc.php"}
    <h1 class="page-heading step-num"><span>1</span> {l s='Addresses'}</h1>
    <div id="opc_account" class="opc-main-block">
        <div id="opc_account-overlay" class="opc-overlay" style="display: none;"></div>
{/if}
<div class="addresses clearfix">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="address_delivery select form-group ">
                <label for="id_address_delivery">{if $cart->isVirtualCart()}{l s='Choose a billing address:'}{else}{l s='Choose a delivery address:'}{/if}</label>
                <select name="id_address_delivery" id="id_address_delivery" class="address_select form-control">
                    {foreach from=$addresses key=k item=address}
                        <option value="{$address.id_address|intval}"{if $address.id_address == $cart->id_address_delivery} selected="selected"{/if}>
                            {$address.alias|escape:'html':'UTF-8'}
                        </option>
                    {/foreach}
                </select><span class="waitimage"></span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
{*            
        <p class="checkbox addressesAreEquals"{if $cart->isVirtualCart()} style="display:none;"{/if}>
            <input type="checkbox" name="same" id="addressesAreEquals" value="1"{if $cart->id_address_invoice == $cart->id_address_delivery || $addresses|@count == 1} checked="checked"{/if} />
            <label for="addressesAreEquals">{l s='Use the delivery address as the billing address.'}</label>
        </p>    
*}        
            <div id="address_invoice_form" class="select form-group"{if $cart->id_address_invoice == $cart->id_address_delivery} style="display: none;"{/if}>
                {if $addresses|@count > 1}
                    <label for="id_address_invoice" class="strong">{l s='Choose a billing address:'}</label>
                    <select name="id_address_invoice" id="id_address_invoice" class="address_select form-control">
                        {section loop=$addresses step=-1 name=address}
                            <option value="{$addresses[address].id_address|intval}"{if $addresses[address].id_address == $cart->id_address_invoice && $cart->id_address_delivery != $cart->id_address_invoice} selected="selected"{/if}>
                                {$addresses[address].alias|escape:'html':'UTF-8'}
                            </option>
                        {/section}
                    </select><span class="waitimage"></span>
                {else}
                    {*
                    <a href="{$link->getPageLink('address', true, NULL, "back={$back_order_page}?step=1&select_address=1{if $back}&mod={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Add'}" class="button button-small btn btn-default">
                        <span>
                            {l s='Add a new address'}
                            <i class="icon-chevron-right right"></i>
                        </span>
                    </a>
                    *}
                {/if}
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row">
        <div class="col-xs-12 col-sm-6"{if $cart->isVirtualCart()} style="display:none;"{/if}>
            <ul class="address item" id="address_delivery"></ul>
        </div>
        <div class="col-xs-12 col-sm-6">
            <ul class="address alternate_item{if $cart->isVirtualCart()} full_width{/if} " id="address_invoice"></ul>
        </div>
    </div> <!-- end row -->
    <p class="address_add submit">
        <a href="{$link->getPageLink('address', true, NULL, "back={$back_order_page}?step=1{if $back}&mod={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Add'}" class="button button-small btn btn-default">
            <span>{l s='Add a new address'}<i class="icon-chevron-right right"></i></span>
        </a>
    </p>
    {if !$opc}
        <div id="ordermsg" class="form-group">
            <label>{l s='If you would like to add a comment about your order, please write it in the field below.'}</label>
            <textarea placeholder="{l s='Wpisz komentarz do zamówienia'}" class="form-control" cols="60" rows="6" name="message">{if isset($oldMessage)}{$oldMessage}{/if}</textarea>
        </div>
    {/if}
</div> <!-- end addresses -->

{if !$opc}
    <div class="cart_navigation clearfix navig">
        <input type="hidden" class="hidden" name="step" value="2" />
        <input type="hidden" name="back" value="{$back}" />
        <a href="{$link->getPageLink($back_order_page, true, NULL, "{if $back}back={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Previous'}" class="button-exclusive btn btn-default hidden">
            <i class="icon-chevron-left"></i>
            {l s='Continue Shopping'}
        </a>
        <div class="clearfix"></div>
        <button type="submit" name="processAddress" class="button btn btn-default button-medium transition">
            <span>{l s='Dalej'}</span>
        </button>
        <div class="clearfix"></div>
    </div>
</form></ul>
{else}
    </div> <!--  end opc_account -->
{/if}

<ul class="navigation_cart bottom">
    <li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done step_done_last{else}step_todo{/if}{/if} four">
        {if $current_step=='payment'}
            <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                <em>04.</em> {l s='Shipping'}
            </a>
        {else}
            <span><em>3.</em> {l s='Sposób dostawy'}</span>
        {/if}
    </li>
    <li id="step_end" class="{if $current_step=='payment'}step_current{else}step_todo{/if} last">
        <span><em>4.</em> {l s='Płatność'}</span>
    </li>
</ul>

</div>



</div>
<div class="col-xs-12 col-md-3">
    <div id="shopping-cart-right">
{include file="$tpl_dir./shopping-cart-right.tpl"}
    </div>
</div>
</div>
</div>
</div></div>
<div class="container">
    <div class="row">
        <div class="center_column col-xs-12 col-sm-12">


{strip}
{if !$opc}
{addJsDef orderProcess='order'}
{addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
{addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{addJsDefL name=CloseTxt}{l s='Submit' js=1}{/addJsDefL}
{/if}
{capture}{if $back}&mod={$back|urlencode}{/if}{/capture}
{capture name=addressUrl}{$link->getPageLink('address', true, NULL, 'back='|cat:$back_order_page|cat:'?step=1'|cat:$smarty.capture.default)|escape:'quotes':'UTF-8'}{/capture}
{addJsDef addressUrl=$smarty.capture.addressUrl}
{capture}{'&multi-shipping=1'|urlencode}{/capture}
{addJsDef addressMultishippingUrl=$smarty.capture.addressUrl|cat:$smarty.capture.default}
{capture name=addressUrlAdd}{$smarty.capture.addressUrl|cat:'&id_address='}{/capture}
{addJsDef addressUrlAdd=$smarty.capture.addressUrlAdd}
{addJsDef formatedAddressFieldsValuesList=$formatedAddressFieldsValuesList}
{addJsDef opc=$opc|boolval}
{capture}<h3 class="page-subheading">{l s='Your billing address' js=1}</h3>{/capture}
{addJsDefL name=titleInvoice}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<h3 class="page-subheading">{l s='Your delivery address' js=1}</h3>{/capture}
{addJsDefL name=titleDelivery}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<a class="button button-small btn btn-default" href="{$smarty.capture.addressUrlAdd}" title="{l s='Update' js=1}"><span>{l s='Update' js=1}<i class="icon-chevron-right right"></i></span></a>{/capture}
{addJsDefL name=liUpdate}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{/strip}