<div id="order-detail-content" class="table_block table-responsive">
    <table id="cart_summary" class="table table-bordered">
        <tfoot>
            {assign var='rowspan_total' value=2+$total_discounts_num+$total_wrapping_taxes_num}

            {if $use_taxes && $show_taxes && $total_tax != 0}
                {assign var='rowspan_total' value=$rowspan_total+1}
            {/if}

            {if $priceDisplay != 0}
                {assign var='rowspan_total' value=$rowspan_total+1}
            {/if}

            {if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}
                {assign var='rowspan_total' value=$rowspan_total+1}
            {else}
                {if $use_taxes && $total_shipping_tax_exc != $total_shipping}
                    {if $priceDisplay && $total_shipping_tax_exc > 0}
                        {assign var='rowspan_total' value=$rowspan_total+1}
                    {elseif $total_shipping > 0}
                        {assign var='rowspan_total' value=$rowspan_total+1}
                    {/if}
                {elseif $total_shipping_tax_exc > 0}
                    {assign var='rowspan_total' value=$rowspan_total+1}
                {/if}
            {/if}

            {if $use_taxes}
                {if $priceDisplay}
                    <tr class="cart_total_price">

                        <td colspan="{$col_span_subtotal}" class="text-left">{if $display_tax_label}{l s='Produkty'}{else}{l s='Produkty'}{/if}</td>
                        <td colspan="2" class="text-right price" id="total_product">{displayPrice price=$total_products}</td>
                    </tr>
                {else}
                    <tr class="cart_total_price">

                        <td colspan="{$col_span_subtotal}" class="text-left">{if $display_tax_label}{l s='Produkty'}{else}{l s='Produkty'}{/if}</td>
                        <td colspan="2" class="text-right price" id="total_product">{displayPrice price=$total_products_wt}</td>
                    </tr>
                {/if}
            {else}
                <tr class="cart_total_price">

                    <td colspan="{$col_span_subtotal}" class="text-left">{l s='Produkty'}</td>
                    <td colspan="2" class="text-right price" id="total_product">{displayPrice price=$total_products}</td>
                </tr>
            {/if}
            <tr{if $total_wrapping == 0} style="display: none;"{/if}>
                <td colspan="3" class="text-left">
                    {if $use_taxes}
                {if $display_tax_label}{l s='Total gift wrapping (tax incl.)'}{else}{l s='Total gift-wrapping cost'}{/if}
            {else}
                {l s='Total gift-wrapping cost'}
            {/if}
        </td>
        <td colspan="2" class="text-right price-discount price" id="total_wrapping">
            {if $use_taxes}
                {if $priceDisplay}
                    {displayPrice price=$total_wrapping_tax_exc}
                {else}
                    {displayPrice price=$total_wrapping}
                {/if}
            {else}
                {displayPrice price=$total_wrapping_tax_exc}
            {/if}
        </td>
    </tr>
    {if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}
        <tr class="cart_total_delivery">
            <td colspan="{$col_span_subtotal}" class="text-left">{l s='Dostawa'}</td>
            <td colspan="2" class="text-right price" id="total_shipping">{l s='Free shipping!'}</td>
        </tr>
    {else}
        {if $use_taxes && $total_shipping_tax_exc != $total_shipping}
            {if $priceDisplay}
                <tr class="cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">
                    <td colspan="{$col_span_subtotal}" class="text-left">{if $display_tax_label}{l s='Total shipping (tax excl.)'}{else}{l s='Total shipping'}{/if}</td>
                    <td colspan="2" class="text-right price" id="total_shipping">{displayPrice price=$total_shipping_tax_exc}</td>
                </tr>
            {else}
                <tr class="cart_total_delivery{if $total_shipping <= 0} unvisible{/if}">
                    <td colspan="{$col_span_subtotal}" class="text-left">{if $display_tax_label}{l s='Total shipping (tax incl.)'}{else}{l s='Total shipping'}{/if}</td>
                    <td colspan="2" class="text-right price" id="total_shipping" >{displayPrice price=$total_shipping}</td>
                </tr>
            {/if}
        {else}
            <tr class="cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">
                <td colspan="{$col_span_subtotal}" class="text-left">{l s='Total shipping'}</td>
                <td colspan="2" class="text-right price" id="total_shipping" >{displayPrice price=$total_shipping_tax_exc}</td>
            </tr>
        {/if}
    {/if}
    <tr class="cart_total_voucher{if $total_discounts == 0} unvisible{/if}">
        <td colspan="{$col_span_subtotal}" class="text-left">
            {l s='Rabat'}
        </td>
        <td colspan="2" class="text-right price-discount price" id="total_discount">
            {if $use_taxes && $priceDisplay == 0}
                {assign var='total_discounts_negative' value=$total_discounts * -1}
            {else}
                {assign var='total_discounts_negative' value=$total_discounts_tax_exc * -1}
            {/if}
            {displayPrice price=$total_discounts_negative}
        </td>
    </tr>
    {if $use_taxes && $show_taxes && $total_tax != 0 }
        {if $priceDisplay != 0}
            <tr class="cart_total_price">
                <td colspan="{$col_span_subtotal}" class="text-left">{if $display_tax_label}{l s='Total (tax excl.)'}{else}{l s='Total'}{/if}</td>
                <td colspan="2" class="text-right price" id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</td>
            </tr>
        {/if}
        <tr class="cart_total_tax">
            <td colspan="{$col_span_subtotal}" class="text-left">{l s='Tax'}</td>
            <td colspan="2" class="text-right price" id="total_tax">{displayPrice price=$total_tax}</td>
        </tr>
    {/if}
    <tr class="cart_total_price prenext">
        <td colspan="{$col_span_subtotal}" class="total_price_container text-left">
            <span>{l s='Razem'} <span>{l s='(brutto)'}</span></span>
            <div class="hookDisplayProductPriceBlock-price">
                {hook h="displayCartTotalPriceLabel"}
            </div>
        </td>
        {if $use_taxes}
            <td colspan="2" class="text-right price" id="total_price_container">
                <span id="total_price">{displayPrice price=$total_price}</span>
            </td>
        {else}
            <td colspan="2" class="text-right price" id="total_price_container">
                <span id="total_price">{displayPrice price=$total_price_without_tax}</span>
            </td>
        {/if}
    </tr>
    
        <tr class="next">
{if $finalizacja}        
            <td colspan="2">
                {if !$opc}
                    <a  href="{if $back}{$link->getPageLink('order', true, NULL, 'step=1&amp;back={$back}')|escape:'html':'UTF-8'}{else}{$link->getPageLink('order', true, NULL, 'step=1')|escape:'html':'UTF-8'}{/if}" class="button btn btn-default standard-checkout button-medium" title="{l s='Proceed to checkout'}">
                        <span>{l s='Finalizacja zamówienia'}</span>
                    </a>
                {/if}
            </td>
 {/if}            
        </tr>
   
</tfoot>
</table>
</div>
</div>
{if $etap_platnosci}
    <div class="hidden">
        <div id="order-detail-content" class="table_block table-responsive voucher_form">
            <table id="cart_summary" class="table table-bordered">
                <tfoot>
                    {if $use_taxes}
                        {if $priceDisplay}
                            <tr class="cart_total_price">
                                <td rowspan="{$rowspan_total}" colspan="3" id="cart_voucher" class="cart_voucher">
                                    {if $voucherAllowed}
                                        <form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
                                            <fieldset>
                                                <div class="vou_info">
                                                    {l s='Wpisz swój'} <span>{l s='kod rabatowy'}</span> {l s='jeśli posiadasz i zyskaj'} <span class="big">{l s='nawet 10% rabatu!'}</span>
                                                </div>
                                                <input placeholder="{l s='Wpisz kod rabatowy'}" type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
                                                <input type="hidden" name="submitDiscount" />
                                                <button type="submit" name="submitAddDiscount" class="button btn btn-default button-small"><span>{l s='OK'}</span></button>
                                            </fieldset>
                                        </form>
                                        {if $displayVouchers}
                                            <p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
                                            <div id="display_cart_vouchers">
                                                {foreach $displayVouchers as $voucher}
                                                    {if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
                                                {/foreach}
                                            </div>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {else}
                            <tr class="cart_total_price">
                                <td rowspan="{$rowspan_total}" colspan="2" id="cart_voucher" class="cart_voucher">
                                    {if $voucherAllowed}
                                        <form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
                                            <fieldset>
                                                <div class="vou_info">
                                                    {l s='Wpisz swój'} <span>{l s='kod rabatowy'}</span> {l s='jeśli posiadasz i zyskaj'} <span class="big">{l s='nawet 10% rabatu!'}</span>
                                                </div>
                                                <input placeholder="{l s='Wpisz kod rabatowy'}" type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
                                                <input type="hidden" name="submitDiscount" />
                                                <button type="submit" name="submitAddDiscount" class="button btn btn-default button-small"><span>{l s='OK'}</span></button>
                                            </fieldset>
                                        </form>
                                        {if $displayVouchers}
                                            <p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
                                            <div id="display_cart_vouchers">
                                                {foreach $displayVouchers as $voucher}
                                                    {if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
                                                {/foreach}
                                            </div>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                    {else}
                        <tr class="cart_total_price">
                            <td rowspan="{$rowspan_total}" colspan="2" id="cart_voucher" class="cart_voucher">
                                {if $voucherAllowed}
                                    <form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
                                        <fieldset>
                                            <div class="vou_info">
                                                {l s='Wpisz swój'} <span>{l s='kod rabatowy'}</span> {l s='jeśli posiadasz i zyskaj'} <span class="big">{l s='nawet 10% rabatu!'}</span>
                                            </div>
                                            <input placeholder="{l s='Wpisz kod rabatowy'}" type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
                                            <input type="hidden" name="submitDiscount" />
                                            <button type="submit" name="submitAddDiscount" class="button btn btn-default button-small">
                                                <span>{l s='OK'}</span>
                                            </button>
                                        </fieldset>
                                    </form>
                                    {if $displayVouchers}
                                        <p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
                                        <div id="display_cart_vouchers">
                                            {foreach $displayVouchers as $voucher}
                                                {if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
                                            {/foreach}
                                        </div>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                    {/if}
                </tfoot>
            </table>
        </div>
    </div>
{/if}
<div class="platnosci_block_right">
    {assign var=platnosci value=CMS::getOneCMS(14, $cookie->id_lang)}
    {$platnosci.content}
</div>