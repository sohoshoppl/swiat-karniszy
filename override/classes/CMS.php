<?php
class CMS extends CMSCore
{
	public static function getOneCMS($id_cms, $id_lang = null, $id_shop = null)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('cms', 'c');

		if ($id_lang)
		{
			if ($id_shop)
				$sql->innerJoin('cms_lang', 'l', 'c.id_cms = l.id_cms AND l.id_lang = '.(int)$id_lang.' AND l.id_shop = '.(int)$id_shop);
			else
				$sql->innerJoin('cms_lang', 'l', 'c.id_cms = l.id_cms AND l.id_lang = '.(int)$id_lang);
		}

		$sql->where('c.active = 1');

		$sql->where('c.id_cms = '.(int)$id_cms);

		return Db::getInstance()->getRow($sql);
	}
}