<?php
class Tools extends ToolsCore
{
	public static function getPath($id_category, $path = '', $link_on_the_item = false, $category_type = 'products', Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $id_category = (int)$id_category;
        if ($id_category == 1) {
            return ''.$path.'';
        }

        $pipe = Configuration::get('PS_NAVIGATION_PIPE');
        if (empty($pipe)) {
            $pipe = '>';
        }

        $full_path = '';
        if ($category_type === 'products') {
            $interval = Category::getInterval($id_category);
            $id_root_category = $context->shop->getCategory();
            $interval_root = Category::getInterval($id_root_category);
            if ($interval) {
                $sql = 'SELECT c.id_category, cl.name, cl.link_rewrite
						FROM '._DB_PREFIX_.'category c
						LEFT JOIN '._DB_PREFIX_.'category_lang cl ON (cl.id_category = c.id_category'.Shop::addSqlRestrictionOnLang('cl').')
						'.Shop::addSqlAssociation('category', 'c').'
						WHERE c.nleft <= '.$interval['nleft'].'
							AND c.nright >= '.$interval['nright'].'
							AND c.nleft >= '.$interval_root['nleft'].'
							AND c.nright <= '.$interval_root['nright'].'
							AND cl.id_lang = '.(int)$context->language->id.'
							AND c.active = 1
							AND c.level_depth > '.(int)$interval_root['level_depth'].'
						ORDER BY c.level_depth ASC';
                $categories = Db::getInstance()->executeS($sql);

                $n = 1;
                $pathpos =0;
                $n_categories = count($categories);
                foreach ($categories as $category) {
                    $full_path .= 
                    (($n < $n_categories || $link_on_the_item) ? '<li property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage"  href="'.Tools::safeOutput($context->link->getCategoryLink((int)$category['id_category'], $category['link_rewrite'])).'" title="'.htmlentities($category['name'], ENT_NOQUOTES, 'UTF-8').'" data-gg="">' : '<li>').
                    htmlentities($category['name'], ENT_NOQUOTES, 'UTF-8').
                    (($n < $n_categories || $link_on_the_item) ? '</a><meta content="'.$pathpos.'" property="position"></li>' : '</li>').
                    (($n++ != $n_categories) ? '' : '');
					$pathpos++;
                }
                $full_path .= ((!empty($path)) ? '<li>' : '') . '';
                return $full_path.$path;
            }
        } elseif ($category_type === 'CMS') {
            $category = new CMSCategory($id_category, $context->language->id);
            if (!Validate::isLoadedObject($category)) {
                die(Tools::displayError());
            }
            $category_link = $context->link->getCMSCategoryLink($category);
			$full_path .= '<li property="itemListElement" typeof="ListItem">';
            if ($path != $category->name) {
                $full_path .= '<a href="'.Tools::safeOutput($category_link).'" data-gg="">'.htmlentities($category->name, ENT_NOQUOTES, 'UTF-8').'</a>'.$path;
            } else {
                $full_path = ($link_on_the_item ? '<a href="'.Tools::safeOutput($category_link).'" data-gg="">' : '').htmlentities($path, ENT_NOQUOTES, 'UTF-8').($link_on_the_item ? '</a>' : '');
            }
			$full_path .= '</li>';
            return Tools::getPath($category->id_parent, $full_path, $link_on_the_item, $category_type);
        }
    }

    /**
    * @param string [optionnal] $type_cat defined what type of categories is used (products or cms)
    */
    public static function getFullPath($id_category, $end, $type_cat = 'products', Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $id_category = (int)$id_category;
        $pipe = (Configuration::get('PS_NAVIGATION_PIPE') ? Configuration::get('PS_NAVIGATION_PIPE') : '>');

        $default_category = 1;
        if ($type_cat === 'products') {
            $default_category = $context->shop->getCategory();
            $category = new Category($id_category, $context->language->id);
        } elseif ($type_cat === 'CMS') {
            $category = new CMSCategory($id_category, $context->language->id);
        }

        if (!Validate::isLoadedObject($category)) {
            $id_category = $default_category;
        }
        if ($id_category == $default_category) {
            return htmlentities($end, ENT_NOQUOTES, 'UTF-8');
        }

        return Tools::getPath($id_category, $category->name, true, $type_cat).'<li typeof="ListItem" property="itemListElement">'.htmlentities($end, ENT_NOQUOTES, 'UTF-8').'</li>';
    }
}