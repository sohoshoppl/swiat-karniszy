<?php

/*
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2016 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @property Product $object
 */
class AdminProductsController extends AdminProductsControllerCore {

    public function ajaxProcessaddProductImage() {
        self::$currentIndex = 'index.php?tab=AdminProducts';
        $product = new Product((int) Tools::getValue('id_product'));
        $legends = Tools::getValue('legend');

        if (!is_array($legends)) {
            $legends = (array) $legends;
        }

        if (!Validate::isLoadedObject($product)) {
            $files = array();
            $files[0]['error'] = Tools::displayError('Cannot add image because product creation failed.');
        }

        $image_uploader = new HelperImageUploader('file');
        $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($this->max_image_size);
        $files = $image_uploader->process();

        foreach ($files as &$file) {
            if ($file['type'] == "image/gif") {
                $image = new Image();
                $image->id_product = (int) ($product->id);
                $image->position = Image::getHighestPosition($product->id) + 1;

                foreach ($legends as $key => $legend) {
                    if (!empty($legend)) {
                        $image->legend[(int) $key] = $legend;
                    }
                }

                if (!Image::getCover($image->id_product)) {
                    $image->cover = 1;
                } else {
                    $image->cover = 0;
                }

                if (($validate = $image->validateFieldsLang(false, true)) !== true) {
                    $file['error'] = Tools::displayError($validate);
                }

                if (isset($file['error']) && (!is_numeric($file['error']) || $file['error'] != 0)) {
                    continue;
                }

                if (!$image->add()) {
                    $file['error'] = Tools::displayError('Error while creating additional image');
                } else {

                    if (!$new_path = $image->getPathForCreation()) {
                        $file['error'] = Tools::displayError('An error occurred during new folder creation');
                        continue;
                    }
                    /* skopiować z phpinfo.php */
                    $imagick = new Imagick($file['save_path']);
                    $imagick->writeImages($new_path . '.gif', true);
                    rename($new_path . '.gif', $new_path . '.' . $image->image_format);

                    $imagick->clear();
                    $imagick->destroy();
                    $error = 0;

                    /* 1. dodajemy orginał  */
                    /* 2. dodajemy kopie w różnych rozmiarach */
                    $imagesTypes = ImageType::getImagesTypes('products');
                    $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');

                    foreach ($imagesTypes as $imageType) {
                        $imagick = new Imagick($file['save_path']);
                        $imagick->writeImages($new_path . '-' . stripslashes($imageType['name']) . '.gif', true);
                        $imagick->clear();
                        $imagick->destroy();
                        rename($new_path . '-' . stripslashes($imageType['name']) . '.gif', $new_path . '-' . stripslashes($imageType['name']) . '.' . $image->image_format);
                    }
                    unlink($file['save_path']);
                    //Necesary to prevent hacking
                    unset($file['save_path']);
                    Hook::exec('actionWatermark', array('id_image' => $image->id, 'id_product' => $product->id));

                    if (!$image->update()) {
                        $file['error'] = Tools::displayError('Error while updating status');
                        continue;
                    }

                    // Associate image to shop from context
                    $shops = Shop::getContextListShopID();
                    $image->associateTo($shops);
                    $json_shops = array();

                    foreach ($shops as $id_shop) {
                        $json_shops[$id_shop] = true;
                    }

                    $file['status'] = 'ok';
                    $file['id'] = $image->id;
                    $file['position'] = $image->position;
                    $file['cover'] = $image->cover;
                    $file['legend'] = $image->legend;
                    $file['path'] = $image->getExistingImgPath();
                    $file['shops'] = $json_shops;

                    @unlink(_PS_TMP_IMG_DIR_ . 'product_' . (int) $product->id . '.jpg');
                    @unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $product->id . '_' . $this->context->shop->id . '.jpg');
                }
            } else {
                $image = new Image();
                $image->id_product = (int) ($product->id);
                $image->position = Image::getHighestPosition($product->id) + 1;

                foreach ($legends as $key => $legend) {
                    if (!empty($legend)) {
                        $image->legend[(int) $key] = $legend;
                    }
                }

                if (!Image::getCover($image->id_product)) {
                    $image->cover = 1;
                } else {
                    $image->cover = 0;
                }

                if (($validate = $image->validateFieldsLang(false, true)) !== true) {
                    $file['error'] = Tools::displayError($validate);
                }

                if (isset($file['error']) && (!is_numeric($file['error']) || $file['error'] != 0)) {
                    continue;
                }

                if (!$image->add()) {
                    $file['error'] = Tools::displayError('Error while creating additional image');
                } else {
                    if (!$new_path = $image->getPathForCreation()) {
                        $file['error'] = Tools::displayError('An error occurred during new folder creation');
                        continue;
                    }

                    $error = 0;

                    if (!ImageManager::resize($file['save_path'], $new_path . '.' . $image->image_format, null, null, 'jpg', false, $error)) {
                        switch ($error) {
                            case ImageManager::ERROR_FILE_NOT_EXIST :
                                $file['error'] = Tools::displayError('An error occurred while copying image, the file does not exist anymore.');
                                break;

                            case ImageManager::ERROR_FILE_WIDTH :
                                $file['error'] = Tools::displayError('An error occurred while copying image, the file width is 0px.');
                                break;

                            case ImageManager::ERROR_MEMORY_LIMIT :
                                $file['error'] = Tools::displayError('An error occurred while copying image, check your memory limit.');
                                break;

                            default:
                                $file['error'] = Tools::displayError('An error occurred while copying image.');
                                break;
                        }
                        continue;
                    } else {
                        $imagesTypes = ImageType::getImagesTypes('products');
                        $generate_hight_dpi_images = (bool) Configuration::get('PS_HIGHT_DPI');

                        foreach ($imagesTypes as $imageType) {
                            if (!ImageManager::resize($file['save_path'], $new_path . '-' . stripslashes($imageType['name']) . '.' . $image->image_format, $imageType['width'], $imageType['height'], $image->image_format)) {
                                $file['error'] = Tools::displayError('An error occurred while copying image:') . ' ' . stripslashes($imageType['name']);
                                continue;
                            }

                            if ($generate_hight_dpi_images) {
                                if (!ImageManager::resize($file['save_path'], $new_path . '-' . stripslashes($imageType['name']) . '2x.' . $image->image_format, (int) $imageType['width'] * 2, (int) $imageType['height'] * 2, $image->image_format)) {
                                    $file['error'] = Tools::displayError('An error occurred while copying image:') . ' ' . stripslashes($imageType['name']);
                                    continue;
                                }
                            }
                        }
                    }

                    unlink($file['save_path']);
                    //Necesary to prevent hacking
                    unset($file['save_path']);
                    Hook::exec('actionWatermark', array('id_image' => $image->id, 'id_product' => $product->id));

                    if (!$image->update()) {
                        $file['error'] = Tools::displayError('Error while updating status');
                        continue;
                    }

                    // Associate image to shop from context
                    $shops = Shop::getContextListShopID();
                    $image->associateTo($shops);
                    $json_shops = array();

                    foreach ($shops as $id_shop) {
                        $json_shops[$id_shop] = true;
                    }

                    $file['status'] = 'ok';
                    $file['id'] = $image->id;
                    $file['position'] = $image->position;
                    $file['cover'] = $image->cover;
                    $file['legend'] = $image->legend;
                    $file['path'] = $image->getExistingImgPath();
                    $file['shops'] = $json_shops;

                    @unlink(_PS_TMP_IMG_DIR_ . 'product_' . (int) $product->id . '.jpg');
                    @unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int) $product->id . '_' . $this->context->shop->id . '.jpg');
                }
            }
        }

        die(Tools::jsonEncode(array($image_uploader->getName() => $files)));
    }

}
