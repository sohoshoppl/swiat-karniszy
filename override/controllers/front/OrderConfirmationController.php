<?
class OrderConfirmationController extends OrderConfirmationControllerCore
{
	
	public function setMedia()
    {
        parent::setMedia();

        $this->addCSS(_THEME_CSS_DIR_.'order17.css');
		$this->addCSS(_THEME_CSS_DIR_.'product_list.css');
		$this->addJS(_THEME_JS_DIR_.'order17.js');
    }

}
?>