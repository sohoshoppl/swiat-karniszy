{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}

{capture name=path}
<li>
    <a href="{smartblog::GetSmartBlogLink('smartblog')|escape:'htmlall':'UTF-8'}">
        {l s='Wszystkie nowe posty' mod='smartblog'}
    </a>
</li>
<li>
    {$meta_title|escape:'htmlall':'UTF-8'}
</li>{/capture}
<div id="content" class="block">
    <div itemtype="#" itemscope="" id="sdsblogArticle" class="blog-post">
        <div class="hsas">
            {$meta_title|escape:'htmlall':'UTF-8'}
        </div>

        <div itemprop="articleBody" class="clearfix">
            <div class="articleContent">
                {include file="./post_format.tpl" post=$post post_img = $post_img smartshownoimg=$smartshownoimg}
            </div>

            <div class="sdsarticle-des">
                {if $post.post_format != 'quote'}

                {$blogcontent|escape:'quotes':'UTF-8'|replace:"\'":"'"}

                {/if}
            </div>
            {if $tags != ''}
            <div class="sdstags-update">
                <span class="tags">
                    <b>
                        {l s='Tagi:' mod='smartblog'}
                    </b>
                    {foreach from=$tags item=tag}
                    <a title="tag" href="{$smartbloglink->getSmartBlogTag($tag.name|urlencode)|escape:'htmlall':'UTF-8'}">
                        {$tag.name|escape:'htmlall':'UTF-8'}
                    </a>

                    {/foreach}
                </span>
            </div>
            {/if}
        </div>

        <div class="sdsarticleHeader">

            <span>
                {if $smartshowauthor ==1} {l s='Autor ' mod='smartblog'} &nbsp;
                <i class="icon icon-user">
                </i>
                <span itemprop="author">
                    {if $smartshowauthorstyle != 0}{l s='ePioro.pl' mod='smartblog'}{else}{l s='ePioro.pl' mod='smartblog'}{/if}
                </span>&nbsp;

                <i class="icon icon-calendar">
                </i>&nbsp;
                <span itemprop="dateCreated">
                    {$created|date_format|escape:'htmlall':'UTF-8'}
                </span>{/if}
                <span itemprop="articleSection">

                    {$assocCats = BlogCategory::getPostCategoriesFull($post.id_post)}
                    {$catCounts = 0}
                    {if !empty($assocCats)}
                    &nbsp;&nbsp;
                    <i class="icon icon-tags">
                    </i>&nbsp;

                    {foreach $assocCats as $catid=>$assoCat}
                    {if $catCounts > 0}, {/if}
                    {$catlink=[]}
                    {$catlink.id_category = $assoCat.id_category}
                    {$catlink.slug = $assoCat.link_rewrite}
                    <a href="{$smartbloglink->getSmartBlogCategoryLink($assoCat.id_category,$assoCat.link_rewrite)|escape:'htmlall':'UTF-8'}">
                        {$assoCat.name|escape:'htmlall':'UTF-8'}
                    </a>
                    {$catCounts = $catCounts + 1}
                    {/foreach}
                    {/if}

                </span> &nbsp;
                <i class="icon icon-comments">
                </i>&nbsp; {if $countcomment != ''}{$countcomment|escape:'htmlall':'UTF-8'}{else}{l s='0' mod='smartblog'}{/if}{l s=' Komentarzy' mod='smartblog'}
            </span>
            <a title="" style="display:none" itemprop="url" href="#">
            </a>
        </div>

        <div class="sdsarticleBottom">

        </div>
    </div>

{*
<div id="product_comments_block_tab">

    <ul class="footer_links clearfix">

        {foreach from=$posts_previous item="post"}
        {if isset($post.id_smart_blog_post)}
        <li>
            <a title="{l s='Poprzedni post' mod='smartblog'}" href="{$smartbloglink->getSmartBlogPostLink($post.id_smart_blog_post,$post.link_rewrite)|escape:'htmlall':'UTF-8'}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
                <span class="transition">
                    {l s='Poprzedni post' mod='smartblog'}
                </span>
            </a>
        </li>
        {/if}
        {/foreach}
        {foreach from=$posts_next item="post"}

        {if isset($post.id_smart_blog_post)}
        <li class="pull-right">
            <a title="{l s='Następny post' mod='smartblog'}" href="{$smartbloglink->getSmartBlogPostLink($post.id_smart_blog_post,$post.link_rewrite)|escape:'htmlall':'UTF-8'}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
                <span class="transition">
                    {l s='Następny post' mod='smartblog'}
                </span></span>
            </a>
        </li>
        {/if}
        {/foreach}
    </ul>

</div>
*}
{if $countcomment != ''}
<div id="articleComments" >
    <h3>
        {if $countcomment != ''}{$countcomment|escape:'htmlall':'UTF-8'}{else}{l s='0' mod='smartblog'}{/if}{l s=' Komantarzy' mod='smartblog'}
        <span>
        </span>
    </h3>
    <div id="comments">
        <ul class="commentList">
            {$i=1}
            {foreach from=$comments item=comment}

            {include file="./comment_loop.tpl" childcommnets=$comment}

            {/foreach}
        </ul>
    </div>
</div>
{/if}

{if ($enableguestcomment==0) && isset($is_looged) && $is_looged==''}
<section class="page-product-box">
    <h3 class="page-product-heading">
        {l s='Komentarze' mod='smartblog'}
    </h3>
    {l s='Zaloguj lub zarejestruj się by dodawać wpisy na blogu' mod='smartblog'}
</section>
{else}
{if Configuration::get('smartenablecomment') == 1}
{if $comment_status == 1}
<div class="smartblogcomments clearfix" id="respond">
    <!-- <h4 id="commentTitle">{l s='Wpisz swój komentarz'  mod='smartblog'}</h4> -->
    <h4 class="comment-reply-title" id="reply-title">
        {l s='Skomentuj'  mod='smartblog'}
        <small style="float:right;">
            <a style="display: none;" href="/wp/sellya/sellya/this-is-a-post-with-preview-image/#respond"
                   id="cancel-comment-reply-link" rel="nofollow">
                {l s='Anuluj odpowiedź'  mod='smartblog'}
            </a>
        </small>
    </h4>
    <div id="commentInput">
        <form action="" method="post" id="commentform">
            {if ($enableguestcomment==0) && isset($is_looged) && $is_looged>0}
            <input type="hidden" tabindex="1" class="inputName form-control grey" value="{$is_looged_fname|escape:'htmlall':'UTF-8'}" name="name" id="name">
            <input type="hidden" tabindex="2" class="inputMail form-control grey" value="{$is_looged_email|escape:'htmlall':'UTF-8'}" name="mail" id="mail">
            <input type="hidden" tabindex="3" value="" name="website" class="form-control grey">
            {else}
            <div class="form-group row">
                <label class="control-label col-sm-4" for="name">
                    {l s='Imię:'  mod='smartblog'}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-sm-8">
                    <input type="text" tabindex="1" class="inputName form-control grey" value="" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4" for="mail">
                    {l s='E-mail:' mod='smartblog'}
                    <span class="required">
                        *
                    </span>
                    <span class="note">
                        {l s='(niewidoczny)'  mod='smartblog'}
                    </span>
                </label>
                <div class="col-sm-8">
                    <input type="text" tabindex="2" class="inputMail form-control grey" value="" name="mail">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4" for="website">
                    {l s='Strona WWW:'  mod='smartblog'}
                    <span class="note">
                        {l s='(URL z '  mod='smartblog'}http://)
                    </span>
                </label>
                <div class="col-sm-8">
                    <input type="text" tabindex="3" value="" name="website" class="form-control grey">
                </div>
            </div>

            {/if}
            <div class="form-group row">
                <label class="control-label col-sm-4" for="comment">
                    {l s='Komentarz:'  mod='smartblog'}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-sm-8">
                    <textarea tabindex="4" class="inputContent form-control grey" rows="8" cols="50" name="comment">
                    </textarea>
                </div>
            </div>

            {if Configuration::get('smartcaptchaoption') == '1'}

            <div class="form-group row">
                <label class="control-label col-sm-4" for="smartblogcaptcha">
                    <img src="{$modules_dir|escape:'htmlall':'UTF-8'}smartblog/classes/CaptchaSecurityImages.php?width=100&height=40&characters=5">
                </label>
                <div class="col-sm-8">
                    <input type="text" tabindex="" value="" name="smartblogcaptcha" placeholder="{l s='Wpisz kod' mod='smartblog'}" class="smartblogcaptcha form-control grey">
                </div>
            </div>

            {/if}

            <input type='hidden' name='comment_post_ID' value='1478' id='comment_post_ID' />
            <input type='hidden' name='id_post' value='{$id_post|escape:'htmlall':'UTF-8'}' id='id_post' />
            <input type='hidden' name='comment_parent' id='comment_parent' value='0' />

            <div class="submit clearfix">
                <button type="submit" name="addComment" id="submitComment" class="button btn btn-default button-medium pull-right" >
                    <span class="transition">
                        {l s='Wyślij' mod='smartblog'}
                    </span>
                </button>
            </div>
        </form>
    </div>
</div>
{/if}
{if isset($HOOK_SMART_BLOG_POST_FOOTER)}
{$HOOK_SMART_BLOG_POST_FOOTER}
{/if}
</div>
<script type="text/javascript">
    $('#submitComment').bind('click',function(event) {
            event.preventDefault();


            var data = {
                'action':'postcomment',
                'id_post':$('input[name=\'id_post\']').val(),
                'comment_parent':$('input[name=\'comment_parent\']').val(),
                'name':$('input[name=\'name\']').val(),
                'website':$('input[name=\'website\']').val(),
                'smartblogcaptcha':$('input[name=\'smartblogcaptcha\']').val(),
                'comment':$('textarea[name=\'comment\']').val(),
                'mail':$('input[name=\'mail\']').val()
            };
            $.ajax( {
                    url: baseDir + 'modules/smartblog/ajax.php',
                    data: data,
                    method: 'POST',
                    dataType: 'json',

                    beforeSend: function() {
                        $('.success, .warning, .error').remove();
                        $('#submitComment').attr('disabled', true);
                        $('#commentInput').before('<div class="attention"><img src="http://321cart.com/sellya/catalog/view/theme/default/image/loading.gif" alt="" />Please wait!</div>');

                    },
                    complete: function() {
                        $('#submitComment').attr('disabled', false);
                        $('.attention').remove();
                    },
                    success: function(json) {
                        if (json['error']) {

                            $('#commentInput').before('<div class="warning">' + '<i class="icon-warning-sign icon-lg"></i>' + json['error']['common'] + '</div>');

                            if (json['error']['name']) {
                                $('.inputName').after('<span class="error">' + json['error']['name'] + '</span>');
                            }
                            if (json['error']['mail']) {
                                $('.inputMail').after('<span class="error">' + json['error']['mail'] + '</span>');
                            }
                            if (json['error']['comment']) {
                                $('.inputContent').after('<span class="error">' + json['error']['comment'] + '</span>');
                            }
                            if (json['error']['captcha']) {
                                $('.smartblogcaptcha').after('<span class="error">' + json['error']['captcha'] + '</span>');
                            }
                        }

                        if (json['success']) {
                            $('input[name=\'name\']').val('');
                            $('input[name=\'mail\']').val('');
                            $('input[name=\'website\']').val('');
                            $('textarea[name=\'comment\']').val('');
                            $('input[name=\'smartblogcaptcha\']').val('');

                            $('#commentInput').before('<div class="success">' + json['success'] + '</div>');
                            setTimeout(function(){
                                    $('.success').fadeOut(300).delay(450).remove();
                                },2500);

                        }
                    }
                } );
        } );






    var addComment = {
        moveForm : function(commId, parentId, respondId, postId) {

            var t = this, div, comm = t.I(commId), respond = t.I(respondId), cancel = t.I('cancel-comment-reply-link'), parent = t.I('comment_parent'), post = t.I('comment_post_ID');
            if ( ! comm || ! respond || ! cancel || ! parent )
            return;

            t.I('mail').value='{$is_looged_email|escape:'htmlall':'UTF-8'}';
            t.I('name').value='{$is_looged_fname|escape:'htmlall':'UTF-8'}';
            t.respondId = respondId;
            postId = postId || false;

            if ( ! t.I('wp-temp-form-div') ) {
                div = document.createElement('div');
                div.id = 'wp-temp-form-div';
                div.style.display = 'none';
                respond.parentNode.insertBefore(div, respond);
            }


            comm.parentNode.insertBefore(respond, comm.nextSibling);
            if ( post && postId )
            post.value = postId;
            parent.value = parentId;
            cancel.style.display = '';

            cancel.onclick = function() {
                var t = addComment, temp = t.I('wp-temp-form-div'), respond = t.I(t.respondId);

                if ( ! temp || ! respond )
                return;

                t.I('comment_parent').value = '0';
                t.I('mail').value='{$is_looged_email|escape:'htmlall':'UTF-8'}';
                t.I('name').value='{$is_looged_fname|escape:'htmlall':'UTF-8'}';
                temp.parentNode.insertBefore(respond, temp);
                temp.parentNode.removeChild(temp);
                this.style.display = 'none';
                this.onclick = null;
                return false;
            };

            try {
                t.I('comment').focus();
            }
            catch(e) {
            }

            return false;
        },

        I : function(e) {
            var elem = document.getElementById(e);
            if(!elem){
                return document.querySelector('[name="'+e+'"]');
            }else{
                return elem;
            }
        }
    };



</script>
{/if}
{/if}
{if isset($smartcustomcss)}
<style>
    {
        $
        smartcustomcss|
        escape: 'htmlall':'UTF-8'
    }
</style>
{/if}
{/if}
