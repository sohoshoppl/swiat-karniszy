<?php

class sohoshoplistapakunkowaprintController extends ModuleAdminController
{

    protected $module_name = 'sohoshoplistapakunkowa';
    public $bootstrap = true;
    public $custom_smarty;

    public function __construct()
    {
        $this->context = Context::getContext();
        $this->custom_smarty = new Smarty();
        $this->custom_smarty->setTemplateDir(_PS_MODULE_DIR_ . $this->module_name . '/views/templates/admin/');
        $this->custom_smarty->registerPlugin('modifier', 'truncate', 'smarty_modifier_truncate');


        parent::__construct();
    }



    public function renderList()
    {
        $id_order = (int)Tools::getValue('id_order');
        if ($id_order > 0) {
            $order_obj     = new OrderCore($id_order);
            $order_details = OrderDetail::getList($id_order);
            $id_cart       = (int) $order_obj->id_cart;
            $products = array();

            foreach ($order_details as $product) {
                $products[$product['id_order_detail']]['kolekcja'] = $this->module->getKolekcja($product['product_id']);
                $products[$product['id_order_detail']]['qnt'] = $product['product_quantity'];
                //$p_tmp_name = explode(' - ', $product['product_name']);
                //$products[$product['id_order_detail']]['name'] = $p_tmp_name[0];
                $products[$product['id_order_detail']]['name'] = $product['product_name'];
                $cart_instructions = $this->module->getCartInstructions($id_cart, $product['product_id'], $product['product_attribute_id']); //pobieramy z koszyka wybrane opcje kreatora
                $products[$product['id_order_detail']]['atr'] = $cart_instructions['atr'];
                $products[$product['id_order_detail']]['dl_rur'] = $cart_instructions['dl_rur'];
                if ($cart_instructions['typ'] != 0) {
                    $products[$product['id_order_detail']]['typ'] = $cart_instructions['typ'];
                }
                if ( sizeof($cart_instructions['srednice_rur']) != 0) {
                    $products[$product['id_order_detail']]['srednice_rur'] = $cart_instructions['srednice_rur'];
                }
            }
        }
        $dictionary = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'sohoshopcartproductrename_dictionary`');
        $tpl        = $this->custom_smarty->createTemplate('content.tpl');
        $tpl->assign(
            array(
                'products'  => $products,
                'dictionary'=> $dictionary,

            ));
        echo $tpl->fetch();
        die();


    }



}

?>