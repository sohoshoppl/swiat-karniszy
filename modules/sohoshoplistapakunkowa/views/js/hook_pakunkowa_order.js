$(document).ready(function () {
        var rury_nazwy = new Array();
        var rury_ilosci = new Array();
        var laczniki_nazwy = new Array();
        var laczniki_ilosci = new Array();
        var zakonczenia_nazwy = new Array();
        var zakonczenia_ilosci = new Array();
        var wsporniki_nazwy = new Array();
        var wsporniki_ilosci = new Array();
        var zawieszki_nazwy = new Array();
        var zawieszki_ilosci = new Array();
        var kolory = new Array();

        $('#sohoshoplistapakunkowa .zakonczenie').each(function () {
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_kolor = li_txt.split("(");// rodzielamy po "(", aby mieć osobno kolor
                if(typeof li_txt_kolor[1]  !== "undefined"){
                    var li_txt_kolor = li_txt_kolor[1].split(")");// rodzielamy po "(", aby mieć osobno kolor
                    kolory.push(li_txt_kolor[0]);
                }
            });

        $('.product_name').each(function(){
                var kolor ='';
                var product_name = $(this).html();
                product_name = product_name.split("-");
                for(i=0; i< SohoshopCartProductRenameDictionary.length; i++){
                    product_name[0] = product_name[0].replace(SohoshopCartProductRenameDictionary[i].word,'');
                };
                $(this).html(product_name[0]+' - '+product_name[1]);

                $(this).find('div').each(function(e){
                        //this->product name
                        var e_txt = $(this).text();
                        var e_text_sptd = e_txt.split("(");// rodzielamy po "(", aby mieć osobno kolor
                        if(typeof e_text_sptd[1]  !== "undefined"){
                            $(this).text(e_text_sptd[0]);//this->div
                            var e_text_sptd = e_text_sptd[1].split(")");
                            if(kolory.indexOf(e_text_sptd[0])!=-1){
                                kolor = e_text_sptd[0];
                            }
                        }

                    });

                var niestandardowa_szerokosc = parseInt($(this).find('.awp_mark_548').text());
                if(niestandardowa_szerokosc > 0) {
                    $(this).parent().find('.dlugosc_niestanadardowa').text('('+niestandardowa_szerokosc+')');    //this->product name
                }
                $(this).parent().find('.rura_color').append(' ('+kolor+') ');
                var zaslepka_txt = $(this).parent().find('.zaslepki').text(); 
                if(typeof zaslepka_txt  !== "undefined"){
                	var zaslepka_txt_spltd = zaslepka_txt.split("-");
                	zaslepka_txt_spltd[0] = zaslepka_txt_spltd[0]+' ('+kolor+') '; 
                $(this).parent().find('.zaslepki').text(zaslepka_txt_spltd[0] + '-' + zaslepka_txt_spltd[1]);
				}
            });

        $('#sohoshoplistapakunkowa .zakonczenie').each(function () {
                //grupujemy i sumujemy zakończenia
                var brak_elementu = true;
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_kolor = li_txt.split("(");// rodzielamy po "(", aby mieć osobno kolor
                if(typeof li_txt_kolor[1]  !== "undefined"){
                    var li_txt_kolor = li_txt_kolor[1].split(")");// rodzielamy po "(", aby mieć osobno kolor
                    kolory.push(li_txt_kolor[0]);
                }
                var li_txt_splited = li_txt.split("-");// rodzielamy po "-", aby mieć osobno nazwę i osobno sztuki
                var nazwa_elmentu = li_txt_splited[0];
                var ilosc_elmentu = parseInt(li_txt_splited[1]);
                for (i = 0; i < zakonczenia_nazwy.length; i++) {
                    //sprawdzamy po nazwie czy już mamy w tablicy taki element
                    if (zakonczenia_nazwy[i].replace(/\s/g,'') == nazwa_elmentu.replace(/\s/g,'')) {
                        //jak mamy to sumujemy ilosc
                        zakonczenia_ilosci[i] = zakonczenia_ilosci[i] + ilosc_elmentu;
                        brak_elementu = false;
                        break;
                    }else {
                        //jak mamy to sumujemy ilosc
                        brak_elementu = true;
                    }
                }
                if (brak_elementu) {
                    //jak nie mamy do dodajemy
                    zakonczenia_nazwy.push(nazwa_elmentu);
                    zakonczenia_ilosci.push(ilosc_elmentu);
                }
            });

        $('#sohoshoplistapakunkowa .rura').each(function () {
                var dlugosc_niestandardowa_txt = $(this).find('.dlugosc_niestanadardowa').text();
                var dlugosc_niestandardowa = parseInt(dlugosc_niestandardowa_txt.replace('(',''));
                if ((dlugosc_niestandardowa > 240) && (dlugosc_niestandardowa <= 480)) {
                    dlugosc_niestandardowa = parseFloat(dlugosc_niestandardowa/2).toFixed(2);    
                } else if ((dlugosc_niestandardowa > 480) && (dlugosc_niestandardowa <= 720)) {
                    dlugosc_niestandardowa = parseFloat(dlugosc_niestandardowa/3).toFixed(2);    
                }
                $(this).find('.dlugosc_niestanadardowa').text('('+dlugosc_niestandardowa+')');
                //grupujemy i sumujemy rury
                var brak_elementu = true;
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_splited = li_txt.split("-");// rodzielamy po "-", aby mieć osobno nazwę i osobno sztuki
                var nazwa_elmentu = li_txt_splited[0];
                var ilosc_elmentu = parseInt(li_txt_splited[1]);

                for (i = 0; i < rury_nazwy.length; i++) {
                    if (rury_nazwy[i].replace(/\s/g,'') == nazwa_elmentu.replace(/\s/g,'')) {
                        rury_ilosci[i] = rury_ilosci[i] + ilosc_elmentu;
                        brak_elementu = false;
                        break;
                    } else {
                        brak_elementu = true;//oznaczamy, że nie mamy
                    }
                }
                if (brak_elementu) {
                    //jak nie mamy to dodajemy
                    rury_nazwy.push(nazwa_elmentu);
                    rury_ilosci.push(ilosc_elmentu);
                }
            });

        $('#sohoshoplistapakunkowa .lacznik').each(function () {
                //grupujemy i sumujemy laczniki
                var brak_elementu = true;
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_splited = li_txt.split("-");// rodzielamy po "-", aby mieć osobno nazwę i osobno sztuki
                var nazwa_elmentu = li_txt_splited[0];
                var ilosc_elmentu = parseInt(li_txt_splited[1]);
                for (i = 0; i < laczniki_nazwy.length; i++) {
                    //sprawdzamy po nazwie czy już mamy w tablicy taki element
                    if (laczniki_nazwy[i].replace(/\s/g,'') == nazwa_elmentu.replace(/\s/g,'')) {
                        //jak mamy to sumujemy ilosc
                        laczniki_ilosci[i] = laczniki_ilosci[i] + ilosc_elmentu;
                        brak_elementu = false;
                        break;
                    }else {
                        brak_elementu = true;
                    }
                }
                if (brak_elementu) {
                    //jak nie mamy to dodajemy
                    laczniki_nazwy.push(nazwa_elmentu);
                    laczniki_ilosci.push(ilosc_elmentu);
                }
            });

        $('#sohoshoplistapakunkowa .wspornik').each(function () {
                //grupujemy i sumujemy wsporniki
                var brak_elementu = true;
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_splited = li_txt.split("-");// rodzielamy po "-", aby mieć osobno nazwę i osobno sztuki
                var nazwa_elmentu = li_txt_splited[0];
                var ilosc_elmentu = parseInt(li_txt_splited[1]);
                for (i = 0; i < wsporniki_nazwy.length; i++) {
                    //sprawdzamy po nazwie czy już mamy w tablicy taki element
                    if (wsporniki_nazwy[i].replace(/\s/g,'') == nazwa_elmentu.replace(/\s/g,'')) {
                        //jak mamy to sumujemy ilosc
                        wsporniki_ilosci[i] = wsporniki_ilosci[i] + ilosc_elmentu;
                        brak_elementu = false;
                        break;
                    }else {
                        brak_elementu = true;
                    }
                }
                if (brak_elementu) {
                    //jak nie mamy to dodajemy
                    wsporniki_nazwy.push(nazwa_elmentu);
                    wsporniki_ilosci.push(ilosc_elmentu);
                }
            });

        $('#sohoshoplistapakunkowa .zawieszka').each(function () {
                //grupujemy i sumujemy zawieszki
                var brak_elementu = true;
                var li_txt = $(this).text(); //pobieramy tekst z <li>
                var li_txt_splited = li_txt.split("-");// rodzielamy po "-", aby mieć osobno nazwę i osobno sztuki
                var nazwa_elmentu = li_txt_splited[0];
                var ilosc_elmentu = parseFloat(li_txt_splited[1]);
                for (i = 0; i < zawieszki_nazwy.length; i++) {
                    //sprawdzamy po nazwie czy już mamy w tablicy taki element
                    if (zawieszki_nazwy[i].replace(/\s/g,'') == nazwa_elmentu.replace(/\s/g,'')) {
                        //jak mamy to sumujemy ilosc
                        zawieszki_ilosci[i] = zawieszki_ilosci[i] + ilosc_elmentu;
                        brak_elementu = false;
                        break;
                    }else {
                        brak_elementu = true;
                    }
                }
                if (brak_elementu) {
                    //jak nie mamy to dodajemy
                    zawieszki_nazwy.push(nazwa_elmentu);
                    zawieszki_ilosci.push(ilosc_elmentu);
                }
            });


        if(rury_nazwy.length >0){
            //tworzymy tablice z rurami
            var html = '<h4>ELEMENTY KARNISZY:</h4><h5>RURY</h5><table class="table row-margin-bottom">';
            for (i = 0; i < rury_nazwy.length; i++) {
                html = html+'<tr><td width="75%">'+rury_nazwy[i]+'</td><td>'+rury_ilosci[i]+'</td>';
            }
            html = html +'</table>';
            $('#sohoshoplistapakunkowa-products').parent().append(html);
        }
        if(laczniki_nazwy.length >0){
            //tworzymy tablice z laczikami
            var html = '<h5>ŁĄCZNIKI</h5><table class="table row-margin-bottom">';
            for (i = 0; i < laczniki_nazwy.length; i++) {
                html = html+'<tr><td width="75%">'+laczniki_nazwy[i]+'</td><td>'+laczniki_ilosci[i]+'</td>';
            }
            html = html +'</table>';
            $('#sohoshoplistapakunkowa-products').parent().append(html);
        }
        if(zakonczenia_nazwy.length >0){
            //tworzymy tablice z zakonczeniami
            var html = '<h5>ZAKOŃCZENIA</h5><table class="table row-margin-bottom">';
            for (i = 0; i < zakonczenia_nazwy.length; i++) {
                html = html+'<tr><td width="75%">'+zakonczenia_nazwy[i]+'</td><td>'+zakonczenia_ilosci[i]+'</td>';
            }
            html = html +'</table>';
            $('#sohoshoplistapakunkowa-products').parent().append(html);
        }
        if(wsporniki_nazwy.length >0){
            //tworzymy tablice z wspornikami
            var html = '<h5>WSPORNIKI</h5><table class="table row-margin-bottom">';
            for (i = 0; i < wsporniki_nazwy.length; i++) {
                html = html+'<tr><td width="75%">'+wsporniki_nazwy[i]+'</td><td>'+wsporniki_ilosci[i]+'</td>';
            }
            html = html +'</table>';
            $('#sohoshoplistapakunkowa-products').parent().append(html);
        }
        if(zawieszki_nazwy.length >0){
            //tworzymy tablice z zawieszkami
            var html = '<h5>ZAWIESZKI</h5><table class="table row-margin-bottom">';
            for (i = 0; i < zawieszki_nazwy.length; i++) {
                html = html+'<tr><td width="75%">'+zawieszki_nazwy[i]+'</td><td>'+zawieszki_ilosci[i]+'</td>';
            }
            html = html +'</table>';
            $('#sohoshoplistapakunkowa-products').parent().append(html);
        }


        $('.awp_mark_548').prepend('szerokość niestandardowa - ');


    });

