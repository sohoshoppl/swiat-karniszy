<style>
#sohoshoplistapakunkowa-products .product_name div{
	display: inline;	
}
#sohoshoplistapakunkowa-products .product_name div:nth-of-type(2n-1)
{
	display: none;
}
</style>

<script>
	var SohoshopCartProductRenameDictionary = {$dictionary|json_encode};
</script>
<div class="tab-pane" id="sohoshoplistapakunkowa">
    <h4 class="visible-print">{l s='Lista pakunkowa' mod='sohoshoplistapakunkowa'} </h4>
    <div class="table-responsive">
        <h4>{l s="ZAMÓWIONE PRODUKTY"}</h4>
        <table id="sohoshoplistapakunkowa-products" class="table row-margin-bottom">
            <thead>
            <th>{l s="nazwa"}</th>
            <th>{l s="ilość"}</th>
            </thead>
            <tbody>
                {foreach from=$products item=product}
                <tr>
                    <td width="75%">
                        <div class="product_name">{$product.name}</div>
                        <ul style="display: none;" >
                            {foreach from=$product.atr item=atr}
                                {if $atr.id_group == '5'}
                                    {assign var="dlugosc_karnisza" value=$atr.code} 
                                {elseif  $atr.id_group == '6'}
                                    {assign var="kolor_karnisza" value=$atr.value} 
                                {/if}
                            {/foreach}
                            {if $kolor_karnisza|strstr:"ryflowana"}
                                {assign var="typ_rury" value="ryflowana"} 
                            {elseif $kolor_karnisza|strstr:"skrętna"}
                                {assign var="typ_rury" value="skrętna"} 
                            {else}
                                {assign var="typ_rury" value="gładka"}     
                            {/if}
                            
                        {foreach from=$product.atr item=atr}
                        {*if $atr.id_group == '28'}
								<pre>{$atr|@print_r}</pre>       
								<pre>{$product.name}</pre>                    
                        {/if*}
                        
                            {if $atr.id_group == '5'}{*długości*}
                                    {if $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240' AND $product.typ eq '1'}
                                        <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                        {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480' AND $product.typ eq '1'}
                                        <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                        <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt}</li> 
                                        {elseif $dlugosc_karnisza  gt '480' AND $dlugosc lte '720' AND $product.typ eq '1'}
                                        <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                        <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.1}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                        <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.2}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                        <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*2}</li> 
                                        {elseif $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240' AND $product.typ eq '2'}
                                            {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''}
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2 }</li> 
                                            {else}
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li>
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            {/if}
                                        {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480' AND $product.typ eq '2'}
                                            {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''}
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*4}</li> 
                                            <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*2}</li>   
                                            {else}
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li>
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                            <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt}</li> 
                                            <li class="lacznik">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt}</li> 
                                            {/if}
                                        {elseif $dlugosc_karnisza  gt '480' AND $dlugosc_karnisza lte '720' AND $product.typ eq '2'}
                                            {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''} {*karnisz jest podwójny, ale są te same średnice rur*}
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.1}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.2}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*4}</li>                                                   
                                                {else}
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.1}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.2}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.1}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.2}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li>                                                        
                                                <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*2}</li> 
                                            	<li class="lacznik">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*2}</li>                                                   
                                                {/if}
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.0}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.1}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="rura">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Rura' mod='sohoshoplistapakunkowa'} {$typ_rury} {if $product.kolekcja eq 54}{l s='kwadro' mod='sohoshoplistapakunkowa'}{/if} {$product.dl_rur.2}{l s='cm' mod='sohoshoplistapakunkowa'} <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="lacznik">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {l s='Łącznik wewnętrzny' mod='sohoshoplistapakunkowa'} - {$product.qnt*2}</li>                                                 {/if}    
                                            {elseif $atr.id_group == '8'} {*zakończenia*}   
                                            <li class="zakonczenie">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {$atr.group_name} {$atr.value} - {$product.qnt*2}</li>
                                                {if $product.typ eq '2'}
                                                <li class="zakonczenie zaslepki">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'}{if $product.kolekcja eq 100} {l s='zakończenia kulka' mod='sohoshoplistapakunkowa'}{elseif $product.kolekcja eq 54} {l s='zakończenia saturn' mod='sohoshoplistapakunkowa'}{else} {l s='zakończenia zaślepka' mod='sohoshoplistapakunkowa'}{/if} - {$product.qnt*2}</li>
                                                {/if}
                                            {elseif $atr.id_group == '13'} {*wsporniki*} 
                                            {assign var=wspornik value=$atr.value|replace:$product.srednice_rur.0:''}  
											{assign var=wspornik value=$wspornik|replace:$product.srednice_rur.1:''}
											{assign var=wspornik value=$wspornik|replace:'ø':''} 
											{assign var=wspornik value=$wspornik|replace:'+':''} 
											{assign var=wspornik value=$wspornik|replace:'mm':''}                                              
                                            <li class="wspornik">{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'}{if $product.srednice_rur.1 neq ''}+{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'}{/if}  {$wspornik} - 
                                                {if $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240'}
                                                    {$product.qnt*2}
                                                {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480'}
                                                    {$product.qnt*3}
                                                {elseif $dlugosc_karnisza  gt '480' AND $dlugosc lte '720'}
                                                    {$product.qnt*4}    
                                                {/if}
                                                </li>                                                
                                            {elseif $atr.id_group == '9'} {*zawieszki*}
                                                {if $product.typ eq '1'}
                                                    <li class="zawieszka">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li>
                                                {else}
                                                    {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''} {*karnisz jest podwójny, ale są te same średnice rur*}
                                                        <li class="zawieszka">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt*2)/10}</li>
                                                    {else}
                                                        <li class="zawieszka">{l s='ø'}{$product.srednice_rur.0}{l s='mm' mod='sohoshoplistapakunkowa'} {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li>
                                                        <li class="zawieszka">{l s='ø'}{$product.srednice_rur.1}{l s='mm' mod='sohoshoplistapakunkowa'} {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li> 
                                                    {/if}
                                                 {/if}
                                            {/if}
                                            {/foreach}
                                            </ul>                    
                    </td>
                    <td>{$product.qnt}</td>
                </tr>    
{*<pre>{$product|@print_r}</pre>*}
                {/foreach}
            </tbody>
        </table>
    </div>
        <a class="btn btn-default" href="{$print_link}" target="_blank"><i class="icon-print"></i>{l s='Drukuj listę' mod='sohoshoplistapakunkowa'}</a>
</div>