<!-- todo formularz wysłania paczek do poczekalni gls-->
<section class="bootstrap">   
    <div class="panel">
        <div class="panel-heading q-heading" data-toggle="collapse" data-target="#sohoshopgls-add-package">
            <span class="icon-chevron-down dropdown-icon"></span> 
            {l s="SohoShopGLS"} {*$sohoshopgls_ajax_request*}
            <div class="panel-heading-action"></div> 
        </div>
        <div class="show_more_div collapse" id="sohoshopgls-add-package">
            <div class="row">
                <hr>
                <div class="col-lg-6">
                    {$senderForm}
                </div>
                <div class="col-lg-6">
                    {$reciverForm}
                </div>
            </div>
            <div>
                {$propertiesForm}
            </div>
            <div class="text-center">
                <button type="button" id="addPackageToWaitRoom" class="btn btn-primary">{l s="Dodaj paczkę do poczekalni"}</button>
            </div>
        </div>
    </div>
</section>
<script>
    var id_order = '{$id_order}';
    var sohoshopgls_ajax_request = '{$sohoshopgls_ajax_request}';
    var confirmTXT = '{l s="Czy napewno dodać paczkę do poczekalni"}';
    var okTXT = '{l s="Paczka dodana poprawnie do 'przygotowalni. List przewozowy można pobrać z: "}';
    var przygotowalniaLink = '{$sohoshopgls_przygotowalniaLink}';
</script>            

