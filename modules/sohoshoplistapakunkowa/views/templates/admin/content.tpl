<style>
    h1,h2,h3,h4,h5{
        padding: 0;
        margin: 0;
    }
    h4{
        margin-top: 30px;}
    table{
        margin-left:30px;
        max-width: 600px;
        width: 100%;}
    td,th{
        border: solid 1px black;
    }
    
	#sohoshoplistapakunkowa-products .product_name div{
		display: inline;	
	}
	#sohoshoplistapakunkowa-products .product_name div:nth-of-type(2n-1)
	{
		display: none;
	}  
</style>
<script>
	var SohoshopCartProductRenameDictionary = {$dictionary|json_encode};
</script>
<div class="tab-pane" id="sohoshoplistapakunkowa">
    {*<h2 class="visible-print">Lista pakunkowa </h2>*}
    <div class="table-responsive">
        <h4>ZAMÓWIONE PRODUKTY</h4>
        <table id="sohoshoplistapakunkowa-products" class="table row-margin-bottom">
            <thead>
            <th>nazwa</th>
            <th>ilość</th>
            </thead>
            <tbody>
                {foreach from=$products item=product}
                    <tr>
                        <td width="75%">
                            <div class="product_name">{$product.name}</div>
                            <ul  style="display:none;">
                            {foreach from=$product.atr item=atr}
                                {if $atr.id_group == '5'}
                                    {assign var="dlugosc_karnisza" value=$atr.code} 
                                {elseif  $atr.id_group == '6'}
                                    {assign var="kolor_karnisza" value=$atr.value} 
                                {/if}
                            {/foreach}
                            {if $kolor_karnisza|strstr:"ryflowana"}
                                {assign var="typ_rury" value="ryflowana"} 
                            {elseif $kolor_karnisza|strstr:"skrętna"}
                                {assign var="typ_rury" value="skrętna"} 
                            {else}
                                {assign var="typ_rury" value="gładka"}     
                            {/if}
                            {foreach from=$product.atr item=atr}
                                {if $atr.id_group == '5'}{*długości*}
                                        {if $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240' AND $product.typ eq '1'}
                                            <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480' AND $product.typ eq '1'}
                                            <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                            <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt}</li> 
                                            {elseif $dlugosc_karnisza  gt '480' AND $dlugosc lte '720' AND $product.typ eq '1'}
                                            <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.1}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.2}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                            <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt*2}</li> 
                                            {elseif $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240' AND $product.typ eq '2'}
                                                {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''}
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2 }</li> 
                                                {else}
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li>
                                                <li class="rura">{$product.srednice_rur.1}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                {/if}
                                            {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480' AND $product.typ eq '2'}
                                                {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''}
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*4}</li> 
                                                <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt*2}</li>   
                                                {else}
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li>
                                                <li class="rura">{$product.srednice_rur.1}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt}</li> 
                                                <li class="lacznik">{$product.srednice_rur.1}mm Łącznik wewnętrzny - {$product.qnt}</li> 
                                                {/if}
                                            {elseif $dlugosc_karnisza  gt '480' AND $dlugosc_karnisza lte '720' AND $product.typ eq '2'}
                                                {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''} {*karnisz jest podwójny, ale są te same średnice rur*}
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.1}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.2}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt*2}</li> 
                                                    <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt*4}</li>                                                   
                                                    {else}
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.1}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                    <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.2}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                    <li class="rura">{$product.srednice_rur.1}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                    <li class="rura">{$product.srednice_rur.1}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.1}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                    <li class="rura">{$product.srednice_rur.1}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.2}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li>                                                        
                                                    <li>{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt*2}</li> 
													<li>{$product.srednice_rur.1}mm Łącznik wewnętrzny - {$product.qnt*2}</li>                                                                                                       
                                                    {/if}
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.0}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.1}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="rura">{$product.srednice_rur.0}mm Rura {$typ_rury} {if $product.kolekcja eq 54}kwadro{/if} {$product.dl_rur.2}cm <span class="dlugosc_niestanadardowa"></span><span class="rura_color"></span> - {$product.qnt}</li> 
                                                <li class="lacznik">{$product.srednice_rur.0}mm Łącznik wewnętrzny - {$product.qnt*2}</li>                                                 {/if}    
                                                {elseif $atr.id_group == '8'} {*zakończenia*}   
                                                <li class="zakonczenie">{$product.srednice_rur.0}mm {$atr.group_name} {$atr.value} - {$product.qnt*2}</li>
                                                    {if $product.typ eq '2'}
                                                    <li class="zakonczenie zaslepki">{$product.srednice_rur.1}mm {if $product.kolekcja eq 100}zakończenia kulka{elseif $product.kolekcja eq 54}zakończenia saturn{else}zakończenia zaślepka{/if} - {$product.qnt*2}</li>
                                                    {/if}
                                                {elseif $atr.id_group == '13'} {*wsporniki*} 
		                 							{assign var=wspornik value=$atr.value|replace:$product.srednice_rur.0:''}  
													{assign var=wspornik value=$wspornik|replace:$product.srednice_rur.1:''}
													{assign var=wspornik value=$wspornik|replace:'ø':''} 
													{assign var=wspornik value=$wspornik|replace:'+':''} 
													{assign var=wspornik value=$wspornik|replace:'mm':''}                                                       
                                            <li class="wspornik">{$product.srednice_rur.0}mm{if $product.srednice_rur.1 neq ''}+{$product.srednice_rur.0}mm{/if}  {$wspornik} - 
                                                    {if $dlugosc_karnisza  gt '0' AND $dlugosc_karnisza lte '240'}
                                                        {$product.qnt*2}
                                                    {elseif $dlugosc_karnisza  gt '240' AND $dlugosc_karnisza lte '480'}
                                                        {$product.qnt*3}
                                                    {elseif $dlugosc_karnisza  gt '480' AND $dlugosc lte '720'}
                                                        {$product.qnt*4}    
                                                    {/if}
                                                </li>                                                
                                            {elseif $atr.id_group == '9'} {*zawieszki*}

                                                {if $product.typ eq '1'}
                                                    <li class="zawieszka">{$product.srednice_rur.0}mm {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li> 
                                                    {else}
                                                        {if $product.srednice_rur.0 eq $product.srednice_rur.1 OR $product.srednice_rur.1 eq ''} {*karnisz jest podwójny, ale są te same średnice rur*}
                                                            <li class="zawieszka">{$product.srednice_rur.0}mm {$atr.value} {$atr.group_name} - {($dlugosc_karnisza/10*$product.qnt*2)/10}</li> 
                                                            {else}
                                                            <li class="zawieszka">{$product.srednice_rur.0}mm {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li>
                                                            <li class="zawieszka">{$product.srednice_rur.1}mm {$atr.group_name} {$atr.value} - {($dlugosc_karnisza/10*$product.qnt)/10}</li>                                                             
                                                            {/if}
                                                        {/if}

                                                {/if}
                                                {/foreach}
                                                </ul>                    
                                            </td>
                                            <td>{$product.qnt}</td>
                                        </tr>    
                                        {*<pre>{$product|@print_r}</pre>*}
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {literal}
                                <script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
                                <script type="text/javascript" src="/modules/sohoshoplistapakunkowa/views/js/hook_pakunkowa_order.js"></script>
                                <script>
                                    /*window.onload = function() { window.print(); }*/
                                </script>
                            {/literal}
