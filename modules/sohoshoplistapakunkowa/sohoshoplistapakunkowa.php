<?php

/**
 * SohoshopGLS module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
if (!defined('_PS_VERSION_'))
    exit;

class sohoshoplistapakunkowa extends Module {

    public function __construct() {
        $this->name = 'sohoshoplistapakunkowa';
        $this->tab = 'shipping_logistics';
        $this->version = '0.1';
        $this->author = 'sohoshop.pl';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('SohoSHOP Lista pakunkowa');
        $this->description = $this->l('Lista pakunkowa.');
        $this->tabClassName = 'sohoshoplistapakunkowaprint';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_path = _PS_MODULE_DIR_ . $this->name . '/';

        $this->module_url = 'index.php?controller=AdminModules&token=' . Tools::getValue('token') . '&configure=' . $this->name;
    }

    public function install() {
        if (!parent::install() || !$this->registerHook('displayAdminOrderTabOrder') || !$this->registerHook('displayAdminOrderContentOrder'))
            return false;

        if (!$id_tab) {
            $tab = new Tab();
            $tab->class_name = $this->tabClassName;
            $tab->id_parent = -1;
            $tab->module = $this->name;
            $languages = Language::getLanguages();
            foreach ($languages as $language)
                $tab->name[$language['id_lang']] = $this->displayName;
            $tab->add();
        }        
        return true;
    }

    public function uninstall() {
        if (
                !parent::uninstall() || !$this->unregisterHook('displayAdminOrderTabOrder') || !$this->unregisterHook('displayAdminOrderContentOrder'))
            return false;
        
        $id_tab = Tab::getIdFromClassName($this->tabClassName);
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        return true;
    }

    public function hookDisplayAdminOrderTabOrder($params) {

        return $this->display(__FILE__, 'views/templates/hook/tab.tpl');
    }

    public function hookDisplayAdminOrderContentOrder($params) {
        
        $id_cart = (int) $params['cart']->id;
        $products = array();
        foreach ($params['products'] as $product) {
            $products[$product['id_order_detail']]['kolekcja'] = $this->getKolekcja($product['product_id']);
            $products[$product['id_order_detail']]['qnt'] = $product['product_quantity'];
            //$p_tmp_name = explode('-', $product['product_name']);
            //$products[$product['id_order_detail']]['name'] = $p_tmp_name[0];
            $products[$product['id_order_detail']]['name'] = $product['product_name'];
            $cart_instructions = $this->getCartInstructions($id_cart, $product['product_id'], $product['product_attribute_id']); //pobieramy z koszyka wybrane opcje kreatora
            $products[$product['id_order_detail']]['atr'] = $cart_instructions['atr'];
            $products[$product['id_order_detail']]['dl_rur'] = $cart_instructions['dl_rur'];
            if($cart_instructions['typ'] != 0){
                $products[$product['id_order_detail']]['typ'] = $cart_instructions['typ'];
            }
            if( sizeof($cart_instructions['srednice_rur']) != 0){
                $products[$product['id_order_detail']]['srednice_rur'] = $cart_instructions['srednice_rur'];
            }
        }
        
		$dictionary = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'sohoshopcartproductrename_dictionary`');        
        $this->context->controller->addJS($this->module_path . 'views/js/hook_pakunkowa_order.js');
        $this->smarty->assign('dictionary', $dictionary);
        $this->smarty->assign('products', $products);
        $this->smarty->assign('print_link', $this->context->link->getAdminLink('sohoshoplistapakunkowaprint') . '&id_order=' . $params['order']->id);
        $html = $this->display(__FILE__, 'views/templates/hook/content.tpl');
        return $html;
        
    }

    public function getCartInstructions($id_cart, $id_product, $id_product_attribute) {
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $atr = array();
        $sql = 'SELECT instructions_id FROM ' . _DB_PREFIX_ . 'cart_product
            WHERE id_shop = ' . (int) $this->context->shop->id . ' '
                . 'AND id_cart =' . (int) $id_cart . ' '
                . 'AND id_cart =' . (int) $id_cart . ' '
                . 'AND id_product_attribute =' . (int) $id_product_attribute;
        if ($row = Db::getInstance()->getRow($sql))
            $row_expl = explode(',', (string) $row['instructions_id']);
        foreach ($row_expl as $val) {
            if ($val > 0) {
                $tmp_attr = new Attribute($val);

                $tmp_attr_group = new AttributeGroup($tmp_attr->id_attribute_group);
                //grupa 13 to wspornik, potrzebujemy na podstawie wspornika określić czy mamy karnisz pojedynczy czy podwójny oraz jakie mamy średnice rur
                if ($tmp_attr->id_attribute_group == 13) {
                    $typ = (int)$tmp_attr->code;
                    //pobieranie średnić rur z nazwy wspornika
                    $str = preg_replace("/\+/", " ", $tmp_attr->name[$lang->id]);
                    $str = preg_replace("/[^0-9\s]/", "", $str);
                    $str = trim($str);
                    $srednice_rur = explode(' ',$str);
                }
                if ($tmp_attr->id_attribute_group == 5) { // długość karnisza
                    $wybrana_dlugosc  = (int)$tmp_attr->code;
                    if( ($wybrana_dlugosc)  > 240 && ($wybrana_dlugosc <= 480) ){
                        $dl_rur[0] =  $wybrana_dlugosc/2;
                        $dl_rur[1] = $wybrana_dlugosc/2;
                    }elseif( ($wybrana_dlugosc > 480) && ($wybrana_dlugosc <= 720) ){ // jeżeli długość karnisza jest w tym przedziale, to potrzebujemy wyliczyć długosći poszczególnych rur połączonych łącznikami wewnętrznymi
                        $dl_rur = $this->liczDlugosciRur($wybrana_dlugosc);  
                    }else{
                        $dl_rur[0] = $wybrana_dlugosc;    
                    }
                }
                $atr[] = array('code' => $tmp_attr->code, 'value' => $tmp_attr->name[$lang->id], 'id_group' => $tmp_attr->id_attribute_group, 'id_attr' => $tmp_attr->id, 'group_name' => $tmp_attr_group->name[$lang->id]);
            }
        }
//to do pobierani obiektu attrybutu , trzeba jeszcze poprawić allegro aby zapisywało w koszyku instructions_id
        if (sizeof($atr) > 0) {
            $r['typ'] = $typ;
            $r['srednice_rur'] = $srednice_rur;
            $r['atr'] = $atr;
            $r['dl_rur'] = $dl_rur;
            return $r;
        }
        return null;
    }
    
    
    public function liczDlugosciRur($wybrana_dlugosc) {
        $rozmiary_karnisza = $this-> getRozmiaryKarniszy();//pobieramy możliwe rozmiary rur
        $dlugosc_pierwszych_dwoch_rur = $wybrana_dlugosc / 3;
        $rozmiary_karnisza_index = 0;
        $i = 0;
        sort($rozmiary_karnisza);   
        for ($i = 1; $i < sizeof($rozmiary_karnisza); $i++) {
            if ( ($dlugosc_pierwszych_dwoch_rur >= $rozmiary_karnisza[$i - 1]) && ($dlugosc_pierwszych_dwoch_rur < $rozmiary_karnisza[$i]) ) {
                    $rozmiary_karnisza_index = $i - 1;
                    break;
                }
        }
        $dlugosc_trzeciej_rury = $wybrana_dlugosc - (2 * $rozmiary_karnisza[$rozmiary_karnisza_index]);
        $rury[0] = $dlugosc_pierwszych_dwoch_rur;
        $rury[1] = $dlugosc_pierwszych_dwoch_rur;
        $rury[2] = $dlugosc_pierwszych_dwoch_rur;
        return $rury;
    }
    
    public function getRozmiaryKarniszy() {
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $results = AttributeGroup::getAttributes($lang->id, 5);
        foreach($results as $result){
            $rozmiary = (int)$result['code'];
        }
        return $rozmiary;
    }
    
    public function getKolekcja($id_produkt) {
        $features = Product::getFeaturesStatic($id_produkt);
        foreach ($features as $feature) {
            if ($feature['id_feature'] == 13) {
                return $feature['id_feature_value'];
            }
        }
    }

}

?>