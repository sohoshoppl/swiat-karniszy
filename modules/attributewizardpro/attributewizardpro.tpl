<!-- MODULE Attribute Wizard Pro-->
{if isset($groups)}
{if $awp_psv >= 1.6}
<style type="text/css">
	.awp_nila { 
		margin-top: -4px;}
</style>
{/if}
<script type="text/javascript" src="{$this_wizard_path}js/ajaxupload.js"></script>
<script type="text/javascript">
        {if isset($awp_product_image) && awp_popup}
            var awp_layered_img_id = 'awp_product_image';
        {else} 
            var awp_layered_img_id = 'bigpic';
        {/if}

        awp_out_of_stock = '{$awp_out_of_stock}';
        connectedAttributes = [];{if isset($connectedAttributes) && $connectedAttributes}{function jsadd keypart=''}{foreach $data as $key => $item}{if not $item|@is_array}{if $keypart eq ''}connectedAttributes['{$key}'] = '{$item}';{else}connectedAttributes{$keypart}['{$key}'] = '{$item}';{/if}{else}connectedAttributes{$keypart}['{$key}'] = [];{jsadd data = $item keypart = "`$keypart`['`$key`']" }{/if}{/foreach}{/function}{jsadd data=$connectedAttributes}{/if}

	notConnectedGroups = [];
	{if isset($notConnectedGroups) && $notConnectedGroups}	
            {function jsaddA keypart=''}
                {foreach $data as $key => $item}		
					
                    {if not $item|@is_array}
                        {if $keypart eq ''}
                            notConnectedGroups['{$key}'] = '{$item}'					
                        {else}
                            notConnectedGroups{$keypart}['{$key}'] = '{$item}'					
					
                    	{/if}
                    {else}		
                        notConnectedGroups{$keypart}['{$key}'] = [];
                        {jsaddA data = $item keypart = "`$keypart`['`$key`']" }
                    {/if}
                {/foreach}
            {/function}   
            {jsaddA data=$notConnectedGroups}
		
	{/if}


	defaultConnectedAttribute = [];
	{if isset($defaultConnectedAttribute) && $defaultConnectedAttribute}	
            {function jsaddC keypart=''}
                {foreach $data as $key => $item}		
					
                    {if not $item|@is_array}
                        {if $keypart eq ''}
                            defaultConnectedAttribute['{$key}'] = '{$item}'					
                        {else}
                            defaultConnectedAttribute{$keypart}['{$key}'] = '{$item}'					
					
                        {/if}
                    {else}		
                        defaultConnectedAttribute{$keypart}['{$key}'] = [];
                        {jsaddC data = $item keypart = "`$keypart`['`$key`']" }
                    {/if}
                {/foreach}
            {/function}   
            {jsaddC data=$defaultConnectedAttribute}
		
	{/if}

	bothConnectedAttributes = [];
	{if isset($bothConnectedAttributes) && $bothConnectedAttributes}	
            {function jsaddB keypart=''}
                {foreach $data as $key => $item}		
					
                    {if not $item|@is_array}
                        {if $keypart eq ''}
                            bothConnectedAttributes['{$key}'] = '{$item}'					
                        {else}
                            bothConnectedAttributes{$keypart}['{$key}'] = '{$item}'					

                        {/if}
                    {else}		
                        bothConnectedAttributes{$keypart}['{$key}'] = [];
                        {jsaddB data = $item keypart = "`$keypart`['`$key`']" }
                    {/if}
                {/foreach}
            {/function}   
            {jsaddB data=$bothConnectedAttributes}
		
	{/if}
	var containsSearchAll = "{$containsSearchAll}";

	var awp_not_available = "{l s='---' mod='attributewizardpro' js=1}";


	awp_selected_attribute = "";
	awp_selected_groups_multiple = new Array();
	awp_selected_group = "";
	var awp_converted_price = {$awp_converted_price};
	var awp_tmp_arr = new Array()
	productHasAttributes = false;

	var awp_no_tax_impact = {if $awp_no_tax_impact}true{else}false{/if};
	var awp_psv = "{$awp_psv}";
	var awp_psv3 = "{$awp_psv3}";
	var awp_stock = "{$awp_stock}";
	var awp_allow_oosp = "{$awp_allow_oosp}";
	var awp_reload_page = "{$awp_reload_page}";
	var awp_display_qty = {if $awp_display_qty}true{else}false{/if};
	var awp_is_edit = {$awp_is_edit};
	var awp_qty_edit = {$awp_qty_edit};
	var awp_no_customize = "{$awp_no_customize}";
	var awp_ajax = {if $awp_ajax}true{else}false{/if};

	//$('#availability_statut').css('display','none');
	//$('#availability_statut').attr('id','availability_statut_disabled');
	$('#quantityAvailable').css('display','none');
	$('#quantityAvailableTxt').css('display','none');
	$('#quantityAvailableTxtMultiple').css('display','none');
	$('#last_quantities').css('display','none');
	var awp_customize = "{l s='Customize Product' mod='attributewizardpro' js=1}";
	var awp_add_cart = "{l s='Add to cart' mod='attributewizardpro' js=1}";
	var awp_add = "{l s='Add' mod='attributewizardpro' js=1}";
	var awp_edit = "{l s='Edit' mod='attributewizardpro' js=1}";
	var awp_sub = "{l s='Subtract' mod='attributewizardpro' js=1}";
	var awp_minimal_1 = "{l s='(Min:' mod='attributewizardpro' js=1}";
	var awp_minimal_2 = "{l s=')' mod='attributewizardpro' js=1}";
	var awp_min_qty_text = "{l s='The minimum quantity for this product is' mod='attributewizardpro' js=1}";
	var awp_ext_err = "{l s='Error: invalid file extension, use only ' mod='attributewizardpro' js=1}";
	var awp_adc_no_attribute = {if $awp_adc_no_attribute}true{else}false{/if};
	var awp_popup = {if $awp_popup}true{else}false{/if};
	var awp_pi_display = '{$awp_pi_display}';
	var awp_currency = {$awp_currency.id_currency};
	var awp_is_required = "{l s='is a required field!' mod='attributewizardpro' js=1}";
	var awp_select_attributes = "{l s='You must select at least 1 product option' mod='attributewizardpro' js=1}";
	var awp_must_select_least = "{l s='You must select at least' mod='attributewizardpro' js=1}";
	var awp_must_select_up = "{l s='You must select up to' mod='attributewizardpro' js=1}";
	var awp_must_select = "{l s='You must select' mod='attributewizardpro' js=1}";
	var awp_option = "{l s='option' mod='attributewizardpro' js=1}";
	var awp_options = "{l s='options' mod='attributewizardpro' js=1}";
	var awp_oos_alert = "{l s='This combination is out of stock, please choose another' mod='attributewizardpro' js=1}";

	var awp_file_ext = new Array();
	var awp_file_list = new Array();
	var awp_required_list = new Array();
	var awp_required_group = new Array();
	var awp_required_list_name = new Array();
	var awp_qty_list = new Array();
	var awp_attr_to_group = new Array();
	var awp_selected_groups = new Array();


	var awp_connected_do_not_hide  = new Array();

	var awp_group_impact = new Array();
	var awp_group_order = new Array();
	var awp_group_type = new Array();
	var awp_group_name = new Array();
	var awp_min_qty = new Array();
	var awp_chk_limit = new Array();
	var awp_impact_list = new Array();
	var awp_impact_only_list = new Array();
	var awp_weight_list = new Array();
	var awp_is_quantity_group = new Array();
	var awp_hin = new Array();
	var awp_multiply_list = new Array();
	var awp_layered_image_list = new Array();
	var awp_selected_attribute_default = false;
	if (typeof group_reduction == 'undefined')
	{
		if (typeof groupReduction  == 'undefined')
		group_reduction = 1;
		else
		group_reduction = groupReduction;
	}
	var awp_group_reduction = group_reduction;
	if (parseFloat(awp_psv) >= 1.6)
	awp_group_reduction = awp_group_reduction == 1?group_reduction:1-group_reduction;

	{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
            {strip}
                {if $attributeImpact.price != 0}
                    awp_impact_only_list[{$attributeImpact.id_attribute}] = '{$attributeImpact.price}';
                {/if}
                awp_min_qty[{$attributeImpact.id_attribute}] = '{$attributeImpact.minimal_quantity}';
                awp_impact_list[{$attributeImpact.id_attribute}] = '{$attributeImpact.price}';
                awp_weight_list[{$attributeImpact.id_attribute}] = '{$attributeImpact.weight}';
            {/strip}
        {/foreach}
	if (awp_add_to_cart_display == "bottom")
        {ldelim}
            $("#quantity_wanted_p").attr("id","awp_quantity_wanted_p");
            $("#awp_quantity_wanted_p").css("display","none");
        {rdelim}
        {if $awp_fade}
                $('body').prepend('<div id="awp_background" style="position: absolute; opacity:{$awp_opacity_fraction};filter:alpha(opacity={$awp_opacity});width:100%;height:100%;z-index:19999;display:none;background-color:black"> </div>');
        {/if}
</script>

<div id="awp_container" {if $awp_popup}style="position: absolute; z-index:20000;top:-5000px;display:block;width:{$awp_popup_width}px;margin:auto"{/if}>
	<div class="awp_box container">
		<span id="close_awp_container" title="Zamknij" class="fancybox-close"></span>	
		<b class="xtop"><b class="xb1"></b><b class="xb2 xbtop"></b><b class="xb3 xbtop"></b><b class="xb4 xbtop"></b></b>
		<div class="awp_header">
			<div><b>{l s='Product Options' mod='attributewizardpro'}</b></div>
			{if $awp_popup}
                            <div class="awp_pop_close">
                                    <img src="{$this_wizard_path}img/close.png"  onclick="$('#awp_container').fadeOut(1000);$('#awp_background').fadeOut(1000);awp_customize_func();" />
                            </div>
			{/if}
		</div>
		<div class="awp_content">
			{if isset($awp_product_image) && $awp_popup}
                            <div id="awp_product_image" >
                                    <img src="{$awp_product_image.src}"	title="{$product->name|escape:'htmlall':'UTF-8'}" alt="{$product->name|escape:'htmlall':'UTF-8'}" id="awp_bigpic" width="{$awp_product_image.width}" height="{$awp_product_image.height}" />
                            </div>
			{/if}
			<form name="awp_wizard" id="awp_wizard">
				<input type="hidden" name="awp_p_impact" id="awp_p_impact" value="" />
				<input type="hidden" name="awp_p_weight" id="awp_p_weight" value="" />
				<input type="hidden" name="awp_ins" id="awp_ins" value="{$awp_ins}" />
				<input type="hidden" name="awp_ipa" id="awp_ipa" value="{$awp_ipa}" />
				{if ($awp_add_to_cart == "both" || $awp_add_to_cart == "bottom") && $groups|@count >= $awp_second_add}
                                    <div class="awp_stock_container awp_sct">
					<div class="awp_stock">
						&nbsp;&nbsp;<b class="price our_price_display" id="awp_second_price"></b>
					</div>
					<div class="awp_quantity_additional awp_stock">
						&nbsp;&nbsp;{l s='Quantity' mod='attributewizardpro'}: <input type="text"  id="awp_q1" onkeyup="$('#quantity_wanted').val(this.value);$('#awp_q2').val(this.value);" value="1" />
						<span class="awp_minimal_text"></span>
					</div>
					{if $awp_is_edit}
                                            <div class="awp_stock_btn">
						<input type="button" value="{l s='Edit' mod='attributewizardpro'}" class="exclusive awp_edit" onclick="$(this){if $awp_psv3 < 1.6}.attr('disabled', 'disabled');{else}.prop('disabled', false);{/if};awp_add_to_cart(true);$(this){if $awp_psv3 < 1.6}.attr('disabled', 'disabled');{else}.prop('disabled', false);{/if}('disabled', {if $awp_psv3 == '1.4.9' || $awp_psv3 == '1.4.10' || $awp_psv >= '1.5'}false{else}''{/if});" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>
					{/if}
					<div class="awp_stock_btn box-info-product">
						{if $awp_psv >= 1.6}
						<button type="button" name="Submit" class="exclusive" onclick="$(this).prop('disabled', true);awp_add_to_cart();{if $awp_popup}awp_customize_func();{/if}$(this).prop('disabled', false);">
							<span>{l s='Add to cart' mod='attributewizardpro'}</span>
						</button>
						{else}
						<input type="button" value="{l s='Add to cart' mod='attributewizardpro'}" class="exclusive" onclick="$(this){if $awp_psv3 < 1.6}.attr('disabled', 'disabled');{else}.prop('disabled', false);{/if};awp_add_to_cart();{if $awp_popup}awp_customize_func();{/if}$(this){if $awp_psv3 < 1.6}.attr('disabled', 'disabled');{else}.prop('disabled', false);{/if}('disabled', {if $awp_psv3 == '1.4.9' || $awp_psv3 == '1.4.10' || $awp_psv >= '1.5'}false{else}''{/if});" />
						{/if}
					</div>
					{if $awp_popup}
					<div class="awp_stock_btn">
						<input type="button" value="{l s='Close' mod='attributewizardpro'}" class="button_small" onclick="$('#awp_container').fadeOut(1000);$('#awp_background').fadeOut(1000);awp_customize_func();" />
					</div>
					{/if}
					<div id="awp_in_stock_second"></div>
                                    </div>
				{/if}
				{foreach from=$groups key=id_attribute_group item=group name=awp_groups}
				{strip}
                                    <script type="text/javascript">
					awp_selected_groups[{$group.id_group}] = 0;
					awp_group_type[{$group.id_group}] = '{$group.group_type}';
					awp_group_order[{$group.id_group}] = {$smarty.foreach.awp_groups.index};
					awp_group_name[{$group.id_group}] = '{$group.name|escape:'htmlall':'UTF-8'}';
					awp_hin[{$group.id_group}] = '{if isset($group.group_hide_name)}{$group.group_hide_name}{else}0{/if}';
					awp_required_group[{$group.id_group}] = {if isset($group.group_required) && $group.group_required}1{else}0{/if};
				
					awp_connected_do_not_hide[{$group.id_group}] = {if isset($group.connected_do_not_hide) && $group.connected_do_not_hide}1{else}0{/if};
				
				
					{if $group.group_type == 'checkbox'}
                                            awp_chk_limit[{$group.id_group}] = Array({$group.chk_limit_min}, {$group.chk_limit_max});
					{/if}
					{assign var='default_impact' value=''}
					{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact name=awp_attributeImpact}
                                            {strip}
                                                {if ($group.default|@count > 0 && $attributeImpact.id_attribute|in_array:$group.default) ||
                                                ($awp_pi_display == 'diff' && $attributeImpact.price != 0 && ($group.group_type == 'textbox' || $group.group_type == 'checkbox' || $group.group_type == 'textarea' || $group.group_type == 'file')) || 
                                                ($group.group_type == 'quantity' && $default_impact == '' && $smarty.foreach.awp_attributeImpact.last)}
                                                    {assign var='default_impact' value=$attributeImpact.price}
                                                    $(document).ready(function () {ldelim}
                                                            if ('{$awp_pi_display}' != 'diff' || awp_attr_to_group[{$attributeImpact.id_attribute|intval}] == {$group.id_group})
                                                            {ldelim}
                                                                awp_selected_attribute = {$attributeImpact.id_attribute|intval};
                                                                /*alert (awp_selected_attribute);*/
                                                                awp_selected_group = {$group.id_group|intval};
                                                                $('.awp_box .awp_attribute_selected').each(function()
                                                                    {ldelim}
                                                                        if (($(this).attr('type') != 'radio' || $(this).attr('checked')) &&
                                                                        ($(this).attr('type') != 'checkbox' || $(this).attr('checked')) &&
                                                                        ($(this).attr('type') != 'text' || $(this).val() != "0") && $(this).val() != "")
                                                                        {ldelim}
                                                                            awp_tmp_arr = $(this).attr('name').split('_');
                                                                            if (awp_selected_group == awp_tmp_arr[2])
                                                                                {ldelim}
                                                                                    if (awp_group_type[awp_tmp_arr[2]] != "quantity" && awp_tmp_arr.length == 4 && awp_tmp_arr[3] != awp_selected_attribute)
                                                                                        awp_selected_attribute = awp_tmp_arr[3];
                                                                                    else if (awp_group_type[awp_tmp_arr[2]] != "quantity" && awp_group_type[awp_tmp_arr[2]] != "textbox" &&
                                                                                        awp_group_type[awp_tmp_arr[2]] != "textarea" && awp_group_type[awp_tmp_arr[2]] != "file" &&
                                                                                        awp_group_type[awp_tmp_arr[2]] != "calculation" && awp_selected_attribute != $(this).val())
                                                                                    awp_selected_attribute = $(this).val();
                                                                                {rdelim}
                                                                        {rdelim}
                                                                    {rdelim});
                                                                    awp_select('{$group.id_group|intval}', awp_selected_attribute, {$awp_currency.id_currency}, true);
                                                                    awp_selected_attribute_default = awp_selected_attribute;
                                                            {rdelim};
                                                    {rdelim});
                                                    {assign var='awp_last_select' value='new_awp_select_image();'}
                                                {/if}
                                            {/strip}
					{/foreach}
					{if $default_impact == ''}
                                            {assign var='default_impact' value=0}
					{/if}
					{foreach from=$group.attributes item=group_attribute}
                                            {strip}
                                                {assign var='id_attribute' value=$group_attribute.0}
                                                {if isset($group.group_required) && $group.group_required}
                                                    awp_required_list[{$id_attribute}] = 1;
                                                {else}
                                                    awp_required_list[{$id_attribute}] = 0;
                                                {/if}
                                                {if $awp_layered_image}
                                                    awp_layered_image_list[{$id_attribute}] = '{getLayeredImageTag id_attribute=$id_attribute v=$group_attribute.3}';
                                                {/if}
                                                {if $group.group_type == "file"}
                                                    awp_file_list.push({$id_attribute});
                                                    awp_file_ext.push(/^({$group.group_file_ext})$/);
                                                {/if}
                                                awp_multiply_list[{$id_attribute}] = '{if isset($group.group_calc_multiply)}{$group.group_calc_multiply}{/if}';
                                                awp_qty_list[{$id_attribute}] = '{$group.attributes_quantity.$id_attribute}';
                                                awp_required_list_name[{$id_attribute}] = '{$group_attribute.1|escape:'htmlall':'UTF-8'}';
                                                awp_attr_to_group[{$id_attribute}] = '{$group.id_group}';
                                            {/strip}
					{/foreach}
                                    </script>
				<div class="awp_group_image_container">
					{if isset($group.group_url) && $group.group_url}<a href="{$group.group_url}" target="_blank" alt="{$group.group_url}">{/if}{if isset($group.image_upload)}{getGroupImageTag id_group=$group.id_group alt=$group.name|escape:'htmlall':'UTF-8' v=$group.image_upload}{if isset($group.group_url) && $group.group_url}</a>{/if}{/if}
				</div>
				{if $group.group_type != "hidden"}
        				<div class="awp_box awp_box_inner">
                				<b class="xtop"><b class="xb1"></b><b class="xb2 xbtop"></b><b class="xb3 xbtop"></b><b class="xb4 xbtop"></b></b>
                                		<div class="awp_header">
                                                    {if isset($group.group_header) && $group.group_header}
                                                        {$group.group_header|escape:'htmlspecialchars':'UTF-8'}
                                                    {else}
                                                        {$group.name|escape:'htmlspecialchars':'UTF-8'}
                                                    {/if}
                                                    {if isset($group.group_required) && $group.group_required}&nbsp;&nbsp;&nbsp;<small class="awp_red">* {l s='Required' mod='attributewizardpro'}</small>{/if}

                                                        {if $group.group_type == "checkbox"}
                                                        {if isset($group.chk_limit_min) && $group.chk_limit_min > 0 && $group.chk_limit_max == $group.chk_limit_max}
                                                            <small>({l s='Select' mod='attributewizardpro'} {$group.chk_limit_min} {if $group.chk_limit_min > 1}{l s='options' mod='attributewizardpro'}{else}{l s='option' mod='attributewizardpro'}{/if})</small>
                                                        {elseif isset($group.chk_limit_min) && $group.chk_limit_min > 0 && $group.chk_limit_max > 0}
                                                            <small>({l s='Select' mod='attributewizardpro'} {$group.chk_limit_min} - {$group.chk_limit_max} {l s='options' mod='attributewizardpro'})</small>
                                                        {elseif isset($group.chk_limit_min) && $group.chk_limit_min > 0 && $group.chk_limit_max <= 0}
                                                            <small>({l s='Select at least' mod='attributewizardpro'} {$group.chk_limit_min} {if $group.chk_limit_min > 1}{l s='options' mod='attributewizardpro'}{else}{l s='option' mod='attributewizardpro'}{/if})</small>
                                                        {elseif isset($group.chk_limit_min) && $group.chk_limit_min <= 0 && $group.chk_limit_max > 0}
                                                            <small>({l s='Select up to' mod='attributewizardpro'} {$group.chk_limit_max} {if $group.chk_limit_max > 1}{l s='options' mod='attributewizardpro'}{else}{l s='option' mod='attributewizardpro'}{/if})</small>
                                                        {/if}
                                                    {/if}
                                                    {if isset($group.group_description) && $group.group_description != ""}
                                                        <br />
                                                        <div class="awp_description">
                                                                {$group.group_description|escape:'htmlspecialchars':'UTF-8'}
                                                        </div>
                                                    {/if}
                                                </div>
                                                <div class="awp_content" id="awp_group_{$group.id_group}">
                                                    {if ($group.id_group eq 5) || ($group.id_group eq 18) || ($group.id_group eq 19)}{*rozmiary*}
                                                        <div class="rozmiary">                                                             

                                                    {else}
                                                            <h3>{l s='Wybierz'} {$group.name|escape:'html':'UTF-8'}</h3>
                                                            <p>{l s='Skontaktuj się z nami, jeżeli nie znalazłeś interesującej cię opcji.'}</p>                                                        
                                                        <div>                                                                                                                   
                                                    {/if}
				{/if}
				{if $group.group_type == "dropdown"}
                                                    <div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group} awp_clear">
                                                        {if $group.group_color == 1}
                                                            <div id="awp_select_colors_{$group.id_group}" {if !$group.group_layout}class="awp_left"{/if} >
                                                                {foreach from=$group.attributes item=group_attribute}
                                                                    {strip}
                                                                        {assign var='id_attribute' value=$group_attribute.0}
                                                                        {if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                                            <div id="awp_group_div_{$id_attribute}" class="awp_group_image" >
                                                                                {if !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive"  src="{$img_col_dir}{$id_attribute}.jpg" alt="" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if !$awp_popup}</a>{/if}
                                                                            </div>
                                                                        {else}
                                                                            <div id="awp_group_div_{$id_attribute}" class="awp_group_image" ></div>
                                                                        {/if}
                                                                    {/strip}
                                                                {/foreach}
                                                            </div>
                                                        {/if}
                                                        <div id="awp_sel_cont_{$id_attribute}" class="{if !$group.group_layout}awp_sel_conth{else}awp_sel_contv{/if}">
                                                                <select class="awp_attribute_selected" onmousedown="if($.browser.msie){ldelim}this.style.width='auto'{rdelim}" onblur="this.style.position='';" name="awp_group_{$group.id_group}" id="awp_group_{$group.id_group}" onchange="awp_select('{$group.id_group|intval}', this.options[this.selectedIndex].value, {$awp_currency.id_currency},false);this.style.position='';$('#awp_select_colors_{$group.id_group} div').each(function() {ldelim}$(this).css('display','none'){rdelim});$('#awp_group_div_'+this.value).fadeIn(1000);">
                                                                        {foreach from=$group.attributes item=group_attribute name=awp_dropdown}
                                                                        {strip}
                                                                        {assign var='id_attribute' value=$group_attribute.0}
                                                                        {if $smarty.foreach.awp_dropdown.first && isset($group.group_required) && $group.group_required}
                                                                        <option value="" selected="selected">{l s='Select' mod='attributewizardpro'} {$group.name|escape:'htmlspecialchars':'UTF-8'}</option>
                                                                        {/if}
                                                                        <option value="{$id_attribute}"{if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1}{if $awp_out_of_stock == 'hide'} class="awp_oos"{/if}{if $awp_out_of_stock == 'disable'} disabled="disabled"{/if}{/if} {if $id_attribute|in_array:$group.default && (!isset($group.group_required) || !$group.group_required)}selected="selected"{/if}>{$group_attribute.1|escape:'htmlall':'UTF-8'}
                                                                                {foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
                                                                                {if $id_attribute == $attributeImpact.id_attribute}
                                                                                {math equation="x - y" x=$attributeImpact.price y=$default_impact assign='awp_pi'}
                                                                                &nbsp;
                                                                                {if $awp_pi_display == ""}
                                                                                {elseif $awp_pi > 0}
                                                                                [{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
                                                                                {elseif $awp_pi < 0}
                                                                                [{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
                                                                                {/if}
                                                                                {/if}
                                                                                {/foreach}
                                                                        </option>
                                                                        {/strip}
                                                                        {/foreach}
                                                                </select>
                                                        </div>
                                                        {if !$group.group_layout && $group.group_height}
                                                        <script type="text/javascript">
                                                                $("#awp_sel_cont_{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
                                                        </script>
                                                        {/if}
                                                    </div>
				{elseif $group.group_type == "radio" || $group.group_type == "image"}
                                    {if $group.id_group eq 5}{*rozmiary*}
                                               <div class="jak rte">
                                                    <h4>{l s='Jak dobrać rozmiar karnisza:'}</h4>  
                                                    {assign var=cms_content value=(CMS::getOneCMS(19, $cookie->id_lang))}
                                                    {$cms_content.content}
                                                </div>
                                                <div class="opcje">
                                                    <h4>{l s='Wybierz rozmiar:'}</h4>  
                                                    {foreach from=$group.attributes name=awp_loop item=group_attribute}
                                                    {strip}
                                                    {assign var='id_attribute' value=$group_attribute.0}
                                                    <div id="awp_cell_cont_{$id_attribute}" data-typ_group="{$group_attribute.4}" class="rozmiary_wybor awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if} {*if $group.id_group neq 6 AND $group.id_group neq 5} unvisible {/if*} ">
                                                            <div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1 && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}  {if $group.attributes_quantity.$id_attribute > 0 || $awp_out_of_stock != 'disable' || $awp_allow_oosp == 1}onclick="$('#awp_radio_group_{$id_attribute}').attr('checked',true);try{ldelim}$.uniform.update('.awp_cell_cont_{$group.id_group}, input[type=\'radio\']');{rdelim}catch(err){ldelim}{rdelim};{if $group.group_type == "image"}awp_toggle_img({$group.id_group|intval},{$group_attribute.0|intval});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false){/if}">
                                                                    {if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                                    <div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
                                                                            {if $group.group_type != "image" && !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive lazyload"  data-src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
                                                                    </div>
                                                                    {elseif $group_attribute.2 != ""}
                                                                    <div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
                                                                            &nbsp;
                                                                    </div>
                                                                    {/if}
                                                                    <div id="awp_radio_cell{$id_attribute}" class="{if !$group.group_layout}awp_rrla{else}awp_rrca{/if}{if $group.group_type == "image"} awp_none{/if}">
                                                                            <input type="radio" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable' && $awp_allow_oosp == false}disabled="disabled"{/if} class="awp_attribute_selected awp_clean" id="awp_radio_group_{$id_attribute}" name="awp_group_{$group.id_group}" value="{$group_attribute.0|intval}" {if $id_attribute|in_array:$group.default && (!isset($group.group_required) || !$group.group_required)}checked="checked"{/if} />&nbsp;
                                                                            {if $smarty.foreach.awp_loop.first}
                                                                            <input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
                                                                            {/if}
                                                                    </div>
                                                                    <div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
                                                                            {if isset($group.group_hide_name) && !$group.group_hide_name}
                                                                            <div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}</div>
                                                                            {/if}
                                                                            {foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
                                                                            {if $id_attribute == $attributeImpact.id_attribute}
                                                                            {math equation="x - y" x=$attributeImpact.price y=$default_impact assign='awp_pi'}
                                                                            <div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if} unvisible" id="price_change_{$id_attribute}">
                                                                                    {if $awp_pi_display == ""}
                                                                                    &nbsp;
                                                                                    {elseif $awp_pi > 0}
                                                                                    [{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
                                                                                    {elseif $awp_pi < 0}
                                                                                    [{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
                                                                                    {else}
                                                                                    &nbsp;
                                                                                    {/if}
                                                                            </div>
                                                                            {/if}
                                                                            {/foreach}
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                            $(document).ready(function() {ldelim}
                                                                                    awp_center_images({$group.id_group});
                                                                                    {rdelim});
                                                                            {if !$group.group_layout && $group.group_height}
                                                                            $("#awp_radio_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
                                                                            $("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
                                                                            {/if}
                                                                    </script>
                                                            </div>
                                                    </div>
                                                    {/strip}                    		
                                                    {/foreach}   
                                                    {if $group.id_group eq 5 }
														<div id="div_show_szer_niestand" class="row">
															<div class="col-xs-1">
																<input type="checkbox" class="form-control" name="show_szer_niestand" id="show_szer_niestand">
															</div>
															<label class="col-xs-11" for=show_szer_niestand>{l s='Rozmiar na zamówienie'}</label>
														</div>
														<div id="div_val_szer_niestand" class="unvisible">
															{*<p>{l s='Proszę wpisać szerokość'}</p>*}
															<div class="row flex-center">
																<div class="col-xs-4">
																	<input type="text" id="val_szer_niestand" pattern="{literal}^[0-9]{1,3}${/literal}" class="form-control" />	
																</div>
																<div class="col-xs-1">{l s='cm'}</div>
																<div class="col-xs-4">
																	<button type="button" id="chse_szer_niestand" name="chse_szer_niestand" class="btn btn-default button button-medium">
																		<span>{l s='wybierz'}</span>
																	</button>																
																</div>
															</div>
														</div> 
													{/if}                                                                                                       
                                                </div>
                                    {elseif ($group.id_group eq 18) || ($group.id_group eq 19)}{*wysokosc rolety*}
                                               <div class="jak rte">
                                                    <h4>{l s='Jak dobrać rozmiar rolety:'}</h4>  
                                                    {if $group.id_group eq 18}
														{assign var=cms_content value=(CMS::getOneCMS(21, $cookie->id_lang))}
													{elseif $group.id_group eq 19}
														{assign var=cms_content value=(CMS::getOneCMS(22, $cookie->id_lang))}
													{/if}
                                                    {$cms_content.content}
                                                </div>
                                                <div class="opcje">
                                                    <h4>{l s='Wybierz rozmiar:'}</h4>  
                                                    {foreach from=$group.attributes name=awp_loop item=group_attribute}
                                                    {strip}
                                                    {assign var='id_attribute' value=$group_attribute.0}
                                                    <div id="awp_cell_cont_{$id_attribute}" class="rozmiary_wybor awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if} {*if $group.id_group neq 6 AND $group.id_group neq 5} unvisible {/if*} ">
                                                            <div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1 && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}  {if $group.attributes_quantity.$id_attribute > 0 || $awp_out_of_stock != 'disable' || $awp_allow_oosp == 1}onclick="$('#awp_radio_group_{$id_attribute}').attr('checked',true);try{ldelim}$.uniform.update('.awp_cell_cont_{$group.id_group}, input[type=\'radio\']');{rdelim}catch(err){ldelim}{rdelim};{if $group.group_type == "image"}awp_toggle_img({$group.id_group|intval},{$group_attribute.0|intval});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false){/if}">
                                                                    {if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                                    <div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
                                                                            {if $group.group_type != "image" && !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive lazyload" data-src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
                                                                    </div>
                                                                    {elseif $group_attribute.2 != ""}
                                                                    <div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
                                                                            &nbsp;
                                                                    </div>
                                                                    {/if}
                                                                    <div id="awp_radio_cell{$id_attribute}" class="{if !$group.group_layout}awp_rrla{else}awp_rrca{/if}{if $group.group_type == "image"} awp_none{/if}">
                                                                            <input type="radio" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable' && $awp_allow_oosp == false}disabled="disabled"{/if} class="awp_attribute_selected awp_clean" id="awp_radio_group_{$id_attribute}" name="awp_group_{$group.id_group}" value="{$group_attribute.0|intval}" {if $id_attribute|in_array:$group.default && (!isset($group.group_required) || !$group.group_required)}checked="checked"{/if} />&nbsp;
                                                                            {if $smarty.foreach.awp_loop.first}
                                                                            <input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
                                                                            {/if}
                                                                    </div>
                                                                    <div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
                                                                            {if isset($group.group_hide_name) && !$group.group_hide_name}
                                                                            <div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}</div>
                                                                            {/if}
                                                                            {foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
                                                                            {if $id_attribute == $attributeImpact.id_attribute}
                                                                            {math equation="x - y" x=$attributeImpact.price y=$default_impact assign='awp_pi'}
                                                                            <div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} unvisible" id="price_change_{$id_attribute}">
                                                                                    {if $awp_pi_display == ""}
                                                                                    &nbsp;
                                                                                    {elseif $awp_pi > 0}
                                                                                    [{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
                                                                                    {elseif $awp_pi < 0}
                                                                                    [{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
                                                                                    {else}
                                                                                    &nbsp;
                                                                                    {/if}
                                                                            </div>
                                                                            {/if}
                                                                            {/foreach}
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                            $(document).ready(function() {ldelim}
                                                                                    awp_center_images({$group.id_group});
                                                                                    {rdelim});
                                                                            {if !$group.group_layout && $group.group_height}
                                                                            $("#awp_radio_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
                                                                            $("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
                                                                            {/if}
                                                                    </script>
                                                            </div>
                                                    </div>
                                                    {/strip}                    		
                                                    {/foreach}  
                                                    {if $group.id_group eq 19}
														<div id="div_show_szer_niestand" class="row">
															<div class="col-xs-1">
																<input type="checkbox" class="form-control" name="show_szer_niestand" id="show_szer_niestand">
															</div>
															<label class="col-xs-11" for=show_szer_niestand>{l s='Rozmiar na zamówienie'}</label>
														</div>
														<div id="div_val_szer_niestand" class="unvisible">
															{*<p>{l s='Proszę wpisać szerokość'}</p>*}
															<div class="row flex-center">
																<div class="col-xs-4">
																	<input type="text" id="val_szer_niestand" pattern="{literal}^[0-9]{1,3}${/literal}" class="form-control" />	
																</div>
																<div class="col-xs-1">{l s='cm'}</div>
																<div class="col-xs-4">
																	<button type="button" id="chse_szer_niestand" name="chse_szer_niestand" class="btn btn-default button button-medium">
																		<span>{l s='wybierz'}</span>
																	</button>																
																</div>
															</div>
														</div> 
													{/if}                                             
                                                </div>
                                    {else}
				<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
				<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
				{foreach from=$group.attributes name=awp_loop item=group_attribute}
				{strip}
				{assign var='id_attribute' value=$group_attribute.0}
				<div id="awp_cell_cont_{$id_attribute}" data-color_group="{$group_attribute.4}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if} {*if $group.id_group neq 6 AND $group.id_group neq 5} unvisible {/if*} ">
					<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1 && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}  {if $group.attributes_quantity.$id_attribute > 0 || $awp_out_of_stock != 'disable' || $awp_allow_oosp == 1}onclick="$('#awp_radio_group_{$id_attribute}').attr('checked',true);try{ldelim}$.uniform.update('.awp_cell_cont_{$group.id_group}, input[type=\'radio\']');{rdelim}catch(err){ldelim}{rdelim};{if $group.group_type == "image"}awp_toggle_img({$group.id_group|intval},{$group_attribute.0|intval});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false){/if}">
						{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
						<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
							{if $group.group_type != "image" && !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive lazyload"  data-src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
						</div>
						{elseif $group_attribute.2 != ""}
						<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}">
							&nbsp;
						</div>
						{/if}
						<div id="awp_radio_cell{$id_attribute}" class="{if !$group.group_layout}awp_rrla{else}awp_rrca{/if}{if $group.group_type == "image"} awp_none{/if}">
							<input type="radio" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable' && $awp_allow_oosp == false}disabled="disabled"{/if} class="awp_attribute_selected awp_clean" id="awp_radio_group_{$id_attribute}" name="awp_group_{$group.id_group}" value="{$group_attribute.0|intval}" {if $id_attribute|in_array:$group.default && (!isset($group.group_required) || !$group.group_required)}checked="checked"{/if} />&nbsp;
							{if $smarty.foreach.awp_loop.first}
							<input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
							{/if}
						</div>
						<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
							{if isset($group.group_hide_name) && !$group.group_hide_name}
							<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}</div>
							{/if}
							{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
							{if $id_attribute == $attributeImpact.id_attribute}
							{math equation="x - y" x=$attributeImpact.price y=$default_impact assign='awp_pi'}
							<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} unvisible " id="price_change_{$id_attribute}">
								{if $awp_pi_display == ""}
								&nbsp;
								{elseif $awp_pi > 0}
								[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
								{elseif $awp_pi < 0}
								[{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
								{else}
								&nbsp;
								{/if}
							</div>
							{/if}
							{/foreach}
						</div>
						<script type="text/javascript">
							$(document).ready(function() {ldelim}
								awp_center_images({$group.id_group});
								{rdelim});
							{if !$group.group_layout && $group.group_height}
							$("#awp_radio_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							{/if}
						</script>
					</div>
				</div>
				{/strip}                    		
				{/foreach}
                                {/if}
				{elseif $group.group_type == "textbox"}
				<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
				<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
				{if isset($group.group_hide_name) && !$group.group_hide_name}
				<script type="text/javascript">
					var awp_max_text_length_{$group.id_group} = 0;
				</script>
				{/if}
				{foreach from=$group.attributes name=awp_loop item=group_attribute}
				{strip}
				{assign var='id_attribute' value=$group_attribute.0}
				<div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if}">
				<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}>
					{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
					<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
						{if $group.group_type != "image" && !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive lazyload" data-src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
					</div>
					{elseif $group_attribute.2 != ""}
					<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
						&nbsp;
					</div>
					{/if}
					{if isset($group.group_hide_name) && !$group.group_hide_name}
					<div id="awp_text_length_{$id_attribute}" class="awp_text_length_group awp_text_length_group_{$group.id_group} {if !$group.group_layout}awp_nila{else}awp_nica{/if}" >{$group_attribute.1|escape:'htmlall':'UTF-8'}&nbsp;</div>
					{/if}
					<div id="awp_textbox_cell{$id_attribute}" class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
						<input type="text" value="{if isset($awp_edit_special_values.$id_attribute)}{$awp_edit_special_values.$id_attribute}{/if}"  {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable'}disabled="disabled"{/if} class="awp_attribute_selected awp_group_class_{$group.id_group}" id="awp_textbox_group_{$id_attribute}" name="awp_group_{$group.id_group}_{$id_attribute}" onkeyup="{if $group.group_max_limit > 0}awp_max_limit_check('{$group_attribute.0|intval}',{$group.group_max_limit});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false);" onblur="{if $group.group_max_limit > 0}awp_max_limit_check('{$group_attribute.0|intval}',{$group.group_max_limit});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false);" />&nbsp;
						{if $smarty.foreach.awp_loop.first}
						<input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
						{/if}
					</div>
					<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
						{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
						{strip}
						{if $id_attribute == $attributeImpact.id_attribute}
						{assign var='awp_pi' value=$attributeImpact.price}
						{if $awp_pi_display  != ""}
						<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}">
							{else}
							<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}" ></div><div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
								{/if}
								{if $awp_pi_display == ""}
								&nbsp;
								{elseif $awp_pi > 0}
								[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
								{elseif $awp_pi < 0}
								[{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
								{/if}
							</div>
							{if isset($group.group_required) && $group.group_required}<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} awp_red">* {l s='Required' mod='attributewizardpro'}</div>{/if}
							{if $group.group_max_limit > 0}<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} awp_red">{l s='Characters left:' mod='attributewizardpro'} <span id="awp_max_limit_{$id_attribute}" class="awp_max_limit" awp_limit="{$group.group_max_limit}">{$group.group_max_limit}</span></div>{/if}
							{/if}
							{/strip}
							{/foreach}
						</div>
						{if !$group.group_layout && $group.group_height}
						<script type="text/javascript">
							$("#awp_textbox_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							$("#awp_text_length_{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
						</script>
						{/if}
					</div>
					{if $group.group_max_limit > 0}
					<script type="text/javascript">
						awp_max_limit_check('{$group_attribute.0|intval}',{$group.group_max_limit});
					</script>
					{/if}
				</div>
				{/strip}
				{/foreach}
				{if isset($group.group_hide_name) && !$group.group_hide_name}
				<script type="text/javascript">
					$(document).ready(function (){ldelim}
						{if $awp_popup}
						$('#awp_container').show();
						{/if}
						$('#awp_cell_cont_{$id_attribute} .awp_text_length_group').each(function () {ldelim}
							awp_max_text_length_{$group.id_group} = Math.max(awp_max_text_length_{$group.id_group}, $(this).width());
							{rdelim});
						$('#awp_cell_cont_{$id_attribute} .awp_text_length_group').width(awp_max_text_length_{$group.id_group});
						awp_center_images({$group.id_group});
						{if $awp_popup}
						$('#awp_container').hide();
						{/if}
						{rdelim});
				</script>
				{/if}
				{elseif $group.group_type == "textarea"}
				<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
				<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
				{if isset($group.group_hide_name) && !$group.group_hide_name}
				<script type="text/javascript">
					var awp_max_text_length_{$group.id_group} = 0;
				</script>
				{/if}
				{foreach from=$group.attributes name=awp_loop item=group_attribute}
				{strip}
				{assign var='id_attribute' value=$group_attribute.0}
				<div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if !$group.group_layout} awp_clear{/if}">
				<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}>
					{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
					<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
						{if $group.group_type != "image" && !awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive" src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
					</div>
					{elseif $group_attribute.2 != ""}
					<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
						&nbsp;
					</div>
					{/if}
					{if isset($group.group_hide_name) && !$group.group_hide_name}
					<div id="awp_text_length_{$id_attribute}" class="awp_text_length_group_{$group.id_group} {if !$group.group_layout}awp_nila{else}awp_nica{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}&nbsp;</div>
					<script type="text/javascript">
						awp_max_text_length_{$group.id_group} = Math.max(awp_max_text_length_{$group.id_group}, $('#awp_text_length_{$id_attribute}').width());
					</script>
					{/if}
					<div id="awp_textarea_cell{$id_attribute}" class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
						<textarea {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable'}disabled="disabled"{/if} class="awp_attribute_selected awp_group_class_{$group.id_group}" id="awp_textarea_group_{$id_attribute}" name="awp_group_{$group.id_group}_{$id_attribute}" onkeyup="{if $group.group_max_limit > 0}awp_max_limit_check('{$group_attribute.0|intval}',{$group.group_max_limit});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false);" onblur="{if $group.group_max_limit > 0}awp_max_limit_check('{$group_attribute.0|intval}',{$group.group_max_limit});{/if}awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency}, false);">{if isset($awp_edit_special_values.$id_attribute)}{$awp_edit_special_values.$id_attribute}{/if}</textarea>&nbsp;
						{if $smarty.foreach.awp_loop.first}
						<input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
						{/if}
					</div>
					<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
						{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
						{strip}
						{if $id_attribute == $attributeImpact.id_attribute}
						{assign var='awp_pi' value=$attributeImpact.price}
						{if $awp_pi_display != ""}
						<div id="price_change_{$id_attribute}" class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
							{else}
							<div id="price_change_{$id_attribute}" class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" style="display:none"></div><div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
								{/if}	
								{if $awp_pi_display == ""}
								&nbsp;
								{elseif $awp_pi > 0}
								[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
								{elseif $awp_pi < 0}
								[{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
								{/if}
							</div>
							{if isset($group.group_required) && $group.group_required}<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} awp_red">* {l s='Required' mod='attributewizardpro'}</div>{/if}
							{if $group.group_max_limit > 0}<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} awp_red">{l s='Characters left:' mod='attributewizardpro'} <span id="awp_max_limit_{$id_attribute}" class="awp_max_limit" awp_limit="{$group.group_max_limit}">{$group.group_max_limit}</span></div>{/if}
							{/if}
							{/strip}
							{/foreach}
						</div>
						{if !$group.group_layout && $group.group_height}
						<script type="text/javascript">
							$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							$("#awp_text_length_{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
						</script>
						{/if}
					</div>
				</div>
				{/strip}	
				{/foreach}
				{if isset($group.group_hide_name) && !$group.group_hide_name}
				<script type="text/javascript">
					$('.awp_text_length_group_{$group.id_group}').width(awp_max_text_length_{$group.id_group});
				</script>
				{/if}
				{elseif $group.group_type == "file"}
				<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
				<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
				{foreach from=$group.attributes name=awp_loop item=group_attribute}
				{strip}
				{assign var='id_attribute' value=$group_attribute.0}
				{assign var='id_attribute_file' value=$group_attribute.0|cat:'_file'}
				<div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if}">
					<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}>
						{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
						<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
							{if $group.group_type != "image" && !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  class="img-responsive"  src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if $group.group_type != "image" && !$awp_popup}</a>{/if}
						</div>
						{elseif $group_attribute.2 != ""}
						<div id="awp_tc_{$id_attribute}" class="awp_group_image awp_gi_{$group.id_group}{if !$group.group_layout} awp_left{/if}{if $group.group_type == "image"}{if $id_attribute|in_array:$group.default} awp_image_sel{else} awp_image_nosel{/if}{/if}" >
							&nbsp;
						</div>
						{/if}
						{if isset($group.group_hide_name) && !$group.group_hide_name}
						<div id="awp_text_length_{$id_attribute}" class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
							{$group_attribute.1|escape:'htmlall':'UTF-8'}
						</div>
						{/if}
						<div id="awp_file_cell{$id_attribute}" class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
							<input id="upload_button_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable'}disabled="disabled"{/if} class="{if $awp_psv < 1.6}button{else}btn btn-default button button-small{/if}" style="{if $awp_psv < 1.6}margin:0;padding:0;{else}margin:0;padding:3px 8px;{/if}cursor:pointer" value="{l s='Upload File' mod='attributewizardpro'}" type="button" />
							<input type="hidden"  class="awp_attribute_selected awp_group_class_{$group.id_group}" id="awp_file_group_{$id_attribute}" name="awp_group_{$group.id_group}_{$id_attribute}" {if isset($awp_edit_special_values.$id_attribute_file)}value=""{/if} />&nbsp;
							{if $smarty.foreach.awp_loop.first}
							<input type="hidden" name="pi_default_{$group.id_group}" id="pi_default_{$group.id_group}" value="{$default_impact}" />
							{/if}
						</div>
						<div id="awp_image_cell_{$id_attribute}" class="up_image_clear awp_tbla">
							{if isset($awp_edit_special_values.$id_attribute)}{$awp_edit_special_values.$id_attribute}{/if}
						</div>
						<div id="awp_image_delete_cell_{$id_attribute}" class="up_image_hide awp_tbla" style="display:{if isset($awp_edit_special_values.$id_attribute)}block{else}none{/if}">
							<img src="{$img_dir}icon/delete.gif" style="cursor: pointer" onclick="$('#awp_image_cell_{$id_attribute}').html('');$('#awp_image_delete_cell_{$id_attribute}').css('display','none');$('#awp_file_group_{$id_attribute}').val('');awp_price_update();" /> 
						</div>
						<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
							{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
							{strip}
							{if $id_attribute == $attributeImpact.id_attribute}
							{assign var='awp_pi' value=$attributeImpact.price}
							{if $awp_pi_display != ""}
							<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}">
								{else}
								<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}" ></div><div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
									{/if}
									{if $awp_pi_display == ""}
									&nbsp;
									{elseif $awp_pi > 0}
									[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
									{elseif $awp_pi < 0}
									[{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
									{/if}	
								</div>
								{if isset($group.group_required) && $group.group_required}<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if} awp_red">* {l s='Required' mod='attributewizardpro'}</div>{/if}
								{/if}
								{/strip}
								{/foreach}
							</div>
							{if !$group.group_layout && $group.group_height}
							<script type="text/javascript">
								$("#awp_textbox_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
								$("#awp_file_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
								$("#awp_image_delete_cell_{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
								$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
								$("#awp_text_length_{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
							</script>
							{/if}
						</div>
					</div>
					{/strip}
					{/foreach}
					{elseif $group.group_type == "checkbox"}
					<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
					<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
					{foreach from=$group.attributes name=awp_loop item=group_attribute}
					{strip}
					{assign var='id_attribute' value=$group_attribute.0}
					<div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if}">
						<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'hide'}class="awp_oos"{/if}  onclick="{if $group.group_color == 100}updateColorSelect({$id_attribute}){/if};">
							{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
							<div id="awp_tc_{$id_attribute}" class="awp_group_image{if !$group.group_layout} awp_left{/if}" >
								{if !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}
									<img  src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />
									{if !$awp_popup}</a>{/if}
							</div>
							{elseif $group_attribute.2 != ""}
							<div id="awp_tc_{$id_attribute}" class="awp_group_image{if !$group.group_layout} awp_left{/if}" >
								&nbsp;
							</div>
							{/if}
							<div id="awp_checkbox_cell{$id_attribute}" class="{if !$group.group_layout}awp_tbla{else}awp_tbca awp_checkbox_group{/if}">
								<input type="checkbox" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable'}disabled="disabled"{/if} class="awp_attribute_selected awp_group_class_{$group.id_group} awp_clean" name="awp_group_{$group.id_group}" id="awp_checkbox_group_{$id_attribute}" onclick="awp_select('{$group.id_group|intval}',{$group_attribute.0|intval}, {$awp_currency.id_currency},false);" value="{$group_attribute.0|intval}" {if $group.default|is_array && $id_attribute|in_array:$group.default}checked{/if} />&nbsp;
							</div>
							<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
								{if isset($group.group_hide_name) && !$group.group_hide_name}
								<div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}</div>
								{/if}
								{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
								{if $id_attribute == $attributeImpact.id_attribute}
								{assign var='awp_pi' value=$attributeImpact.price}
								{if $awp_pi_display != ""}
								<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}">
									{else}	
									<span class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}" id="price_change_{$id_attribute}" ></span><div class="{if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">
										{/if}
										{math equation="x * y" x=$awp_pi y=$awp_currency_rate assign=converted}
										{if $awp_pi_display == ""}
										&nbsp;
										{elseif $awp_pi > 0}
										[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$converted currency=$awp_currency}]
										{elseif $awp_pi < 0}
										[{l s='Subtract' mod='attributewizardpro'} {convertPriceWithCurrency price=$converted currency=$awp_currency}]
										{/if}
									</div>
									{/if}	
									{/foreach}
								</div>
								<script type="text/javascript">
									{if !$group.group_layout && $group.group_height}
									$("#awp_checkbox_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
									$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
									{/if}
									$(document).ready(function() {ldelim}
										awp_center_images({$group.id_group});
										{rdelim});
								</script>              						
							</div>
						</div>
						{/strip}
						{/foreach}
						{elseif $group.group_type == "quantity"}
						<script type="text/javascript">
							awp_is_quantity_group.push({$group.id_group});
						</script>
						<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
						<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
						{foreach from=$group.attributes name=awp_loop item=group_attribute}
						{strip}
						{assign var='id_attribute' value=$group_attribute.0}
						<div id="awp_cell_cont_{$id_attribute}" class="awp_cell_cont awp_cell_cont_{$group.id_group}{if $smarty.foreach.awp_loop.iteration % $group.group_per_row == 1 || $group.group_per_row == 1} awp_clear{/if}">
							<div id="awp_cell_{$id_attribute}" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'hide'}class="awp_oos"{/if} >
								{if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
								<div id="awp_tc_{$id_attribute}" class="awp_group_image{if !$group.group_layout} awp_left{/if}" >
									{if !$awp_popup}<a href="{$img_col_dir}{$id_attribute}.jpg" border="0" class="{if $awp_psv < 1.6}thickbox{else}fancybox shown{/if}">{/if}<img  src="{$img_col_dir}{$id_attribute}.jpg" alt="{$group_attribute.1|escape:'htmlall':'UTF-8'}" title="{$group_attribute.1|escape:'htmlall':'UTF-8'}" />{if !$awp_popup}</a>{/if}
								</div>
								{elseif $group_attribute.2 != ""}
								<div id="awp_tc_{$id_attribute}" class="awp_group_image{if !$group.group_layout} awp_left{/if}" >
									&nbsp;
								</div>	
								{/if}
								<div id="awp_quantity_cell{$id_attribute}" class="awp_quantity_cell {if !$group.group_layout}awp_nila{else}awp_nica{/if}">
									{l s='Quantity' mod='attributewizardpro'}: <input type="text" {if $group.attributes_quantity.$id_attribute == 0 && $awp_allow_oosp != 1  && $awp_out_of_stock == 'disable'}disabled="disabled"{/if} class="awp_attribute_selected awp_qty_box" onchange="awp_add_to_cart_func()" alt="awp_group_{$group.id_group}" name="awp_group_{$group.id_group}_{$id_attribute}" id="awp_quantity_group_{$id_attribute}"  value="{if $id_attribute|in_array:$group.default && !$group.group_quantity_zero}1{else}0{/if}" />
								</div>
								<div id="awp_impact_cell{$id_attribute}" class="{if !$group.group_layout}awp_nila{else}awp_nica{/if}">
									{if isset($group.group_hide_name) && !$group.group_hide_name}
									<div class="qty_name_{$id_attribute} {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}">{$group_attribute.1|escape:'htmlall':'UTF-8'}</div>
									{/if}
									{foreach from=$attributeImpacts key=id_attributeImpact item=attributeImpact}
									{strip}
									{if $id_attribute == $attributeImpact.id_attribute}
									{assign var='awp_pi' value=$attributeImpact.price}
									<div class="unvisible {if !$group.group_layout}awp_tbla{else}awp_tbca{/if}"  id="price_change_{$id_attribute}">
										{if $awp_pi_display == ""}
										&nbsp;
										{elseif $awp_pi > 0}
										[{l s='Add' mod='attributewizardpro'} {convertPriceWithCurrency price=$awp_pi currency=$awp_currency}]
										{elseif $awp_pi < 0}
										[{l s='Subtract' mod='attributewizardpro'} {convertPrice price=$awp_pi|abs}]
										{/if}
									</div>
									{/if}
									{/strip}
									{/foreach}
								</div>
								{if !$group.group_layout && $group.group_height}
								<script type="text/javascript">
									$("#awp_quantity_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
									$("#awp_impact_cell{$id_attribute}").css('margin-top',({$group.group_height}/2) - 8);
								</script>
								{/if}
							</div>
						</div>
						{/strip}
						{/foreach}
						{elseif $group.group_type == "hidden"}
						<input type="hidden" id="awp_group_layout_{$group.id_group}" value="{$group.group_layout}" />
						<input type="hidden" id="awp_group_per_row_{$group.id_group}" value="{$group.group_per_row}" />
						{foreach from=$group.attributes name=awp_loop item=group_attribute}
						{strip}
						{assign var='id_attribute' value=$group_attribute.0}
						<input type="text" class="awp_attribute_selected unvisible" name="awp_group_{$group.id_group}_{$id_attribute}" id="awp_quantity_group_{$id_attribute}"  value="1" />
						{/strip}
						{/foreach}

						{/if}						
						{if $group.group_type != "hidden"}
					</div>
                                    </div>
				</div>
				{/if}
				{/strip}
				{/foreach}
				{if $awp_add_to_cart == "both" || $awp_add_to_cart == "bottom"}
				<div class="awp_stock_container awp_sct">
					<div class="awp_stock">
						&nbsp;&nbsp;<b class="price our_price_display" id="awp_price"></b>
					</div>
					<div class="awp_quantity_additional awp_stock">
						&nbsp;&nbsp;{l s='Quantity' mod='attributewizardpro'}: <input type="text" id="awp_q2" onkeyup="$('#quantity_wanted').val(this.value);$('#awp_q2').val(this.value);" value="1" />
						<span class="awp_minimal_text"></span>
					</div>
					{if $awp_is_edit}
					<div class="awp_stock_btn">
						<input type="button" value="{l s='Edit' mod='attributewizardpro'}" class="exclusive awp_edit" onclick="{if ($awp_psv3 < 1.6)}$(this).attr('disabled', 'disabled'){else}$(this).prop('disabled', true){/if};awp_add_to_cart(true);$(this){if ($awp_psv3 < 1.6)}.attr{else}.prop{/if}('disabled', {if $awp_psv3 == '1.4.9' || $awp_psv3 == '1.4.10' || $awp_psv >= '1.5'}false{else}''{/if});" />&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					{/if}	
					<div class="awp_stock_btn box-info-product">
						{if $awp_psv >= 1.6}
						<button type="button" name="Submit" class="exclusive" onclick="$(this).prop('disabled', true);awp_add_to_cart();{if $awp_popup}awp_customize_func();{/if}$(this).prop('disabled', false);">
							<span>{l s='Add to cart' mod='attributewizardpro'}</span>
						</button>
						{else}
						<input type="button" value="{l s='Add to cart' mod='attributewizardpro'}" class="exclusive" onclick="{if ($awp_psv3 < 1.6)}$(this).attr('disabled', 'disabled'){else}$(this).prop('disabled', true){/if};awp_add_to_cart();{if $awp_popup}awp_customize_func();{/if}$(this){if ($awp_psv3 < 1.6)}.attr{else}.prop{/if}('disabled', {if $awp_psv3 == '1.4.9' || $awp_psv3 == '1.4.10' || $awp_psv >= '1.5'}false{else}''{/if});" />
						{/if}
					</div>
					{if $awp_popup}
					<div class="awp_stock_btn">
						<input type="button" value="{l s='Close' mod='attributewizardpro'}" class="button_small" onclick="$('#awp_container').fadeOut(1000);$('#awp_background').fadeOut(1000);awp_customize_func();" />
					</div>
					{/if}
					<div id="awp_in_stock" ></div>
				</div>
				{/if}
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {ldelim}
		if (awp_selected_attribute_default)
		{ldelim}
		{$awp_last_select}
		{rdelim}
		{rdelim});
	/* Javascript used to align certain elements in the wizard */
	{if $awp_popup}
	$('#awp_container').show();
	{/if}
	awp_max_gi = 0;
	$('.awp_gi').each(function () {ldelim}
		if ($(this).width() > awp_max_gi)
		awp_max_gi = $(this).width();
		{rdelim});
	if (awp_max_gi > 0)
	{ldelim}
	$('.awp_box_inner').width($('.awp_content').width() - awp_max_gi - 18);
	$( window ).resize(function() {ldelim}
		$('.awp_box_inner').width($('.awp_content').width() - awp_max_gi - 18);
		{rdelim});

	{rdelim}
	{if $awp_popup}
	$('#awp_container').hide();
	{/if}
	{if $awp_popup}
	//alert($('#page').width());
	$("#awp_container").css('display','none');
	$("#awp_container").css('top','{$awp_popup_top}px');
	$("#awp_container").css('width', ($('#center_column').width() * 0.92)+'px');
	$("#awp_container").css('margin-left', ($('#center_column').width() * 0.05)+'px');
	$("#awp_background").css('height',Math.max($('#page').height(),$(window).height()));
	//alert($(window).width());
	$( window ).resize(function() {ldelim}
		$("#awp_background").css('height',Math.max($('#page').height(),$(window).height()));
		$("#awp_background").css('width',Math.max($('#page').width(), $(window).width()));
		$("#awp_container").css('margin-left', ($('#center_column').width() * 0.05)+'px');
		$("#awp_container").css('width', ($('#center_column').width() * 0.92)+'px');
		{rdelim});
	{/if}
	if (awp_layered_image_list.length > 0)
	$('#view_full_size .span_link').css('display', 'none'); 

</script>
{/if}
<!-- /MODULE AttributeWizardPro -->