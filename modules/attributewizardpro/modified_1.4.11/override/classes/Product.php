<?php

class Product extends ProductCore
{

	/**
	 * Get all available attribute groups
	 *
	 * @param integer $id_lang Language id
	 * @return array Attribute groups
	 *
	 * Removing temporary attributes.
	 */
	public function getAttributesGroups($id_lang)
	{
		$sql = 'SELECT ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` group_name, agl.`public_name` public_group_name, a.`id_attribute`, al.`name` attribute_name,
		a.`color` attribute_color, pa.*
		FROM `'._DB_PREFIX_.'product_attribute` pa
		LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
		LEFT JOIN `'._DB_PREFIX_.'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
		LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
		LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute`)
		LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group`)
		WHERE pa.`id_product` = '.(int)$this->id.' AND al.`id_lang` = '.(int)$id_lang.' AND agl.`id_lang` = '.(int)$id_lang.'
		ORDER BY agl.`public_name`, al.`name`';
		$result = Db::getInstance()->executeS($sql);
		$backtrace = debug_backtrace();
		//print_r($backtrace);
		if (isset($backtrace[0]) && strpos($backtrace[0]['file'], 'ProductController') > 0)
			foreach($result as $key => $val)
				if ($val['group_name'] == 'awp_details')
					unset($result[$key]);	
		return $result;
	}
}

