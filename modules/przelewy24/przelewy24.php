<?php
/**
 * Created by michalz on 13.03.14
 * Modified by AdamM on 28.10.2014
 * Modified by Kzielin on 2015-06
 */
ini_set("display_errors", 0);
if (!defined('E_DEPRECATED')) define('E_DEPRECATED', 8192);
ini_set("error_reporting", E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

if (!defined('_PS_VERSION_'))
    die("Nieznana wersja Prestashop/Unknown Prestashop version.");

require_once(dirname(__FILE__) . '/class_przelewy24.php');
require_once(dirname(__FILE__) . '/classes/Extracharge.php');
require_once(dirname(__FILE__) . '/shared-libraries/autoloader.php');

class Przelewy24 extends PaymentModule
{
    /**
     *
     */
    public $currencies = true;
    public $currencies_mode = 'checkbox';

    public function __construct()
    {
        $this->name = 'przelewy24';
        $this->tab = 'payments_gateways';
        $this->version = '3.3.19';
        $this->is_eu_compatible = 1;
        $this->currencies = true;
        $this->author = 'Przelewy24';
        $this->module_key = "5904b0e92c361acd8b5e76bb45b60b2b";
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->bootstrap = true;
        $this->p24_css = 'https://secure.przelewy24.pl/skrypty/ecommerce_plugin.css.php';
        $this->discount_code_prefix = 'P24_';
        parent::__construct();
        $this->displayName = $this->l('Przelewy24');
        if (_PS_VERSION_ < 1.5) {
            $this->description = 'Wtyczka Przelewy24 w wersji 3 przeznaczona jest wyłącznie dla PrestaShop 1.5 i nowszych. Dla starszych wersji Prestashop użyj wtyczki w wersji 2.x.x (dostępna na naszej stronie internetowej).';
            parent::uninstall();
        } else {
            $this->description = $this->l('Przelewy24.pl Payment Service');
        }
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (Configuration::get('P24_ORDER_STATE_1') === null) {
            Configuration::updateValue('P24_ORDER_STATE_1', 1);
        }
        if (Configuration::get('P24_ORDER_STATE_2') === null) {
            Configuration::updateValue('P24_ORDER_STATE_2', 2);
        }

        if (Configuration::get('P24_INSTALLMENT_SHOW') === null) {
            Configuration::updateValue('P24_INSTALLMENT_SHOW', 0);
        }

        if (Configuration::get("P24_ORDER_TITLE_ID") === null) {
            Configuration::updateValue("P24_ORDER_TITLE_ID", 0);
        }

        if (Configuration::get("P24_SHOW_SUMMARY") === null) {
            Configuration::updateValue("P24_SHOW_SUMMARY", 0);
        }

        if (Configuration::get("P24_DEBUG") === null) {
            Configuration::updateValue("P24_DEBUG", 0);
        }

        if (Configuration::get("P24_VERIFYORDER") === null) {
            Configuration::updateValue("P24_VERIFYORDER", 0);
        }

        if (Configuration::get('P24_CONFIGURED') != 1) {
            $this->warning = $this->l('Module is not configured.');
        }

        if (Configuration::get("P24_WAIT_FOR_RESULT") === null) {
            Configuration::updateValue("P24_WAIT_FOR_RESULT", 0);
        }
    }

    private function validateCredentials($merchant, $pos, $salt, $test)
    {
        Configuration::updateValue('P24_CONFIGURED', 0);
        try {
            $P24C = new Przelewy24Class($merchant, $pos, $salt, $test);
            $ret = $P24C->testConnection();
        } catch (Exception $e) {
            error_log(__METHOD__ . ' ' . $e->getMessage());
        }
        if (!empty($merchant) && !empty($pos) && !empty($salt)) {
            if ($ret['error'] == 0) {
                Configuration::updateValue('P24_CONFIGURED', 1);
                return true;
            }
            return false;
        }
        return null;
    }

    public function getCurrencyCode($currency_id)
    {
        $c = $this->getActualCurrencyById($currency_id);
        return $c['iso_code'];
    }

    public function addExtrachargeToOrder(Cart $cart, Order $order)
    {
        if ($extracharge = Extracharge::getExtrachargeByOrderId((int)$order->id))
            return false;

        $extrachargeAmount = $this->getExtrachargeAmount($cart->id);
        if ($extrachargeAmount) {
            $order->total_paid = $order->total_paid + ($extrachargeAmount / 100);
            $order->total_paid_tax_incl = $order->total_paid_tax_incl + ($extrachargeAmount / 100);
            $order->total_paid_tax_excl = $order->total_paid_tax_excl + ($extrachargeAmount / 100);

            try {
                $order->update();

                $extracharge = new Extracharge();
                $extracharge->id_order = (int)$order->id;
                $extracharge->extracharge_amount = ($extrachargeAmount / 100);
                $extracharge->add();
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
            }
        }
    }

    public function removeExtrachargeFromOrder(Order $order)
    {
        if ($extracharge = Extracharge::getExtrachargeByOrderId((int)$order->id)) {
            $order->total_paid = $order->total_paid - (float)$extracharge->extracharge_amount;
            $order->total_paid_tax_incl = $order->total_paid_tax_incl - (float)$extracharge->extracharge_amount;
            $order->total_paid_tax_excl = $order->total_paid_tax_excl - (float)$extracharge->extracharge_amount;

            try {
                $order->update();
                $extracharge->delete();
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
            }
        }
    }

    public function getP24Amount($currency_id, $amount)
    {
        $c = $this->getActualCurrencyById($currency_id);
        if (isset($c['decimals']) && $c['decimals'] == '0') {
            if (Configuration::get('PS_PRICE_ROUND_MODE') != null) {
                switch (Configuration::get('PS_PRICE_ROUND_MODE')) {
                    case 0:
                        $amount = ceil($amount);
                        break;
                    case 1:
                        $amount = floor($amount);
                        break;
                    case 2:
                        $amount = round($amount);
                        break;
                }
            }
        }

        return number_format($amount, 2, '.', '') * 100;
    }

    private function getActualCurrencyById($id)
    {
        static $currency = null;
        if ($currency === null) {
            $currencies = $this->getCurrency($id);
            foreach ($currencies as $c) {
                if ($c['id_currency'] == $id) {
                    $currency = $c;
                    break;
                }
            }
        }
        return $currency;
    }

    private static function removeWsdlCache()
    {
        $dir = ini_get('soap.wsdl_cache_dir');
        $files = glob($dir . '/wsdl*');
        if ($files) {
            foreach ($files as $file) {
                try {
                    if (is_writable($file)) unlink($file);
                } catch (Exception $e) {
                    Logger::addLog($e->getMessage(), 1);
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $propertyExists = property_exists('Przelewy24', "_html");

        if (Tools::isSubmit('submit')) {
            $this->removeWsdlCache();
            Logger::addLog("Zmieniona konfiguracja", 1);

            foreach (CurrencyCore::getCurrencies() as $currency) {

                $c_sufix = $currency['iso_code'] == 'PLN' ? '' : '_' . $currency['iso_code'];

                Configuration::updateValue('P24_MERCHANT_ID' . $c_sufix, trim(Tools::getValue('merchantID' . $c_sufix)));
                Configuration::updateValue('P24_SHOP_ID' . $c_sufix, trim(Tools::getValue('posID' . $c_sufix)));
                Configuration::updateValue('P24_SALT' . $c_sufix, trim(Tools::getValue('salt' . $c_sufix)));
                Configuration::updateValue('P24_TEST_MODE' . $c_sufix, Tools::getValue('test_mode' . $c_sufix));
                Configuration::updateValue('P24_TEST_MODE_TRANSACTION', Tools::getValue('test_transaction_select'));
                Configuration::updateValue('P24_COMMISSION', 0);
                // zachowanie wstecznej zgodności z wtyczkami 1.4, 1.5
                Configuration::updateValue('P24_ORDER_STATE_1', Tools::getValue('order_state_1'));
                Configuration::updateValue('P24_ORDER_STATE_2', Tools::getValue('order_state_2'));

                Configuration::updateValue('P24_ONECLICK_ENABLED' . $c_sufix, Tools::getValue('oneclick_enabled' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_DEFAULT_REMEMBER_CARD' . $c_sufix, Tools::getValue('default_remember_card' . $c_sufix) != "" ? 1 : 0);

                Configuration::updateValue('P24_API_KEY' . $c_sufix, Tools::getValue('api_key' . $c_sufix));
                Configuration::updateValue('P24_GA_KEY' . $c_sufix, trim(Tools::getValue('ga_key' . $c_sufix)));
                Configuration::updateValue('P24_GA_KEY_TAX' . $c_sufix, Tools::getValue('ga_key_tax' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_BASIC_AUTH' . $c_sufix, Tools::getValue('basic_auth' . $c_sufix));

                // ajax payment
                Configuration::updateValue('P24_PAYINSHOP_ENABLED' . $c_sufix, Tools::getValue('payinshop_enabled' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_ACCEPTINSHOP_ENABLED' . $c_sufix, Tools::getValue('acceptinshop_enabled' . $c_sufix) != "" ? 1 : 0);

                // płatności natychmiastowe
                Configuration::updateValue('P24_PAYSLOW_ENABLED' . $c_sufix, Tools::getValue('payslow_enabled' . $c_sufix) != "" ? 1 : 0);

                // własny limit czasu
                Configuration::updateValue('P24_CUSTOM_TIMELIMIT' . $c_sufix, (int)Tools::getValue('custom_timelimit' . $c_sufix));

                // korzystanie z IVR
                Configuration::updateValue('P24_IVR_ENABLED' . $c_sufix, Tools::getValue('ivr_enabled' . $c_sufix) != "" ? 1 : 0);

                // własny opis na liście bramek
                Configuration::updateValue("P24_CUSTOM_DESCRIPTION" . $c_sufix, Tools::getValue("custom_description" . $c_sufix));

                // wybór metody płatności w sklepie
                Configuration::updateValue('P24_PAYMETHOD_LIST' . $c_sufix, Tools::getValue('payment_method_list' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_PAYMETHOD_GRAPHICS' . $c_sufix, Tools::getValue('graphics_payment_method_list' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_PAYMETHOD_FIRST' . $c_sufix, Tools::getValue('paymethod_first' . $c_sufix));
                Configuration::updateValue('P24_PAYMETHOD_SECOND' . $c_sufix, Tools::getValue('paymethod_second' . $c_sufix));

                // Wyróżnienie wybranych form płatności
                Configuration::updateValue('P24_PAYMETHOD_PROMOTE' . $c_sufix, Tools::getValue('paymethod_promote' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_PAYMETHOD_PROMOTED' . $c_sufix, Tools::getValue('paymethod_promoted' . $c_sufix));

                // Ustawienia wyświetlania
                Configuration::updateValue('P24_GATE_LOGO' . $c_sufix, Tools::getValue('gate_logo' . $c_sufix)); // lista bramek - logo jako tło, albo jako obrazek
                Configuration::updateValue('P24_CHEVRON_RIGHT' . $c_sufix, Tools::getValue('chevron_right' . $c_sufix) != "" ? 1 : 0); // Czy wyświetlać znaczek ">"
                Configuration::updateValue('P24_GATE_CLASS' . $c_sufix, Tools::getValue('gate_class' . $c_sufix)); // klasa css na linku bramki

                // extracharge
                Configuration::updateValue('P24_EXTRACHARGE_ENABLED' . $c_sufix, Tools::getValue('extracharge_enabled' . $c_sufix) != "" ? 1 : 0);
                Configuration::updateValue('P24_EXTRACHARGE_AMOUNT' . $c_sufix, (float)Tools::getValue('increase_amount' . $c_sufix));
                Configuration::updateValue('P24_EXTRACHARGE_PERCENT' . $c_sufix, (float)Tools::getValue('increase_percent' . $c_sufix));

                // extradiscount
                Configuration::updateValue('P24_EXTRADISCOUNT_ENABLED' . $c_sufix, (int)Tools::getValue('extradiscount_enabled' . $c_sufix));
                Configuration::updateValue('P24_EXTRADISCOUNT_AMOUNT' . $c_sufix, (float)Tools::getValue('extradiscount_amount' . $c_sufix));
                Configuration::updateValue('P24_EXTRADISCOUNT_PERCENT' . $c_sufix, (float)Tools::getValue('extradiscount_percent' . $c_sufix));

                Configuration::updateValue('P24_INSTALLMENT_SHOW' . $c_sufix, Tools::getValue('installment_show' . $c_sufix));
                Configuration::updateValue('P24_ORDER_TITLE_ID' . $c_sufix, Tools::getValue('order_title_id' . $c_sufix));
                Configuration::updateValue("P24_SHOW_SUMMARY" . $c_sufix, (Tools::getValue("show_summary" . $c_sufix) != "" ? 1 : 0));
                Configuration::updateValue("P24_DEBUG" . $c_sufix, (Tools::getValue("debug" . $c_sufix) != "" ? 1 : 0));
                Configuration::updateValue("P24_WAIT_FOR_RESULT", (Tools::getValue("wait_for_result") != "" ? 1 : 0));
                Configuration::updateValue("P24_VERIFYORDER" . $c_sufix, Tools::getValue("verify_order_select" . $c_sufix));

                // ZenCard
                $zenCardApi = new ZenCardApi(Configuration::get("P24_MERCHANT_ID" . $c_sufix), Configuration::get("P24_API_KEY" . $c_sufix));
                if ($zenCardApi->isEnabled()) {
                    Configuration::updateValue('P24_MODE_ZENCARD' . $c_sufix, (Tools::getValue('mode_zencard' . $c_sufix) != "" ? 1 : 0));
                } else {
                    Configuration::updateValue('P24_MODE_ZENCARD' . $c_sufix, 0);
                }

                $this->_displayForm(true);
            }

        } else if (!$propertyExists || !$this->_html) {
            $this->_displayForm(false);
        }

        return $this->_html;
    }

    /**
     * @param bool $b_updated
     */
    private function _displayForm($b_updated = false)
    {
        global $cookie;
        $this->_html = '';
        $module_description = html_entity_decode($this->l("You have to be registered to use module Przelewy24. If you have just done it, you can configure module in Payment Settings. However if you still have not registered, go to"));
        $p24_css = $this->p24_css;
        $this->_html .= <<<HTML
        <style>
			@font-face {
				font-family: 'FontAwesome';
				src: url("../modules/przelewy24/css/fontawesome-webfont.eot?v=4.1.0");
				src: url("../modules/przelewy24/css/fontawesome-webfont.eot?#iefix&v=4.1.0") format("embedded-opentype"),
				     url("../modules/przelewy24/css/fontawesome-webfont.woff?v=4.1.0") format("woff"),
				     url("../modules/przelewy24/css/fontawesome-webfont.ttf?v=4.1.0") format("truetype"),
				     url("../modules/przelewy24/css/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg");
				font-weight: normal;
				font-style: normal
			}

			#content form table tr td:first-child { min-width: 250px; text-align: right; padding-right: 10px; }
			#content form table tr { height: 30px; }
			input.short { display:inline-block !important; width: 80% !important; margin-right: 20px; }
			fieldset table {   width: 100%; max-width: 640px !important; }
			fieldset table tr td:first-child { width: 35%; }
			.paylistprom_item { cursor: pointer; display: block !important; background: white; background: rgba(255,255,255,0.8); font-weight: normal !important; text-align: left; }

			.badge-ok, .badge-no { position: absolute; }
			.badge-ok:before,.badge-no:before, .badge-eye { font-family: FontAwesome; font-size: 2em; }
			.badge-ok:before { color: #90cb5c; content: "\\f058"; }
			.badge-no:before { color: #e17875; content: "\\f071"; }
			.badge-eye:before { color: gray; content: "\\f06e"; }

			span.p24info { font-size: 10px; position: relative; top: -1px; }
			span.p24info:before { content: "\\f05a"; font-family: FontAwesome; margin-right: 4px; color: #00aff0; font-size: 1.5em; position: relative; top: 1px;}

			.bank-box { transition: transform 0.2s ease,box-shadow 0.2s ease; }
			.bank-box.ui-sortable-helper { transform: rotate(10deg); box-shadow: 10px 10px 10px lightgray; }
			.bank-box.ui-unrotate-helper { transform: rotate(0deg) !important; box-shadow: 0 0 0 lightgray !important; }
        </style>
		<h2><a target="_blank" href="http://przelewy24.pl"><img src="../modules/przelewy24/img/logo.png" title="Przelewy24" alt="Przelewy24" /></a>&nbsp;{$this->l("Configuration")}</h2>
		<script type="text/javascript">
            $(function() {
                $("#test_mode").change(
                    function() {
                        var o_Test_Transaction=$("#test-transaction");

                        if($(this).val()==1) {
                            o_Test_Transaction.css("visibility","visible");
                        } else {
                            o_Test_Transaction.css("visibility","hidden");
                        }
                    }
                );
            });
       </script>
	<p style="max-width: 830px; text-align: justify;">{$module_description} <a target="_blank" href="http://www.przelewy24.pl/rejestracja">{$this->l("this link")}</a>.</p>
HTML;

        foreach (CurrencyCore::getCurrencies() as $currency) {

            $c_sufix = $currency['iso_code'] == 'PLN' ? '' : '_' . $currency['iso_code'];

            $merchantId[$currency['iso_code']] = Configuration::get('P24_MERCHANT_ID' . $c_sufix);
            $posId[$currency['iso_code']] = Configuration::get('P24_SHOP_ID' . $c_sufix);
            $salt[$currency['iso_code']] = Configuration::get('P24_SALT' . $c_sufix);
            $test[$currency['iso_code']] = Configuration::get('P24_TEST_MODE' . $c_sufix);
            $valid[$currency['iso_code']] =
                $this->validateCredentials($merchantId[$currency['iso_code']], $posId[$currency['iso_code']], $salt[$currency['iso_code']], $test[$currency['iso_code']]);
            $status_class[$currency['iso_code']] = '';

            if (is_bool($valid[$currency['iso_code']])) {
                $status_class[$currency['iso_code']] = ($valid[$currency['iso_code']]) ? 'badge-ok' : 'badge-no';
            }
            if ($b_updated) {
                if ($valid[$currency['iso_code']]) {
                    $this->_html .= <<<HTML
				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
				({$currency['iso_code']})
				{$this->l("Updated")}</div>
HTML;
                } else {
                    $this->_html .= <<<HTML
				<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>
				({$currency['iso_code']})
				{$this->l("Wrong CRC Key for this Merchant/Shop ID and module mode!")}</div>
HTML;
                }
            }
        }

        if (Tools::getValue("send_diagnostics")) {
            $this->_html .= <<<HTML
				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
				{$this->l("Dane diagnostyczne wysłano pomyślnie!")}
				</div>
HTML;
        }

        $current_tab = isset($_POST['tab-hook']) ? $_POST['tab-hook'] : 'PLN';

        $this->_html .= <<<HTML
        <style>
            .hidden {
               display: none;
            }
            .configuration-switch {
                padding-left: 50px;
                padding-bottom: 20px;
            }
            .tab-hook {
                width: 30px;
                height: 30px;
            }
            .tab-hook-description {
                font-size: 15px;
                padding-right: 30px;
            }
        </style>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#{$current_tab}-tab").removeClass('hidden');
                $("#{$current_tab}-hook").attr('checked', 'checked');
            });

            $(document).on('click', '.tab-hook', function() {
               var tab = $(this).attr('data-tab');
               $('.configuration-tab').addClass('hidden');
               $('#'+tab).removeClass('hidden');
               $('.required').attr('required');
            });
        </script>
        <form action="{$_SERVER["REQUEST_URI"]}" method="post">
HTML;
        // PAYMENT CONFIGURATION TAB SWITCHER
        $this->_html .= <<<HTML
        <fieldset>
            <legend>
                <img src="../img/admin/payment.gif" />
                {$this->l('Select currency for which you want to configure your merchant')}
            </legend>
            <div class="configuration-switch">
                <input value="PLN" class="tab-hook" id="PLN-hook" data-tab="PLN-tab" name="tab-hook" type="radio">
                <label class="tab-hook-description" for="PLN-tab"> PLN </label>
HTML;
        foreach (CurrencyCore::getCurrencies() as $currency) {
            if ($currency['iso_code'] != 'PLN') {
                $this->_html .= <<<HTML
		    <input value="{$currency['iso_code']}" class="tab-hook" id="{$currency['iso_code']}-hook" data-tab="{$currency['iso_code']}-tab" name="tab-hook" type="radio">
		    <label class="tab-hook-description" for="{$currency['iso_code']}-tab"> {$currency['iso_code']} </label>
HTML;
            }
        }

        $this->_html .= <<<HTML
        <br />
          <span class="p24info"> <sb> {$this->l('For each currency in your store you need to configure module Przelewy24. Switch between currencies to do it.')}</sb> </span>
            </div>
        </fieldset>
HTML;

        foreach (CurrencyCore::getCurrencies() as $currency) { // FOREACH LANGUAGE CONF START;

            $c_sufix = ($currency['iso_code'] == 'PLN') ? '' : ('_' . $currency['iso_code']);
            $conf_tab = $currency['iso_code'] . '-tab';

            $this->_html .= <<<HTML

        <div id="{$conf_tab}" class="configuration-tab hidden">
            <fieldset>
                <legend><img src="../img/admin/payment.gif" /> {$this->l('Payment Settings')}</legend>
			<p><sup style="color:red">*</sup> {$this->l("mandatory")}</p>

				<table>
					<tr>
						<td><label><img src="../img/admin/tab-customers.gif" />{$this->l("Merchant ID")}<sup style="color:red">*</sup></label></td>
						<td>
							<input id="merchantID{$c_sufix}" type="text" name="merchantID{$c_sufix}" value="{$merchantId[$currency['iso_code']]}" class="short" class="required"/>
							<span class="{$status_class[$currency['iso_code']]}"></span>
						</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="p24info">{$this->l("Where is ID")}</span></td>
					</tr>
HTML;
            $this->_html .= <<<HTML
					<tr>
						<td><label><img src="../img/admin/tab-customers.gif" />{$this->l("Shop ID")}<sup style="color:red">*</sup></label></td>
						<td>
							<input id="posID{$c_sufix}" type="text" name="posID{$c_sufix}" value="{$posId[$currency['iso_code']]}" class="short" class="required"/>
							<span class="{$status_class[$currency['iso_code']]}"></span>
						</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="p24info">{$this->l("Where is ID")}</span></td>
					</tr>
HTML;
            $this->_html .= <<<HTML
					<tr>
						<td><label><img src="../img/admin/tab-customers.gif" />{$this->l("CRC Key")}<sup style="color:red">*</sup></label></td>
						<td>
							<input id="salt{$c_sufix}" type="text" name="salt{$c_sufix}" value="{$salt[$currency['iso_code']]}" class="short" class="required"/>
							<span class="{$status_class[$currency['iso_code']]}"></span>
						</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><span class="p24info">{$this->l("Where is CRC")}</span></td>
					</tr>
HTML;
            $order_title_id_0_selected = ((Configuration::get('P24_ORDER_TITLE_ID') == 0) ? ' selected="selected"' : '');
            $order_title_id_1_selected = ((Configuration::get('P24_ORDER_TITLE_ID') == 1) ? ' selected="selected"' : '');
            $this->_html .= <<<HTML
					<tr>
						<td><label for="order_title_id"{$c_sufix}>{$this->l('Order id in title')}</label></td>
						<td>
							<select id="order_title_id{$c_sufix}" name="order_title_id{$c_sufix}">
								<option value="0"{$order_title_id_0_selected}>{$this->l("Order id in database (e.g. 1, 2, 3)")}</option>
								<option value="1"{$order_title_id_1_selected}>{$this->l("Masked order id (e.g. QYTUVLHOW)")}</option>
							</select>
						</td>
					</tr>
HTML;
            $show_summary_checked = ((Configuration::get('P24_SHOW_SUMMARY') == 1) ? ' checked="checked"' : '');
            $this->_html .= <<< HTML
		            <tr>
		                <td><label for="show_summary{$c_sufix}"><h5 style="display: inline">{$this->l("Show summary on confirmation page")}</h5></label></td>
		                <td><input type="checkbox" name="show_summary{$c_sufix}" id="show_summary{$c_sufix}" {$show_summary_checked} /></td>
		            </tr>
					<tr>
						<td><label>{$this->l('Status before completing payment')}</label></td>
						<td>
							<select name="order_state_1">
HTML;

            foreach (OrderState::getOrderStates($cookie->id_lang) as $orderState) {
                $selected = (Configuration::get('P24_ORDER_STATE_1') == $orderState['id_order_state']) ? ' selected="selected"' : '';
                $this->_html .= <<<HTML
				<option value="{$orderState['id_order_state']}"{$selected}>{$orderState['name']}</option>';
HTML;
            }
            $this->_html .= <<<HTML
							</select>
						</td>
					</tr>
					<tr>
						<td><label>{$this->l('Status after completing payment')}</label></td>
						<td>
							<select name="order_state_2">
HTML;
            foreach (OrderState::getOrderStates($cookie->id_lang) as $orderState) {
                $selected = (Configuration::get('P24_ORDER_STATE_2') == $orderState['id_order_state']) ? ' selected="selected"' : '';
                $this->_html .= <<<HTML
								<option value="{$orderState['id_order_state']}"{$selected}>{$orderState['name']}</option>';
HTML;
            }
            $this->_html .= <<<HTML
							</select>
						</td>
					</tr>
HTML;
            $test_mode_1_selected = (($test[$currency['iso_code']] == 1) ? ' selected="selected"' : '');
            $test_mode_2_selected = (($test[$currency['iso_code']] == 0) ? ' selected="selected"' : '');
            $this->_html .= <<<HTML
					<tr>
						<td><label>{$this->l('Module mode')}</label></td>
						<td>
                            <select name="test_mode{$c_sufix}" id="test_mode{$c_sufix}">
                                    <option value="1"{$test_mode_1_selected}>{$this->l('test')}</option>
                                    <option value="0"{$test_mode_2_selected}>{$this->l('normal')}</option>
                            </select>
                        </td>
					</tr>
HTML;
            $verify_order_2_select = ((Configuration::get('P24_VERIFYORDER' . $c_sufix) == 2) ? ' selected="selected"' : '');
            $verify_order_1_select = ((Configuration::get('P24_VERIFYORDER' . $c_sufix) == 1) ? ' selected="selected"' : '');
            $verify_order_0_select = ((Configuration::get('P24_VERIFYORDER' . $c_sufix) == 0) ? ' selected="selected"' : '');

            $test_transaction_1_select = ((Configuration::get('P24_TEST_MODE_TRANSACTION') == 1) ? ' selected="selected"' : '');
            $test_transaction_0_select = ((Configuration::get('P24_TEST_MODE_TRANSACTION') == 0) ? ' selected="selected"' : '');
            $test_transaction_visible = ((Configuration::get('P24_TEST_MODE') == 1) ? 'visible' : 'hidden');
            $this->_html .= <<<HTML
					<tr>
						<td id="verify-order"{$c_sufix}>
							<label for="verify_order_select"><h5 style="display: inline">{$this->l('Order validation (change cart to order):')}</h5></label>
                        </td>
						<td>
                            <select name="verify_order_select{$c_sufix}" id="verify_order_select{$c_sufix}">
                                    <option value="0"{$verify_order_0_select}>{$this->l('transaction verification')}</option>
                                    <option value="1"{$verify_order_1_select}>{$this->l('order confirmation')}</option>
                                    <option value="2"{$verify_order_2_select}>{$this->l('after click Confirm button')}</option>
                            </select>
                            <span class="p24info"> <sb> {$this->l('If ZenCard is active then always')}: {$this->l('order confirmation')}.</sb> </span>
                        </td>
					</tr>
HTML;
            $wait_for_result_checked = ((Configuration::get('P24_WAIT_FOR_RESULT') == 1) ? ' checked="checked"' : '');
            $this->_html .= <<< HTML
		            <tr>
		                <td><label for="wait_for_result"><h5 style="display: inline">{$this->l("Wait for the result of the transaction")}</h5></label></td>
		                <td>
		                    <input type="checkbox" name="wait_for_result" id="wait_for_result" {$wait_for_result_checked} />
		                    <span class="p24info"> <sb> {$this->l('setting shared between currencies')} </sb> </span>
		                </td>
		            </tr>
HTML;
            $debug_checked = ((Configuration::get('P24_DEBUG') == 1) ? ' checked="checked"' : '');
            $this->_html .= <<< HTML
		            <tr>
		                <td><label for="debug"><h5 style="display: inline">{$this->l("Debug mode")}</h5></label></td>
		                <td>
		                    <input type="checkbox" name="debug" id="debug" {$debug_checked} />
		                    <span class="p24info"> <sb> {$this->l('setting shared between currencies')} </sb> </span>
		                </td>
		            </tr>
				</table>
        </fieldset>
HTML;

            $ga_key = Configuration::get('P24_GA_KEY' . $c_sufix);
            $ga_key_tax_checked = ((Configuration::get('P24_GA_KEY_TAX' . $c_sufix) == 1) ? ' checked="checked"' : '');
            $validateGA = $this->validateGA($ga_key);
            $status_ga_class = '';
            if ($ga_key) {
                $status_ga_class = $validateGA ? 'badge-ok' : 'badge-no';
            }


            $api_key = Configuration::get('P24_API_KEY' . $c_sufix);
            $status_api_class = '';
            $apiTestAccess = false;
            if (!empty($api_key)) {
                $apiTestAccess = $this->apiTestAccess($c_sufix);
                $status_api_class = $apiTestAccess ? 'badge-ok' : 'badge-no';
            }

            $basic_auth = Configuration::get('P24_BASIC_AUTH');
            $this->_html .= <<< HTML
            <fieldset>
                <legend><img src="../img/admin/payment.gif" /> {$this->l('Keys')}</legend>

				<table>
					<tr>
						<td><label for="api_key{$c_sufix}">{$this->l("API Key")}</label></td>
						<td>
							<input type="text" name="api_key{$c_sufix}" id="api_key{$c_sufix}" class="short" value="{$api_key}" />
							<span class="{$status_api_class}"></span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><span class="p24info">{$this->l("Where is API")}</span></td>
					</tr>
					<tr>
						<td><label for="ga_key{$c_sufix}"><h5>{$this->l("GA Key")}</h5></label></td>
						<td>
							<input type="text" name="ga_key{$c_sufix}" id="ga_key{$c_sufix}" class="short" value="{$ga_key}" placeholder="UA-XXXXXXXX-X"/>
							<span class="{$status_ga_class}"></span>
						</td>
					</tr>
					<tr>
						<td><label for="ga_key_tax"><h5>{$this->l("Send to GA with tax")}</h5></label></td>
						<td><input type="checkbox" name="ga_key_tax{$c_sufix}" id="ga_key_tax{$c_sufix}"{$ga_key_tax_checked} /></td>
					</tr>
					<tr>
						<td><label for="basic_auth{$c_sufix}"><h5>{$this->l("Basic auth")}</h5></label></td>
						<td>
							<span class="badge-eye" onclick="$('#basic_auth').css('visibility','visible'); $(this).hide();" style="cursor:pointer"></span>
							<input type="text" name="basic_auth{$c_sufix}" id="basic_auth{$c_sufix}" class="short" value="{$basic_auth}" placeholder="login:password" style="visibility:hidden" />
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<span class="p24info">{$this->l("Fill in this field if Your shop is protected with basic-auth mechanism")}</span>
						</td>
					</tr>

				</table>
        </fieldset>
HTML;
            if ($currency['iso_code'] == 'PLN' && $apiTestAccess && $this->ccCheckRecurrency($c_sufix)) {
                $oneclick_enabled_checked = ((Configuration::get('P24_ONECLICK_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $payinshop_enabled_checked = ((Configuration::get('P24_PAYINSHOP_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $default_remember_card_checked = ((Configuration::get('P24_DEFAULT_REMEMBER_CARD' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $this->_html .= <<<HTML
			<fieldset>
				<legend><img src="../img/admin/payment.gif" /> {$this->l("Oneclick payments")}</legend>
				<table>
					<tr>
						<td><label for="oneclick_enabled{$c_sufix}"><h5>{$this->l("Oneclick payments")}</h5></label></td>
						<td>
						    <input type="checkbox" name="oneclick_enabled{$c_sufix}" id="oneclick_enabled{$c_sufix}"{$oneclick_enabled_checked} />
						    <span class="p24info">{$this->l("Allows you to order products with on-click")}</span>
						 </td>
					</tr>
HTML;
                if ($oneclick_enabled_checked):
                    $this->_html .= <<<HTML
						<tr>
						<td><label for="default_remember_card{$c_sufix}"><h5>{$this->l("Default memorize the card?")}</h5></label></td>
						<td>
							<input type="checkbox" name="default_remember_card{$c_sufix}" id="default_remember_card{$c_sufix}"{$default_remember_card_checked} /> 
							<span class="p24info">{$this->l('Default option "Memorize payment cards, which I pay" on the confirmation page is selected?')}</span>
						</td>
					</tr>
HTML;
                endif;
                $this->_html .= <<<HTML
					<tr>
						<td><label for="payinshop_enabled{$c_sufix}"><h5>{$this->l("Pay in shop")}</h5></label></td>
						<td>
							<input type="checkbox" name="payinshop_enabled{$c_sufix}" id="payinshop_enabled{$c_sufix}"{$payinshop_enabled_checked} />
							<span class="p24info">{$this->l("Pay in shop description")}</span>
						</td>
					</tr>
				</table>
			</fieldset>
HTML;
            }
            $acceptinshop_enabled_checked = ((Configuration::get('P24_ACCEPTINSHOP_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');
            $payslow_enabled_checked = ((Configuration::get('P24_PAYSLOW_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');
            $custom_timelimit = Configuration::get('P24_CUSTOM_TIMELIMIT' . $c_sufix);
            if ($custom_timelimit === '') $custom_timelimit = 15;

            $ivr_enabled_checked = ((Configuration::get('P24_IVR_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');

            $installment_2_show = ((Configuration::get('P24_INSTALLMENT_SHOW' . $c_sufix) == 2) ? ' selected="selected"' : '');
            $installment_1_show = ((Configuration::get('P24_INSTALLMENT_SHOW' . $c_sufix) == 1) ? ' selected="selected"' : '');
            $installment_0_show = ((Configuration::get('P24_INSTALLMENT_SHOW' . $c_sufix) == 0) ? ' selected="selected"' : '');
            $this->_html .= <<<HTML
        <fieldset>
        	<legend><img src="../img/admin/payment.gif" /> {$this->l("Payment Settings")}</legend>
        	<table>
HTML;

            if ($currency['iso_code'] == 'PLN') {
                $this->_html .= <<<HTML

        		<tr>
        			<td><label for="acceptinshop_enabled{$c_sufix}"><h5>{$this->l("Show accept button in shop")}</h5></label></td>
        			<td>
						<input type="checkbox" name="acceptinshop_enabled{$c_sufix}" id="acceptinshop_enabled{$c_sufix}"{$acceptinshop_enabled_checked} />
						<span class="p24info">{$this->l("Show accept button in shop description")}</span>
					</td>
        		</tr>
                <tr>
                    <td><label for="installment_show"><h5 style="display:inline">{$this->l('Installment Settings')}</h5></label></td>
                    <td>
                        <select name="installment_show{$c_sufix}" id="installment_show{$c_sufix}">
                            <option value="2"{$installment_2_show}>{$this->l('button (payment page) and information (product page)')}</option>
                            <option value="1"{$installment_1_show}>{$this->l('button (payment page) only')}</option>
                            <option value="0"{$installment_0_show}>{$this->l('hide all')}</option>
                        </select>
                    </td>
                </tr>
        		<tr>
        			<td><label for="payslow_enabled"><h5>{$this->l("Allow also for not immediate payments")}</h5></label></td>
        			<td>
						<input type="checkbox" name="payslow_enabled{$c_sufix}" id="payslow_enabled"{$payslow_enabled_checked} />
						<span class="p24info">{$this->l("This setting increases the number of available payment methods")}</span>
					</td>
        		</tr>
        		<tr>
        			<td><h5>{$this->l("Adjust the time limit for completion of the transaction")}</h5></td>
        			<td>
						<div class="input-group col-lg-2">
						<input type="number" name="custom_timelimit{$c_sufix}" id="custom_timelimit{$c_sufix}" min="0" max="99" style="width: 70px;" value="{$custom_timelimit}" class="form-control"/>
						<span class="input-group-addon">min</span>
						</div>
					</td>
        		</tr>
        		<tr>
					<td></td>
					<td><span class="p24info">{$this->l("Value 0 means no time limit")}</span></td>
        		</tr>
HTML;
                if ($apiTestAccess && $this->checkIvrPresent($c_sufix)) {
                    $this->_html .= <<<HTML
        		<tr>
        			<td><h5>{$this->l("Show IVR button at Order Details page")}</h5></td>
        			<td>
						<input type="checkbox" name="ivr_enabled{$c_sufix}" id="payslow_enabled{$c_sufix}"{$ivr_enabled_checked} />
						<span class="p24info">{$this->l("This button allows to call back to the Customer")}</span>
					</td>
        		</tr>
HTML;
                }
            }

            $custom_description = Configuration::get('P24_CUSTOM_DESCRIPTION' . $c_sufix);
            $this->_html .= <<<HTML
        		<tr>
        			<td><h5>{$this->l("Custom module description")}</h5></td>
        			<td>
						<input type="text" name="custom_description{$c_sufix}" id="custom_description{$c_sufix}" value="{$custom_description}" />
					</td>
        		</tr>
        	</table>
        </fieldset>
HTML;

            if ($apiTestAccess) {
                $payment_method_list_checked = '';
                $paymethod_style = 'display:none;';
                if (Configuration::get('P24_PAYMETHOD_LIST' . $c_sufix) == 1) {
                    $payment_method_list_checked = ' checked="checked"';
                    $paymethod_style = '';
                }
                $graphics_payment_method_list_checked = ((Configuration::get('P24_PAYMETHOD_GRAPHICS' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $payment_list_res = $this->availablePaymentMethods(false, $currency['iso_code']);
                $payment_list = array();
                if (is_array($payment_list_res)) {
                    foreach ($payment_list_res as $item) {
                        $payment_list[$item->id] = $item;
                    }
                }

                $html_selected = '';
                $html_available = '';

                $P24_PAYMETHOD_FIRST = explode(',', Configuration::get('P24_PAYMETHOD_FIRST' . $c_sufix));
                $P24_PAYMETHOD_SECOND = explode(',', Configuration::get('P24_PAYMETHOD_SECOND' . $c_sufix));

                if (!function_exists('getBankHtml')) {
                    function getBankHtml($bank)
                    {
                        return '<a class="bank-box" data-id="' . $bank->id . '"><div class="bank-logo bank-logo-' . $bank->id . '"></div><div class="bank-name">' . $bank->name . '</div></a>';
                    }
                }

                foreach ($P24_PAYMETHOD_FIRST as $id) {
                    if (isset($payment_list[$id])) {
                        $html_selected .= getBankHtml($payment_list[$id]);
                        unset($payment_list[$id]);
                    }
                }

                foreach ($P24_PAYMETHOD_SECOND as $id) {
                    if (isset($payment_list[$id])) {
                        $html_available .= getBankHtml($payment_list[$id]);
                        unset($payment_list[$id]);
                    }
                }

                foreach ($payment_list as $id => $bank) {
                    $html_available .= getBankHtml($bank);
                }

                $maxNo = 5;
                $drop_here_translation = sprintf($this->l('Drop here max. %s payment methods'), $maxNo);

                if ($currency['iso_code'] == 'PLN') {
                    $this->_html .= <<<HTML
		<style>
			.paymethod .selected {
				width: 730px;
				border: 5px dashed lightgray;
				height: 101px;
				padding: 0.5em;
				overflow: hidden;
			}
			.bank-placeholder { opacity: 0.6; }
			.sortable.available .bank-box:last-child { clear: both; }
  		</style>
        <fieldset>
        	<legend><img src="../img/admin/payment.gif" /> {$this->l("Payment methods")}</legend>
        	<table>
        		<tr>
        			<td><label for="payment_method_list{$c_sufix}"><h5>{$this->l("Show available payment methods in shop")}</h5></label></td>
        			<td>
						<input type="checkbox" name="payment_method_list{$c_sufix}" id="payment_method_list{$c_sufix}"{$payment_method_list_checked} />
						<span class="p24info">{$this->l("Customer can chose payment method on confirmation page")}</span>
					</td>
        		</tr>
            </table>
            <input type="hidden" name="paymethod_first{$c_sufix}"  id="paymethod_first{$c_sufix}">
            <input type="hidden" name="paymethod_second{$c_sufix}" id="paymethod_second{$c_sufix}">

			<div class="paymethod" style="{$paymethod_style}">

				<table>
					<tr>
						<td><label for="graphics_payment_method_list{$c_sufix}"><h5>{$this->l("Use graphics list of payment methods")}</h5></label></td>
						<td>
							<input type="checkbox" name="graphics_payment_method_list{$c_sufix}" id="graphics_payment_method_list{$c_sufix}"{$graphics_payment_method_list_checked} />
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="button" onClick="setAutoOrder()">{$this->l("Set the payment methods order of the most used")}</button>
						</td>
					</tr>
				</table>

				<div style="margin-left: 2em;">
					<p style="margin: 1em 0 0 0;">{$this->l("Payment methods visible right away:")} ({$drop_here_translation})</p>
					<div class="sortable selected" style="float: left" data-max="{$maxNo}">{$html_selected}</div>
					<img src="../modules/przelewy24/img/arrow-left-icon.png" style="display: inline-block; opacity: 0.3;position: relative; top: 30px; left: 7px;">
					<p style="margin: 1em 0 0 0; clear: both;">{$this->l("Payment methods visible on more-button:")}</p>
					<div class="sortable available">{$html_available} <div style="clear:both" id="clear"></div></div>
					<div class="tempHolder"></div>
				</div>
			</div>
        </fieldset>
		<script>
			function setAutoOrder() {
				$.ajax('{$this->context->link->getModuleLink('przelewy24', 'paymentAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1)}', {
					method: 'POST', type: 'POST',
					data : { action: 'setOrder' },
					success : function(data) {
						$('.paymethod .selected a.bank-box,.paymethod .available a.bank-box').appendTo('.paymethod .tempHolder');
						var order = JSON.parse(data);
						var counter = 0;
						for (var i in order) {
							counter++;
							if (counter < 4) {
								$('.paymethod .tempHolder a.bank-box[data-id=' + order[i] + ']').appendTo('.paymethod .selected');
							} else {
								$('.paymethod .tempHolder a.bank-box[data-id=' + order[i] + ']').appendTo('.paymethod .available');
							}
						};
						$('.paymethod .tempHolder a.bank-box').appendTo('.paymethod .available');
						$('#clear').appendTo('.paymethod .available');
						$('.paymethod .selected,.paymethod .available')
							.css({ backgroundColor : "#FEFCD7" })
							.animate({ backgroundColor : "#fff" }, 2000)
						;
						updatePaymethods();
					},
				});
			}
			function updatePaymethods() {
				$('.bank-box.ui-unrotate-helper').removeClass('ui-unrotate-helper');
				var maxNo = parseInt($('.paymethod .selected').attr('data-max'));
				if (maxNo > 0) {
					if ($('.paymethod .selected a[data-id]').length > maxNo) {
						var i = 0;
						$('.paymethod .selected a[data-id]').each(function(){
							i++;
							if (i > maxNo) {
								$(this).prependTo($('.paymethod .available'));
								$('#clear').appendTo('.paymethod .available');
							}
						});
					}
				}

				$('#paymethod_first').val('');
				$('.paymethod .selected a[data-id]').each(function(){
					$('#paymethod_first').val($('#paymethod_first').val() + ($('#paymethod_first').val().length ?',':'') + $(this).attr('data-id'));
				});

				$('#paymethod_second').val('');
				$('.paymethod .available a[data-id]').each(function(){
					$('#paymethod_second').val($('#paymethod_second').val() + ($('#paymethod_second').val().length ?',':'') + $(this).attr('data-id'));
				});
			}
			$(document).ready(function(){
				updatePaymethods();
				$('#payment_method_list').change(function(){
					if ($(this).is(":checked")) {
						$('.paymethod:not(:visible)').slideDown(400, function(){ $('body').animate({ scrollTop: $(window).scrollTop() + $('.paymethod').height() }) });
					} else {
						$('.paymethod:visible').slideUp();
					}
				});
			});
		</script>
HTML;
                }

                $promote_enabled_checked = ((Configuration::get('P24_PAYMETHOD_PROMOTE' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $paymethod_promoted = Configuration::get('P24_PAYMETHOD_PROMOTED' . $c_sufix);
                $paymethod_promoted_ar = explode(',', $paymethod_promoted);
                $paymethod_promote_list_style = 'display:none';
                if (Configuration::get('P24_PAYMETHOD_PROMOTE' . $c_sufix) == 1) {
                    $paymethod_promote_list_style = '';
                }

                $paymentListToPromote = array();
                if (is_array($payment_list_res)) {
                    foreach ($payment_list_res as $item) {
                        if (!in_array($item->id, array(142, 2000))) { // bez kart i rat, bez przedpłaty
                            $item->checked = '';
                            if (in_array($item->id, $paymethod_promoted_ar)) {
                                $item->checked = ' checked="checked"';
                            }
                            $paymentListToPromote[] = $item;
                        }
                    }
                }

                $payment_list_promote = '';
                foreach ($paymentListToPromote as $item) {
                    $payment_list_promote .= "<label class=\"paylistprom_item paylistprom_item_{$item->id}\"
				for=\"paylistprom_{$item->id}\"><span
				style=\"cursor: ns-resize; display: inline-block\" class=\"ui-icon ui-icon-grip-dotted-vertical\"></span><input
				class=\"paylistprom\" id=\"paylistprom_{$item->id}\" type=\"checkbox\" data-val=\"{$item->id}\" {$item->checked}> <span
				style=\"position:relative; top: -2px\">{$item->name}</span></label>";
                }

                $gate_logo_0_selected = ((Configuration::get('P24_GATE_LOGO' . $c_sufix) == 0) ? ' selected="selected"' : '');
                $gate_logo_1_selected = ((Configuration::get('P24_GATE_LOGO' . $c_sufix) == 1) ? ' selected="selected"' : '');

                $show_chevron_right_checked = ((Configuration::get('P24_CHEVRON_RIGHT' . $c_sufix) == 1) ? ' checked="checked"' : '');
                $gate_class_value = Configuration::get('P24_GATE_CLASS' . $c_sufix);

                $this->_html .= <<<HTML
        <fieldset>
        	<legend><img src="../img/admin/payment.gif" /> {$this->l("Payment gateways list")}</legend>
        	<table>
        		<tr>
        			<td><label for="paymethod_promote"><h5>{$this->l("How to display logo on a list of payment gateways")}</h5></label></td>
        			<td>
						<select name="gate_logo"{$c_sufix}>
							<option value="0"{$gate_logo_0_selected}>{$this->l("Display as background (default)")}</option>
							<option value="1"{$gate_logo_1_selected}>{$this->l("Display as separate image")}</option>
						</select>
					</td>
				</tr>
				<tr>
        			<td></td>
        			<td>
						<span class="p24info">{$this->l("Can improve display with some particular templates or modules")}</span>
					</td>
        		</tr>
				<tr>
					<td><label for="chevron_right{$c_sufix}"><h5 style="display: inline">{$this->l("Show right arrow on a list of payment gateways")}</h5></label></td>
					<td><input type="checkbox" name="chevron_right{$c_sufix}" id="chevron_right{$c_sufix}"{$show_chevron_right_checked} /></td>
				</tr>
				<tr>
					<td><label for="gate_class"><h5 style="display: inline">{$this->l("Additional CSS Class for payment gateway")}</h5></label></td>
					<td>
						<input type="text" name="gate_class{$c_sufix}" id="gate_class" value="{$gate_class_value}" />
					</td>
				</tr>
HTML;

                if ($currency['iso_code'] == 'PLN') {
                    $this->_html .= <<<HTML
        		<tr>
        			<td><label for="paymethod_promote{$c_sufix}"><h5>{$this->l("Promote some payment methods")}</h5></label></td>
        			<td>
						<input type="checkbox" name="paymethod_promote{$c_sufix}" id="paymethod_promote{$c_sufix}"{$promote_enabled_checked} />
						<span class="p24info">{$this->l("Selected items will be shown next to other payments gateways")}</span>
					</td>
        		</tr>
        		<tr>
        			<td></td>
        			<td>
						<input type="hidden" name="paymethod_promoted{$c_sufix}" id="paymethod_promoted{$c_sufix}" value="{$paymethod_promoted}" />
						<div id="paymethod_promote_list{$c_sufix}" style="{$paymethod_promote_list_style}">
							{$payment_list_promote}
						</div>
					</td>
        		</tr>
HTML;
                }
                $this->_html .= <<<HTML
        	</table>
        </fieldset>

		<script>
			function updatePaymethodPromoted() {
				var paymethod_promoted = [];
				$('.paylistprom:checked').each(function() {
					paymethod_promoted.push($(this).attr('data-val'));
				});
				$('#paymethod_promoted').val(paymethod_promoted.join(','));
			}
			$(document).ready(function(){
				$('head').append('<link rel="stylesheet" href="{$p24_css}" type="text/css" />');
				$('head').append('<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/smoothness/jquery.ui.all.css" type="text/css" />');
				$.getScript("//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js",
					function() {
						$(".sortable.selected,.sortable.available").sortable({
							connectWith: ".sortable.selected,.sortable.available",
							placeholder: "bank-box bank-placeholder",
							stop: function(){ updatePaymethods(); },
							revert: true,
							start: function(){
								window.setTimeout(function(){
									$('.bank-box.ui-sortable-helper').on('mouseup', function(){
										$(this).addClass('ui-unrotate-helper');
									});
								});
							},
						}).disableSelection();

						$("#paymethod_promote_list")
							.sortable({
								stop: function() { updatePaymethodPromoted(); },
								axis: 'y',
							})
							.disableSelection()
						;
					}
				);
				$('.paylistprom').change(function(){
					updatePaymethodPromoted();
				});
				$('#paymethod_promote').change(function(){
					if ($(this).is(":checked")) {
						$('#paymethod_promote_list:not(:visible)').slideDown(400, function(){ $('body').animate({ scrollTop: $('#paymethod_promote_list').closest('fieldset').offset().top - $('.page-head').height() - 10 }) });
					} else {
						$('#paymethod_promote_list:visible').slideUp();
					}
				});
				var paymethod_promoted = $('#paymethod_promoted').val().split(',').reverse();
				if (paymethod_promoted.length > 0) {
					for (i in paymethod_promoted) {
						$('.paylistprom_item_' + paymethod_promoted[i]).prependTo('#paymethod_promote_list');
					}
				}
			});
		</script>

HTML;
            }


            $extracharge_enabled_checked = ((Configuration::get('P24_EXTRACHARGE_ENABLED' . $c_sufix) == 1) ? ' checked="checked"' : '');
            //$currency = Context::getContext()->currency->iso_code;
            $increase_amount = Configuration::get('P24_EXTRACHARGE_AMOUNT' . $c_sufix);
            $increase_percent = Configuration::get('P24_EXTRACHARGE_PERCENT' . $c_sufix);

            $this->_html .= <<<HTML
        <fieldset>
        	<legend><img src="../img/admin/payment.gif" /> {$this->l("Extra charge settings")}</legend>
        	<table>
        		<tr>
        			<td><label for="extracharge_enabled{$c_sufix}"><h5>{$this->l("Enable extra charge")}</h5></label></td>
        			<td>
						<input type="checkbox" name="extracharge_enabled{$c_sufix}" id="extracharge_enabled"{$extracharge_enabled_checked} />
					</td>
        		</tr>
        		<tr>
        			<td>{$this->l("Increase payment (amount)")}</td>
        			<td>
						<div class="input-group col-lg-2">
						<input type="number" min="0" max="9999" step="0.01" name="increase_amount{$c_sufix}" id="increase_amount{$c_sufix}" style="width: 70px;" value="{$increase_amount}"  class="form-control"/>
						<span class="input-group-addon">{$currency['iso_code']}</span>
						</div>
					</td>
        		</tr>
        		<tr>
        			<td>{$this->l("Increase payment (percent)")}</td>
        			<td>
						<div class="input-group col-lg-2">
						<input type="number" min="0" max="100" step="0.01" pattern="[0-9]+\.\d{2}" name="increase_percent{$c_sufix}" id="increase_percent{$c_sufix}" style="width: 70px;" value="{$increase_percent}"  class="form-control"/>
						<span class="input-group-addon">%</span>
						</div>
					</td>
        		</tr>
        		<tr>
        			<td></td>
        			<td><span class="p24info">{$this->l("Extra charge description")}</span></td>
        		</tr>
        	</table>
        </fieldset>
HTML;

            $extradiscount = Configuration::get('P24_EXTRADISCOUNT_ENABLED' . $c_sufix);
            $extradiscount_amount = Configuration::get('P24_EXTRADISCOUNT_AMOUNT' . $c_sufix);
            $extradiscount_percent = Configuration::get('P24_EXTRADISCOUNT_PERCENT' . $c_sufix);

            $extradiscount_selected = array('', '', '');
            $extradiscount_selected[$extradiscount] = ' selected="selected" ';

            $this->_html .= <<<HTML
<fieldset>
    <legend><img src="../img/admin/payment.gif"/> {$this->l("Extra discount settings")}</legend>
    <table>
        <tr>
            <td><label for="extradiscount_enabled"><h5>{$this->l("Enable extra discount")}</h5></label></td>
            <td>
                <select name="extradiscount_enabled{$c_sufix}"
                        onChange="var c='.extradiscount-'+$(this).val();$('.extradiscount:not('+c+')').hide().find('input').val('');$(c).show()">
                    <option value="0" {$extradiscount_selected[0]}>{$this->l("Discount disabled")}</option>
                    <option value="1" {$extradiscount_selected[1]}>{$this->l("Discount amount")}</option>
                    <option value="2" {$extradiscount_selected[2]}>{$this->l("Discount percent")}</option>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><span class="p24info">{$this->l("It allows for an additional discount if the customer uses Przelewy24")}</span></td>
        </tr>
        <tr class="extradiscount extradiscount-1" style="display:none">
            <td>{$this->l("Discount amount")}</td>
            <td>
                <div class="input-group col-lg-2">
                    <input type="number" min="0" max="9999" step="0.01" name="extradiscount_amount{$c_sufix}"
                           id="extradiscount_amount{$c_sufix}" style="width: 70px;" value="{$extradiscount_amount}"
                           class="form-control"/>
                    <span class="input-group-addon">{$currency['iso_code']}</span>
                </div>
            </td>
        </tr>
        <tr class="extradiscount extradiscount-2" style="display:none">
            <td>{$this->l("Discount percent")}</td>
            <td>
                <div class="input-group col-lg-2">
                    <input type="number" min="0" max="100" step="0.01" pattern="[0-9]+\.\d{2}"
                           name="extradiscount_percent{$c_sufix}" id="extradiscount_percent{$c_sufix}"
                           style="width: 70px;" value="{$extradiscount_percent}" class="form-control"/>
                    <span class="input-group-addon">%</span>
                </div>
            </td>
        </tr>
    </table>
</fieldset>
HTML;

            if ($currency['iso_code'] == 'PLN') {

                $zencard_alert = '';
                $mode_zencard_badge[$currency['iso_code']] = '';
                $mode_zencard = (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1) ? ' checked="checked"' : '';
                if ((Tools::getValue('mode_zencard' . $c_sufix) != '') && ($mode_zencard == '')) {
                    $zencard_alert =
                        "<tr><td></td><td><div class='alert alert-warning' style='margin-top: 10px;'>" .
                        $this->l('Usługa nie jest dostępna') .
                        "</div></td></tr>";
                }
                if ($mode_zencard != '') {
                    $mode_zencard_badge[$currency['iso_code']] = 'badge-yes';
                }

                $this->_html .= <<<HTML
<fieldset> <!-- Zencard -->
    <legend><img src="../img/admin/payment.gif"/> {$this->l("ZenCard configuration")}</legend>
    <table>
        <tbody>
         <tr>
            <td>{$this->l("Enable ZenCard")}</td>
            <td>
                <input id="mode_zencard{$c_sufix}"
                       name="mode_zencard{$c_sufix}" type="checkbox"{$mode_zencard} style="margin-right: 10px;">
                <span class="{$mode_zencard_badge[$currency['iso_code']]}"></span>
            </td>
        </tr>
        {$zencard_alert}
        <tr>
            <td></td>
            <td><span class="p24info">{$this->l("ZenCard configuration you can find in Przelewy24 panel")}</span></td>
        </tr>
        </tbody>
    </table>
</fieldset>
HTML;
            } // IF ZENCARD END

            $this->_html .= <<<HTML
<script>$(document).ready(function () {
    $('[name=extradiscount_enabled]').trigger('change');
});</script>
</div>
HTML;

        } // FOREACH LANGUAGE CONF END;
        $this->_html .= <<<HTML
        <input type="submit" name="submit" value="{$this->l('Save settings')}" class="button" />
    </form>
HTML;

    }

    /**
     * @return bool
     */
    public function install()
    {
        if (_PS_VERSION_ >= 1.5) {
            parent::install();
            $tableName = _DB_PREFIX_ . 'p24_recuring';
            $recsql = <<<SQLTEXT
CREATE TABLE IF NOT EXISTS `{$tableName}` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `website_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  `reference_id` VARCHAR(35) NOT NULL,
  `expires` VARCHAR(4) NOT NULL,
  `mask` VARCHAR (32) NOT NULL,
  `card_type` VARCHAR (20) NOT NULL,
  `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_FIELDS` (`mask`,`card_type`,`expires`,`customer_id`,`website_id`)
) ENGINE = InnoDB;
SQLTEXT;
            Db::getInstance()->Execute($recsql);

            $this->addOrderState('Awaiting Przelewy24 payment', '1', 'Lightblue');
            $this->addOrderState('Payment Przelewy24 accepted', '2', 'Limegreen', 1, 1, 'payment');
            $this->addOrderState('Przelewy24. Cart changed after confirmed order', '3', 'Red', 1, 1);

            // Set default settings in installation
            foreach (CurrencyCore::getCurrencies() as $currency) {
                $c_sufix = $currency['iso_code'] == 'PLN' ? '' : '_' . $currency['iso_code'];
                if (!Configuration::get('P24_PAYMETHOD_GRAPHICS' . $c_sufix)) {
                    Configuration::updateValue('P24_PAYMETHOD_GRAPHICS' . $c_sufix, 1);
                }
                if (!Configuration::get('P24_PAYMETHOD_FIRST' . $c_sufix)) {
                    Configuration::updateValue('P24_PAYMETHOD_FIRST' . $c_sufix, '25,31,112,20,65');
                }
                if (!Configuration::get('P24_DEFAULT_REMEMBER_CARD' . $c_sufix)) {
                    Configuration::updateValue('P24_DEFAULT_REMEMBER_CARD' . $c_sufix, 1);
                }
            }

            if (!$this->registerHook('displayPayment')
                || !$this->registerHook('displayOrderDetail')
                || !$this->registerHook('displayRightColumnProduct')
                || !$this->registerHook('displayCustomerAccount')
                || !$this->registerHook('displayHeader')
                || !$this->registerHook('displayBackOfficeTop')
                || !$this->registerHook('displayInvoice')
                || !$this->registerHook('displayShoppingCartFooter')
                || !$this->registerHook('displayShoppingCart')
                || !$this->registerHook('displayAdminOrderTabOrder')
                || !$this->registerHook('displayAdminOrderContentOrder')
                || !$this->registerHook('displayPDFInvoice')
                || !$this->registerHook('actionValidateOrder')
                || !$this->registerHook('actionOrderStatusPostUpdate')
                || !$this->registerHook('displayFooter')
                || !$this->createAmountTable()
                || !$this->createLastPaymentMethodTable()
                || !$this->createCustomerSettingsTable()
                || !$this->createExtrachargesTable()
            ) {
                return false;
            }
            return true;
        } else {
            parent::uninstall();
            return false;
        }
    }

    /**
     * @param $title
     * @param $number
     * @param $color
     * @param int $payed
     * @param int $invoice
     * @param string $template
     */
    private function addOrderState($title, $number, $color, $payed = 0, $invoice = 0, $template = '')
    {
        $title = $this->l($title);
        $lang_table = Db::getInstance()->ExecuteS('SELECT `id_lang` FROM `' . _DB_PREFIX_ . 'lang`');
        foreach ($lang_table as $langItem) {
            $langId = (int)$langItem['id_lang'];

            $orderStateExists = Db::getInstance()->getRow('SELECT `id_order_state` FROM `' . _DB_PREFIX_ . 'order_state_lang` WHERE name = \'' . pSQL($title) . '\'');
            $rq = Db::getInstance()->getRow('SELECT `id_order_state` FROM `' . _DB_PREFIX_ . 'order_state_lang`	WHERE id_lang = \'' . $langId . '\' AND  name = \'' . pSQL($title) . '\'');
            if ($rq && isset($rq['id_order_state']) && intval($rq['id_order_state']) > 0) {
                define('P24_ORDER_STATE_' . $number, $rq['id_order_state']);
                Configuration::updateValue('P24_ORDER_STATE_' . $number, $rq['id_order_state']);
            } else {
                if (!$orderStateExists) {
                    Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'order_state` (`unremovable`, `color`, `paid`, `invoice`) VALUES(1, \'' . $color . '\', ' . pSQL($payed) . ', ' . pSQL($invoice) . ')');
                    $stateid = Db::getInstance()->Insert_ID();
                }
                Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES(' . intval($stateid) . ', ' . $langId . ', \'' . pSQL($title) . '\',\'' . pSQL($template) . '\')');

                define('P24_ORDER_STATE_' . $number, $stateid);
                Configuration::updateValue('P24_ORDER_STATE_' . $number, $stateid);
            }
        }
    }

    /**
     * @return bool
     */
    private function createAmountTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`' . _DB_PREFIX_ . 'przelewy24_amount` (
			`i_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`s_sid`	char(32) NOT NULL,
			`i_id_order` INT UNSIGNED NOT NULL ,
			`i_amount` INT UNSIGNED NOT NULL
			);');

        /* backwards compatibility */
        $has_field_p24_method = Db::getInstance()->getRow("SELECT EXISTS(
			SELECT * FROM information_schema.columns
			WHERE table_schema = database()
			AND COLUMN_NAME = 'p24_method'
			AND table_name = '" . _DB_PREFIX_ . "przelewy24_amount') AS jest");
        if (!$has_field_p24_method['jest']) {
            Db::getInstance()->Execute('ALTER TABLE ' . _DB_PREFIX_ . 'przelewy24_amount ADD COLUMN p24_method INT;');
        }
        return true;
    }

    private function createLastPaymentMethodTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`' . _DB_PREFIX_ . 'przelewy24_lastmethod` (
			`customer_id` INT NOT NULL PRIMARY KEY,
			`p24_method` INT NOT NULL
			);');
        return true;
    }

    private function createCustomerSettingsTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`' . _DB_PREFIX_ . 'przelewy24_customersettings` (
			`customer_id` INT NOT NULL PRIMARY KEY,
			`cc_forget` INT DEFAULT 0
			);');
        return true;
    }

    private function createExtrachargesTable()
    {
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS`' . _DB_PREFIX_ . 'przelewy24_extracharges` (
			`id_extracharge` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`id_order` INT NOT NULL,
			`extracharge_amount` DECIMAL(20,6) NOT NULL
			);');
        return true;
    }

    public function hookDisplayPayment()
    {
        global $smarty, $cart;

        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        if (empty($currency)) return;
        //$amount = $order->total_products_wt * 100; // total products price tax included
        $amount = $cart->getOrderTotal(true, Cart::BOTH);
        $protocol = Przelewy24::getHttpProtocol();
        $smarty->assign('p24_url', $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__);
        $smarty->assign('ps_version', _PS_VERSION_);

        $p24_url_payment = $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array(), Configuration::get('PS_SSL_ENABLED') == 1);

        $channels_ids = array();
        $channels_list = array();

        if ($amount >= 300 && Configuration::get('P24_INSTALLMENT_SHOW' . $c_sufix) > 0) { // installment with suffix
            $channels_ids = $this->getChannelsRaty();
        }

        if (Configuration::get('P24_PAYMETHOD_PROMOTE' . $c_sufix) == 1 && $currency == 'PLN') {
            $paymethod_promoted = Configuration::get('P24_PAYMETHOD_PROMOTED' . $c_sufix);
            $paymethod_promoted_ar = explode(',', $paymethod_promoted);
            if (is_array($paymethod_promoted_ar)) {
                foreach ($paymethod_promoted_ar as $id) {
                    if ($id > 0) {
                        $channels_ids[] = $id;
                    }
                }
            }
        }

        if (sizeof($channels_ids) > 0) {
            $payment_list_res = $this->availablePaymentMethods(Configuration::get('P24_PAYSLOW_ENABLED' . $c_sufix) != 1, $currency);
            $payment_list = array();
            if (is_array($payment_list_res)) {
                foreach ($payment_list_res as $item) {
                    $payment_list[$item->id] = $item;
                }
            }

            foreach ($channels_ids as $id) {
                if (isset($payment_list[$id])) {
                    $channels_list[] = array(
                        'name' => $payment_list[$id]->name,
                        'logo' => "https://secure.przelewy24.pl/template/201312/bank/logo_{$id}.gif",
                        'url' => "{$p24_url_payment}?payment_method={$id}",
                        'desc' => Configuration::get('P24_CUSTOM_DESCRIPTION' . $c_sufix)
                    );
                }
            }
        }

        $channels_list[] = array(
            'name' => "Przelewy24",
            'logo' => _MODULE_DIR_ . "przelewy24/img/logo_small.png",
            'url' => $p24_url_payment,
            'desc' => Configuration::get('P24_CUSTOM_DESCRIPTION' . $c_sufix)
        );

        $smarty->assign('p24_channels_list', $channels_list);
        $smarty->assign('p24_gate_logo', (int)Configuration::get('P24_GATE_LOGO' . $c_sufix));
        $smarty->assign('p24_gate_class', Configuration::get('P24_GATE_CLASS' . $c_sufix));
        $smarty->assign('p24_chevron_right', (int)Configuration::get('P24_CHEVRON_RIGHT' . $c_sufix));

        return $this->display(__FILE__, 'payment.tpl');
    }

    public static function requestGet($url)
    {
        $isCurl = function_exists('curl_init') && function_exists('curl_setopt') && function_exists('curl_exec') && function_exists('curl_close');

        if ($isCurl) {
            $userAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
            $curlConnection = curl_init();
            curl_setopt($curlConnection, CURLOPT_URL, $url);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlConnection, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curlConnection);
            curl_close($curlConnection);
            return $result;
        }
        return "";
    }

    public function hookDisplayRightColumnProduct()
    {
        global $smarty, $cookie;
        if (Configuration::get('P24_INSTALLMENT_SHOW') == 2) {
            $productID = Tools::getValue('id_product');

            if (isset($productID) && $productID != '') {
                $product = new Product((int)$productID, true, $cookie->id_lang);
                $amount = $product->getPrice(true, null, 2, null, false, true, 1);
                $context = Context::getContext();
                $id_currency = (int)Validate::isLoadedObject($context->currency) ? $context->currency->id : Configuration::get('PS_CURRENCY_DEFAULT');
                $currencies = $this->getCurrency(intval($id_currency));
                foreach ($currencies as $c) {
                    if ($c['id_currency'] == $id_currency) {
                        $currency = $c['iso_code'];
                    }
                }
                if ($amount >= 300 && $currency == 'PLN') {
                    $amountGrosh = $amount * 100;

                    $result = self::requestGet("https://secure.przelewy24.pl/kalkulator_raty.php?ammount=" . $amountGrosh);
                    $result_tab = explode('<br>', $result);

                    $smarty->assign(array(
                        'part_count' => $result_tab[0],
                        'part_cost' => $result_tab[1],
                        'product_ammount' => $amount,
                        'p24_ajax_url' => $this->context->link->getModuleLink('przelewy24', 'paymentAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1),
                    ));

                    return $this->display(__FILE__, 'installmentRightColumnProduct.tpl');
                }
            }
        }
    }

    public function hookDisplayOrderDetail($params)
    {
        global $smarty;
        $order = new Order($_GET['id_order']);
        $state = new OrderState($order->current_state);

        $przelewy24Statuses = array(
            (int)Configuration::get('P24_ORDER_STATE_1'),
            (int)Configuration::get('P24_ORDER_STATE_2'),
            (int)Configuration::get('P24_ORDER_STATE_3')
        );

        $currency = new Currency($params['order']->id_currency);
        $extracharge = Extracharge::getExtrachargeByOrderId((int)$params['order']->id);
        $extracharge_amount = $extracharge->extracharge_amount;

        if ($extracharge_amount > 0) {
            $smarty->assign(
                array(
                    'extracharge' => number_format(round($extracharge_amount, 2), 2, ",", '.') . ' ' . $currency->sign,
                    'extracharge_text' => $this->l('Extracharge Przelewy24')
                )
            );
        }
        $customer = new Customer((int)($order->id_customer));
        if ($order->module == 'przelewy24' && !($state->paid || $order->current_state == Configuration::get('P24_ORDER_STATE_2'))) {
            $smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array('order_id' => $order->id, 'token'=>$customer->secure_key)));
            return $this->display(__FILE__, 'renewPaymentOrderDetail.tpl');
        } else {
            return $this->display(__FILE__, 'user_order_extracharge.tpl');
        }
    }

    public function hookDisplayCustomerAccount()
    {
        if (Configuration::get('P24_ONECLICK_ENABLED') == 1) {
            global $smarty;

            $protocol = Przelewy24::getHttpProtocol();
            $smarty->assign('base_url', $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__);

            return $this->display(__FILE__, 'myStoredCards.tpl');
        }
    }

    public function hookDisplayHeader($params)
    {
        $this->removeDiscount();
    }

    public function hookDisplayFooter()
    {

        global $smarty, $cart;
        $this->removeDiscount();

        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        if (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1) {

            $protocol = $this->getProtocol();
            $smarty->assign('mode_zencard', true);
            $smarty->assign('p24_zencard_script', $this->p24_zencard_script($c_sufix));
            $summary_details = $cart->getSummaryDetails();
            $smarty->assign('p24_zencard_products_with_tax', $summary_details["total_products_wt"]);
            $smarty->assign('get_cart_total_amount_url', $protocol .
                htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') .
                __PS_BASE_URI__ .
                '?action=getGrandTotal&fc=module&module=przelewy24&controller=zenCardAjax');
            //Poprzednia wersja linku zostawiam gdyby coś nie pykło po poprawce
//                'module/przelewy24/zenCardAjax?action=getGrandTotal');

            // currency sign
            $actualCurrency = $this->getActualCurrencyById($cart->id_currency);
            $currencySign = empty($actualCurrency['sign']) ? 'zł' : $actualCurrency['sign'];
            $smarty->assign('currency_sign', $currencySign);

            return $this->display(__FILE__, 'zenCardFooter.tpl');
        }
    }

    private function getProtocol()
    {
        return 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : '') . '://';
    }

    public function hookDisplayInvoice($params)
    {
        global $smarty;
        if (Configuration::get('P24_IVR_ENABLED') == 1) {
            $id_order = (int)$params['id_order'];
            $order = new Order($id_order);
            $state = new OrderState($order->current_state);
            $address = new Address((int)$order->id_address_delivery);
            $clientPhone = (!empty($address->phone_mobile) ? $address->phone_mobile : $address->phone);

            if (!$state->paid) return <<<HTML
			<script>
				$(document).ready(function(){
					$('ul#toolbar-nav').append('<li><a id="page-header-p24-ivr" class="toolbar_btn" href="#" title="IVR"><i class="process-icon-phone icon-phone"></i><div>{$this->l('Pay with IVR')}</div></a></li>');
					$('#page-header-p24-ivr').click(function(){
						if (confirm("{$this->l('Do you want to start payment by IVR and make call back to the customer?')} {$clientPhone}")) {
							$.ajax('{$this->context->link->getModuleLink('przelewy24', 'paymentAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1)}', {
								method: 'POST', type: 'POST',
								data : { action: 'runIVR', id_order: '{$id_order}' },
								success : function(data) {
									window.location.reload();
								},
							});
						}
					});
				});
			</script>
HTML;
        }
    }

    public function hookDisplayBackOfficeTop($params)
    {
        global $smarty;
        $url = 'http://www.przelewy24.pl/pobierz';

        if (Tools::getValue('p24_update_plugin') == true) {
            $success = false;
            $html = self::requestGet($url);
            if (preg_match('#<a href="([^"]+/przelewy24_presta15_16_[^/"]+.zip)"#im', $html, $matches) == 1) {

                $url = "http://przelewy24.pl" . $matches[1];
                $file = tempnam(sys_get_temp_dir(), 'p24_update_');

                file_put_contents($file, self::requestGet($url));

                if (filesize($file) > 0) {
                    if (Tools::ZipExtract($file, _PS_MODULE_DIR_)) {
                        parent::uninstall();
                    }
                }

                @unlink($file);
                if (isset($_COOKIE["p24_plugin_update"])) unset($_COOKIE["p24_plugin_update"]);
                if (_PS_VERSION_ < 1.6) {
                    $link = new Link();
                    Tools::redirectAdmin($link->getAdminLink('AdminModules') . '&anchor=anchorPrzelewy24');
                }
            }
            return;
        }

    }


    public static function getHttpProtocol()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
            return 'https://';
        } else {
            return 'http://';
        }
    }

    /*
     * Zwraca kwotę dodatkowej opłaty przy wyborze przelewy24
     * @param int
     * @return float
     *
     * */
    public function getExtrachargeAmount($id_cart)
    {
        $extracharge_amount = 0;
        $cart = new Cart((int)$id_cart);

        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        $amount = $cart->getOrderTotal(true, Cart::BOTH);
        $amount = $this->getP24Amount(intval($cart->id_currency), $amount);

        if (Configuration::get('P24_EXTRACHARGE_ENABLED' . $c_sufix) == 1 && $amount > 0) {
            $inc_amount_settings = (float)Configuration::get('P24_EXTRACHARGE_AMOUNT' . $c_sufix);
            $inc_percent_settings = (float)Configuration::get('P24_EXTRACHARGE_PERCENT' . $c_sufix);

            $inc_amount = round($inc_amount_settings > 0 ? $inc_amount_settings * 100 : 0);
            $inc_percent = round($inc_percent_settings > 0 ? $inc_percent_settings / 100 * $amount : 0);

            $extracharge_amount = max($inc_amount, $inc_percent);
        }

        return $extracharge_amount;
    }

    public function cartHasDiscount($cart = null)
    {
        if (empty($cart)) {
            $cart = Context::getContext()->cart;
        }
        if ($cart) {
            foreach ($cart->getCartRules() as $item) {
                if (substr($item['code'], 0, 4) == $this->discount_code_prefix) {
                    return $item['id_cart_rule'];
                }
            }
        }
        return false;
    }

    public function orderHasDiscount($order)
    {
        if ($order) {
            foreach ($order->getCartRules() as $item) {
                if (strpos($item['name'], 'Rabat Zencard') === 0) {
                    return $item['id_cart_rule'];
                }
            }
        }
        return false;
    }

    public function removeDiscount($cart = null)
    {
        if (strpos($_SERVER['REQUEST_URI'], $this->name) === false) {

            if (empty($cart)) {
                $cart = Context::getContext()->cart;
            }

            $currency = $this->getCurrencyCode($cart->id_currency);
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            if (Configuration::get('P24_EXTRADISCOUNT_ENABLED' . $c_sufix) > 0) {
                $discountId = $this->cartHasDiscount($cart);
                if ($discountId) {
                    $cart->removeCartRule($discountId);
                    $cart_rule = new CartRule((int)$discountId);
                    $cart_rule->delete();
                }
            }
        }
    }

    public function addOrderDiscount($order, $amount, $name, $description = '')
    {
        if ($order) {

            $cart_rule = new CartRule();
            $names = array();

            $languages = Language::getLanguages();
            foreach ($languages as $key => $language) {
                $names[$language['id_lang']] = $name ? "Rabat Zencard - " . $name : "Rabat Zencard";
            }
            $prefix = $this->p24_zencard_coupon_prefix();
            $couponCode = uniqid($prefix);

            $cart_rule->name = $names;
            $cart_rule->id_customer = $order->id_customer;
            $cart_rule->date_from = date('Y-m-d H:i:s');
            $cart_rule->date_to = date('2036-01-01 13:00:00');
            $cart_rule->description = $description;
            $cart_rule->quantity = 1;
            $cart_rule->quantity_per_user = 1;
            $cart_rule->priority = 1;
            $cart_rule->partial_use = 0;
            $cart_rule->code = $couponCode;
            $cart_rule->minimum_amount = 0.01;
            $cart_rule->minimum_amount_currency = 1;
            $cart_rule->product_restriction = 1;
            $cart_rule->reduction_amount = $amount;
            $cart_rule->reduction_tax = 1;
            $cart_rule->reduction_currency = intval(Configuration::get('PS_CURRENCY_DEFAULT'));
            $cart_rule->reduction_product = 0;
            $cart_rule->active = 1;
            $cart_rule->date_add = date('Y-m-d H:i:s');
            $cart_rule->date_upd = date('Y-m-d H:i:s');
            $cart_rule->add();
            $cart = new Cart($order->id_cart);


            $cart->addCartRule($cart_rule->id);
            $values = array(
                'tax_incl' => (float)$amount,
                'tax_excl' => (float)$amount
            );

            $cart->update();
            $order->addCartRule($cart_rule->id, $cart_rule->name[Configuration::get('PS_LANG_DEFAULT')], $values);

            $order->update();
            $this->updateOrderDiscounts($order);

            return true;
        }

        return false;
    }



    /*
     * Dodaje do koszyka kupon zniżkowy, przy wyborze przelewy24
     * @param cart
     * @return void
     *
     * */
    public function addDiscount($cart = null, $currency = 'PLN')
    {

        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        if (Configuration::get('P24_EXTRADISCOUNT_ENABLED' . $c_sufix) > 0) {
            if (empty($cart)) {
                $cart = Context::getContext()->cart;
            }
            if ($cart) {
                $discountId = $this->cartHasDiscount($cart);
                if ($discountId == false) {
                    $cart_rule = new CartRule();

                    $languages = Language::getLanguages();
                    foreach ($languages as $key => $language) {
                        $array[$language['id_lang']] = "Rabat za użycie Przelewy24";
                    }

                    $cart_rule->name = $array;
                    $cart_rule->code = strtoupper(uniqid($this->discount_code_prefix));
                    $cart_rule->description = "Rabat za użycie Przelewy24";
                    $cart_rule->partial_use = 1;
                    $cart_rule->reduction_currency = intval(Configuration::get('PS_CURRENCY_DEFAULT'));
                    $cart_rule->active = 1;
                    $cart_rule->id_customer = $cart->id_customer;
                    $cart_rule->date_from = date('Y-m-d 00:00:00');
                    $cart_rule->date_to = date('Y-m-d h:i:s', mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
                    $cart_rule->minimum_amount = '1';
                    $cart_rule->minimum_amount_currency = '1';
                    $cart_rule->quantity = '1';
                    $cart_rule->quantity_per_user = '1';
                    $cart_rule->product_restriction = '1';
                    $cart_rule->priority = '1';

                    $cart_rule->reduction_tax = true;
                    $cart_rule->reduction_product = 0;
                    $cart_rule->date_add = 'NOW()';
                    $cart_rule->date_upd = 'NOW()';

                    $cart_rule->reduction_amount = Configuration::get('P24_EXTRADISCOUNT_ENABLED' . $c_sufix) == 1 ? Configuration::get('P24_EXTRADISCOUNT_AMOUNT' . $c_sufix) : 0;
                    $cart_rule->reduction_percent = Configuration::get('P24_EXTRADISCOUNT_ENABLED' . $c_sufix) == 2 ? Configuration::get('P24_EXTRADISCOUNT_PERCENT' . $c_sufix) : 0;

                    $cart_rule->add();

                    $cart->addCartRule($cart_rule->id);
                    return true;
                }
            }
        }
        return false;
    }

    public static function soap_method_exists($soapClient, $method)
    {
        $list = $soapClient->__getFunctions();
        if (is_array($list)) {
            foreach ($list as $line) {
                list($type, $name) = explode(' ', $line, 2);
                if (strpos($name, $method) === 0) return true;
            }
        }
        return false;
    }

    private function checkIvrPresent($c_sufix)
    {
        $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));
        $has_ivr = false;
        try {
            $s = new SoapClient($P24C->getHost() . $this->getWsdlService(Configuration::get('P24_MERCHANT_ID' . $c_sufix)), array('trace' => true, 'exceptions' => true));
            $has_ivr = $this->soap_method_exists($s, 'TransactionMotoCallBackRegister');
        } catch (Exception $e) {
            error_log(__METHOD__ . ' ' . $e->getMessage());
        }

        return !!$has_ivr;
    }

    private function ccCheckRecurrency($c_sufix)
    {
        $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));
        try {
            $s = new SoapClient($P24C->getHost() . $this->getWsdlCCService(), array('trace' => true, 'exceptions' => true));
            $res = $s->checkRecurrency(Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix));
        } catch (Exception $e) {
            error_log(__METHOD__ . ' ' . $e->getMessage());
        }
        return !!$res;
    }

    private function apiTestAccess($c_sufix)
    {
        $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));
        try {
            $s = new SoapClient($P24C->getHost() . $this->getWsdlService(Configuration::get('P24_MERCHANT_ID' . $c_sufix)), array('trace' => true, 'exceptions' => true));
            $res = $s->TestAccess(Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_API_KEY' . $c_sufix));
        } catch (Exception $e) {
            error_log(__METHOD__ . ' ' . $e->getMessage());
        }

        return !!$res;
    }

    public function availablePaymentMethods($only24at7 = true, $currency = 'PLN')
    {

        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));

        try {
            $s = new SoapClient($P24C->getHost() . $this->getWsdlService(Configuration::get('P24_MERCHANT_ID' . $c_sufix)), array('trace' => true, 'exceptions' => true));
            $res = $s->PaymentMethods(Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_API_KEY' . $c_sufix), Context::getContext()->language->iso_code);
        } catch (Exception $e) {
            error_log(__METHOD__ . ' ' . $e->getMessage());
        }
        if ($res->error->errorCode === 0) {
            if ($only24at7) {
                foreach ($res->result as $key => $item) {
                    if (!$item->status) {
                        unset($res->result[$key]);
                    }
                }
            }
            if ($currency != 'PLN') {
                foreach ($res->result as $key => $item) {
                    if (!in_array($item->id, $this->getChannelsNonPln())) {
                        unset($res->result[$key]);
                    }
                }
            }
            return $res->result;
        }
        return false;
    }

    public function validateGA($nr)
    {
        if (preg_match('#^[A-Z]{2}\-\d+\-\d+$#', $nr)) return $nr;
        return false;
    }

    public static function getChannelsNonPln()
    {
        return array(124, 140, 145, 152, 66, 92);
    }

    public static function getChannelsRaty()
    {
        return array(72, 129, 136);
    }

    public static function getWsdlService($merchantId)
    {
        return str_replace('[P24_MERCHANT_ID]', $merchantId, 'external/[P24_MERCHANT_ID].wsdl');
    }

    public static function getWsdlCCService()
    {
        return 'external/wsdl/charge_card_service.php?wsdl';
    }

    /***** ZenCard *****/

    public function zenCardOrderProcessing( $order, $transactionConfirm )
    {
        try
        {
            if( $transactionConfirm->withZencard() || ( !$transactionConfirm->withZencard() && $transactionConfirm->getInfo() != '' ) )
            {
                $info = $transactionConfirm->getInfo();

                $amount = $transactionConfirm->getAmountWithDiscount();
                $without = $transactionConfirm->getAmount();
                $discountAmount = round( $without - $amount );

                $this->addOrderDiscount( $order, $discountAmount / 100, $info );
            }
        }
        catch( Exception $e )
        {
            error_log( __METHOD__ . ' ' . $e->getMessage() );
        }

        return true;
    }

    public function p24_zencard_script($c_sufix = '')
    {
        $zenCardApi = new ZenCardApi(Configuration::get("P24_MERCHANT_ID" . $c_sufix), Configuration::get("P24_API_KEY" . $c_sufix));
        $zencardScript = $zenCardApi->getScript();
        $zencardScript = str_replace("script", "div id=\"zenScript\"", $zencardScript);
        return $zencardScript;
    }

    public function p24_zencard_coupon_prefix()
    {
        return 'zencard';
    }


    public function addZenCardCoupon($cart, $couponCode, $description = '', $amount = 0)
    {
        $customer = Context::getContext()->customer;

        $cart_rule = new CartRule();

        $languages = Language::getLanguages();
        foreach ($languages as $key => $language) {
            $names[$language['id_lang']] = "Rabat Zencard";
        }

        $cart_rule->name = $names;
        $cart_rule->id_customer = $customer->id;
        $cart_rule->date_from = date('Y-m-d H:i:s');
        $cart_rule->date_to = date('2036-01-01 13:00:00');
        $cart_rule->description = $description;
        $cart_rule->quantity = 1;
        $cart_rule->quantity_per_user = 1;
        $cart_rule->priority = 1;
        $cart_rule->partial_use = 0;
        $cart_rule->code = $couponCode;
        $cart_rule->minimum_amount = 0.01;
        $cart_rule->minimum_amount_currency = 1;
        $cart_rule->product_restriction = 1;
        $cart_rule->reduction_amount = $amount;
        $cart_rule->reduction_tax = 1;
        $cart_rule->reduction_currency = intval(Configuration::get('PS_CURRENCY_DEFAULT'));
        $cart_rule->reduction_product = 0;
        $cart_rule->active = 1;
        $cart_rule->date_add = date('Y-m-d H:i:s');
        $cart_rule->date_upd = date('Y-m-d H:i:s');

        $cart_rule->add();
        $cart->addCartRule($cart_rule->id);
    }


    public function hookDisplayShoppingCart($cart)
    {
        global $smarty, $cart;

        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        if (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1) {
            $summary_details = $cart->getSummaryDetails();

            // TEMPLATE VARIABLES
            $smarty->assign('mode_zencard', Configuration::get("P24_MODE_ZENCARD"));
            $smarty->assign('p24_basket_price_class', '\'#p24_zencard_products_with_tax\'');
            $smarty->assign('p24_zencard_products_with_tax', $summary_details["total_products_wt"]);
            //$smarty->assign('p24_price_with_discount', '\'#p24_zencard_total_price\'');
            $smarty->assign(
                'p24_zen_card_ajax_url',
                Context::getContext()->link->getModuleLink('przelewy24', 'zenCardAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1)
            );
            $smarty->assign(
                'p24_ajax_order_url',
                Context::getContext()->link->getPageLink('order', true, NULL)
            );
            $smarty->assign('p24_zencard_script', $this->p24_zencard_script($c_sufix));

            return $this->display(__FILE__, 'zenCard.tpl');
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        global $cart;
        $newOrderStatus = $params['newOrderStatus'];
        $order = new Order($params['id_order']);
        if (empty($cart->id_currency)) {
            $cart = new Cart($order->id_cart);
        }
        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;
        if (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1 && empty(Context::getContext()->controller->admin_webpath)) {
            $this->updateOrderDiscounts($order);
            // when Cash on delivery
            if ($order->module === 'cashondelivery') {
                try {
                    $params = array(
                        'amount' => $order->total_paid
                    );
                    Db::getInstance()->update('order_payment', $params,
                        "`order_reference` = '" . pSQL($order->reference) . "'");

                } catch (Exception $e) {
                    Logger::addLog($e->getMessage(), 1);
                }
            }
        }

        if (
            (int)$order->current_state == (int)Configuration::get('P24_ORDER_STATE_1') ||
            (int)$order->current_state == (int)Configuration::get('P24_ORDER_STATE_2') ||
            (int)$order->current_state == (int)Configuration::get('P24_ORDER_STATE_3')
        ) {
            $this->addExtrachargeToOrder($cart, $order);
        } else {
            if(empty(Context::getContext()->controller->admin_webpath)){ // not in admin page
                $this->removeExtrachargeFromOrder($order);
            }
        }
    }


    public function updateOrderDiscounts($order)
    {
        $result = $order->getCartRules();

        foreach ($result as $cart_rule) {
            if (strpos($cart_rule['name'], 'Rabat Zencard') === 0) {

                $rule = new CartRule($cart_rule['id_cart_rule']);
                $values = array(
                    'tax_incl' => $rule->getContextualValue(true),
                    'tax_excl' => $rule->getContextualValue(false)
                );

                $order->total_discounts = (float)$order->total_discounts + (float)$values['tax_incl'];
                $order->total_discounts_tax_incl = (float)$order->total_discounts_tax_incl + (float)$values['tax_incl'];
                $order->total_paid = (float)$order->total_paid - (float)$values['tax_incl'];
                $order->total_paid_tax_incl = (float)$order->total_paid_tax_incl - (float)$values['tax_incl'];
            }
        }

        $order->update();
    }


    public function hookDisplayPDFInvoice($params)
    {

        $order_invoice = $params['object'];
        if (!($order_invoice instanceof OrderInvoice))
            return;

        $id_order = (int)$order_invoice->id_order;
        $order = new Order($id_order);
        $currency = new Currency((int)$order->id_currency);
        $extracharge = Extracharge::getExtrachargeByOrderId($id_order);
        if ($extracharge)
            return $this->l('Extracharged was added to this order by Przelewy24: ') . ' ' . number_format(round($extracharge->extracharge_amount, 2), 2, ",", ".") . ' ' . $currency->sign;
    }

    public function hookActionValidateOrder($params)
    {
        global $cart;

        $przelewy24Statuses = array(
            (int)Configuration::get('P24_ORDER_STATE_1'),
            (int)Configuration::get('P24_ORDER_STATE_2'),
            (int)Configuration::get('P24_ORDER_STATE_3')
        );

        $currency = $this->getCurrencyCode($cart->id_currency);
        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;
        $order_id = Order::getOrderByCartId($cart->id);
        $order = new Order($order_id);

        if (in_array($params['order']->current_state, $przelewy24Statuses))
            $this->addExtrachargeToOrder($cart, $order);

        if (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1) {

            $result = $this->processZenCard($order, $c_sufix);
            if (!$result) {
                $order->delete();
                $url = $this->context->link->getModuleLink('przelewy24', 'zenCardOrderFailed', array(), Configuration::get('PS_SSL_ENABLED') == 1);
                Tools::redirect($url);
            }
        }
    }

    public function processZenCard($order, $c_sufix)
    {
        try {
            $zenCardApi = new ZenCardApi(
                Configuration::get("P24_MERCHANT_ID" . $c_sufix), Configuration::get("P24_API_KEY" . $c_sufix)
            );
            $customer = Context::getContext()->customer;
            $amount = $order->total_products_wt * 100; // total products price tax included
            $zenCardOrderId = $zenCardApi->buildZenCardOrderId($order->id_cart, $_SERVER['SERVER_NAME']);
            $customerEmail = $customer->email;

            if ($customerEmail === null) {
                $milliseconds = round(microtime(true) * 1000);
                $customerEmail = 'przelewy_' . $milliseconds . '@zencard.pl';
            }

            $transaction = $zenCardApi->verify($customerEmail, $amount, $zenCardOrderId);
            Logger::addLog('[zencard verify] ' . print_r($transaction, true), 1);

            if ($transaction->isVerified()) {
                $transactionConfirm = $zenCardApi->confirm($zenCardOrderId, $amount);
                Logger::addLog('[zencard confirm]' . print_r($transactionConfirm, true), 1);

                if (!$transactionConfirm->isConfirmed()) {
                    return false;
                }
                if ($transactionConfirm->hasDiscount() || $transactionConfirm->withZencard()) {
                    return $this->zenCardOrderProcessing($order, $transactionConfirm);
                }
            }
        } catch (Exception $e) {
            Logger::addLog($e->getMessage(), 1);
        }
        return false;
    }

    public function hookDisplayAdminOrderContentOrder($params)
    {
        $currency = new Currency($params['order']->id_currency);
        $extracharge = Extracharge::getExtrachargeByOrderId((int)$params['order']->id);
        if ($extracharge) {
            $extracharge_amount = $extracharge->extracharge_amount;

            if ($extracharge_amount > 0) {
                $this->context->smarty->assign(
                    array(
                        'extracharge' => number_format(round($extracharge_amount, 2), 2, ",", '.') . ' ' . $currency->sign,
                        'extracharge_text' => $this->l('Extracharge Przelewy24')
                    )
                );

                return $this->display(__FILE__, 'extracharge.tpl');
            }
        }
    }

    public function hookDisplayAdminOrderTabOrder($params)
    {
        $order = $params['order'];
        $orderId = intval($order->id);

        if (!$this->isSoapExtensionInstalled() || !$orderId)
            return;

        $dataToRefund = $this->checkIfRefundIsPossibleAndReturnDataToRefund($orderId);

        if (!$dataToRefund)
            return false;

        if (Tools::isSubmit('submitRefund')) {
            $amountToRefund = floatval(Tools::getValue('amountToRefund')) * 100;
            $this->refundProcess($dataToRefund['sessionId'], $dataToRefund['p24OrderId'], $amountToRefund);
        }

        $dataToRefund['refunds'] = false;
        $refunds = $this->getRefunds($dataToRefund['p24OrderId']);

        if ($refunds) {
            $dataToRefund['amount'] -= $refunds['maxToRefund'];
            $dataToRefund['refunds'] = $refunds['refunds'];
        }

        $this->assignToTransactionRefundView($dataToRefund);

        return $this->display(__FILE__, 'transactionRefund.tpl');
    }

    /**
     * @param $orderId
     * @return bool|array
     */
    private function checkIfRefundIsPossibleAndReturnDataToRefund($orderId)
    {
        $dataFromDB = $this->getRefundDataFromDB($orderId);
        if (!$dataFromDB) return false;

        $dataFromP24 = $this->checkIfPaymentCompletedUsingWSAndReturnData($dataFromDB['sessionId']);
        if (!$dataFromP24) return false;

        $return = array(
            'sessionId' => $dataFromDB['sessionId'],
            'amount' => $dataFromP24->result->amount,
            'p24OrderId' => $dataFromP24->result->orderId
        );

        return $return;
    }

    /**
     * @param $orderId
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    private function getRefunds($orderId)
    {
        try {
            $return = false;
            $url = $this->getWSurl();

            $soap = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
            $result = $soap->GetRefundInfo(Configuration::get('P24_MERCHANT_ID'), Configuration::get('P24_API_KEY'), $orderId);

            if (!empty($result->result)) {
                $return['maxToRefund'] = 0;
                foreach ($result->result as $key => $value) {
                    $return['refunds'][$key]['amount_refunded'] = $value->amount;
                    $date = new DateTime($value->date);
                    $return['refunds'][$key]['created'] = $date->format('Y-m-d H:i:s');
                    $return['refunds'][$key]['status'] = $this->getStatusMessage($value->status);

                    if ($value->status === 1 || $value->status === 3) {
                        $return['maxToRefund'] += $value->amount;
                    }
                }
            }

            return $return;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function assignToTransactionRefundView($dataToRefund)
    {
        $result = $this->context->smarty->assign(
            array(
                'amount' => $dataToRefund['amount'],
                'refunds' => $dataToRefund['refunds']
            )
        );

        return $result;
    }

    /**
     * @param $status
     * @return string
     */
    private function getStatusMessage($status)
    {
        switch ($status) {
            case 0:
                $statusMessage = 'błąd';
                break;
            case 1:
                $statusMessage = 'zrealizowany';
                break;
            case 3:
                $statusMessage = 'oczekuje';
                break;
            case 4:
                $statusMessage = 'odrzucony';
                break;
            default:
                $statusMessage = 'nierozpoznany status';
        }

        return $statusMessage;
    }

    private function isSoapExtensionInstalled()
    {
        return extension_loaded('soap');
    }

    private function getWSurl()
    {
        $mode = (Configuration::get('P24_TEST_MODE')) ? 'sandbox' : 'secure';
        $url = 'https://' . $mode . '.przelewy24.pl/external/' . Configuration::get('P24_MERCHANT_ID') . '.wsdl';

        return $url;
    }

    /**
     * @return mixed
     */
    private function refundProcess($sessionId, $p24OrderId, $amountToRefund)
    {
        $refunds = array(
            0 => array(
                'sessionId' => $sessionId,
                'orderId' => $p24OrderId,
                'amount' => $amountToRefund
            )
        );

        $result = $this->refundTransaction($refunds);

        return $result;
    }

    /**
     * @param $refunds
     * @return mixed
     */
    private function refundTransaction($refunds)
    {
        try {
            $url = $this->getWSurl();

            $soap = new SoapClient($url);
            $response = $soap->refundTransaction(
                Configuration::get('P24_MERCHANT_ID'),
                Configuration::get('P24_API_KEY'),
                time(),
                $refunds
            );

            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $orderId
     * @return array|bool
     * @throws PrestaShopDatabaseException
     */
    private function getRefundDataFromDB($orderId)
    {
        $sql = "
            SELECT
              o.id_order as orderId,
              o.id_cart as cartId,
              concat(o.id_cart,'|',pa.s_sid) as sessionId,
              pa.i_amount as amount,
              op.transaction_id as p24_OrderId,
              op.id_order_payment as orderPaymentId
            FROM " . _DB_PREFIX_ . "orders o
              JOIN " . _DB_PREFIX_ . "przelewy24_amount pa ON (o.id_cart = pa.i_id_order)
              JOIN " . _DB_PREFIX_ . "order_payment op ON (o.reference = op.order_reference)
            WHERE o.id_order = {$orderId} and pa.p24_method is not null
        ";

        $result = DB::getInstance()->executeS($sql);

        if (isset($result[0]) && !empty($result[0])) {
            $return = array(
                'sessionId' => $result[0]['sessionId'],
                'amount' => $result[0]['amount'],
                'p24OrderId' => $result[0]['p24_OrderId']
            );

            return $return;
        }

        return false;
    }

    /**
     * @param $sessionId
     * @return bool
     */
    private function checkIfPaymentCompletedUsingWSAndReturnData($sessionId)
    {
        try {
            $url = $this->getWSurl();
            $soap = new SoapClient($url, array('cache_wsdl' => WSDL_CACHE_NONE));
            $result = $soap->GetTransactionBySessionId(Configuration::get('P24_MERCHANT_ID'), Configuration::get('P24_API_KEY'), $sessionId);

            /**
             * drugi warunek sprawdza czy płatność zrealizowana
             */
            if ($result->error->errorCode > 0 || $result->result->status != 2) {
                return false;
            }

            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

}
