<?php

/**
 * Created by PhpStorm.
 * User: jakub
 * Date: 2016-09-05
 * Time: 12:30
 */
class Extracharge extends ObjectModel
{
    public $id_extracharge;
    public $id_order;
    public $extracharge_amount;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'przelewy24_extracharges',
        'primary' => 'id_extracharge',
        'multilang' => false,
        'fields' => array(
            'id_order' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true ),
            'extracharge_amount' => array( 'type' => self::TYPE_FLOAT, 'validate' => 'isPrice' ),
        )
    );

    public function __construct( $id = null, $id_lang = null )
    {
        parent::__construct( $id, $id_lang );
    }

    public static function getExtrachargeByOrderId( $id_order )
    {
        $extracharge = Db::getInstance()->getRow('
					SELECT `id_extracharge`
					FROM `'._DB_PREFIX_.bqSQL( Extracharge::$definition['table']).'`
					WHERE `id_order` = '.(int)$id_order
        );

        if( $extracharge['id_extracharge'] )
            return new Extracharge( (int)$extracharge['id_extracharge'] );

    }
}