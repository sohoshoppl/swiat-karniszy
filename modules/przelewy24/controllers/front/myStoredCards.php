<?php

/**
 * Created by KrzysztofZ on 2015-04-23
 */
require_once(dirname(_PS_MODULE_DIR_) . '/modules/przelewy24/class_przelewy24.php');

class Przelewy24MyStoredCardsModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $ssl = true;

    public function init()
    {
        $this->page_name = 'mystoredcards';
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();

        global $cart;
        $smarty = $this->context->smarty;
        $cartId = Context::getContext()->cart->id;
        if (is_null($cartId)) {

           $cart = Context::getContext()->cart->add();
            $cartId = $cart->id;
            Context::getContext()->cookie->__set('id_cart', Context::getContext()->cart->id);
        }
        $p24_session_id = $cartId."|".md5(time());

		// zapis formularza
		if (Tools::isSubmit('submit')) {
			Db::getInstance()->Execute("REPLACE INTO "._DB_PREFIX_."przelewy24_customersettings (`customer_id`, `cc_forget`) ".
				   "VALUES (".Context::getContext()->customer->id.", '".((int)Tools::getValue('ccards_forget') ? '1' : '0')."')");
			// ewentualne usunięcie zapisanych kart
			if ((int)Tools::getValue('ccards_forget')) {
				Db::getInstance()->ExecuteS(
					' DELETE FROM '._DB_PREFIX_.'p24_recuring '.
					' WHERE customer_id='.Context::getContext()->customer->id
				);
			}
		}

		// odczyt ustawień
		$ccards_forget = Db::getInstance()->getRow(
				' SELECT cc_forget '.
				' FROM '._DB_PREFIX_.'przelewy24_customersettings '.
				' WHERE customer_id='.Context::getContext()->customer->id
				);
		$smarty->assign('ccards_forget', $ccards_forget['cc_forget'] == 1);


		if (Configuration::get('P24_ONECLICK_ENABLED')==1) {

            $ccards = Db::getInstance()->ExecuteS(' SELECT id, mask, card_type, expires ' . ' FROM ' . _DB_PREFIX_ . 'p24_recuring ' . ' WHERE customer_id=' . Context::getContext()->customer->id . ' AND expires >= ' . date("ym"));
           $smarty->assign('ccards', $ccards);

            $smarty->assign('p24_session_id', $p24_session_id);
            $protocol = Przelewy24::getHttpProtocol();

            $smarty->assign('base_url', $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__);
           $smarty->assign('p24_ajax_url', $this->context->link->getModuleLink('przelewy24', 'paymentAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1));
           $smarty->assign('p24_url_status', $this->context->link->getModuleLink('przelewy24', 'paymentStatus', array(), Configuration::get('PS_SSL_ENABLED') == 1));
           $smarty->assign('clientId',Context::getContext()->customer->id);
           $this->setTemplate('myStoredCardsList.tpl');
        }
    }
}
