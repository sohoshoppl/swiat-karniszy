<?php

/**
 * Created by michalz on 19.03.14
 */
class Przelewy24PaymentFailedModuleFrontController extends ModuleFrontController {
    public $display_column_left = false;
    public $display_column_right = false;
	public $ssl = true;

    public function initContent() {
        global $smarty, $cart;
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();

		$przelewy24 = new Przelewy24();
		$currency = $przelewy24->getCurrencyCode($cart->id_currency);
		$c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_'.$currency;

		$smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array(), Configuration::get('PS_SSL_ENABLED') == 1));

		if (Configuration::get('P24_VERIFYORDER'.$c_sufix) > 0) {
			// wykonano zamówienie, ale brak płatności - znajdź ostatnie zamówienie tego klienta
			$customerOrderList = Order::getCustomerOrders($this->context->customer->id);
			
			if (is_array($customerOrderList)) foreach ($customerOrderList as $item) {
                $customer = new Customer((int)($item['id_customer']));
				$smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array('order_id'=>$item['id_order'], 'token'=>$customer->secure_key), Configuration::get('PS_SSL_ENABLED') == 1));
				break;
			}
		}

		if (!empty($_GET['order_id'])) {
            $order = new Order((int)$_GET['order_id']);
            $customer = new Customer((int)($order->id_customer));
			$smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array('order_id'=>$_GET['order_id'], 'token'=>$customer->secure_key), Configuration::get('PS_SSL_ENABLED') == 1));
		}
		
        if (!empty($_POST)) {

			list($cartID,$sa_sid) = explode('|', $_POST["p24_session_id"], 2);

			$cartID = (int)$cartID;
			$sa_sid = pSQL($sa_sid);

            $o_order = Db::getInstance()->getRow('SELECT `i_id_order`,`i_amount` FROM `'._DB_PREFIX_.'przelewy24_amount` WHERE `s_sid`="'.$sa_sid.'"');
            $p24_order_id = Order::getOrderByCartId(intval($o_order['i_id_order']));
            $order = new Order((int)$_GET['order_id']);
            $customer = new Customer((int)($order->id_customer));
			$smarty->assign('p24_retryPaymentUrl', $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', array('order_id'=>$p24_order_id,'token'=>$customer->secure_key), Configuration::get('PS_SSL_ENABLED') == 1));
        }
        $przelewy24 = new Przelewy24();
		$smarty->assign('ga_key', $przelewy24->validateGA(Configuration::get('P24_GA_KEY'.$c_sufix)));

		$smarty->assign('url_history', $this->context->link->getPageLink('history', true));

        $this->setTemplate('paymentFailed.tpl');
    }
}
