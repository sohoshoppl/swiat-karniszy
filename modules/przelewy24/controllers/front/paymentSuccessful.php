<?php

/**
 * Created by michalz on 19.03.14
 * Modified by AdamM on 28.10.2014
 */
class Przelewy24PaymentSuccessfulModuleFrontController extends ModuleFrontController {
    public $display_column_left = false;
    public $display_column_right = false;
	public $ssl = true;

    public function initContent() {
        global $smarty ;
        $this->display_column_left = false;
        $this->display_column_right = false;
        parent::initContent();

        $przelewy24 = new Przelewy24();

		$ga_cart_id = (int)Tools::getValue('ga_cart_id');
		if ( $ga_cart_id > 0) {
			$ga_key = $przelewy24->validateGA(Configuration::get('P24_GA_KEY'));
			$smarty->assign('ga_key', $ga_key);
			try {
				$cart = new Cart($ga_cart_id);

				$order_id = Order::getOrderByCartId(intval($cart->id));
				$order = new Order($order_id);

				$transaction_id = Configuration::get("P24_ORDER_TITLE_ID")==1 ? $order->reference : $order_id;

				$przelewy24 = new Przelewy24();
				$currency = $przelewy24->getCurrencyCode($cart->id_currency);

				$address = new Address($cart->id_address_delivery);

				$ga_conversion = array(
				    array('_setAccount', $ga_key),
				    array('_trackPageview'),
					array('_set', 'currencyCode', $currency ),
					array('_addTrans',
							$transaction_id,
							Configuration::get('PS_SHOP_NAME') ,
							$cart->getOrderTotal(Configuration::get('P24_GA_KEY_TAX')==1, Cart::BOTH_WITHOUT_SHIPPING),
							$cart->getOrderTotal(true) - $cart->getOrderTotal(false),
							$cart->getPackageShippingCost(),
							$address->city,
							$address->state,
							$address->country,
					),
				);
				$products = $cart->getProducts();
				if (is_array($products)) {
					foreach ($products as $product) {
						$ga_conversion[] = array(
							'_addItem',
							$transaction_id,
							$product['id_product'] . '-' . $product['id_product_attribute'],
							$product['name'],
							$product['id_customization'],
							$product['price'],
							$product['quantity'],
						);
					}
				}
				$ga_conversion[] = array('_trackTrans');
				$smarty->assign('ga_conversion', $ga_conversion );

				$smarty->assign(array(
					'HOOK_ORDER_CONFIRMATION' => $this->displayOrderConfirmation($order_id),
					'HOOK_PAYMENT_RETURN' => $this->displayPaymentReturn($order_id)
				));

				$currencyObj = new Currency($order->id_currency);
				
			} catch (Exception $e) {
				error_log($e->getMessage());
			}
		}
        $this->setTemplate('paymentSuccessful.tpl');
    }
	/**
	 * Execute the hook displayPaymentReturn
	 */
	public function displayPaymentReturn($order_id)
	{
		$id_module = (int)(Tools::getValue('id_module', 0));
		if (Validate::isUnsignedId($order_id) && Validate::isUnsignedId($id_module))
		{
			$params = array();
			$order = new Order($order_id);
			$currency = new Currency($order->id_currency);

			if (Validate::isLoadedObject($order))
			{
				$params['total_to_pay'] = $order->getOrdersTotalPaid();
				$params['currency'] = $currency->sign;
				$params['objOrder'] = $order;
				$params['currencyObj'] = $currency;

				return Hook::exec('displayPaymentReturn', $params, $id_module);
			}
		}
		return false;
	}

	/**
	 * Execute the hook displayOrderConfirmation
	 */
	public function displayOrderConfirmation($order_id)
	{
		if (Validate::isUnsignedId($order_id))
		{
			$params = array();
			$order = new Order($order_id);
			$currency = new Currency($order->id_currency);

			if (Validate::isLoadedObject($order))
			{
				$params['total_to_pay'] = $order->getOrdersTotalPaid();
				$params['currency'] = $currency->sign;
				$params['objOrder'] = $order;
				$params['currencyObj'] = $currency;

				return Hook::exec('displayOrderConfirmation', $params);
			}
		}
		return false;
	}
}