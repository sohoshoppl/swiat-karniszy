<?php

/**
 * Created by AdamM on 28.10.2014
 */
require_once(dirname(_PS_MODULE_DIR_) . '/modules/przelewy24/class_przelewy24.php');

class Przelewy24PaymentStatusModuleFrontController extends ModuleFrontController
{

    public $ssl = true;

    private $debug;
    private $extrachargeAmount = 0;

    public function initContent()
    {
        if ($_POST['p24_session_id'] != null) {

            $przelewy24 = new Przelewy24();

            list($cartID, $sa_sid) = explode('|', $_POST["p24_session_id"], 2);
            $cartID = (int)$cartID;
            $sa_sid = pSQL($sa_sid);

            $cart = new Cart($cartID);

            Context::getContext()->currency = Currency::getCurrencyInstance((int)$cart->id_currency);

            $przelewy24 = new Przelewy24();
            $currency = $przelewy24->getCurrencyCode(intval($cart->id_currency));
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            $this->setDebug($c_sufix); // set debugging mode based on configuration

            $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix),
                Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix),
                Configuration::get('P24_TEST_MODE' . $c_sufix));

            $this->extrachargeAmount = $przelewy24->getExtrachargeAmount($cart->id);

            $this->logDebugData(array(
                'version' => 'v.' . $przelewy24->version,
                'testConnection' => $P24C->testConnection(),
                'merchantId' => Configuration::get('P24_MERCHANT_ID' . $c_sufix),
                'shopId' => Configuration::get('P24_SHOP_ID' . $c_sufix)
            ));

            $this->logDebugData($_POST);

            $order_id = Order::getOrderByCartId($cart->id);

            $o_order = Db::getInstance()->getRow('SELECT `i_id_order`,`i_amount` FROM `' . _DB_PREFIX_ . 'przelewy24_amount` WHERE `s_sid`="' . $sa_sid . '"');

            if( $order_id )
            {
                // if order exists take price from order
                $amount_from = 'amount_from_order';
                $order = new Order($order_id);
                $amount = $this->p24GetOrderTotalPriceGross($order);
            }
            else
            {
                $extracharge_amount = $przelewy24->getExtrachargeAmount( (int)$cart->id );

                // else take prise from cart
                $cart_amount = $cart->getOrderTotal() + $extracharge_amount;

                $customer = new Customer((int)($cart->id_customer));
                $przelewy24->validateOrder(
                    $cart->id,
                    (int)Configuration::get('P24_ORDER_STATE_1'), // order beginning state
                    $cart_amount,
                    'Przelewy24',
                    null,
                    array(),
                    null,
                    false,
                    $customer->secure_key);

                $order_id = Order::getOrderByCartId($cart->id);
                $order = new Order($order_id);
                $przelewy24->addExtrachargeToOrder($cart, $order);


                $amount = $this->p24GetOrderTotalPriceGross($order);

                $amount_from = 'amount_from_cart';
                $this->logDebugData(array(
                    'cart_getOrderTotal' => $cart->getOrderTotal() + $extracharge_amount
                ));
            }

            $this->logDebugData(array(
                'sa_sid' => $sa_sid,
                'o_order' => $o_order,
                $amount_from => $amount
            ));

            if ($_POST['action'] == 'trnVerify' && !empty($_POST['p24_order_id'])) {

                $_POST['p24_merchant_id'] = Configuration::get('P24_MERCHANT_ID' . $c_sufix);
                $_POST['p24_pos_id'] = Configuration::get('P24_SHOP_ID' . $c_sufix);
                $_POST['p24_amount'] = $amount;
                $_POST['p24_currency'] = $currency;
                $_POST['p24_method'] = 142;

                $_POST['p24_sign'] = md5($_POST['p24_session_id'] . '|' . $_POST['p24_order_id'] . '|' . $amount . '|' . $currency . '|' . Configuration::get('P24_SALT' . $c_sufix));
            }

            $validation = array('p24_amount' => $amount, 'p24_currency' => $currency);
            $WYNIK = $P24C->trnVerifyEx($validation);

            $order_id = Order::getOrderByCartId($cart->id);

            $this->logDebugData(array(
                'validation' => $validation,
                'result' => $WYNIK,
            ));

            if ($WYNIK === null) {
                exit("\n" . 'MALFORMED POST DATA');
            } elseif ($WYNIK === true) {
                if ($order_id) {

                    // checks if order wasn't already confirmed
                    if ($this->checkIfOrderAlreadyConfirmed($order_id)) {
                        exit("\n" . 'OK');
                    }

                    $order = new Order($order_id);
                    $_POST['p24_method'] = intval($_POST['p24_method']);

                    // changes state to confirmed or informs if cart content was changed between redirection to p24 and back to shop
                    $this->checkCartAndSetProperOrderState($order_id);

                    /* zapis wybranej metody płatności - do statystyk kolejności */
                    Db::getInstance()->Execute('UPDATE `' . _DB_PREFIX_ . 'przelewy24_amount` SET p24_method = ' . $_POST['p24_method'] . ' WHERE `s_sid`="' . $sa_sid . '"');

                    if (in_array($_POST['p24_method'], array(140, 142, 145))) {
                        $this->addCard($order->id_customer, intval($_POST['p24_order_id']), $_POST['p24_currency']);
                        Db::getInstance()->Execute("DELETE FROM " . _DB_PREFIX_ . "przelewy24_lastmethod WHERE customer_id = {$order->id_customer} LIMIT 1");
                    } else {
                        Db::getInstance()->Execute("REPLACE INTO " . _DB_PREFIX_ . "przelewy24_lastmethod (customer_id, p24_method) VALUES ( {$order->id_customer}, {$_POST['p24_method']} )");
                    }
                }
                Hook::exec('actionPaymentConfirmation', array('id_order' => $order->id));
                exit("\n" . 'OK');
            } else {
                if ($order_id) {
                    $this->setOrderState($order_id, Configuration::get('PS_OS_ERROR'));
                }
                exit("\n" . 'ERROR');
            }
        } else {
            exit('INVALID_SESSION_ID');
        }
    }

    private function addCard($uid, $oid, $currency)
    {

        $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

        if ($uid > 0 && Configuration::get('P24_ONECLICK_ENABLED' . $c_sufix) && strlen(Configuration::get('P24_API_KEY' . $c_sufix)) == 32) {

            $ccards_forget = Db::getInstance()->getRow('SELECT cc_forget FROM ' . _DB_PREFIX_ . 'przelewy24_customersettings WHERE customer_id=' . $uid);
            if ((int)$ccards_forget['cc_forget'] == 1) {
                return;
            } // nie zapamiętuj karty - Customer sobie nie życzy

            try {
                $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix),
                    Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix),
                    Configuration::get('P24_TEST_MODE' . $c_sufix));
                $przelewy24 = new Przelewy24();
                $s = new SoapClient($P24C->getHost() . $przelewy24->getWsdlCCService(),
                    array('trace' => true, 'exceptions' => true));

                $res = $s->GetTransactionReference(Configuration::get('P24_SHOP_ID' . $c_sufix),
                    Configuration::get('P24_API_KEY' . $c_sufix), $oid);
                if ($res->error->errorCode === 0) {
                    $ref = $res->result->refId;
                    $exp = substr($res->result->cardExp, 2, 2) . substr($res->result->cardExp, 0, 2);
                    if (!empty($ref)) {
                        $hasRecurency = $s->CheckCard(Configuration::get('P24_SHOP_ID' . $c_sufix),
                            Configuration::get('P24_API_KEY' . $c_sufix), $ref);
                        if ($hasRecurency->error->errorCode === 0 && $hasRecurency->result == true) {
                            if (date('ym') <= $exp) {
                                Db::getInstance()->Execute("REPLACE INTO " . _DB_PREFIX_ . "p24_recuring (`website_id`, `customer_id`, `reference_id`, `expires`, `mask`, `card_type`) " .
                                    "VALUES (1, " . $uid . ", '" . $ref . "', '" . $exp . "', '" . $res->result->mask . "', '" . $res->result->cardType . "')");
                                Hook::exec('actionPaymentCCAdd');
                            } else {
                                $msg = __METHOD__ . ' termin ważności ' . var_export($exp, true);
                                Logger::addLog($msg, 1);
                                error_log($msg);
                            }
                        } else {
                            $msg = __METHOD__ . ' karta nie ma rekurencji ' . var_export($hasRecurency, true);
                            Logger::addLog($msg, 1);
                            error_log($msg);
                        }
                    }
                }
            } catch (Exception $e) {
                error_log(__METHOD__ . ' ' . $e->getMessage());
            }
        }
    }

    private function setOrderState($order_id, $state)
    {
        $order = new Order($order_id);
        $history = new OrderHistory();
        $history->id_order = intval($order_id);
        $history->changeIdOrderState($state, intval($order_id));
        $history->addWithemail(true);

        if (method_exists($order, 'getOrderPaymentCollection')) {
            $payments = $order->getOrderPaymentCollection();
            if (count($payments) > 0) {
                $payments[0]->transaction_id = $_POST["p24_order_id"];
                $payments[0]->update();
            }
        }
    }

    private function checkCartAndSetProperOrderState($order_id)
    {
        $przelewy24 = new Przelewy24();
        $cart = Cart::getCartByOrderId($order_id);
        if ($cart) {
            $extracharge_amount = $przelewy24->getExtrachargeAmount( (int)$cart->id );
            $cart_total = number_format( ( ( $cart->getOrderTotal() * 100 ) + $extracharge_amount  ), 0, '', '' );
            $p24_amount = number_format($_POST['p24_amount'], 0, '', '');

            if ($cart_total !== $p24_amount) {
                $this->setOrderState($order_id, Configuration::get('P24_ORDER_STATE_3'));
            } else {
                $this->setOrderState($order_id, Configuration::get('P24_ORDER_STATE_2'));
            }
        } else {
            $this->setOrderState($order_id, Configuration::get('P24_ORDER_STATE_2'));
        }
    }

    private function checkIfOrderAlreadyConfirmed($order_id)
    {
        $query = "SELECT id_order_state FROM " . _DB_PREFIX_ . "order_history WHERE id_order=" . (int)$order_id . " ORDER BY date_add desc";
        $id_order_state = Db::getInstance()->getValue($query);

        if ($id_order_state == Configuration::get('P24_ORDER_STATE_2')) {
            return true;
        }
        return false;
    }

    /**
     * @param array $array
     * @param bool $toDatabase
     */
    private function logDebugData($array)
    {
        if ($this->debug) {
            print_r($array);
        }
    }

    private function setDebug($c_sufix)
    {
        (Configuration::get('P24_DEBUG' . $c_sufix) &&
            isset($_GET['test']) &&
            $_GET['test'] == md5('p24' . Configuration::get('P24_SALT' . $c_sufix) . 'debug'))
            ? $this->debug = true : $this->debug = false;
    }

    private function p24GetOrderTotalPriceGross($order)
    {
        return number_format($order->total_paid * 100, 0, '', '');
    }
}