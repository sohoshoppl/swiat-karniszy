<?php

/**
 * Created by michalz on 14.03.14
 * Modified by krzsztofz on 2015-04-23
 */
class Przelewy24PaymentAjaxModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $auth = false;

    public static function requestGet($url)
    {
        $isCurl = function_exists('curl_init') && function_exists('curl_setopt') && function_exists('curl_exec') && function_exists('curl_close');

        if ($isCurl) {
            $userAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
            $curlConnection = curl_init();
            curl_setopt($curlConnection, CURLOPT_URL, $url);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlConnection, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curlConnection);
            curl_close($curlConnection);
            return $result;
        }
        return "";
    }

    public function initContent()
    {

        if ($_POST['action'] == 'setOrder') {

            /* zwraca id banków wg kolejności najczęściej używanych */
            $przelewy24 = new Przelewy24();
            $availablePaymentMethods = $przelewy24->availablePaymentMethods();
            $allMethods = array();
            foreach ($availablePaymentMethods as $bank) {
                $allMethods[$bank->id] = $bank->name;
            }

            $order = array();
            $most_used = Db::getInstance()->ExecuteS('SELECT p24_method, count(*) as ile FROM ps_przelewy24_amount WHERE p24_method IS NOT NULL GROUP BY p24_method order by ile DESC');
            foreach ($most_used as $row) {
                if (isset($allMethods[$row['p24_method']])) {
                    $order[] = (int)$row['p24_method'];
                    unset($allMethods[$row['p24_method']]);
                }
            }

            foreach ($allMethods as $id => $name) {
                $order[] = (int)$id;
            }
            exit(json_encode($order));
        } elseif ($_POST['action'] == 'runIVR') {


            $przelewy24 = new Przelewy24();
            $cart = Cart::getCartByOrderId($_POST['id_order']);

            $currency = $przelewy24->getCurrencyCode($cart->id_currency);
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));


            $order = new Order($_POST['id_order']);
            $customer = new Customer((int)($cart->id_customer));
            $address = new Address((int)$order->id_address_delivery);

            $amount = round($przelewy24->getP24Amount(intval($cart->id_currency), $cart->getOrderTotal(true, Cart::BOTH)));
            $url_status = $this->context->link->getModuleLink('przelewy24', 'paymentStatus', array(), Configuration::get('PS_SSL_ENABLED') == 1);

            // dodanie ewentualnego BasicAuth do url_status
            $basic_auth = Configuration::get('P24_BASIC_AUTH' . $c_sufix);
            if (!empty($basic_auth)) {
                list($protocol, $url) = explode('://', $url_status, 2);
                $url_status = "{$protocol}://{$basic_auth}@{$url}";
            }

            $s_sid = md5(time());
            $s_lang = new Country((int)($address->id_country));

            Db::getInstance()->Execute('INSERT INTO ' . _DB_PREFIX_ . 'przelewy24_amount (s_sid,i_id_order,i_amount) ' . 'VALUES(\'' . $s_sid . '\',' . $cart->id . ',' . $amount . ')');

            try {
                $s = new SoapClient($P24C->getHost() . $przelewy24->getWsdlService(Configuration::get('P24_MERCHANT_ID' . $c_sufix)), array('trace' => true, 'exceptions' => true));
                if ($przelewy24->soap_method_exists($s, 'TransactionMotoCallBackRegister')) {

                    $clientPhone = (!empty($address->phone_mobile) ? $address->phone_mobile : $address->phone);
                    // usunięcie prefiksu kraju
                    if (strpos($clientPhone, '+48') === 0) {
                        $clientPhone = substr($clientPhone, 3);
                    } elseif (strpos($clientPhone, '0048') === 0) {
                        $clientPhone = substr($clientPhone, 4);
                    } elseif (strpos($clientPhone, '48') === 0) {
                        $clientPhone = substr($clientPhone, 2);
                    }

                    $res = $s->__call('TransactionMotoCallBackRegister', array('login' => Configuration::get('P24_MERCHANT_ID' . $c_sufix), 'pass' => Configuration::get('P24_API_KEY' . $c_sufix), 'details' => array('clientPhone' => $clientPhone, 'amount' => $amount, 'currency' => $currency, 'paymentId' => '#' . $_POST['id_order'], 'description' => 'zamówienie numer ' . $_POST['id_order'], 'sessionId' => $cart->id . '|' . $s_sid, 'clientEmail' => $customer->email, 'merchantEmail' => Configuration::get('PS_SHOP_EMAIL'), 'client' => $customer->firstname . ' ' . $customer->lastname, 'urlStatus' => $url_status, 'typeOfResponse' => 'post', 'sendEmail' => 0, 'time' => 0, 'additionalInfo' => '',)));
                    $msg = 'Uruchomiono IVR na nr ' . $clientPhone;
                    error_log($msg);
                    Logger::addLog(__METHOD__ . ' ' . $msg, 1);
                    if ($res->error->errorCode > 0) {
                        error_log(__METHOD__ . ' ' . $res->error->errorMessage);
                    }
                }
            } catch (Exception $e) {
                error_log(__METHOD__ . ' ' . $e->getMessage());
            }
            exit();
        } elseif ($_POST['action'] == 'cc_forget') {
            $cc_forget = $_POST['value'] == 'true';
            Db::getInstance()->Execute("REPLACE INTO " . _DB_PREFIX_ . "przelewy24_customersettings (`customer_id`, `cc_forget`) " . "VALUES (" . Context::getContext()->customer->id . ", '" . ($cc_forget ? '1' : '0') . "')");
            exit();
        } elseif ($_POST['action'] == 'calculatePart') {
            $result = self::requestGet("https://secure.przelewy24.pl/kalkulator_raty.php?ammount=" . ($_POST['amount'] * 100));
            list($ileRat, $kwotaRaty) = explode('<br>', $result, 2);
            exit(json_encode(array('ileRat' => $ileRat, 'kwotaRaty' => $kwotaRaty)));

        }elseif ($_POST['action'] == 'cardRegisterQuery') {
            $ccToken = $_POST['ccToken'];
            $cardData = trim(substr($_POST['ReceiptText'], 0, strpos($_POST['ReceiptText'], '$')));
            $cardType = trim(substr($cardData, 0, strpos($cardData, 'xxxx')));
            $cardNumber = trim(chunk_split(trim(str_replace($cardType, '', $cardData)), 4, ' ') );
            $query = "INSERT INTO "._DB_PREFIX_."p24_recuring (website_id,customer_id,reference_id,expires,mask,card_type) VALUES ('".(int)Context::getContext()->shop->id."','".Context::getContext()->customer->id."','".$ccToken."','".$_POST['expires']."','".$cardNumber."','".$cardType."' ) ";

            if(Db::getInstance()->Execute($query)){
                exit(json_encode(array('status'=>true)));
            }else{
                exit(json_encode(array('status'=>false)));
            }
//            exit(json_encode(array('query'=>$query,'ccToken' =>$ccToken,'cardType'=>$cardType,'cardNumber'=>$cardNumber,'expires'=>$_POST['expires'])));

        } elseif ($_POST['action'] == 'makeOrder') {
            $cart = new Cart((int)$_POST['cart_id']);
            $customer = new Customer((int)($cart->id_customer));

            $przelewy24 = new Przelewy24();
            $currency = $przelewy24->getCurrencyCode($cart->id_currency);
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            if ($cart && Configuration::get('P24_VERIFYORDER') == 2) {

                if ($cart->OrderExists() == null) {
                    $amount = $cart->getOrderTotal(true, Cart::BOTH);
                    $amount = $przelewy24->getP24Amount(intval($cart->id_currency), $amount);

                    $orderBeginingState = Configuration::get('P24_ORDER_STATE_1');
                    $przelewy24->validateOrder($cart->id, (int)$orderBeginingState, floatval($amount / 100),
                        'Przelewy24', null, array(), null, false, $customer->secure_key);
                }

                $order_id = Order::getOrderByCartId($cart->id);
                $result = $order_id;
                if (Configuration::get("P24_ORDER_TITLE_ID") == 1) {
                    $order = new Order($order_id);
                    $result = $order->reference;
                }

                exit(json_encode(array('order_id' => $result, 'description' => "Zamówienie: " . $result)));
            }
            exit(json_encode(array()));
        } elseif ($_POST['action'] == 'trnRegister' && !empty($_POST['p24_session_id'])) {
            if (isset($_POST['order_id'])) {
                $cart = Cart::getCartByOrderId($_POST['order_id']);
                if ($cart == null) {
                    die();
                }

                $order = new Order($_POST['order_id']);
                if (Configuration::get("P24_ORDER_TITLE_ID") == 1) {
                    $s_descr = 'Zamówienie ' . $order->reference;
                    $id_zamowienia = $order->reference;
                } else {
                    $s_descr = 'Zamówienie ' . $_POST['order_id'];
                }

            } else {
                global $cart;
                $s_descr = "Koszyk: " . $cart->id;
            }

            $przelewy24 = new Przelewy24();
            $currency = $przelewy24->getCurrencyCode($cart->id_currency);
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            $amount = $cart->getOrderTotal(true, Cart::BOTH);
            $amount = $przelewy24->getP24Amount(intval($cart->id_currency), $amount);
            $extracharge_amount = $przelewy24->getExtrachargeAmount($cart->id);
            if ($extracharge_amount > 0) {
                $amount = $amount + $extracharge_amount;
            }

            $customer = new Customer((int)($cart->id_customer));
            $address = new Address((int)$cart->id_address_invoice);

            $s_lang = new Country((int)($address->id_country));

            $url_status = $this->context->link->getModuleLink('przelewy24', 'paymentStatus', array(), Configuration::get('PS_SSL_ENABLED') == 1);

            // dodanie ewentualnego BasicAuth do url_status
            $basic_auth = Configuration::get('P24_BASIC_AUTH' . $c_sufix);
            if (!empty($basic_auth)) {
                list($protocol, $url) = explode('://', $url_status, 2);
                $url_status = "{$protocol}://{$basic_auth}@{$url}";
            }

            $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));
            $post_data = array('p24_merchant_id' => Configuration::get('P24_MERCHANT_ID' . $c_sufix), 'p24_pos_id' => Configuration::get('P24_SHOP_ID' . $c_sufix), 'p24_session_id' => $_POST['p24_session_id'], 'p24_amount' => $amount, 'p24_currency' => $currency, 'p24_description' => $s_descr, 'p24_email' => $customer->email,

                'p24_client' => $customer->firstname . ' ' . $customer->lastname, 'p24_address' => $address->address1 . " " . $address->address2, 'p24_zip' => $address->postcode, 'p24_city' => $address->city, 'p24_country' => $s_lang->iso_code, 'p24_language' => strtolower(Language::getIsoById($cart->id_lang)), 'p24_url_return' => $this->context->link->getModuleLink('przelewy24', 'paymentSuccessful', array(), Configuration::get('PS_SSL_ENABLED') == 1), 'p24_url_status' => $url_status, 'p24_url_cancel' => $this->context->link->getModuleLink('przelewy24', 'paymentFailed', array(), Configuration::get('PS_SSL_ENABLED') == 1), 'p24_api_version' => P24_VERSION, 'p24_ecommerce' => 'PrestaShop ' . _PS_VERSION_, 'p24_shipping' => (float)$cart->getPackageShippingCost((int)$cart->id_carrier) * 100);

            $products = $this->getP24Products($cart, $amount);
            $post_data = array_merge($post_data, $products);

            foreach ($post_data as $k => $v) {
                $P24C->addValue($k, $v);
            }
            $token = $P24C->trnRegister();
            if (is_array($token)) {
                $token = $token['token'];
                exit(json_encode(array('p24jsURL' => $P24C->getHost() . 'inchtml/ajaxPayment/ajax.js?token=' . $token, 'p24cssURL' => $P24C->getHost() . 'inchtml/ajaxPayment/ajax.css',)));
            }
            exit();
        } elseif ($_POST['action'] == 'cardRegister' && !empty($_POST['p24_session_id'])) {
            global $cart;
            $s_descr = "Rejestracja karty: " . $_POST['p24_session_id'];
            $przelewy24 = new Przelewy24();
            $currency = $przelewy24->getCurrencyCode($cart->id_currency);
            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_' . $currency;

            $amount = 0;

            $customer = new Customer((int)($cart->id_customer));
            $address = new Address((int)$cart->id_address_invoice);

            $s_lang = new Country((int)($address->id_country));

            $url_status = $this->context->link->getModuleLink('przelewy24', 'paymentStatus', array(), Configuration::get('PS_SSL_ENABLED') == 1);

            // dodanie ewentualnego BasicAuth do url_status
            $basic_auth = Configuration::get('P24_BASIC_AUTH' . $c_sufix);
            if (!empty($basic_auth)) {
                list($protocol, $url) = explode('://', $url_status, 2);
                $url_status = "{$protocol}://{$basic_auth}@{$url}";
            }

            $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID' . $c_sufix), Configuration::get('P24_SHOP_ID' . $c_sufix), Configuration::get('P24_SALT' . $c_sufix), Configuration::get('P24_TEST_MODE' . $c_sufix));
            $post_data = array(
                'p24_merchant_id' => Configuration::get('P24_MERCHANT_ID' . $c_sufix),
                'p24_pos_id' => Configuration::get('P24_SHOP_ID' . $c_sufix),
                'p24_session_id' => $_POST['p24_session_id'],
                'p24_amount' => $amount,
                'p24_currency' => $currency,
                'p24_description' => $s_descr,
                'p24_email' => $customer->email,
                'p24_client' => $customer->firstname . ' ' . $customer->lastname,
                'p24_address' => $address->address1 . " " . $address->address2,
                'p24_zip' => $address->postcode,
                'p24_city' => $address->city,
                'p24_country' => $s_lang->iso_code,
                'p24_language' => strtolower(Language::getIsoById($cart->id_lang)),
                'p24_url_return' => $this->context->link->getModuleLink('przelewy24','paymentSuccessful', array(), Configuration::get('PS_SSL_ENABLED') == 1),
                'p24_url_status' => $url_status,
                'p24_url_cancel' => $this->context->link->getModuleLink('przelewy24', 'paymentFailed', array(), Configuration::get('PS_SSL_ENABLED') == 1),
                'p24_api_version' => P24_VERSION,
                'p24_ecommerce' => 'PrestaShop ' . _PS_VERSION_,
                'p24_shipping' => (float)$cart->getPackageShippingCost((int)$cart->id_carrier) * 100
            );

            $products = $this->getP24Products($cart, $amount);
            $post_data = array_merge($post_data, $products);

            foreach ($post_data as $k => $v) {
                $P24C->addValue($k, $v);
            }
            $token = $P24C->trnRegister();
            if (is_array($token)) {
                $sign = $token['sign'];
                $token = $token['token'];
                exit(json_encode(array('p24jsURL' => $P24C->getHost() . 'inchtml/card/register_card/ajax.js?token=' . $token, 'p24cssURL' => $P24C->getHost() . 'inchtml/card/register_card/ajax.css','p24sign' => $sign)));
            }
            exit();
        }
    }

    /**
     * @param Cart $cart
     * @param $amount
     *
     * @return array
     */
    public function getP24Products(Cart $cart, $amount)
    {
        $productsInfo = array();
        foreach ($cart->getProducts() as $key => $product) {
            $productsInfo[] = array('name' => $product['name'], 'description' => $product['description_short'], 'quantity' => (int)$product['cart_quantity'], 'price' => (int)($product['price'] * 100), 'number' => $product['id_product'],);
        }
        $shipping = $cart->getPackageShippingCost((int)$cart->id_carrier) * 100;
        $translations = array('virtual_product_name' => $this->module->l('Extra charge [VAT and discounts]'), 'cart_as_product' => $this->module->l('Your order'));
        $p24Product = new Przelewy24Product($translations);
        $p24ProductItems = $p24Product->prepareCartItems($amount, $productsInfo, $shipping);

        return $p24ProductItems;
    }
}