<?php

/**
 * Created by adamm on 07.01.15
 */
class Przelewy24PaymentRecurringModuleFrontController extends ModuleFrontController {
	public $auth = true;
	public $ssl = true;
    public function initContent() {
        if (Context::getContext()->customer->isLogged()) {
            $this->uid=Context::getContext()->customer->id;
            if (isset($_GET['cardrm']) && is_numeric($_GET['cardrm']) && $_GET['cardrm'] > 0) $this->removeCard(intval($_GET['cardrm']));
            else $this->chargeCard();
        }
        exit;
    }
    private function removeCard($cc) {
        $cc=intval($cc);
        if ($cc > 0) {
            Db::getInstance()->Execute("DELETE FROM `"._DB_PREFIX_."p24_recuring` WHERE `website_id`=1 AND `id`=".$cc." AND `customer_id`=".$this->uid);
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }
    private function chargeCard() {
        if ($_POST['p24_session_id']!=null) {
            list($cartID,$sa_sid) = explode('|', $_POST["p24_session_id"], 2);
			$cartID = (int)$cartID;
			$sa_sid = pSQL($sa_sid);
				
            $o_order = Db::getInstance()->getRow('SELECT `i_id_order`,`i_amount` FROM `'._DB_PREFIX_.'przelewy24_amount` WHERE `s_sid`="'.$sa_sid.'"');
            $p24_kwota = (int)$o_order['i_amount'];
            $i_id_order = $o_order['i_id_order'];
            
            $cart = new Cart(intval($i_id_order));

            $przelewy24 = new Przelewy24();
            $currencies = $przelewy24->getCurrency(intval($cart->id_currency));
            $currency = '';
            foreach ($currencies as $c) {
                if ($c['id_currency']==$cart->id_currency) {
                    $currency = $c['iso_code'];
                }
            }

            $c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_'.$currency;

            $rr=Db::getInstance()->getRow('SELECT `reference_id` FROM '._DB_PREFIX_.'p24_recuring WHERE id='.intval($_POST['p24_cc']).' AND customer_id='.$this->uid);
            $ref=$rr['reference_id'];
            $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID'.$c_sufix), Configuration::get('P24_SHOP_ID'.$c_sufix), Configuration::get('P24_SALT'.$c_sufix), Configuration::get('P24_TEST_MODE'.$c_sufix));
			try {
				$s=new SoapClient($P24C->getHost().$przelewy24->getWsdlCCService(), array('trace'=>true, 'exceptions'=>true));
				$res=$s->ChargeCard(Configuration::get('P24_SHOP_ID'.$c_sufix),Configuration::get('P24_API_KEY'.$c_sufix),$ref,$p24_kwota,$currency,$_POST['p24_email'],$_POST['p24_session_id'],$_POST['p24_client'],$_POST['p24_description']);
			} catch (Exception $e) {
				if ($debug) echo $e->getMessage();
				error_log(__METHOD__.' '.$e->getMessage());
			}

            if ($cart->OrderExists() == null) {

				$customer = new Customer((int)($cart->id_customer));
                $przelewy24->validateOrder($cart->id, Configuration::get('P24_ORDER_STATE_1'), floatval($p24_kwota/100), 'Przelewy24', NULL, array(), NULL, false, $customer->secure_key);
            } else {
                if($debug) echo "\n".' HAD BASKET '."\n";
				error_log(__METHOD__.' HAD BASKET ');
            }

            $order_id = Order::getOrderByCartId(intval($i_id_order));
            $order = new Order($order_id);
            
            $history = new OrderHistory();

            $history->id_order = intval($order_id);

            $rq = Db::getInstance()->getRow('SELECT `id_order_state` FROM `'._DB_PREFIX_.'order_state_lang`	WHERE id_lang = \''.pSQL('1').'\' AND  template = \''.pSQL('payment_error').'\'');
            $order_state = $rq['id_order_state'];
            if($res->error->errorCode==0) {
                $order_state = Configuration::get('P24_ORDER_STATE_2');
            }
            $history->changeIdOrderState($order_state, intval($order_id));
            $history->addWithemail(true);
            try {
                $payments = $order->getOrderPaymentCollection();
                if (count($payments) > 0) {
                        $payments[0]->transaction_id = $_POST["p24_order_id"];
                        $payments[0]->update();
                }
            } catch (Exception $e) {}
            if($res->error->errorCode==0) {
				Db::getInstance()->Execute("DELETE FROM "._DB_PREFIX_."przelewy24_lastmethod WHERE customer_id = {$order->id_customer} LIMIT 1");
                header('Location: ' . $this->context->link->getModuleLink('przelewy24', 'paymentSuccessful', array(), Configuration::get('PS_SSL_ENABLED') == 1));
                Hook::exec('actionPaymentConfirmation', array('id_order' => $order->id));
            } else {
                header('Location: ' . $this->context->link->getModuleLink('przelewy24', 'paymentFailed', array(), Configuration::get('PS_SSL_ENABLED') == 1));
            }
        }
    }
}
