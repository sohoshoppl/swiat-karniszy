<?php
/**
 * Created by michalz on 13.03.14
 * Modified by AdamM on 28.10.2014
 */
require_once(dirname(_PS_MODULE_DIR_).'/modules/przelewy24/class_przelewy24.php');

class Przelewy24PaymentConfirmationModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;
    public $display_column_right = false;
	public $ssl = true;

    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;

        Db::getInstance()->Execute("DELETE FROM "._DB_PREFIX_."p24_recuring WHERE `expires`<".date('ym'));
    }

    public function initContent()
    {
        parent::initContent();

        $this->display_column_left = false;
        $this->display_column_right = false;

        global $smarty;

       $orderId =  (int)Tools::getValue('order_id', 0);

        if( $orderId > 0)
        {
			$smarty->assign( 'get_order_id', $orderId );
            $cart = Cart::getCartByOrderId( $orderId );
        }
        else
        {
            global $cart;
        }

        if( $cart == null || empty($cart->id) ) {
            Tools::redirect( 'index.php?controller=order&step=1' );
            return;
        }

        $token = (string)Tools::getValue('token', '');
        $customer = new Customer((int)($cart->id_customer));

        if (!empty(Context::getContext()->customer->id) && Context::getContext()->customer->id != $customer->id) {
            Controller::getController('PageNotFoundController')->run();
        }

        if (empty(Context::getContext()->customer->id) && $token != $customer->secure_key) {
            Controller::getController('PageNotFoundController')->run();
        }

        $przelewy24 = new Przelewy24();
		$currency = $przelewy24->getCurrencyCode( $cart->id_currency );
		$c_sufix = ( $currency == 'PLN' || empty( $currency ) ) ? '' : '_' . $currency;

		$address = new Address( (int)$cart->id_address_invoice );
		$s_lang = new Country( (int)( $address->id_country ) );

		$przelewy24->addDiscount( $cart, $currency );
		if( empty( $currency ) )
		{
            Tools::redirect( 'index.php?controller=order&step=1' );
            return;
        }

        $amount = $cart->getOrderTotal( true, Cart::BOTH );

        $classicAmount = Tools::displayPrice($amount);
        $amount = $przelewy24->getP24Amount(intval($cart->id_currency), $amount);
        $order_id = Order::getOrderByCartId( intval( $cart->id ) );
        // <extracharge>
        $smarty->assign('extracharge_amount', 0);
        $extracharge_amount = $przelewy24->getExtrachargeAmount($cart->id);

        if ($extracharge_amount > 0) {
            $smarty->assign('extracharge_enabled', true);
            $smarty->assign('extracharge_amount', Tools::displayPrice(round($extracharge_amount / 100, 2)));
            $amount = $amount + $extracharge_amount;
            $classicAmount = Tools::displayPrice(round($amount / 100, 2));
        }
        // </extracharge>

        // extra discount
        $total_discounts = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
        if ($total_discounts > 0) {
            $smarty->assign('extradiscount_amount', Tools::displayPrice($total_discounts));
        }


        $s_sid = md5(time());

        Db::getInstance()->Execute( 'INSERT INTO '._DB_PREFIX_.'przelewy24_amount (s_sid,i_id_order,i_amount) '.'VALUES(\''.$s_sid.'\','.$cart->id.','.$amount.' )' );

		$payment_method = trim(Tools::getValue('payment_method'));
        $freeOrder = false;
        if ($order_id == null && (float)$amount === (float)0) {
            $freeOrder = true;
        }

		// utworzenie zamówienia, jeśli ma powstać już teraz
        if ($order_id == null && (Configuration::get('P24_VERIFYORDER' . $c_sufix) == 1 || (Configuration::get('P24_MODE_ZENCARD' . $c_sufix) == 1)) || $freeOrder) {
            $orderBeginingState = Configuration::get('P24_ORDER_STATE_1');

            if ($freeOrder) {
                $orderBeginingState = Configuration::get('P24_ORDER_STATE_2');
            }

            $przelewy24->validateOrder($cart->id, (int)$orderBeginingState, floatval($amount / 100), 'Przelewy24', NULL, array(), NULL, false, $customer->secure_key);
            $order_id = Order::getOrderByCartId(intval($cart->id));

            $args = array();
            $args['order_id'] = $order_id;
            $args['token'] = $customer->secure_key;
            if (!empty($payment_method)) {
                $args['payment_method'] = $payment_method;
            }

            header('Location: ' . $this->context->link->getModuleLink('przelewy24', 'paymentConfirmation', $args, Configuration::get('PS_SSL_ENABLED') == 1));
            exit();
        }

        if ((float)$amount === (float)0) {
            $smarty->assign( 'p24FreeOrder', true );
            $smarty->assign('url_history', $this->context->link->getPageLink('history', true));
        }

        if ($order_id == null) {
            $s_descr = $this->module->l("Cart") . ': ' . $cart->id;
            $id_zamowienia = '';
        } else {
            $order = new Order($order_id);
            if (Configuration::get("P24_ORDER_TITLE_ID") == 1) {
                $s_descr = $this->module->l("Order") . ' ' . $order->reference;
                $id_zamowienia = $order->reference;
            } else {
                $s_descr = $this->module->l("Order") . ' ' . $order_id;
            }
        }

        $P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID'.$c_sufix), Configuration::get('P24_SHOP_ID'.$c_sufix), Configuration::get('P24_SALT'.$c_sufix), Configuration::get('P24_TEST_MODE'.$c_sufix));

        $url_status = $this->context->link->getModuleLink('przelewy24', 'paymentStatus', array(), Configuration::get('PS_SSL_ENABLED') == 1);

		// dodanie ewentualnego BasicAuth do url_status
		$basic_auth = Configuration::get('P24_BASIC_AUTH'.$c_sufix);
		if (!empty($basic_auth)) {
			list($protocol,$url) = explode('://', $url_status, 2);
			$url_status = "{$protocol}://{$basic_auth}@{$url}";
		}

        $post_data = array(
            'productsNumber'=>$cart->nbProducts(),
            'p24_classic_amount'=>$classicAmount,
            'p24_id_zamowienia'=>$id_zamowienia,
            'p24_show_summary'=>Configuration::get("P24_SHOW_SUMMARY".$c_sufix),
            'p24_url'=>$P24C->trnDirectUrl(),
            'p24_session_id'=>$cart->id.'|'.$s_sid,
            'p24_merchant_id'=>Configuration::get('P24_MERCHANT_ID'.$c_sufix),
            'p24_pos_id'=>Configuration::get('P24_SHOP_ID'.$c_sufix),
            'p24_email'=>$customer->email,
            'p24_amount'=>$amount,
            'p24_currency'=>$currency,
            'p24_description'=>$s_descr,
            'p24_language'=>strtolower(Language::getIsoById($cart->id_lang)),
            'p24_client'=>$customer->firstname.' '.$customer->lastname,
            'p24_address'=>$address->address1." ".$address->address2,
            'p24_city'=>$address->city,
            'p24_zip'=>$address->postcode,
            'p24_country'=>$s_lang->iso_code,
            'p24_encoding'=>'UTF-8',
            'p24_method'=>$payment_method,
            'p24_ajax_url' => $this->context->link->getModuleLink('przelewy24', 'paymentAjax', array(), Configuration::get('PS_SSL_ENABLED') == 1),
            'p24_url_status'=>$url_status,
            'p24_url_return'=>$this->context->link->getModuleLink('przelewy24', 'paymentSuccessful', array('ga_cart_id' => $cart->id), Configuration::get('PS_SSL_ENABLED') == 1),
            'p24_url_cancel'=>$this->context->link->getModuleLink('przelewy24', 'paymentFailed', array(), Configuration::get('PS_SSL_ENABLED') == 1),
            'p24_api_version'=>P24_VERSION,
            'p24_ecommerce'  => 'PrestaShop '._PS_VERSION_,
            'p24_validationRequired'=>Configuration::get('P24_VERIFYORDER'.$c_sufix),
			'p24_wait_for_result'=>Configuration::get('P24_WAIT_FOR_RESULT'),
			'p24_shipping' => (float)$cart->getPackageShippingCost( (int)$cart->id_carrier ) * 100
        );

        $productsInfo = array();
        foreach ($cart->getProducts() as $key => $product) {
            $productsInfo[] = array(
                'name' => $product['name'],
                'description' => $product['description_short'],
                'quantity' => (int)$product['cart_quantity'],
                'price' => (int)($product['price'] * 100),
                'number' => $product['id_product'],
            );
        }
        $shipping = $cart->getPackageShippingCost((int)$cart->id_carrier) * 100;
        $translations = array(
            'virtual_product_name' => $this->module->l('Extra charge [VAT and discounts]'),
            'cart_as_product' => $this->module->l('Your order')
        );
        $p24Product = new Przelewy24Product($translations);
        $p24ProductItems = $p24Product->prepareCartItems($amount, $productsInfo, $shipping);

        $post_data['p24_sign'] = $P24C->trnDirectSign( $post_data );
        $P24C->checkMandatoryFieldsForAction( $post_data, 'trnDirect' );
        if( Configuration::get( 'P24_ONECLICK_ENABLED'.$c_sufix ) && $this->context->customer->isLogged() )
        {
            $post_data['oneclick'] = true;
            $post_data['p24_recuring_url']=$this->context->link->getModuleLink('przelewy24', 'paymentRecurring', array(), Configuration::get('PS_SSL_ENABLED') == 1);
            $result = Db::getInstance()->ExecuteS('SELECT `id`,`mask`,`card_type` FROM `'._DB_PREFIX_.'p24_recuring` WHERE customer_id='.$customer->id.' AND expires >= ' . date("ym"));
            foreach( $result as $row )
            {
                $post_data['p24_cards'][$row['id']]=sprintf(html_entity_decode($przelewy24->l('Use <b>%s</b> card with number <b>%s</b>')),$row['card_type'],$row['mask']);
                $post_data['p24_cclist'][$row['id']]=array('type' => $row['card_type'], 'mask' => $row['mask']);
            }
        }

        $smarty->assign($post_data);

        $smarty->assign( 'p24ProductItems', $p24ProductItems );
		// checkbox akceptacji wewnątrz sklepu
        if (Configuration::get('P24_ACCEPTINSHOP_ENABLED'.$c_sufix) && $this->context->customer->isLogged()) {
			$smarty->assign('accept_in_shop', true);
			$result = Db::getInstance()->getRow('SELECT EXISTS(SELECT 1 FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$customer->id.' AND payment = \''.$przelewy24->displayName.'\' ) AS exist ');
			if ($result['exist']) {
				$smarty->assign('accept_in_shop', false);
				$smarty->assign('accept_in_shop_accepted', true);
			}
		}

		// ostatnia używana metoda płatności
		$result = Db::getInstance()->getRow('SELECT p24_method FROM '._DB_PREFIX_.'przelewy24_lastmethod WHERE customer_id ='.$customer->id);
		if (!empty($result['p24_method'])) {
			$smarty->assign('last_pay_method', $result['p24_method']);
		}

		$custom_timelimit = Configuration::get('P24_CUSTOM_TIMELIMIT'.$c_sufix);
        if (is_numeric($custom_timelimit) && $custom_timelimit >=0 && $custom_timelimit <= 99) {
			$smarty->assign('custom_timelimit', $custom_timelimit);
		}

		$smarty->assign('back_url', $this->context->link->getPageLink(Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order', true));

		$smarty->assign('ga_key', $przelewy24->validateGA(Configuration::get('P24_GA_KEY'.$c_sufix)));
		$smarty->assign('payslow_enabled', Configuration::get('P24_PAYSLOW_ENABLED'.$c_sufix)==1);
		$smarty->assign('pay_in_shop', Configuration::get('P24_PAYINSHOP_ENABLED'.$c_sufix)==1);
        $smarty->assign('default_remember_card', Configuration::get('P24_DEFAULT_REMEMBER_CARD'.$c_sufix)==1);
		$smarty->assign('accept_in_shop_translation', html_entity_decode($przelewy24->l('Yes, I have read and accept Przelewy24.pl terms.')));

		$ccards_forget = Db::getInstance()->getRow('SELECT cc_forget FROM '._DB_PREFIX_.'przelewy24_customersettings WHERE customer_id='.$customer->id);

		$smarty->assign('ccards_forget', $ccards_forget['cc_forget'] == 1);
		$smarty->assign('myStoredCardsUrl', $this->context->link->getModuleLink('przelewy24', 'myStoredCards', array(), Configuration::get('PS_SSL_ENABLED') == 1));
		if ($cart) $smarty->assign('cartId',$cart->id);

		//inne metody płatności
		$p24_paymethod_list = Configuration::get('P24_PAYMETHOD_LIST'.$c_sufix);
		if ($p24_paymethod_list) {
			$res_allMethods = $przelewy24->availablePaymentMethods(Configuration::get('P24_PAYSLOW_ENABLED'.$c_sufix)!=1, $currency);
			if (is_array($res_allMethods)) {

				Tools::addCSS($przelewy24->p24_css);
				$this->context->controller->addCSS($przelewy24->p24_css,'all');

				$allMethods = array();
				foreach ($res_allMethods as $bank) {
					$allMethods[$bank->id] = $bank->name;
				}

				$p24_paymethod_first = explode(',',Configuration::get('P24_PAYMETHOD_FIRST'.$c_sufix));
				$p24_paymethod_second = explode(',',Configuration::get('P24_PAYMETHOD_SECOND'.$c_sufix));

				foreach ($p24_paymethod_first as $key => $item) {
					if (!isset($allMethods[$item])) unset($p24_paymethod_first[$key]);
				}

				foreach ($p24_paymethod_second as $key => $item) {
					if (!isset($allMethods[$item])) unset($p24_paymethod_second[$key]);
				}

				if (sizeof($p24_paymethod_first) == 0) {
					$p24_paymethod_first = $p24_paymethod_second;
					$p24_paymethod_second = array();
				}

				$smarty->assign(array(
					'p24_paymethod_all'      => $allMethods,
					'p24_paymethod_list'     => $p24_paymethod_list,
					'p24_paymethod_graphics' => Configuration::get('P24_PAYMETHOD_GRAPHICS'.$c_sufix),
					'p24_paymethod_first'    => $p24_paymethod_first,
					'p24_paymethod_second'   => $p24_paymethod_second,
				));
			}
		}
		$smarty->assign('p24_css',  $przelewy24->p24_css);

        $protocol = Przelewy24::getHttpProtocol();
        $smarty->assign( 'base_url', $protocol . htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8') . __PS_BASE_URI__ );

        $this->setTemplate('paymentConfirmation.tpl');
    }

    public function addStringToBasketInformation( $name, &$basketInformation, $arrayIndex, $item )
    {
        if( isset( $item[$arrayIndex] ) )
            $basketInformation[$name] = strip_tags( $item[$arrayIndex] );
    }

    public function addPriceToBasketInformation( $name, &$basketInformation, $arrayIndex, $item )
    {
        if( isset( $item[$arrayIndex] ) )
            $basketInformation[$name] = ( string )( round( $item[$arrayIndex], 2 ) * 100 );
    }

	public static function is_ssl()
    {
		if ( isset($_SERVER['HTTPS']) ) {
			if ( 'on' == strtolower($_SERVER['HTTPS']) )
				return true;
			if ( '1' == $_SERVER['HTTPS'] )
				return true;
		} elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
			return true;
		}
		return false;
	}
}
