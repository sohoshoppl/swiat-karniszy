<?php
/**
 * Created by kzielin
 */
class Przelewy24ApiModuleFrontController extends ModuleFrontController {
	public $ssl = true;

	private $description = array(
		'methods' => array(
			'login' => array(
				'params' => array(
					'method' => 'login',
					'login' => 'string',
					'pass'  => 'string',
				),
				'result' => 'array',
			),
			'products' => array(
				'params' => array(
					'method' => 'products',
				),
				'params_optional' => array(
					'offset' => 'int',
					'limit' => 'int',
					'order' => 'enum(name,id_product,price,date_add,date_upd,position)',
					'way' => 'enum(ASC,DESC)',
				),
				'result' => 'array',
			),
			'search' => array(
				'params' => array(
					'method' => 'search',
					'query' => 'string',
				),
				'result' => 'array',
			),
			'countries' => array(
				'params' => array(
					'method' => 'countries',
				),
				'params_optional' => array(
					'active' => 'bool',
				),
				'result' => 'array',
			),
			'register' => array(
				'params' => array(
					'method' => 'register',
					'lastname' => 'string',
					'firstname' => 'string',
					'email' => 'string',
					'passwd' => 'string',
					'alias' => 'string',
					'id_country' => 'int',
					'address1' => 'string',
					'postcode' => 'string',
					'city' => 'string',
				),
				'params_optional' => array(
					'company' => 'string',
					'address2' => 'string',
					'phone' => 'string',
					'phone_mobile' => 'string',
					'other' => 'string',
				),
				'result' => array(
					'method' => 'register',
					'result' => 'boolean',
					'customer_id' => 'int',
					'address_id' => 'int',
				),
			),
			'modify' => array(
				'params' => array(),
				'params_optional' => array(
					'firstname' => 'string',
					'lastname' => 'string',
					'email' => 'string',
					'passwd' => 'string',
					'id_country' => 'int',
				),
				'result' => 'array',
			),
			'addresses' => array(
				'params' => array(),
				'result' => 'array',
			),
			'addAddress' => array(
				'params' => array(
					'method' => 'addAdress',
					'alias' => 'string',
					'id_country' => 'int',
					'address1' => 'string',
					'postcode' => 'string',
					'city' => 'string',
				),
				'params_optional' => array(
					'company' => 'string',
					'address2' => 'string',
					'phone' => 'string',
					'phone_mobile' => 'string',
					'other' => 'string',
				),
				'result' => array(
					'result' => 'bool',
					'addresses' => 'array',
				),
			),
			'delAddress' => array(
				'params' => array(
					'address_id' => 'int',
				),
				'result' => 'array',
			),
			'updateAddress' => array(
				'params' => array(
					'method' => 'updateAddress',
					'address_id' => 'int',
					'alias' => 'string',
					'id_country' => 'int',
					'address1' => 'string',
					'postcode' => 'string',
					'city' => 'string',
				),
				'params_optional' => array(
					'company' => 'string',
					'address2' => 'string',
					'phone' => 'string',
					'phone_mobile' => 'string',
					'other' => 'string',
				),
				'result' => array(
					'result' => 'bool',
					'addresses' => 'array',
				),
			),
			'addOrder' => array(
				'params' => array(
					'cart' => array(
						array('id' => 'int', 'qt' => 'int'),
						array('id' => 'int', 'qt' => 'int'),
					),
				),
				'result' => 'array',
			),
			'getOrders' => array(
				'params' => array(),
				'result' => 'array',
			),
			'getOrder' => array(
				'params' => array(
					'id' => 'int',
				),
				'result' => 'array',
			),
			'getSessionId' => array(
				'params' => array(
					'order_id' => 'int',
				),
				'result' => array(
					'session_id' => 'string',
				),
			),
			'getOrderStates' => array(
				'params' => array(),
				'result' => 'array',
			),
			'ccards' => array(
				'params' => array(),
				'result' => 'array',
			),
/*			'ccadd' => array(
				'params' => array(
					'p24_order_id' => 'int',
				),
				'result' => 'array',
			),
*/
			'ccdel' => array(
				'params' => array(
					'id' => 'int',
				),
				'result' => 'array',
			),
			'charge' => array(
				'params' => array(
					'card_id' => 'int',
					'order_id' => 'int',
				),
				'result' => 'array',
			),

		),
	);

	private function _getCCards($customer_id, $all = false) {
		return Db::getInstance()->ExecuteS(
			' SELECT id, mask, card_type, expires '.
			' FROM '._DB_PREFIX_.'p24_recuring '.
			' WHERE customer_id='.$customer_id.
			(!$all ? ' AND expires >= ' . date("ym") : '')
		);
	}
	
	private function _delCCard($customer_id, $card_id) {
		Db::getInstance()->ExecuteS(
			' DELETE FROM '._DB_PREFIX_.'p24_recuring '.
			' WHERE customer_id='.(int)$customer_id.
			' AND id='.(int)$card_id.
			' LIMIT 1;'
		);
	}

	private function _validatePost($method, $fields, &$res) {
		foreach ($fields as $field) {
			if (!isset($_POST[$field])) {
				$res = array('method' => $method, 'error' => "missing {$field} param");
				return false;
			}
		}
		return true;
	}

	private function _isLogged($method, $customer, &$res) {
		if (!$customer) {
			$res = array('method' => $method, 'error' => "customer login problem");
			return false;
		}
		return true;
	}

	private function _delAddress($customer, $id, &$res) {
		$address = new Address((int)$id);
		if ($address->id_customer != $customer->id) {
			$res = array('method' => $_POST['method'], 'error' => "address_id and customer_id dont match");
			return false;
		}
		$address->delete();
		return true;
	}
	private function _addAddress($customer, $fields) {
		$address = new Address();
		$address->id_customer = $customer->id;
		$address->id_country = $fields['id_country'];
		$address->id_state = $fields['id_state'];
		$address->alias = $fields['alias'];
		$address->id_manufacturer = 0;
		$address->id_supplier = 0;
		$address->id_warehouse = 0;
		$address->deleted = 0;
		
		$address->firstname = $customer->firstname;
		$address->lastname = $customer->lastname;
		
		$address->address1 = $fields['address1'];
		$address->address2 = $fields['address2'];
		
		$address->postcode = $fields['postcode'];
		$address->city = $fields['city'];
		$address->phone = $fields['phone'];
		$address->phone_mobile = $fields['phone_mobile'];
		
		$address->other = $fields['other'];
		$address->company = $fields['company'];
		$is_address_added = $address->add();
		
		if ($is_address_added) {
			$ccards = $this->_getCCards($customer->id, true);
			if (is_array($ccards)) {
				foreach ($ccards as $ccard) {
					$this->_delCCard($customer->id, $ccard['id']);
				}
			}
		}
		
		return $address->id;
	}

	public function initContent() {
		$res = $this->description;

		if (isset($_POST['login'], $_POST['pass'])) {
			$customerCore = new CustomerCore();
			$customer = $customerCore->getByEmail($_POST['login'], $_POST['pass']);
		}

		$LANG = Configuration::get('PS_LANG_DEFAULT');
		
		switch ($_POST['method']) {
			case 'login':
				$res = array('method' => $_POST['method'], 'customer' => $customer );
				break;
			case 'products':
				if (!isset($_POST['offset'])) $_POST['offset'] = 0;
				if (!isset($_POST['limit']))  $_POST['limit'] = 10;
				if (!isset($_POST['order']))  $_POST['order'] = 'name';
				if (!isset($_POST['way']))    $_POST['way'] = 'ASC';
				
				$products = ProductCore::getProducts($LANG, (int)$_POST['offset'], (int)$_POST['limit'], $_POST['order'], strtoupper($_POST['way']));
				foreach ($products as &$item) {
					$cover = ProductCore::getCover($item['id_product']);
					$link = new LinkCore();
					$item['img_url'] = $link->getImageLink($item['link_rewrite'], $cover['id_image'], 'home_default');
					$item['price_final'] = round(ProductCore::getPriceStatic($item['id_product']),2);
				}
				$res = array('method' => $_POST['method'], 'products' => $products);
				break;
			case 'search':
				if (!$this->_validatePost($_POST['method'], array('query'), $res)) break;
				$products = array();
				$result = ProductCore::searchByName($LANG, $_POST['query']);
				if ($result) foreach ($result as $item) {
					$product = new ProductCore($item['id_product'], false, $LANG);
					$cover = ProductCore::getCover($item['id_product']);
					$link = new LinkCore();
					if (is_array($product->link_rewrite)) $product->link_rewrite = array_shift($product->link_rewrite);
					$product->img_url = $link->getImageLink($product->link_rewrite, $cover['id_image'], 'home_default');
					$products[] = $product;
				}
				
				$res = array('method' => $_POST['method'], 'products' => $products);
				break;
			case 'countries':
				if (!isset($_POST['active']))  $_POST['active'] = true;
				$result = CountryCore::getCountries($LANG, $_POST['active']);
				$countries = array();
				foreach ($result as $item) {
					$countries[$item['id_country']] = array(
						'name' => $item['name'],
						'iso_code' => $item['iso_code'],
						'id_currency' => $item['id_currency'],
						'zone' => $item['zone'],
					);
				}
				$res = array('method' => $_POST['method'], 'countries' => $countries);
				break;
			case 'register':
				if (!$this->_validatePost($_POST['method'], array('lastname','firstname','email','passwd','id_country','alias','address1','postcode','city'), $res)) break;

				if (!isset($_POST['phone']) && !isset($_POST['phone_mobile'])) {
					$res = array('method' => $_POST['method'], 'error' => "missing phone or phone_mobile param");
					break;
				}
				
				$customer = new Customer();
				$customer->lastname  = $_POST['lastname'];
				$customer->firstname = $_POST['firstname'];
				$customer->email     = $_POST['email'];
				$customer->passwd    = Tools::encrypt($_POST['passwd']);
				$customer->is_guest  = 0;
				$is_customer_added = $customer->add();
				if ($is_customer_added) {
					$address_id = $this->_addAddress($customer, $_POST);
				}

				$res = array('method' => $_POST['method'], 'result' => $is_customer_added && $address_id, 'customer_id' => $customer->id, 'address_id' => $address_id ); 

				break;
			case 'modify':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;

				foreach (array('firstname','lastname','email','passwd','id_country') as $field) {
					if (isset($_POST[$field])) {
						if ($field == 'passwd') $_POST[$field] = Tools::encrypt($_POST[$field]);
						$customer->$field = $_POST[$field];
					}
				}
				$customer->update();
				
				$res = array('method' => $_POST['method'], 'result' => $customer);
				break;
			case 'addresses':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				$res = array('method' => $_POST['method'], 'result' => $customer->getAddresses($LANG)); 
				break;
			case 'addAddress':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('id_country','alias','address1','postcode','city'), $res)) break;
				if (!isset($_POST['phone']) && !isset($_POST['phone_mobile'])) {
					$res = array('method' => $_POST['method'], 'error' => "missing phone or phone_mobile param");
					break;
				}
				
				$adress_id = $this->_addAddress($customer, $_POST);

				$res = array('method' => $_POST['method'], 'result' => !!$adress_id, 'addresses' => $customer->getAddresses($LANG)); 
				break;
			case 'delAddress':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('address_id'), $res)) break;
				if (!$this->_delAddress($customer, (int)$_POST['address_id'], $res)) break;
				$res = array('method' => $_POST['method'], 'result' => $customer->getAddresses($LANG));
				break;
			case 'updateAddress' :
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('address_id','id_country','alias','address1','postcode','city'), $res)) break;
				if (!$this->_delAddress($customer, (int)$_POST['address_id'], $res)) break;
				$adress_id = $this->_addAddress($customer, $_POST);
				$res = array('method' => $_POST['method'], 'result' => !!$adress_id, 'addresses' => $customer->getAddresses($LANG)); 
				break;
			case 'addOrder':
				global $currency;
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('cart','address_id'), $res)) break;

				$cart = new Cart();
				$cart->id_customer = $customer->id;
				$cart->id_address_delivery = (int)$_POST['address_id'];
				$cart->id_address_invoice = (int)$_POST['address_id'];
				$cart->id_lang = $LANG;
				$cart->id_currency = $currency->id;
				$cart->id_carrier = 1;
				$cart->recyclable = 0;
				$cart->gift = 0;
				$cart->add();
				$cart->update();

				foreach ($_POST['cart'] as $item) {
					$cart->updateQty((int)$item['qt'], (int)$item['id'], null, false);
				}

				$cart->update();
				$cart->getPackageList(true);
				
				$przelewy24 = new Przelewy24();
				$currency = $przelewy24->getCurrencyCode($cart->id_currency);
				$c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_'.$currency;

				$amount = $cart->getOrderTotal(true, Cart::BOTH);
				$amount = $przelewy24->getP24Amount(intval($cart->id_currency), $amount);
				try {
					$przelewy24->validateOrder($cart->id,
						(int)Configuration::get('P24_ORDER_STATE_1'),
						(float)$cart->getOrderTotal(true, Cart::BOTH),
						'Przelewy24', NULL, array(), NULL, false,
						$customer->secure_key
					);
				} catch (Exception $e) {
					$res = array('method' => $_POST['method'], 'error' => $e->getMessage());
					break;
				}
				$order_id = Order::getOrderByCartId($cart->id);
				$order = new Order($order_id);

				$s_sid = uniqid("",true);
				Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'przelewy24_amount` '.'(`s_sid`,`i_id_order`,`i_amount`) '.'VALUES("'.$s_sid.'",'.$cart->id.','.$amount.')');

				$res = array(
					'method' => $_POST['method'],
					'session_id' => $cart->id.'|'.$s_sid,
					'result' => $order,
				);
					
				break;
			case 'getSessionId':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('order_id'), $res)) break;

				$order = new Order($_POST['order_id']);
				$cart = new Cart($order->id_cart);

				$przelewy24 = new Przelewy24();
				
				$amount = $cart->getOrderTotal(true, Cart::BOTH);
				$amount = $przelewy24->getP24Amount(intval($cart->id_currency), $amount);

				$s_sid = uniqid("",true);
				Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'przelewy24_amount` '.'(`s_sid`,`i_id_order`,`i_amount`) '.'VALUES("'.$s_sid.'",'.$cart->id.','.$amount.')');

				$res = array(
					'method' => $_POST['method'],
					'session_id' => $cart->id.'|'.$s_sid,
				);
				break;
			case 'getOrders':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				$orders = OrderCore::getCustomerOrders($customer->id);
				foreach ($orders as $key => $order) {
					if (!isset($order['current_state'])) {
						unset($orders[$key]);
					}
				}
				$res = array('method' => $_POST['method'], 'result' => $orders);
				break;
			case 'getOrder':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('id'), $res)) break;

				$order = new Order((int)$_POST['id']);
				
				if ($order->id_customer == $customer->id || true) {
					$res = array(
						'method' => $_POST['method'],
						'result' => $order,
					);
				}
				break;
			case 'getOrderStates':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				$res = array('method' => $_POST['method'], 'result' => OrderStateCore::getOrderStates($LANG));
				break;
			case 'ccards':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				$res = array('method' => $_POST['method'], 'result' => $this->_getCCards($customer->id));
				break;
			case 'ccdel':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('id'), $res)) break;

				$this->_delCCard($customer->id, (int)$_POST['id']);
				$res = array('method' => $_POST['method'], 'result' => $this->_getCCards($customer->id));
				break;
/*
			case 'ccadd':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('p24_order_id'), $res)) break;

				try {
					$P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID'), Configuration::get('P24_SHOP_ID'), Configuration::get('P24_SALT'), Configuration::get('P24_TEST_MODE'));
					$przelewy24 = new Przelewy24();
					$soap=new SoapClient($P24C->getHost().$przelewy24->getWsdlCCService(), array('trace'=>true, 'exceptions'=>true));
					$req = $soap->GetTransactionReference(Configuration::get('P24_SHOP_ID'), Configuration::get('P24_API_KEY'), (int)$_POST['p24_order_id']);

					if ($req->error->errorCode === 0) {
						$ref=$req->result->refId;
						$exp=substr($req->result->cardExp,2,2).substr($req->result->cardExp,0,2);
						if (!empty($ref)) {
							if (date('ym')<=$exp) {
								Db::getInstance()->Execute("REPLACE INTO "._DB_PREFIX_."p24_recuring (website_id, customer_id, reference_id, expires, mask, card_type) ".
									   "VALUES (1, '{$customer->id}', '{$ref}', '{$exp}', '{$req->result->mask}', '{$req->result->cardType}')");
								
							}
						}
					}
				} catch(Exception $e) {
					error_log(__METHOD__.' '.$e->getMessage());
				}

				$res = array('method' => $_POST['method'], 'result' => $this->_getCCards($customer->id));
				break;
*/
			case 'charge':
				if (!$this->_isLogged($_POST['method'], $customer, $res)) break;
				if (!$this->_validatePost($_POST['method'], array('card_id','order_id','session_id'), $res)) break;

				$order = new Order((int)$_POST['order_id']);
				if ($order->id_customer != $customer->id) {
					$res = array('method' => $_POST['method'], 'error' => "order_id and customer_id dont match");
					break;
				}
				$oderState = array();
				foreach (OrderStateCore::getOrderStates($LANG) as $item) {
					if ($item->id_order_state == $order->current_state) {
						$orderState = $item;
					}
				}
				if ($oderState->paid != 0) {
					$res = array('method' => $_POST['method'], 'error' => "order already paid");
					break;
				}

				list($cartID,$sa_sid) = explode('|', $_POST["session_id"], 2);
				$cartID = (int)$cartID;
				$sa_sid = pSQL($sa_sid);

				$o_order = Db::getInstance()->getRow('SELECT `i_id_order`,`i_amount` FROM `'._DB_PREFIX_.'przelewy24_amount` WHERE `s_sid`="'.$sa_sid.'"');
				$p24_kwota = (int)$o_order['i_amount'];
				$i_id_order = $o_order['i_id_order'];
				$cart = new Cart(intval($i_id_order));
				$order_id = Order::getOrderByCartId(intval($i_id_order));

				if ($order_id != (int)$_POST['order_id']) {
					$res = array('method' => $_POST['method'], 'error' => "order id and session_id dont match");
					break;
				}
				

				$przelewy24 = new Przelewy24();
				$currencies = $przelewy24->getCurrency(intval($cart->id_currency));
				$currency = '';
				foreach ($currencies as $c) {
					if ($c['id_currency']==$cart->id_currency) {
						$currency = $c['iso_code'];
					}
				}

				$rr=Db::getInstance()->getRow('SELECT reference_id FROM '._DB_PREFIX_.'p24_recuring WHERE id='.(int)$_POST['card_id'].' AND customer_id='.$customer->id);
				$ref=$rr['reference_id'];

				if (!$ref) {
					$res = array('method' => $_POST['method'], 'error' => "card_id and customer dont match");
					break;
				}

				$c_sufix = ($currency == 'PLN' || empty($currency)) ? '' : '_'.$currency;

				$P24C = new Przelewy24Class(Configuration::get('P24_MERCHANT_ID'.$c_sufix), Configuration::get('P24_SHOP_ID'.$c_sufix), Configuration::get('P24_SALT'.$c_sufix), Configuration::get('P24_TEST_MODE'.$c_sufix));

				try {
					$s=new SoapClient($P24C->getHost().$przelewy24->getWsdlCCService(), array('trace'=>true, 'exceptions'=>true));
					$wsres=$s->ChargeCard(
						Configuration::get('P24_SHOP_ID'.$c_sufix),
						Configuration::get('P24_API_KEY'.$c_sufix),
						$ref,$p24_kwota,$currency,
						$customer->email,
						$_POST['session_id'],
						$customer->firstname . ' ' . $customer->lastname,
						'Zamówienie: ' . $_POST['order_id'] . '(' . $order->reference . ')'
					);
				} catch (Exception $e) {
					if ($debug) echo $e->getMessage();
					error_log(__METHOD__.' '.$e->getMessage());
				}

				$history = new OrderHistory();

				$history->id_order = intval($order_id);

				$rq = Db::getInstance()->getRow('SELECT `id_order_state` FROM `'._DB_PREFIX_.'order_state_lang`	WHERE id_lang = \''.pSQL('1').'\' AND  template = \''.pSQL('payment_error').'\'');
				$order_state = $rq['id_order_state'];
				if($wsres->error->errorCode==0) {
					$order_state = Configuration::get('P24_ORDER_STATE_2');
					Db::getInstance()->Execute("DELETE FROM "._DB_PREFIX_."przelewy24_lastmethod WHERE customer_id = {$order->id_customer} LIMIT 1");
				}
				$history->changeIdOrderState($order_state, intval($order_id));
				$history->addWithemail(true);
				try {
					$payments = $order->getOrderPaymentCollection();
					if (count($payments) > 0) {
							$payments[0]->transaction_id = $_POST["p24_order_id"];
							$payments[0]->update();
					}
				} catch (Exception $e) {}
				
				$res = array('method' => $_POST['method'], 'result' => $wsres->error->errorCode==0 ? 'ok' : 'fail', 'wsResult' => $wsres);
				break;
		}
		
		header('content-type: application/json');
		exit(json_encode( $res ));
	}
}
