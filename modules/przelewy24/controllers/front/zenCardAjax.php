<?php

class Przelewy24zenCardAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        if( isset( $_REQUEST['action']) && $_REQUEST['action'] == 'getGrandTotal' )
        {
            die( $this->context->cart->getOrderTotal( true, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING ) );
        }
        if( isset( $_REQUEST['action']) && $_REQUEST['action'] == 'shippingCosts' )
        {
            die( $this->context->cart->getOrderTotal( true, Cart::ONLY_SHIPPING ) );
        }
    }

}