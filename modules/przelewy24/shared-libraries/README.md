# Zbiór wspólnych bibliotek/klas dla wszystkich wtyczek sklepowych Przelewy24 #

## Instrukcje ##

### Sub repozytorium ###
Repozytorium należy umieścić wewnątrz repozytorium wtyczki sklepowej. 
Więcej informacji o subrepozytorium można znaleźć [tutaj](https://git-scm.com/book/pl/v1/Narz%C4%99dzia-Gita-Modu%C5%82y-zale%C5%BCne).

### Jak podpiąć to repozytorium pod inne (np. w repozytorium wtyczki sklepowej)? ###

`git submodule add http://gitlab.zeus:8282/wtyczki/shared-libraries.git shared-libraries`

Każde repozytorium działa niezależnie, to oznacza, że gdy chcemy pobrać najnowsze zmiany z subrepo 
`shared-libraries` to należy w katalogu subrepozytorium `shared-libraries` wywołać komendę: 

`git pull origin master`

Więcej informacji [tutaj](https://git-scm.com/book/pl/v1/Narz%C4%99dzia-Gita-Modu%C5%82y-zale%C5%BCne).

### Klonowanie projektu z modułami zależnymi - gdy sub-repozytorium jest już definiowane i trzeba je tylko pobrać ###
- Sklonujesz projekt, który ma moduł zależny. Kiedy pobierzesz taki projekt, 
otrzymasz katalogi które zawierają moduły zależne, ale nie będzie w nich żadnych plików!
- Musisz uruchomić dwie komendy: `git submodule init`, aby zainicjować lokalny plik konfiguracyjny, 
oraz `git submodule update`, aby pobrać wszystkie dane/pliki z tego projektu.

### Dołączenie repozytorium do silnika sklepu ###

Wystarczy zaincludować plik `autoloader.php`, który znajduje się w root, np. (ścieżka dla każdego sklepu może być inna):

```php
require_once(dirname(__FILE__) . '/modules/przelewy24/shared-libraries/autoloader.php');
```

Ten plik ładuje wszystkie klasy/biblioteki.

### Dodatkowe informacje/uwagi ###

- Pakowanie do ZIP katalogów, które zawierają pliki git - tak by pliki git pominąć:

`zip -r plugin.zip plugin_dir -x *.git*`

## Spis bibliotek/klas ##
- [klasa Przelewy24Product](docs/przelewy24product.md)
- [klasa Przelewy24Installer](docs/przelewy24installer.md)

### Minimalne wymagania ###

- PHP 5.2+
- SOAP
- cURL
