# Change Log

## [3.3.19] - 2017-03-28 ##
- Added payment card registration from panel.

## [3.3.18] - 2017-03-28 ##
- Fixed id_currency call.

## [3.3.17] - 2017-03-27 ##
- Added security token to paymentConfirmation.

## [3.3.16] - 2017-01-12 ##
- Fixed translations (Bug #2049)

## [3.3.15] - 2017-01-09 ##
- Fixed value in write context issue (Bug #2003)

## [3.3.14] - 2017-01-03 ##
- Added display of selected payment method in confirmation (Bug #1981)

## [3.3.13] - 2016-12-15 ##
- Updated Hungary translations.

## [3.3.12] - 2016-12-13 ##
- Fixed discount with different currencies. 

## [3.3.11] - 2016-12-07 ##

- Added Hungary language.
- Added missing translations.

## [3.3.10] - 2016-12-07 ##

- ZenCard - cash on delivery.

## [3.3.9] - 2016-12-06 ##

- Fixed price zc on delivery payment.
- Fixed delete card link.

## [3.3.8] - 2016-11-25 ##

- Fixed extra-charge when update order status

## [3.3.7a] - 2016-11-10 ##

- ZenCard - fixed total price including delivery costs

## [3.3.7] - 2016-11-10 ##

- ZenCard - has coupon field.
- Fixed ZenCard amount on checkout.

## [3.3.6b] - 2016-11-09 - Jakub Saleniuk ##

- Added `p24_shipping` to ajax payment.

## [3.3.6a] - 2016-11-08 - Jakub Saleniuk ##

- Prepare products cart for p24 in ajax payment.

## [3.3.6] - 2016-11-04 ##

- Fixed double discount ZC in front.
- Fixed discount price on invoice.

## [3.3.5] - 2016-10-27 ##

- ZenCard - use function withZencard().

## [3.3.4] - 2016-10-21 ##

- Create free order when cart amount is 0.
- Fixed tax in extra discount.
- Moved ZenCard sdk to shared-libraries.

## [3.3.3b] - 2016-10-19 ##

- Removed all functions in empty().

## [3.3.3a] - 2016-10-17 ##

- Removed function in empty(). 

## [3.3.3] - 2016-10-13 ##

- Added class `Przelewy24Product`.
- Prepare products cart for p24. 

## [3.3.2l] - 2016-10-10 ##

- Confirmation - updated descriptions/translations.

## [3.3.2k] - 2016-10-05 - Jakub Saleniuk

- Changed functionality of ZenCard coupons to a new version.
- Added _zencard field and withZencard method to the Transaction Class.

## [3.3.2j] - 2016-10-03

- Fixed base_url on myStoredCards page.

## [3.3.2i] - 2016-10-03

- Added ZenCard transaction logger.

## [3.3.2h] - 2016-09-29 

- Fixed cart display on confirm page.

## [3.3.2g] - 2016-09-28 - Jakub Saleniuk

- Added a ZenCard amount to PDFInvoice.
- When ZenCard's discount amount equals to 0 a discount is not being created for an order.

## [] - 2016-09-27

- Updated PL translations. Added missing descriptions. 

## [3.3.2f] - 2016-09-26

- Add zencard info to orderCartRule.

## [3.3.2e] - 2016-09-23

- Added info about extracharge_amount in paymentConfirmation.tpl. 

## [3.3.2d] - 2016-09-23 - Jakub Saleniuk

- Fixed redirecting to success status page after card payment.

## [3.3.2c] - 2016-09-21 - Jakub Saleniuk

- Fixed creating my stored cards page url.

## [3.3.2b] - 2016-09-20

- Fixed Zencard_Util number format amount.

## [3.3.2a] - 2016-09-20 - Jakub Saleniuk

- Fixed refreshing a ZenCard coupon after updating a cart.

## [3.3.2] - 2016-09-15

- Fixed verify ZenCard process.

## [3.3.1r] - 2016-09-08

- Fixed minimum amount for discount.

## [3.3.1p] - 2016-09-08

- Fixed cart reference in hookActionCartSave.
- Delete cart rules after change discount in cart.

## [3.3.1l] - 2016-09-07 - Jakub Saleniuk

- Added `p24_shipping` field to Przelewy24 form.

## [3.3.1j] - 2016-09-06 - Jakub Saleniuk

- Changed way of adding extracharge to order.
- New table `przelewy24_extracharges` is created during installation.
- New model `Extracharge` added.
- Client doesn't need to choose a virtual product in the module to add a extracharge
to order any more. In the `actionValidateOrder` hook the extracharge is added to a order
prices.
- Removing extracharge from order if the payment method has beed changed to other in the
admin panel.
- Adding extracharge to order if the payment method has beed changed to Przelewy24 in the
admin panel.
- Extracharges are stored in the database in the `przelewy24_extracharges` table. It ensures
a proper removing extracharge from order even after changing this value by client in the
module.
- Added info about extracharge to the order's details view in the admin panel.
- Added info about extracharge to the order's pdf.









