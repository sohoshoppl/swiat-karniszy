<?php
/**
 * SohoshopMedia module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */
 
if (!defined('_PS_VERSION_'))
    exit;
require_once (dirname(__FILE__) . '/classes/SohoshopCartProductRenameDictionary.php');

class SohoshopCartProductRename extends Module
{

    public function __construct()
    {
        $this->name = 'sohoshopcartproductrename';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'sohoshop.pl';
        
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Sohoshop cart product rename');
        $this->description = $this->l('Moduł usuwa wyrazy z nazwy produktów w koszyku, które występują w słownika.');
    }
    
    

    public function install()
	{
		if (
			!parent::install() ||
			!$this->registerHook('displayHeader')
		)
			return false;
		
		// słownik
		Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'sohoshopcartproductrename_dictionary` (
			`id_sohoshopcartproductrename_dictionary` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`word` VARCHAR(255) NOT NULL,
			INDEX (`id_sohoshopcartproductrename_dictionary`)
			) ENGINE = '._MYSQL_ENGINE_.' CHARACTER SET utf8 COLLATE utf8_general_ci;
		');
		
			
		return true;
	}
	
	public function uninstall()
	{		
		if (
			!parent::uninstall()
		)
			return false;
			
		Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'sohoshopcartproductrename_dictionary`');
		
		
		return true;	
	}
	
    
	
	public function hookdisplayHeader($params)
    {

		$dictionary = SohoshopCartProductRenameDictionary::getDictionary();
		$this->context->smarty->assign(array('dictionary' => $dictionary));

		return $this->display(__FILE__, 'header.tpl');
    }
    
    
 public function getContent() {
	  
	$id_tab = Tab::getIdFromClassName($this->tabClassName);
	$html = '';
	
	if (Tools::isSubmit('settings')){
	
		
		$html .= $this->displayConfirmation($this->l('Zapisano ustawienia dla modułu'));
	
	}elseif (Tools::isSubmit('updatesohoshopcartproductrename')) {
		$id_sohoshopcartproductrename_dictionary = Tools::getValue('id_sohoshopcartproductrename_dictionary', 0);
		$html .= "edytowanie";
	}
	elseif (Tools::isSubmit('deletesohoshopcartproductrename')) {
		$id_sohoshopcartproductrename_dictionary = Tools::getValue('id_sohoshopcartproductrename_dictionary', 0);
		$dictionary = new SohoshopCartProductRenameDictionary((int)$id_sohoshopcartproductrename_dictionary);
		$dictionary->delete();
		$html .= $this->displayConfirmation($this->l('Usunięto'));
	}elseif (Tools::isSubmit('addsohoshopcartproductrename'))	{
		$dictionary = new SohoshopCartProductRenameDictionary;
		$word = Tools::getValue('word');
		$dictionary->word=$word;
		$dictionary->add();
		$html .= "dodawanie";
	}

	$html .= $this->renderForm();
	$html .= $this->renderList();
	
	return $html; 
 }


	private function renderForm()
	{
		
		$fields_form[] = array(
			'form' => array(
			'legend' => array(
				'title' => $this->l('Dodawanie słowa.'),
				'icon' => 'icon-cogs'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Fraza'),
					'name' => 'word',
					'desc' => $this->l('Pamiętaj o dodaniu wywołania funkcji SohoshopCartProductRename przy dodawaniu produktów do koszyka ( ajax-cart.js -> displayNewProducts )')

				)
			),
			'submit' => array(
				'title' => $this->l('Dodaj słowo'),
				'name' => $this->l('addsohoshopcartproductrename')
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm($fields_form);
	}  
	
public function renderList()
	{
		$dictionary = SohoshopCartProductRenameDictionary::getDictionary();
	
		$fields_list = array(
			'id_sohoshopcartproductrename_dictionary' => array(
				'title' => $this->l('ID'),
				'type' => 'int',
			),
			'word' => array(
				'title' => $this->l('Fraza'),
				'type' => 'text',
			),

		);

		$helper_list = new HelperList();
        $helper_list->shopLinkType = '';
        $helper_list->simple_header = true;
        $helper_list->identifier = 'id_sohoshopcartproductrename_dictionary';
        $helper_list->table = $this->name;
        //$helper_list->actions = array('edit', 'delete');
		$helper_list->actions = array('delete');
        $helper_list->show_toolbar = false;
        $helper_list->module = $this;
        $helper_list->title = $this->l('Lista');
        $helper_list->token = Tools::getAdminTokenLite('AdminModules');
        $helper_list->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		return $helper_list->generateList($dictionary, $fields_list);
	}		
	

    
}
