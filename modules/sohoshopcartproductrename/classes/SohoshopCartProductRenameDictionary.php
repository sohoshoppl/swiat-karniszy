<?php
/**
 * SohoshopMedia module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class SohoshopCartProductRenameDictionary extends ObjectModel
{

    public $id_sohoshopcartproductrename_dictionary;
	public $word;


    public static $definition = array(
        'table' => 'sohoshopcartproductrename_dictionary',
        'primary' => 'id_sohoshopcartproductrename_dictionary',
        'fields' => array(
			'word' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),

        ),
    );
    
    	public static function getDictionary()
	{
		$dictionary = Db::getInstance()->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'sohoshopcartproductrename_dictionary`');
		if (empty($dictionary) === true OR !sizeof($dictionary))
			return array();
	
		return ($dictionary);
	}
	
	
}
