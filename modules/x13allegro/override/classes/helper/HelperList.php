<?php

class HelperList extends HelperListCore
{
	
	/*
	* module: x13allegro
	* date: 2015-06-01 02:02:35
	* version: 3.1.0
	*/
	public function displayXallegroLink($token = null, $id, $name = null)
	{
		$href = $this->context->link->getAdminLink('AdminXAllegroMain') .'&id_product=' . $id;
		$allegro_status = (int)XAllegroAuction::getAllegroStatus($id);
		if($allegro_status === 0) {
			$color = '';
			$icon = 'AdminXAllegroMainDefault.png';
			$txt  = (isset($this->bootstrap) && $this->bootstrap) ? $this->l('Wystaw') : '';
			
		}
		else {
			$color = ' style="color:#FF5A00;"';
			$icon = 'AdminXAllegroMain.png';
			$txt  = (isset($this->bootstrap) && $this->bootstrap) ? $this->l('Wystawiony').' ('.$allegro_status.')' : '';
		}
		if(true) {
			return '
				<a class="list-allegro btn btn-default" title="'.$this->l('Wystaw na Allegro').'" href="'.$href.'"'.$color.'>
					<img height=14 src="../modules/x13allegro/img/'.$icon.'" alt="' . $this->l('Wystaw na Allegro') . '" />
					'.$txt.'
				</a>
			';
		}
		else {
			return '
				<a href="' . $href . '" class="edit" title="' . $this->l('Wystaw na Allegro') . '">
					<img src="../modules/x13allegro/img/AdminXAllegroMain.gif" alt="' . $this->l('Wystaw na Allegro') . '" />
				</a>
			';
		}
	}
	
}
