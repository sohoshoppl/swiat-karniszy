<?php

class AdminProductsController extends AdminProductsControllerCore
{
	
	public function __construct()
	{
		parent::__construct();
		$this->bulk_actions['xallegro'] = array('text' => $this->l('Wystaw zaznaczone'));
	}

	public function renderList()
	{
		$this->addRowAction('xallegro');
		return parent::renderList();
	}

	public function processBulkXallegro()
	{
		if (is_array($this->boxes) && !empty($this->boxes)) {
			$this->redirect_after = $this->context->link->getAdminLink('AdminXAllegroMain') . '&id_product=' . implode(',', array_map('intval', $this->boxes));
		}
	}

}
