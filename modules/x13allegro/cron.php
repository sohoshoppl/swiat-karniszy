<?php

	if(realpath('..') == '/home/htmnet/www/prestashop_modules') {
		include '/home/htmnet/www/prestashop.1.6/config/config.inc.php';
		include '/home/htmnet/www/prestashop.1.6/init.php';
	}
	else {
		include dirname(__FILE__) . '/../../config/config.inc.php';
		include dirname(__FILE__) . '/../../init.php';
	}
    
    require_once (dirname(__FILE__) . '/x13allegro.ion.php');
    
    if (strlen(X13_ION_VERSION)) {
        $pattern = '/.*-7\.php$/';
    } else {
        $pattern = '/.*[^-7]\.php$/';
    }
    
    $path = dirname(__FILE__) . '/classes/';
    $dir = dir($path);
    while (false !== ($file = $dir->read())) {
        if (!is_dir($path . $file) && preg_match($pattern, $file)) {
            require_once ($path . $file);
        }
    }
    
    if (Tools::getValue('token') != XAllegroConfiguration::get('CRON_TOKEN')) {
        echo 'INVALID TOKEN';
        exit;
    }
    
    $sql = '
        SELECT `id_xallegro_auction`, `id_auction`, `id_allegro_account`, `id_product`, `id_product_attribute`, `title` 
        FROM `' . _DB_PREFIX_ . 'xallegro_auction` 
        WHERE `end` > ' . time() . ' 
        ORDER BY `last_sync` DESC
    ';
    //p($sql);
    $auctions = Db::getInstance()->ExecuteS($sql);
    //d($auctions);
    $idAccountsAuctions = array();
    
    foreach ($auctions as $auction) {
        if (!isset($idAccountsAuctions[$auction['id_allegro_account']])) {
            $idAccountsAuctions[$auction['id_allegro_account']] = array();
        }
        
        $idAccountsAuctions[$auction['id_allegro_account']][] = (float)$auction['id_auction'];
    }
    
    Db::getInstance()->execute('SET autocommit = 0;');
    
    foreach ($idAccountsAuctions as $idAccount => $idFullAuctions) {
        foreach (array_chunk($idFullAuctions, 25) as $idAuctions) {
            Db::getInstance()->execute('START TRANSACTION');

            $account = new XAllegroAccount($idAccount);
            
            if (!ValidateCore::isLoadedObject($account)) {
                continue;
            }
            
            $API = new XAllegroAPI($account);
            
            $itemsInfo = $API->API()->GetItemsInfo(array(
                'items-id-array' => $idAuctions,
                'get-desc' => false,
                'get-image-url' => false,
                'get-attribs' => false,
                'get-postage-options' => false,
                'get-company-info' => false
            ));
            
            foreach ($itemsInfo['array-item-list-info'] as $itemInfo) {
//                Update info about auctions
                Db::getInstance()->update('xallegro_auction', array(
                    'title' => $itemInfo->{'item-info'}->{'it-name'},
                    'quantity' => $itemInfo->{'item-info'}->{'it-quantity'},
                    'bids' => $itemInfo->{'item-info'}->{'it-bid-count'},
                    'end' => $itemInfo->{'item-info'}->{'it-ending-time'},
                    'price' => $itemInfo->{'item-info'}->{'it-price'},
                    'price_buy_now' => $itemInfo->{'item-info'}->{'it-buy-now-price'},
                    'hits' => $itemInfo->{'item-info'}->{'it-hit-count'},
                    'last_sync' => time()
                ), '`id_auction` = ' . $itemInfo->{'item-info'}->{'it-id'}, 1);
            }
        
            $idTransactions = $API->API()->GetTransactionsIDs(array(
                'items-id-array' => $idAuctions,
                'user-role' => 'seller'
            ));
        
            if ($idTransactions) {
                $dataForSellers = $API->API()->GetPostBuyFormsDataForSellers($idTransactions);

                foreach ($dataForSellers as $form) {
                    if (XAllegroForm::isExistsForm($form['post-buy-form-id'])) {
                        continue;
                    }
                    
//                    Create form row in db
                    Db::getInstance()->insert('xallegro_form', array(
                        'id_allegro_form' => $form['post-buy-form-id']
                    ));
                    
//                    Create bids
                    foreach ($form['post-buy-form-items'] as $item) {
                        $auction = XAllegroTools::findElementByKeyValue($auctions, 'id_auction', $item['post-buy-form-it-id']);

                        if (!$auction) {
                            continue;
                        }

                        foreach ($item['post-buy-form-it-deals'] as $deal) {
                            Db::getInstance()->insert('xallegro_auction_bid', array(
                                'id_auction' => $auction['id_auction'],
                                'deal_date' => strtotime($deal['deal-date']),
                                'deal_quantity' => $deal['deal-quantity'],
                                'user_id' => $form['post-buy-form-buyer-id'],
                                'user_login' => $form['post-buy-form-buyer-login'],
                                'user_fullname' => $form['post-buy-form-shipment-address']['post-buy-form-adr-full-name'],
                                'user_email' => $form['post-buy-form-buyer-email']
                            ));
                        }
                        
//                        Quanity shop update
                        if (XAllegroConfiguration::get('QUANITY_SHOP_UPDATE')) {
                            StockAvailable::updateQuantity($auction['id_product'], $auction['id_product_attribute'], (-1 * $item['post-buy-form-it-quantity']));
                        }
                    }

                    if (XAllegroConfiguration::get('IMPORT_ORDERS')) {
                        $fullname = explode(' ', $form['post-buy-form-shipment-address']['post-buy-form-adr-full-name'], 2);

                        $customer = new Customer();
                        $customer->getByEmail($form['post-buy-form-buyer-email']);

                        if (!Validate::isLoadedObject($customer)) {
                            $customer->email = $form['post-buy-form-buyer-email'];
                            $customer->id_gender = 1;
//                            $customer->is_guest = 1;
                            $customer->id_default_group = 1;
                            $customer->secure_key = md5(time());
                            $customer->passwd = md5(Tools::passwdGen() . md5(time()));
                            $customer->firstname = $fullname[0];
                            $customer->lastname = $fullname[1];
                            $customer->save();
                        }
                        
                        $address = new Address();
                        $address->id_customer = $customer->id;
                        $address->id_country = 14;
                        $address->alias = 'Allegro.pl';
                        $address->company = $form['post-buy-form-shipment-address']['post-buy-form-adr-company'];
                        $address->firstname = $fullname[0];
                        $address->lastname = $fullname[1];
                        $address->address1 = $form['post-buy-form-shipment-address']['post-buy-form-adr-street'];
                        $address->postcode = $form['post-buy-form-shipment-address']['post-buy-form-adr-postcode'];
                        $address->city = $form['post-buy-form-shipment-address']['post-buy-form-adr-city'];
                        $address->phone = $form['post-buy-form-shipment-address']['post-buy-form-adr-phone'];
                        $address->vat_number = $form['post-buy-form-shipment-address']['post-buy-form-adr-nip'];
                        $address->active = 1;
                        $address->save();

                        $addressInvoice = clone $address;

                        if ($form['post-buy-form-invoice-option']) {
                            $fullnameInvoice = explode(' ', $form['post-buy-form-invoice-data']['post-buy-form-adr-full-name'], 2);

                            $addressInvoice = new Address();
                            $addressInvoice->id_customer = $customer->id;
                            $addressInvoice->id_country = 14;
                            $addressInvoice->alias = 'Allegro.pl';
                            $addressInvoice->company = $form['post-buy-form-invoice-data']['post-buy-form-adr-company'];
                            $addressInvoice->firstname = isset($fullnameInvoice[0]) && $fullnameInvoice[0] ? $fullnameInvoice[0] : 'Brak';
                            $addressInvoice->lastname = isset($fullnameInvoice[1]) && $fullnameInvoice[1] ? $fullnameInvoice[1] : 'Brak';
                            $addressInvoice->address1 = $form['post-buy-form-invoice-data']['post-buy-form-adr-street'];
                            $addressInvoice->postcode = $form['post-buy-form-invoice-data']['post-buy-form-adr-postcode'];
                            $addressInvoice->city = $form['post-buy-form-invoice-data']['post-buy-form-adr-city'];
                            $addressInvoice->phone = $form['post-buy-form-invoice-data']['post-buy-form-adr-phone'];
                            $addressInvoice->vat_number = $form['post-buy-form-invoice-data']['post-buy-form-adr-nip'];
                            $addressInvoice->active = 1;
                            $addressInvoice->save();
                        }
                        
                        $carrier = new Carrier(2);

                        $cart = new Cart();
                        $cart->id_shop_group = Context::getContext()->shop->id_shop_group;
                        $cart->id_shop = Context::getContext()->shop->id;
                        $cart->id_address_delivery = $address->id;
                        $cart->id_address_invoice = $addressInvoice->id;
                        $cart->id_currency = Currency::getIdByIsoCode('PLN');
                        $cart->id_customer = Context::getContext()->customer->id;
                        $cart->id_customer = $customer->id;
                        $cart->id_lang = Context::getContext()->language->id;
                        $cart->id_carrier = $carrier->id;
                        $cart->add();
                        
                        $order = new Order();
                        $order->current_state = 12;
                        $order->id_address_delivery = $address->id;
                        $order->id_address_invoice = $addressInvoice->id;
                        $order->id_cart = $cart->id;
                        $order->id_currency = Currency::getIdByIsoCode('PLN');
                        $order->id_lang = $customer->id_lang;
                        $order->id_customer = $customer->id;
                        $order->id_carrier = $carrier->id;
                        $order->payment = 'Zakup Allegro';
                        $order->module = 'XAllegro';
                        $order->total_paid = $form['post-buy-form-amount'];
                        $order->total_paid_tax_incl = $form['post-buy-form-amount'];
                        $order->total_paid_real = 0;
                        $order->total_products = $form['post-buy-form-amount']-$form['post-buy-form-postage-amount'];
                        $order->total_products_wt = $form['post-buy-form-amount']-$form['post-buy-form-postage-amount'];
                        $order->conversion_rate = 1;
                        $order->valid = 1;
                        $order->reference = Order::generateReference();
                        $order->total_shipping = $form['post-buy-form-postage-amount'];
                        $order->total_shipping_tax_incl = $form['post-buy-form-postage-amount'];
                        $order->save();

                        $orderCarrier = new OrderCarrier();
                        $orderCarrier->id_carrier = $carrier->id;
                        $orderCarrier->id_order = $order->id;
                        $orderCarrier->id_order_invoice = 0;
                        $orderCarrier->shipping_cost_tax_incl = $form['post-buy-form-postage-amount'];
                        $orderCarrier->save();
                        
                        $productList = '';
                        
                        foreach ($form['post-buy-form-items'] as $key => $item) {
                            $auction = XAllegroTools::findElementByKeyValue($auctions, 'id_auction', $item['post-buy-form-it-id']);
                            
                            if (!$auction) {
                                continue;
                            }

                            $product = new Product($auction['id_product'], true);
                            $tax = Tax::getProductTaxRate($auction['id_product']);

                            $productAttributeName = array();

                            if ($auction['id_product_attribute']) {
                                $combination = $product->getAttributeCombinationsById($auction['id_product_attribute'], $customer->id_lang);

                                foreach ($combination as $attribute) {
                                    $productAttributeName[] = $attribute['group_name'] . ' : ' . $attribute['attribute_name'];
                                }
                            }

                            $orderDetail = new OrderDetail();
                            $orderDetail->id_order = $order->id;
                            $orderDetail->id_warehouse = 0;
                            $orderDetail->id_shop = $product->id_shop_default;
                            $orderDetail->product_id = $auction['id_product'];
                            $orderDetail->product_attribute_id = $auction['id_product_attribute'];
                            $orderDetail->product_name = $product->name[$customer->id_lang] . (count($productAttributeName) > 0 ? ' - ' . implode(', ', $productAttributeName) : '');
                            $orderDetail->product_quantity = $item['post-buy-form-it-quantity'];
                            $orderDetail->product_price = number_format($item['post-buy-form-it-price']/(100 + $tax) * 100, 2);
                            $orderDetail->total_price_tax_incl = $item['post-buy-form-it-amount'];
                            $orderDetail->total_price_tax_excl = number_format($item['post-buy-form-it-amount']/(100 + $tax) * 100, 2);
                            $orderDetail->unit_price_tax_incl = $item['post-buy-form-it-price'];
                            $orderDetail->unit_price_tax_excl = number_format($item['post-buy-form-it-price']/(100 + $tax) * 100, 2); 
                            $orderDetail->save();
                        }
                        
                        $shipment = XAllegroTools::findElementByKeyValue(current($API->API()->GetShipmentData()), 'shipment-id', $form['post-buy-form-shipment-id']);

                        if ($form['post-buy-form-pay-type'] == 'co') {
                            $payment = 'Checkout PayU';
                        } elseif ($form['post-buy-form-pay-type'] == 'ai') {
                            $payment = 'Raty PayU';
                        } elseif ($form['post-buy-form-pay-type'] == 'collect_on_delivery') {
                            $payment = 'Płatność przy odbiorze';
                        } elseif ($form['post-buy-form-pay-type'] == 'wire_transfer') {
                            $payment = 'Zwykły przelew';
                        } elseif ($form['post-buy-form-pay-type'] == 'not_specified') {
                            $payment = 'Nie dotyczy';
                        } else {
                            $payment = 'PayU';
                        }

                        $messageText = 'Źródło: ALLEGRO | Polska | ' . $account->username . "\n";
                        $messageText .= 'ID formularza dostawy: ' . $form['post-buy-form-id'] . "\n";
                        $messageText .= 'Identyfikator kupującego: ' . $form['post-buy-form-buyer-login'] . "\n";
                        $messageText .= 'Sposób płatności: ' . $payment . "\n";
                        $messageText .= 'Sposób wysyłki: ' . $shipment->{'shipment-name'} . "\n";
                        $messageText .= 'Faktura: ' . ($form['post-buy-form-invoice-option'] ? 'Tak' : 'Nie') . "\n";
                        $messageText .= 'Wiadomość:' . "\n";
                        $messageText .= $form['post-buy-form-msg-to-seller'] ? $form['post-buy-form-msg-to-seller']  . "\n\n": 'Brak' . "\n\n";
                        $messageText .= 'Aukcje:' . "\n";

                        foreach ($form['post-buy-form-items'] as $item) {
                            $auction = XAllegroTools::findElementByKeyValue($auctions, 'id_auction', $item['post-buy-form-it-id']);

                            if (!$auction) {
                                continue;
                            }

                            $messageText .= ' - (' . $auction['id_auction'] . ') ' . $auction['title'] . "\n";
                        }

                        $message = new Message();
                        $message->message = $messageText;
                        $message->id_order = $order->id;
                        $message->id_customer = $customer->id;
                        $message->add();
                        
//                        $order_status = new OrderState(12);

                        $orderHistory = new OrderHistory();
                        $orderHistory->id_order = $order->id;
                        $orderHistory->id_employee = 0;
//                        $orderHistory->id_order_state = 10;
//                        $orderHistory->changeIdOrderState(2, $order->id);
                        
                        $orderHistory->add();
//                        if (XAllegroConfiguration::get('ORDER_SEND_MAIL')) {
//                            $orderHistory->addWithemail();
//                        } else {
//                            $orderHistory->add();
//                        }
                        
                        if (XAllegroConfiguration::get('ORDER_SEND_MAIL')) {
//                            Hook::exec('actionValidateOrder', array(
//                                'cart' => $cart,
//                                'order' => $order,
//                                'customer' => $customer,
//                                'currency' => Context::getContext()->currency,
//                                'orderStatus' => $order_status,
//                                'XAllegroOrder' => true
//                            ));
                            
//                            From prestashop
                            
//                            function getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
//                            {
//                                return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
//                            }
//                        
//                            $products_list = '';
//
//                            foreach ($order->getCartProducts() as $key => $product) {
//                                $customization_quantity = 0;
//                                $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart);
//                                if (isset($customized_datas[$product['id_product']][$product['product_attribute_id']]))
//                                {
//                                    $customization_text = '';
//                                    foreach ($customized_datas[$product['id_product']][$product['product_attribute_id']][$order->id_address_delivery] as $customization)
//                                    {
//                                        if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD]))
//                                            foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text)
//                                                $customization_text .= $text['name'].': '.$text['value'].'<br />';
//
//                                        if (isset($customization['datas'][Product::CUSTOMIZE_FILE]))
//                                            $customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';
//                                        $customization_text .= '---<br />';
//                                    }
//
//                                    $customization_text = Tools::rtrimString($customization_text, '---<br />');
//
//                                    $customization_quantity = (int)$product['customization_quantity'];
//                                    $products_list .=
//                                    '<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
//                                        <td style="padding: 0.6em 0.4em;width: 15%;">'.$product['reference'].'</td>
//                                        <td style="padding: 0.6em 0.4em;width: 30%;"><strong>'.$product['product_name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').' - '.Tools::displayError('Customized').(!empty($customization_text) ? ' - '.$customization_text : '').'</strong></td>
//                                        <td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice($product['unit_price_tax_incl'], Currency::getIdByIsoCode('PLN'), false).'</td>
//                                        <td style="padding: 0.6em 0.4em; width: 15%;">'.$customization_quantity.'</td>
//                                        <td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice($customization_quantity * ($product['unit_price_tax_incl']), Currency::getIdByIsoCode('PLN'), false).'</td>
//                                    </tr>';
//                                }
//
//                                if (!$customization_quantity || (int)$product['cart_quantity'] > $customization_quantity)
//                                    $products_list .=
//                                    '<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
//                                        <td style="padding: 0.6em 0.4em;width: 15%;">'.$product['reference'].'</td>
//                                        <td style="padding: 0.6em 0.4em;width: 30%;"><strong>'.$product['product_name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').'</strong></td>
//                                        <td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice($product['unit_price_tax_incl'], Currency::getIdByIsoCode('PLN'), false).'</td>
//                                        <td style="padding: 0.6em 0.4em; width: 15%;">'.((int)$product['product_quantity'] - $customization_quantity).'</td>
//                                        <td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice(((int)$product['product_quantity'] - $customization_quantity) * ($product['unit_price_tax_incl']), Currency::getIdByIsoCode('PLN'), false).'</td>
//                                    </tr>';
//                            }
//                            
//                            $delivery_state = $address->id_state ? new State($address->id_state) : false;
//                            $invoice_state = $addressInvoice->id_state ? new State($addressInvoice->id_state) : false;
//                            
//                            $data = array(
//                            '{firstname}' => $customer->firstname,
//                            '{lastname}' => $customer->lastname,
//                            '{email}' => $customer->email,
//                            '{delivery_block_txt}' => getFormatedAddress($address, "\n"),
//                            '{invoice_block_txt}' => getFormatedAddress($addressInvoice, "\n"),
//                            '{delivery_block_html}' => getFormatedAddress($address, '<br />', array(
//                                'firstname'	=> '<span style="font-weight:bold;">%s</span>',
//                                'lastname'	=> '<span style="font-weight:bold;">%s</span>'
//                            )),
//                            '{invoice_block_html}' => getFormatedAddress($addressInvoice, '<br />', array(
//                                    'firstname'	=> '<span style="font-weight:bold;">%s</span>',
//                                    'lastname'	=> '<span style="font-weight:bold;">%s</span>'
//                            )),
//                            '{delivery_company}' => $address->company,
//                            '{delivery_firstname}' => $address->firstname,
//                            '{delivery_lastname}' => $address->lastname,
//                            '{delivery_address1}' => $address->address1,
//                            '{delivery_address2}' => $address->address2,
//                            '{delivery_city}' => $address->city,
//                            '{delivery_postal_code}' => $address->postcode,
//                            '{delivery_country}' => $address->country,
//                            '{delivery_state}' => $address->id_state ? $address_state->name : '',
//                            '{delivery_phone}' => ($address->phone) ? $address->phone : $address->phone_mobile,
//                            '{delivery_other}' => $address->other,
//                            '{invoice_company}' => $addressInvoice->company,
//                            '{invoice_vat_number}' => $addressInvoice->vat_number,
//                            '{invoice_firstname}' => $addressInvoice->firstname,
//                            '{invoice_lastname}' => $addressInvoice->lastname,
//                            '{invoice_address2}' => $addressInvoice->address2,
//                            '{invoice_address1}' => $addressInvoice->address1,
//                            '{invoice_city}' => $addressInvoice->city,
//                            '{invoice_postal_code}' => $addressInvoice->postcode,
//                            '{invoice_country}' => $addressInvoice->country,
//                            '{invoice_state}' => $addressInvoice->id_state ? $addressInvoice_state->name : '',
//                            '{invoice_phone}' => ($addressInvoice->phone) ? $addressInvoice->phone : $addressInvoice->phone_mobile,
//                            '{invoice_other}' => $addressInvoice->other,
//                            '{order_name}' => $order->getUniqReference(),
//                            '{date}' => Tools::displayDate(date('Y-m-d H:i:s'),null , 1),
//                            '{carrier}' => $carrier->name,
//                            '{payment}' => Tools::substr($order->payment, 0, 32),
//                            '{products}' => 'gdzie to',
//                            '{products}' => $products_list,
//                            '{discounts}' => '',
//                            '{total_paid}' => Tools::displayPrice($order->total_paid, $order->id_currency, false),
//                            '{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $order->id_currency, false),
//                            '{total_discounts}' => Tools::displayPrice($order->total_discounts, $order->id_currency, false),
//                            '{total_shipping}' => Tools::displayPrice($order->total_shipping, $order->id_currency, false),
//                            '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $order->id_currency, false),
//                            '{total_tax_paid}' => Tools::displayPrice(($order->total_products_wt - $order->total_products) + ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl), $order->id_currency, false));
//							
//                            Mail::Send(
//								(int)$order->id_lang,
//								'order_conf',
//								Mail::l('Order confirmation', (int)$order->id_lang),
//								$data,
//								$customer->email,
//								$customer->firstname.' '.$customer->lastname,
//								null,
//								null,
//								null,
//								null, _PS_MAIL_DIR_, false, (int)$order->id_shop
//							);
//                            End
                            
                        }
                    }
                }
            }
            
            Db::getInstance()->execute('COMMIT');
        }
    }
    
