<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }
    
    require_once (dirname(__FILE__) . '/x13allegro.ion.php');
    
    if (strlen(X13_ION_VERSION)) {
        $pattern = '/.*-7\.php$/';
    } else {
        $pattern = '/.*[^-7]\.php$/';
    }
    
    $path = dirname(__FILE__) . '/classes/';
    $dir = dir($path);
    while (false !== ($file = $dir->read())) {
        if (!is_dir($path . $file) && preg_match($pattern, $file)) {
            require_once ($path . $file);
        }
    }
    
    class x13allegro extends Module {
        
        public $update = false;
        
        protected $_tabs = array(
            'AdminXAllegroMain' => 'Allegro',
            'AdminXAllegroTemplates' => 'Szablony',
            'AdminXAllegroAccounts' => 'Konta',
            'AdminXAllegroPas' => 'Płatność i dostawa',
            'AdminXAllegroCategories' => 'Kategorie',
            'AdminXAllegroAuctions' => 'Lista aukcji',
            'AdminXAllegroOverlay' => 'Ramki miniaturek',
            'AdminXAllegroStatus' => 'Statusy zamowień',
            'AdminXAllegroCarriers' => 'Przewoźnicy',
            'AdminXAllegroConfiguration' => 'Konfiguracja',
            'AdminXAllegroAuctionsBids' => '@Oferty w aukcji'
        );

        public function __construct()
        {
            $this->name = 'x13allegro';
            $this->tab = 'market_place';
            $this->version = '3.6.3';
            $this->author = 'X13.pl';
            $this->need_instance = 0;
            $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7'); 
            
            parent::__construct();
            
			if(Tools::version_compare(1.6, _PS_VERSION_, '>')) {
				$this->ps_version = 1.5;
				$this->bootstrap = false;
			}
			else {
				$this->bootstrap = true;
				$this->ps_version = 1.6;
			}
            
            $this->displayName = $this->l('X13Allegro');
            $this->description = $this->l('Integracja z Allegro.pl');
        }
        
		
        public function hookDisplayBackOfficeHeader()
        {
            $this->context->controller->addJquery();
            $this->context->controller->addjqueryPlugin('fancybox');
			$this->context->controller->addCSS(($this->_path) . 'css/x13allegro.css');
			$this->context->controller->addJS(($this->_path) . 'js/x13allegro-'.$this->ps_version.'.js');
            $this->context->controller->addJS(($this->_path) . 'js/imgareaselect/scripts/jquery.imgareaselect.js');
            $this->context->controller->addCSS(($this->_path) . 'js/imgareaselect/css/imgareaselect-default.css');
             
            $content = '';
            $content .= '<script type="text/javascript">';
            if($this->ps_version == 1.5) 
            $content .= 'var token = \''.Tools::getValue('token').'\';';
            $content .= 'var admin_xallegro_ajax_url = \'' . $this->context->link->getAdminLink($this->context->controller->tabAccess['class_name']) . '\';';
            $content .= '$(function() {';
            $content .= 'var XAllegro = $.X13Allegro();';
            
            $XAllegroUpdate = new XAllegroUpdate();
            $this->update = $XAllegroUpdate->checkUpdate();
            if ($this->update) {
                $message = 'X13Allegro - Dostępna nowa wersja modulu <b>' . $this->update->attributes()->version . '</b>. Kliknij <a style="text-decoration:underline;color:inherit;font-weight:bold;" href="'.$this->context->link->getAdminLink('AdminXAllegroConfiguration').'&update'.'">TUTAJ</a> aby zainstalować aktualizację.';
                $content .= 'XAllegro.showMessage(\'' . $message . '\');';
            }
            $content .= '});';
            $content .= '</script>';
            $content .= 
                '<style>' .
                'span.process-icon-save.xallegro,' .
                'span.process-icon-perform.xallegro {' .
                'background-image: url(\'../modules/x13allegro/logo.png\'); !important'  .
                '}' . 
                'span.process-icon-test.xallegro {' .
                'background-image: url(\'themes/default/img/process-icon-preview.png\'); !important'  .
                '}' . 
                '#xallegro_configuration_form label {' .
                'width: 350px;' .
                '}' .
                '#xallegro_configuration_form .margin-form {' .
                'padding-left: 370px;' .
                '}' .
				'.icon-AdminXAllegroMain:before{content: "\f0e3";}'.
                '</style>';
            
            return $content;
        }
        
        public function hookActionUpdateQuantity($params)
        {
			try {
				if(strpos($_SERVER['SCRIPT_NAME'], 'sync.php') !== false) {
					XAllegroLogger::getInstance()
						->setAuction(null)
						->setType('inf')
						->setCode('HOOK-SKIP')
						->setMessage(array('id_product' => $params['id_product'], 'id_product_attribute' => $params['id_product_attribute'], 'quantity' => $params['quantity']))
						->save();
				}
				elseif(XAllegroConfiguration::get('QUANITY_ALLEGRO_UPDATE')) {
					XAllegroLogger::getInstance()
						->setAuction(null)
						->setType('inf')
						->setCode('HOOK-REQU')
						->setMessage(array('id_product' => $params['id_product'], 'id_product_attribute' => $params['id_product_attribute'], 'quantity' => $params['quantity']))
						->save();
						
					$auctions = DB::getInstance()->executeS('SELECT a.id_auction, a.id_product, a.id_product_attribute, a.quantity, a.quantity_start, ac.username, ac.password, ac.apikey, ac.sandbox, ac.country_code '
						. 'FROM ' . _DB_PREFIX_ . 'xallegro_auction AS a '
						. 'LEFT JOIN ' . _DB_PREFIX_ . 'xallegro_account AS ac '
						. 'ON (a.id_allegro_account = ac.id_xallegro_account) '
						. 'WHERE a.id_product = ' . (int)$params['id_product'] . ' '
						. 'AND a.id_product_attribute = ' . (int)$params['id_product_attribute'] . ' '
						. 'AND a.closed = 0');
					
					$quantity = $params['quantity'];
					
					foreach ($auctions as $auction) {
						if ($quantity < $auction['quantity']) {
							//diff = 'ilość sprzedanych'
							$diff = $auction['quantity_start'] - $auction['quantity'];
							$API = new XAllegroAPI($auction);
							
							if ($quantity > 0) {
								$data = array();
								$data['quantity_start'] = $quantity+$diff;
								$data['quantity'] = $quantity;
								try {
									$result = $API->API()->ChangeQuantityItem(array(
										'item-id' => (float)$auction['id_auction'],
										'new-item-quantity' => $quantity+$diff
									));
									XAllegroLogger::getInstance()
										->setAuction($auction['id_auction'])
										->setType('set')
										->setCode('HOOK_UPDT')
										->setMessage(array('method' => 'ChangeQuantityItem', 'request' => $data, $result))
										->save();
									Db::getInstance()->update('xallegro_auction', $data, '`id_auction` = \'' . pSQL($auction['id_auction']) . '\'');
								}
								catch (Exception $e) { 
									XAllegroLogger::getInstance()
										->setAuction($auction['id_auction'])
										->setType('err')
										->setCode('HOOK_UERR')
										->setMessage(array('method' => 'ChangeQuantityItem', 'request' => $data, 'api_error_message' => $e->getMessage()))
										->save();
								}
							} else {
								$data['quantity'] = 0;
								$data['quantity_start'] = $quantity+$diff;
								$data['closed'] = 1;
								try {
									$result = $API->API()->FinishItem(array(
										'finish-item-id' => (float)$auction['id_auction'],
										'finish-cancel-all-bids' => 0,
										'finish-cancel-reason' => ''
									));
									XAllegroLogger::getInstance()
										->setAuction($auction['id_auction'])
										->setCode('HOOK_FNSH')
										->setType('set')
										->setMessage(array('method' => 'FinishItem', $result))
										->save();
									Db::getInstance()->update('xallegro_auction', $data, '`id_auction` = \'' . pSQL($auction['id_auction']) . '\'');
								} 
								catch (Exception $e) { 
									XAllegroLogger::getInstance()
										->setAuction($auction['id_auction'])
										->setType('err')
										->setCode('HOOK_FERR')
										->setMessage(array('method' => 'FinishItem', 'api_error_message' => $e->getMessage()))->save();
								}
							}
						}
					}
				}
			}
			catch(Exception $e) {
				//do nothing
			}
        }
        
        public function install()
        {
            if( !parent::install() || 
                !$this->registerHook('backoffice') || 
                !$this->registerHook('backOfficeHeader') ||
                !$this->registerHook('actionUpdateQuantity') ||
                !$this->registerHook('actionValidateOrder')
                ){
                return false;
            } else {
                $parent_id = 0;
                
                foreach ( $this->_tabs as $class => $name ) {
                    $tab = new Tab();
                    $tab->active = $name[0] == '@' ? 0 : 1;
                    
                    if ($name[0] == '@') {
                        $name = substr($name, 1);
                    }
                    
                    $tab->class_name = $class;
                    $tab->name = array();
                    foreach (Language::getLanguages(true) as $item) {
                        $tab->name[$item['id_lang']] = $name;
                    }
                    $tab->id_parent = $parent_id;
                    $tab->module = $this->name;
                    $tab->add();
                    
                    if ( $parent_id == 0 ) {
                        $parent_id = $tab->id;
                    }
                }
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_account`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_account` (
                    `id_xallegro_account` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `username` varchar(50) NOT NULL,
                    `password` varchar(50) NOT NULL,
                    `apikey` varchar(50) NOT NULL,
                    `country_code` int(10) unsigned NOT NULL,
                    `country` int(10) unsigned NOT NULL,
                    `shop` tinyint(1) NOT NULL,
                    `default` tinyint(1) NOT NULL,
                    `active` tinyint(1) NOT NULL,
                    `sandbox` tinyint(1) NOT NULL,
                    `last_point` bigint(20) unsigned NOT NULL DEFAULT \'0\',
                    `last_deals_point` bigint(20) unsigned NOT NULL DEFAULT \'0\',
                    PRIMARY KEY (`id_xallegro_account`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_auction`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_auction` (
                    `id_xallegro_auction`    int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `id_shop`                int(10) unsigned NOT NULL DEFAULT 1,
                    `id_shop_group`          int(10) unsigned NOT NULL DEFAULT 1,
                    `id_auction`             bigint(10)       NOT NULL,
                    `id_allegro_account`     int(10) unsigned NOT NULL,
                    `id_product`             int(10) unsigned NOT NULL DEFAULT \'0\',
                    `id_product_attribute`   int(10) unsigned NOT NULL DEFAULT \'0\',
                    `quantity`               int(10) unsigned NOT NULL,
                    `quantity_start`         int(10) unsigned NOT NULL,
                    `allegro_shop`           TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0,
                    `abroad`                 TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0,
                    `invoice`                TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0,
                    `bank_transfer`          TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0,
                    `account_number`         VARCHAR(64)  NOT NULL DEFAULT \'\',
                    `account_number_second`  VARCHAR(64)  NOT NULL DEFAULT \'\',
                    `pas`                    SMALLINT(5)  UNSIGNED,
                    `state`                  VARCHAR(128) NOT NULL DEFAULT \'\',
                    `city`                   VARCHAR(128) NOT NULL DEFAULT \'\',
                    `zipcode`                CHAR(6)      NOT NULL DEFAULT \'00-000\',
                    `id_allegro_category`    VARCHAR(255) NOT NULL DEFAULT \'\',
                    `category_fields`        TEXT,
                    `item`                   TEXT,
                    `shipping_cost`          TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0,
                    `shipment`               TEXT,
                    `prepare_time`           SMALLINT(5)  UNSIGNED,
                    `details`                TEXT,
                    `start`                  TINYINT(1),
                    `start_time`             VARCHAR(32),
                    `default`                TINYINT(1),
                    `resume_auction` TINYINT(1) NOT NULL DEFAULT \'0\',
                    `closed` int(10) unsigned NOT NULL DEFAULT \'0\',
                    PRIMARY KEY (`id_xallegro_auction`),
                    INDEX (`id_auction`),
                    INDEX (`id_allegro_account`),
                    INDEX (`id_product`),
                    INDEX (`id_product_attribute`),
                    INDEX (`default`),
                    INDEX (`resume_auction`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_category`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_category` (
                    `id_xallegro_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `id_category` int(10) unsigned NOT NULL,
                    `id_allegro_category` int(10) unsigned NOT NULL,
                    `id_allegro_shop_category` int(10) unsigned NOT NULL,
                    `path` varchar(255) NOT NULL,
                    `fields` longtext NOT NULL,
                    PRIMARY KEY (`id_xallegro_category`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_configuration`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_configuration` (
                    `id_xallegro_configuration` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `name` varchar(32) NOT NULL,
                    `value` text NOT NULL,
                    PRIMARY KEY (`id_xallegro_configuration`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_form`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_form` (
                    `id_xallegro_form` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `id_allegro_form` bigint(10) unsigned NOT NULL,
                    `id_order` int(10) unsigned NULL,
                    `content` text,
                    PRIMARY KEY (`id_xallegro_form`),
                    UNIQUE (`id_allegro_form`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_overlay`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_overlay` (
                    `id_xallegro_overlay` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `name` varchar(50) NOT NULL,
                    `default` tinyint(1) NOT NULL,
                    `active` tinyint(1) NOT NULL,
                    `X1` int(10) unsigned NOT NULL DEFAULT \'0\',
                    `Y1` int(10) unsigned NOT NULL DEFAULT \'0\',
                    `X2` int(10) unsigned NOT NULL DEFAULT \'0\',
                    `Y2` int(10) unsigned NOT NULL DEFAULT \'0\',
                    PRIMARY KEY (`id_xallegro_overlay`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_pas`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_pas` (
                    `id_xallegro_pas` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `name` varchar(50) NOT NULL,
                    `state` int(10) unsigned NOT NULL,
                    `city` varchar(50) NOT NULL,
                    `zipcode` varchar(10) NOT NULL,
                    `shipping_cost` tinyint(1) NOT NULL,
                    `customer_pickup` tinyint(1) NOT NULL,
                    `shipment_email` tinyint(1) NOT NULL,
                    `invoice` tinyint(1) NOT NULL,
                    `abroad` tinyint(1) NOT NULL,
                    `details` text NOT NULL,
                    `bank_transfer` tinyint(1) NOT NULL,
                    `account_number` varchar(35) NOT NULL,
                    `account_number_second` varchar(35) NOT NULL,
                    `prepare_time` int(10) unsigned NOT NULL,
                    `shipments` longtext NOT NULL,
                    `default` tinyint(1) NOT NULL,
                    `active` tinyint(1) NOT NULL,
                    PRIMARY KEY (`id_xallegro_pas`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_product`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_product` (
                    `id_xallegro_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `id_product` int(10) unsigned NOT NULL,
                    `id_product_attribute` int(10) unsigned NOT NULL,
                    `description` text NOT NULL,
                    PRIMARY KEY (`id_xallegro_product`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_site`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_site` (
                    `id_xallegro_site` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `name` varchar(32) NOT NULL,
                    `url` varchar(255) NOT NULL,
                    `country_code` int(10) unsigned NOT NULL,
                    PRIMARY KEY (`id_xallegro_site`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');
                
                Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'xallegro_site` (`id_xallegro_site`, `name`, `url`, `country_code`) VALUES (1, \'Allegro.pl\', \'http://allegro.pl\', 1);');
                
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_template`');
                Db::getInstance()->Execute('
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_template` (
                    `id_xallegro_template` int(11) unsigned NOT NULL AUTO_INCREMENT,
                    `name` varchar(50) NOT NULL,
                    `content` longtext NOT NULL,
                    `default` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
                    `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
                    PRIMARY KEY (`id_xallegro_template`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
                ');

				Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'xallegro_status`');
				$sql = '
				CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_status` (
					`id_xallegro_status`     INT(10)      UNSIGNED NOT NULL AUTO_INCREMENT,
					`id_order_state`         INT(10)      NOT NULL DEFAULT 0,
					`id_allegro_state`       VARCHAR(255) NOT NULL DEFAULT \'\',
					`allegro_name`           VARCHAR(255) NOT NULL DEFAULT \'\',
					`position`               INT(10) NOT  NULL DEFAULT 0,
				PRIMARY KEY(`id_xallegro_status`),
				UNIQUE(`id_allegro_state`),
				INDEX(`position`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
				Db::getInstance()->Execute($sql);
				
				//Wypełnianie tabeli statusuów
				$sql = '
					INSERT IGNORE INTO `'._DB_PREFIX_.'xallegro_status`(`id_order_state`, `id_allegro_state`, `allegro_name`, `position`)
					VALUES
						(12, \'co\',                  \'Status dla płatności Checkout PayU\',       0),
						(3,  \'ai\',                  \'Status dla rat PayU\',                      1),
						(3,  \'collect_on_delivery\', \'Status dla platnosci przy odbiorze (COD)\', 2),
						(10, \'wire_transfer\',       \'Status dla przelewu bankowego\',            3),
						(3,  \'not_specified\',       \'Pozostałe statusy\',                        4),
						(3,  \'Rozpoczęta\',          \'Status PayU - rozpoczęta\',                 5),
						(6,  \'Anulowana\',           \'Status PayU - anulowana\',                  6),
						(2,  \'Zakończona\',          \'Status PayU - zakończona\',                 7)
				';
				Db::getInstance()->Execute($sql);
				
				//Tworzenie tabeli dla metod dostawy
					$sql = '
				CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_carrier` ( 
					`id_fields_shipment` INT(10)   PRIMARY KEY NOT NULL DEFAULT 0, 
					`id_carrier`         INT(10)   NOT NULL DEFAULT 0, 
				INDEX(`id_carrier`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
				Db::getInstance()->Execute($sql);
				
				$sql = '
				CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro__sell_form_fields` (
					`country_code`            INT(10) UNSIGNED NOT NULL,
					`sell_form_id`            INT(10) UNSIGNED NOT NULL,
					`sell_form_title`         VARCHAR(255),
					`sell_form_cat`           INT(10) UNSIGNED,
					`sell_form_type`          INT(10) UNSIGNED,
					`sell_form_res_type`      INT(10) UNSIGNED,
					`sell_form_def_value`     INT(10) UNSIGNED,
					`sell_form_opt`           INT(10) UNSIGNED,
					`sell_form_pos`           INT(10) UNSIGNED,
					`sell_form_length`        INT(10) UNSIGNED,
					`sell_min_value`          DECIMAL(10,2),
					`sell_max_value`          DECIMAL(10,2),
					`sell_form_desc`          TEXT,
					`sell_form_opts_values`   TEXT,
					`sell_form_field_desc`    TEXT,
					`sell_form_param_id`      INT(10) UNSIGNED,
					`sell_form_param_values`  TEXT,
					`sell_form_parent_id`     INT(10) UNSIGNED,
					`sell_form_parent_value`  INT(10) UNSIGNED,
					`sell_form_unit`          VARCHAR(16),
					`sell_form_options`       INT(10) UNSIGNED,
				UNIQUE(`country_code`, `sell_form_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8';
				Db::getInstance()->Execute($sql);

				$sql = '
				CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro__cats_list` (
					`country_code`           INT(10) UNSIGNED NOT NULL,
					`id`                     INT(10) UNSIGNED NOT NULL,
					`name`                   VARCHAR(128),
					`parent_id`              INT(10) UNSIGNED,
					`position`               INT(10) UNSIGNED,
				UNIQUE(`country_code`, `id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8';
                Db::getInstance()->Execute($sql);
                
                $configuration = array(
					'VERSION_TIMESTAMP' => 0,
					'VERSION_CATEGORY_LIST' => 0,
					'VERSION_SELL_FORM_FIELDS' => 0,
                    'TITLE_PATTERN' => '{product_name} {product_name_attribute}',
                    'MARKUP' => 0,
                    'DURATION_DEFAULT' => 4,
                    'SELECT_ALL' => 1,
                    'PRICE_ROUND' => 0,
                    'IMAGES_COMBINATION' => 0,
                    'SEND_ADDITIONAL_IMAGES' => 0,
                    'IMPORT_ORDERS' => 1,
                    'ORDER_SEND_MAIL' => 0,
                    'QUANITY_CHECK' => 1,
                    'QUANITY_SHOP_UPDATE' => 1,
                    'QUANITY_ALLEGRO_UPDATE' => 1,
                    'QUANITY_ALLEGRO_UPDATE_CHUNK' => 100,
                    'QUANITY_ALLEGRO_ALWAYS_MAX' => 0,
                    'OVERLAY_UNDER_IMAGE' => 0,
                    'VERSION' => $this->version,
                    'SYNC_TOKEN' => Tools::passwdGen()
                );
                
                foreach ($configuration as $key => $value) {
                    XAllegroConfiguration::updateValue($key, $value);
                }
                
                if (!file_exists(_PS_IMG_DIR_ . 'overlay')) {
                    mkdir(_PS_IMG_DIR_ . 'overlay');
                }
                
                return true;
            }
        }
        
        public function uninstall()
        {
            $tabs = Tab::getCollectionFromModule($this->name);
            
            if (!empty($tabs))
            {
                foreach ($tabs as $item)
                {
                    $item->delete();
                }
            }
            
            return parent::uninstall();
        }
    }
