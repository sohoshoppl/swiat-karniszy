<?php

class XAllegroTemplate extends ObjectModel {

	public $id_xallegro_template ;
	public $name;
	public $content;
	public $default ;
	public $active;
	
	private $features;
	private $attributes;

	public static $definition = array(
		'table' => 'xallegro_template',
		'primary' => 'id_xallegro_template',
		'multilang' => false,
		'fields' => array(
			'name' => array('type' => self::TYPE_STRING),
			'content' => array('type' => self::TYPE_HTML),
			'default' => array('type' => self::TYPE_BOOL),
			'active' => array('type' => self::TYPE_BOOL)
		)
	);
	
	public static $re = array(
		'@^(product|product_lang)\.([a-z0-9_]+)$@' => '_getFromDB',
		'@^manufacturer_name$@' => '_manufacturerName',
		'@^manufacturer_logo$@' => '_manufacturerLogo',
		'@^feature_(name|value)_([0-9]+)$@' => '_singleFeature',
		'@^attribute_(name|value)_([0-9]+)$@' => '_singleAttribute',
		'@^gallery$@' => '_gallery',
		'@^gallery_full$@' => '_gallery_full',
		'@^gallery_link$@' => '_gallery_link',
		'@^gallery_image$@' => '_gallery_image',
		'@^product_features$@' => '_features',
		'@^image_([a-z\_]+)_([0-9]+)$@' => '_image'
	);
	
	public function setDefault() {
		if((int)$this->id > 0) {
			$sql = '
				UPDATE `'._DB_PREFIX_.'xallegro_template`
				SET `default` = 0
				WHERE `id_xallegro_template` <> '.(int)$this->id.'
			';
			Db::getInstance()->execute($sql);
		}
		return ;
	}

	public function switchActive($id) {
		$sql = '
			UPDATE `'._DB_PREFIX_.'xallegro_template`
			SET `active` = MOD(`active` + 1, 2)
			WHERE `id_xallegro_template` = '.(int)$id.'
		';
		return Db::getInstance()->execute($sql);
	}

	public function switchDefault($id) {
		$sql = '
			UPDATE `'._DB_PREFIX_.'xallegro_template`
			SET `default` = IF(`id_xallegro_template` = '.(int)$id.', 1, 0)
		';
		return Db::getInstance()->execute($sql);
	}
	
	protected $_product;
	protected $_productImages = false;
	protected $data;
	
	public function hookDisplayAdminListBefore()
	{
		echo 'xxx';
	}
	
	public function setProduct($product)
	{
		$this->_product = $product;
		$this->features = array();
		$this->attributes = array();
	}
	
	public function setProductAttribute($attributes) {
		$this->attributes = $attributes;
	}
	
	public function render($data)
	{
		$this->data = $data;
		$content = $this->content;
		preg_match_all('@\{([a-z0-9_\.]+)\}@', $content, $variables);
		foreach ($variables[1] as $variable) {
			foreach (self::$re as $regex => $callback) {
				if (preg_match($regex, $variable, $match)) {
					$value = call_user_func(array($this, $callback), $match);
				}
			}
			
			if (!isset($value) && array_key_exists($variable, $data)) {
				$value = $data[$variable];
			}
			
			if (isset($value)) {
				$content = str_replace('{' . $variable . '}', $value, $content);
				
				unset($value);
			}
		}
		
		return $content;
	}
	
	protected function _getFromDB($match)
	{
		$sql = 'SELECT * FROM `' . _DB_PREFIX_ . $match[1] . '` WHERE `id_product` = ' . (int) $this->_product->id;
		
		$res = Db::getInstance()->executeS($sql);
		
		foreach ($res as $row) {
			if (isset($row[$match[2]])) {
				return $row[$match[2]];
			}
		}
	}
	
	protected function _manufacturerName()
	{
		return Manufacturer::getNameById($this->_product->id_manufacturer);
	}
	
	protected function _manufacturerLogo()
	{
		return '<img src="' . _PS_BASE_URL_ . _THEME_MANU_DIR_ . $this->_product->id_manufacturer . '.jpg" />';
	}
	
	protected function _gallery()
	{
		if (!isset($this->data['images']) || !$this->data['images']) {
			return;
		}
		
		$return = '<div class="x13allegro_galeria">
		<div class="x13allegro_container">
		<div class="x13allegro_zdj"></div>';
		
		$link = new Link();
		
		foreach ($this->data['images'] as $index => $idImage) {
			$return .= '
			<div class="x13allegro_image_li" >
				<a class="a_thumb"><div style="background-image: url(http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'medium_default') . ');"></div></a>
				<a class="a_hidden" '.(($index == 0) ? ' style="z-index:2"' : '').'><img src="http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'thickbox_default') . '" /></a>
			</div>
			';
		}
		
		$return .= '</div>
		</div>';
		
	
		
		return $return;
	}
	
	protected function _gallery_full()
	{
		if (!isset($this->data['images']) || !$this->data['images']) {
			return;
		}
		
		$return = '<div class="x13allegro_galeria">
		<div class="x13allegro_container">
		<div class="x13allegro_zdj"></div>';
		
		$link = new Link();
		
		foreach ($this->data['images'] as $index => $idImage) {
			$return .= '
			<div class="x13allegro_image_li" >
				<a class="a_thumb"><div style="background-image: url(http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'medium_default') . ');"></div></a>
				<a class="a_hidden" '.(($index == 0) ? ' style="z-index:2"' : '').'><img src="http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, '') . '" /></a>
			</div>
			';
		}
		
		$return .= '</div>
		</div>';
		
	
		
		return $return;
	}
	
		protected function _gallery_link()
	{
		if (!isset($this->data['images']) || !$this->data['images']) {
			return;
		}
		
		$return = '<div class="x13allegro_galeria">
		<div class="x13allegro_container">
		<div class="x13allegro_zdj"></div>';
		
		$link = new Link();
		
		foreach ($this->data['images'] as $index => $idImage) {
			$return .= '
			<div class="x13allegro_image_li" >
				<a class="a_thumb" href="http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'thickbox_default') . '" target="_blank"><div style="background-image: url(http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'medium_default') . ');"></div></a>
				<a class="a_hidden" '.(($index == 0) ? ' style="z-index:2"' : '').'><img src="http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'thickbox_default') . '" /></a>
			</div>
			';
		}
		
		$return .= '</div>
		</div>';
		
	
		
		return $return;
	}

	
	protected function _gallery_image()
	{
		if (!isset($this->data['images']) || !$this->data['images']) {
			return;
		}
		
		$return = '<div class="x13allegro_galeria_image">';
		
		$link = new Link();
		
		foreach ($this->data['images'] as $index => $idImage) {
			$return .= '
			<div class="x13allegro_galeria_image_li" >
				<img src="http://' . $link->getImageLink($this->_product->link_rewrite, $idImage, 'thickbox_default') . '" />
			</div>
			';
		}
		
		$return .= '</div>';
		
	
		
		return $return;
	}

	
	protected function _singleFeature($match) {
		if(count($match) != 3) return null;
		if(empty($this->features)) {
			$this->features = $this->_product->getFrontFeatures(Context::getContext()->language->id);
		}
		foreach($this->features as $f) {
			if($match[2] == $f['id_feature']) {
				return $f[$match[1]];
			}
		}
		return '';
	}
	
	protected function _singleAttribute($match) {
		if(!empty($this->attributes)) {
			foreach($this->attributes as $a) {
				if($match[2] == $a['id_attribute_group']) {
					return ($match[1] === 'name')
					? $a['group_name']
					: $a['attribute_name'];
				}
			}
		}
		return '';
	}
	
	protected function _features()
	{
		if(empty($this->features)) {
			$this->features = $this->_product->getFrontFeatures(Context::getContext()->language->id);
		}
		$return = '<ul>';
		
		foreach ($this->features as $feature) { 
			$return .= '<li>' . $feature['name'] . ' <span>' . $feature['value'] . '</span></li>';
		}
		
		$return .= '</ul>';
		
		return $return;
	}
	
	protected function _image($match)
	{
		if (!isset($this->data['images'][$match[2]-1])) {
			return ' ';
		}
		
		$type = $match[1];
		
		if ($type == 'oryginal') {
			$type = '';
		}
		
		$link = new Link();
		
		return '<img src="http://' . $link->getImageLink($this->_product->link_rewrite, $this->data['images'][$match[2]-1], $type) . '"/>';
	}
}
