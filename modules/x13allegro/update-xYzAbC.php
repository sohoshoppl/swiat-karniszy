<?php

define('_PRESTA_DIR_', dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME']))));

require_once(_PRESTA_DIR_ . '/config/config.inc.php');
require_once(_PRESTA_DIR_ . '/init.php');

/*

$sql = '
SET @s = (SELECT IF(
	(SELECT COUNT(*)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = \''._DB_PREFIX_.'xallegro_auction\'
		AND table_schema = DATABASE()
		AND column_name = \'resume_auction\'
	) > 0,
	"SELECT 1",
	"ALTER TABLE `'._DB_PREFIX_.'xallegro_auction` 
		ADD `resume_auction` TINYINT(1) NOT NULL DEFAULT 0
			AFTER `quantity_start`, 
		ADD INDEX (`resume_auction`)"
));
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt';
Db::getInstance()->execute($sql);

$sql = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xallegro__sell_form_fields` (
	`country_code`            INT(10) UNSIGNED NOT NULL,
	`sell_form_id`            INT(10) UNSIGNED NOT NULL,
	`sell_form_title`         VARCHAR(255),
	`sell_form_cat`           INT(10) UNSIGNED,
	`sell_form_type`          INT(10) UNSIGNED,
	`sell_form_res_type`      INT(10) UNSIGNED,
	`sell_form_def_value`     INT(10) UNSIGNED,
	`sell_form_opt`           INT(10) UNSIGNED,
	`sell_form_pos`           INT(10) UNSIGNED,
	`sell_form_length`        INT(10) UNSIGNED,
	`sell_min_value`          DECIMAL(10,2),
	`sell_max_value`          DECIMAL(10,2),
	`sell_form_desc`          TEXT,
	`sell_form_opts_values`   TEXT,
	`sell_form_field_desc`    TEXT,
	`sell_form_param_id`      INT(10) UNSIGNED,
	`sell_form_param_values`  TEXT,
	`sell_form_parent_id`     INT(10) UNSIGNED,
	`sell_form_parent_value`  INT(10) UNSIGNED,
	`sell_form_unit`          VARCHAR(16),
	`sell_form_options`       INT(10) UNSIGNED,
UNIQUE(`country_code`, `sell_form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8';
Db::getInstance()->execute($sql);

$sql = '
	DROP TABLE IF EXISTS `'._DB_PREFIX_.'xallegro__cats_list`
';
Db::getInstance()->execute($sql);

$sql = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xallegro__cats_list` (
	`country_code`           INT(10) UNSIGNED NOT NULL,
	`id`                     INT(10) UNSIGNED NOT NULL,
	`name`                   VARCHAR(128),
	`parent_id`              INT(10) UNSIGNED,
	`position`               INT(10) UNSIGNED,
UNIQUE(`country_code`, `id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8';
Db::getInstance()->execute($sql);

$sql = '
	DROP TABLE IF EXISTS `'._DB_PREFIX_.'xallegro__sell_form_fields`
';
Db::getInstance()->execute($sql);

$sql = '
CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xallegro__sell_form_fields` (
	`country_code`            INT(10) NOT NULL,
	`sell_form_id`            INT(10) NOT NULL,
	`sell_form_title`         VARCHAR(255),
	`sell_form_cat`           VARCHAR(255),
	`sell_form_type`          VARCHAR(255),
	`sell_form_res_type`      VARCHAR(255),
	`sell_form_def_value`     VARCHAR(255),
	`sell_form_opt`           VARCHAR(255),
	`sell_form_pos`           INT(10),
	`sell_form_length`        INT(10),
	`sell_min_value`          DECIMAL(10,2),
	`sell_max_value`          DECIMAL(10,2),
	`sell_form_desc`          TEXT,
	`sell_form_opts_values`   TEXT,
	`sell_form_field_desc`    TEXT,
	`sell_form_param_id`      INT(10),
	`sell_form_param_values`  TEXT,
	`sell_form_parent_id`     INT(10),
	`sell_form_parent_value`  TEXT,
	`sell_form_unit`          VARCHAR(255),
	`sell_form_options`       TEXT,
UNIQUE(`country_code`, `sell_form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8';
Db::getInstance()->execute($sql);

$sql = '
SET @s = (SELECT IF(
	(SELECT COUNT(*)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = \''._DB_PREFIX_.'xallegro_auction\'
		AND table_schema = DATABASE()
		AND column_name = \'default\'
	) > 0,
	"SELECT 1",
		"ALTER TABLE `'._DB_PREFIX_.'xallegro_auction` 
			ADD `allegro_shop`           TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0  AFTER `quantity_start`,
			ADD `abroad`                 TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0  AFTER `allegro_shop`,
			ADD `invoice`                TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0  AFTER `abroad`,
			ADD `bank_transfer`          TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0  AFTER `invoice`,
			ADD `account_number`         VARCHAR(64)  NOT NULL DEFAULT \'\'        AFTER `bank_transfer`,
			ADD `account_number_second`  VARCHAR(64)  NOT NULL DEFAULT \'\'        AFTER `account_number`,
			ADD `pas`                    SMALLINT(5)  UNSIGNED                     AFTER `account_number_second`,
			ADD `state`                  VARCHAR(128) NOT NULL DEFAULT \'\'        AFTER `pas`,
			ADD `city`                   VARCHAR(128) NOT NULL DEFAULT \'\'        AFTER `state`,
			ADD `zipcode`                CHAR(6)      NOT NULL DEFAULT \'00-000\'  AFTER `city`,
			ADD `id_allegro_category`    VARCHAR(255) NOT NULL DEFAULT \'\'        AFTER `zipcode`,
			ADD `category_fields`        TEXT                                      AFTER `id_allegro_category`,
			ADD `item`                   TEXT                                      AFTER `category_fields`,
			ADD `shipping_cost`          TINYINT(1)   UNSIGNED NOT NULL DEFAULT 0  AFTER `item`,
			ADD `shipment`               TEXT                                      AFTER `shipping_cost`,
			ADD `prepare_time`           SMALLINT(5)  UNSIGNED                     AFTER `shipment`,
			ADD `details`                TEXT                                      AFTER `prepare_time`,
			ADD `start`                  TINYINT(1)   NOT NULL DEFAULT 0           AFTER `details`,
			ADD `start_time`             VARCHAR(32)  NOT NULL DEFAULT \'\'        AFTER `start`,
			ADD `default`                TINYINT(1)   NOT NULL DEFAULT 0           AFTER `resume_auction`,
			ADD INDEX (`id_auction`),
			ADD INDEX (`id_allegro_account`),
			ADD INDEX (`id_product`),
			ADD INDEX (`id_product_attribute`),
			ADD INDEX (`default`),
			CHANGE `id_auction` `id_auction` BIGINT(10) NOT NULL"
));
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt';
Db::getInstance()->execute($sql);

$sql = '
	UPDATE `'._DB_PREFIX_.'xallegro_account`
	SET `last_deals_point` = 0
	WHERE `last_deals_point` = 4294967295
';
Db::getInstance()->execute($sql);

*/

$sql = '
SET @s = (SELECT IF(
	(SELECT COUNT(*)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = \''._DB_PREFIX_.'xallegro_auction\'
		AND table_schema = DATABASE()
		AND column_name = \'id_shop\'
	) > 0,
	"SELECT 1",
	"ALTER TABLE `'._DB_PREFIX_.'xallegro_auction` 
		ADD `id_shop`       INT UNSIGNED NOT NULL DEFAULT 1 AFTER `id_xallegro_auction` ,
		ADD `id_shop_group` INT UNSIGNED NOT NULL DEFAULT 1 AFTER `id_shop` ,
		ADD INDEX ( `id_shop` , `id_shop_group` )"
));
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt';
Db::getInstance()->execute($sql);

$sql = '
CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xallegro_status` (
	`id_xallegro_status`     INT(10)      UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_order_state`         INT(10)      NOT NULL DEFAULT 0,
	`id_allegro_state`       VARCHAR(255) NOT NULL DEFAULT \'\',
	`allegro_name`           VARCHAR(255) NOT NULL DEFAULT \'\',
	`position`               INT(10) NOT  NULL DEFAULT 0,
PRIMARY KEY(`id_xallegro_status`),
UNIQUE(`id_allegro_state`),
INDEX(`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8';
Db::getInstance()->Execute($sql);

//Wypełnianie tabeli statusuów
$sql = '
	INSERT IGNORE INTO `'._DB_PREFIX_.'xallegro_status`(`id_order_state`, `id_allegro_state`, `allegro_name`, `position`)
	VALUES
		(12, \'co\',                  \'Status dla płatności Checkout PayU\',       0),
		(3,  \'ai\',                  \'Status dla rat PayU\',                      1),
		(3,  \'collect_on_delivery\', \'Status dla platnosci przy odbiorze (COD)\', 2),
		(10, \'wire_transfer\',       \'Status dla przelewu bankowego\',            3),
		(3,  \'not_specified\',       \'Pozostałe statusy\',                        4),
		(3,  \'Rozpoczęta\',          \'Status PayU - rozpoczęta\',                 5),
		(6,  \'Anulowana\',           \'Status PayU - anulowana\',                  6),
		(2,  \'Zakończona\',          \'Status PayU - zakończona\',                 7)
';
Db::getInstance()->Execute($sql);

//Dodawanie kart dla przewoźników
//na wszelki wypadek sprawdzam, czy karta już istnieje
$sql = '
	SELECT `id_tab`
	FROM `'._DB_PREFIX_.'tab`
	WHERE `class_name` = \'AdminXAllegroCarriers\'
';

if(!Db::getInstance()->getValue($sql)) {
	
	//pobieram id karty Allegro
	$sql = '
		SELECT `id_tab`
		FROM `'._DB_PREFIX_.'tab`
		WHERE `class_name` = \'AdminXAllegroMain\'
	';
	$id_xallegro_tab = Db::getInstance()->getValue($sql);

	$tab = new Tab();
	$tab->active = 1;
	$tab->class_name = 'AdminXAllegroCarriers';
	$tab->name = array();
	foreach (Language::getLanguages(true) as $item) {
		$tab->name[$item['id_lang']] = 'Przewoźnicy';
	}
	$tab->id_parent = $id_xallegro_tab;
	$tab->module = 'x13allegro';
	$tab->add();
	
}
	
$sql = '
	CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xallegro_carrier` ( 
		`id_fields_shipment` INT(10)   PRIMARY KEY NOT NULL DEFAULT 0, 
		`id_carrier`         INT(10)   NOT NULL DEFAULT 0, 
	INDEX(`id_carrier`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
';
Db::getInstance()->Execute($sql);

$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'_xallegro_carrier`';
Db::getInstance()->Execute($sql);

//usuwamy z xallegro_form rekordy powiązane z nieistniejącymi zamówieniami
$sql = '
	DELETE af.* FROM `'._DB_PREFIX_.'xallegro_form` af
	LEFT JOIN `'._DB_PREFIX_.'orders` o ON af.`id_order` = o.`id_order`
	WHERE o.`id_order` IS NULL
';
Db::getInstance()->execute($sql);

//usuwamy z xallegro_form rekordy ze zdublowanym id_xallegro_form
$sql = '
	DELETE af2.* FROM `'._DB_PREFIX_.'xallegro_form` af1
	LEFT JOIN `'._DB_PREFIX_.'xallegro_form` af2 ON af1.`id_allegro_form` = af2.`id_allegro_form`
	WHERE af1.`id_xallegro_form` < af2.`id_xallegro_form`
';
Db::getInstance()->execute($sql);

//Alter tabeli form, jak już upewnimy się, że usunięto zdublowane wiersze
$sql = '
SET @s = (SELECT IF(
	(SELECT COUNT(*)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = \''._DB_PREFIX_.'xallegro_form\'
		AND table_schema = DATABASE()
		AND column_name = \'content\'
	) > 0,
	"SELECT 1",
	"ALTER TABLE `'._DB_PREFIX_.'xallegro_form` 
		ADD `content` TEXT NOT NULL AFTER `id_order`, 
		ADD UNIQUE (`id_allegro_form`),
		CHANGE `id_order` `id_order` INT(10) UNSIGNED NULL"
));
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt';
Db::getInstance()->execute($sql);

//Rezygnujemy z poleceń samo walidujących się, bo okazuje się, że na niektórych serwerach nie rzucają one błedów i robi się maniana.
//Tym samym ten plik przestaje być uniwersalnym wytrychem do każdej wersji. Problem będzie jedynie z ALTER-ami, bo niektóre z nich dwa razy się nie wykonają - wtedy konieczne będzie wykomentowanie tego co trzeba.
//Lepiej będzie jak klient zobaczy, że coś poszło nie tak i to zgłosi, niż później mamy szukać igły w stogu siana (tym bardziej, że o dostępy trudno).
//To samo będzie trzeba zrobić przy aktualizacji modułu.
//PROBLEM: Jeśli komus zaktualizujesz moduł tym plikiem, to może się wysypywać update. Zalecam używać pliku tylko lokalnie, a u klientów w ostateczności.


echo 'ok';

?>
