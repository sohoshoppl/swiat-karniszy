(function($) {
    $.X13Allegro = function(options) {

        init = function() {
            _cast();
        }
        
        _cast = function() {
            var cast = function() {
                var type = $(this).attr('x-cast');
                
                if ($(this).prop('value') === '') {
                    $(this).prop('value', 0);
                }
                
                if (type == 'float') {
                    $(this).prop('value', formatNumber(formatedNumberToFloat($(this).prop('value').replace(/[^0-9,\.]/g, '0'), 2, null), 2, '', '.'))
                } else if (type == 'integer') {
                    $(this).prop('value', parseInt($(this).prop('value')));
                }
            }
            
            $('[x-cast]').each(cast).on('focusout change xchange', cast);
        }
        
        showMessage = function(message) {
            $('#content').prepend('<div class="hint clear" style="display:block;"><div id="infos_block">' + message + '</div></div><br />');
        }
        
        _shipmentsSwitch = function(element) {
            sw = function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().find('input:text').removeAttr('disabled');
                } else {
                    $(this).parent().parent().find('input:text').attr('disabled', 'disabled');
                }
            }
            
            $(element).on('change', sw).each(sw);
        }
        
        accountForm = function() {
            change = function() {
                if (!$('#apikey').prop('value')) {
                    return;
                }
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCountries',
                        ajax: true,
                        apikey: $('#apikey').prop('value'),
                        country_code: $('#country_code').prop('value'),
                        sandbox: $('[name=sandbox]:checked').prop('value')
                    },
                    beforeSend: function() {
                        $('#fieldset_0').fadeTo('fast', 0.2);
                        $('#country option[value!=0]').remove();
                        $('#country option[value=0]').show();
                    },
                    success: function(data) {
                        if (data.status == 'ok') {
                            $('#country option[value=0]').remove();
                            
                            $.each(data.countries, function(index, country) {
                                $('#country').append($('<option>', { 
                                    value: country['country-id'],
                                    text : country['country-name']
                                }));
                            });
                        } else if (data.status == 'error') {
                            $('#country').append(new Option('Wprowadź poprawny klucz API', '0'));
                        }
                    },
                    complete: function() {
                        $('#fieldset_0').fadeTo('fast', 1);
                    }
                });
            }
            
            $('input#apikey, select#country_code').on('change', change);
        }
        
        pasForm = function() {
            _shipmentsSwitch($('[x-name=switch]'));
        }
        
        categoryForm = function() {
            if (!$('#fieldset_1 > legend').nextAll().size()) {
                $('#fieldset_1').hide();
            }
            
            $('#fieldset_0').on('change', 'select', function() {
                var select = $(this);
                
                if (select.prop('value') == 0) {
                    return;
                }

                var selects = $('#fieldset_0').find('select');
                var index = selects.index($(this));

                for (i=index+1;i<selects.length;i++) {
                    selects.eq(i).remove();
                }
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCategories',
                        ajax : true,
                        id_allegro_category : select.prop('value')
                    },
                    beforeSend: function()
                    {
						select.closest('.panel').fadeTo('fast', 0.2);
                    },
                    success: function(data)
                    {
                        if (data['categories'].length) {
                            var element = select.clone().insertAfter(select).hide();

                            $("option[value!='0']", element).remove();

                            $.each(data['categories'], function(index, category){
                                element.append(new Option(category['name'], category['id']));
                            });

                            element.css('display', 'inline');
                        }

                        if (data['fields']) {
                            $('#fieldset_1_1 .form-wrapper').html(data['fields']);
                            $('#fieldset_1_1').show();
                        }
                    },
                    complete: function() {
                        select.closest('.panel').fadeTo('fast', 1);
                    }
                });
            });
        };
        
        auctionForm = function() {
			
			$('[x-name=allegro_account]').on('change', '[name="allegro_shop"]', function(){
				$.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getAllegroDuration',
                        ajax : true,
                        dataType: 'json',
                        allegro_shop : $('[name="allegro_shop"]:checked').val()
                    },
                    beforeSend: function()
                    {
                        $('[x-name=duration]').fadeTo('fast', 0.2);
                    },
                    success: function(json)
                    {

                        $('[x-name=duration]').each(function() {
                            var select = $(this);
                            var value = select.val();

                            $(this).empty();

                            $.each(json.durations, function (index, duration) {
                                select.append($('<option>', { 
                                    value: duration.value,
                                    text : duration.label
                                }));
                            });

                            select.find('option[value=' + value + ']').prop('selected', true);
                        });
                        
                        $('[x-name=duration]').fadeTo('fast', 1);

                    }
                });
			});
			
            $('[x-name=allegro_account]').on('change', '[name=allegro_account]', function() {
                var select = $(this);
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getAllegroAccount',
                        ajax : true,
                        dataType: 'json',
                        allegro_account : select.val()
                    },
                    beforeSend: function()
                    {
                        select.parent().parent().fadeTo('fast', 0.2);
                    },
                    success: function(json)
                    {
                        var fieldset = select.parent().parent();

                        fieldset.find('legend').nextAll().remove();

                        $('[x-name=duration]').each(function() {
                            var select = $(this);
                            var value = select.val();

                            $(this).find('option').remove();

                            $.each(json.durations, function (index, duration) {
                                select.append($('<option>', { 
                                    value: duration.value,
                                    text : duration.label
                                }));
                            });

                            select.find('option[value=' + value + ']').prop('selected', true);
                        });

                        fieldset.append(json.fieldset).fadeTo('fast', 1);
                    }
                });
            });
            
            $('input[name=start]').on('change', function() {
                if ($('input[name=start]:checked').val() == 1) {
                    $('input[name=start_time]').prop('disabled', false);
                } else {
                    $('input[name=start_time]').prop('disabled', true);
                }
            });
            
            
            /*
            $(document).ready(function(){
				$("td[x-thumbnail]").tooltip({
					title : 'It works in absence of title attribute.'
				});
			});
            
			$('td[x-thumbnail]').tooltip({
				
			});
            
            
            $('td[x-thumbnail]').tooltip({
				'placement' : 'top',
				'title' : 'test',
				'original-title' : 'test',
				'originalTitle' : 'test',
				'html' : true
			});
			
			*/
            
            $(function() {
				$(document).tooltip({
					items: "",
					content: function() {
						return 'dupa'
					}
				});
			});
        
//        _allegroAccountChange = function(e) {
//            var select = $(e.target);
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: {
//                    action : 'getAllegroAccount',
//                    ajax : true,
//                    dataType: 'json',
//                    allegro_account : select.val()
//                },
//                beforeSend: function()
//                {
//                    select.parent().parent().fadeTo('fast', 0.2);
//                    
//                    $('[x-name=product]').each(function() {
//                        if ($(this).is(':visible')) {
//                            $(this).fadeTo('fast', 0.2);
//                        }
//                    });
//                },
//                success: function(json)
//                {
//                    var fieldset = select.parent().parent();
//                    
//                    fieldset.find('legend').nextAll().remove();
//                    
//                    $('[x-name=quantity_type]').each(function() {
//                        var select = $(this);
//                        var value = select.val();
//                        
//                        $(this).find('option').remove();
//                        
//                        $.each(json.durations, function (index, duration) {
//                            select.append($('<option>', { 
//                                value: duration.value,
//                                text : duration.label
//                            }));
//                        });
//                        
//                        select.find('option[value=' + value + ']').prop('selected', true);
//                    });
//                    
//                    fieldset.append(json.fieldset).fadeTo('fast', 1);
//                    
//                    $('[x-name=product]').each(function() {
//                        if ($(this).is(':visible')) {
//                            $(this).fadeTo('fast', 1);
//                        }
//                    });
//                }
//            });
//        }
            
            
            $('input[x-name=quantity]').on('focusout xchange', function() {
                if ($(this).attr('x-max') !== '' && parseInt($(this).prop('value')) > parseInt($(this).attr('x-max'))) {
                    $(this).prop('value', $(this).attr('x-max'));
                }
                
                if (parseInt($(this).prop('value')) < 1) {
                    $(this).prop('value', 1);
                }
            });
            
//            Shipments switcher
            _shipmentsSwitch($('[x-name=shipments] [x-name=switch]'));
            
//            Product switcher
            $('[x-name=product_switch]').on('change', function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().next().show();
                } else {
                    $(this).parent().parent().next().hide();
                }
            });
            
            
            $('.xallegro-test').parent().on('click', function() {
                var data = $('#configuration_form').serializeArray();

                data[data.length] = {name: 'ajax', value: true};
                data[data.length] = {name: 'action', value: 'newAuctionTest'};
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: data,
                    beforeSend: function()
                    {
						var cover = $('<div id="cover" style="z-index:2000000;position:fixed; left:0; top:0; width:100%; height:100%; background: rgba(0,0,0,0.6) url(../img/admin/ajax-loader-big.gif) center center no-repeat;"></div>');
						cover.appendTo('body').hide().fadeIn(400);
						
					},
                    success: function(data)
                    {
						console.log(data);
						$('#cover').remove();
                        fancyMsgBox(data.content, 'Symulacja wystawienie aukcji');
                    }
                });
            
                return false;
            });
            
            $('.xallegro-perform').parent().on('click', function(e){
				e.preventDefault();
				$('#configuration_form').submit();
			});
            
//            Change category
            $('[x-name=categories]').on('change', 'select', function() {
                var select = $(this);
                
                if (select.prop('value') == 0) {
                    return;
                }

                var selects = $('[x-name=categories]').find('select');
                var index = selects.index($(this));

                for (i=index+1;i<selects.length;i++) {
                    selects.eq(i).remove();
                }
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCategories',
                        ajax : true,
                        id_allegro_category : select.prop('value')
                    },
                    beforeSend: function()
                    {
                        $('[x-name=categories]').parent().fadeTo('fast', 0.2);
                        $('[x-name=category_fields]').parent().hide().find('.panel-heading').nextAll().remove();
                        $('div[x-name=product_category_fields]').html('');
                    },
                    success: function(data)
                    {
                        if (data['categories'].length) {
                            var element = select.clone().insertAfter(select).hide();

                            $("option[value!='0']", element).remove();

                            $.each(data['categories'], function(index, category){
                                element.append(new Option(category['name'], category['id']));
                            });

                            element.show();
                        }

                        if (data['fields']) {
                            $('[x-name=category_fields]').append(data.fields).parent().show();
                            
                            $('div[x-name=product_category_fields]').each(function() {
                                $(this).html(data.fields.replace(/category_fields/g, 'item[' + $(this).attr('x-index') + '][category_fields]'));
                            })
                        }
                    },
                    complete: function() {
                        $('[x-name=categories]').parent().fadeTo('fast', 1);
                    }
                });
            });
            
//            Change pas profile
            $('select[name=pas]').on('change', function() {
                if ($(this).prop('value') != 0) {
                    $.ajax({
                        type: 'POST',
                        url: admin_xallegro_ajax_url,
                        dataType: 'json',
                        data: {
                            ajax : true,
                            action : 'getPas',
                            id: $(this).prop('value')
                        },
                        beforeSend: function() {
                            $('[x-name=pas]').parent().fadeTo('fast', 0.2);
                        },
                        success: function(data)
                        {
                            $.each(data, function(index, value) {
                                var input = $('[x-name=pas]').find('[name=' + index + ']');

                                if (input.is(':text') || input.is('textarea')) {
                                    input.prop('value', value);
                                } else if (input.is(':checkbox')) {

                                } else if (input.is('select')) {
                                    input.find('option').prop('selected', false);
                                    input.find('option[value=' + value + ']').prop('selected', true);
                                } else if (input.is(':radio')) {
                                    input.filter("[value='" + value + "']").prop('checked', true);
                                }
                            });

                            $('[x-name=shipments]').find('tr[x-id]').each(function() {
                                var id = $(this).attr('x-id');
                                
                                if (parseInt(id) in data.shipments) {
                                    $(this).find('input:checkbox').prop('checked', true).trigger('change');

                                    $(this).find('input:text').each(function(index) {
                                        $(this).prop('value', data.shipments[id][index]).trigger('focusout');
                                    });
                                } else {
                                    $(this).find('input:checkbox').prop('checked', false).trigger('change');
                                    $(this).find('input:text').prop('value', 0).trigger('focusout').eq(2).prop('value', 1).trigger('focusout');
                                }
                            });
                        },
                        complete: function() {
                            $('[x-name=pas]').parent().fadeTo('fast', 1);
                        }
                    });
                }
            });
            
//            Auction preview button
            $('[x-name=preview]').on('click', function() {
                var data = $('#configuration_form').serializeArray();
                data[data.length] = {name: 'index', value: $(this).attr('x-id')};
                data[data.length] = {name: 'ajax', value: true};
                data[data.length] = {name: 'action', value: 'preview'};
                
                var preview = window.open("", "popupWindow", "width=1200, height=600, scrollbars=yes");
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: data,
                    beforeSend: function(data)
                    {
						var cover = $('<div id="cover" style="z-index:2000000;position:fixed; left:0; top:0; width:100%; height:100%; background: rgba(0,0,0,0.6) url(../img/admin/ajax-loader-big.gif) center center no-repeat;"></div>');
						$(preview.document.body).append(cover);
					},
                    success: function(data)
                    {
                        $(preview.document.body).html(data['preview']);
                    }
                });
            
                return false;
            });
        
//            Toogle product category fields
            $('a[x-name=product_category_fields]').click(function() {
                var button = $(this);
                var container = button.parent().parent().parent();
                var fieldset = container.find('div[x-name=product_category_fields]');
                var semaphore = container.find('[x-name=product_category_fields_enabled]');

                if (fieldset.is(':visible')) {
                    fieldset.attr('x-enabled', 0).hide();
                    semaphore.prop('value', 0);
                } else {
                    fieldset.attr('x-enabled', 1).show();
                    semaphore.prop('value', 1);
                }
                
                return false;
            });
            
//            Description edit button
            $('a[x-name=description_edit]').click(function() {
                var button = $(this);
                var description = button.parent().parent().find('input[x-name=description]').prop('value');

                var html = '';
                html += '<textarea class="rte" rows="10">' + description + '</textarea>';
                html += '<br /><input class="button" type="button" value="Zapisz" x-name="description_save" />';

                $.fancybox(html, {
                    autoResize: false,
                    minWidth: 705,
                    minHeight: 300,
                    beforeShow: function () {
                        tinySetup({
                            height: 350
                        });
                        $('[x-name=description_save]').on('click', function(){
                            button.parent().parent().find('input[x-name=description]').prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));

                            $(this).off('click');
                            $.fancybox.close();
                        });
                    }
                });
                
                return false;
            });
            
//            Description cudstom edit button
            $('a[x-name=description_custom_edit]').click(function() {
                var button = $(this);
                var description = button.parent().parent().find('input[x-name=description_custom]').prop('value');

                var html = '';
                html += '<textarea class="rte" rows="10">' + description + '</textarea>';
                html += '<br /><input class="button" type="button" value="Zapisz" x-name="save" />';

                $.fancybox(html, {
                    autoResize: false,
                    minWidth: 705,
                    minHeight: 300,
                    beforeShow: function () {
                        tinySetup({
                            height: 350
                        });
                        $('[x-name=save]').on('click', function(){
                            button.parent().parent().find('input[x-name=description_custom]').prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));

                            $(this).off('click');

                            $.ajax({
                                type: 'POST',
                                url: admin_xallegro_ajax_url,
                                data: {
                                    action : 'saveDesc',
                                    ajax : true,
                                    id_product : button.parent().parent().find('[x-name=id_product]').prop('value'),
                                    id_product_attribute: button.parent().parent().find('[x-name=id_product_attribute]').prop('value'),
                                    description: tinyMCE.activeEditor.getContent({format : 'raw'})
                                },
                                complete: function()
                                {
                                    $.fancybox.close();
                                }
                            });
                        });
                    }
                });

                return false;
            });
            
            $('[x-name=title]').on('input xchange', function() {
                var strlen = function(string) {
                    var customChars = {
                        '<': 4,
                        '>': 4,
                        '&': 5,
                        '"': 6
                    }

                    var size = string.length;
                    var count = 0;

                    for(var key in customChars) {
                        count = string.split(key).length - 1;

                        size += (count*(customChars[key]-1));
                    }

                    return size;
                }
                
                var size = strlen($(this).prop('value'));
                
                while (size > 50) {
                    $(this).prop('value', $(this).prop('value').slice(0,-1));

                    size = strlen($(this).prop('value'));
                }

                $(this).parent().find('[x-name=counter]').html(size);
            });
            
            
//            Masowe
            $('[x-name=additional] input').on('change', function() {
                var input = $(this);
                $('[x-name=product_switch]').each(function() {
                    if (!$(this).is(':checked')) {
                        return;
                    }
                    
                    $(this).parent().parent().next().find('[x-name=' + input.attr('x-name') + ']').prop('checked', input.is(':checked'));
                });
            });
            
            $('div[x-name=images] img').on('click', function() {
                $(this).parent().parent().find('div').removeClass('main_image');
                $(this).parent().addClass('main_image');
                $(this).parent().parent().find('[x-name=image_main]').prop('value', $(this).attr('x-value'));
            });
            
            $('input[x-name=images]').on('change', function() {
                if ($(this).is(':checked')) {
                    if (!$(this).parent().parent().find('[x-name=image_main]').val()) {
                        $(this).next().trigger('click');
                    }
                }
            });
            
            
            $('a[x-name=bulk_manufacturer]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    var manufacturer = $(this).parent().parent().next().find('[x-name=manufacturer]').prop('value');
                    var input = $(this).parent().parent().next().find('[x-name=title]');
                    
                    if (manufacturer) {
                        input.prop('value', input.prop('value') + ' ' + manufacturer).trigger('xchange');
                    }
                });
                
                return false;
            });
            
            $('a[x-name=bulk_attribute]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    var atrributeName = $(this).parent().parent().next().find('[x-name=attribute_name]').prop('value');
                    var input = $(this).parent().parent().next().find('[x-name=title]');
                    
                    if (atrributeName) {
                        input.prop('value', input.prop('value') + ' ' + atrributeName).trigger('xchange');
                    }
                });
                
                return false;
            });
            
            $('a[x-name=bulk_price_buy_now]').on('click', function() {
                var markup = parseInt($('input[x-name=bulk_price_buy_now]').prop('value'))/100;
                var action = $(this).attr('x-action');
                
                if (action === 'down') {
                    markup *= -1;
                }
                
                $('[x-name=product_switch]:checked').each(function() {
                    var input = $(this).parent().parent().next().find('[x-name=price_buy_now]');
                    var value = parseFloat(input.attr('x-value'));
                    value += (value*markup);
                    
                    input.prop('value', value).trigger('xchange');
                });
                
                return false;
            });
            
            $('input[x-name=bulk_price_asking],input[x-name=bulk_price_minimal],input[x-name=bulk_quantity]').on('focusout', function() {
                var value = $(this).prop('value');
                var name = $(this).attr('x-name').substr(5) ;
                
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('[x-name=' + name + ']').prop('value', value).trigger('xchange');
                });
            });
            
            $('select[x-name=bulk_quantity_type],select[x-name=bulk_duration],select[x-name=bulk_template],select[x-name=bulk_overlay]').on('change', function() {
                var value = $(this).prop('value');
                var name = $(this).attr('x-name').substr(5) ;
                
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('[x-name=' + name + ']').prop('value', value);
                });
            });
            
//            Select all images
            $('[x-name=bulk_images_all]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').prop('checked', true).trigger('change');
                });
                
                return false;
            });
            
//            Select images invert
            $('[x-name=bulk_images_invert]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').each(function() {
                        $(this).prop('checked', !$(this).prop('checked'));
                    });
                });
                
                return false;
            });
            
//            Select images first
            $('[x-name=bulk_images_first]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').eq(0).prop('checked', true).trigger('change');
                });
                
                return false;
            });
        }
        
        auctionEditForm = function() {
			
            $('input#name').autocomplete('../modules/x13allegro/ajax_products_list.php?excludeIds=999999999', {}).result(function(event, data, formatted) {
                $('input#id_product').prop('value', data[1]);
                _getProductAttributes(data[1]);
            });
            
            $('input#name').on('input', function() {
                if ($(this).prop('value') === '') {
                    $('input#id_product').prop('value', '0');
                }
            });
            
            $('input#id_product').on('input', function(){
				var val = parseInt($(this).val());
				if(!isNaN(val) && val > 0) {
					_getProductAttributes(val);
				}
			});
            
        }
        
        _getProductAttributes = function(id_product) {
			$.ajax({
				type: 'POST',
				url: admin_xallegro_ajax_url,
				dataType: 'json',
				data: {
					action : 'getAttributes',
					ajax : true,
					id_product : id_product,//data[1],
				},
				beforeSend: function()
				{
					$('fieldset#fieldset_0').parent().fadeTo('fast', 0.2);
				},
				success: function(data)
				{
					$('select#id_product_attribute option').remove();
					
					$.each(data, function(index, value) {
						$('select#id_product_attribute').append(new Option(value.attribute_designation, value.id_product_attribute));
					});
					
					if ($('select#id_product_attribute option').size() === 0) {
						$('select#id_product_attribute').append(new Option('Brak', '0'));
					}
				},
				complete: function()
				{
					$('fieldset#fieldset_0').parent().fadeTo('fast', 1);
				}
			});
		}
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        _hooks = function() {
            $(settings.cast).on('focusout', _cast);
            $(settings.allegro_account).parent().parent().on('change', settings.allegro_account, _allegroAccountChange);
            $(settings.product.quantity.input).on('focusout', _quantityCheck);
            
            $(settings.product.images.images).find('img').on('click', _setImageMain);
            $(settings.product.images.images).find('input:checkbox').on('change', _imagesChange);
            
            $(settings.product.description_custom.buttonEdit).on('click', _editorDescriptionCustom);
            
            $(settings.product.switch).on('change', _productSwitch)
            $(settings.title.input).on('input', _inputTitle);
            $(settings.description.buttonEdit).on('click', _editorDescription);
            $(settings.preview.button).on('click', _preview);
            $(settings.category.fieldset).on('change', 'select', _categoryChange);
            $(settings.product_category_fields.button).on('click', _productCategoryField);
            
            
            $(settings.pas).find('select[name=pas]').on('change', _pasChange);
            $(settings.shipments).find('input:checkbox').on('change', _shipmentsSwitch);
        }
        
        _shipmentsSetSwitchs = function() {
            $(settings.shipments).find('input:checkbox').each(function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().find('input:text').removeAttr('disabled');
                } else {
                    $(this).parent().parent().find('input:text').attr('disabled', 'disabled');
                }
            });
        }
        
//        _cast = function(e) {
//            var input = $(e.target);
//            var value = input.prop('value');
//            var type = input.attr('x13-type');
//            
//            if (type == 'float') {
//                input.prop('value', formatNumber(formatedNumberToFloat(value.replace(/[^0-9,\.]/g, ''), 2, null), 2, '', '.'))
//            } else if (type == 'integer') {
//                input.prop('value', parseInt(value));
//            }
//        }
        
        _quantityCheck = function(e) {
            var input = $(e.target);
            
            if (input.attr('x13-max')) {
                if (parseInt(input.prop('value')) > parseInt(input.attr('x13-max'))) {
                    input.prop('value', input.attr('x13-max'));

//                        Blur fix
                    setTimeout(function(){
                        alert('Ilość przedmiotów nie może być większa niż stan magazynowy.');
                    }, 50);
                }
            } else if (input.prop('value') > 1000) {
                input.prop('value', 1000);
                alert('Ilość przedmiotów nie może być większa niż 1000.');
            }
        }
        
        _allegroAccountChange = function(e) {
            var select = $(e.target);
            
            $.ajax({
                type: 'POST',
                url: admin_xallegro_ajax_url,
                dataType: 'json',
                data: {
                    action : 'getAllegroAccount',
                    ajax : true,
                    dataType: 'json',
                    allegro_account : select.val()
                },
                beforeSend: function()
                {
                    select.parent().parent().fadeTo('fast', 0.2);
                    
                    $(settings.product.item).each(function() {
                        if ($(this).is(':visible')) {
                            $(this).fadeTo('fast', 0.2);
                        }
                    });
                },
                success: function(json)
                {
                    var fieldset = select.parent().parent();
                    
                    fieldset.find('legend').nextAll().remove();
                    
                    $(settings.product.duration.input).each(function() {
                        var select = $(this);
                        var value = select.val();
                        
                        $(this).find('option').remove();
                        
                        $.each(json.durations, function (index, duration) {
                            select.append($('<option>', { 
                                value: duration.value,
                                text : duration.label
                            }));
                        });
                        
                        select.find('option[value=' + value + ']').prop('selected', true);
                    });
                    
                    fieldset.append(json.fieldset).fadeTo('fast', 1);
                    
                    $(settings.product.item).each(function() {
                        if ($(this).is(':visible')) {
                            $(this).fadeTo('fast', 1);
                        }
                    });
                }
            });
        }
        
//        _productSwitch = function(e) {
//            var input = $(e.target);
//            var container = input.parent().parent().next();
//            
//            if (input.is(':checked')) {
//                container.show();
//            } else {
//                container.hide();
//            }
//        }
        
//        _inputTitle = function(e) {
//            var input = $(e.target);
//            var size = _countTitleSize(input.prop('value'));
//            
//            while (size > 50) {
//                input.prop('value', input.prop('value').slice(0,-1));
//                
//                size = _countTitleSize(input.prop('value'));
//            }
//            
//            input.parent().find(settings.title.counter).html(size);
//        }
//        
//        _countTitleSize = function(string) {
//            var customChars = {
//                '<': 4,
//                '>': 4,
//                '&': 5,
//                '"': 6
//            }
//            
//            var size = string.length;
//            var count = 0;
//            
//            for(var key in customChars) {
//                count = string.split(key).length - 1;
//                
//                size += (count*(customChars[key]-1));
//            }
//            
//            return size;
//        }
        
//        _editorDescription = function(e) {
//            var button = $(e.currentTarget);
//            var description = button.parent().parent().find(settings.description.input).prop('value');
//            
//            var html = '';
//            html += '<textarea class="rte" rows="10">' + description + '</textarea>';
//            html += '<br /><input class="button" type="button" value="Zapisz" x13-name="description_save" />';
//            
//            $.fancybox(html, {
//                autoResize: false,
//                minWidth: 705,
//                minHeight: 300,
//                beforeShow: function () {
//                    tinySetup({
//                        height: 350
//                    });
//                    $(settings.description.buttonSave).on('click', function(){
//                        button.parent().parent().find(settings.description.input).prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));
//                        
//                        $(settings.description.buttonSave).off('click');
//                        $.fancybox.close();
//                    });
//                }
//            });
//            
//            return false;
//        }
//        
//        _editorDescriptionCustom = function(e) {
//            var button = $(e.currentTarget);
//            var description = button.parent().parent().find(settings.product.description_custom.input).prop('value');
//            
//            var html = '';
//            html += '<textarea class="rte" rows="10">' + description + '</textarea>';
//            html += '<br /><input class="button" type="button" value="Zapisz" x13-name="save" />';
//            
//            $.fancybox(html, {
//                autoResize: false,
//                minWidth: 705,
//                minHeight: 300,
//                beforeShow: function () {
//                    tinySetup({
//                        height: 350
//                    });
//                    $('[x13-name=save]').on('click', function(){
//                        button.parent().parent().find(settings.product.description_custom.input).prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));
//                        
//                        $('[x13-name=save]').off('click');
//                        
//                        $.ajax({
//                            type: 'POST',
//                            url: admin_xallegro_ajax_url,
//                            data: {
//                                action : 'saveDesc',
//                                ajax : true,
//                                id_product : button.parent().parent().find('[x13-name=id_product]').prop('value'),
//                                id_product_attribute: button.parent().parent().find('[x13-name=id_product_attribute]').prop('value'),
//                                description: tinyMCE.activeEditor.getContent({format : 'raw'})
//                            },
//                            success: function(data)
//                            {
//                            }
//                        });
//                        
//                        $.fancybox.close();
//                    });
//                }
//            });
//            
//            return false;
//        }
        
//        _setImageMain = function(e) {
//            var image = $(e.currentTarget);
//            
//            image.parent().parent().find('div').removeClass('main_image');
//            
//            image.parent().addClass('main_image');
//            
//            image.parent().parent().find(settings.product.images.inputImageMain).prop('value', image.attr('x13-value'));
//        }
        
//        _imagesChange = function(e) {
//            var input = $(e.currentTarget);
//            
//            if (input.is(':checked')) {
//                if (!input.parent().parent().find(settings.product.images.inputImageMain).val()) {
//                    $(this).next().trigger('click');
//                }
//            }
//        }
        
//        _categoryChange = function(e) {
//            var select = $(e.target);
//            
//            if (select.prop('value') == 0) {
//                return;
//            }
//            
//            var selects = $(settings.category.fieldset).find('select');
//            var index = selects.index($(this));
//
//            for (i=index+1;i<selects.length;i++) {
//                selects.eq(i).remove();
//            }
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: {
//                    action : 'getCategories',
//                    ajax : true,
//                    dataType: 'json',
//                    id_allegro_category : select.prop('value')
//                },
//                beforeSend: function()
//                {
//                    $(settings.category_fields.fieldset).hide().find('legend').nextAll().remove();
//                    $(settings.product_category_fields.fieldset).hide().html('');
//                },
//                success: function(data)
//                {
//                    if (data['categories'].length) {
//                        var element = select.clone().insertAfter(select).hide();
//
//                        $("option[value!='0']", element).remove();
//
//                        $.each(data['categories'], function(index, category){
//                            element.append(new Option(category['name'], category['id']));
//                        });
//
//                        element.show();
//                    }
//
//                    if (data['fields']) {
//                        $(settings.category_fields.fieldset).append(data.fields);
//                        $(settings.category_fields.fieldset).show();
//                        
//                        $(settings.product_category_fields.fieldset).each(function() {
//                            $(this).html(data.fields.replace(/category_fields/g, 'item[' + $(this).attr('x13-index') + '][category_fields]'));
//                            if ($(this).attr('x13-enabled') == '1') {
//                                $(this).show();
//                            }
//                        });
//                    }
//                }
//            });
//        }
//        
//        X_preview = function(e) {
//            var button = $(e.currentTarget);
//            var container = button.parent().parent().parent();
//            var data = {
//                action : 'preview',
//                ajax : true
//            }
//            
//            container.find('[x13-name]').each(function() {
//                if ($(this).is(':checkbox') && !$(this).is(':checked')) {
//                    return;
//                }
//                
//                if (container.find('[x13-name=' + $(this).attr('x13-name') + ']').size() == 1) {
//                    data[$(this).attr('x13-name')] = $(this).prop('value');
//                } else {
//                    if ($(this).attr('x13-name') in data) {
//                        data[$(this).attr('x13-name')].push($(this).prop('value'));
//                    } else {
//                        data[$(this).attr('x13-name')] = [$(this).prop('value')];
//                    }
//                }
//            });
//            
//            var preview = window.open("", "popupWindow", "width=1200, height=600, scrollbars=yes");
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: data,
//                success: function(data)
//                {
//                    $(preview.document.body).html(data['preview']);
//                }
//            });
//            
//            return false;
//        }
        
//        _productCategoryField = function(e) {
//            var button = $(e.currentTarget);
//            var container = button.parent().parent().parent();
//            var fieldset = container.find(settings.product_category_fields.fieldset);
//            var semaphore = container.find(settings.product_category_fields.semaphore);
//            
//            if (fieldset.is(':visible')) {
//                fieldset.attr('x13-enabled', 0).hide();
//                semaphore.prop('value', 0);
//            } else {
//                fieldset.attr('x13-enabled', 1).show();
//                semaphore.prop('value', 1);
//            }
//            
//            return false;
//        }
        
//        _shipmentsSwitch = function(e) {
//            var input = $(e.target);
//            
//            if (input.is(':checked')) {
//                input.parent().parent().find('input:text').removeAttr('disabled');
//            } else {
//                input.parent().parent().find('input:text').attr('disabled', 'disabled');
//            }
//        }
        
//        _pasChange = function(e) {
//            var select = $(e.target);
//            
//            if (select.prop('value') != 0) {
//                $.ajax({
//                    type: 'POST',
//                    url: admin_xallegro_ajax_url,
//                    dataType: 'json',
//                    data: {
//                        ajax : true,
//                        action : 'getPas',
//                        id: select.prop('value')
//                    },
//                    success: function(data)
//                    {
//                        $.each(data, function(index, value) {
//                            var input = $(settings.pas).find('[name=' + index + ']');
//                            
//                            if (input.is(':text') || input.is('textarea')) {
//                                input.prop('value', value);
//                            } else if (input.is(':checkbox')) {
//                                
//                            } else if (input.is('select')) {
//                                input.find('option').prop('selected', false);
//                                input.find('option[value=' + value + ']').prop('selected', true);
//                            } else if (input.is(':radio')) {
//                                input.filter("[value='" + value + "']").prop('checked', true);
//                            }
//                        });
//                        
//                        $(settings.shipments).find('tr[x13-shipment-id]').each(function() {
//                            var id = $(this).attr('x13-shipment-id');
//                            console.log(data.shipments);
//                            if (parseInt(id) in data.shipments) {
//                                $(this).find('input:checkbox').prop('checked', true).trigger('change');
//                                
//                                $(this).find('input:text').each(function(index) {
//                                    $(this).prop('value', data.shipments[id][index]).trigger('focusout');
//                                });
//                            } else {
//                                $(this).find('input:checkbox').prop('checked', false).trigger('change');
//                                $(this).find('input:text').prop('value', 0).trigger('focusout').eq(2).prop('value', 1).trigger('focusout');
//                            }
//                        });
//                    }
//                });
//            }
//        }
        
        init();
        
        return self;

    }
})(jQuery);

