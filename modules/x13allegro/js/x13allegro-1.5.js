(function($) {
    $.X13Allegro = function(options) {
        init = function() {
            _cast();
        }
        
        _cast = function() {
            var cast = function() {
                var type = $(this).attr('x-cast');
                
                if ($(this).prop('value') === '') {
                    $(this).prop('value', 0);
                }
                
                if (type == 'float') {
                    $(this).prop('value', formatNumber(formatedNumberToFloat($(this).prop('value').replace(/[^0-9,\.]/g, '0'), 2, null), 2, '', '.'))
                } else if (type == 'integer') {
                    $(this).prop('value', parseInt($(this).prop('value')));
                }
            }
            
            $('[x-cast]').each(cast).on('focusout change xchange', cast);
        }
        
        showMessage = function(message) {
            $('#content').prepend('<div class="hint clear" style="display:block;"><div id="infos_block">' + message + '</div></div><br />');
        }
        
        _shipmentsSwitch = function(element) {
            sw = function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().find('input:text').removeAttr('disabled');
                } else {
                    $(this).parent().parent().find('input:text').attr('disabled', 'disabled');
                }
            }
            
            $(element).on('change', sw).each(sw);
        }
        
        accountForm = function() {
            change = function() {
                if (!$('#apikey').prop('value')) {
                    return;
                }
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCountries',
                        ajax: true,
                        apikey: $('#apikey').prop('value'),
                        country_code: $('#country_code').prop('value'),
                        sandbox: $('[name=sandbox]:checked').prop('value')
                    },
                    beforeSend: function() {
                        $('#fieldset_0').fadeTo('fast', 0.2);
                        $('#country option[value!=0]').remove();
                        $('#country option[value=0]').show();
                    },
                    success: function(data) {
                        if (data.status == 'ok') {
                            $('#country option[value=0]').remove();
                            
                            $.each(data.countries, function(index, country) {
                                $('#country').append($('<option>', { 
                                    value: country['country-id'],
                                    text : country['country-name']
                                }));
                            });
                        } else if (data.status == 'error') {
                            $('#country').append(new Option('Wprowadź poprawny klucz API', '0'));
                            showErrorMessage(data.error);
                        }
                    },
                    complete: function() {
                        $('#fieldset_0').fadeTo('fast', 1);
                    }
                });
            }
            
            $('input#apikey, select#country_code').on('change', change);
        }
        
        pasForm = function() {
			$('[name="account_number"], [name="account_number_second"]').on('input', function(){
				$(this).val($(this).val().replace(/[^0-9]/gi, ''));
			});
            _shipmentsSwitch($('[x-name=switch]'));
        }
        
		configurationForm = function() {
			//aktualizacja wystawionych
			$('[href="#syncClosedStatus"]').on('click', function(e){
				e.preventDefault();
				showSuccessMessage('Aktualizacja wystawień...');
				syncClosedStatus(0, -1);
			});
		}
        
        categoryForm = function() {
            if (!$('#fieldset_1 > legend').nextAll().size()) {
                $('#fieldset_1').hide();
            }
            
            $('#fieldset_0').on('change', 'select', function() {
                var select = $(this);
                
                if (select.prop('value') == 0) {
                    return;
                }

                var selects = $('#fieldset_0').find('select');
                var index = selects.index($(this));

                for (i=index+1;i<selects.length;i++) {
                    selects.eq(i).remove();
                }
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCategories',
                        ajax : true,
                        id_allegro_category : select.prop('value')
                    },
                    beforeSend: function()
                    {
                        $('#fieldset_1').hide().find('legend').nextAll().remove();
                    },
                    success: function(data)
                    {
                        if (data['categories'].length) {
                            var element = select.clone().insertAfter(select).hide();

                            $("option[value!='0']", element).remove();

                            $.each(data['categories'], function(index, category){
                                element.append(new Option(category['name'], category['id']));
                            });

                            element.show();
                        }

                        if (typeof data['fields'] != 'object') {
                            $('#fieldset_1').append(data.fields);
                            $('#fieldset_1').show();
                        }
                    }
                });
            });
        };
        
        auctionForm = function() {
        	
			//sprawdzanie, czy aktualne są pliki kategorii i formularzy oraz aktualizacja
			if(newest_VERSION_CATEGORY_LIST != '' && newest_VERSION_CATEGORY_LIST != current_VERSION_CATEGORY_LIST) {
				updateCategoryTree();
			}
			/*
			if(newest_VERSION_SELL_FORM_FIELDS != '' && newest_VERSION_SELL_FORM_FIELDS != current_VERSION_SELL_FORM_FIELDS) {
				updateFormsFields();
			}
			*/
			
			//Kopiowanie klikniętych pól do nieaktywnych cech
			$('[x-name="category_fields"]').on('change', 'select', function(){
				var _el = $(this);
				var i, id;
				$('[x-name="product"]').each(function(){
					id = $(this).attr('x-id');
					if($(this).find('[name="item\['+id+'\]\[category_fields_enabled\]"]').val() == 0) {
						$(this).find('[name="item\['+id+'\]'+_el.attr('name').replace('category_fields', '[category_fields]')+'"]').val(_el.val());
					}
				});
			});
			$('[x-name="category_fields"]').on('input', 'input[type="text"]', function(){
				var _el = $(this);
				var i, id;
				$('[x-name="product"]').each(function(){
					id = $(this).attr('x-id');
					if($(this).find('[name="item\['+id+'\]\[category_fields_enabled\]"]').val() == 0) {
						$(this).find('[name="item\['+id+'\]'+_el.attr('name').replace('category_fields', '[category_fields]')+'"]').val(_el.val());
					}
				});
			});
			$('[x-name="category_fields"]').on('click', 'input[type="checkbox"]', function(){
				var _el = $(this);
				var i, id;
				$('[x-name="product"]').each(function(){
					id = $(this).attr('x-id');
					if($(this).find('[name="item\['+id+'\]\[category_fields_enabled\]"]').val() == 0) {
						$(this).find('[name="item\['+id+'\]'+_el.attr('name').replace('category_fields', '[category_fields]')+'"][value="'+_el.val()+'"]').trigger('click');
					}
				});
			});
			
			$('[name="account_number"], [name="account_number_second"]').on('input', function(){
				$(this).val($(this).val().replace(/[^0-9]/gi, ''));
			});
			
			$('[x-name=allegro_account]').on('change', '[name="allegro_shop"]', function(){
				$.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getAllegroDuration',
                        ajax : true,
                        dataType: 'json',
                        allegro_shop : $('[name="allegro_shop"]:checked').val()
                    },
                    beforeSend: function()
                    {
                        $('[x-name=duration]').fadeTo('fast', 0.2);
                    },
                    success: function(json)
                    {

                        $('[x-name=duration]').each(function() {
                            var select = $(this);
                            var value = select.val();

                            $(this).empty();

                            $.each(json.durations, function (index, duration) {
                                select.append($('<option>', { 
                                    value: duration.value,
                                    text : duration.label
                                }));
                            });

                            select.find('option[value=' + value + ']').prop('selected', true);
                        });
                        
                        $('[x-name=duration]').fadeTo('fast', 1);

                    }
                });
			});
			
            $('[x-name=allegro_account]').on('change', '[name=allegro_account]', function() {
                var select = $(this);
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getAllegroAccount',
                        ajax : true,
                        dataType: 'json',
                        allegro_account : select.val()
                    },
                    beforeSend: function()
                    {
                        select.parent().parent().fadeTo('fast', 0.2);
                    },
                    success: function(json)
                    {
                        var fieldset = select.parent().parent();

                        fieldset.find('legend').nextAll().remove();

                        $('[x-name=duration]').each(function() {
                            var select = $(this);
                            var value = select.val();

                            $(this).find('option').remove();

                            $.each(json.durations, function (index, duration) {
                                select.append($('<option>', { 
                                    value: duration.value,
                                    text : duration.label
                                }));
                            });

                            select.find('option[value=' + value + ']').prop('selected', true);
                        });

                        fieldset.append(json.fieldset).fadeTo('fast', 1);
                    }
                });
            });
            
            $('input[name=start]').on('change', function() {
                if ($('input[name=start]:checked').val() == 1) {
                    $('input[name=start_time]').prop('disabled', false);
                } else {
                    $('input[name=start_time]').prop('disabled', true);
                }
            });
        
//        _allegroAccountChange = function(e) {
//            var select = $(e.target);
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: {
//                    action : 'getAllegroAccount',
//                    ajax : true,
//                    dataType: 'json',
//                    allegro_account : select.val()
//                },
//                beforeSend: function()
//                {
//                    select.parent().parent().fadeTo('fast', 0.2);
//                    
//                    $('[x-name=product]').each(function() {
//                        if ($(this).is(':visible')) {
//                            $(this).fadeTo('fast', 0.2);
//                        }
//                    });
//                },
//                success: function(json)
//                {
//                    var fieldset = select.parent().parent();
//                    
//                    fieldset.find('legend').nextAll().remove();
//                    
//                    $('[x-name=quantity_type]').each(function() {
//                        var select = $(this);
//                        var value = select.val();
//                        
//                        $(this).find('option').remove();
//                        
//                        $.each(json.durations, function (index, duration) {
//                            select.append($('<option>', { 
//                                value: duration.value,
//                                text : duration.label
//                            }));
//                        });
//                        
//                        select.find('option[value=' + value + ']').prop('selected', true);
//                    });
//                    
//                    fieldset.append(json.fieldset).fadeTo('fast', 1);
//                    
//                    $('[x-name=product]').each(function() {
//                        if ($(this).is(':visible')) {
//                            $(this).fadeTo('fast', 1);
//                        }
//                    });
//                }
//            });
//        }
            
            
            $('input[x-name=quantity]').on('focusout xchange', function() {
                if ($(this).attr('x-max') !== '' && parseInt($(this).prop('value')) > parseInt($(this).attr('x-max'))) {
                    $(this).prop('value', $(this).attr('x-max'));
                }
                
                if (parseInt($(this).prop('value')) < 1) {
                    $(this).prop('value', 1);
                }
            });
            
//            Shipments switcher
            _shipmentsSwitch($('[x-name=shipments] [x-name=switch]'));
            
//            Product switcher
            $('[x-name=product_switch]').on('change', function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().next().show();
                } else {
                    $(this).parent().parent().next().hide();
                }
            });
            
            $('.process-icon-test.xallegro').parent().on('click', function(e) {
                e.preventDefault();
                testAuctions(0);
            });
            
            
            $('.process-icon-perform.xallegro').parent().on('click', function(e) {
				e.preventDefault();
				saveAuctions(0);
			});
            
            /*
            $('.process-icon-perform.xallegro').parent().on('click', function(e) {
				e.preventDefault();
				$('#allegro-perform-error-message').remove();
				if($('#configuration_form').length > 0) {
					var _configuration_form = $('#configuration_form');
				}
                else
					var _configuration_form = $('#_form');
				var data = _configuration_form.serializeArray();
				data[data.length] = {name: 'ajax', value: true};
				data[data.length] = {name: 'action', value: 'newAuctionTest'};
				
				$.ajax({
					type: 'POST',
					url: admin_xallegro_ajax_url,
					dataType: 'json',
					data: data,
					success: function(data)
					{
						if(data.status == '1') {
							var _message = $(
							'<div style="position: fixed;top: 0px;left: 40%;z-index: 10000;background-color: #FFF1A8;border: 1px solid #FFD96F;color: #000;font-weight: bold;font-size: 14px;padding: 5px 20px;border-radius: 0px 0px 3px 3px;width: 300px;">'+
								'<img alt="" src="../img/admin/ajax-loader-yellow.gif"></img>'+
								'Czekaj, trwa wystawianie aukcji'+
							'</div>'
							);
							$('body').prepend(_message);
							var btn_submit = $('#configuration_form_submit_btn');
							btn_submit.before('<input type="hidden" name="' + btn_submit.attr("name") + '" value="1" />');
							_configuration_form.submit();
						}
						else {
							var _error = $(
							'<div id="allegro-perform-error-message" class="error clear" style="display:block">'+
								data.content+
							'</div>'
							);
							_configuration_form.prepend(_error);
						}
					}
				});
				return false;
			})
            */
            
//            Change category
            $('[x-name=categories]').on('change', 'select', function() {
                var select = $(this);
                
                if (select.prop('value') == 0) {
                    return;
                }

                var selects = $('[x-name=categories]').find('select');
                var index = selects.index($(this));

                for (i=index+1;i<selects.length;i++) {
                    selects.eq(i).remove();
                }
            
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getCategories',
                        ajax : true,
                        id_allegro_category : select.prop('value')
                    },
                    beforeSend: function()
                    {
                        $('[x-name=categories]').fadeTo('fast', 0.2);
                        $('[x-name=category_fields]').hide().find('legend').nextAll().remove();
                        $('div[x-name=product_category_fields]').html('');
                    },
                    success: function(data)
                    {
                        if (data['categories'].length) {
                            var element = select.clone().insertAfter(select).hide();

                            $("option[value!='0']", element).remove();

                            $.each(data['categories'], function(index, category){
                                element.append(new Option(category['name'], category['id']));
                            });

                            element.show();
                        }

                        if (data['fields']) {
                            $('[x-name=category_fields]').append(data.fields).show();
                            
                            $('div[x-name=product_category_fields]').each(function() {
                                $(this).html(data.fields.replace(/category_fields/g, 'item[' + $(this).attr('x-index') + '][category_fields]'));
                            })
                        }
                    },
                    complete: function() {
                        $('[x-name=categories]').fadeTo('fast', 1);
                    }
                });
            });
            
//            Change pas profile
            $('select[name=pas]').on('change', function() {
                if ($(this).prop('value') != 0) {
                    $.ajax({
                        type: 'POST',
                        url: admin_xallegro_ajax_url,
                        dataType: 'json',
                        data: {
                            ajax : true,
                            action : 'getPas',
                            id: $(this).prop('value')
                        },
                        beforeSend: function() {
                            $('[x-name=pas]').parent().fadeTo('fast', 0.2);
                        },
                        success: function(data)
                        {
                            $.each(data, function(index, value) {
                                var input = $('[x-name=pas]').find('[name=' + index + ']');

                                if (input.is(':text') || input.is('textarea')) {
                                    input.prop('value', value);
                                } else if (input.is(':checkbox')) {

                                } else if (input.is('select')) {
                                    input.find('option').prop('selected', false);
                                    input.find('option[value=' + value + ']').prop('selected', true);
                                } else if (input.is(':radio')) {
                                    input.filter("[value='" + value + "']").prop('checked', true);
                                }
                            });

                            $('[x-name=shipments]').find('tr[x-id]').each(function() {
                                var id = $(this).attr('x-id');
                                
                                if (parseInt(id) in data.shipments) {
                                    $(this).find('input:checkbox').prop('checked', true).trigger('change');

                                    $(this).find('input:text').each(function(index) {
                                        $(this).prop('value', data.shipments[id][index]).trigger('focusout');
                                    });
                                } else {
                                    $(this).find('input:checkbox').prop('checked', false).trigger('change');
                                    $(this).find('input:text').prop('value', 0).trigger('focusout').eq(2).prop('value', 1).trigger('focusout');
                                }
                            });
                        },
                        complete: function() {
                            $('[x-name=pas]').parent().fadeTo('fast', 1);
                        }
                    });
                }
            });
            
//            Auction preview button
            $('[x-name=preview]').on('click', function() {
                var data = $('#configuration_form').serializeArray();
                data[data.length] = {name: 'index', value: $(this).attr('x-id')};
                data[data.length] = {name: 'ajax', value: true};
                data[data.length] = {name: 'action', value: 'preview'};
                
                var preview = window.open("", "popupWindow", "width=1200, height=600, scrollbars=yes");
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: data,
                    beforeSend: function(data)
                    {
						var cover = $('<div id="cover" style="z-index:2000000;position:fixed; left:0; top:0; width:100%; height:100%; background: rgba(0,0,0,0.6) url(../img/admin/ajax-loader-big.gif) center center no-repeat;"></div>');
						$(preview.document.body).append(cover);
					},
                    success: function(data)
                    {
                        $(preview.document.body).html(data['preview']);
                    }
                });
            
                return false;
            });
        
//            Toogle product category fields
            $('a[x-name=product_category_fields]').click(function() {
                var button = $(this);
                var container = button.parent().parent().parent();
                var fieldset = container.find('div[x-name=product_category_fields]');
                var semaphore = container.find('[x-name=product_category_fields_enabled]');

                if (fieldset.is(':visible')) {
                    fieldset.attr('x-enabled', 0).hide();
                    semaphore.prop('value', 0);
                } else {
                    fieldset.attr('x-enabled', 1).show();
                    semaphore.prop('value', 1);
                }
                
                return false;
            });
            
//            Description edit button
            $('a[x-name=description_edit]').click(function() {
                var button = $(this);
                var description = button.parent().parent().find('textarea[x-name=description]').val();

                var html = '';
                html += '<textarea class="rte" rows="10" cols="128">' + description + '</textarea>';
                html += '<br /><input class="button" type="button" value="Zapisz" x-name="description_save" />';

                $.fancybox(html, {
					autoDimensions: false,
                    autoResize: true,
                    width: 650,
                    height: 450,
                });
                
                tinySetup({
					// General options
					height: 400,
					mode: "specific_textareas",
					theme : "advanced",
					plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
					// Theme options
					skin:"default",
					content_css : '',
					theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : false,
					document_base_url : '/{$folder_admin}',
					font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
					elements : "nourlconvert,ajaxfilemanager",
					force_br_newlines : true,
					force_p_newlines : false,
					forced_root_block : "",
					cleanup_on_startup: false,
					trim_span_elements: false,
					verify_html: false,
					cleanup: false,
					convert_urls: false,
					relative_urls : false,
					valid_children : "+body[style],+div[style],+a[div|h1|h2|h3|h4|h5|h6|p|#text]",
					valid_elements : "*[*]",
					language : "pl"
				});
				$('[x-name=description_save]').on('click', function(){
					button.parent().parent().find('textarea[x-name=description]').val(tinyMCE.activeEditor.getContent({format : 'raw'}));

					$(this).off('click');
					$.fancybox.close();
				});
                
                return false;
            });
            
//            Description cudstom edit button
            $('a[x-name=description_custom_edit]').click(function() {
                var button = $(this);
                var description = button.parent().parent().find('textarea[x-name=description_custom]').val();

                var html = '';
                html += '<textarea class="rte" rows="10">' + description + '</textarea>';
                html += '<br /><input class="button" type="button" value="Zapisz" x-name="save" />';
                
                $.fancybox(html, {
                    autoDimensions: false,
                    autoResize: true,
                    width: 650,
                    height: 450,
                });
                
				tinySetup({
					// General options
					mode: "specific_textareas",
					theme : "advanced",
					height: 400,
					plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
					// Theme options
					skin:"default",
					content_css : '',
					theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : false,
					document_base_url : '/{$folder_admin}',
					font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
					elements : "nourlconvert,ajaxfilemanager",
					force_br_newlines : true,
					force_p_newlines : false,
					forced_root_block : "",
					cleanup_on_startup: false,
					trim_span_elements: false,
					verify_html: false,
					cleanup: false,
					convert_urls: false,
					relative_urls : false,
					valid_children : "+body[style],+div[style],+a[div|h1|h2|h3|h4|h5|h6|p|#text]",
					valid_elements : "*[*]",
					language : "pl"
				});
				
				$('[x-name=save]').on('click', function(){
					button.parent().parent().find('textarea[x-name=description_custom]').val(tinyMCE.activeEditor.getContent({format : 'raw'}));

					$(this).off('click');

					$.ajax({
						type: 'POST',
						url: admin_xallegro_ajax_url,
						data: {
							action : 'saveDesc',
							ajax : true,
							id_product : button.parent().parent().find('[x-name=id_product]').prop('value'),
							id_product_attribute: button.parent().parent().find('[x-name=id_product_attribute]').prop('value'),
							description: tinyMCE.activeEditor.getContent({format : 'raw'})
						},
						complete: function()
						{
							$.fancybox.close();
						}
					});
				});

                return false;
            });
            
            $('[x-name=title]').on('input xchange', function() {
                var strlen = function(string) {
                    var customChars = {
                        '<': 4,
                        '>': 4,
                        '&': 5,
                        '"': 6
                    }

                    var size = string.length;
                    var count = 0;

                    for(var key in customChars) {
                        count = string.split(key).length - 1;

                        size += (count*(customChars[key]-1));
                    }

                    return size;
                }
                
                var size = strlen($(this).prop('value'));
                
                while (size > 50) {
                    $(this).prop('value', $(this).prop('value').slice(0,-1));

                    size = strlen($(this).prop('value'));
                }

                $(this).parent().find('[x-name=counter]').html(size);
            });
            
            
//            Masowe
            $('[x-name=additional] input').on('change', function() {
                var input = $(this);
                $('[x-name=product_switch]').each(function() {
                    if (!$(this).is(':checked')) {
                        return;
                    }
                    
                    $(this).parent().parent().next().find('[x-name=' + input.attr('x-name') + ']').prop('checked', input.is(':checked'));
                });
            });
            
            $('div[x-name=images] img').on('click', function() {
                $(this).parent().parent().find('div').removeClass('main_image');
                $(this).parent().addClass('main_image');
                $(this).parent().parent().find('[x-name=image_main]').prop('value', $(this).attr('x-value'));
            });
            
            $('input[x-name=images]').on('change', function() {
                if ($(this).is(':checked')) {
                    if (!$(this).parent().parent().find('[x-name=image_main]').val()) {
                        $(this).next().trigger('click');
                    }
                }
            });
            
            
            $('a[x-name=bulk_manufacturer]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    var manufacturer = $(this).parent().parent().next().find('[x-name=manufacturer]').prop('value');
                    var input = $(this).parent().parent().next().find('[x-name=title]');
                    
                    if (manufacturer) {
                        input.prop('value', input.prop('value') + ' ' + manufacturer).trigger('xchange');
                    }
                });
                
                return false;
            });
            
            $('a[x-name=bulk_attribute]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    var atrributeName = $(this).parent().parent().next().find('[x-name=attribute_name]').prop('value');
                    var input = $(this).parent().parent().next().find('[x-name=title]');
                    
                    if (atrributeName) {
                        input.prop('value', input.prop('value') + ' ' + atrributeName).trigger('xchange');
                    }
                });
                
                return false;
            });
            
            $('a[x-name=bulk_price_buy_now]').on('click', function() {
                var markup = parseInt($('input[x-name=bulk_price_buy_now]').prop('value'))/100;
                var action = $(this).attr('x-action');
                
                if (action === 'down') {
                    markup *= -1;
                }
                
                $('[x-name=product_switch]:checked').each(function() {
                    var input = $(this).parent().parent().next().find('[x-name=price_buy_now]');
                    var value = parseFloat(input.attr('x-value'));
                    value += (value*markup);
                    
                    input.prop('value', value).trigger('xchange');
                });
                
                return false;
            });
            
            $('input[x-name=bulk_price_asking],input[x-name=bulk_price_minimal],input[x-name=bulk_quantity]').on('focusout', function() {
                var value = $(this).prop('value');
                var name = $(this).attr('x-name').substr(5) ;
                
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('[x-name=' + name + ']').prop('value', value).trigger('xchange');
                });
            });
            
            $('select[x-name=bulk_quantity_type],select[x-name=bulk_duration],select[x-name=bulk_template],select[x-name=bulk_overlay]').on('change', function() {
                var value = $(this).prop('value');
                var name = $(this).attr('x-name').substr(5) ;
                
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('[x-name=' + name + ']').prop('value', value);
                });
            });
            
//            Select all images
            $('[x-name=bulk_images_all]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').prop('checked', true).trigger('change');
                });
                
                return false;
            });
            
//            Select images invert
            $('[x-name=bulk_images_invert]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').each(function() {
                        $(this).prop('checked', !$(this).prop('checked'));
                    });
                });
                
                return false;
            });
            
//            Select images first
            $('[x-name=bulk_images_first]').on('click', function() {
                $('[x-name=product_switch]:checked').each(function() {
                    $(this).parent().parent().next().find('input[x-name=images]').eq(0).prop('checked', true).trigger('change');
                });
                
                return false;
            });
        }
        
        auctionEditForm = function() {
			
            $('input#name').autocomplete('index.php?controller=AdminXAllegroAuctions&token='+token+'&ajax=1&action=get_product_list', {}).result(function(event, data, formatted) {
                $('input#id_product').prop('value', data[1]);
                
                $.ajax({
                    type: 'POST',
                    url: admin_xallegro_ajax_url,
                    dataType: 'json',
                    data: {
                        action : 'getAttributes',
                        ajax : true,
                        id_product : data[1],
                    },
                    beforeSend: function()
                    {
                        $('fieldset#fieldset_0').parent().fadeTo('fast', 0.2);
                    },
                    success: function(data)
                    {
                        $('select#id_product_attribute option').remove();
                        
                        $.each(data, function(index, value) {
                            $('select#id_product_attribute').append(new Option(value.attribute_designation, value.id_product_attribute));
                        });
                        
                        if ($('select#id_product_attribute option').size() === 0) {
                            $('select#id_product_attribute').append(new Option('Brak', '0'));
                        }
                    },
                    complete: function()
                    {
                        $('fieldset#fieldset_0').parent().fadeTo('fast', 1);
                    }
                });
            });
            
            $('input#name').on('change', function() {
                if ($(this).prop('value') === '') {
                    $('input#id_product').prop('value', '0');
                }
            });
        }
        
		auctionList = function() {
			
			$('.action-finish').on('click', function(e){
				e.preventDefault();
				var items = new Array();
				items.push({
					'id'    : $(this).data('id'),
					'title' : $(this).data('title')
				});
				show_popup_form(items, 'finish');
			});
			
			$('[name="submitBulkbulkFinishxallegro_auction"]').on('click', function(e){
				e.preventDefault();
				if($('[name="xallegro_auctionBox[]"]:checked').length > 0) {
					var items = new Array();
					$('[name="xallegro_auctionBox[]"]:checked').each(function(){
						var finish = $(this).closest('tr').find('.action-finish');
						items.push({
							'id'    : finish.data('id'),
							'title' : finish.data('title')
						});
					});
					show_popup_form(items, 'finish');
				}
				else {
					alert('Nie wybrano aukcji do usunięcia.');
				}
			});
			
			$('.action-clone').on('click', function(e){
				e.preventDefault();
				alert('Funkcja aktualnie niedostępna.');
			});
			
			$('.action-redo').on('click', function(e){
				e.preventDefault();
				var items = new Array();
				items.push({
					'id'    : $(this).data('id'),
					'title' : $(this).data('title')
				});
				show_popup_form(items, 'redo');
			});
			
			$('[name="submitBulkbulkRedoxallegro_auction"].bulkRedoAuction').parent().on('click', function(e){
				if($('[name="xallegro_auctionBox[]"]:checked').length > 0) {
					var items = new Array();
					$('[name="xallegro_auctionBox[]"]:checked').each(function(){
						var redo = $(this).closest('tr').find('.action-redo');
						if(redo.length > 0 && items.length <= 25) {
							items.push({
								'id'    : redo.data('id'),
								'title' : redo.data('title')
							});
						}
					});
					show_popup_form(items, 'redo');
				}
				else {
					alert('Nie wybrano aukcji do zakończenia.');
				}
			});
			
			show_popup_form = function(items, action, form_title) {
				
				if(action == 'finish') var form_title = 'Aukcje do zakonczenia:';
				else if(action == 'redo') var form_title = 'Aukcje do ponowniego wystawienia:';
				
				var html = '<form action="'+location.href+'" method="POST" id="finish-auction-form">'+
						'<input type="hidden" name="action" value="'+action+'_auction">'+
						'<input type="hidden" name="id_allegro_account" value="'+$('.change_allegro_account option:selected').data('id_allegro_account')+'">'+
						'<div class="bootstrap" style="min-width:600px;">'+
							'<h2 style="margin-top:0;">'+form_title+'</h2>'+
							'<table style="width:100%;">'
				;
				for(i in items) {
					html += '<tr>'+
								'<td style="padding:6px;border-top:1px dashed #ddd;border-bottom:1px dashed #ddd;">'+
									'<strong>'+items[i]['title']+'</strong> (nr aukcji: '+items[i]['id']+')'+
									'<input type="hidden" name="item_id[]" value="'+items[i]['id']+'">'+
								'</td>'+
								'<td style="text-align:right;padding:6px;border-top:1px dashed #ddd;border-bottom:1px dashed #ddd;">'+
									'<i class="icon-times" style="color:#EB543A;cursor:pointer;"></i>'+
								'</td>'+
							'</tr>'
					;
				}
				if(action == 'finish') {
					html += '</table>'+
							'<p style="padding:6px;margin-bottom:6px;color:gray;">'+
								'<input style="vertical-align:-2px;" type="checkbox" name="cancel_all_bids" value="1" id="cancel-offer"> odwołaj wszystkie oferty<br>'+
							'</p>'+
							'<div id="cancel-offer-reason" style="margin-bottom:5px;display:none;">'+
								'<textarea name="cancel_reason" style="width:100%;height:50px;resize:vertical;box-sizing:border-box;padding:6px;" name="" placeholder="Podaj powód odwołania ofert"></textarea>'+
							'</div>'+
							'<button class="btn btn-default button" type="submit" name="finishAuction" value="1">zakończ aukcje</button>'+
						'</div>'+
					'</form>'
					;
				}
				else if(action == 'redo') {
					var currentdate = new Date(); 
					var datetime = 
						currentdate.getFullYear() + '-' +
						(currentdate.getMonth()+1) + '-' +
						currentdate.getDate() + ' ' +
						currentdate.getHours() + ':' +
						currentdate.getMinutes();
					html += '</table>'+
							'<div style="border:1px solid #ddd;background:#FFF;border-radius:3px;padding:8px 0 0 0;margin-top:10px;">'+
								'<div style="margin:0 8px 8px 8px;">'+
									'<label style="width:40%;float:left;padding-top:5px;">data rozpoczęcia aukcji</label>'+
									'<div style="width:60%;margin-left:40%;">'+
										'<input type="text" name="date_start" placeholder="RRRR-mm-dd GG:mm">'+
										'<p style="color:#008ABD;margin-top:5px;font-size:0.8em;">Pozostaw to pole puste, jeśli aukcje chcesz wystawić natychmiast.</p>'+
									'</div>'+
								'</div>'+
								'<div style="margin:0 8px 8px 8px;">'+
									'<label style="width:40%;float:left;padding-top:5px;">czas trwania standardowych aukcji</label>'+
									'<div style="width:60%;margin-left:40%;">'+
										'<select name="duration">'+
											'<option value="3">3 dni</option>'+
											'<option value="5">5 dni</option>'+
											'<option value="7">7 dni</option>'+
											'<option value="10">10 dni</option>'+
											'<option value="14">14 dni</option>'+
										'</select>'+
										//'<p style="color:#008ABD;margin-top:5px;font-size:0.8em;">Dla sklepów Allegro aukcje zostaną wystawione na 30 dni, niezależnie od wyboru powyżej.</p>'+
									'</div>'+
								'</div>'+
								'<div style="margin:0 8px 8px 8px;">'+
									'<label style="width:40%;float:left;padding-top:5px;">czas trwania aukcji dla sklepu</label>'+
									'<div style="width:60%;margin-left:40%;">'+
										'<select name="duration_shop">'+
											'<option value="30">30 dni</option>'+
											'<option value="0">do wyczerpania zapasów</option>'+
										'</select>'+
										//'<p style="color:#008ABD;margin-top:5px;font-size:0.8em;">Dla sklepów Allegro aukcje zostaną wystawione na 30 dni, niezależnie od wyboru powyżej.</p>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div style="margin:8px 0 0 0;">'+
								'<button class="btn btn-default button" type="submit" name="redoAuction" value="1">wystaw ponownie wybrane aukcje</button>'+
							'</div>'+
						'</div>'+
					'</form>'
					;
				}
				$.fancybox(html, {
					autoResize: true,
					autoDimensions: true,
				});
				
				$('#finish-auction-form .icon-times').click(function(){
					$(this).closest('tr').remove();
					if($('#finish-auction-form .icon-times').length <= 0) {
						parent.$.fancybox.close();
					}
				});
				
				$('#cancel-offer').change(function(){
					if($(this).prop('checked')) {
						$('#cancel-offer-reason').slideDown('fast');
					}
					else {
						$('#cancel-offer-reason').slideUp('fast');
					}
				});
				
				$('#finish-auction-form').submit(function(){
					if($('#cancel-offer').prop('checked') && $('#cancel-offer-reason textarea').val().replace(/[^a-zA-Z]/,'') == '') {
						$('#cancel-offer-reason p').remove();
						$('#cancel-offer-reason').append('<p style="font-size:0.9em;padding:3px 6px;color:#EB543A;"><i class="icon-exclamation-circle"></i> Należy podać powód odwołania ofert.</p>')
						return false;
					}
					else {
						return true;
					}
				});
			
			}
			
		}
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        _hooks = function() {
            $(settings.cast).on('focusout', _cast);
            $(settings.allegro_account).parent().parent().on('change', settings.allegro_account, _allegroAccountChange);
            $(settings.product.quantity.input).on('focusout', _quantityCheck);
            
            $(settings.product.images.images).find('img').on('click', _setImageMain);
            $(settings.product.images.images).find('input:checkbox').on('change', _imagesChange);
            
            $(settings.product.description_custom.buttonEdit).on('click', _editorDescriptionCustom);
            
            $(settings.product.switch).on('change', _productSwitch)
            $(settings.title.input).on('input', _inputTitle);
            $(settings.description.buttonEdit).on('click', _editorDescription);
            $(settings.preview.button).on('click', _preview);
            $(settings.category.fieldset).on('change', 'select', _categoryChange);
            $(settings.product_category_fields.button).on('click', _productCategoryField);
            
            
            $(settings.pas).find('select[name=pas]').on('change', _pasChange);
            $(settings.shipments).find('input:checkbox').on('change', _shipmentsSwitch);
        }
        
        _shipmentsSetSwitchs = function() {
            $(settings.shipments).find('input:checkbox').each(function() {
                if ($(this).is(':checked')) {
                    $(this).parent().parent().find('input:text').removeAttr('disabled');
                } else {
                    $(this).parent().parent().find('input:text').attr('disabled', 'disabled');
                }
            });
        }
        
//        _cast = function(e) {
//            var input = $(e.target);
//            var value = input.prop('value');
//            var type = input.attr('x13-type');
//            
//            if (type == 'float') {
//                input.prop('value', formatNumber(formatedNumberToFloat(value.replace(/[^0-9,\.]/g, ''), 2, null), 2, '', '.'))
//            } else if (type == 'integer') {
//                input.prop('value', parseInt(value));
//            }
//        }
        
        _quantityCheck = function(e) {
            var input = $(e.target);
            
            if (input.attr('x13-max')) {
                if (parseInt(input.prop('value')) > parseInt(input.attr('x13-max'))) {
                    input.prop('value', input.attr('x13-max'));

//                        Blur fix
                    setTimeout(function(){
                        alert('Ilość przedmiotów nie może być większa niż stan magazynowy.');
                    }, 50);
                }
            } else if (input.prop('value') > 1000) {
                input.prop('value', 1000);
                alert('Ilość przedmiotów nie może być większa niż 1000.');
            }
        }
        
        _allegroAccountChange = function(e) {
            var select = $(e.target);
            
            $.ajax({
                type: 'POST',
                url: admin_xallegro_ajax_url,
                dataType: 'json',
                data: {
                    action : 'getAllegroAccount',
                    ajax : true,
                    dataType: 'json',
                    allegro_account : select.val()
                },
                beforeSend: function()
                {
                    select.parent().parent().fadeTo('fast', 0.2);
                    
                    $(settings.product.item).each(function() {
                        if ($(this).is(':visible')) {
                            $(this).fadeTo('fast', 0.2);
                        }
                    });
                },
                success: function(json)
                {
                    var fieldset = select.parent().parent();
                    
                    fieldset.find('legend').nextAll().remove();
                    
                    $(settings.product.duration.input).each(function() {
                        var select = $(this);
                        var value = select.val();
                        
                        $(this).find('option').remove();
                        
                        $.each(json.durations, function (index, duration) {
                            select.append($('<option>', { 
                                value: duration.value,
                                text : duration.label
                            }));
                        });
                        
                        select.find('option[value=' + value + ']').prop('selected', true);
                    });
                    
                    fieldset.append(json.fieldset).fadeTo('fast', 1);
                    
                    $(settings.product.item).each(function() {
                        if ($(this).is(':visible')) {
                            $(this).fadeTo('fast', 1);
                        }
                    });
                }
            });
        }
        
//        _productSwitch = function(e) {
//            var input = $(e.target);
//            var container = input.parent().parent().next();
//            
//            if (input.is(':checked')) {
//                container.show();
//            } else {
//                container.hide();
//            }
//        }
        
//        _inputTitle = function(e) {
//            var input = $(e.target);
//            var size = _countTitleSize(input.prop('value'));
//            
//            while (size > 50) {
//                input.prop('value', input.prop('value').slice(0,-1));
//                
//                size = _countTitleSize(input.prop('value'));
//            }
//            
//            input.parent().find(settings.title.counter).html(size);
//        }
//        
//        _countTitleSize = function(string) {
//            var customChars = {
//                '<': 4,
//                '>': 4,
//                '&': 5,
//                '"': 6
//            }
//            
//            var size = string.length;
//            var count = 0;
//            
//            for(var key in customChars) {
//                count = string.split(key).length - 1;
//                
//                size += (count*(customChars[key]-1));
//            }
//            
//            return size;
//        }
        
//        _editorDescription = function(e) {
//            var button = $(e.currentTarget);
//            var description = button.parent().parent().find(settings.description.input).prop('value');
//            
//            var html = '';
//            html += '<textarea class="rte" rows="10">' + description + '</textarea>';
//            html += '<br /><input class="button" type="button" value="Zapisz" x13-name="description_save" />';
//            
//            $.fancybox(html, {
//                autoResize: false,
//                minWidth: 705,
//                minHeight: 300,
//                beforeShow: function () {
//                    tinySetup({
//                        height: 350
//                    });
//                    $(settings.description.buttonSave).on('click', function(){
//                        button.parent().parent().find(settings.description.input).prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));
//                        
//                        $(settings.description.buttonSave).off('click');
//                        $.fancybox.close();
//                    });
//                }
//            });
//            
//            return false;
//        }
//        
//        _editorDescriptionCustom = function(e) {
//            var button = $(e.currentTarget);
//            var description = button.parent().parent().find(settings.product.description_custom.input).prop('value');
//            
//            var html = '';
//            html += '<textarea class="rte" rows="10">' + description + '</textarea>';
//            html += '<br /><input class="button" type="button" value="Zapisz" x13-name="save" />';
//            
//            $.fancybox(html, {
//                autoResize: false,
//                minWidth: 705,
//                minHeight: 300,
//                beforeShow: function () {
//                    tinySetup({
//                        height: 350
//                    });
//                    $('[x13-name=save]').on('click', function(){
//                        button.parent().parent().find(settings.product.description_custom.input).prop('value', tinyMCE.activeEditor.getContent({format : 'raw'}));
//                        
//                        $('[x13-name=save]').off('click');
//                        
//                        $.ajax({
//                            type: 'POST',
//                            url: admin_xallegro_ajax_url,
//                            data: {
//                                action : 'saveDesc',
//                                ajax : true,
//                                id_product : button.parent().parent().find('[x13-name=id_product]').prop('value'),
//                                id_product_attribute: button.parent().parent().find('[x13-name=id_product_attribute]').prop('value'),
//                                description: tinyMCE.activeEditor.getContent({format : 'raw'})
//                            },
//                            success: function(data)
//                            {
//                            }
//                        });
//                        
//                        $.fancybox.close();
//                    });
//                }
//            });
//            
//            return false;
//        }
        
//        _setImageMain = function(e) {
//            var image = $(e.currentTarget);
//            
//            image.parent().parent().find('div').removeClass('main_image');
//            
//            image.parent().addClass('main_image');
//            
//            image.parent().parent().find(settings.product.images.inputImageMain).prop('value', image.attr('x13-value'));
//        }
        
//        _imagesChange = function(e) {
//            var input = $(e.currentTarget);
//            
//            if (input.is(':checked')) {
//                if (!input.parent().parent().find(settings.product.images.inputImageMain).val()) {
//                    $(this).next().trigger('click');
//                }
//            }
//        }
        
//        _categoryChange = function(e) {
//            var select = $(e.target);
//            
//            if (select.prop('value') == 0) {
//                return;
//            }
//            
//            var selects = $(settings.category.fieldset).find('select');
//            var index = selects.index($(this));
//
//            for (i=index+1;i<selects.length;i++) {
//                selects.eq(i).remove();
//            }
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: {
//                    action : 'getCategories',
//                    ajax : true,
//                    dataType: 'json',
//                    id_allegro_category : select.prop('value')
//                },
//                beforeSend: function()
//                {
//                    $(settings.category_fields.fieldset).hide().find('legend').nextAll().remove();
//                    $(settings.product_category_fields.fieldset).hide().html('');
//                },
//                success: function(data)
//                {
//                    if (data['categories'].length) {
//                        var element = select.clone().insertAfter(select).hide();
//
//                        $("option[value!='0']", element).remove();
//
//                        $.each(data['categories'], function(index, category){
//                            element.append(new Option(category['name'], category['id']));
//                        });
//
//                        element.show();
//                    }
//
//                    if (data['fields']) {
//                        $(settings.category_fields.fieldset).append(data.fields);
//                        $(settings.category_fields.fieldset).show();
//                        
//                        $(settings.product_category_fields.fieldset).each(function() {
//                            $(this).html(data.fields.replace(/category_fields/g, 'item[' + $(this).attr('x13-index') + '][category_fields]'));
//                            if ($(this).attr('x13-enabled') == '1') {
//                                $(this).show();
//                            }
//                        });
//                    }
//                }
//            });
//        }
//        
//        X_preview = function(e) {
//            var button = $(e.currentTarget);
//            var container = button.parent().parent().parent();
//            var data = {
//                action : 'preview',
//                ajax : true
//            }
//            
//            container.find('[x13-name]').each(function() {
//                if ($(this).is(':checkbox') && !$(this).is(':checked')) {
//                    return;
//                }
//                
//                if (container.find('[x13-name=' + $(this).attr('x13-name') + ']').size() == 1) {
//                    data[$(this).attr('x13-name')] = $(this).prop('value');
//                } else {
//                    if ($(this).attr('x13-name') in data) {
//                        data[$(this).attr('x13-name')].push($(this).prop('value'));
//                    } else {
//                        data[$(this).attr('x13-name')] = [$(this).prop('value')];
//                    }
//                }
//            });
//            
//            var preview = window.open("", "popupWindow", "width=1200, height=600, scrollbars=yes");
//            
//            $.ajax({
//                type: 'POST',
//                url: admin_xallegro_ajax_url,
//                dataType: 'json',
//                data: data,
//                success: function(data)
//                {
//                    $(preview.document.body).html(data['preview']);
//                }
//            });
//            
//            return false;
//        }
        
//        _productCategoryField = function(e) {
//            var button = $(e.currentTarget);
//            var container = button.parent().parent().parent();
//            var fieldset = container.find(settings.product_category_fields.fieldset);
//            var semaphore = container.find(settings.product_category_fields.semaphore);
//            
//            if (fieldset.is(':visible')) {
//                fieldset.attr('x13-enabled', 0).hide();
//                semaphore.prop('value', 0);
//            } else {
//                fieldset.attr('x13-enabled', 1).show();
//                semaphore.prop('value', 1);
//            }
//            
//            return false;
//        }
        
//        _shipmentsSwitch = function(e) {
//            var input = $(e.target);
//            
//            if (input.is(':checked')) {
//                input.parent().parent().find('input:text').removeAttr('disabled');
//            } else {
//                input.parent().parent().find('input:text').attr('disabled', 'disabled');
//            }
//        }
        
//        _pasChange = function(e) {
//            var select = $(e.target);
//            
//            if (select.prop('value') != 0) {
//                $.ajax({
//                    type: 'POST',
//                    url: admin_xallegro_ajax_url,
//                    dataType: 'json',
//                    data: {
//                        ajax : true,
//                        action : 'getPas',
//                        id: select.prop('value')
//                    },
//                    success: function(data)
//                    {
//                        $.each(data, function(index, value) {
//                            var input = $(settings.pas).find('[name=' + index + ']');
//                            
//                            if (input.is(':text') || input.is('textarea')) {
//                                input.prop('value', value);
//                            } else if (input.is(':checkbox')) {
//                                
//                            } else if (input.is('select')) {
//                                input.find('option').prop('selected', false);
//                                input.find('option[value=' + value + ']').prop('selected', true);
//                            } else if (input.is(':radio')) {
//                                input.filter("[value='" + value + "']").prop('checked', true);
//                            }
//                        });
//                        
//                        $(settings.shipments).find('tr[x13-shipment-id]').each(function() {
//                            var id = $(this).attr('x13-shipment-id');
//                            console.log(data.shipments);
//                            if (parseInt(id) in data.shipments) {
//                                $(this).find('input:checkbox').prop('checked', true).trigger('change');
//                                
//                                $(this).find('input:text').each(function(index) {
//                                    $(this).prop('value', data.shipments[id][index]).trigger('focusout');
//                                });
//                            } else {
//                                $(this).find('input:checkbox').prop('checked', false).trigger('change');
//                                $(this).find('input:text').prop('value', 0).trigger('focusout').eq(2).prop('value', 1).trigger('focusout');
//                            }
//                        });
//                    }
//                });
//            }
//        }
        
        init();
        
        return self;
    }
})(jQuery);

var testAuctionsMsg = '';
function testAuctions(start_index)
{
    if (start_index == 0) {
        testAuctionsMsg = '';
    }

    if ($('#configuration_form').length > 0) {
        var _form = $('#configuration_form');
    } else {
        var _form = $('#_form');
    }
    var data_temp = _form.serializeArray();
    var items = new Array();
    var data = new Array();
    var i, j;
    var limit = 20;
    var chunk;
    var prepared_data = new Array();
    for(i in data_temp) {
        if(data_temp[i].name.substr(0,4) !== 'item') {
            data.push({'name' : data_temp[i].name, 'value': data_temp[i].value});
        }
        else {
            item_index = data_temp[i].name.substr(0,10).replace(/[^0-9]/gi, '');
            if(items[item_index] == undefined) items[item_index] = new Array();
            items[item_index].push({'name' : data_temp[i].name, 'value': data_temp[i].value});
        }
    }

    data[data.length] = {name: 'ajax', value: true};
    data[data.length] = {name: 'action', value: 'newAuctionTest'};

    prepared_data = data;
    prepared_data.push({name : 'start_index', value: start_index});
    chunk = items.slice(start_index*limit,(start_index+1)*limit);
    for(j in chunk) {
        prepared_data = prepared_data.concat(chunk[j]);
    }

    $.ajax({
        type: 'POST',
        url: admin_xallegro_ajax_url,
        dataType: 'json',
        data: prepared_data,
        success: function(data) {
            testAuctionsMsg += data.content;
            
            if((start_index+1)*limit >= items.length) {
                $.fancybox(testAuctionsMsg, {width:600, 'autoDimensions': false});
            }
            else {
                testAuctions(start_index+1);
            }
        }
    });
}

function saveAuctions(start_index) {
	$('#allegro-perform-error-message').remove();
	if($('#configuration_form').length > 0) {
		var _configuration_form = $('#configuration_form');
	}
	else
		var _configuration_form = $('#_form');
	var _form = _configuration_form;
	var data_temp = _form.serializeArray();
	var items = new Array();
	var data = new Array();
	var i, j, limit = 20, chunk;
	var prepared_data = new Array();
	for(i in data_temp) {
		if(data_temp[i].name.substr(0,4) !== 'item') {
			data.push({'name' : data_temp[i].name, 'value': data_temp[i].value});
		}
		else {
			item_index = data_temp[i].name.substr(0,10).replace(/[^0-9]/gi, '');
			if(items[item_index] == undefined) items[item_index] = new Array();
			items[item_index].push({'name' : data_temp[i].name, 'value': data_temp[i].value});
		}
	}
	
	var items_length = $('[x-name="product_switch"]:checked').length;
	data[data.length] = {name: 'ajax', value: true};
	data[data.length] = {name: 'action', value: 'save_new_auctions'};
	
	prepared_data = data;
	prepared_data.push({name : 'start_index', value: start_index});
	chunk = items.slice(start_index*limit,(start_index+1)*limit);
	for(j in chunk) {
		prepared_data = prepared_data.concat(chunk[j]);
	}
	$.ajax({
		type: 'POST',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: prepared_data,
		beforeSend: function() {
			if($('#allegro-perform-info-message').length == 0) {
				var _message = $(
					'<div id="allegro-perform-info-message" class="alert" style="color: #31B0D5;border: 1px solid #C5E9F3;background-color: #F8FCFE;">'+
						'<p>'+
							'<i class="icon-refresh icon-spin" style="font-size:2em;vertical-align:middle;"></i> &nbsp; '+
							'<strong>Czekaj, trwa zapisywanie aukcji</strong> ( <b class="current">-</b> / <b class="total">'+items_length+'</b> )'+
						'</p>'+
					'</div>'
				);
				$('html, body').animate({'scrollTop' : 0}, 'fast');
				if($('#allegro-perform-error-message').length > 0) 
					$('#allegro-perform-error-message').replaceWith(_message);
				else
					_message.insertBefore(_form).hide().slideDown('slow');
			}
			else{
				$('#allegro-perform-info-message strong').text('Czekaj, trwa zapisywanie aukcji ');
					$('#allegro-perform-info-message .current').text('-');
					$('#allegro-perform-info-message .total').text(items_length);
			}
		},
		success: function(data) {
			if(data.status == '1') {
				if((start_index+1)*limit >= items.length) {
					$('#allegro-perform-info-message strong').text('Sprawdzanie poprawności aukcji ');
					$('#allegro-perform-info-message .current').text('-');
					checkAuctions();
				}
				else {
					saveAuctions(start_index+1);
					$('#allegro-perform-info-message strong').text('Czekaj, trwa zapisywanie aukcji ');
					var current_number = $('#allegro-perform-info-message .current').text();
					$('#allegro-perform-info-message .current').text(parseInt(current_number.replace('-','0')) + parseInt(data.number));
				}
			}
			else {
				var _error = $(
				'<div id="allegro-perform-error-message" class="alert alert-danger">'+
					data.content +
				'</div>'
				);
				$('#allegro-perform-info-message').replaceWith(_error);
			}
		}
	});
}

function checkAuctions() {
	data = new Array();
	data.push({name: 'ajax', value: true});
	data.push({name: 'action', value: 'check_auction'});
	$.ajax({
		type: 'POST',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: data,
		success: function(data) {
			if(data.status == 1) {
				$('#allegro-perform-info-message strong').text('Sprawdzanie poprawności aukcji ');
				var total = parseInt($('#allegro-perform-info-message .total').text());
				$('#allegro-perform-info-message .current').text(total-data.number);
				checkAuctions();
			}
			else if(data.status == 2) {
				$('#allegro-perform-info-message strong').text('Wystawianie aukcji ');
				$('#allegro-perform-info-message .current').text('-');
				sendAuctions();
			}
			else {
				var _error = $(
				'<div id="allegro-perform-error-message" class="alert alert-danger">'+
					data.content +
				'</div>'
				);
				$('#allegro-perform-info-message').replaceWith(_error);
			}
		}
	});
}

function sendAuctions() {
	data = new Array();
	data.push({name: 'ajax', value: true});
	data.push({name: 'action', value: 'send_auction'});
	$.ajax({
		type: 'POST',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: data,
		success: function(data) {
			if(data.status == 1) {
				$('#allegro-perform-info-message strong').text('Wystawianie aukcji ');
				var total = parseInt($('#allegro-perform-info-message .total').text());
				$('#allegro-perform-info-message .current').text(total-data.number);
				sendAuctions();
			}
			else if(data.status == 2) {
				$('#allegro-perform-info-message strong').text('Wszystkie aukcje zostały wystawione ');
				location.reload();
			}
			else {
				var _error = $(
				'<div id="allegro-perform-error-message" class="alert alert-danger">'+
					data.content +
				'</div>'
				);
				$('#allegro-perform-info-message').replaceWith(_error);
			}
		}
	});
}

function updateStatus() {
	$('#allegro-update-fields-cats').slideDown(400);
	$('#configuration_form').fadeOut(400);
	$.ajax({
		type: 'GET',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: {
			action : 'getAllegroUpdateStatus',
			ajax : true,
			dataType: 'json'
		},
		success: function(json) {
			if(json) {
				if(parseInt(json.progress) > 30) {
					$('#allegro-update-progress-bar').removeClass('small');
				}
				else {
					$('#allegro-update-progress-bar').addClass('small');
				}
				$('#allegro-update-progress-bar .aucf-message').text(json.message);
				$('#allegro-update-progress-bar .aucf-progress').text(parseInt(json.progress) == 0 ? '' : json.progress + '%');
				$('#allegro-update-progress-bar > div').outerWidth(json.progress + '%');
				if(json.code == 'DOWNLOADING') {
					$('#allegro-update-progress-bar div').fadeOut(500, function(){$('#allegro-update-progress-bar div').fadeIn(500);});
					setTimeout(function(){updateStatus();}, 1000);
				}
				else if(json.code == 'IN_PROGRESS') {
					setTimeout(function(){updateStatus();}, 1000);
				}
				else if(json.code == 'COMPLETE_CAT') {
					updateFormsFields();
				}
				else if(json.code == 'COMPLETE_FIELDS') {
					setTimeout(function(){$('#allegro-update-fields-cats').slideUp(400, location.reload());}, 5000);
				}
			}
			else {
				setTimeout(function(){updateStatus();}, 1000);
			}
		}
	});
}

function updateCategoryTree() {
	$.ajax({
		type: 'GET',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: {
			action : 'downloadCategoryTree',
			ajax : true,
			dataType: 'json',
			version: newest_VERSION_CATEGORY_LIST
		}
	});
	setTimeout(function() {updateStatus();}, 2000);
}

function updateFormsFields() {
	$.ajax({
		type: 'GET',
		url: admin_xallegro_ajax_url,
		dataType: 'json',
		data: {
			action : 'downloadFormFields',
			ajax : true,
			dataType: 'json',
			version: newest_VERSION_SELL_FORM_FIELDS
		}
	});
	setTimeout(function() {updateStatus();}, 2000);
}

function syncClosedStatus(start, total) {
	doAdminAjax(
		{
			controller: 'AdminXAllegroConfiguration',
			action: 'syncClosedStatus',
			token: token,
			ajax: true,
			start: start,
			total: total
		},
		function(response) 
		{
			var data = $.parseJSON(response);
			
			if (data.status == 'ok') {
				if(data.performed < data.total) {
					showSuccessMessage('Trwa aktualizacja wystawień - przetworzono: '+data.performed+'/'+data.total+' aukcji.');
					syncClosedStatus(start+1, data.total);
				}
				else {
					showSuccessMessage('Aktualizacja wystawień zakończona.');
				}
				
			} else {
				showErrorMessage(data.message);
			}
		}
	);
}
