<?php

	$start = microtime(true);

	define('_PRESTA_DIR_', dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME']))));
	
	require_once (_PRESTA_DIR_ . '/config/config.inc.php');
	require_once (_PRESTA_DIR_ . '/init.php');
    require_once (dirname(__FILE__) . '/x13allegro.ion.php');
    
    if (strlen(X13_ION_VERSION)) {
        $pattern = '/.*-7\.php$/';
    } else {
        $pattern = '/.*[^-7]\.php$/';
    }
    
    $path = dirname(__FILE__) . '/classes/';
    $dir = dir($path);
    while (false !== ($file = $dir->read())) {
        if (!is_dir($path . $file) && preg_match($pattern, $file)) {
            require_once ($path . $file);
        }
    }
    
    echo '<pre>';
    
    if (Tools::getValue('token') != XAllegroConfiguration::get('SYNC_TOKEN')) {
        echo 'Token error';
        exit;
    }
    
    if((int)XAllegroConfiguration::get('LOCK_SYNC') != 0 && time() - XAllegroConfiguration::get('LOCK_SYNC') < 1800) {
		echo 'MAX ONE INSTANCE!';
		XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('!!! SYNC LOCKED: '.(time() - XAllegroConfiguration::get('LOCK_SYNC')).'sek !!!')->save();
		if(!isset($_GET['ignore_limits'])) exit;
	}

	XAllegroConfiguration::updateValue('LOCK_SYNC',time());
    
    $sync = new XAllegroSync();
    
    //Pobieranie zamówień i aktualizacja stanu magazynowego po zakupie na Allego
    XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('@@@ START sync orders')->save();
    $sync->sync();
    XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('@@@ END sync orders')->save();
    
    //Synchronizacja stanów magazynowych na Allegro
    XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('### START sync quantities')->save();
    $sync->sync_quantities();
    XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('### END sync quantities')->save();
    
    //Aktualizacja statusów zakończonych aukcji
    //XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('### START sync allegro closed')->save();
    //$sync->sync_allegro_closed_status();
    //XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setCode()->setMessage('### END sync allegro closed')->save();
    
    //Czyszczenie starych logów (trzymamy logi tylko z dwóch tygodni)
    $logs_folder = dirname(__FILE__).'/log/';
    $dir = opendir($logs_folder);
    $deleted = array();
    while($file = readdir($dir)) {
		if(strpos($file, '.log')) {
			if(time() - strtotime(str_replace('.log', '', $file)) > 21*86400) {
				$deleted[] = $file;
				unlink($logs_folder.$file);
			}
		}
	}
	if(count($deleted)) {
		XAllegroLogger::getInstance()->setAuction(null)->setType('inf')->setMessage('deleted logs: '.implode(', ', $deleted))->save();
	}
	
	XAllegroConfiguration::updateValue('LOCK_SYNC', 0);
	
	$time_elapsed_secs = microtime(true) - $start;
	die('exec time (sec): ' . $time_elapsed_secs);
    
?>
