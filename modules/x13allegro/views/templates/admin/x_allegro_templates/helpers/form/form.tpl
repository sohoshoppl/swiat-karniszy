{extends file="helpers/form/form.tpl"}

{block name="after"}
{if $is_bootstrap}
<div class="panel col-lg-12">
	<div class="panel-heading">
		Dostępne znaczniki
	</div>
	<table class="table" cellspacing="0" cellpadding="0" style="margin: auto;">
		<thead>
			<tr>
				<th>Znacznik</th>
				<th>Opis</th>
			</tr>
		</thead>
		<tbody>
			{foreach $template_variables as $key => $value}
			<tr class="{cycle values=",alt_row"}">
				<td>{literal}{{/literal}{$key}{literal}}{/literal}</td>
				<td>{$value}</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
</div>
{else}
<br />
<fieldset>
	<legend>Dostępne znaczniki</legend>
	<table class="table" cellspacing="0" cellpadding="0" style="margin: auto;">
		<thead>
			<tr>
				<th width="300">Znacznik</th>
				<th>Opis</th>
			</tr>
		</thead>
		<tbody>
			{foreach $template_variables as $key => $value}
			<tr class="{cycle values=",alt_row"}">
				<td>{literal}{{/literal}{$key}{literal}}{/literal}</td>
				<td>{$value}</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
</fieldset>
{/if}





{if $is_bootstrap}
<div class="panel col-lg-12">
	<div class="panel-heading">
		Przykładowa galeria zdjęć dla znacznika &#123;gallery&#125;
	</div>
{else}
<br />
<fieldset>
	<legend>Przykładowa galeria zdjęć dla znacznika &#123;gallery&#125;</legend>
{/if}


<p>Kod dodajemy w sekci <b>&lt;style&gt;</b> w naszym szablonie<p/>
{literal}
<textarea rows="15" cols="50" style="width:99%;">
.x13allegro_galeria a, .x13allegro_galeria a:hover { text-decoration: none; }
.x13allegro_galeria img { border: 0; margin: 0; padding: 0; }
.x13allegro_galeria { width: 740px; margin: auto;}
.x13allegro_galeria .x13allegro_container { width: 740px; position: relative; text-align: center; z-index: 1; }
.x13allegro_galeria .x13allegro_image_li{ margin: 0 6px 12px 0; display: inline-block; vertical-align: top; }

.x13allegro_galeria .a_thumb { border: 1px solid #eee; display: block; cursor: default; } 
.x13allegro_galeria .a_thumb div{display:block; width: 130px; height: 130px; margin:2px; display: block; background-position: center center; background-size: contain; background-repeat: no-repeat; cursor: pointer;}
.x13allegro_galeria .a_thumb:hover { border:#666 1px solid; }

.x13allegro_galeria .x13allegro_zdj { height: 420px; width: 100%; }
.x13allegro_galeria .a_hidden { position: absolute; left: 0; top: 0; height: 420px; text-align: center; width: 740px; overflow: hidden; z-index: 1; background: #fff; display: block; cursor: default;}
.x13allegro_galeria .a_hidden img { border: none; max-width: 740px; max-height: 400px; }
.x13allegro_galeria .x13allegro_image_li:hover .a_hidden { z-index: 999;}
</textarea>
{/literal}

{if $is_bootstrap}
</div>
{else}
<br />
</fieldset>
{/if}


{/block}

{block name=autoload_tinyMCE}
{if $is_bootstrap}
	var path = $(location).attr('pathname');
	var path_array = path.split('/');
	path_array.splice((path_array.length - 2), 2);
	var final_path = path_array.join('/');
	setTimeout(function(){
		tinyMCE.baseURL = final_path+'/js/tiny_mce';
		tinyMCE.suffix = '.min';
		tinySetup({
			editor_selector :"autoload_rte",
			mode : "specific_textareas",
			theme : "advanced",
			height: 400,
			content_css : "content.min.css",
			plugins : "code,autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,colorpicker",
			toolbar1 : "styleselect,|,formatselect,|,fontselect,|,fontsizeselect,", 
			toolbar2 : "bold,italic,underline,|,strikethrough,superscript,subscript,|,alignleft,aligncenter,alignright,alignjustify,|,forecolor,colorpicker,backcolor,|,bullist,numlist,outdent,indent",
			toolbar3 : "code,|,table,|,cut,copy,paste,searchreplace,|,blockquote,|,undo,redo,|,link,unlink,anchor,|,image,emoticons,media,|,inserttime,|,preview,|,visualblocks ",
			force_br_newlines : true,
			force_p_newlines : false,
			forced_root_block : "",                   
			cleanup_on_startup: false,
			trim_span_elements: false,
			verify_html: false,
			cleanup: false,
			convert_urls: false,
			relative_urls : false,
			valid_children : "+body[style],+div[style],+a[div|span|h1|h2|h3|h4|h5|h6|p|#text]",
			valid_elements : "*[*]",
			language : 'pl'
		});
	}, 500);
{else}
    tinySetup({
		// General options
		mode: "specific_textareas",
		theme : "advanced",
		width : 1200,
		height : 400,
		skin:"default",
		content_css : '',
		plugins : "lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		document_base_url : '/{$folder_admin}',
		font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
		elements : "nourlconvert,ajaxfilemanager",
		force_br_newlines : true,
		force_p_newlines : false,
		forced_root_block : "",
		cleanup_on_startup: false,
		trim_span_elements: false,
		verify_html: false,
		cleanup: false,
		convert_urls: false,
		relative_urls : false,
		valid_children : "+body[style],+div[style],+a[div|h1|h2|h3|h4|h5|h6|p|#text]",
		valid_elements : "*[*]",
        language : 'pl'
	});
{/if}
{/block}

