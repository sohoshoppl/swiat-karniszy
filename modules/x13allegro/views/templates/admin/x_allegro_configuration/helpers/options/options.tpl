{extends file="helpers/options/options.tpl"}

{block name="input"}
    {if $key == 'CRON_URL'}
        <div class="col-lg-9">
            <input type="{$field['type']}" size="{if isset($field['size'])}{$field['size']|intval}{else}5{/if}" value="{$field['value']|escape:'htmlall':'UTF-8'}" readonly="readonyl" />
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}

{block name="defaultOptions"}
    {if $xallegro_update}
		{if $is_bootstrap}
		<div class="panel">
			<div class="panel-heading">Aktualizacja</div>
			<table class="table" cellspacing="0" cellpadding="0" style="width: 100%; margin: auto;">
                <col width="120px"/>
                <col width="200px"/>
                <col />
                <thead>
                    <tr class="nodrag nodrop">
                        <th class="center">Wersja</th>
                        <th class="center">Data wydania</th>
                        <th>Lista zmian</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $xallegro_update->changelog->bundle AS $bundle}
                    
                        <tr>
                            <td class="center">{$bundle->attributes()->version}</td>
                            <td class="center">{$bundle->attributes()->date|date_format:"%d.%m.%Y"}</td>
                            <td>
                                <ul>
								{foreach $bundle->list AS $change}
									<li>{$change}</li>
								{/foreach}
                                </ul>
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
		</div>
		{else}
        <fieldset>
            <legend>Aktualizacja</legend>
            <table class="table" cellspacing="0" cellpadding="0" style="width: 65%; margin: auto;">
                <col width="120px"/>
                <col width="200px"/>
                <col />
                <thead>
                    <tr class="nodrag nodrop">
                        <th class="center">Wersja</th>
                        <th class="center">Data wydania</th>
                        <th>Lista zmian</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $xallegro_update->changelog->bundle AS $bundle}
                        <tr>
                            <td class="center">{$bundle->attributes()->version}</td>
                            <td class="center">{$bundle->attributes()->date|date_format:"%d.%m.%Y"}</td>
                            <td>
                                <ul>
                                    {foreach $bundle->list AS $change}
                                        <li>{$change}</li>
                                    {/foreach}
                                </ul>
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </fieldset>
        <br />
		{/if}
    {/if}
    
    {$smarty.block.parent}
{/block}

{block name="after"}
<script type="text/javascript">
    var XAllegro = new $.X13Allegro();
    XAllegro.configurationForm();
</script>
{/block}
