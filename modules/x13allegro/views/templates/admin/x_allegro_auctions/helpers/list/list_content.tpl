{extends file="helpers/list/list_content.tpl"}

{block name="open_td"}
    <td
        {if isset($params.position)}
            id="td_{if !empty($id_category)}{$id_category}{else}0{/if}_{$tr.$identifier}"
        {/if}
        class="{if !$no_link}pointer{/if}
        {if isset($params.position) && $order_by == 'position'  && $order_way != 'DESC'} dragHandle{/if}
        {if isset($params.class)} {$params.class}{/if}
        {if isset($params.align)} {$params.align}{/if}"
        {if (!isset($params.position) && !$no_link && !isset($params.remove_onclick))}
            onclick="document.location = '{$current_index}&{$identifier}={$tr.$identifier}{if $view}&view{else}&update{/if}{$table}&token={$token}'">
        {else}
        >
    {/if}
    {if $key == 'dummy' AND $tr.thumbnail}
        <img width=48 src="{$tr.thumbnail_small}" data-toggle="tooltip" class="big-tooltip label-tooltip" data-original-title="&lt;img src=&quot;{$tr.thumbnail}&quot; &gt;" data-html="true" data-placement="right">
    {/if}
{/block}
