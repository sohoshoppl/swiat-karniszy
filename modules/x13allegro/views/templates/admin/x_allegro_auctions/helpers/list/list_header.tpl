{extends file="helpers/list/list_header.tpl"}
{block name="override_header"}

	{if !$is_bootstrap}
	<p style="text-align: right;">
		<a href="{$currentIndex}&token={$token}" class="button active">Sprzedaje</a>
		<a href="{$currentIndex}&token={$token}&show=sold" class="button">Sprzedane</a>
		<a href="{$currentIndex}&token={$token}&show=notsold" class="button">Niesprzedane</a>
		<a href="{$currentIndex}&token={$token}&show=future" class="button">Oczekujące do wystawienia</a>
	</p>
	<fieldset x-name="allegro_account">
		<legend>
			<img alt="" src="../modules/x13allegro/logo.gif" />  Allegro
		</legend>
		<label>Konto Allegro </label>
		<div class="margin-form">
			<select class="change_allegro_account" onChange="javascript:location.href=this.value;">
				{foreach from=$allegroAccounts item="allegroAccount"}
				<option value="{$allegroAccount.url}"{if $allegroAccount.selected} selected="selected"{/if} data-id_allegro_account="{$allegroAccount.id}">{$allegroAccount.username}</option>
				{/foreach}
			</select>
		</div>
	<div class="clear"></div>
	</fieldset><br>
		
	{else}

	<div class="panel col-lg-12" x-name="allegro_account">
		<div class="panel-heading">
			<img alt="" src="../modules/x13allegro/img/AdminXAllegroMain.png" /> Allegro
		</div>
		<label>Konto Allegro </label>    
		<div class="margin-form">
			<select class="change_allegro_account" onChange="javascript:location.href=this.value;">
				{foreach from=$allegroAccounts item="allegroAccount"}
				<option value="{$allegroAccount.url}"{if $allegroAccount.selected} selected="selected"{/if} data-id_allegro_account="{$allegroAccount.id}">{$allegroAccount.username}</option>
				{/foreach}
			</select>
		</div>
		<div class="clear"></div>
	</div>
	
	{/if}
{/block}
{block name="startForm"}
	<form method="post" action="{$override_action_attr}" class="form-horizontal clearfix" id="form-{$list_id}">
{/block}