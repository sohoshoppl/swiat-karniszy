{extends file="helpers/form/form.tpl"}

{block name="field"}
    {$smarty.block.parent}
{/block}

{block name="input"}
	{if $input.name == 'name'}
		{$smarty.block.parent} 
		{if $fields_value.id_product}
			<a title="Edytuj produkty" href="{$link->getAdminLink('AdminProducts')}&updateproduct&id_product={$fields_value.id_product}">
				<img alt="Edytuj produkt" src="../img/admin/edit.gif" />
			</a>
			<a class="btn btn-default button bt-icon" title="Zobacz produkty w sklepie" target="_blank" href="{$link->getProductLink($fields_value.id_product)}">
				<img alt="Zobacz produkt w sklepie" src="../img/admin/subdomain.gif" />
			</a>
		{/if}
	{elseif $input.name == 'title'}
		{if $is_bootstrap}
		<span style="display:inline-block; width:60%;">{$smarty.block.parent}</span>
		<a style="vertical-align:top;" class="btn btn-default button bt-icon" title="Zobacz aukcji" target="_blank" href="{$fields_value.service}/show_item.php?item={$fields_value.id_auction}">
			<img alt="Zobacz aukcje" src="../modules/x13allegro/img/AdminXAllegroMain.png" /> zobacz aukcję
		</a>
		{else}
		{$smarty.block.parent}
		<a title="Zobacz aukcji" target="_blank" href="{$fields_value.service}/show_item.php?item={$fields_value.id_auction}">
			<img alt="Zobacz aukcje" src="../modules/x13allegro/logo.gif" />
		</a>
		{/if}
	{elseif $input.name == 'price_buy_now'}
		<div class="input-group">
			<span class="input-group-addon">zł</span>
			<input type="text" 
				value="{$fields_value[$input.name]|escape:'htmlall':'UTF-8'|number_format:2:'.':''}" 
				{if isset($input.size)}size="{$input.size}"{/if} 
				disabled="disabled" />
		</div>
	{elseif $input.name == 'closed'}
		<input type="text" 
			value="{if $fields_value[$input.name]}Tak{else}Nie{/if}" 
			{if isset($input.size)}size="{$input.size}"{/if} 
			disabled="disabled" />
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="script"}
    var XAllegro = new $.X13Allegro();
    XAllegro.auctionEditForm();
{/block}
