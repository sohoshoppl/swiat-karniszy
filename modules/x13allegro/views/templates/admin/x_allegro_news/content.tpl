<fieldset>
    <legend>Aktualności</legend>
    <ol style="margin-left: 30px;">
        <li>{l s='Informative only'}</li>
        <li style="color: orange;">{l s='Warning'}</li>
        <li style="color: orange;">{l s='Error'}</li>
        <li style="color: red;">{l s='Major issue (crash)!'}</li>
    </ol>
</fieldset>