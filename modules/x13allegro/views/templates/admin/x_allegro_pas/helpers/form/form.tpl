{extends file="helpers/form/form.tpl"}

{block name="other_input"}
    {if $key == 'shipments'}
        <div>
            <table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
                <thead>
                    <tr>
                        <th>Darmowe opcje dostawy</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $field as $row}
                        {if $row.type == 0}
                            {$row_value = $fields_value.shipments[$row.id]}
                            {$enable = ($row.value == 1 && in_array($row_value, array(1,3,5,7))) || ($row.value == 2 && in_array($row_value, array(2,3,6,7))) || ($row.value == 4 && in_array($row_value, array(4,5,6,8)))}
                            <tr class="{cycle values=",alt_row"}">
                                <td style="padding-top:6px;padding-bottom:6px">
									<input{if $enable} checked="checked"{/if} type="checkbox" name="shipments[{$row.id}][]" value="{$row.value}" /> {$row.name}
								</td>
                            </tr>
                        {/if}
                    {/foreach}
                </tbody>
            </table>
            <br />
            <table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
                <thead>
                    <tr>
                        <th>Płatność z góry</th>
                        <th width="140">Pierwsza sztuka</th>
                        <th width="140">Kolejna sztuka</th>
                        <th width="120">Ilość w paczce</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $field as $row}
                        {if $row.type == 1}
                            {$row_value = $fields_value.shipments[$row.id]}

                            <tr class="{cycle values=",alt_row"}">
                                <td><input type="checkbox" name="shipments[{$row.id}][enabled]" value="1" {if $row_value.enabled}checked="checked"{/if} x-name="switch" /> {$row.name}</td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][0]" value="{$row_value.0|number_format:2:".":""}" x-type="float" />
                                        <span class="input-group-addon"> zł</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][1]" value="{$row_value.1|number_format:2:".":""}" x-type="float" />
                                        <span class="input-group-addon"> zł</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][2]" value="{$row_value.2}" x-type="float" />
                                    </div>
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                </tbody>
            </table>
            <br />
            <table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
                <thead>
                    <tr>
                        <th>Płatność przy odbiorze</th>
                        <th width="140">Pierwsza sztuka</th>
                        <th width="140">Kolejna sztuka</th>
                        <th width="120">Ilość w paczce</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $field as $row}
                        {if $row.type == 2}
                            {$row_value = $fields_value.shipments[$row.id]}

                            <tr class="{cycle values=",alt_row"}">
                                <td><input type="checkbox" name="shipments[{$row.id}][enabled]" value="1" {if $row_value.enabled}checked="checked"{/if} x-name="switch" /> {$row.name}</td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][0]" value="{$row_value.0|number_format:2:".":""}" x-type="float" />
                                        <span class="input-group-addon"> zł</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][1]" value="{$row_value.1|number_format:2:".":""}" x-type="float" />
                                        <span class="input-group-addon"> zł</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group col-lg-2">
                                        <input style="width: 100px;" type="text" name="shipments[{$row.id}][2]" value="{$row_value.2}" x-type="float" />
                                    </div>
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
{/block}

{block name="script"}
    var XAllegro = new $.X13Allegro();
    XAllegro.pasForm();
{/block}
