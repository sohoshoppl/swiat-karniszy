{if $is_bootstrap}
<div class="panel">
	<form class="form-horizontal" action="{$form_action}" method="post">
	<input type="hidden" name="action" value="{$action}">
	<div class="panel-heading">{l s='Powiąż przewożników w sklepie i formy dostawy na Allegro' mod='x13allegro'}</div>
	{foreach from=$fields_shipments item=fs}
	<div class="form-group">
		<label class="control-label col-lg-6">{$fs.name}</label>
		<div class="col-lg-6">
			<select class="select fixed-width-xxl" name="id_fields_shipment[{$fs.id}]">
				<option value="0"> - </option>
				{foreach from=$carriers item=c}
				<option{if isset($assign[$fs.id]) && $assign[$fs.id] == $c.id_reference} selected="selected"{/if} value="{$c.id_reference}">{$c.name}</option>
				{/foreach}
			</select>
		</div>
	</div>
	{/foreach}
	<div class="panel-footer">
		<button type="submit" name="submitXAllegroCarrier" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Zapisz' mod='x13allegro'}</button>
	</div>
	</form>
</div>

{else}

<h2>Statusy dla płatności </h2>
<form class="form-horizontal" action="{$form_action}" method="post">
<input type="hidden" name="action" value="{$action}">
<fieldset>
	<legend>{l s='Powiąż przewożników w sklepie i formy dostawy na Allegro' mod='x13allegro'}</legend>
	{foreach from=$fields_shipments item=fs}
		<label>{$fs.name}</label>
		<div class="margin-form">
			<select class="select fixed-width-xxl" name="id_fields_shipment[{$fs.id}]">
                <option value="0"> - </option>
                {foreach from=$carriers item=c}
                <option{if isset($assign[$fs.id]) && $assign[$fs.id] == $c.id_reference} selected="selected"{/if} value="{$c.id_reference}">{$c.name}</option>
                {/foreach}
            </select>
		</div>
		<div style="clear:both;"></div>
	{/foreach}
	<center>
		<button type="submit" name="submitXAllegroCarrier" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Zapisz' mod='x13allegro'}</button>
	</center>
</fieldset>
</form>

{/if}
