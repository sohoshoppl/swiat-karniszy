<style type="text/css">
{literal}
	#allegro-update-fields-cats{display:none;background:#DCF4F9; color:#2794AB; padding:5px 15px 10px 15px; border-radius:3px; border-left:3px solid #2794AB; margin-bottom:17px;}
	#allegro-update-progress-bar{height:20px;background:#FFF;border-bottom:1px solid #2794AB;}
	#allegro-update-progress-bar > div{width:0; background:#2794AB; font-size:0.8em; padding:2px; line-height:15px; height:19px;text-align:right;}
	#allegro-update-progress-bar p{color:#FFF;margin:0;padding:0 2px;min-width:200px;}
	#allegro-update-progress-bar.small p{color:#2794AB;margin-left:100%; position:relative;left:5px;text-align:left;}
{/literal}
</style>

<div id="allegro-update-fields-cats">
	<p style="margin-bottom:5px;">Proszę czekać  - trwa aktualizacja pól Allegro</p>
	<div id="allegro-update-progress-bar" class="small">
		<div>
			<p>
				<b class="aucf-message">Rozpoczynanie....</b>&nbsp;<b class="aucf-progress"></b>
			</p>
		</div>
	</div>
</div>

{if $is_bootstrap}
	{if isset($products)}

	{if $show_toolbar && !$bootstrap}
		{include file="page_header_toolbar.tpl" page_header_toolbar_btn=$toolbar_btn title=$toolbar_title}
		<div class="leadin">{block name="leadin"}{/block}</div>
	{/if}

	{literal}
	<style>
		div.item_form.image {
			float: left;
			border: 1px solid #ccc;
			margin: 0 4px 4px 0;
		}
		div.main_image {
			border-color: #fd620f !important;
		}
		label.t{
			font-weight:normal;
		}
	/*
		.product_category_fields {width: 200px;}
		.product_category_fields label {width: 150px;}
		.product_category_fields > div.margin-form {width: 500px; padding-left: 160px;}
	*/
	</style>
	{/literal}

	<form id="{$table}_form" action="" method="POST">

	<div class="row">
		<div class="panel col-lg-12" x-name="allegro_account">
			<div class="panel-heading">
			<img alt="" src="../modules/x13allegro/img/AdminXAllegroMain.png" /> Allegro
			</div>
			{include file='./helpers/form/fields_form.tpl' fields_form=$fields_form.allegro}
		</div>
	</div>
	
	<div class="row">
		<div class="panel col-lg-12" x-name="allegro_account">
			<div class="panel-heading">
				Produkty
			</div>
			<table class="table" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom:10px;">
			<colgroup>
				<col width="10px"/>
				<col width="280px"/>
				<col width="110px"/>
				<col width="110px"/>
				<col>
				<col width="130px"/>
				<col width="130px"/>
			</colgroup>
				<thead>
					<tr class="nodrag nodrop" style="height: 30px">
						<th class="center"></th>
						<th><span class="title_box">Tytuł aukcji</span></th>
						<th><span class="title_box">Ilość</span></th>
						<th><span class="title_box">Cena</span></th>
						<th><span class="title_box">Zdjęcia</span></th>
						<th><span class="title_box">Szablon</span></th>
						<th><span class="title_box">Dodatkowe opcje</span></th>
					</tr>
				</thead>
				<tbody>
				{foreach $products AS $index => $product}
					{$field_value = $fields_value.item[$index]}
					<tr class="{if $index is odd}alt_row odd{/if}">
						<td class="center" style="border-bottom: 1px dashed #DDD;vertical-align:middle;padding-top:8px;padding-bottom:8px;">
							<input type="checkbox" name="item[{$index}][enabled]" value="1" class="noborder" {if $field_value.enabled AND !$product.disabled}checked="checked"{/if} {if $product.disabled}disabled="disabled"{/if} x-name="product_switch" />
						</td>
						<td colspan="7" style="border-bottom: 1px dashed #DDD;vertical-align:middle;">
							<p style="margin:0;">{$product.name}{if $product.name_attribute} - {$product.name_attribute}{/if} - <strong>{$product.price_buy_now_default} zł</strong></p>
						</td>
					</tr>
					<tr class="{if $index is odd}alt_row odd{/if}" style="{if !$field_value.enabled OR $product.disabled}display: none;{/if}" x-name="product" x-id="{$index}">
						<td></td>
						<td style="vertical-align: top;padding-top:10px;">
							<input type="hidden" name="item[{$index}][id_product]" value="{$product.id}" x-name="id_product" />
							<input type="hidden" name="item[{$index}][id_product_attribute]" value="{$product.id_attribute}" x-name="id_product_attribute" />
							
							<input type="hidden" value="{$product.manufacturer_name}" x-name="manufacturer" />
							<input type="hidden" value="{if isset($product.name_attribute)}{$product.name_attribute}{/if}" x-name="attribute_name" />
							
							{*
							<input type="hidden" name="item[{$index}][description]" value="{$field_value.description|escape}" x-name="description" />
							<input type="hidden" name="item[{$index}][description_custom]" value="{$field_value.description_custom|escape}" x-name="description_custom" />
							*}
							
							<textarea style="display:none;" name="item[{$index}][description]" x-name="description">{$field_value.description}</textarea>
							<textarea style="display:none;" name="item[{$index}][description_custom]" x-name="description_custom">{$field_value.description_custom|escape}</textarea>
							
							<input type="text" name="item[{$index}][title]" value="{$field_value.title|escape}" size="45" x-name="title" />
							<p>Ilość znaków: <strong><span x-name="counter">{$field_value.title_size}</span>/50</strong></p>
							
							<p>
								<a class="btn btn-default button bt-icon" href="#" x-name="description_edit">
									<img alt="" src="../img/admin/edit.gif" />
									<span>Edytuj opis produktu</span>
								</a>
							</p>
							<p>
								<a class="btn btn-default button bt-icon" href="#" x-name="description_custom_edit">
									<img alt="" src="../img/admin/cms.gif" />
									<span>Edytuj dodatkowy opis</span>
								</a>
							</p>
							<p>
								<a class="btn btn-default button bt-icon" href="#" x-name="product_category_fields">
									<img alt="" src="../img/admin/asterisk.gif" />
									<span>Ustaw indywidualne cechy</span>
								</a>
								<input type="hidden" value="{if $field_value.category_fields_enabled}1{else}0{/if}" name="item[{$index}][category_fields_enabled]" x-name="product_category_fields_enabled" />
							</p>
							<div class="product_category_fields" x-name="product_category_fields" x-index="{$index}" x-enabled="{if $field_value.category_fields_enabled}1{else}0{/if}" {if !$field_value.category_fields_enabled}style="display: none;"{/if}>
								{include file='./helpers/form/category_fields.tpl' fields=$category_fields values=$category_fields_values index=$index}
							</div>
						</td>
						<td style="vertical-align: top;padding-top:10px;">
							<p>
								<input type="text" name="item[{$index}][quantity]" value="{$field_value.quantity}" size="3" x-cast="integer" x-name="quantity" {if $product.quantity_check}x-max="{$product.quantity_max}"{/if}/>
							</p>
							<p>
							<select name="item[{$index}][quantity_type]" x-name="quantity_type">
								<option value="0">szt</option>
								<option value="1">kpl</option>
								<option value="2">par</option>
							</select>
							Na stanie: <strong>{$product.quantity_stock}</strong>
							</p>
							
							<select name="item[{$index}][duration]" x-name="duration">
								{foreach $durations AS $duration}
									<option value="{$duration.value}" {if $field_value.duration == $duration.value}selected="selected"{/if}>{$duration.label}</option>
								{/foreach}
							</select>
							<p>Czas trwania</p>
						</td>
						<td style="vertical-align: top;padding-top:10px;">
							<input type="text" name="item[{$index}][price_buy_now]" value="{$field_value.price_buy_now}" size="11" x-cast="float" x-name="price_buy_now" x-value="{$product.price_buy_now_default}" />
							<p>Cena Kup teraz</p>
							<input type="text" name="item[{$index}][price_asking]" value="{$field_value.price_asking}" size="11" {*x13-type="float"*} x-name="price_asking" />
							<p>Cena wywoławcza</p>
							<input type="text" name="item[{$index}][price_minimal]" value="{$field_value.price_minimal}" size="11" {*x13-type="float"*} x-name="price_minimal" />
							<p>Cena minimalna</p>
							
						</td>
						<td style="vertical-align: top;padding-top:10px;">
							{foreach $product.images as $key => $item}
								{if $select_images == 'all'}
								<div class="item_form image {if $key==0}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" checked="checked" style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'cart_default')}" x-value="{$item.id_image}" />
								</div>
								{elseif $select_images == 'first'}
								<div class="item_form image {if $key==0}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" {if $key==0}checked="checked"{/if} style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'cart_default')}" x-value="{$item.id_image}" />
								</div>
								{else}
								<div class="item_form image {if $item.id_image == $field_value.image_main}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" {if in_array($item.id_image, $field_value.images)}checked="checked"{/if} style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'cart_default')}" x-value="{$item.id_image}" />
								</div>
								{/if}
							{/foreach}
							{if isset($product.images[0]['id_image']) && ($select_images == 'all' || $select_images == 'first')}
								<input type="hidden" name="item[{$index}][image_main]" value="{$product.images[0]['id_image']}" x-name="image_main" />
							{else}
								<input type="hidden" name="item[{$index}][image_main]" value="{$field_value.image_main}" x-name="image_main" />
							{/if}
						</td>
						<td style="vertical-align: top;padding-top:10px;">
							<p>
							<select name="item[{$index}][template]" x-name="template" style="width: 120px;">
								{foreach $templates AS $template}
									<option value="{$template->id}" {if $field_value.template == $template->id}selected="selected"{/if}>{$template->name}</option>
								{/foreach}
							</select>
							Szablon
							</p>
							{if $overlays}
								<p>
								<select name="item[{$index}][overlay]" x-name="overlay">
									<option value="">Brak</option>
									{foreach $overlays AS $overlay}
										<option value="{$overlay->id}" {if ((isset($field_value.overlay) AND $field_value.overlay == $overlay->id) OR (!isset($field_value.overlay) AND $overlay->default == 1))}selected="selected"{/if}>{$overlay->name}</option>
									{/foreach}
								</select>
								Ramka miniaturki
								</p>
							{/if}
							<p>
								<a class="btn btn-default button bt-icon" href="#" x-name="preview" x-id="{$index}">
									<img alt="" src="../img/admin/appearance.gif" />
									<span>Podgląd aukcji</span>
								</a>
							</p>
						</td>
						<td style="vertical-align: top;padding-top:10px;">
							<input type="checkbox" name="item[{$index}][additional][bold]" x-name="bold" value="1" {if in_array(1, $field_value.additional)}checked="checked"{/if}/> <label class="t">Pogrubienie</label><br />
							<input type="checkbox" name="item[{$index}][additional][thumbnail]" x-name="thumbnail" value="2" {if in_array(2, $field_value.additional) || $select_images == 'all' || $select_images == 'first'}checked="checked"{/if}/> <label class="t">Miniaturka</label><br />
							<input type="checkbox" name="item[{$index}][additional][backlight]" x-name="backlight" value="4" {if in_array(4, $field_value.additional)}checked="checked"{/if}/> <label class="t">Podświetlenie</label><br />
							<input type="checkbox" name="item[{$index}][additional][distinction]" x-name="distinction" value="8" {if in_array(8, $field_value.additional)}checked="checked"{/if}/> <label class="t">Wyróżnienie</label><br />
							<input type="checkbox" name="item[{$index}][additional][category_main]" x-name="category_main" value="16" {if in_array(16, $field_value.additional)}checked="checked"{/if}/> <label class="t">Strona kategorii</label><br />
							<input type="checkbox" name="item[{$index}][additional][watermark]" x-name="watermark" value="64" {if in_array(64, $field_value.additional)}checked="checked"{/if}/> <label class="t">Znak wodny</label><br />
						</td>
					</tr>
				{/foreach}
				<tr style="height: 40px;">
					<td colspan="7" style="padding-left: 20px;">
						<strong style="font-size: 14px;"><img alt="" src="../img/admin/duplicate.png" style="margin-right:10px;"/> Masowe ustawienia dla wszystkich aukcji</strong>
					</td>
				</tr>
			</tbody>
			<thead>
				<tr class="nodrag nodrop" style="height: 30px">
					<th class="center"></th>
					<th><span class="title_box">Tytuł aukcji</span></th>
					<th><span class="title_box">Ilość</span></th>
					<th><span class="title_box">Cena</span></th>
					<th><span class="title_box">Zdjęcia</span></th>
					<th><span class="title_box">Szablon</span></th>
					<th><span class="title_box">Dodatkowe opcje</span></th>
				</tr>
			</thead>
			<tbody>
				<tr style="background-color: #ececec;">
					<td>&nbsp;</td>
					<td style="border-bottom: none; vertical-align: top">
						<p>
							<a class="button bt-icon" href="#" x-name="bulk_attribute">
								<img alt="" src="../img/admin/add.gif" />
								<span>Dodaj nazwę atrybutu do tytułu aukcji</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_manufacturer">
								<img alt="" src="../img/admin/add.gif" />
								<span>Dodaj nazwę producenta do tytułu aukcji</span>
							</a>
						</p>
					</td>
					<td style="border-bottom: none; vertical-align: top">
						<input type="text" value="" size="3" x-name="bulk_quantity" x-cast="integer" />
						<select x-name="bulk_quantity_type">
							<option value="0">szt</option>
							<option value="1">kpl</option>
							<option value="2">par</option>
						</select>
						<p>Ilość:</p>
				 
						<select x-name="bulk_duration">
							{foreach $durations AS $duration}
								<option value="{$duration.value}" {if $field_value.duration == $duration.value}selected="selected"{/if}>{$duration.label}</option>
							{/foreach}
						</select>
						<p>Czas trwania:</p>
					</td>
					<td style="border-bottom: none; vertical-align: top">
						<input type="text" value="" size="11" x-name="bulk_price_buy_now" x-cast="integer" />
						<a href="#" title="Dodaj marżę" x-name="bulk_price_buy_now" x-action="up"><img src="../img/admin/add.gif" alt=""/></a>
						<a href="#" title="Usuń marżę" x-name="bulk_price_buy_now" x-action="down"><img src="../img/admin/forbbiden.gif" alt=""/></a>
						<p>Cena Kup Teraz (narzut):</p>

						<input type="text" value="" size="11" x-name="bulk_price_asking" />
						<p>Cena wywoławcza:</p>
		   
						<input type="text" value="" size="11" x-name="bulk_price_minimal" />
						<p>Cena minimalna:</p>
					</td>
					<td style="vertical-align: top; border-bottom: none;">
						<p>                            
							<a class="button bt-icon" href="#" x-name="bulk_images_all">
								<img alt="" src="../img/admin/multishop_config.png" />
								<span>Zaznacz wszystkie zdjęcia</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_images_invert">
								<img alt="" src="../img/admin/transfer_stock.png" height="16px" />
								<span>Odwróć zaznaczenie zdjęć</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_images_first">
								<img alt="" src="../img/admin/photo.gif" />
								<span>Zaznacz pierwsze zdjęcie</span>
							</a>
						</p>
					</td>
					<td style="vertical-align: top; border-bottom: none;">
						<select x-name="bulk_template" style="width: 120px;">
							{foreach $templates AS $template}
							<option value="{$template->id}">{$template->name}</option>
							{/foreach}
						</select>
						{if $overlays}
							<p>Szablon</p>
							<select x-name="bulk_overlay" style="width: 120px;">
								<option value="">Brak</option>
								{foreach $overlays AS $overlay}
								<option value="{$overlay->id}" {if ((isset($field_value.overlay) AND $field_value.overlay == $overlay->id) OR (!isset($field_value.overlay) AND $overlay->default == 1))}selected="selected"{/if}>{$overlay->name}</option>
								{/foreach}
							</select>
							<p>Ramka miniaturki</p>
						{/if}
					</td>
					<td style="vertical-align: top; border-bottom: none;" x-name="additional">
						<input type="checkbox" x-name="bold" value="1" /> <label class="t">Pogrubienie</label><br />
						<input type="checkbox" x-name="thumbnail" value="2" /> <label class="t">Miniaturka</label><br />
						<input type="checkbox" x-name="backlight" value="4" /> <label class="t">Podświetlenie</label><br />
						<input type="checkbox" x-name="distinction" value="8" /> <label class="t">Wyróżnienie</label><br />
						<input type="checkbox" x-name="category_main" value="16" /> <label class="t">Strona kategorii</label><br />
						<input type="checkbox" x-name="watermark" value="64" /> <label class="t">Znak wodny</label><br />
					</td>
				</tr>
			</tbody>
		</table>
		</div>

	</div><!--/.row-->

	<div class="row">

		<div class="col-lg-6" style="padding-left:0;">
			<div class="panel" x-name="categories">
				<div class="panel-heading">
					<img alt="" src="../img/admin/tab-categories.gif" />  Kategoria aukcji
				</div>
				{include file='./helpers/form/category.tpl' category_path=$category_path categories=$categories}
			</div>
		</div>
		
		<div class="col-lg-6" style="padding-right:0;{if !$category_fields} display: none;{/if}">
			<div class="panel" x-name="category_fields">
				<div class="panel-heading">
					<button id="copyFieldsToItems" class="btn btn-primary pull-right" style="font-size:0.78em;margin-top:3px;text-transform:none;font-weight:normal;">
						<i class="icon-copy"></i>  skopiuj do cech indywidualnych
					</button>
					<img alt="" src="../img/admin/asterisk.gif" /> Cechy kategorii 
				</div>
				{include file='./helpers/form/category_fields.tpl' fields=$category_fields values=$category_fields_values index=''}
			</div>
		</div>

	</div><!--/.row-->

	<div class="row">

		<div class="panel col-lg-12">
			<div class="panel-heading">
			  <img alt="" src="../img/admin/___info-ca.gif" /> Płatność i dostawa
			</div>
			
			<div class="row">
			
				<div class="col-md-12 col-lg-4 adminxallegropas" x-name="pas">
					{include file='./fields_form.tpl' fields_form=$fields_form.pas}
				</div>
		
				<div class="col-md-12 col-lg-8" x-name="shipments">
					{include file='./helpers/form/shipments.tpl'}
				</div>
				
			</div><!--/.row-->
			
		</div>
		
	</div><!--/.row-->

	<div class="row">

		<div class="panel col-lg-12" style="padding-bottom:0;">
		
			<div class="panel-heading">
			  <img alt="" src="../img/admin/time.gif" /> Czas rozpoczęcia
			</div>
			
			<div class="margin-form">
				<input type="radio" name="start" id="start_0" value="0" {if !$fields_value.start}checked="checked"{/if} />
				<label class="t" for="start_0">Wystaw teraz</label>
				<br />
				<input type="radio" name="start" id="start_1" value="1" {if $fields_value.start}checked="checked"{/if} />
				<label class="t" for="start_1">Rozpocznij w innym terminie</label>
				<p style="padding-left: 20px;"><input type="text" name="start_time" class="datepicker" value="{$fields_value.start_time}" {if !$fields_value.start}disabled="disabled"{/if} /></p>
			</div>
			
			<input type="hidden" value="x13allegro" name="x13allegro">
		</div>
		
		<div class="panel col-lg-12" style="display:none;">
		
			<div class="panel-heading">
			  <img alt="" src="../img/admin/metatags.gif" /> Automatyczne wznawianie aukcji
			</div>
			
			<div class="margin-form">
				<input type="radio" name="resume_auction" id="resume_auction_0" value="0" checked="checked" />
				<label class="t" for="resume_auction_0">Nie wznawiaj</label>
				<br />
				<input type="radio" name="resume_auction" id="resume_auction_1" value="1" />
				<label class="t" for="resume_auction_1">Wznów z pełnym zestawem przedmiotów</label>
				<br />
				<input type="radio" name="resume_auction" id="resume_auction_2" value="2" />
				<label class="t" for="resume_auction_2">Wznów tylko z przedmiotami niesprzedanymi</label>
			</div>
			
		</div>
		
	</div><!--/.row-->
		
		
	</form>

	<script type="text/javascript">

		var current_VERSION_CATEGORY_LIST = '{$current_VERSION_CATEGORY_LIST}';
		var current_VERSION_SELL_FORM_FIELDS = '{$current_VERSION_SELL_FORM_FIELDS}';
		var newest_VERSION_CATEGORY_LIST = '{$newest_VERSION_CATEGORY_LIST}';
		var newest_VERSION_SELL_FORM_FIELDS = '{$newest_VERSION_SELL_FORM_FIELDS}';

		$(document).ready(function() {
			if ($(".datepicker").length > 0) {
				$(".datepicker").datetimepicker();
			}
		});

		var iso = '{$iso}';
		var pathCSS = '{$smarty.const._THEME_CSS_DIR_}';
		var ad = '{$ad}';

		var XAllegro = new $.X13Allegro({});
		XAllegro.auctionForm();

	</script>
	{/if}
{else}
	{if isset($products)}
	{literal}
	<style>
		div.item_form.image {
			float: left;
			border: 1px solid #ccc;
			margin: 5px;
		}
		div.main_image {
			border-color: #fd620f !important;
		}
		
		.product_category_fields {width: 200px;}
		.product_category_fields label {width: 150px;}
		.product_category_fields > div.margin-form {width: 500px; padding-left: 160px;}
		
	</style>
	{/literal}

	{if $show_toolbar}
		{include file="toolbar.tpl" toolbar_btn=$toolbar_btn toolbar_scroll=$toolbar_scroll title=$toolbar_title}
		<div class="leadin">{block name="leadin"}{/block}</div>
	{/if}

	<form id="{$table}_form" action="" method="POST">
		<fieldset x-name="allegro_account">
			<legend>
			  <img alt="" src="../modules/x13allegro/logo.gif" />  Allegro
			</legend>
			{include file='./helpers/form/fields_form.tpl' fields_form=$fields_form.allegro}
		</fieldset>
		
		<br />
		<table class="table" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom:10px;">
			<col width="10px"/>
			<col width="270px"/>
			<col width="100px"/>
			<col width="130px"/>
	{*        <col width="100px"/>*}
			<col width="420px"/>
			<col width="150px"/>
			<col width="150px"/>
			<thead>
				<tr class="nodrag nodrop" style="height: 30px">
					<th class="center"></th>
					<th><span class="title_box">Tytuł aukcji</span></th>
					<th><span class="title_box">Ilość</span></th>
					<th><span class="title_box">Cena</span></th>
	{*                <th><span class="title_box">Czas trwania</span></th>*}
					<th><span class="title_box">Zdjęcia</span></th>
					<th><span class="title_box">Szablon</span></th>
					<th><span class="title_box">Dodatkowe opcje</span></th>
				</tr>
			</thead>
			<tbody>
				{foreach $products AS $index => $product}
					{$field_value = $fields_value.item[$index]}
		
					<tr class="{if $index is odd}alt_row{/if}">
						<td class="center" style="border-bottom: none;">
							<input type="checkbox" name="item[{$index}][enabled]" value="1" class="noborder" {if $field_value.enabled AND !$product.disabled}checked="checked"{/if} {if $product.disabled}disabled="disabled"{/if} x-name="product_switch" />
						</td>
						<td colspan="7" style="border-bottom: none">
							<p>{$product.name}{if $product.name_attribute} - {$product.name_attribute}{/if} - <strong>{$product.price_buy_now_default} zł</strong></p>
						</td>
					</tr>
					<tr 
					class="{if $index is odd}alt_row{/if}"
					style="{if !$field_value.enabled OR $product.disabled}display: none;{/if}"
					x-name="product"
					x-id="{$index}"
					>
						<td></td>
						<td style="vertical-align: top;">
							<input type="hidden" name="item[{$index}][id_product]" value="{$product.id}" x-name="id_product" />
							<input type="hidden" name="item[{$index}][id_product_attribute]" value="{$product.id_attribute}" x-name="id_product_attribute" />
							
							<input type="hidden" value="{$product.manufacturer_name}" x-name="manufacturer" />
							<input type="hidden" value="{if isset($product.name_attribute)}{$product.name_attribute}{/if}" x-name="attribute_name" />
							
							{*
							<input type="hidden" name="item[{$index}][description]" value="{$field_value.description|escape}" x-name="description" />
							<input type="hidden" name="item[{$index}][description_custom]" value="{$field_value.description_custom|escape}" x-name="description_custom" />
							*}
							
							<textarea style="display:none;" name="item[{$index}][description]" x-name="description">{$field_value.description}</textarea>
							<textarea style="display:none;" name="item[{$index}][description_custom]" x-name="description_custom">{$field_value.description_custom|escape}</textarea>
							

							<input type="text" name="item[{$index}][title]" value="{$field_value.title|escape}" size="45" x-name="title" />
							<p>Ilość znaków: <strong><span x-name="counter">{$field_value.title_size}</span>/50</strong></p>
							
							<p>
								<a class="button bt-icon" href="#" x-name="description_edit">
									<img alt="" src="../img/admin/edit.gif" />
									<span>Edytuj opis produktu</span>
								</a>
								<br />
								<a class="button bt-icon" href="#" x-name="description_custom_edit">
									<img alt="" src="../img/admin/cms.gif" />
									<span>Edytuj dodatkowy opis produktu</span>
								</a>
								<br />
								<a class="button bt-icon" href="#" x-name="product_category_fields">
									<img alt="" src="../img/admin/asterisk.gif" />
									<span>Ustaw indywidualne cechy kategorii</span>
								</a>
								<input type="hidden" value="{if $field_value.category_fields_enabled}1{else}0{/if}" name="item[{$index}][category_fields_enabled]" x-name="product_category_fields_enabled" />
								<div class="product_category_fields" x-name="product_category_fields" x-index="{$index}" x-enabled="{if $field_value.category_fields_enabled}1{else}0{/if}" {if !$field_value.category_fields_enabled}style="display: none;"{/if}>
									{include file='./helpers/form/category_fields.tpl' fields=$category_fields values=$category_fields_values index=$index}
								</div>
							</p>
						</td>
						<td style="vertical-align: top;">
							<input type="text" name="item[{$index}][quantity]" value="{$field_value.quantity}" size="3" x-cast="integer" x-name="quantity" {if $product.quantity_check}x-max="{$product.quantity_max}"{/if}/>
							<select name="item[{$index}][quantity_type]" x-name="quantity_type">
								<option value="0">szt</option>
								<option value="1">kpl</option>
								<option value="2">par</option>
							</select>
							<p>Na stanie: <strong>{$field_value.quantity_stock}</strong></p>
							<select name="item[{$index}][duration]" x-name="duration">
								{foreach $durations AS $duration}
									<option value="{$duration.value}" {if $field_value.duration == $duration.value}selected="selected"{/if}>{$duration.label}</option>
								{/foreach}
							</select>
							<p>Czas trwania</p>
						</td>
						<td style="vertical-align: top;">
							<input type="text" name="item[{$index}][price_buy_now]" value="{$field_value.price_buy_now}" size="11" x-cast="float" x-name="price_buy_now" x-value="{$product.price_buy_now_default}" />
							<p>Cena Kup teraz</p>
							<input type="text" name="item[{$index}][price_asking]" value="{$field_value.price_asking}" size="11" {*x13-type="float"*} x-name="price_asking" />
							<p>Cena wywoławcza</p>
							<input type="text" name="item[{$index}][price_minimal]" value="{$field_value.price_minimal}" size="11" {*x13-type="float"*} x-name="price_minimal" />
							<p>Cena minimalna</p>
							
						</td>
						<td style="vertical-align: top;">
							{foreach $product.images as $key => $item}
								{if $select_images == 'all'}
								<div class="item_form image {if $key==0}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" checked="checked" style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'medium_default')}" x-value="{$item.id_image}" />
								</div>
								{elseif $select_images == 'first'}
								<div class="item_form image {if $key==0}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" {if $key==0}checked="checked"{/if} style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'medium_default')}" x-value="{$item.id_image}" />
								</div>
								{else}
								<div class="item_form image {if $item.id_image == $field_value.image_main}main_image{/if}" x-name="images">
									<input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" {if in_array($item.id_image, $field_value.images)}checked="checked"{/if} style="position: absolute; margin: 5px 0px 0px 5px;" x-name="images" />
									<img src="{$link->getImageLink($product.link_rewrite, $item.id_image, 'medium_default')}" x-value="{$item.id_image}" />
								</div>
								{/if}
							{/foreach}
							{if isset($product.images[0]['id_image']) && ($select_images == 'all' || $select_images == 'first')}
								<input type="hidden" name="item[{$index}][image_main]" value="{$product.images[0]['id_image']}" x-name="image_main" />
							{else}
								<input type="hidden" name="item[{$index}][image_main]" value="{$field_value.image_main}" x-name="image_main" />
							{/if}
						</td>
						<td style="vertical-align: top;">
							<select name="item[{$index}][template]" x-name="template" style="width: 120px;">
								{foreach $templates AS $template}
									<option value="{$template->id}" {if $field_value.template == $template->id}selected="selected"{/if}>{$template->name}</option>
								{/foreach}
							</select>
							{if $overlays}
								<p>Szablon</p>
								<select name="item[{$index}][overlay]" x-name="overlay">
									<option value="">Brak</option>
									{foreach $overlays AS $overlay}
										<option value="{$overlay->id}" {if ((isset($field_value.overlay) AND $field_value.overlay == $overlay->id) OR (!isset($field_value.overlay) AND $overlay->default == 1))}selected="selected"{/if}>{$overlay->name}</option>
									{/foreach}
								</select>
								<p>Ramka miniaturki</p>
							{/if}
							<p>
								<a class="button bt-icon" href="#" x-name="preview" x-id="{$index}">
									<img alt="" src="../img/admin/appearance.gif" />
									<span>Podgląd aukcji</span>
								</a>
							</p>
						</td>
						<td style="vertical-align: top;">
							<input type="checkbox" name="item[{$index}][additional][bold]" x-name="bold" value="1" {if in_array(1, $field_value.additional)}checked="checked"{/if}/><label class="t">Pogrubienie</label><br />
							<input type="checkbox" name="item[{$index}][additional][thumbnail]" x-name="thumbnail" value="2" {if in_array(2, $field_value.additional) || $select_images == 'all' || $select_images == 'first'}checked="checked"{/if}/><label class="t">Miniaturka</label><br />
							<input type="checkbox" name="item[{$index}][additional][backlight]" x-name="backlight" value="4" {if in_array(4, $field_value.additional)}checked="checked"{/if}/><label class="t">Podświetlenie</label><br />
							<input type="checkbox" name="item[{$index}][additional][distinction]" x-name="distinction" value="8" {if in_array(8, $field_value.additional)}checked="checked"{/if}/><label class="t">Wyróżnienie</label><br />
							<input type="checkbox" name="item[{$index}][additional][category_main]" x-name="category_main" value="16" {if in_array(16, $field_value.additional)}checked="checked"{/if}/><label class="t">Strona kategorii</label><br />
							<input type="checkbox" name="item[{$index}][additional][watermark]" x-name="watermark" value="64" {if in_array(64, $field_value.additional)}checked="checked"{/if}/><label class="t">Znak wodny</label><br />
						</td>
					</tr>
				{/foreach}
				<tr style="height: 40px;">
					<td colspan="7" style="padding-left: 20px;">
						<strong style="font-size: 14px;"><img alt="" src="../img/admin/duplicate.png" style="margin-right:10px;"/> Masowe ustawienia dla wszystkich aukcji</strong>
					</td>
				</tr>
			</tbody>
			<thead>
				<tr class="nodrag nodrop" style="height: 30px">
					<th class="center"></th>
					<th><span class="title_box">Tytuł aukcji</span></th>
					<th><span class="title_box">Ilość</span></th>
					<th><span class="title_box">Cena</span></th>
	{*                <th><span class="title_box">Czas trwania</span></th>*}
					<th><span class="title_box">Zdjęcia</span></th>
					<th><span class="title_box">Szablon</span></th>
					<th><span class="title_box">Dodatkowe opcje</span></th>
				</tr>
			</thead>
			<tbody>
				<tr style="background-color: #ececec;">
					<td>&nbsp;</td>
					<td style="border-bottom: none; vertical-align: top">
						<p>
							<a class="button bt-icon" href="#" x-name="bulk_attribute">
								<img alt="" src="../img/admin/add.gif" />
								<span>Dodaj nazwę atrybutu do tytułu aukcji</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_manufacturer">
								<img alt="" src="../img/admin/add.gif" />
								<span>Dodaj nazwę producenta do tytułu aukcji</span>
							</a>
						</p>
					</td>
					<td style="border-bottom: none; vertical-align: top">
			
								<input type="text" value="1" size="3" x-name="bulk_quantity" x-cast="integer" />
								<select x-name="bulk_quantity_type">
									<option value="0">szt</option>
									<option value="1">kpl</option>
									<option value="2">par</option>
								</select>
								<p>Ilość:</p>
						 
								<select x-name="bulk_duration">
									{foreach $durations AS $duration}
										<option value="{$duration.value}">{$duration.label}</option>
									{/foreach}
								</select>
								<p>Czas trwania:</p>
						   
					
					</td>
					<td style="border-bottom: none; vertical-align: top">

								<input type="text" value="" size="11" x-name="bulk_price_buy_now" x-cast="integer" />
								<a href="#" title="Dodaj marżę" x-name="bulk_price_buy_now" x-action="up"><img src="../img/admin/add.gif" alt=""/></a>
								<a href="#" title="Usuń marżę" x-name="bulk_price_buy_now" x-action="down"><img src="../img/admin/forbbiden.gif" alt=""/></a>
								<p>Cena Kup Teraz (narzut):</p>

								<input type="text" value="" size="11" x-name="bulk_price_asking" />
								<p>Cena wywoławcza:</p>
				   
								<input type="text" value="" size="11" x-name="bulk_price_minimal" />
								<p>Cena minimalna:</p>
						 
					</td>
					<td style="vertical-align: top; border-bottom: none;">
						<p>                            
							<a class="button bt-icon" href="#" x-name="bulk_images_all">
								<img alt="" src="../img/admin/multishop_config.png" />
								<span>Zaznacz wszystkie zdjęcia</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_images_invert">
								<img alt="" src="../img/admin/transfer_stock.png" height="16px" />
								<span>Odwróć zaznaczenie zdjęć</span>
							</a>
							<br />
							<a class="button bt-icon" href="#" x-name="bulk_images_first">
								<img alt="" src="../img/admin/photo.gif" />
								<span>Zaznacz pierwsze zdjęcie</span>
							</a>
						</p>
					</td>
					<td style="vertical-align: top; border-bottom: none;">
						<select x-name="bulk_template" style="width: 120px;">
							{foreach $templates AS $template}
								<option value="{$template->id}">{$template->name}</option>
							{/foreach}
						</select>
						{if $overlays}
							<p>Szablon</p>
							<select x-name="bulk_overlay" style="width: 120px;">
								<option value="">Brak</option>
								{foreach $overlays AS $overlay}
									<option value="{$overlay->id}" {if ((isset($field_value.overlay) AND $field_value.overlay == $overlay->id) OR (!isset($field_value.overlay) AND $overlay->default == 1))}selected="selected"{/if}>{$overlay->name}</option>
								{/foreach}
							</select>
							<p>Ramka miniaturki</p>
						{/if}
					</td>
					<td style="vertical-align: top; border-bottom: none;" x-name="additional">
						<input type="checkbox" x-name="bold" value="1" /><label class="t">Pogrubienie</label><br />
						<input type="checkbox" x-name="thumbnail" value="2" /><label class="t">Miniaturka</label><br />
						<input type="checkbox" x-name="backlight" value="4" /><label class="t">Podświetlenie</label><br />
						<input type="checkbox" x-name="distinction" value="8" /><label class="t">Wyróżnienie</label><br />
						<input type="checkbox" x-name="category_main" value="16" /><label class="t">Strona kategorii</label><br />
						<input type="checkbox" x-name="watermark" value="64" /><label class="t">Znak wodny</label><br />
					</td>
				</tr>
			</tbody>
		</table>
			
		<br />
		
		<div style="width:49%; float:left;">
		
		<fieldset x-name="categories">
			<legend>
			  <img alt="" src="../img/admin/tab-categories.gif" />  Kategoria aukcji
			</legend>
			{include file='./helpers/form/category.tpl' category_path=$category_path categories=$categories}
		</fieldset>
		
		</div>
		<div style="width:49%; float:left;margin-left:2%;">
		
		<fieldset x-name="category_fields" {if !$category_fields}style="display: none;"{/if}>
			<legend>
			   <img alt="" src="../img/admin/asterisk.gif" /> Cechy kategorii
			</legend>
			{include file='./helpers/form/category_fields.tpl' fields=$category_fields values=$category_fields_values index=''}
		</fieldset>
		
		</div>
		
		<br style="clear:both" />
		<br style="clear:both" />
		<br style="clear:both" />
		
		<fieldset>
			<legend>
			  <img alt="" src="../img/admin/___info-ca.gif" /> Płatność i dostawa
			</legend>
			<div style="width: 480px; float: left;" x-name="pas">
				{include file='./fields_form.tpl' fields_form=$fields_form.pas}
			</div>
			<div style="width: 700px; float: right;" x-name="shipments">
				{include file='./helpers/form/shipments.tpl'}
			</div> 
		</fieldset>
			
		<br />
		
		<fieldset>
			<legend>
			  <img alt="" src="../img/admin/___info-ca.gif" /> Czas rozpoczęcia
			</legend>
			
			<label>Czas rozpoczęcia</label>
			<div class="margin-form">
				<input type="radio" name="start" id="start_0" value="0" {if !$fields_value.start}checked="checked"{/if} />
				<label class="t" for="start_0">Wystaw teraz</label>
				<br />
				<input type="radio" name="start" id="start_1" value="1" {if $fields_value.start}checked="checked"{/if} />
				<label class="t" for="start_1">Rozpocznij w innym terminie</label>
				<p style="padding-left: 20px;"><input type="text" name="start_time" class="datepicker" value="{$fields_value.start_time}" {if !$fields_value.start}disabled="disabled"{/if} /></p>
			</div>
		</fieldset>
		
		
		<div class="panel col-lg-12" style="display:none;">
		
			<div class="panel-heading">
			  <img alt="" src="../img/admin/metatags.gif" /> Automatyczne wznawianie aukcji
			</div>
			
			<div class="margin-form">
				<input type="radio" name="resume_auction" id="resume_auction_0" value="0" checked="checked" />
				<label class="t" for="resume_auction_0">Nie wznawiaj</label>
				<br />
				<input type="radio" name="resume_auction" id="resume_auction_1" value="1" />
				<label class="t" for="resume_auction_1">Wznów z pełnym zestawem przedmiotów</label>
				<br />
				<input type="radio" name="resume_auction" id="resume_auction_2" value="2" />
				<label class="t" for="resume_auction_2">Wznów tylko z przedmiotami niesprzedanymi</label>
			</div>
			
		</div>
		
		<input type="submit" id="{$table}_form_submit_btn" value="x13allegro" name="x13allegro" style="display:none;" />
	</form>

		<script type="text/javascript">

			var current_VERSION_CATEGORY_LIST = '{$current_VERSION_CATEGORY_LIST}';
			var current_VERSION_SELL_FORM_FIELDS = '{$current_VERSION_SELL_FORM_FIELDS}';
			var newest_VERSION_CATEGORY_LIST = '{$newest_VERSION_CATEGORY_LIST}';
			var newest_VERSION_SELL_FORM_FIELDS = '{$newest_VERSION_SELL_FORM_FIELDS}';

			$(document).ready(function() {
				if ($(".datepicker").length > 0) {
					$(".datepicker").datetimepicker();
				}

			});

		var iso = '{$iso}';
		var pathCSS = '{$smarty.const._THEME_CSS_DIR_}';
		var ad = '{$ad}';
		
		var XAllegro = new $.X13Allegro({});
		XAllegro.auctionForm();

</script>
{/if}
{/if}
