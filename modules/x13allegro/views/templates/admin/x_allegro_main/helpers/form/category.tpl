{$parent_id = 0}

{foreach $category_path as $id}
    <select name="id_allegro_category[]" style="width: 200px; padding: 5px; margin-right: 20px; display:block; margin-bottom:10px;">
            <option value="0">-- Wybierz --</option>
            {foreach $categories as $category}
                {if $parent_id == $category.parent_id}
                    <option value="{$category.id}" {if $category.id == $id}selected="selected"{/if}>{$category.name}</option>
                {/if}
            {/foreach}
    </select>

    {$parent_id = $id}
{/foreach}
