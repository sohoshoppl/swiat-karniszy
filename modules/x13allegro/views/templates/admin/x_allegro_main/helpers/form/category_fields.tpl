{foreach $fields as $input}
    {if isset($index) && $index !== ''}
        {$field_name = "item[{$index}][category_fields][{$input.id}]"}
        {$field_value = $values.items[$index][$input.id]}
    {else}
        {$field_name = "category_fields[{$input.id}]"}
        {$field_value = $values[$input.id]}
    {/if}
    

    {if isset($input.label)}
		<label {if isset($input.required) && $input.required && $input.type != 'radio'} class="required"{/if}><strong>{$input.label} {if isset($input.required) && $input.required}<sup>*</sup>{/if}</strong></label>
    {/if}
    <div class="margin-form" style="margin-bottom:6px;">
        {if $input.type == 'text'}
			<div class="input-group">
			{if isset($input.suffix) and $input.suffix}<span class="input-group-addon">{$input.suffix}</span>{/if}
            <input type="text"
            name="{$field_name}"
            {if isset($input.size)}size="{$input.size}"{/if}
            {if isset($input.maxlength)}maxlength="{$input.maxlength}"{/if}
            {if isset($input.readonly) && $input.readonly}readonly="readonly"{/if}
            {if isset($input.disabled) && $input.disabled}disabled="disabled"{/if}
            value="{$field_value}"
            />
            </div>
            {if isset($input.unit) and $input.unit}[<small>{$input.unit}</small>]{/if}
        {elseif $input.type == 'select'}
            <select name="{$field_name}" class="{if isset($input.class)}{$input.class}{/if}"
            {if isset($input.multiple)}multiple="multiple" {/if}
            {if isset($input.size)}size="{$input.size}"{/if}>
                {foreach $input.options.query as $option}
                    {if is_object($option)}
                        <option 
                            value="{$option->$input.options.id}"
                            {if $field_value == $option->$input.options.id}selected="selected"{/if}
                            >{$option->$input.options.name}</option>
                    {else}
                        <option 
                            value="{$option[$input.options.id]}"
                            {if $field_value == $option[$input.options.id]}selected="selected"{/if}
                            >{$option[$input.options.name]}</option>
                    {/if}
                {/foreach}
            </select>
        {elseif $input.type == 'radio'}
            {foreach $input.values as $value}
                <input 
                    type="radio" 
                    name="{$field_name}" 
                    value="{$value.value}" 
                    {if $field_value == $value.value}checked="checked"{/if} 
                    {if isset($input.disabled) && $input.disabled}disabled="disabled"{/if} />
                <label {if isset($input.class)}class="{$input.class}"{/if}>
                 {if isset($input.is_bool) && $input.is_bool == true}
                    {if $value.value == 1}
                        <img src="../img/admin/enabled.gif" alt="{$value.label}" title="{$value.label}" />
                    {else}
                        <img src="../img/admin/disabled.gif" alt="{$value.label}" title="{$value.label}" />
                    {/if}
                 {else}
                    {$value.label}
                 {/if}
                </label>
                {if isset($input.br) && $input.br}<br />{/if}
                {if isset($value.p) && $value.p}<p>{$value.p}</p>{/if}
            {/foreach}
        {elseif $input.type == 'textarea'}
            <textarea name="{$input.name}{if isset($input.key)}[{$input.key}]{/if}" id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}" cols="{$input.cols}" rows="{$input.rows}">{$field_value}</textarea>
        {elseif $input.type == 'checkbox'}
            {foreach $input.options.query as $value}
                <input type="checkbox"
                    name="{$field_name}[]"
                    class="{if isset($input.class)}{$input.class}{/if}"
                    {if $field_value and in_array($value[$input.options.id], $field_value)}checked="checked"{/if} 
                    value="{$value[$input.options.id]}" />
                <label class="t">{$value[$input.options.name]}</label><br />
            {/foreach}
        {/if}

        {if $input.type == 'text' and isset($input.value_type)}
            <p class="preference_description">
                {if $input.value_type == 'integer'}
                    Tylko liczby całkowite.
                {elseif $input.value_type == 'float'}
                    Dowolona liczba.
                {/if}
            </p>
        {/if}
    </div>
    <div class="clear"></div>
{/foreach}
