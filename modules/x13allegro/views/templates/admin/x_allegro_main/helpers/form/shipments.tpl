{block name="other_input"}
<div>
<table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
	<thead>
		<tr>
			<th>Darmowe opcje dostawy</th>
		</tr>
	</thead>
	<tbody>
		{foreach $shipments as $shipment}
			{if $shipment.type == 0}
				{$shipment_value = $fields_value.shipment[$shipment.id]}
				{$enable = ($shipment.value == 1 && in_array($shipment_value, array(1,3,5,7))) || ($shipment.value == 2 && in_array($shipment_value, array(2,3,6,7))) || ($shipment.value == 4 && in_array($shipment_value, array(4,5,6,8)))}
				<tr class="{cycle values=",alt_row"}">
					<td style="padding-top:6px;padding-bottom:6px">
						<input{if $enable} checked="checked"{/if} type="checkbox" name="shipment[{$shipment.id}][]" value="{$shipment.value}" /> {$shipment.name}
					</td>
				</tr>
			{/if}
		{/foreach}
	</tbody>
</table>
<br />
<table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
    <thead>
        <tr>
            <th>Płatność z góry</th>
            <th width="110">Pierwsza sztuka</th>
            <th width="110">Kolejna sztuka</th>
            <th width="100">Ilość w paczce</th>
        </tr>
    </thead>
    <tbody>
        {foreach $shipments as $shipment}
            {if $shipment.type == 1}
                <tr class="{cycle values=",alt_row"}" x-id="{$shipment.id}">
                    <td>
                        <input type="checkbox" name="shipment[{$shipment.id}][enabled]" value="1" {if $fields_value.shipment[$shipment.id]['enabled']}checked="checked"{/if} x-name="switch" /> 
                        {$shipment.name}
                    </td>
                    <td class="center">
                    	<div class="input-group col-lg-12">
						<input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][0]" value="{$fields_value.shipment[$shipment.id].0|number_format:2:".":","}" x-cast="float" />
						<span class="input-group-addon"> zł</span>
                    </div>
                    </td>
                    
                    <td class="center">
                    <div class="input-group col-lg-12">
						<input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][1]" value="{$fields_value.shipment[$shipment.id].1|number_format:2:".":","}" x-cast="float" />
						<span class="input-group-addon"> zł</span>
                    </div>
                    </td>
                    <td class="center"><input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][2]" value="{$fields_value.shipment[$shipment.id].2|default:"1"}" x-cast="integer" /></td>
                </tr>
            {/if}
        {/foreach}
    </tbody>
</table>
<br />
<table class="table" cellspacing="0" cellpadding="0" style="width: 100%">
    <thead>
        <tr>
            <th>Płatność przy odbiorze</th>
            <th width="110">Pierwsza sztuka</th>
            <th width="110">Kolejna sztuka</th>
            <th width="100">Ilość w paczce</th>
        </tr>
    </thead>
    <tbody>
        {foreach $shipments as $shipment}
            {if $shipment.type == 2}
                <tr class="{cycle values=",alt_row"}" x-id="{$shipment.id}">
                    <td>
                        <input type="checkbox" name="shipment[{$shipment.id}][enabled]" value="1" {if $fields_value.shipment[$shipment.id]['enabled']}checked="checked"{/if} x-name="switch" /> 
                        {$shipment.name}
                    </td>
                    <td class="center">
                    <div class="input-group col-lg-12">
                    	<input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][0]" value="{$fields_value.shipment[$shipment.id].0|number_format:2:".":" "}" x-cast="float" />
                    	<span class="input-group-addon"> zł</span>
                    </div>
                    </td>
                    <td class="center">
                    <div class="input-group col-lg-12">
                    	<input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][1]" value="{$fields_value.shipment[$shipment.id].1|number_format:2:".":" "}" x-cast="float" />
                    	<span class="input-group-addon"> zł</span>
                    </div>
                    </td>
                    <td class="center"><input type="text"{if !$is_bootstrap} style="width:60%;"{/if} name="shipment[{$shipment.id}][2]" value="{$fields_value.shipment[$shipment.id].2|default:"1"}" x-cast="integer" /></td>
                </tr>
            {/if}
        {/foreach}
    </tbody>
</table>
</div>
{/block}
