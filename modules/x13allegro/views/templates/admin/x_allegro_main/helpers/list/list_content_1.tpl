<tbody>
{if count($list)}
{foreach $list AS $index => $tr}
    {$field_value = $tr.fields_value}
    
    <tr class="{if $index is odd}alt_row{/if}">
        <td class="center" style="border-bottom: none">
            <input type="checkbox" name="item[{$index}][enabled]" value="1" class="noborder" {if count($list) == 1 OR $field_value.enabled}checked="checked"{/if} />
        </td>
        <td colspan="{$fields_display|@count}" style="border-bottom: none">
            <p>{$tr.name}{if $tr.name_product_attribute} {$tr.name_product_attribute}</span>{/if}</p>
        </td>
    </tr>
	<tr
	{if $position_identifier}id="tr_{$id_category}_{$tr.$identifier}_{if isset($tr.position['position'])}{$tr.position['position']}{else}0{/if}"{/if}
	class="{if $index is odd}alt_row{/if} {if $row_hover}row_hover{/if}"
	style="{if count($list) > 1 AND !$field_value.enabled}display: none;{/if}{if isset($tr.color) && $color_on_bg}background-color: {$tr.color};{/if}"
	>
		<td class="center">
			{if {$has_bulk_actions}}
				{if isset($list_skip_actions.delete)}
					{if !in_array($tr.$identifier, $list_skip_actions.delete)}
						<input type="checkbox" name="{$table}Box[]" value="{$tr.$identifier}" class="noborder" />
					{/if}
				{else}
					<input type="checkbox" name="{$table}Box[]" value="{$tr.$identifier}" class="noborder" />
				{/if}
			{/if}
		</td>
		{foreach $fields_display AS $key => $params}
			{block name="open_td"}
				<td
					{if isset($params.position)}
						id="td_{if !empty($id_category)}{$id_category}{else}0{/if}_{$tr.$identifier}"
					{/if}
					class="{if !$no_link}pointer{/if}
					{if isset($params.position) && $order_by == 'position'  && $order_way != 'DESC'} dragHandle{/if}
					{if isset($params.class)} {$params.class}{/if}
					{if isset($params.align)} {$params.align}{/if}"
					{if (!isset($params.position) && !$no_link && !isset($params.remove_onclick))}
						onclick="document.location = '{$current_index}&{$identifier}={$tr.$identifier}{if $view}&view{else}&update{/if}{$table}&token={$token}'">
					{else}
					>
				{/if}
			{/block}
			{block name="td_content"}
                {if $key == 'name'}
                    <input type="hidden" name="item[{$index}][id_product]" value="{$tr.id_product}"/>
                    <input type="hidden" name="item[{$index}][id_product_attribute]" value="{$tr.id_product_attribute}"/>
                    
                    <input type="text" name="item[{$index}][title]" value="{if $field_value.title}{$field_value.title}{else}{$tr.name}{if $tr.id_product_attribute} - {$tr.name_product_attribute}{/if}{/if}" size="28" maxlength="50" />
                {elseif $key == 'quantity'}
                    <input type="text" name="item[{$index}][quantity]" value="{$tr.quantity}" size="3" />
                    <select name="item[{$index}][quanity_type]">
                        <option value="0">szt</option>
                        <option value="1">kpl</option>
                        <option value="2">par</option>
                    </select>
                    <br />
                    Na stanie: {$tr.quantity}
                {elseif $key == 'price'}
                    <input type="text" name="item[{$index}][price_buy_now]" value="{if $field_value.price_buy_now}{$field_value.price_buy_now}{else}{$tr.price}{/if}" size="8" />
                    <br />
                    Kup teraz
                    <br />
                    <input type="text" name="item[{$index}][price_asking]" value="" size="8" maxlength="50" />
                    <br />
                    Cena wywoławcza
                    <br />
                    <input type="text" name="item[{$index}][price_minimal]" value="" size="8" maxlength="50" />
                    <br />
                    Cena minimalna
                {elseif $key == 'duration'}
                    <select name="item[{$index}][duration]">
                        {foreach $tr.duration_list AS $item}
                            <option value="{$item['value']}">{$item['label']}</option>
                        {/foreach}
                    </select>
                {elseif $key == 'images'}
                    {foreach $tr.images as $item}
                        <div class="item_form" style="float: left; border: 1px solid #ccc; margin: 5px;">
                            <input type="checkbox" name="item[{$index}][images][]" value="{$item.id_image}" style="position: absolute; margin: 5px 0px 0px 5px;"/>
                            <img src="{$link->getImageLink($tr.link_rewrite, $item.id_image, 'medium_default')}" />
                        </div>
                    {/foreach}
                {elseif $key == 'theme'}
                    <select name="item[{$index}][template]">
                        {foreach $tr.templates_list AS $item}
                            <option value="{$item['id']}">{$item['name']}</option>
                        {/foreach}
                    </select>
                {elseif $key == 'additional'}
                    <input type="checkbox" name="item[{$index}][additional][bold]" value="1" /><label class="t">Pogrubienie</label><br />
                    <input type="checkbox" name="item[{$index}][additional][thumbnail]" value="2" /><label class="t">Miniaturka</label><br />
                    <input type="checkbox" name="item[{$index}][additional][backlight]" value="4" /><label class="t">Podświetlenie</label><br />
                    <input type="checkbox" name="item[{$index}][additional][distinction]" value="8" /><label class="t">Wyróżnienie</label><br />
                    <input type="checkbox" name="item[{$index}][additional][category_main]" value="16" /><label class="t">Strona kategorii</label><br />
                    <input type="checkbox" name="item[{$index}][additional][watermark]" value="64" /><label class="t">Znak wodny</label><br />
                {else}
                    {$key}
                {/if}
			{/block}
			{block name="close_td"}
				</td>
			{/block}
		{/foreach}

	{if $shop_link_type}
		<td class="center" title="{$tr.shop_name}">
			{if isset($tr.shop_short_name)}
				{$tr.shop_short_name}
			{else}
				{$tr.shop_name}
			{/if}</td>
	{/if}
	{if $has_actions}
		<td class="center" style="white-space: nowrap;">
			{foreach $actions AS $action}
				{if isset($tr.$action)}
					{$tr.$action}
				{/if}
			{/foreach}
		</td>
	{/if}
	</tr>
{/foreach}
{else}
	<tr><td class="center" colspan="{count($fields_display) + 2}">{l s='No items found'}</td></tr>
{/if}
</tbody>
