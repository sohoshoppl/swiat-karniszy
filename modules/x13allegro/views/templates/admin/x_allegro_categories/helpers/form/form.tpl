{extends file="helpers/form/form.tpl"}

{block name="label"}
	{if isset($input.label)}
		<label class="col-lg-3{if isset($input.required) && $input.required && $input.type != 'radio'} required{/if}"><strong>{$input.label} {if isset($input.required) && $input.required}<sup>*</sup>{/if}</strong>
			{if isset($input.hint)}
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{if is_array($input.hint)}
						{foreach $input.hint as $hint}
							{if is_array($hint)}
								{$hint.text|escape:'html':'UTF-8'}
							{else}
								{$hint|escape:'html':'UTF-8'}
							{/if}
						{/foreach}
					{else}
						{$input.hint|escape:'html':'UTF-8'}
					{/if}">
			{/if}
		</label>
	{/if}
{/block}
{block name="field"}
	{if $input.name == 'id_allegro_category'}
        {$parent_id = 0}
        {foreach $input.path as $value}
            <select name="{$input.name}[]" class="{if isset($input.class)}{$input.class}{/if}"
                id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}"
                {if isset($input.size)}size="{$input.size}"{/if} style="width: 200px; padding: 5px; margin-right: 20px; display:inline;">
                <option value="0">-- Wybierz --</option>
                {foreach $input.categories as $category}
                    {if $parent_id == $category.parent_id}
                        <option value="{$category.id}"
                            {if $value == $category.id}
                                selected="selected"
                            {/if}
                        >{$category.name}</option>
                    {/if}
                {/foreach}
            </select>

            {$parent_id = $value}
        {/foreach}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="input"}
    {if $input.name == 'category_fields'}
        {$field_value = $category_fields_values[$input.id]}
        {$field_feature = $category_fields_values["{$input.id}_feature"]}
        
        {if $input.type == 'text'}
            <input type="text" class="{if isset($input.class)}{$input.class}{/if}"
            style="display:inline-block"
            name="{$input.name}[{$input.id}]"
            {if isset($input.size)}size="{$input.size}"{/if}
            {if isset($input.maxlength)}maxlength="{$input.maxlength}"{/if}
            {if isset($input.readonly) && $input.readonly}readonly="readonly"{/if}
            {if isset($input.disabled) && $input.disabled}disabled="disabled"{/if}
            value="{$field_value}"
            />
            
            {if $features}
                <span>lub</span> 
                <select name="{$input.name}[{$input.id}_feature]" class="{if isset($input.class)}{$input.class}{/if}" style="display:inline-block;">
                    <option value="0">Wybierz wartość cechy</option>
                    {foreach $features as $option}
                        <option value="{$option.id_feature}" {if $option.id_feature == $field_feature}selected="selected"{/if}>{$option.name}</option>
                    {/foreach}
                </select>
            {/if}
            {if isset($input.unit) and $input.unit}[<small>{$input.unit}</small>]{/if}
        {elseif $input.type == 'select'}
            <select name="{$input.name}[{$input.id}]" class="{if isset($input.class)}{$input.class}{/if}"
            {if isset($input.multiple)}multiple="multiple" {/if}
            {if isset($input.size)}size="{$input.size}"{/if}>
                {foreach $input.options.query as $option}
                    {if is_object($option)}
                        <option 
                            value="{$option->$input.options.id}"
                            {if $field_value == $option->$input.options.id}selected="selected"{/if}
                            >{$option->$input.options.name}</option>
                    {else}
                        <option 
                            value="{$option[$input.options.id]}"
                            {if $field_value == $option[$input.options.id]}selected="selected"{/if}
                            >{$option[$input.options.name]}</option>
                    {/if}
                {/foreach}
            </select>
        {elseif $input.type == 'radio'}
            {foreach $input.values as $value}
                <input 
                    type="radio" 
                    name="{$input.name}[{$input.id}]" 
                    value="{$value.value}" 
                    {if $field_value == $value.value}checked="checked"{/if} 
                    {if isset($input.disabled) && $input.disabled}disabled="disabled"{/if} />
                <label {if isset($input.class)}class="{$input.class}"{/if}>
                 {if isset($input.is_bool) && $input.is_bool == true}
                    {if $value.value == 1}
                        <img src="../img/admin/enabled.gif" alt="{$value.label}" title="{$value.label}" />
                    {else}
                        <img src="../img/admin/disabled.gif" alt="{$value.label}" title="{$value.label}" />
                    {/if}
                 {else}
                    {$value.label}
                 {/if}
                </label>
                {if isset($input.br) && $input.br}<br />{/if}
                {if isset($value.p) && $value.p}<p>{$value.p}</p>{/if}
            {/foreach}
        {elseif $input.type == 'textarea'}
            <textarea name="{$input.name}{if isset($input.key)}[{$input.key}]{/if}" id="{if isset($input.id)}{$input.id}{else}{$input.name}{/if}" cols="{$input.cols}" rows="{$input.rows}">{$field_value}</textarea>
        {elseif $input.type == 'checkbox'}
            {foreach $input.options.query as $value}
                <input type="checkbox"
                    name="{$input.name}[{$input.id}][]"
                    class="{if isset($input.class)}{$input.class}{/if}"
                    {if $field_value and in_array($value[$input.options.id], $field_value)}checked="checked"{/if} 
                    value="{$value[$input.options.id]}" />
                <label class="t">{$value[$input.options.name]}</label><br />
            {/foreach}
        {/if}
        
        {if !$is_bootstrap && isset($input.required) && $input.required && $input.type != 'radio'} <sup>*</sup>{/if}

        {if $input.type == 'text' and isset($input.value_type)}
            <p class="preference_description">
                {if $input.value_type == 'integer'}
                    Tylko liczby całkowite.
                {elseif $input.value_type == 'float'}
                    Dowolona liczba.
                {/if}
            </p>
        {/if}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}

{block name="script"}
    var XAllegro = new $.X13Allegro();
    XAllegro.categoryForm();
{/block}
