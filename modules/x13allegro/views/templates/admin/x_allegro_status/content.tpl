{if $bootstrap}

<div class="panel">
	<form class="form-horizontal" action="{$form_action}" method="post">
	<input type="hidden" name="action" value="{$action}">
	<div class="panel-heading">{l s='Powiązania statusów płatności' mod='x13allegro'}</div>
	{foreach from=$statuses item=stat}
	<div class="form-group">
		<label class="control-label col-lg-3" for="simple_product">{$stat.allegro_name}</label>
		<div class="col-lg-3">
			<select class="select fixed-width-xxl" name="id_order_state[{$stat.id_xallegro_status}]">
				<option value="0"> - </option>
				{foreach from=$order_states item=os}
				<option value="{$os.id_order_state}"{if $os.id_order_state == $stat.id_order_state} selected="selected"{/if}>{$os.name}</option>
				{/foreach}
			</select>
		</div>
	</div>
	{/foreach}
	<div class="panel-footer">
		<button type="submit" name="submitXAllegroStatus" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Zapisz' mod='x13allegro'}</button>
	</div>
	</form>
</div>

{else}

<h2>Statusy dla płatności </h2>
<form class="form-horizontal" action="{$form_action}" method="post">
<input type="hidden" name="action" value="{$action}">
<fieldset>
	<legend>{l s='Powiązania statusów płatności' mod='x13allegro'}</legend>
	{foreach from=$statuses item=stat}
		<label>{$stat.allegro_name}</label>
		<div class="margin-form">
			<select class="select fixed-width-xxl" name="id_order_state[{$stat.id_xallegro_status}]">
				<option value="0"> - </option>
				{foreach from=$order_states item=os}
				<option value="{$os.id_order_state}"{if $os.id_order_state == $stat.id_order_state} selected="selected"{/if}>{$os.name}</option>
				{/foreach}
			</select>
		</div>
		<div style="clear:both;"></div>
	{/foreach}
	<center>
		<button type="submit" name="submitXAllegroStatus" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Zapisz' mod='x13allegro'}</button>
	</center>
</fieldset>
</form>

{/if}