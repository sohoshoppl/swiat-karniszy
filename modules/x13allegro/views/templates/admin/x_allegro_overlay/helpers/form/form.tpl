{extends file="helpers/form/form.tpl"}

{block name="input"}
	{if $input.type == 'image'}
		{if $bootstrap}
			image
		{/if}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}

{block name=script}
    {if $is_bootstrap}
		$(document).ready(function () {

		if ($('div#image-images-thumbnails img').size() > 0) {
		{if $fields_value.X1 == 0 AND $fields_value.Y1 == 0 AND $fields_value.X2 == 0 AND $fields_value.Y2 == 0}
			alert('Zaznacz maksymalne pole na zdjęcie.');
		{/if}
		}


		function change(img, selection) {
			if (!selection.width || !selection.height)
				return;

			$('#X1').val(selection.x1);
			$('#Y1').val(selection.y1);
			$('#X2').val(selection.x2);
			$('#Y2').val(selection.y2);  
		}

			var ias = $('div#image-images-thumbnails img').eq(0).imgAreaSelect({
				{if $fields_value.X1 AND $fields_value.Y1 AND $fields_value.X2 AND $fields_value.Y2}
				x1: {$fields_value.X1}, 
				y1: {$fields_value.Y1}, 
				x2: {$fields_value.X2}, 
				y2: {$fields_value.Y2},
				{/if}
				handles: true,
				onSelectChange: change,
				onSelectEnd: function (img, selection) {
					if (!selection.width || !selection.height) {
						$('#X1').val(0);
						$('#Y1').val(0);
						$('#X2').val(0);
						$('#Y2').val(0);  
					}
				}
			});
		});
	{else}
		$(document).ready(function () {

		if ($('div#image img').size() > 0) {
		{if $fields_value.X1 == 0 AND $fields_value.Y1 == 0 AND $fields_value.X2 == 0 AND $fields_value.Y2 == 0}
			alert('Zaznacz maksymalne pole na zdjęcie.');
		{/if}
		}

		function change(img, selection) {
			if (!selection.width || !selection.height)
				return;

			$('#X1').val(selection.x1);
			$('#Y1').val(selection.y1);
			$('#X2').val(selection.x2);
			$('#Y2').val(selection.y2);  
		}

			var ias = $('div#image img').eq(0).imgAreaSelect({
				{if $fields_value.X1 AND $fields_value.Y1 AND $fields_value.X2 AND $fields_value.Y2}
				x1: {$fields_value.X1}, 
				y1: {$fields_value.Y1}, 
				x2: {$fields_value.X2}, 
				y2: {$fields_value.Y2},
				{/if}
				handles: true,
				onSelectChange: change,
				onSelectEnd: function (img, selection) {
					if (!selection.width || !selection.height) {
						$('#X1').val(0);
						$('#Y1').val(0);
						$('#X2').val(0);
						$('#Y2').val(0);  
					}
				}
			});
		});
	{/if}
{/block}
