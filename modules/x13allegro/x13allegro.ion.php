<?php

if (!function_exists('x13ionCheckPhp')) {
    function x13ionCheckPhp()
    {
        if (!defined('X13_ION_VERSION')) {
            define('X13_ION_VERSION', (PHP_VERSION_ID >= 70000 ? '-7' : ''));
        }
    }
}

x13ionCheckPhp();
