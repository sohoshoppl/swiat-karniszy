<?php

if(realpath('..') == '/home/htmnet/www/prestashop_modules') {
	define('_DIR_', '/home/htmnet/www/prestashop.1.6/');
}
else {
	define('_DIR_', dirname(__FILE__).'/../../');
}

include(_DIR_.'config/config.inc.php');
require_once(_DIR_.'init.php');

$query = Tools::getValue('q', false);
if (!$query OR $query == '' OR strlen($query) < 1)
	die();

if($pos = strpos($query, ' (ref:'))
	$query = substr($query, 0, $pos);

$sql = '
	SELECT p.`id_product`, p.`reference`, pl.`name`
	FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
	WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.'
	GROUP BY p.id_product
';

$items = Db::getInstance()->executeS($sql);

foreach ($items AS $item)
	echo 'id: '.$item['id_product'].' - '.trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";

?>
