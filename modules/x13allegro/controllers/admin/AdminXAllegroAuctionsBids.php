<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

    class AdminXAllegroAuctionsBidsController extends XAllegroController
    {
        
        protected $_checkAccount = false;
        protected $_auctionTitle = '';
        
        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            parent::__construct();
            
            if (Tools::getValue('id_auction') && Tools::getValue('id_allegro_account')) {
                $account = Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'xallegro_account WHERE id_xallegro_account = ' . (int)Tools::getValue('id_allegro_account'));
                    
                if (!$account) {
                    $this->errors[] = $this->l('Niepoprawne parametry wywołania');
                    return;
                }
                
                try {
                    $this->_API = new XAllegroAPI($account);
                    
                    $itemBids = $this->_API->doGetBidItem2((float)Tools::getValue('id_auction'));
                    
                    if ($itemBids) {
                        $postBuyData = $this->_API->doGetPostBuyData(array((float)Tools::getValue('id_auction')));
                        
                        $users = array();
                        
                        foreach ($postBuyData[0]->{'users-post-buy-data'} as $user) {
                            $users[$user->{'user-data'}->{'user-id'}] = $user;
                        }
                        
                        foreach ($itemBids[0] as $bid) {
                            $this->_list[] = array(
                                'user_id' => $users[$bid[1]]->{'user-data'}->{'user-id'},
                                'user_login' => $users[$bid[1]]->{'user-data'}->{'user-login'},
                                'user_fullname' => $users[$bid[1]]->{'user-data'}->{'user-first-name'} . ' ' . $users[$bid[1]]->{'user-data'}->{'user-last-name'},
                                'user_email' => $users[$bid[1]]->{'user-data'}->{'user-email'},
                                'user_email' => $users[$bid[1]]->{'user-data'}->{'user-email'},
                                'deal_quantity' => $bid[5],
                                'deal_date' => $bid[7]
                            );
                        }
                    }
                    
                    $auction = $this->_API->doGetItemsInfo(array(
                        (float)Tools::getValue('id_auction')
                    ));
                    $this->_auctionTitle = $auction['array-item-list-info'][0]->{'item-info'}->{'it-name'};
                } catch (Exception $e) {
                    $this->_APIError = $e->getMessage();
                }
            }
        }
        
        public function listTimestamp($timestamp)
        {
            return date('d.m.Y G:i:s', $timestamp);
        }
        
        public function renderList()
        {
            $this->list_no_link = true;
            $this->list_simple_header = true;
            $this->row_hover = false;
            
            $this->fields_list = array(
                'user_id' => array(
                    'title' => $this->l('Identyfikator użytkownika'),
                    'width' => 'auto'
                ),
                'user_login' => array(
                    'title' => $this->l('Nazwa użytkownika'),
                    'width' => 240,
                ),
                'user_fullname' => array(
                    'title' => $this->l('Imie i nazwisko'),
                    'width' => 240,
                ),
                'user_email' => array(
                    'title' => $this->l('Adres e-mail'),
                    'width' => 240
                ),
                'deal_quantity' => array(
                    'title' => $this->l('Ilość'),
                    'width' => 100
                ),
                'deal_date' => array(
                    'title' => $this->l('Data złożenia oferty'),
                    'width' => 160,
                    'callback' => 'listTimestamp'
                )
            );
            
            $helper = new HelperList();
            $this->setHelperDisplay($helper);
            $helper->identifier = 'user_id';
            $helper->tpl_vars = $this->tpl_list_vars;
            $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;
            
            foreach ($this->actions_available as $action)
            {
                if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action)
                    $this->actions[] = $action;
            }
            $helper->is_cms = $this->is_cms;
            $list = $helper->generateList($this->_list, $this->fields_list);

            return $list;
        }
        
        public function initToolbar()
        {
            $this->toolbar_btn['back'] = array(
                'href' => $this->context->link->getAdminLink('AdminXAllegroAuctions') . '&show=' . Tools::getValue('show'),
                'desc' => $this->l('Powrót do listy aukcji')
            );
        }
        
        public function initPageHeaderToolbar()
        {
            if (!$this->bootstrap) {
                return;
            }
            
            $this->page_header_toolbar_btn['allegro_current'] = array(
                'href' => $this->context->link->getAdminLink('AdminXAllegroAuctions') . '&show=' . Tools::getValue('show') . (Tools::getIsset('id_account') ? '&id_account='.Tools::getvalue('id_account') : ''),
                'desc' => $this->l('Powrót do listy aukcji'),
                'icon' => 'process-icon-arrow-left icon-arrow-left',
            );
            
            parent::initPageHeaderToolbar();
        
        }
        
        public function initToolbarTitle()
        {
            parent::initToolbarTitle();
            
            $this->toolbar_title[] = $this->_auctionTitle;
        }
    }
