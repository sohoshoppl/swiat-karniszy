<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

    class AdminXAllegroAccountsController extends XAllegroController
    {
        protected $_checkAccount = false;
        
        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            $this->table = 'xallegro_account';
            $this->className = 'XAllegroAccount';
            $this->lang = false;

            $this->addRowAction('edit');
            $this->addRowAction('delete');
            $this->bulk_actions = array('delete' => array('text' => $this->l('Usuń zaznaczone'), 'confirm' => $this->l('Usunąć zaznaczone elementy?')));
            
            $this->fields_list = array(
                'id_xallegro_account' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'width' => 20
                ),
                'username' => array(
                    'title' => $this->l('Nazwa użytkownika'),
                    'width' => 300
                ),
                'apikey' => array(
                    'title' => $this->l('Klucz API'),
                    'width' => 'auto'
                ),
                'shop' => array(
                    'title' => $this->l('Sklep'),
                    'active' => 'shop',
                    'width' => 70,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                ),
                'default' => array(
                    'title' => $this->l('Domyślny'),
                    'active' => 'default',
                    'width' => 70,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                ),
                'active' => array(
                    'title' => $this->l('Aktywny'),
                    'active' => 'active',
                    'width' => 70,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                ),
                'sandbox' => array(
                    'title' => $this->l('SB'),
                    'active' => 'sandbox',
                    'width' => 70,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                )
            );
            
            parent::__construct();

        }

        public function renderList()
        {
            if(Tools::getIsset('wrong_authentication_data'))
                $this->displayWarning(Tools::getValue('wrong_authentication_data'));
            return parent::renderList();
        }

        public function renderForm()
        {
            $collection = new Collection('XAllegroSite');
            $sites = $collection->getResults();

            $countries = array(array(
                'country-id' => '0',
                'country-name' => 'Wprowadź poprawny klucz API'
            ));
            
            $this->loadObject(true);
            
            if (Validate::isLoadedObject($this->object)) {
                try {
                    $API = new XAllegroAPI($this->object);
                    $countries = $API->getCountries($this->object->country_code);
                } catch (SoapFault $e) { }
            }
            
            $this->fields_form = array(
                'legend' => array(       
                    'title' => $this->l('Podstawowe dane'),       
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_' . $this->table
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa użytkownika'),
                        'name' => 'username',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'password',
                        'label' => $this->l('Hasło użytkownika'),
                        'name' => 'password',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Klucz API'),
                        'name' => 'apikey',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Portal'),
                        'name' => 'country_code',
                        'options' => array(
                            'query' => $sites,
                            'id' => 'country_code',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Kraj'),
                        'name' => 'country',
                        'options' => array(
                            'query' => $countries,
                            'id' => 'country-id',
                            'name' => 'country-name'
                        )
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Sklep Allegro'),
                        'name' => 'shop',
                        'required' => false,
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Domyślne konto'),
                        'name' => 'default',
                        'required' => false,
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'desc' => $this->l('Automatycznie wybierane podczas wystawiania przedmiotów'),
                        'default_value' => 1
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Aktywne konto'),
                        'name' => 'active',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Sandbox'),
                        'name' => 'sandbox',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'desc' => $this->l('Konto testowe w sandboxie')
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz')
                )
            );

            return parent::renderForm();
        }

        public function postProcess()
        {
            if (Tools::isSubmit('save') && !$this->errors) {
                $this->object = $this->loadObject(true);
                try {
                    $account = new XAllegroAccount();
                    $account->username = Tools::getValue('username');
                    
                    if (Tools::getValue('password')) {
                        $account->password = Tools::getValue('password');
                        $account->rijndael = false;
                    } else {
                        $account->password = $this->object->password;
                    }
                    
                    $account->apikey = Tools::getValue('apikey');
                    $account->sandbox = Tools::getValue('sandbox');
                    $account->country_code = Tools::getValue('country_code');

                    $API = new XAllegroAPI($account);
                    
                    if (Tools::getValue('shop')) {
                        $user = $API->getUser(Tools::getValue('username'));

                        if (!$user['user-has-shop']) {
                            $this->errors[] = Tools::displayError('Wybrane konto nie posiada sklepu Allegro.');
                        }
                    }
                } catch(SoapFault $error) {
                    $this->errors[] = $error->faultstring;
                }
            }
            
            
            parent::postProcess();

			if((int)Tools::getValue('default') == 1) {
				$this->object->setDefault();
			}

        }
        
        protected function copyFromPost(&$object, $table)
        {
            /* Classical fields */
            foreach ($_POST as $key => $value)
                if (key_exists($key, $object) && $key != 'id_'.$table)
                {
                    /* Do not take care of password field if empty */
                    if ($key == 'password' && Tools::getValue('id_'.$table) && empty($value))
                        continue;
                    /* Automatically encrypt password in MD5 */
                    if ($key == 'password' && !empty($value)) {
						if(defined('_RIJNDAEL_IV_') && defined('_RIJNDAEL_IV_')) {
							$rijndael = new Rijndael(_RIJNDAEL_KEY_, _RIJNDAEL_IV_);
							$value = $rijndael->encrypt($value);
						}
                    }
                    
                    $object->{$key} = $value;
                }

            /* Multilingual fields */
            $rules = call_user_func(array(get_class($object), 'getValidationRules'), get_class($object));
            if (count($rules['validateLang']))
            {
                $languages = Language::getLanguages(false);
                foreach ($languages as $language)
                    foreach (array_keys($rules['validateLang']) as $field)
                        if (isset($_POST[$field.'_'.(int)$language['id_lang']]))
                            $object->{$field}[(int)$language['id_lang']] = $_POST[$field.'_'.(int)$language['id_lang']];
            }
        }
        
        public function ajaxProcessGetCountries()
        {
            $apikey = Tools::getValue('apikey');
            $countryCode = Tools::getValue('country_code');
            $sandbox = Tools::getValue('sandbox');
            
            $account = new XAllegroAccount();
            $account->apikey = $apikey;
            $account->sandbox = $sandbox;
            $account->country_code = $countryCode;
            
            try {
                $API = new XAllegroAPI($account, false);
                $countries = $API->getCountries($countryCode);

                die(json_encode(array(
                    'status' => 'ok',
                    'countries' => $countries
                )));
            } catch(SoapFault $error) {
                die(json_encode(array(
                    'status' => 'error',
                    'error' => $this->l('Niepoprawny klucz API')
                )));
            }
        }
    }
