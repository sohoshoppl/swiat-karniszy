<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

    class AdminXAllegroAuctionsController extends XAllegroController
    {
        protected $_checkAccount = false;
        protected $_auctions = array();
        
        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            $this->table = 'xallegro_auction';
            $this->className = 'XAllegroAuction';
            $this->lang = false;
            
            $this->addRowAction('XAllegroAuctionEdit');
            $this->addRowAction('XAllegroDetails');
            $this->addRowAction('XAllegroAuctionUnlink');
            $this->addRowAction('XAllegroAuctionUrl');
            $this->addRowAction('XAllegroAuctionFinish');
            $this->addRowAction('XAllegroAuctionRedo');
            $this->addRowAction('XAllegroAuctionClone');
            $this->addRowAction('XAllegroAuctionBids');
            
			$this->bulk_actions['bulkRedo'] = array(
				'text' => $this->l('Wystaw ponownie'), 
				'icon' => 'icon-repeat bulkRedoAuction',
			);
			if(!Tools::getValue('show')) {
				$this->bulk_actions['bulkFinish'] = array(
					'text' => $this->l('Zakończ wybrane'), 
					'icon' => 'icon-times bulkFinishAuction',
				);
			}

            $this->list_no_link = true;
            $this->fields_list = array(
                'dummy' => array(
                    'title' => $this->l(''),
                    'width' => 'auto',
                    'search' => false,
                    'align' => 'center',
                    'orderby' => false,
                ),
                'title' => array(
                    'title' => $this->l('Tytuł aukcji'),
                    'width' => 'auto',
                    'class' => 'auction-title',
                    'search' => true,
                    'orderby' => true,
                ),
                'shop_info' => array(
                    'type' => 'bool',
                    'title' => $this->l('Sklep'),
                    'width' => 50,
                    'class' => 'fixed-width-xs',
                    'align' => 'center',
                    'icon' => true,
                    'src' => 'test',
                    'search' => false,
                    'orderby' => false,
                ),
                //'account_name' => array(
                //    'title' => $this->l('Nazwa konta'),
                //    'width' => 110,
                //    'search' => false,
                //    'orderby' => false,
                //),
                'quantity' => array(
                    'title' => $this->l('Ilość'),
                    'width' => 130,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => true,
                ),
                'bids' => array(
                    'title' => $this->l('Ofert'),
                    'width' => 90,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => true,
                ),
                'price_buy_now' => array(
                    'title' => $this->l('Cena "Kup Teraz"'),
                    'type' => 'price',
                    'width' => 130,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => true,
                ),
                'start' => array(
                    'title' => $this->l('Data rozpoczęcia'),
                    'width' => 130,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => false,
                ),
                'end' => array(
                    'title' => $this->l('Data zakończenia'),
                    'width' => 130,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => true,
                ),
                'closed' => array(
                    'title' => $this->l('Zakończona'),
                    'width' => 50,
                    'align' => 'center',
                    'class' => 'fixed-width-xs',
                    'align' => 'center',
                    'icon' => true,
                    'src' => 'test',
                    'search' => false,
                    'orderby' => false,
                ),
                'hits' => array(
                    'title' => $this->l('Wyświetleń'),
                    'width' => 100,
                    'align' => 'center',
                    'search' => false,
                    'orderby' => false,
                )
            );
            
            parent::__construct();
            
            $this->_conf[80] = $this->l('Zakończono wybrane aukcje.');
            
//            Pobranie relacji do aukcji
            $this->_auctions = Db::getInstance()->executeS('SELECT id_product, id_auction, closed FROM ' . _DB_PREFIX_ . 'xallegro_auction');
			//p($this->_auctions);

//            Dane aukcji i konta
            if (Tools::getValue('id_auction') && Tools::getValue('id_allegro_account')) {
                $this->object = new StdClass;
                $this->object->id = false;
                $this->object->show = Tools::getValue('show');
                
//                Pobranie produktu z bazy jeśli istnieje
                $auction = Db::getInstance()->getRow('SELECT id_xallegro_auction, id_product, id_product_attribute '
                    . 'FROM ' . _DB_PREFIX_ . 'xallegro_auction '
                    . 'WHERE id_auction = ' . Tools::getValue('id_auction'));
                
                if ($auction) {
                    $this->object->id = $auction['id_xallegro_auction'];
                    $this->object->id_product = $auction['id_product'];
                    $this->object->name = Product::getProductName($auction['id_product'], $auction['id_product_attribute']);
                }
                
//                Pobranie konta
                $account = Db::getInstance()->getRow('SELECT a.*, IF(a.sandbox, CONCAT(s.url, \'.webapisandbox.pl\'), s.url) as service '
                    . 'FROM ' . _DB_PREFIX_ . 'xallegro_account AS a '
                    . 'LEFT JOIN ' . _DB_PREFIX_ . 'xallegro_site AS s '
                    . 'ON (a.country_code = s.country_code)'
                    . 'WHERE a.id_xallegro_account = ' . (int)Tools::getValue('id_allegro_account'));
                    
                if (!$account) {
                    $this->errors[] = $this->l('Niepoprawne parametry wywołania');
                    return;
                }
                
                try {
                    $this->_API = new XAllegroAPI($account);
                    
                    $item = $this->_API->doShowItemInfoExt((float)Tools::getValue('id_auction'));
                    
                    if (!$item['item-list-info-ext']) {
                        $this->errors[] = $this->l('Niepoprawne parametry wywołania');
                        return;
                    }
                    $this->object->id_allegro_account = $account['id_xallegro_account'];
                    $this->object->service = $account['service'];
                    $this->object->title = $item['item-list-info-ext']->{'it-name'};
                    $this->object->price_buy_now = $item['item-list-info-ext']->{'it-buy-now-price'};
                    $this->object->quantity = $item['item-list-info-ext']->{'it-quantity'};
                    $this->object->quantity_start = $item['item-list-info-ext']->{'it-starting-quantity'};
                    $this->object->closed = ($item['item-list-info-ext']->{'it-ending-info'} > 1 ? 1 : 0);
                } catch (Exception $e) {
                    $this->_APIError = $e->getMessage();
                }
            }
        }

        public function setMedia()
        {
            parent::setMedia();
            
            $this->addJqueryPlugin('autocomplete');
        }
        
        public function initToolbar()
        {
            if ($this->display == 'edit') {
                
				parent::initToolbar();
//                Dodanie do zmiennej informacji o wcześniej wybranym show
                $this->toolbar_btn['back']['href'] = $this->context->link->getAdminLink('AdminXAllegroAuctions') . '&show=' . Tools::getValue('show');
                
//                Jeśli produkt przypisany usuwany button zapisz
                if (ValidateCore::isLoadedObject($this->object)) {
                    unset($this->toolbar_btn['save']);
                }
            }
        }

        public function initPageHeaderToolbar()
        {
            if (!$this->bootstrap) {
                return;
            }
            
            if (empty($this->display)) {
                $this->page_header_toolbar_btn['allegro_current'] = array(
                    'href' => self::$currentIndex.'&token='.$this->token.(Tools::getIsset('id_account') ? '&id_account='.Tools::getvalue('id_account') : ''),
                    'desc' => $this->l('Sprzedaję'),
                    'icon' => 'process-icon-play-circle-o icon-play-circle-o',
                    'class' => 'x-allegro_current'
                );

                $this->page_header_toolbar_btn['allegro_sold'] = array(
                    'href' => self::$currentIndex.'&token='.$this->token.(Tools::getIsset('id_account') ? '&id_account='.Tools::getvalue('id_account') : '').'&show=sold',
                    'desc' => $this->l('Sprzedane'),
                    'icon' => 'process-icon-chech-circle-o icon-check-circle-o',
                    'class' => 'x-allegro_sold'
                );

                $this->page_header_toolbar_btn['allegro_notsold'] = array(
                    'href' => self::$currentIndex.'&token='.$this->token.(Tools::getIsset('id_account') ? '&id_account='.Tools::getvalue('id_account') : '').'&show=notsold',
                    'desc' => $this->l('Nie sprzedane'),
                    'icon' => 'process-icon-times-circle-o icon-times-circle-o',
                    'class' => 'x-allegro_notsold'
                );

                $this->page_header_toolbar_btn['allegro_future'] = array(
                    'href' => self::$currentIndex.'&token='.$this->token.(Tools::getIsset('id_account') ? '&id_account='.Tools::getvalue('id_account') : '').'&show=future',
                    'desc' => $this->l('Oczekujące do wystawienia'),
                    'icon' => 'process-icon-clock-o icon-clock-o',
                    'class' => 'x-allegro_future'
                );
            }
            
            parent::initPageHeaderToolbar();
        }
        
        public function initToolbarTitle()
        {
            parent::initToolbarTitle();
            
            if (Tools::getValue('show') == 'sold') {
				$this->meta_title = Tools::version_compare(_PS_VERSION_, '1.6.0.14', '>') ? array('Allegro', 'Sprzedane') : 'Allegro - sprzedane';
                $title = 'Sprzedane';
            } elseif (Tools::getValue('show') == 'notsold') {
				$this->meta_title = Tools::version_compare(_PS_VERSION_, '1.6.0.14', '>') ? array('Allegro', 'Nie sprzedane') : 'Allegro - nie sprzedane';
                $title = 'Niesprzedane';
            } elseif (Tools::getValue('show') == 'future') {
				$this->meta_title = Tools::version_compare(_PS_VERSION_, '1.6.0.14', '>') ? array('Allegro', 'Oczekujące') : 'Allegro - oczekujące';
                $title = 'Oczekujące do wystawienia';
            } else {
				$this->meta_title = Tools::version_compare(_PS_VERSION_, '1.6.0.14', '>') ? array('Allegro', 'Sprzedaję') : 'Allegro - sprzedaję';
                $title = 'Sprzedaję';
            }
            
            $this->toolbar_title[] = $title;
        }

        public function displayXAllegroAuctionEditLink($token = null, $id, $name = null) {
			if(Tools::getValue('show') != 'notsold') {
				$ids = explode('|', $id);
				$row = XAllegroTools::findElementByKeyValue($this->_auctions, 'id_auction', current($ids));
				$href = $this->context->link->getAdminLink('AdminXAllegroAuctions') . '&id_auction=' . current($ids) . '&id_allegro_account=' . end($ids) . '&show=' . Tools::getValue('show');
				if ($row) {
					return 
					'<a href="' . $href . '" title="' . $this->l('Powiązanie produktu') . '">
						<img src="../img/admin/return.gif" alt="' . $this->l('Powiązanie produktu') . '" />
						'.($this->bootstrap ? $this->l('Powiązanie') : '').'
					</a>';
				} else {
					return '
					<a href="' . $href . '" title="' . $this->l('Przypisz aukcje do produktu') . '">
						<img src="../img/admin/subdomain.gif" alt="' . $this->l('Przypisz aukcje do produktu') . '" />
						'.($this->bootstrap ? $this->l('Przypisz') : '').'
					</a>';
				}
			}
		}
        
		public function displayXAllegroAuctionUnlinkLink($token = null, $id, $name = null) {
			if(Tools::getValue('show') != 'notsold') {
				$ids = explode('|', $id);
				$row = XAllegroTools::findElementByKeyValue($this->_auctions, 'id_auction', current($ids));
				$href = $this->context->link->getAdminLink('AdminXAllegroAuctions') . '&id_auction=' . current($ids) . '&id_allegro_account=' . end($ids) . '&delete_link';
				if ($row) {
					return 
					'<a href="' . $href . '" title="' . $this->l('Usuń powiązanie produktu') . '" onclick="return confirm(\''.$this->l('Czy na pewno chcesz usunąć wybrane powiązanie?').'\')">
						<img src="../img/admin/forbbiden.gif" alt="' . $this->l('Usuń powiązanie produktu') . '" />
						'.($this->bootstrap ? $this->l('Usuń powiązanie') : '').'
					</a>';
				}
			}
		}

		public function displayXAllegroAuctionUrlLink($token = null, $id, $name = null) {
			$row = XAllegroTools::findElementByKeyValue($this->_list, 'id_auction', $id);
			$ids = explode('|', $id);
			return '
				<a title="'.$this->l('Zobacz na na Allegro').'" href="' . $row['service'] . '/show_item.php?item=' . current($ids) . '" target="_blank" style="color:#FF5A00;">
					<img height=14 src="../modules/x13allegro/img/AdminXAllegroMain.png" alt="a" />
					'.($this->bootstrap ? $this->l('Zobacz') : '').'
				</a>
			';
		}

		public function displayXAllegroAuctionFinishLink($token = null, $id, $name = null) {
			//if(!Tools::getValue('show')) {
				$ids = explode('|', $id);
				$row = XAllegroTools::findElementByKeyValue($this->_list, 'id_auction', $id);
				if($row && !is_array($row['closed']))
				return '
					<a class="action-finish" 
					   href="#finish" 
					   data-id="' . current($ids) .'"
					   data-title="' . htmlspecialchars($row['title']) . '"
					   data-thumbnail="' . $row['thumbnail'] . '"
					   title="' . $this->l('Zakończ aukcję') . '">
						<img src="../modules/x13allegro/img/finish.png" alt="' . $this->l('zakończ') . '" />
						'.($this->bootstrap ? $this->l('Zakończ') : '').'
					</a>
				';
			//}
		}
		
		public function displayXAllegroAuctionCloneLink($token = null, $id, $name = null) {
			$ids = explode('|', $id);
			$row = XAllegroTools::findElementByKeyValue($this->_auctions, 'id_auction', current($ids));
			$href = 'index.php?controller=AdminXAllegroMain&token='.Tools::getAdminTokenLite('AdminXAllegroMain').'&clone_id='.current($ids);
			if($row) {
				return '
					<a class="action-clone" 
					   href="'.$href.'" 
					   title="' . $this->l('Wystaw podobną aukcję') . '">
						<img src="../modules/x13allegro/img/clone.png" alt="' . $this->l('wystaw podobną') . '" />
						'.($this->bootstrap ? $this->l('Wystaw podobną') : '').'
					</a>
				';
			}
		}
		
		public function displayXAllegroAuctionRedoLink($token = null, $id, $name = null) {
			$ids = explode('|', $id);
			$row = XAllegroTools::findElementByKeyValue($this->_list, 'id_auction', $id);
			$row_2 = XAllegroTools::findElementByKeyValue($this->_auctions, 'id_auction', current($ids));
			if($row_2 && is_array($row['closed'])) {
				return '
					<a class="action-redo" 
					   href="#redo"
					   data-id="' . current($ids) .'"
					   data-title="' . htmlspecialchars($row['title']) . '"
					   data-thumbnail="' . $row['thumbnail'] . '"
					   title="' . $this->l('Wystaw ponownie wybraną aukcję') . '">
						<img src="../modules/x13allegro/img/redo.png" alt="' . $this->l('wystaw ponownie') . '" />
						'.($this->bootstrap ? $this->l('Wystaw ponownie') : '').'
					</a>
				';
			}
		}

        public function displayXAllegroAuctionBidsLink($token = null, $id, $name = null)
        {
            $ids = explode('|', $id);
            $href = $this->context->link->getAdminLink('AdminXAllegroAuctionsBids') . '&id_auction=' . current($ids) . '&id_allegro_account=' . end($ids) . '&show=' . Tools::getValue('show');
            return '
				<a href="' . $href . '" title="' . $this->l('Oferty w aukcji') . '">
					<img src="../img/admin/tab-orders.gif" alt="' . $this->l('Oferty w aukcji') . '" />
					'.($this->bootstrap ? $this->l('Oferty') : '').'
				</a>';
        }

        public function renderList()
        {
			//Get allegro accounts
			$collection = new Collection('XAllegroAccount');
			$collection->where('active', '=', 1);
			$collection->orderBy('default', 'desc');
			$allegroAccounts_temp = $collection->getResults();
			
			if (!count($allegroAccounts_temp)) {
				$this->errors[] = $this->l('Brak utworzonych kont Allegro.');
				return;
			}
			else {
				foreach($allegroAccounts_temp as $a) {
					$allegroAccounts[] = array(
						'username' => $a->username,
						'selected' => (Tools::getValue('id_account') && Tools::getValue('id_account') == $a->id),
						'url' => self::$currentIndex.'&token='.Tools::getValue('token').'&id_account='.$a->id.(Tools::getValue('show') ? '&show='.Tools::getValue('show') : ''),
						'id' => $a->id,
					);
				}
			}

            $helper = new HelperList();
            $this->setHelperDisplay($helper);
            $this->_getList(Tools::getValue('id_account'));
            $helper->simple_header = false;
			$helper->show_toolbar = true;
			$helper->toolbar_scroll = true;
			$helper->table = 'xallegro_auction';
            $helper->identifier = 'id_auction';
            $helper->_defaultOrderBy = 'end';
            //$helper->_pagination = array(5, 10, 20, 50, 100);
            $helper->orderBy = $this->context->cookie->xallegroauctionsxallegro_auctionOrderby;
            $helper->orderWay = strtoupper($this->context->cookie->xallegroauctionsxallegro_auctionOrderway);
            $helper->toolbar_btn = array(null);
            $helper->tpl_vars = $this->tpl_list_vars;
            $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;
            $helper->currentIndex = self::$currentIndex.'&id_account='.Tools::getValue('id_account').(Tools::getIsset('show') ? '&show='.Tools::getValue('show') : '');
			
			//override default action attribute
			$helper->tpl_vars['override_action_attr'] = self::$currentIndex.'&id_account='.Tools::getValue('id_account').'&token='.$this->token.(Tools::getIsset('show') ? '&show='.Tools::getValue('show') : '').'#xallegro_auction';
            
			$helper->tpl_vars['allegroAccounts'] = $allegroAccounts;
			
            foreach ($this->actions_available as $action)
            {
                if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action)
                    $this->actions[] = $action;
            }
            //$helper->is_cms = $this->is_cms;
            $helper->listTotal = $this->_listTotal;
            $list = $helper->generateList($this->_list, $this->fields_list);

            return $list;

        }
        
        public function _getList($id_xallegro_account = null)
        {
			if(Tools::getIsset('pagination')) {
				$limit = (int)Tools::getValue('pagination', $limit);
				$this->context->cookie->{$this->table.'_pagination'} = $limit;
			} else {
				$limit = (int)$this->context->cookie->xallegro_auction_pagination;
			}
			
			$page = (int)$this->context->cookie->submitFilterxallegro_auction;
			
            $accounts = Db::getInstance()->executeS('SELECT a.*, IF(a.sandbox, CONCAT(s.url, \'.webapisandbox.pl\'), s.url) as service '
                . 'FROM ' . _DB_PREFIX_ . 'xallegro_account AS a '
                . 'LEFT JOIN ' . _DB_PREFIX_ . 'xallegro_site AS s '
                . 'ON (a.country_code = s.country_code)'
                . 'WHERE a.active = 1'
                . ($id_xallegro_account ? ' AND a.`id_xallegro_account` = '.(int)$id_xallegro_account : ' AND a.`default` = 1')
            );

            foreach ($accounts as $account) {
                try {
                    $this->_API = new XAllegroAPI($account);

					//Sprawdzam, czy są w bazie aukcje z nieprzypisanymi numerami aukcji.
					//Jeśli tak, odpytuję Allegro o identyfikatory tych aukcji.
					//Zapytanie idzie w pętli, więc żeby nie zajechac allegro jednorazowo odpytuję o max 20 rekordów.
					$sql = '
						SELECT `id_xallegro_auction`
						FROM `'._DB_PREFIX_.'xallegro_auction`
						WHERE `id_auction` = 0
							AND `id_allegro_account` = '.(int)$account['id_xallegro_account'].'
						LIMIT 0, 20
					';
					$results = Db::getInstance()->executeS($sql);
					foreach($results as $r) {
						$verify = $this->_API->doVerifyItem($r['id_xallegro_auction']);
						if($verify['item-id'] > 0) {
							$to_update[$r['id_xallegro_auction']] = $verify['item-id'];
						}
					}
					if(isset($to_update)) {
						XAllegroAuction::assignIdAuction($to_update);
					}
					
					//sortType:
					//  1 - po czasie zakończenia oferty (wartość domyślna)
					//  2 - po aktualnej cenie
					//  3 - po nazwie oferty
					//  4 - po ilości ofert
					//  5 - po najwyższej ofercie
					//  7 - po cenie minimalnej
					//  9 - po ilości sztuk wystawionych
					//  10 - po ilości sztuk sprzedanych
					//sortOrder:
					//  1 - rosnąco (wartość domyślna)
					//  2 - malejąco

					switch($this->context->cookie->xallegroauctionsxallegro_auctionOrderby) {
						case 'title': $sort_type = 3; break;
						case 'quantity': $sort_type = 9; break;
						case 'bids': $sort_type = 4; break;
						case 'price_buy_now': $sort_type = 2; break;
						default: $sort_type = 1; break;
					}
					
					switch($this->context->cookie->xallegroauctionsxallegro_auctionOrderway) {
						case 'desc': $sort_order = 2; break;
						default: $sort_order = 1; break;
					}
					
					if($this->context->cookie->xallegroauctionsxallegro_auctionFilter_shop_info === false)
						$si = -1;
					else
						$si = (int)$this->context->cookie->xallegroauctionsxallegro_auctionFilter_shop_info;
					switch($si) {
						case 0:  $filter_format = 1; break;
						case 1:  $filter_format = 2; break;
						default: $filter_format = 0; break;
					}
					
					$search = $this->context->cookie->xallegroauctionsxallegro_auctionFilter_title;
					
                    if (Tools::getValue('show') == 'sold') {
                        $data = $this->_API->doGetMySoldItems(
							array(
								'sort-type' => $sort_type,
								'sort-order' => $sort_order,
							),
							null,
							//Filtrowanie nie działa - dzlaczego?
							//array(
							//	'filter-format' => $filter_format,
							//),
							$search ? '*'.$search.'*' : '',
							max(0, $page-1),
							$limit
                        );
                        $auctions = $data['sold-items-list'];
                        $this->_listTotal = $data['sold-items-counter'];
                    } elseif (Tools::getValue('show') == 'notsold') {
                        $data = $this->_API->doGetMyNotSoldItems(
							array(
								'sort-type' => $sort_type,
								'sort-order' => $sort_order,
							),
							null,
							//Filtrowanie nie działa - dzlaczego?
							//array(
							//	'filter-format' => $filter_format,
							//),
							$search ? '*'.$search.'*' : '',
							max(0, $page-1),
							$limit
                        );
                        $auctions = $data['not-sold-items-list'];
                        $this->_listTotal = $data['not-sold-items-counter'];
                    } elseif (Tools::getValue('show') == 'future') {
                        $data = $this->_API->doGetMyFutureItems(
                            max(0, $page-1),
							$limit
                        );
                        $auctions = $data['future-items-list'];
                        $this->_listTotal = $data['future-items-counter'];
                    } else {
                        $data = $this->_API->doGetMySellItems(
							array(
								'sort-type' => $sort_type,
								'sort-order' => $sort_order,
							),
							null,
							//Filtrowanie nie działa - dzlaczego?
							//array(
							//	'filter-format' => $filter_format,
							//),
							$search ? '*'.$search.'*' : '',
							max(0, $page-1),
							$limit
                        );
                        $auctions = $data['sell-items-list'];
                        $this->_listTotal = $data['sell-items-counter'];
                    }
                    foreach ($auctions as $auction) {
                        $priceBuyNow = 0;
                        
                        foreach ($auction->{'item-price'} as $price) {
                            if ($price->{'price-type'} == 1) {
                                $priceBuyNow = $price->{'price-value'};
                                break;
                            }
                        }
                        
                        if (isset($auction->{'item-sold-quantity'})) {
                            $left = $auction->{'item-start-quantity'}-$auction->{'item-sold-quantity'};
                        } else {
                            $left = $auction->{'item-start-quantity'};
                        }
                        $this->_list[] = array(
                            'id_auction' => $auction->{'item-id'} . '|' . $account['id_xallegro_account'],
                            'dummy' => '',
                            'account_name' => $account['username'],
                            'service' => $account['service'],
                            'title' => $auction->{'item-title'},
                            'thumbnail_small' => $auction->{'item-thumbnail-url'},
                            'thumbnail' => str_replace('64x48', '128x96', $auction->{'item-thumbnail-url'}),
                            'id_image' => $auction->{'item-thumbnail-url'},
							'shop_info' => array(
								'src' => $auction->{'item-shop-info'} ? 'status_green.png' : 'status_red.png',
								'alt' => $auction->{'item-shop-info'} ? $this->l('oferta w sklepie Allegro') : $this->l('aukcja standardowa'),
							),
                            'quantity' => $left . '/' . $auction->{'item-start-quantity'},
                            'bids' => (isset($auction->{'item-bidders-counter'}) ? $auction->{'item-bidders-counter'} : 0),
                            'price_buy_now' => number_format($priceBuyNow, 2, '.', ''),
                            'start' => date('d.m.Y G:i', $auction->{'item-start-time'}),
                            'end' => $auction->{'item-end-time'} ? date('d.m.Y G:i', $auction->{'item-end-time'}) : null,
                            'hits' => (isset($auction->{'item-views-counter'}) ? $auction->{'item-views-counter'} : null),
							'closed' => (!isset($auction->{'item-end-time-left'}) || $auction->{'item-end-time-left'} == 'Zakończona')
								? array(
									'src' => 'error.png',
									'alt' => $this->l('Aukcja została zakończona.'),
								) 
								: null,
                        );
                    }
                } catch (Exception $ex) {
					$this->errors[] = $ex->getMessage();
                }
            }
        }
        
        public function initProcess() {
			if(Tools::getIsset('delete_link')) {
				XAllegroAuction::unlinkByIdAuction((float)Tools::getValue('id_auction'));
				Tools::redirectAdmin(self::$currentIndex . '&token=' . $this->token . '&conf=1');
			}
			if(Tools::getValue('action')) {
				$method = 'process'.Tools::toCamelCase(Tools::getValue('action'), 1);
				if(method_exists($this, $method)) {
					$this->$method();
				}
			}
			parent::initProcess();
		}
        
        public function initContent()
        {
			//komunikaty
			if(isset($this->context->cookie->xallegro_informations)) {
				$this->informations = array_merge($this->informations, unserialize($this->context->cookie->xallegro_informations));
				unset($this->context->cookie->xallegro_informations);
			}
			if(isset($this->context->cookie->xallegro_warnings)) {
				$this->warnings = array_merge($this->warnings, unserialize($this->context->cookie->xallegro_warnings));
				unset($this->context->cookie->xallegro_warnings);
			}
			if(isset($this->context->cookie->xallegro_errors)) {
				$this->errors = array_merge($this->errors, unserialize($this->context->cookie->xallegro_errors));
				unset($this->context->cookie->xallegro_errors);
			}
			
//            Jeśli jest ID aukcji oraz konta to włączamy edytor
            if (Tools::getValue('id_auction') && Tools::getValue('id_allegro_account')) {
                $this->display = 'edit';
            }
            
            if(Tools::getValue('xallegro_auction_pagination')) {
				$this->context->cookie->xallegro_auction_pagination = (int)Tools::getValue('xallegro_auction_pagination');
			}
			else{
				if(!isset($this->context->cookie->xallegro_auction_pagination)) $this->context->cookie->xallegro_auction_pagination = 20;
			}
			if(Tools::getValue('submitFilterxallegro_auction')) {
				$this->context->cookie->submitFilterxallegro_auction = (int)Tools::getValue('submitFilterxallegro_auction');
			}
			else {
				$this->context->cookie->submitFilterxallegro_auction = 1;
			}
            
            return parent::initContent();
        }
        
        public function processSave()
        {
//            Jeśli relacja istnieje w bazie, pomijamy
            if ($this->object->id) {
                return false;
            }
            
//            Walidacja modelu
            $this->validateRules('XAllegroAuction');
            if (count($this->errors))
            {
                $this->display = 'edit';
                return false;
            }
            
            $quantity       = $this->object->quantity;
            $quantity_start = $this->object->quantity_start;
            $closed         = $this->object->closed;
            
            $this->object = new XAllegroAuction();
            $this->object->id_shop = Context::getContext()->shop->id;
            $this->object->id_shop_group = Context::getContext()->shop->id_shop_group;
            $this->object->id_auction = Tools::getValue('id_auction');
            $this->object->id_allegro_account = (int)Tools::getValue('id_allegro_account');
            $this->object->id_product = (int)Tools::getValue('id_product');
            $this->object->id_product_attribute = (int)Tools::getValue('id_product_attribute');
            $this->object->resume_auction = (int)Tools::getValue('resume_auction');
            $this->object->quantity = $quantity;
            $this->object->quantity_start = $quantity_start;
            $this->object->closed = $closed;
            $this->object->add();
            
            $this->redirect_after = self::$currentIndex . '&conf=3&token=' . $this->token . '&show=' . Tools::getValue('show');
        
            return $this->object;
        }

		public function processFinishAuction() {
			if(Tools::getValue('item_id')) {
				if(Tools::getValue('cancel_all_bids') && Tools::getValue('cancel_reason') == '') {
					$this->errors[$this->l('Wybierając opcję odwołania ofert musisz podać przyczynę odwołania.')];
				}
				else {
					$items = Tools::getValue('item_id');
					foreach($items as $i) {
						$finish_items_list[] = array(
							'finish-item-id' => (float)$i,
							'finish-cancel-all-bids' => Tools::getValue('cancel_all_bids'),
							'finish-cancel-reason' => Tools::getValue('cancel_reason'),
						);
					}
					//Pobranie konta
					$account = Db::getInstance()->getRow('SELECT a.*, IF(a.sandbox, CONCAT(s.url, \'.webapisandbox.pl\'), s.url) as service '
						. 'FROM ' . _DB_PREFIX_ . 'xallegro_account AS a '
						. 'LEFT JOIN ' . _DB_PREFIX_ . 'xallegro_site AS s '
						. 'ON (a.country_code = s.country_code)'
						. 'WHERE a.id_xallegro_account = ' . (int)Tools::getValue('id_allegro_account'));
					if (!$account) {
						$this->errors[] = $this->l('Niepoprawne parametry wywołania');
						return;
					}
					$this->_API = new XAllegroAPI($account);
					$data = $this->_API->doFinishItems($finish_items_list);
					foreach($data['finish-items-succeed'] as $ok) {
						$data_ok[] = $ok;
					}
					if(isset($data_ok)) {
						XAllegroAuction::closeAuctions($data_ok);
						$this->informations[] = $this->l('Zakończone aukcje: ').' '.implode(', ', $data_ok);
					}
					foreach($data['finish-items-failed'] as $err) {
						$this->warnings[] = '<strong>'.$err->{'finish-item-id'}.': </strong>'.$err->{'finish-error-message'};
					}
				}
			}
			else $this->errors[$this->l('Nie wybrano żadnych przedmiotów.')];
		}
		
		public function processRedoAuction() {
			
			$collection = new Collection('XAllegroAuction');
			$collection->where('id_auction', 'in', Tools::getValue('item_id'));
			$collection->where('closed', '=', 1);
			$auctions_src = $collection->getResults();
			
			$all_new_auctions_ids = array();
			foreach($auctions_src as &$auction) {
				$id_auction = $auction->id_auction;
				$auction->id = null;
				$auction->id_auction = 0;
				$auction_item_temp = json_decode($auction->item);
				$auction_item_temp->duration = $auction->allegro_shop ? (int)Tools::getValue('duration_shop') : (int)Tools::getValue('duration');
				$auction->item = json_encode($auction_item_temp);
				if(trim(Tools::getValue('date_start')) == '') {
					$auction->start = 0;
					$auction->start_time = '';
				}
				else {
					$auction->start = 1;
					$auction->start_time = Tools::getValue('date_start');
				}
				$auction->closed = 0;
				$auction->add();
				$auctions[$auction->allegro_shop ? 1 : 0][(float)$auction->id] = (float)$id_auction;
				$all_new_auctions_ids[] = (float)$auction->id;
			}
			
			if(isset($auctions)) {
				
				//Pobranie konta
				$account = Db::getInstance()->getRow('SELECT a.*, IF(a.sandbox, CONCAT(s.url, \'.webapisandbox.pl\'), s.url) as service '
					. 'FROM ' . _DB_PREFIX_ . 'xallegro_account AS a '
					. 'LEFT JOIN ' . _DB_PREFIX_ . 'xallegro_site AS s '
					. 'ON (a.country_code = s.country_code)'
					. 'WHERE a.id_xallegro_account = ' . (float)Tools::getValue('id_allegro_account'));
				if (!$account) {
					$this->errors[] = $this->l('Niepoprawne parametry wywołania');
					return;
				}
				
				$this->_API = new XAllegroAPI($account);
				
				if(isset($auctions[0])) {
					//przygotowuję i wysyłam zapytanie dla aukcji wystawionych standardowo
					$redo_items_list = array(
						'sell-items-array' => array_values($auctions[0]),
						'sell-starting-time' => (trim(Tools::getValue('date_start')) == '') ? 0 : strtotime(Tools::getValue('date_start')),
						'sell-auction-duration' => (int)Tools::getValue('duration'),
						'sell-option' => 2,
						'local-ids' => array_keys($auctions[0]),
					);
					$data[0] = $this->_API->doSellSomeAgain($redo_items_list);
				}
				
				if(isset($auctions[1])) {
					//przygotowuję i wysylam zapytanie dla aukcji wystawionych ze sklepu
					$redo_items_list = array(
						'sell-items-array' => array_values($auctions[1]),
						'sell-starting-time' => (trim(Tools::getValue('date_start')) == '') ? 0 : strtotime(Tools::getValue('date_start')),
						'sell-shop-duration' => (int)Tools::getValue('duration_shop'),
						'sell-shop-options' => 2,
						'sell-prolong-options' => 0,
						'sell-shop-category' => 0,
						'local-ids' => array_keys($auctions[1]),
					);
					$data[1] = $this->_API->doSellSomeAgainInShop($redo_items_list);
				}
				//przetwarzam wyniki
				$local_ids = array();
				foreach($data as $d) {
					foreach($d['items-sell-again'] as $ok) {
						$data_ok[] = $this->l('Wystawiono ponownie aukcję #').' <b>'.$ok->{'sell-item-id'}.'</b>. '.$ok->{'sell-item-info'};
						$local_ids[] = $ok->{'sell-item-local-id'};
					}
					$this->informations[] = implode('<br />', $data_ok);
					foreach($d['items-sell-failed'] as $warn) {
						$this->warnings[] = '<strong>'.$warn->{'sell-item-id'}.': </strong>'.$warn->{'sell-fault-string'};
					}
					foreach($d['items-sell-not-found'] as $error) {
						$data_error[] = $error;
					}
					if(isset($data_error)) {
						$this->errors[] = $this->l('Nie odnalezione oferty: ').'<strong>'.implode(', ', $data_error).'</strong>';
					}
					//usuwam z bazy aukcje, które nie zostały prawidłowo wystawione
				}
				
				if(!empty($this->informations)) $this->context->cookie->xallegro_informations = serialize($this->informations);
				if(!empty($this->warnings)) $this->context->cookie->xallegro_warnings = serialize($this->warnings);
				if(!empty($errors->informations)) $this->context->cookie->xallegro_errors = serialize($this->errors);
				
				$to_delete = array_diff($all_new_auctions_ids, $local_ids);
				XAllegroAuction::deleteById($to_delete);
			}
			//Przekierowanie, żeby przy odświażaniu nie wystawiać ponownie tych samych aukcji
			Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getValue('token').'&show='.Tools::getValue('show').'&id_account='.Tools::getValue('id_account')).'&conf=120';
			exit;
		}
		
        public function renderForm()
        {
            $this->fields_form = array(
                'legend' => array(
                    'title' => $this->l('Przypisz aukcję do produktu'),       
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_auction'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_allegro_account'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'service'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_product',
                    //  'label' => $this->l('Id produktu'),
                    //  'size' => 70,
                    //  'desc' => $this->l('Podaj id produktu lub skorzystaj z pola poniżej.'),
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'show'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa produktu'),
                        'name' => 'name',
                        'size' => 70,
                        'class' => 'custom_ac_input',
                        'desc' => (Validate::isLoadedObject($this->object) ? false : $this->l('Zacznij wpisywać pierwsze litery nazwy produktu, następnie wybierz produkt z listy rozwijalnej')),
                        'disabled' => (Validate::isLoadedObject($this->object) ? true : null)
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Nazwa atrybutu'),
                        'name' => 'id_product_attribute',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 0,
                                    'name' => 'Brak'
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'disabled' => (Validate::isLoadedObject($this->object) ? true : null)
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Tytuł aukcji'),
                        'name' => 'title',
                        'size' => 50,
                        'disabled' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cena Kup Teraz'),
                        'name' => 'price_buy_now',
                        'size' => 7,
                        'disabled' => true,
                        'suffix' => ' zł',
                        'callback' => 'priceFormat',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Pozostała ilość przedmiotów'),
                        'name' => 'quantity',
                        'size' => 7,
                        'disabled' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Zakończona'),
                        'name' => 'closed',
                        'size' => 7
                    ),
                ),
                'submit' => array(
                    'name' => 'save',
                    'title' => $this->l('Zapisz'),
                    'class' => 'button btn btn-default pull-right'
                ),
                'buttons' => array(),
            );
            
//            Jeśli przypisany produkt usuwamy wybór atrybutu oraz button zapisz
            if (ValidateCore::isLoadedObject($this->object)) {
                unset($this->fields_form['input'][6], $this->fields_form['submit']);
            }

            return parent::renderForm();
        }

		public function ajaxProcessGetByIdAuction() {
			die(json_encode(XAllegroAuction::getByIdAuction(Tools::getValue('items'))));
		}

		public function	ajaxProcessGetProductList() {
			$query = Tools::getValue('q', false);
			if (!$query OR $query == '' OR strlen($query) < 1)
				die();
			if($pos = strpos($query, ' (ref:'))
				$query = substr($query, 0, $pos);
			$sql = '
				SELECT p.`id_product`, p.`reference`, pl.`name`
				FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
				WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.'
				GROUP BY p.id_product
			';
			$items = Db::getInstance()->executeS($sql);
			foreach ($items AS $item)
				echo 'id: '.$item['id_product'].' - '.trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";
			exit;
		}

        public function	ajaxProcessGetAttributes()
        {
            $product = new Product(Tools::getValue('id_product'));
            
            die(Tools::jsonEncode($product->getAttributesResume($this->context->language->id)));
        }
        
		public function setRedirectAfter($url) {
			$this->redirect_after = $url
				. (Tools::getIsset('id_account') ? '&id_account='.Tools::getValue('id_account') : '')
				. (Tools::getIsset('show') ? '&show='.Tools::getValue('show') : '')
			;
		}

    }
