<?php

if (!defined('_PS_VERSION_')) {
	exit;
}

require_once dirname(__FILE__) . '/../../x13allegro.php';
include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

class AdminXAllegroCarriersController extends XAllegroController {


	public function __construct() {
		//Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
		
		
		parent::__construct();
		
		if (!ValidateCore::isLoadedObject($this->_account)) {
			return;
		}
		
		try {
			$this->_API = new XAllegroAPI($this->_account);
		} catch (SoapFault $e) {
			d($e);
			$this->_APIError = $e->getMessage();
		}
	}

	public function initContent() {
	    
	    $fields_shipments_raw = $this->_API->doGetShipmentData();
	    foreach($fields_shipments_raw['shipment-data-list'] as $fsr) {
	        $fields_shipments_temp[$fsr->{'shipment-time'}->{'shipment-time-from'}][] = array('id' => $fsr->{'shipment-id'}, 'name' => $fsr->{'shipment-name'});
	    }
	    ksort($fields_shipments_temp);
	    $fields_shipments = array();
	    foreach($fields_shipments_temp as $temp) {
			$fields_shipments = array_merge($fields_shipments, $temp);
		}
	    unset($fields_shipments_raw);
		$this->context->smarty->assign(array(
			'fields_shipments' => $fields_shipments,
			'carriers' => Carrier::getCarriers($this->context->cookie->id_lang, true, false, false, null, null),
		    'assign' => XAllegroCarrier::getAssignations(),
			'form_action'  => self::$currentIndex.'&token='.$this->token,
			'action'       => 'assign_carriers',
		));
		
		if($this->ps_version == 1.5) {
			if(Tools::getValue('action') == 'assign_carriers') {
				$this->processAssignCarriers();
				Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&conf=4');
			}
		}
		
		parent::initContent();
	}

	public function initToolbarTitle() {
		parent::initToolbarTitle();
		$this->toolbar_title[] = $this->l('Przypisywanie przewoźników');
	}

	public function processAssignCarriers() {
		if(Tools::getValue('id_fields_shipment')) {
			if(XAllegroCarrier::updateCarriersAsignations(Tools::getValue('id_fields_shipment'))) {
				$this->confirmations[] = $this->l('Przypisano przewoźników.');
			}
		}
		else {
			$this->errors[] = $this->l('Nie przesłano przewożników');
		}
	}

}

?>
