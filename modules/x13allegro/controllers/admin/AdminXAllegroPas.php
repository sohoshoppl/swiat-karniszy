<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';
    
    class AdminXAllegroPasController extends XAllegroController
    {
        protected $_API;
        protected $_shipments;
        
        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
            $this->table = 'xallegro_pas';
            $this->className = 'XAllegroPas';
            $this->lang = false;
            
            $this->addRowAction('edit');
			$this->addRowAction('XAllegroDuplicate');
            $this->addRowAction('delete');
            $this->bulk_actions = array('delete' => array('text' => $this->l('Usuń zaznaczone'), 'confirm' => $this->l('Usunąć zaznaczone elementy?')));
            
            $this->fields_list = array(
                'id_xallegro_pas' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'width' => 20
                ),
                'name' => array(
                    'title' => $this->l('Nazwa profilu')
                ),
                'default' => array(
                    'title' => $this->l('Domyślny'),
                    'active' => 'default',
                    'width' => 100,
                    'align' => 'center',
                    'type' => 'bool',
                    'class' => 'fixed-width-sm',
                ),
                'active' => array(
                    'title' => $this->l('Aktywny'),
                    'active' => 'active',
                    'width' => 100,
                    'align' => 'center',
                    'type' => 'bool',
                    'class' => 'fixed-width-sm',
                )
            );
            
            parent::__construct();
            
            if (!ValidateCore::isLoadedObject($this->_account)) {
                return;
            }
            
            try {
                $this->_API = new XAllegroAPI($this->_account);
                $this->_shipments = $this->_API->getShipments();
            } catch (SoapFault $e) {
                $this->_APIError = $e->getMessage();
            }
        }
        
        public function postProcess()
        {
            parent::postProcess();
            
			if((int)Tools::getValue('default') == 1) {
				$this->object->setDefault();
			}
			elseif(Tools::getIsset('duplicatexallegro_pas')) {
				$obj = new XAllegroPas(Tools::getValue('id_xallegro_pas'));
				$obj->id = null;
				$obj->id_xallegro_pas = null;
				$obj->shipments = serialize($obj->shipments);
				$obj->name .= ' - duplikat';
				$obj->default = 0;
				$obj->active = 0;
				$obj->add();
				Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getValue('token').'&conf=3');
			}
            
        }
        
        public function renderList()
        {
            return parent::renderList();
        }

        public function renderForm()
        {
            $this->fields_form = array(
                'legend' => array(
                    'title' => $this->l('Płatność i dostawa'),       
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_' . $this->table
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa profilu'),
                        'name' => 'name',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Województwo'),
                        'name' => 'state',
                        'required' => true,
                        'options' => array(
                            'query' => $this->_API->getStates(),
                            'id' => 'value',
                            'name' => 'label'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Miasto'),
                        'name' => 'city',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kod pocztowy'),
                        'name' => 'zipcode',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Koszt przesyłki pokrywa'),
                        'name' => 'shipping_cost',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Kupujący')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Sprzedający')
                            )
                        ),
                        'default_value' => 1
                    ),
                    /*
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Odbiór osobisty'),
                        'name' => 'customer_pickup',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Wysyłka towaru e-mailem'),
                        'name' => 'shipment_email',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    */
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Wystawiam fakturę VAT'),
                        'name' => 'invoice',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Wysyłka za granicę'),
                        'name' => 'abroad',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Szczegóły dotyczące wysyłki'),
                        'name' => 'details',
                        'rows' => 5,
                        'cols' => 50
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Płatność przelewem'),
                        'name' => 'bank_transfer',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Numer konta bankowego'),
                        'name' => 'account_number',
                        'size' => 53,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Numer drugiego konta bankowego'),
                        'name' => 'account_number_second',
                        'size' => 53,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Wysyłka w ciągu'),
                        'name' => 'prepare_time',
                        'options' => array(
                            'query' => $this->_API->getDeliveryPrepareTime(),
                            'id' => 'value',
                            'name' => 'label'
                        )
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'shipment'
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Domyślny profil'),
                        'name' => 'default',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Aktywny profil'),
                        'name' => 'active',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 1,
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 0,
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    )
                ),
                'shipments' => $this->_shipments,
                'submit' => array(
                    'name' => 'save',
                    'title' => $this->l('Zapisz'),
                    'class' => 'btn btn-default pull-right'
                )
            );

            return parent::renderForm();
        }
        
        public function getFieldsValue($obj)
        {
            $this->fields_value = parent::getFieldsValue($obj);
            
            $this->fields_value['shipments'] = array();
            
            $postValues = Tools::getValue('shipments');
            
            foreach ($this->_shipments as $shipment) {
                if (isset($this->fields_value[$shipment['id']])) {
                    continue;
                }
                
				//Darmowe opcje dostawy
				if($shipment['id'] == 35) {
					$sum_val = 0;
					if (isset($postValues[35])) {
						foreach($postValues[35] as $pV) {
							$sum_val += (int)$pV;
						}
					} elseif (Validate::isLoadedObject($this->object) && isset($this->object->shipments[35])) {
						$sum_val = $this->object->shipments[35];
					}
					$this->fields_value['shipments'][35] = $sum_val;
					continue;
				}
                
                if (isset($postValues[$shipment['id']]['enabled'])) {
                    $this->fields_value['shipments'][$shipment['id']]['enabled'] = $postValues[$shipment['id']]['enabled'];
                } elseif (Validate::isLoadedObject($this->object) && isset($this->object->shipments[$shipment['id']]['enabled'])) {
                        $this->fields_value['shipments'][$shipment['id']]['enabled'] = $this->object->shipments[$shipment['id']]['enabled'];
                } else {
                    $this->fields_value['shipments'][$shipment['id']]['enabled'] = null;
                }
                
                for ($i=0; $i<2; $i++) {
					
                    if (isset($postValues[$shipment['id']][$i])) {
                        $this->fields_value['shipments'][$shipment['id']][$i] = $postValues[$shipment['id']][$i];
                    } elseif (Validate::isLoadedObject($this->object) && isset($this->object->shipments[$shipment['id']][$i])) {
                        $this->fields_value['shipments'][$shipment['id']][$i] = $this->object->shipments[$shipment['id']][$i];
                    } else {
                        $this->fields_value['shipments'][$shipment['id']][$i] = 0;
                    }
                }
                
                if (isset($postValues[$shipment['id']][2])) {
                    $this->fields_value['shipments'][$shipment['id']][2] = $postValues[$shipment['id']][$i];
                } elseif (Validate::isLoadedObject($this->object) && isset($this->object->shipments[$shipment['id']][2])) {
                        $this->fields_value['shipments'][$shipment['id']][2] = $this->object->shipments[$shipment['id']][2];
                } else {
                    $this->fields_value['shipments'][$shipment['id']][2] = 1;
                }
            }
            
            return $this->fields_value;
        }
        
        protected function beforeAdd($object)
        {
			$schipments = Tools::getValue('shipments');
			$sum_val = 0;
			if(isset($schipments[35])) {
				foreach($schipments[35] as $s) $sum_val += (int)$s;
				$schipments[35] = $sum_val;
			}
            $object->shipments = serialize($schipments);
        }
        
        protected function afterUpdate($object)
        {
            $schipments = Tools::getValue('shipments');
			$sum_val = 0;
			if(isset($schipments[35])) {
				foreach($schipments[35] as $s) $sum_val += (int)$s;
				$schipments[35] = $sum_val;
			}
            $object->shipments = serialize($schipments);
            $object->save();
        }
		
		public function displayXAllegroDuplicateLink($token = null, $id, $name = null) {
			$href = $this->context->link->getAdminLink('AdminXAllegroPas') . '&duplicatexallegro_pas&id_xallegro_pas=' . $id;
			return 
			'<a href="' . $href . '" title="' . $this->l('Skopiuj') . '">
				'.($this->bootstrap ? '<i class="icon-copy"></i> '.$this->l('Skopiuj') : '<img src="../img/admin/duplicate.png" alt="' . $this->l('Skopiuj') . '" />').'
			</a>';
		}
		
    }
