<?php

if (! defined('_PS_VERSION_')) {
    exit();
}

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once dirname(__FILE__) . '/../../x13allegro.php';
include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

class AdminXAllegroMainController extends XAllegroController
{

    protected $_idLang;

    protected $_API;

    protected $_allegroAccount;

    protected $_allegroAccounts;

    protected $_allegroCategoryId = false;

    protected $_allegroCategoryPath = array();

    protected $_allegroCategoryFields = array();

    protected $_allegroCategoryFieldsDefault = array();

    protected $_idProductsCategories = array();

    protected $_products = array();

    protected $_durations = array();

    protected $_pas = array();

    protected $_pasDefault = false;

    protected $_shipments = array();

    public function __construct()
    {
	    
	    //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	    
        parent::__construct();
        
        $this->_conf[1] = $this->l('Produkty zostały wystawione i w ciągu maksymalnie 60 minut pojawią się na liście aukcji.');
        
        if (! Validate::isLoadedObject($this->_account)) {
            return;
        }
        
        $this->_idLang = $this->context->language->id;
        $this->_idShop = $this->context->shop->id;
        $this->context->controller->addJS(array(
            _PS_JS_DIR_ . 'jquery/jquery-1.11.0.min.js',
            _PS_JS_DIR_ . 'tiny_mce/tiny_mce.js',
            ($this->ps_version == 1.6) ? dirname(__FILE__) . '/../../js/tinymce.inc.js' : _PS_JS_DIR_ . 'tinymce.inc.js'
        ));
        
        // Get allegro accounts
        $collection = new Collection('XAllegroAccount');
        $collection->where('active', '=', 1);
        $collection->orderBy('default', 'desc');
        $this->_allegroAccounts = $collection->getResults();
        
        if (! count($this->_allegroAccounts)) {
            $this->errors[] = $this->l('Brak utworzonych kont Allegro.');
            return;
        }
        
        // Get templates
        $collection = new Collection('XAllegroTemplate');
        $collection->orderBy('default', 'desc');
        $this->templates = $collection->getResults();
        
        if (! count($this->templates)) {
            $this->errors[] = $this->l('Brak utworzonych szablonów.');
        }
        
        // Select account
        $this->_setAccount(Tools::getValue('allegro_account'));
        
        if ($this->_API === NULL) {
			if(Tools::getIsset('ajax')) {
				die(implode(' ', $this->errors));
			}
			else {
				Tools::redirectAdmin('index.php?controller=AdminXAllegroAccounts&token=' . Tools::getAdminTokenLite('AdminXAllegroAccounts') . '&wrong_authentication_data=' . implode(' ', $this->errors));
			}
        }
        
        $this->_initProducts();
        
        // Set category from post
        $this->_setCategory(Tools::getValue('id_allegro_category'));
        
        // Get duration
        $this->_durations = $this->_API->getDurations($this->_allegroAccount->shop);
        
        $collection = new Collection('XAllegroPas');
        $collection->where('active', '=', 1);
        $collection->orderBy('default', 'desc');
        $this->_pas = $collection->getResults();
        if ($collection->getFirst() && $collection->getFirst()->default) {
            $this->_pasDefault = $collection->getFirst();
        }
        
        $this->_shipments = $this->_API->getShipments();
        
        // Dodanie pól formularzy
        $this->_fieldsForm();
        
        // Sprawdzenie aktualności drzewa kategorii i pól formularza
        
        $needs_update = array(
            'current_VERSION_CATEGORY_LIST' => XAllegroConfiguration::get('VERSION_CATEGORY_LIST'),
            'current_VERSION_SELL_FORM_FIELDS' => XAllegroConfiguration::get('VERSION_SELL_FORM_FIELDS'),
            'newest_VERSION_CATEGORY_LIST' => null,
            'newest_VERSION_SELL_FORM_FIELDS' => null
        );
        try {
            // Sprawdzanie wersji drzewa kategorii i pól formularza sprzedaży
            $update_timestamp = true;
            if (time() - (float) XAllegroConfiguration::get('VERSION_TIMESTAMP') > 3600) {
                $sysStats = $this->_API->doQueryAllSysStatus();
                foreach ($sysStats as $sS) {
                    if ($sS->{'country-id'} == 1) {
                        if (XAllegroConfiguration::get('VERSION_CATEGORY_LIST') != $sS->{'cats-version'}) {
                            $needs_update['newest_VERSION_CATEGORY_LIST'] = $sS->{'cats-version'};
                            $update_timestamp = false;
                        }
                        if (XAllegroConfiguration::get('VERSION_SELL_FORM_FIELDS') != $sS->{'form-sell-version'}) {
                            $needs_update['newest_VERSION_SELL_FORM_FIELDS'] = $sS->{'form-sell-version'};
                            $update_timestamp = false;
                        }
                    }
                }
                if ($update_timestamp) {
                    XAllegroConfiguration::updateValue('VERSION_TIMESTAMP', time());
                }
            }
        } catch (Exception $e) {
            $message = 'ERROR ALLEGRO: ' . $e->getMessage();
            die($message);
        }
        $this->context->smarty->assign($needs_update);
        
    }

	public static function getAllegroUpdateStatus() {
		$result = file_get_contents(dirname(__FILE__).'/json.tmp');
		$json = json_decode($result, true);
		if(!$json['code'] == 'END') {
			unlink(dirname(__FILE__).'/json.tmp');
		}
		die($result);
	}
	
	public static function setAllegroUpdateStatus($status) {
		file_put_contents(dirname(__FILE__).'/json.tmp', json_encode($status));
	}

	public function ajaxProcessGetAllegroUpdateStatus() {
		$this->getAllegroUpdateStatus();
	}

    public function ajaxProcessDownloadCategoryTree()
    {
        try {
			AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'DOWNLOADING', 'progress' => '0', 'message' => $this->l('Pobieranie danych o kategoriach z Allegro.')));
            $categoriesArray = $this->_API->doGetCatsData();
            XAllegroCategory::insertCategories($categoriesArray, $this->_allegroAccount->country_code);
            AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'COMPLETE_CAT', 'progress' => '100', 'message' => $this->l('Przetworzono wszystkie kategorie.')));
            XAllegroConfiguration::updateValue('VERSION_CATEGORY_LIST', Tools::getValue('version'));
        }
        catch(Exception $e) {
			AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'ERROR', 'progress' => '0', 'message' => $e->getMessage()));
        }
    }

    public function ajaxProcessDownloadFormFields()
    {
        try {
			AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'DOWNLOADING', 'progress' => '0', 'message' => $this->l('Pobieranie danych o formularzach z Allegro.')));
            $formsArray = $this->_API->doGetSellFormFieldsExt();
            XAllegroCategory::insertForms($formsArray, $this->_allegroAccount->country_code);
            XAllegroConfiguration::updateValue('VERSION_SELL_FORM_FIELDS', Tools::getValue('version'));
            AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'COMPLETE_FIELDS', 'progress' => '100', 'message' => $this->l('Przetworzono wszystkie pola formularzy.')));
        }
        catch(Exception $e) {
            AdminXAllegroMainController::setAllegroUpdateStatus(array('code' => 'ERROR', 'progress' => '0', 'message' => $e->getMessage()));
        }
    }

    public function _setAccount($id_xallegro_account)
    {
        if ($id_xallegro_account) {
            $collection = new Collection('XAllegroAccount');
            $collection->where('id_xallegro_account', '=', (int) $id_xallegro_account);
            $this->_allegroAccount = $collection->getFirst();
        } else {
            $this->_allegroAccount = current($this->_allegroAccounts);
        }

        for ($crashes = 0; $crashes < 3; $crashes++) {
            try {
                $this->_API = new XAllegroAPI($this->_allegroAccount);
                break;

            } catch (SoapFault $e) {
                if ($crashes < 2) {
                    sleep(1);
                } else {
                    $this->errors[] = $e->getMessage();
                }
            }
        }
    }

    public function _setCategory($allegroCategoryId)
    {
        // If isset category path array
        if ($allegroCategoryId) {
            // If select any category
            if (! is_array($allegroCategoryId)) {
                $this->_allegroCategoryId = (int) $allegroCategoryId;
            } else 
                if (current($allegroCategoryId) != 0 && end($allegroCategoryId) == 0) {
                    $this->_allegroCategoryId = (int) $allegroCategoryId[count($allegroCategoryId) - 2];
                } else {
                    $this->_allegroCategoryId = (int) end($allegroCategoryId);
                }
        } else {
            // Try get from categories map
            if (count($this->_idProductsCategories) == 1) {
                $collection = new Collection('XAllegroCategory');
                $collection->where('id_category', '=', current($this->_idProductsCategories));
                $allegroCategory = $collection->getFirst();
                if ($allegroCategory) {
                    $this->_allegroCategoryId = $allegroCategory->id_allegro_category;
                }
            }
        }
        
        // If is selected any category
        if ($this->_allegroCategoryId) {
            $categoryPath = $this->_API->getCategoryPath($this->_allegroCategoryId);
            foreach ($categoryPath as $path) {
                $this->_allegroCategoryPath[] = $path['id'];
            }
            
            // Check is last node
            if ($this->_API->categoryIsLastNode($this->_allegroCategoryId)) {
                
                // Get all fields for category
                $this->_allegroCategoryFields = $this->_API->getCategoryFields($this->_allegroCategoryId);
                
                // Try get category fields default values
                $collection = new Collection('XAllegroCategory');
                $collection->where('id_allegro_category', '=', $this->_allegroCategoryId);
                $allegroCategory = $collection->getFirst();
                if ($allegroCategory) {
                    $this->_allegroCategoryFieldsDefault = unserialize($allegroCategory->fields);
                }
            } else {
                // Add last category to select
                $this->_allegroCategoryPath[] = 0;
            }
        } else {
            $this->_allegroCategoryPath[] = 0;
        }
    }

    public function _initProducts()
    {
        $idProducts = array_map('intval', explode(',', Tools::getValue('id_product')));
        $this->_idProductsCategories = array();
        
        foreach ($idProducts as $idProduct) {
			$product = new Product($idProduct, false, $this->_idLang, $this->_idShop);
            // If product not exist
            if (! Validate::isLoadedObject($product)) {
                continue;
            }
            
            // If product default category exist, and not exist in list
            if ($product->id_category_default && ! in_array($product->id_category_default, $this->_idProductsCategories)) {
                $this->_idProductsCategories[] = $product->id_category_default;
            }
            
            // Default product data
            $data = array(
                'enabled' => XAllegroConfiguration::get('SELECT_ALL'),
                'category_fields_enabled' => false,
                'id' => $product->id,
                'id_attribute' => 0,
                'reference' => $product->reference,
                'ean13' => $product->ean13,
                'weight' => $product->weight,
                'name' => $product->name,
                'manufacturer_name' => Manufacturer::getNameById($product->id_manufacturer),
                'name_attribute' => false,
                'description' => $this->prepareDescription($product->description, $product->name),
                'link_rewrite' => $product->link_rewrite,
                'images' => $product->getImages($this->_idLang),
                'image_main' => false,
                'features' => $product->getFrontFeatures($this->_idLang),
                'price_asking' => '',
                'price_minimal' => '',
                'duration' => XAllegroConfiguration::get('DURATION_DEFAULT'),
                'template' => '',
                'additional' => array(),
                'quantity_check' => (int) XAllegroConfiguration::get('QUANITY_CHECK')
            );
            
            // Generate products by combinations main product
            $combinations = $product->getAttributesResume($this->_idLang);
            
            if ($combinations) {
                foreach ($combinations as $combination) {
                    $descriptionCustom = '';
                    
                    $collection = new Collection('XAllegroProduct');
                    $collection->where('id_product', '=', $product->id);
                    $collection->where('id_product_attribute', '=', $combination['id_product_attribute']);
                    
                    if (Validate::isLoadedObject($collection->getFirst())) {
                        $descriptionCustom = $collection->getFirst()->description;
                    }
                    
                    $attributeDesignation = array();
                    $tmpArray = explode(', ', $combination['attribute_designation']);
                    
                    foreach ($tmpArray as $tmp) {
                        $tmp2 = explode(' - ', $tmp);
                        $attributeDesignation[] = $tmp2[1];
                    }
                    
                    $data = array_merge($data, array(
                        'id_attribute' => $combination['id_product_attribute'],
                        'title' => $product->name . ' - ' . implode(' ', $attributeDesignation),
                        'title_size' => $this->_API->countTitleSize($product->name . ' - ' . implode(' ', $attributeDesignation)),
                        'name_attribute' => implode(' ', $attributeDesignation),
                        'quantity' => Product::getQuantity($product->id, $combination['id_product_attribute']),
                        'price_buy_now' => Product::getPriceStatic($product->id, true, $combination['id_product_attribute'], 2),
                        'description_custom' => $descriptionCustom
                    ));
                    
                    if (! XAllegroConfiguration::get('IMAGES_COMBINATION')) {
                        $images = Image::getImages($this->_idLang, $product->id, $combination['id_product_attribute']);
                        if ($images) {
                            $data['images'] = Image::getImages($this->_idLang, $product->id, $combination['id_product_attribute']);
                        }
                    }
                    
                    $this->_products[] = $data;
                }
            } else {
                
                $descriptionCustom = '';
                $collection = new Collection('XAllegroProduct');
                $collection->where('id_product', '=', $product->id);
                $collection->where('id_product_attribute', '=', 0);
                if (Validate::isLoadedObject($collection->getFirst())) {
                    $descriptionCustom = $collection->getFirst()->description;
                }
                
                $data = array_merge($data, array(
                    'quantity' => Product::getQuantity($product->id, 0),
                    'price_buy_now' => Product::getPriceStatic($product->id, true, 0, 2),
                    'description_custom' => $descriptionCustom
                ));
                
                $this->_products[] = $data;
            }
        }
        
        foreach ($this->_products as &$product) {
            $product['title'] = $this->_renderTitle($product);
            $product['title_size'] = $this->_API->countTitleSize($product['title']);
            $product['price_buy_now_default'] = $product['price_buy_now'];
            $product['price_buy_now'] = $this->_calculatePrice($product['price_buy_now']);
            $product['disabled'] = $this->_setDisabled($product['quantity']);
            $product['quantity_stock'] = $product['quantity'];
            $product['quantity_max'] = $this->_calculateQuantityMax($product['quantity']);
            $product['quantity'] = $this->_calculateQuantity($product['quantity']);
        }
    }

    public function postProcess()
    {
        $this->addJqueryUI(array(
            'ui.core',
            'ui.datepicker',
            'ui.slider'
        ));
        $this->addjQueryPlugin(array(
            'date'
        ));
        $this->addJS(array(
            _PS_JS_DIR_ . 'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js'
        ));
        $this->addCSS(array(
            _PS_JS_DIR_ . 'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'
        ));
        
        parent::postProcess();
    }

    public function initPageHeaderToolbar()
    {
        if (! $this->bootstrap) {
            return;
        }
        
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['allegro_current'] = array(
                'href' => self::$currentIndex . '&token=' . $this->token,
                'desc' => $this->l('Symuluj wystawianie'),
                'icon' => 'process-icon-umbrella icon-umbrella',
                'class' => 'xallegro-test process-icon-test'
            );
            
            $this->page_header_toolbar_btn['allegro_sold'] = array(
                'href' => self::$currentIndex . '&token=' . $this->token . '&show=sold',
                'desc' => $this->l('Wystaw aukcje'),
                'icon' => 'process-icon-gavel icon-gavel',
                'class' => 'xallegro-perform'
            );
        }
        
        parent::initPageHeaderToolbar();
    }

    public function initContent()
    {
		
		XAllegroConfiguration::updateValue('INPOST_LETTER', 7.20);
		XAllegroConfiguration::updateValue('INPOST_MACHINE', 8.60);
		XAllegroConfiguration::updateValue('INPOST_MACHINE_COD', 12.10);
		XAllegroConfiguration::updateValue('INPOST_CARRIER', 12.18);
		XAllegroConfiguration::updateValue('INPOST_CARRIER_COD', 15.68);
		
        // If is ajax request not render content
        if ($this->ajax) {
            return;
        }
        
        if (parent::initContent() === false || ! count($this->templates)) {
            return;
        }
        
        $iso = $this->context->language->iso_code;
        $iso = file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en';
        
        $collection = new Collection('XAllegroOverlay');
        $collection->where('active', '=', 1);
        $collection->orderBy('default', 'desc');
        $overlays = $collection->getResults();
        
        $this->context->smarty->assign(array(
            'products' => $this->_products,
            'durations' => $this->_durations,
            'templates' => $this->templates,
            'overlays' => $overlays,
            'categories' => $this->_API->getCategories(),
            'category_path' => $this->_allegroCategoryPath,
            'category_fields' => $this->_getCategoryFields(),
            'category_fields_values' => $this->_getCategoryFieldsValues(),
            'fields_value' => $this->getFieldsValue(),
            'select_images' => XAllegroConfiguration::get('SELECT_IMAGES'),
            // Dla JS
            'ad' => dirname($_SERVER['PHP_SELF']),
            'iso' => $iso,
            'shipments' => $this->_shipments,
            'url_post' => self::$currentIndex . '&token=' . $this->token,
            'fields_form' => $this->fields_form,
            'show_toolbar' => true,
            'toolbar_title' => array(
                'Allegro',
                'Wystaw aukcje'
            ),
            'toolbar_scroll' => true,
            'toolbar_btn' => array(
                array(
                    'imgclass' => $this->bootstrap ? 'process-icon-exchange icon-exchange test xallegro' : 'test xallegro',
                    'href' => '#',
                    'desc' => $this->l('Symuluj wystawienie aukcji')
                ),
                array(
                    'imgclass' => $this->bootstrap ? 'process-icon-legal icon-legal save xallegro' : 'perform xallegro',
                    'href' => '#',
                    'desc' => $this->l('Wystaw aukcje')
                )
            )
        ));
    }

    public function getFieldsValue($obj = null)
    {
        $this->fields_value = array();
        
        foreach ($this->fields_form as $fieldset => $inputs) {
            foreach ($inputs as $input) {
                $fieldValue = Tools::getValue($input['name'], null);
                if ($fieldValue === null && isset($input['default_value'])) {
                    $fieldValue = $input['default_value'];
                }
                $this->fields_value[$input['name']] = $fieldValue;
            }
        }
        
        // Get fields value for products
        $item = Tools::getValue('item');
        foreach ($this->_products as $index => $product) {
            if (! isset($this->fields_value['item'][$index])) {
                foreach ($product as $key => $value) {
                    if (! isset($item[$index][$key])) {
                        $field_value = $value;
                    } elseif (isset($item[$index][$key])) {
                        $field_value = $item[$index][$key];
                    } else {
                        $field_value = false;
                    }
                    $this->fields_value['item'][$index][$key] = $field_value;
                }
            }
        }
        
        // Get fields for shipments
        $shipments = Tools::getValue('shipment');
        foreach ($this->_shipments as $shipment) {
            // Darmowe opcje dostawy
            if ($shipment['id'] == 35) {
                $this->fields_value['shipment'][$shipment['id']] = 0;
                if (isset($shipments[$shipment['id']]))
                    $this->fields_value['shipment'][$shipment['id']] = $shipments[$shipment['id']];
                if ($this->fields_value['shipment'][$shipment['id']] == 0 && isset($this->_pasDefault->shipments[$shipment['id']]))
                    $this->fields_value['shipment'][$shipment['id']] = $this->_pasDefault->shipments[$shipment['id']];
                continue;
            }
            
            $this->fields_value['shipment'][$shipment['id']] = array();
            
            $fieldValueArray = isset($shipments[$shipment['id']]) ? $shipments[$shipment['id']] : array();
            
            if (isset($fieldValueArray['enabled'])) {
                $fieldEnabled = $fieldValueArray['enabled'];
            } elseif (isset($this->_pasDefault->shipments[$shipment['id']]['enabled'])) {
                $fieldEnabled = $this->_pasDefault->shipments[$shipment['id']]['enabled'];
            } else {
                $fieldEnabled = null;
            }
            $this->fields_value['shipment'][$shipment['id']]['enabled'] = $fieldEnabled;
            
            for ($i = 0; $i < 3; $i ++) {
                if (isset($fieldValueArray[$i])) {
                    $fieldValue = $fieldValueArray[$i];
                } elseif (isset($this->_pasDefault->shipments[$shipment['id']][$i])) {
                    $fieldValue = $this->_pasDefault->shipments[$shipment['id']][$i];
                } else {
                    $fieldValue = null;
                }
                $this->fields_value['shipment'][$shipment['id']][$i] = $fieldValue;
            }
        }
        
        // Others
        if (Tools::getValue('start')) {
            $this->fields_value['start'] = Tools::getValue('start');
        } else {
            $this->fields_value['start'] = null;
        }
        
        if (Tools::getValue('start_time')) {
            $this->fields_value['start_time'] = Tools::getValue('start_time');
        } else {
            $this->fields_value['start_time'] = date('d.m.Y H:i');
        }
        
        return $this->fields_value;
    }

    public function _fieldsForm()
    {
        $this->fields_form = array();
        
        $this->fields_form['allegro'] = array(
            array(
                'type' => 'select',
                'label' => $this->l('Konto Allegro'),
                'name' => 'allegro_account',
                'options' => array(
                    'query' => $this->_allegroAccounts,
                    'id' => 'id',
                    'name' => 'username'
                ),
                'default_value' => $this->_allegroAccount->id
            )
        );
        
        if ($this->_allegroAccount->shop) {
            $this->fields_form['allegro'] = array_merge($this->fields_form['allegro'], array(
                array(
                    'type' => 'radio',
                    'label' => $this->l('Sklep Allegro'),
                    'name' => 'allegro_shop',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'value' => 1,
                            'label' => $this->l('Tak')
                        ),
                        array(
                            'value' => 0,
                            'label' => $this->l('Nie')
                        )
                    ),
                    'default_value' => 1
                )
            ));
        }
        
        $this->fields_form['pas'] = array(
            array(
                'type' => 'select',
                'label' => $this->l('Profil'),
                'name' => 'pas',
                'options' => array(
                    'query' => array_merge(array(
                        array(
                            'id' => '0',
                            'name' => '-- Wybierz --'
                        )
                    ), $this->_pas),
                    'id' => 'id',
                    'name' => 'name'
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->id : false
            ),
            array(
                'type' => 'select',
                'label' => $this->l('Województwo'),
                'name' => 'state',
                'options' => array(
                    'query' => $this->_API->getStates(),
                    'id' => 'value',
                    'name' => 'label'
                ),
                'required' => true,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->state : false
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Miasto'),
                'name' => 'city',
                'size' => 28,
                'required' => true,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->city : false
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Kod pocztowy'),
                'name' => 'zipcode',
                'size' => 28,
                'required' => true,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->zipcode : false
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Koszt przesyłki pokrywa'),
                'name' => 'shipping_cost',
                'class' => 't',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => $this->l('Kupujący')
                    ),
                    array(
                        'value' => 0,
                        'label' => $this->l('Sprzedający')
                    )
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->shipping_cost : 1
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Wystawiam fakturę VAT'),
                'name' => 'invoice',
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => $this->l('Tak')
                    ),
                    array(
                        'value' => 0,
                        'label' => $this->l('Nie')
                    )
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->invoice : 1
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Wysyłka za granicę'),
                'name' => 'abroad',
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => $this->l('Tak')
                    ),
                    array(
                        'value' => 0,
                        'label' => $this->l('Nie')
                    )
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->abroad : 1
            ),
            array(
                'type' => 'textarea',
                'label' => $this->l('Szczegóły dotyczące wysyłki'),
                'name' => 'details',
                'rows' => 5,
                'cols' => 26,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->details : false
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Płatność przelewem'),
                'name' => 'bank_transfer',
                'class' => 't',
                'is_bool' => true,
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => $this->l('Tak')
                    ),
                    array(
                        'value' => 0,
                        'label' => $this->l('Nie')
                    )
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->bank_transfer : 1
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Numer konta bankowego'),
                'name' => 'account_number',
                'size' => 28,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->account_number : false
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Numer drugiego konta bankowego'),
                'name' => 'account_number_second',
                'size' => 28,
                'default_value' => $this->_pasDefault ? $this->_pasDefault->account_number_second : false
            ),
            array(
                'type' => 'select',
                'label' => $this->l('Wysyłka w ciągu'),
                'name' => 'prepare_time',
                'options' => array(
                    'query' => $this->_API->getDeliveryPrepareTime(),
                    'id' => 'value',
                    'name' => 'label'
                ),
                'default_value' => $this->_pasDefault ? $this->_pasDefault->prepare_time : false
            )
        );
    }

    protected function _getCategoryFields()
    {
        $fields = array();
        foreach ($this->_allegroCategoryFields as $categoryField) {
			//sortowanie opcji
			if(isset($categoryField['options']) && is_array($categoryField['options'])) {
				$options = array();
				foreach($categoryField['options'] as $cF) {
					$options[Tools::str2url($cF['label'])] = $cF;
				}
				ksort($options, SORT_STRING);
				$categoryField['options'] = array_values($options);
			}
            $field = array(
                'id' => $categoryField['id'],
                'type' => $categoryField['type'],
                'label' => $this->l($categoryField['label'] . ':'),
                'suffix' => $categoryField['unit'],
                'required' => $categoryField['required']
            );
            $field['options'] = array(
                'query' => $categoryField['options'],
                'id' => 'value',
                'name' => 'label'
            );
            $field['values'] = $categoryField['options'];
            $field['class'] = 't';
            $field['br'] = true;
            $fields[] = $field;
        }
        return $fields;
    }

    protected function _getCategoryFieldsValues()
    {
        $fieldsValues = array(
            'items' => array()
        );
        $postValues = Tools::getValue('category_fields');
        $items = Tools::getValue('item');
        foreach ($this->_allegroCategoryFields as $categoryField) {
            if (array_key_exists($categoryField['id'], $fieldsValues)) {
                continue;
            }
            if (isset($postValues[$categoryField['id']])) {
                $fieldsValues[$categoryField['id']] = $postValues[$categoryField['id']];
            } elseif (isset($this->_allegroCategoryFieldsDefault[$categoryField['id']])) {
                $fieldsValues[$categoryField['id']] = $this->_allegroCategoryFieldsDefault[$categoryField['id']];
				//Jeśli ustawiona jest cecha
				if(isset($this->_allegroCategoryFieldsDefault[$categoryField['id'].'_feature'])) {
					$fieldsValues[$categoryField['id']] = '::feature::';
				}
            } else {
                $fieldsValues[$categoryField['id']] = null;
            }
            foreach ($this->_products as $index => $product) {
                if (array_key_exists($categoryField['id'], $fieldsValues['items'])) {
                    continue;
                }
                
                if (isset($items[$index]['category_fields_enabled']) && $items[$index]['category_fields_enabled']) {
                    if (isset($items[$index]['category_fields'][$categoryField['id']])) {
                        $fieldsValues['items'][$index][$categoryField['id']] = $items[$index]['category_fields'][$categoryField['id']];
                    } elseif (isset($this->_allegroCategoryFieldsDefault[$categoryField['id']])) {
                        $fieldsValues['items'][$index][$categoryField['id']] = $this->_allegroCategoryFieldsDefault[$categoryField['id']];
                    } else {
                        $fieldsValues['items'][$index][$categoryField['id']] = null;
                    }
                } else {
                    $fieldsValues['items'][$index][$categoryField['id']] = $fieldsValues[$categoryField['id']];
					if(isset($this->_allegroCategoryFieldsDefault[$categoryField['id'].'_feature'])) {
						foreach($product['features'] as $f) {
							if($f['id_feature'] == $this->_allegroCategoryFieldsDefault[$categoryField['id'].'_feature']) {
								$fieldsValues['items'][$index][$categoryField['id']] = $f['value'];
								break;
							}
						}
					}
                }
            }
        }
        return $fieldsValues;
    }

    protected function _getCategoryFieldsValuesCompare($item, $categoryFields)
    {
        $fieldsValues = array(
            'items' => array()
        );
        foreach ($this->_allegroCategoryFields as $categoryField) {
            if (array_key_exists($categoryField['id'], $fieldsValues)) {
                continue;
            }
            
            //Jeśli przypisujemy cechę w allegro po cesze z Presty, to sprawdzamy czy cecha dla kategorii jest pusta.
            //Jeśli tak, a wypełniona jest cecha indywidualna - bierzemy cechę indywidualną, niezależnie od tego czy cechy indywidualne są włączone czy nie.
			if(
				isset($item['category_fields'][$categoryField['id']]) &&
				isset($categoryFields[$categoryField['id']]) &&
				$categoryFields[$categoryField['id']] == '::feature::' &&
				$item['category_fields'][$categoryField['id']]
			) {
				$fieldsValues[$categoryField['id']] = $item['category_fields'][$categoryField['id']];
            } elseif (isset($item['category_fields_enabled']) && $item['category_fields_enabled']) {
                if (isset($item['category_fields'][$categoryField['id']])) {
                    $fieldsValues[$categoryField['id']] = $item['category_fields'][$categoryField['id']];
                } else {
                    $fieldsValues[$categoryField['id']] = null;
                }
            } else {
                if (isset($categoryFields[$categoryField['id']])) {
                    $fieldsValues[$categoryField['id']] = $categoryFields[$categoryField['id']];
                } elseif (isset($this->_allegroCategoryFieldsDefault[$categoryField['id']])) {
                    $fieldsValues[$categoryField['id']] = $this->_allegroCategoryFieldsDefault[$categoryField['id']];
                } else {
                    $fieldsValues[$categoryField['id']] = null;
                }
            }
        }
        return $fieldsValues;
    }

    protected function _checkCategoryFieldsValues()
    {}

    protected function _renderCategoryFields()
    {}

    protected function _renderTemplate($item)
    {
        $template = new XAllegroTemplate($item['template']);
        $product = new Product($item['id_product'], false, $this->_idLang, $this->_idShop);
        
        if (! Validate::isLoadedObject($template) || ! Validate::isLoadedObject($product)) {
            return;
        }
        
        $template->setProduct($product);
        
        $attributereference = '';
        $attributeName = array();
        
        if ($item['id_product_attribute']) {
            $combination = $product->getAttributeCombinationsById($item['id_product_attribute'], $this->_idLang);
            $template->setProductAttribute($combination);
            foreach ($combination as $attribute) {
                $attributereference = $attribute['reference'];
                $attributeName[] = $attribute['attribute_name'];
            }
        }
        
        $variables = array(
            'auction_price' => $item['price_buy_now'],
            'auction_title' => Tools::stripslashes($item['title']),
            'auction_description' => Tools::stripslashes($item['description']),
            'images' => isset($item['images']) ? $item['images'] : array(),
            'product_name' => Tools::stripslashes($product->name),
            'product_attribute_name' => implode(' ', $attributeName),
            'product_reference' => $product->reference,
            'product_attribute_reference' => $attributereference,
            'product_ean13' => $product->ean13,
            'product_description' => $this->prepareDescription($product->description, $product->name),
            'product_description_short' => $this->prepareDescription($product->description_short),
            'product_description_custom' => Tools::stripslashes($item['description_custom']),
            'product_price' => Product::getPriceStatic($product->id, true, $item['id_product_attribute'], 2),
            'product_weight' => number_format($product->weight, 2, '.', '')
        );
        
        return $template->render($variables);
    }

    protected function _newAuction($auction, $test = false)
    {
        $errors = array();
        $item = $auction['item'];
        $product = new Product($item['id_product'], false, $this->_idLang, $this->_idShop);
        
        // Cast prices
        $item['price_buy_now'] = (float) str_replace(',', '.', $item['price_buy_now']);
        $item['price_asking'] = (float) str_replace(',', '.', $item['price_asking']);
        $item['price_minimal'] = (float) str_replace(',', '.', $item['price_minimal']);
        
        if (! isset($item['quantity']) || ! $item['quantity']) {
            $errors[] = Tools::displayError($item['title'] . ' - Nie podano ilości.');
        } elseif ($item['quantity'] < 1) {
            $errors[] = Tools::displayError($item['title'] . ' - Podana ilość musi być większa od zera.');
        } elseif (XAllegroConfiguration::get('QUANITY_CHECK') && $item['quantity'] > Product::getQuantity($product->id, $item['id_product_attribute'])) {
            $errors[] = Tools::displayError($item['title'] . ' - Podana ilość jest większą niż dostępna.');
        }
        if ($errors) {
            return array(
                'status' => 'ERROR',
                'errors' => $errors
            );
        }
        
        $categoryFields = array();
        if(!isset($auction['category_fields']) || !is_array($auction['category_fields'])) $auction['category_fields'] = array();
        $categoryFieldsValues = $this->_getCategoryFieldsValuesCompare($auction['item'], $auction['category_fields']);
        foreach ($this->_allegroCategoryFields as $field) {
            if (! isset($categoryFieldsValues[$field['id']]) || ! $categoryFieldsValues[$field['id']]) {
                continue;
            }
            $array = array(
                'fid' => $field['id']
            );
            if (is_array($categoryFieldsValues[$field['id']])) {
                $bits = 0;
                foreach ($categoryFieldsValues[$field['id']] as $bit) {
                    $bits |= $bit;
                }
                ;
                unset($bit);
                $array['fvalue-int'] = $bits;
            } elseif ($field['value_type'] == 'integer') {
                $array['fvalue-int'] = (int) $categoryFieldsValues[$field['id']];
            } elseif ($field['value_type'] == 'float') {
                $array['fvalue-float'] = (float) $categoryFieldsValues[$field['id']];
            } else {
                $array['fvalue-string'] = (string) $categoryFieldsValues[$field['id']];
            }
            $categoryFields[] = $array;
        }
        ;
        
        $additional = (isset($item['additional']['bold']) ? 1 : 0) | (isset($item['additional']['thumbnail']) ? 2 : 0) | (isset($item['additional']['backlight']) ? 4 : 0) | (isset($item['additional']['distinction']) ? 8 : 0) | (isset($item['additional']['category_main']) ? 16 : 0) | (isset($item['additional']['watermark']) ? 64 : 0);
        
        $delivery_options = ($auction['details'] ? 16 : 0) | ($auction['abroad'] ? 32 : 0);
        
        $payment_methods = ($auction['bank_transfer'] ? 1 : 0) | ($auction['invoice'] ? 32 : 0);
        
        $images = array();
        $images_type = (XAllegroConfiguration::get('IMAGES_TYPE')) ? '-' . XAllegroConfiguration::get('IMAGES_TYPE') : '';
        if ((int) XAllegroConfiguration::get('SEND_ADDITIONAL_IMAGES') === 1) {
            if (isset($item['images']) && is_array($item['images'])) {
                foreach ($item['images'] as $i) {
                    if ($i != $item['image_main']) {
						$image_file = _PS_PROD_IMG_DIR_ . Image::getImgFolderStatic((int) $i) . (int) $i . $images_type . '.jpg';
						if(file_exists($image_file)) {
							$images[] = file_get_contents($image_file);
						}
						else {
							$errors[] = Tools::displayError('Nie można załadować obrazu: '.$image_file);
							return array('status' => 'ERROR', 'errors' => $errors);
						}
					}
                }
            }
        }
        
        $content = $this->_renderTemplate($item);
        
        if (isset($item['image_main']) && $item['image_main']) {
			$image_file = _PS_PROD_IMG_DIR_ . Image::getImgFolderStatic((int) $item['image_main']) . (int) $item['image_main'] . $images_type . '.jpg';
			if(!file_exists($image_file)) {
				$errors[] = Tools::displayError('Nie można załadować obrazu: '.$image_file);
				return array('status' => 'ERROR', 'errors' => $errors);
			}
            if (isset($item['overlay']) && $item['overlay']) {
                $overlay = new XAllegroOverlay($item['overlay']);
                $image = $overlay->createImage($image_file);
                ob_start();
                imagepng($image);
                $item['image_main'] = ob_get_clean();
            } else {
                $item['image_main'] = file_get_contents($image_file);
            }
        } else {
            $item['image_main'] = '';
        }
        $ean = '';
        if (XAllegroConfiguration::get('AUCTION_USE_EAN') && ($product->ean13 && $product->ean13 != 0)) {
            $ean = (string) $product->ean13;
        }
        
        $shipment = (isset($auction['shipment']) && is_array($auction['shipment'])) ? $auction['shipment'] : array();
        
        // Dla darmowej opcji dostawy(35) sumujemy wartości
        if (isset($shipment[35])) {
            $shipment[35] = array_sum($shipment[35]);
        }
        $fields = array(
            'id_category' => $this->_allegroCategoryId,
            'title' => $item['title'], // str_replace(array('\'', '"', '&quot;', '&apos;'), '', $item['title']),
            'start' => $auction['start'] ? strtotime($auction['start_time']) : 0,
            'content' => $content,
            'duration' => $item['duration'],
            'quantity' => $item['quantity'],
            'quantity_type' => $item['quantity_type'],
            'price_asking' => $item['price_asking'],
            'price_minimal' => $item['price_minimal'],
            'price_buy_now' => $item['price_buy_now'],
            'state' => $auction['state'],
            'city' => $auction['city'],
            'shipping_cost' => $auction['shipping_cost'],
            'delivery_options' => $delivery_options,
            'payment_methods' => $payment_methods,
            'additional' => $additional,
            'image' => $item['image_main'],
            'images' => $images,
            'details' => $auction['details'],
            'zipcode' => $auction['zipcode'],
            'account_number' => $auction['account_number'],
            'account_number_second' => $auction['account_number_second'],
            // 'delivery_free' => $delivery_free,
            'prepare_time' => $auction['prepare_time'],
            'category_fields' => $categoryFields,
            'in_shop' => isset($auction['allegro_shop']) ? $auction['allegro_shop'] : 0,
            'shipment' => $shipment,
            'ean' => $ean
        );
        $result = $this->_API->newAuction($fields, $test);
        if (is_array($result)) {
            if ($test) {
                return array(
                    'status' => 'OK',
                    'price' => $result['item-price']
                );
            }
            return array(
                'status' => 'OK',
                'id' => $result['item-id']
            );
        } else {
            return array(
                'status' => 'ERROR',
                'errors' => array(
                    $result
                )
            );
        }
    }

    protected function _renderTitle($product)
    {
        return $this->_API->trimTitleToSize(str_replace(array(
            '{product_id}',
            '{product_name}',
            '{product_name_attribute}',
            '{product_reference}',
            '{product_reference_attribute}',
            '{product_ean13}',
            '{product_weight}',
            '{product_price}',
            '{manufacturer_name}'
        ), array(
            $product['id'],
            $product['name'],
            $product['name_attribute'],
            $product['reference'],
            '',
            $product['ean13'],
            number_format($product['weight'], 2, '.', ''),
            $product['price_buy_now'],
            $product['manufacturer_name']
        ), XAllegroConfiguration::get('TITLE_PATTERN')));
    }

    public function _setDisabled($quantity)
    {
        if (XAllegroConfiguration::get('QUANITY_CHECK') && $quantity == 0) {
            return true;
        }
        return false;
    }

    protected function _calculatePrice($price)
    {
        if (XAllegroConfiguration::get('MARKUP_PERCENT')) {
            $price = number_format($price + ($price * (XAllegroConfiguration::get('MARKUP_PERCENT') / 100)), 2, '.', '');
        }
        if (XAllegroConfiguration::get('MARKUP_VALUE')) {
            $price += XAllegroConfiguration::get('MARKUP_VALUE');
        }
        if (XAllegroConfiguration::get('PRICE_ROUND')) {
            $price = round($price);
        }
        return $price;
    }

    protected function _calculateQuantity($quantity)
    {
        if ($quantity > 1000) {
            $quantity = 1000;
        }
        if (XAllegroConfiguration::get('QUANTITY_CHECK')) {
            if (XAllegroConfiguration::get('QUANTITY_DEFAULT') && XAllegroConfiguration::get('QUANTITY_DEFAULT') < $quantity) {
                $quantity = XAllegroConfiguration::get('QUANTITY_DEFAULT');
            }
            if ($quantity == 0) {
                $quantity = 1;
            }
        } else {
            if (XAllegroConfiguration::get('QUANTITY_DEFAULT')) {
                $quantity = XAllegroConfiguration::get('QUANTITY_DEFAULT');
            } elseif ($quantity == 0) {
                $quantity = 1;
            }
        }
        return $quantity;
    }

    protected function _calculateQuantityMax($quantity)
    {
        if ($quantity > 1000) {
            $quantity = 1000;
        }
        return $quantity;
    }

    public function ajaxProcessGetCategories()
    {
        $idCategory = (int) Tools::getValue('id_allegro_category');
        $categories = $this->_API->getCategories($idCategory);
        $fields = '';
        if (! $categories) {
            $this->context->smarty->assign(array(
                'fields' => $this->_getCategoryFields(),
                'values' => $this->_getCategoryFieldsValues(),
                'index' => ''
            ));
            $fields = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/x_allegro_main/helpers/form/category_fields.tpl');
        }
        
        die(Tools::jsonEncode(array(
            'fields' => $fields,
            'categories' => $categories
        )));
    }

    public function ajaxProcessNewAuctionTest()
    {
        $auctions = $_POST;
        
        $this->test_status = 1;
        $this->test_content = '';
        $this->test_errors = array();
        
        foreach ($auctions['item'] as $index => $item) {
            // Pomijam niewybrane produkty
            if (! isset($item['enabled']))
                continue;
            $auction = $auctions;
            $auction['item'] = $item;
			$this->test($auction);
        }
        die(Tools::jsonEncode(array(
            'status' => $this->test_status,
            'content' => $this->test_content
        )));
    }

    private function test($auction)
    {

        $errors = array();
        $item = $auction['item'];
        
        $this->test_content .= '<p><strong>' . $auction['item']['title'] . ':</strong></p>';
        
        if (! $this->_allegroCategoryId) {
            $errors[] = 'Nie wybrano kategorii.';
            $this->test_status = 0;
        } elseif (! $this->_API->categoryIsLastNode($this->_allegroCategoryId)) {
            $errors[] = 'Wybrana kategoria nie jest kategorią najniższego rzędu.';
            $this->test_status = 0;
        } else {
			if (isset($auction['shipment'][50][0]) && (float)str_replace(',', '.', $auction['shipment'][50][0]) > XAllegroConfiguration::get('INPOST_LETTER')) {
				$errors[] = 'Cena opcji dostawy "Allegro Polecony InPost" nie może przekroczyć '.(string)XAllegroConfiguration::get('INPOST_LETTER').' zł.';
				$this->test_status = 0;
			}
			if (isset($auction['shipment'][59][0]) && (float)str_replace(',', '.', $auction['shipment'][59][0]) > XAllegroConfiguration::get('INPOST_MACHINE')) {
				$errors[] = 'Cena opcji dostawy "Odbiór w punkcie po przedpłacie - Allegro Paczkomaty InPost" nie może przekroczyć '.(string)XAllegroConfiguration::get('INPOST_MACHINE').' zł.';
				$this->test_status = 0;
			}
			if (isset($auction['shipment'][60][0]) && (float)str_replace(',', '.', $auction['shipment'][60][0]) > XAllegroConfiguration::get('INPOST_MACHINE_COD')) {
				$errors[] = 'Cena opcji dostawy "Odbiór w punkcie - Allegro Paczkomaty InPost" nie może przekroczyć '.(string)XAllegroConfiguration::get('INPOST_MACHINE_COD').' zł.';
				$this->test_status = 0;
			}
			if (isset($auction['shipment'][61][0]) && (float)str_replace(',', '.', $auction['shipment'][61][0]) > XAllegroConfiguration::get('INPOST_CARRIER')) {
				$errors[] = 'Cena opcji dostawy "Allegro Kurier InPost" nie może przekroczyć '.(string)XAllegroConfiguration::get('INPOST_CARRIER').' zł.';
				$this->test_status = 0;
			}
			if (isset($auction['shipment'][62][0]) && (float)str_replace(',', '.', $auction['shipment'][62][0]) > XAllegroConfiguration::get('INPOST_CARRIER_COD')) {
				$errors[] = 'Cena opcji dostawy "Allegro Kurier InPost (za pobraniem)" nie może przekroczyć '.(string)XAllegroConfiguration::get('INPOST_CARRIER_COD').' zł.';
				$this->test_status = 0;
			}
		}
        if ($errors) {
            $this->test_status = 0;
            $this->test_content .= 'Wystapiły nastepujące błędy:';
            $this->test_content .= '<ul>';
            foreach ($errors as $error) {
                $this->test_content .= '<li>' . $error;
            }
            $this->test_content .= '</ul>';
        } else {
            
            // Sprawdzam, czy podano cenę
            if ((float) $item['price_buy_now'] == 0 && (float) $item['price_asking'] == 0) {
                $this->test_status = 0;
                $this->test_content .= '<ul>';
                $this->test_content .= '<li>Wystapiły nastepujące błędy:';
                $this->test_content .= '<br> - Nie podano prawidłowej ceny</li>';
                $this->test_content .= '</ul>';
                return 0;
            }
			$result = $this->_newAuction($auction, true);
            if ($result['status'] == 'OK') {
                $this->test_status = 1;
                $this->test_content .= '<ul>';
                $this->test_content .= '<li><strong>Koszt wystawienia aukcji: ' . $result['price'] . '</strong>';
                $this->test_content .= '</ul>';
            } else {
                $this->test_status = 0;
                $this->test_content .= '<ul>';
                $this->test_content .= '<li>Wystapiły nastepujące błędy:';
                foreach ($result['errors'] as $error) {
                    $this->test_content .= '<br> - ' . $error . '</li>';
                }
                $this->test_content .= '</ul>';
            }
            
            $this->test_content .= '<br />';
        }
        return $this->test_status;
    }

    public function ajaxProcessPreview()
    {
        $items = Tools::getValue('item');
        die(Tools::jsonEncode(array(
            'preview' => $this->_renderTemplate($items[Tools::getValue('index')])
        )));
    }

    public function ajaxProcessGetPas()
    {
        die(Tools::jsonEncode(new XAllegroPas(Tools::getValue('id'))));
    }

    public function ajaxProcessGetAllegroAccount()
    {
        $account = new XAllegroAccount(Tools::getValue('allegro_account'));
        if (! Validate::isLoadedObject($account)) {
            return;
        }
        $this->context->smarty->assign(array(
            'fields_form' => $this->fields_form['allegro'],
            'fields_value' => $this->getFieldsValue()
        ));
        $fieldset = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/x_allegro_main/helpers/form/fields_form.tpl');
        $data = array(
            'account' => $account,
            'fieldset' => $fieldset,
            'durations' => $this->_durations
        );
        die(Tools::jsonEncode($data));
    }

    public function ajaxProcessGetAllegroDuration()
    {
        $data = array(
            'durations' => $this->_API->getDurations(Tools::getValue('allegro_shop'))
        );
        die(Tools::jsonEncode($data));
    }

    public function ajaxProcessSaveDesc()
    {
        $collection = new Collection('XAllegroProduct');
        $collection->where('id_product', '=', Tools::getValue('id_product'));
        $collection->where('id_product_attribute', '=', Tools::getValue('id_product_attribute'));
        
        if (Validate::isLoadedObject($collection->getFirst())) {
            $collection->getFirst()->description = Tools::getValue('description');
            $collection->getFirst()->save();
        } else {
            $product = new XAllegroProduct();
            $product->id_product = Tools::getValue('id_product');
            $product->id_product_attribute = Tools::getValue('id_product_attribute');
            $product->description = Tools::getValue('description');
            $product->save();
        }
    }

 public function ajaxProcessSaveNewAuctions()
    {
        $number = 0;
        $items = Tools::getValue('item');
        foreach ($items as $item) {
            if (! isset($item['enabled']) || ! $item['enabled']) {
                continue;
            }
            $number ++;
            $auctions[] = array(
                'id_shop' => Context::getContext()->shop->id,
                'id_shop_group' => Context::getContext()->shop->id_shop_group,
                'id_auction' => - 2,
                'id_allegro_account' => (int) Tools::getValue('allegro_account'),
                'id_product' => (int) $item['id_product'],
                'id_product_attribute' => (int) $item['id_product_attribute'],
                'quantity' => (int) $item['quantity'],
                'quantity_start' => (int) $item['quantity'],
                
                'allegro_shop' => (int) Tools::getValue('allegro_shop'),
                'abroad' => (int) Tools::getValue('abroad'),
                'invoice' => (int) Tools::getValue('invoice'),
                'bank_transfer' => (int) Tools::getValue('bank_transfer'),
                'account_number' => pSQL(Tools::getValue('account_number')),
                'account_number_second' => pSQL(Tools::getValue('account_number_second')),
                'pas' => (int) Tools::getValue('pas'),
                'state' => pSQL(Tools::getValue('state')),
                'city' => pSQL(Tools::getValue('city')),
                'zipcode' => pSQL(Tools::getValue('zipcode')),
                'id_allegro_category' => pSQL(json_encode(Tools::getValue('id_allegro_category'))),
                'category_fields' => pSQL(json_encode(Tools::getValue('category_fields'))),
                'item' => ! _PS_MAGIC_QUOTES_GPC_ ? pSQL(json_encode($item), true) : pSQL(addslashes(json_encode($item)), true),
                'shipping_cost' => (int) Tools::getValue('shipping_cost'),
                'shipment' => pSQL(json_encode(Tools::getValue('shipment'))),
                'prepare_time' => pSQL(Tools::getValue('prepare_time')),
                'details' => pSQL(Tools::getValue('details')),
                'start' => (int) Tools::getValue('start'),
                'start_time' => pSQL(Tools::getValue('start_time')),
                'default' => (int) Tools::getValue('default'),
                'resume_auction' => (int) Tools::getValue('resume_auction'),
                
                'closed' => 0
            );
        }
        if(isset($items) && count($items) > 0) {
            if (isset($auctions)) {
                if (XAllegroAuction::saveAuctions($auctions, (int) Tools::getValue('start_index'))) {
                    $response = array(
                        'status' => 1,
                        'number' => $number,
                        'content' => $this->l('Zapisano informacje o wystawianych aukcjach.')
                    );
              } else {
                    $response = array(
                        'status' => 0,
                        'number' => null,
                        'content' => $this->l('Zapis aukcji nie powiód się.')
                    );
              }
           }
           else {
                $response = array(
                        'status' => 1,
                        'number' => 0,
                        'content' => $this->l('Zapisano informacje o wystawianych aukcjach.')
                    );
            }
        } else {
            $response = array(
                'status' => 0,
                'number' => null,
                'content' => $this->l('Brak aukcji do wystawienia.')
            );
        }
        die(json_encode($response));
    }


    public function ajaxProcessCheckAuction()
    {
        $auction = XAllegroAuction::getTempAuction(-2);
        if ($auction) {
            
            // ustawianie konta, z którego wystawiamy aukcje
            $this->_setAccount($auction['id_allegro_account']);
            
            // definiowanie categorii i cech
            $this->_setCategory($auction['id_allegro_category']);
            
            $this->test_status = 1;
            $this->test_content = '';
            $this->test_errors = array();
            
            // testowanie aukcji
            $test_result = $this->test($auction);
            if ($test_result) {
                // aktualizacja statusu (`id_auction` = -1)
                XAllegroAuction::markTempAuction($auction['id_xallegro_auction'], -1);
            }
            else {
				//var_dump($test_result);
				//d($auction);
			}
			$response = array(
				'status' => $this->test_status,
				'number' => XAllegroAuction::getTempAuctionQuantity(-2),
				'content' => $this->test_content
			);
        } else {
            $response = array(
                'status' => 2,
                'number' => null,
                'content' => $this->l('Sprawdzono wszystkie aukcje.')
            );
        }
        die(json_encode($response));
    }

    public function ajaxProcessSendAuction()
    {
        $auction = XAllegroAuction::getTempAuction(-1);
        if ($auction) {
            
            // ustawianie konta, z którego wystawiamy aukcje
            $this->_setAccount($auction['id_allegro_account']);
            
            // definiowanie categorii i cech
            $this->_setCategory($auction['id_allegro_category']);
            
            $this->test_status = 1;
            $this->test_content = '';
            $this->test_errors = array();
            
            // wystawianie aukcji
            $result = $this->_newAuction($auction, false);
            if ($result['status'] == 'OK') {
                // przypisanie numeru aukcji allegro
                XAllegroAuction::markTempAuction($auction['id_xallegro_auction'], $result['id']);
                $response = array(
                    'status' => 1,
                    'number' => XAllegroAuction::getTempAuctionQuantity(-1),
                    'content' => $this->l('Wystawiono aukcję.')
                );
            } else {
                $error_content = '<ul>';
                $error_content .= '<li>Wystapiły nastepujące błędy:</li>';
                foreach ($result['errors'] as $error) {
                    $error_content .= ' - ' . $error . '<br>';
                }
                $error_content .= '</ul>';
                $response = array(
                    'status' => 0,
                    'number' => null,
                    'content' => $error_content
                );
            }
        } else {
            $response = array(
                'status' => 2,
                'number' => 0,
                'content' => $this->l('Wszystkie aukcje zostały wystawione.')
            );
        }
        die(json_encode($response));
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS(_PS_JS_DIR_ . 'admin/tinymce.inc.js');
        $this->addJS(_PS_JS_DIR_ . 'tinymce.inc.js');
    }
    
    //Modyfikuje opis aukcji wg zadanych kryteriów.
    private function prepareDescription($description, $name = 'empty') {
		//Ustalanie adresu sklepu i domeny
		$shop_url = Tools::getHttpHost(true).__PS_BASE_URI__;
		$server_url = $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$server_url .= $_SERVER['HTTP_HOST'];
		//Zastąpienie względnych ścieżek do zdjęć ścieżkami bezwzględnymi w domenie.
		$description = Tools::stripslashes($description);
		preg_match_all('/<img(.+?)>/mi', $description, $imgs);
		if(isset($imgs[0])) {
			foreach($imgs[0] as $img) {
				$s = strpos($img, 'src="') + 5;
				if($s) {
					$src_attribute = substr($img, $s, strpos($img, '"', $s) - $s);
					if(substr($src_attribute, 0, 3) == '../') {
						$replacements[$src_attribute] = $shop_url.substr($src_attribute, 3, strlen($src_attribute)-3);
					}
					elseif(substr($src_attribute, 0, 1) == '/') {
						$replacements[$src_attribute] = $server_url.$src_attribute;
					}
				}
			}
		}
		//zastąpienie tagów zdjęć
		preg_match_all('/\[img(.+?)\]/mi', $description, $tags);
		if(isset($tags[1]) && is_array($tags[1])) {
			foreach($tags[1] as $tag_index => $tag) {
				$elements = explode('-', $tag);
				$image_link = $this->context->link->getImageLink(Tools::str2url($name), $elements[1], $elements[3]);
				$replacements[$tags[0][$tag_index]] = '<img class="imageFloat'.Tools::toCamelCase($elements[2], true).'" src="'.$image_link.'" alt="'.Tools::str2url($name).'">';
			}
		}
		if(isset($replacements)) {
			$description = str_replace(array_keys($replacements), array_values($replacements), $description);
		}
		return $description;
	}
    
}
