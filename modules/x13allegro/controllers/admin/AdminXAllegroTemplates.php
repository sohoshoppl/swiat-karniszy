<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

    class AdminXAllegroTemplatesController extends XAllegroController
    {
        
        protected $_checkAccount = false;

        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
            $this->table = 'xallegro_template';
            $this->className = 'XAllegroTemplate';
            $this->module = 'main';
            $this->lang = false;
            
            $this->addRowAction('edit');
            $this->addRowAction('delete');
            $this->bulk_actions = array('delete' => array('text' => $this->l('Usuń zaznaczone'), 'confirm' => $this->l('Usunąć zaznaczone elementy?')));
            
            $this->fields_list = array(
                'id_xallegro_template' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'width' => 20
                ),
                'name' => array(
                    'title' => $this->l('Nazwa szablonu'),
                    'width' => 'auto'
                ),
                'default' => array(
                    'title' => $this->l('Domyślny'),
                    'width' => 100,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'active' => 'default',
                    'type' => 'bool'
                ),
                'active' => array(
                    'title' => $this->l('Aktywny'),
                    'width' => 100,
                    'align' => 'center',
                    'active' => 'active',
                    'type' => 'bool',
                    'class' => 'fixed-width-sm',
                )
            );
            
            parent::__construct();
            
        }

        public function renderList()
        {
            return parent::renderList();
        }

        public function renderForm()
        {
            $this->fields_form = array(
                'tinymce' => true,
                'legend' => array(
                    'title' => $this->l('Podstawowe dane'),  
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_' . $this->table
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa'),
                        'name' => 'name',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(           
                        'type' => 'textarea',
                        'label' => $this->l('Treść'),
                        'name' => 'content',
                        'rows' => 100,
                        'cols' => 96,
                        'id' => 'tinymce',
                        'class' => 'rte',
                        'autoload_rte' => $this->bootstrap ? true : 'rte',
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Domyślny'),
                        'name' => 'default',
                        'required' => false,
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Aktywny'),
                        'name' => 'active',
                        'required' => false,
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz')
                )
            );
            
            $variables = array(
                'auction_title' => 'Tytuł aukcji',
                'auction_description' => 'Opis aukcji (z ewentualnymi zmianami podczas wystawiania)',
                'auction_price' => 'Cena "Kup Teraz"',
                'product_name' => 'Nazwa produktu',
                'product_reference' => 'Kod referencyjny',
                'product_attribute_name' => 'Nazwa atrybutu (jeśli posiada)',
                'product_attribute_reference' => 'Kod referencyjny atrybutu (jeśli posiada)',
                'product_ean13' => 'Kod EAN13',
                'product_description' => 'Opis produktu w sklepie',
                'product_description_short' => 'Krótki opis produktu w sklepie',
                'product_description_custom' => 'Dodatkowy opis produktu',
                'product_features' => 'Cechy produktu',
                'feature_name_X' => 'Nazwa cechy o identyfikatorze X dla produktu',
                'feature_value_X' => 'Wartość cechy o identyfikatorze X dla produktu',
                'attribute_name_X' => 'Nazwa atrybutu o identyfikatorze X dla produktu',
                'attribute_value_X' => 'Wartość  atrybutu o identyfikatorze X dla produktu',
                'product_price' => 'Cena produktu w sklepie',
                'product_weight' => 'Waga produktu',
                'manufacturer_name' => 'Nazwa producent',
                'manufacturer_logo' => 'Logo producenta',
                'image_ROZMIAR_X' => 'Zdjęcia rozmiar ROZMIAR, gdzie X to jego numer (liczone od 1 (1,2,3 itd.)) np. image_thickbox_default_1',
                'image_thickbox_default_X' => 'Zdjęcia rozmiar thickbox_default, gdzie X to jego numer',
                'image_oryginal_X' => 'Zdjęcia w oryginalnym rozmiarze, gdzie X to jego numer',
                'gallery' => 'Automatycznie generowana galeria zdjęć (lista ze zdjęć thicbkox_default i medium_default)',
                'gallery_full' => 'Automatycznie generowana galeria zdjęć (lista ze zdjęć oryginalne i medium_default)',
                'gallery_link' => 'Automatycznie generowana galeria zdjęć (lista ze zdjęć thicbkox_default i medium_default z linkiem do kliknięcia)',
                'gallery_image' => 'Automatycznie generowana galeria zdjęć (sama lista zdjęć thicbkox_default)',
                'product.POLE' => 'Wartośc pola z tabeli product',
                'product_lang.POLE' => 'Wartość pola z tabeli product_lang'
            );
            
            $this->context->smarty->assign(array(
                'template_variables' => $variables,
                'folder_admin' => basename(_PS_ADMIN_DIR_)
            ));
            
            return parent::renderForm();
        }
        
    }
