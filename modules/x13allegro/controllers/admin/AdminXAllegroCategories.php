<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';
    
    class AdminXAllegroCategoriesController extends XAllegroController
    {
        
        protected $_API;
        protected $_categoryId;
        protected $_categoryPath = array();
        protected $_categoryFields = array();

        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            $this->table = 'xallegro_category';
            $this->className = 'XAllegroCategory';
            $this->lang = false;
            
            $this->_select = 'COUNT(`id_xallegro_category`) AS count_assigned';
            $this->_group = 'GROUP BY `id_allegro_category`';
            
            $this->addRowAction('edit');
            $this->addRowAction('delete');
            $this->bulk_actions = array('delete' => array('text' => $this->l('Usuń zaznaczone'), 'confirm' => $this->l('Usunąć zaznaczone elementy?')));
            
            $this->fields_list = array(
                //'id_xallegro_category' => array(
                //    'title' => $this->l('ID'),
                //    'align' => 'center',
                //    'width' => 20
                //),
                'path' => array(
                    'title' => $this->l('Kategoria')
                ),
                'count_assigned' => array(
                    'title' => $this->l('Ilość przypisanych kategorii'),
                    'align' => 'center',
                    'search' => false,
                    'filter' => false,
                ),
            );
            
            parent::__construct();
            
            if (!Validate::isLoadedObject($this->_account)) {
                return;
            }
            
            try {
                $this->_API = new XAllegroAPI($this->_account);
            } catch (SoapFault $e) {
                $this->_APIError = $e->getMessage();
            }
            
//            Try load object
            $this->loadObject(true);

//            Set category from post
            $allegroCategoryId = Tools::getValue('id_allegro_category');
            
//            If isset category path array
            if ($allegroCategoryId) {
//                If select any category
                if (!is_array($allegroCategoryId)) {
                    $this->_categoryId = (int) $allegroCategoryId;
                } else if (current($allegroCategoryId) != 0 && end($allegroCategoryId) == 0) {
                    $this->_categoryId = (int) $allegroCategoryId[count($allegroCategoryId)-2];
                } else {
                    $this->_categoryId = (int) end($allegroCategoryId);
                }
            } elseif(ValidateCore::isLoadedObject($this->object)) {
                $this->_categoryId = $this->object->id_allegro_category;
            }
            
//            If is selected any category
            if ($this->_categoryId) {
                $categoryPath = $this->_API->getCategoryPath($this->_categoryId);

                foreach ($categoryPath as $path) {
                    $this->_categoryPath[] = $path['id'];
                    $this->_categoryPathName[] = $path['name'];
                }

//                Check is last node
                if ($this->_API->categoryIsLastNode($this->_categoryId)) {
//                    Get all fields for category
                    $this->_categoryFields = $this->_API->getCategoryFields($this->_categoryId);
                } else {
//                    Add last category to select
                    $this->_categoryPath[] = 0;
                }
            } else {
                $this->_categoryPath[] = 0;
            }
        }

		public function postProcess() {
			if(Tools::getValue('id_category')) {
				$ids_categories = Tools::getValue('id_category');
				$id_xallegro_category = (int)Tools::getValue('id_xallegro_category');
				$id_allegro_category_array = Tools::getValue('id_allegro_category');
				$id_allegro_category = (int)end($id_allegro_category_array);
				$xallegro_category = XAllegroCategory::prepareCategory($id_allegro_category, $ids_categories);
				//d($xallegro_category);
				foreach($ids_categories as $id_cat) {
					$_POST['id_category'] = $id_cat;
					parent::processAdd();
				}
				unset($_POST);
				return parent::postProcess();
			}
			else {
				return parent::postProcess();
			}
		}

        public function processSave()
        {
            if (!Tools::getValue('id_category')) {
                $this->errors[] = Tools::displayError('Nie wybrano kategorii w sklepie.');
            } elseif (!$this->_categoryId) {
                $this->errors[] = Tools::displayError('Nie wybrano kategorii Allegro.');
            } elseif (!$this->_API->categoryIsLastNode($this->_categoryId)) {
                $this->errors[] = Tools::displayError('Wybrana kategoria Allegro nie jest kategorią najniższego rzędu.');
            }
            
            if (!$this->errors && $this->_categoryFields) {
                $categoryFieldsValues = $this->_getCategoryFieldsValues();
                foreach ($this->_categoryFields as $categoryField) {
                    if (!$categoryFieldsValues[$categoryField['id']]) {
                        continue;
                    }
                    
                    if ($categoryField['value_type'] == 'integer' && 
                        (!is_array($categoryFieldsValues[$categoryField['id']]) && 
                        $categoryFieldsValues[$categoryField['id']] != (string) intval($categoryFieldsValues[$categoryField['id']])
                        )) {
                        $this->errors[] = Tools::displayError($categoryField['label'] . ' - Niepoprawna wartość pola.');
                    }

                    if ($categoryField['value_type'] == 'float' && 
                        !is_numeric($categoryFieldsValues[$categoryField['id']])) {
                        $this->errors[] = Tools::displayError($categoryField['label'] . ' - Niepoprawna wartość pola.');
                    }
                }
            }
            return parent::processSave();
        }
        
        protected function beforeAdd($object)
        {
            $object->id_allegro_category = end($object->id_allegro_category);
            $object->fields = serialize(Tools::getValue('category_fields'));
            $object->path = implode(' > ', $this->_categoryPathName);
        }
        
        protected function afterUpdate($object)
        {
            $object->id_allegro_category = end($object->id_allegro_category);
            $object->fields = serialize(Tools::getValue('category_fields'));
            $object->path = implode(' > ', $this->_categoryPathName);
            $object->save();
        }

        public function renderList()
        {
            return parent::renderList();
        }

        public function renderForm()
        {
            $this->multiple_fieldsets = true;
            
            $root_category = Category::getRootCategory();
            $root_category = array('id_category' => $root_category->id, 'name' => $root_category->name);

			$selected_cat = array();
			if (Tools::getValue('id_category')) {
                $selected_cat[] = Tools::getValue('id_category');
            } elseif (Validate::isLoadedObject($this->object)) {
                $selected_cat[] = $this->object->id_category;
            }
            
            if(!isset($this->only_fields) || !$this->only_fields) {
				$this->fields_form[0]['form'] = array(
					'legend' => array(
						'title' => $this->l('Kategoria aukcji'),   
					),
					'input' => array(
						array(
							'name' => 'id_allegro_category',
							'type' => 'category',
							'categories' => $this->_API->getCategories(),
							'path' => $this->_categoryPath,
						),
					),
				);
			}
            
			$this->fields_form[1]['form'] = array(
				'legend' => array(       
					'title' => $this->l('Cechy kategorii'),       
				),
				'input' => array()
			);
            
            if ($this->_categoryFields) {
                foreach ($this->_categoryFields as $categoryField) {
					//sortowanie opcji
					if(isset($categoryField['options']) && is_array($categoryField['options'])) {
						$options = array();
						foreach($categoryField['options'] as $cF) {
							$options[Tools::str2url($cF['label'])] = $cF;
						}
						ksort($options, SORT_STRING);
						$categoryField['options'] = array_values($options);
					}
                    $this->fields_form[1]['form']['input'][] = array(
                        'id' => $categoryField['id'],
                        'name' => 'category_fields',
                        'type' => $categoryField['type'],
                        'label' => $this->l($categoryField['label']),
                        'suffix' => $categoryField['unit'],
                        //'unit' => $categoryField['unit'],
                        'required' => (bool)$categoryField['required'],
                        'options' => array(
                            'query' => $categoryField['options'],
                            'id' => 'value',
                            'name' => 'label'
                        ),
                        'values' => $categoryField['options'],
                        'class' => 't'.(($categoryField['type'] != 'checkbox' && $categoryField['type'] != 'radio') ? ' fixed-width-xxl' : ''),
                        'br' => true
                    );
                }
            }
            /*
            if(!isset($this->only_fields) || !$this->only_fields) {
				if ($this->_account->shop) {
					$this->fields_form[]['form'] = array(
						'legend' => array(       
							'title' => $this->l('Kategoria w sklepie Allegro.pl'),       
						),
						'input' => array(
							array(
								'name' => 'id_allegro_shop_category',
								'type' => 'select',
								'label' => $this->l('Kategoria'),
								'options' => array(
									'query' => array_merge(array(array(
										'id' => 0,
										'name' => 'Brak kategorii'
									)), $this->_API->getShopCategories()),
									'id' => 'id',
									'name' => 'name'
								)
							)
						)
					);
				}
			}
            */
            
			if(!isset($this->only_fields) || !$this->only_fields) {
				$this->fields_form[]['form'] = array(
					'legend' => array(       
						'title' => $this->l('Kategoria w sklepie internetowym'),       
					),
					'input' => array(
						array(
							'type' => 'categories',
							'label' => $this->l('Kategoria'),
							'name' => 'id_category',
							'tree' => array(
								'id' => 'id_category',
								'root_category' => $root_category['id_category'],
								'use_search' => true,
								'use_checkbox' => true,
								'use_radio' => false,
								'selected_categories' => $this->object->getAssignedCategories(),
								'disabled_categories' => array(),
							),
							'values' => array(
								'trads' => array(
									'Root' => $root_category,
									'selected' => $this->l('Selected'),
									'Collapse All' => $this->l('Collapse All'),
									'Check All' => $this->l('Check All'),
									'Uncheck All' => $this->l('Uncheck All'),
									'Expand All' => $this->l('Expand All'),
									'search' => $this->l('search'),
								),
								'selected_cat' => $this->object->getAssignedCategories(),
								'disabled_categories' => array(),
								'input_name' => 'id_category[]',
								'use_checkbox' => true,
								'use_radio' => false,
								'use_search' => true,
								'top_category' => Category::getTopCategory(),
								'use_context' => true,
							)
						)
					),
					'submit' => array(
						'name' => 'save',
						'title' => $this->l('Zapisz'),
						'class' => 'btn btn-default pull-right'
					)
				);
			}

            $this->tpl_form_vars['category_fields_values'] = $this->_getCategoryFieldsValues();
            $this->tpl_form_vars['features'] = Feature::getFeatures($this->context->language->id);
            
            return parent::renderForm();
        }
        
        protected function _getCategoryFieldsValues()
        {
            $fieldsValues = array(
                'items' => array()
            );
            
            // If post array is set - get values from post array
            $postValues = Tools::getValue('category_fields');
            
            // If post array is empty - get values from database 
            if(!$postValues) $postValues = $this->object->fields;
            
            foreach ($this->_categoryFields as $categoryField) {
                if (array_key_exists($categoryField['id'], $fieldsValues)) {
                    continue;
                }
                
                if (isset($postValues[$categoryField['id']])) {
                    $fieldsValues[$categoryField['id']] = $postValues[$categoryField['id']];
                } elseif (Validate::isLoadedObject($this->object) && isset($this->object->fields[$categoryField['id']])) {
                    $fieldsValues[$categoryField['id']] = $this->object->fields[$categoryField['id']];
                } else {
                    $fieldsValues[$categoryField['id']] = null;
                }
                
                if (isset($postValues[$categoryField['id'] . '_feature'])) {
                    $fieldsValues[$categoryField['id'] . '_feature'] = $postValues[$categoryField['id'] . '_feature'];
                } elseif(Validate::isLoadedObject($this->object) && isset($this->object->fields[$categoryField['id'] . '_feature'])) { 
                    $fieldsValues[$categoryField['id'] . '_feature'] = $this->object->fields[$categoryField['id'] . '_feature'];
                }else {
                    $fieldsValues[$categoryField['id'] . '_feature'] = null;
                }
            }
            
            return $fieldsValues;
        }
        
        public function ajaxProcessGetCategories()
        {
            $idCategory = (int)Tools::getValue('id_allegro_category');
            
            $categories = $this->_API->getCategories($idCategory);
            $fields = $this->_API->getCategoryFields($idCategory);
            
//            Fast FIX, don't kill me - it's not important ;'(
            if (!$categories) {
				if($this->ps_version == 1.5) {
					$tmp = explode('id="fieldset_1">', $this->renderForm());
					preg_match('@</legend>(.*)</fieldset>@sU', $tmp[1], $fields);
					$fields = trim($fields[1]);
				}
				else {
					$this->only_fields = true;
					$fields_source = $this->renderForm();
					if(preg_match('/<div class=\"form\-wrapper\">(.+?)<\/div><\!\-\- \/\.form\-wrapper \-\->/', str_replace(array("\n", "\r", "\t"), ' ', $fields_source), $fields_form))
						$fields = $fields_form[1];
					else
						$fields = '';
				}
            }
            else{
				$this->only_fields = false;
			}
            
            die(Tools::jsonEncode(array(
                'fields' => $fields,
                'categories' => $categories
            )));
        }
    }
