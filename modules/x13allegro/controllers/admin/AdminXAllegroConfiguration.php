<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

    class AdminXAllegroConfigurationController extends XAllegroController
    {
        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            $this->className = 'Configuration';
            $this->table = 'configuration';

            parent::__construct();
            
            $this->_conf[32] = $this->l('Aktualizacja przebiegła pomyślnie.');
            
            if (!ValidateCore::isLoadedObject($this->_account)) {
                return;
            }
            
            $durations = array();
            
            try {
                $this->_API = new XAllegroAPI($this->_account);

                foreach ($this->_API->getDurations() as $duration) {
                    $durations[] = array(
                        'id' => $duration['value'],
                        'name' => $duration['label']
                    );
                }
            } catch (SoapFault $e) {
                $this->_APIError = $e->getMessage();
            }
            
            $imageTypes = array(array(
                'id' => '',
                'name' => 'oryginalny rozmiar'
            ));
            $imageTypesArray = Db::getInstance()->ExecuteS('SELECT `name`, `width`, `height` FROM `' . _DB_PREFIX_ . 'image_type`');
            
            foreach ($imageTypesArray as $imageType) {
                $imageTypes[] = array(
                    'id' => $imageType['name'],
                    'name' => $imageType['name'] . ' (' . $imageType['width'] . ' x ' . $imageType['height'] . ')'
                );
            }
            
            $this->fields_options = array(
                'general' => array(
                    'title' =>	$this->l('Ustawienia podstawowe'),
                    'image' => '../img/admin/tab-tools.gif',
                    'fields' =>	array(
                        'TITLE_PATTERN' => array(
                            'title' => $this->l('Domyślny tytuł aukcji'),
                            'type' => 'text',
                            'size' => 70,
                            'desc' => ''
                                . $this->l('{product_id} - ID produktu') . '<br />'
                                . $this->l('{product_name} - Nazwa produktu') . '<br />'
                                . $this->l('{product_name_attribute} - Nazwa atrybutu') . '<br />'
                                . $this->l('{product_reference} - Kod referencyjny produktu') . '<br />'
                                . $this->l('{product_reference_attribute} - Kod referencyjny atrybutu produktu') . '<br />'
                                . $this->l('{product_ean13} - Kod EAN13') . '<br />'
                                . $this->l('{product_weight} - Waga produktu') . '<br />'
                                . $this->l('{product_price} - Cena produktu') . '<br />'
                                . $this->l('{manufacturer_name} - Nazwa producent') . '<br />',
                        ),
                        'MARKUP_PERCENT' => array(
                            'title' => $this->l('Narzut ceny dla produktów'),
                            'type' => 'text',
                            'size' => 20,
                            'desc' => $this->l('Wartość podana w procentach.'),
                            'defaultValue' => '0',
                            'suffix' => '%'
                        ),
                        'MARKUP_VALUE' => array(
                            'title' => $this->l('Marża ceny dla produktów'),
                            'type' => 'text',
                            'size' => 20,
                            'desc' => $this->l('Wartość podana w złotówkach.'),
                            'defaultValue' => '0',
                            'suffix' => 'Zł'
                        ),
                        'QUANTITY_DEFAULT' => array(
                            'title' => $this->l('Domyślna ilość produktów'),
                            'type' => 'text',
                            'size' => 20,
                            'desc' => $this->l('Pusta pole oznacza maksymalną dostępną ilość produktów.')
                        ),
                        'DURATION_DEFAULT' => array(
                            'title' => $this->l('Domyślny czas trwania aukcji'),
                            'type' => 'select',
                            'identifier' => 'id',
                            'list' => $durations
                        ),
                        'SELECT_ALL' => array(
                            'title' => $this->l('Domyślne zaznaczenie wszystkich produktów'),
                            'type' => 'bool',
                            'desc' => $this->l('Włączenie tej opcji spowoduje domyślne zaznaczanie wszystkich produktów do wystawienia w formularzu wystawiania aukcji.')
                        ),
                        'AUCTION_USE_EAN' => array(
                            'title' => $this->l('Używaj kodu ean13 w aukcjach'),
                            'type' => 'bool',
                        ),
                        'PRICE_ROUND' => array(
                            'title' => $this->l('Zaokrąglaj ceny do pełnych złotówek'),
                            'type' => 'bool',
                        ),
                        'SEND_ADDITIONAL_IMAGES' => array(
                            'title' => $this->l('Przesyłaj dodatkowe zdjęcia do Allegro'),
                            'desc' => $this->l('Przy włączonej opcji wybrane podczas wystawiania dodatkowe zdjęcia produktu zostaną przesłane do serwisu Allegro.').'<br><b>'.$this->l('Uwaga!!! Może sie to wiązać z wyższą opłatą za wystawienie aukcji.').'</b>',
                            'type' => 'bool',
                        ),
                        'SELECT_IMAGES' => array(
                            'title' => $this->l('Domyślnie zaznaczone zdjęcia'),
                            'desc' => $this->l('Włączenie tej opcji spowoduje zaznaczenie na liści eproduktów do wystawienia ustawionej ilości zdjęć.'),
                            'type' => 'select',
                            'identifier' => 'id_selection',
                            'list' => array(
								array('id_selection' => '0', 'name' => $this->l('brak zaznaczonych')),
								array('id_selection' => 'first', 'name' => $this->l('tylko pierwsze')),
								array('id_selection' => 'all', 'name' => $this->l('wszystkie')),
                            ),
                        ),
                        'IMAGES_COMBINATION' => array(
                            'title' => $this->l('Przenoś zdjęcia do kombinacji'),
                            'type' => 'bool',
                        ),
                        'IMAGES_TYPE' => array(
                            'title' => $this->l('Typ zdjęcia używanego w aukcjach'),
                            'type' => 'select',
                            'identifier' => 'id',
                            'list' => $imageTypes
                        ),
                        'OVERLAY_UNDER_IMAGE' => array(
                            'title' => $this->l('Ramka pod zdjęciem'),
                            'desc' => $this->l('Przy włączonej opcji ramka miniaturki będzie znajdowała się pod zdjęciem produktu.'),
                            'type' => 'bool',
                        ),
//                        'XALLEGRO_RENEWAL_SETTINGS' => array(
//                            'title' => $this->l('Podczas wznawianiu aukcji:'),
//                            'type' => 'radio',
//                            'choices' => array(
//                                0 => 'Pobieraj aktualny opis i ilość',
//                                1 => 'Pobieraj aktualny opis, ilość i cenę',
//                                2 => 'Pobieraj aktualną ilość i cenę',
//                                4 => 'Wystaw tak samo jak pierwotny produkt'
//                            )
//                        )
                    ),
                    'submit' => array('title' => $this->l('Zapisz'))
                ),
                'sync' => array(
                    'title' =>	$this->l('Synchronizacja stanów magazynowych i zamówień'),
                    'image' => '../img/admin/products.gif',
                    'fields' => array(
                        'IMPORT_ORDERS' => array(
                            'title' => $this->l('Import zamówień z Allegro do sklepu'),
                            'type' => 'bool'
                        ),
//                        'ORDER_SEND_MAIL' => array(
//                            'title' => $this->l('Wysyłaj powiadomienie do klientów, którzy złożyli zamówienie'),
//                            'type' => 'bool',
//                            'desc' => 'Włącznie tej opcji spowoduje wysłanie wiadomości e-mail (podsumowania zakupów w sklepie) dla kupujących za pośrednictwem Allegro'
//                        ),
                        'QUANITY_CHECK' => array(
                            'title' => $this->l('Nadzoruj stany magazynowe'),
                            'type' => 'bool',
                            'desc' => $this->l('Wyłączenie tej opcji spowoduje możliwość sprzedaży ilości niezależnie od stanu magazynowego')
                        ),
                        'QUANITY_SHOP_UPDATE' => array(
                            'title' => $this->l('Aktualizuj stany magazynowe w sklepie'),
                            'type' => 'bool'
                        ),
                        'QUANITY_ALLEGRO_UPDATE' => array(
                            'title' => $this->l('Aktualizuj stany magazynowe w Allegro'),
                            'type' => 'bool'
                        ),
                        'QUANITY_ALLEGRO_UPDATE_CHUNK' => array(
                            'title' => $this->l('Liczba aktualizowanych produktów'),
                            'desc' => $this->l('Liczba produktów, których stany magazynowe aktualizowane są podczas jednorazowego uruchomienia skryptu sync.php. Uwaga!!! Zbyt duża liczba w tym polu może spowolnić wykonywanie skryptu.'),
                            'type' => 'text',
                            'cast' => 'intval',
                            'class' => 'fixed-width-sm',
                            'size' => 10,
                            'maxlength' => 5,
                        ),
                        'QUANITY_ALLEGRO_ALWAYS_MAX' => array(
                            'title' => $this->l('Utrzymuj maksymalne stany magazynowe na Allegro'),
                            'type' => 'bool'
                        )
                    ),
                    'submit' => array('title' => $this->l('Zapisz'))
                ),
                'notifications' => array(
                    'title' =>$this->l('Aktualizacja'),
                    'image' => '../img/admin/time.gif',
                    'fields' =>	array(
                        'CRON_URL' => array(
                            'title' => $this->l('Adres wywołania dla CRON'),
                            'type' => 'text',
                            'size' => 20,
                            'is_invisible' => true,
                            'defaultValue' => constant('_PS_BASE_URL_') . $this->context->controller->module->getPathUri() . 'sync.php?token=' . XAllegroConfiguration::get('SYNC_TOKEN')
                        )
                    )
                )
            );
        }
        
        public function postProcess()
        {
            if (Tools::getIsset('update')) {
                $XAllegroUpdate = new XAllegroUpdate();
                if ($XAllegroUpdate->update()) {
                    Tools::redirectAdmin($this->context->link->getAdminLink($this->controller_name) . '&conf=32');
                } else {
                    $this->errors[] = $this->l('Aktualizacja nie powiodła się! Skontaktuj się z dostawcą modułu.');
                }
            }
            elseif(Tools::getIsset('refresh_categories')) {
				$this->_API->updateCategories(Tools::getValue('version'));
				Tools::redirectAdmin($this->context->link->getAdminLink($this->controller_name) . '&conf=32');
			}
			elseif(Tools::getIsset('refresh_fields')) {
				$this->_API->updateFields(Tools::getValue('version'));
				Tools::redirectAdmin($this->context->link->getAdminLink($this->controller_name) . '&conf=32');
			}
            
            parent::postProcess();
        }
        
        public function initPageHeaderToolbar()
        {
            if (!$this->bootstrap) {
                return;
            }
			$this->page_header_toolbar_btn['sync_closed_status'] = array(
				'href' => '#syncClosedStatus',
				'desc' => $this->l('Aktualizuj wystawienia'),
				'icon' => 'process-icon-retweet icon-retweet',
				'help' => $this->l('Synchronizuje statusy ilości wystawionych aukcji na liście produktów.'),
			);
            if (Context::getContext()->controller->module->update) {
				$this->page_header_toolbar_btn['update_module'] = array(
					'href' => $this->context->link->getAdminLink('AdminXAllegroConfiguration').'&update',
					'desc' => $this->l('Aktualizacja'),
					'icon' => 'process-icon-refresh',
				);
			}
            parent::initPageHeaderToolbar();
        }
        
        public function initToolbar()
        {
			$this->toolbar_btn['sync_closed_status'] = array(
				'href' => '#syncClosedStatus',
				'desc' => $this->l('Aktualizuj wystawienia'),
				'class' => 'process-icon-refresh-cache',
			);
            if (Context::getContext()->controller->module->update) {
                $this->toolbar_btn['refresh'] = array(
                    'href' => $this->context->link->getAdminLink('AdminXAllegroConfiguration').'&update',
                    'desc' => $this->l('Aktualizacja'),
                    'class' => 'process-icon-refresh-index'
                );
            }
            
            parent::initToolbar();
            
        }
        
        public function renderOptions()
        {
            if ($this->fields_options && is_array($this->fields_options)) {
                $this->tpl_option_vars['xallegro_update'] = $this->context->controller->module->update;
                unset($this->toolbar_btn);
                $this->initToolbar();
                $helper = new HelperXAllegroOptions($this);
                $this->setHelperDisplay($helper);
                $helper->id = $this->id;
                $helper->tpl_vars = $this->tpl_option_vars;
                $options = $helper->generateOptions($this->fields_options);

                return $options;
            }
        }
        
        protected function processUpdateOptions()
        {
            $this->beforeUpdateOptions();
            
            foreach ($this->fields_options as $category_data)
            {
                if (!isset($category_data['fields']))
                    continue;

                $fields = $category_data['fields'];
                
                foreach ($fields as $key => $options)
                {
                    $value = Tools::getValue($key, '');
                    
                    if ($key == 'MARKUP_PERCENT') {
                        $value = (int)$value;
                    } elseif ($key == 'MARKUP_VALUE') {
                        $value = preg_replace('/[^0-9\.]/', '', str_replace(',', '.', $value));
                    }
                    
                    XAllegroConfiguration::updateValue($key, $value);
                }
            }
            
            $this->confirmations[] = $this->_conf[6];
        }
        
		public function ajaxProcessSyncClosedStatus() {
			$xAllegroSync = new XAllegroSync();
			$data = $xAllegroSync->sync_allegro_closed_status(Tools::getValue('start'), Tools::getValue('total'));
			die(json_encode($data));
		}
        
    }
