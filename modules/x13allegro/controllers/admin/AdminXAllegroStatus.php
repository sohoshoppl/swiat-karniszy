<?php

if (!defined('_PS_VERSION_')) {
	exit;
}

require_once dirname(__FILE__) . '/../../x13allegro.php';
include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';

class AdminXAllegroStatusController extends XAllegroController {

	public function initContent() {
		
		$this->context->smarty->assign(array(
			'statuses'     => XAllegroStatus::getAll(),
			'order_states' => OrderState::getOrderStates($this->context->cookie->id_lang),
			'form_action'  => self::$currentIndex.'&token='.$this->token,
			'action'       => 'set_status_payment',
		));
		
		if($this->ps_version == 1.5) {
			if(Tools::getValue('action') == 'set_status_payment') {
				$this->processSetStatusPayment();
				Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&conf=4');
			}
		}
		
		parent::initContent();
	}

	public function initToolbarTitle() {
		parent::initToolbarTitle();
		$this->toolbar_title[] = $this->l('Statusy dla płatności');
	}

	public function processPost() {
		d('oko');
	}

	public function processSetStatusPayment() {
		if(Tools::getValue('id_order_state')) {
			if(XAllegroStatus::updateOrderStates(Tools::getValue('id_order_state'))) {
				$this->confirmations[] = $this->l('Statusy zostały przypisane.');
			}
		}
		else {
			$this->errors[] = $this->l('Nie przesłano statusów');
		}
	}

}

?>