<?php

    if (!defined('_PS_VERSION_')) {
        exit;
    }

    require_once dirname(__FILE__) . '/../../x13allegro.php';
    include dirname(__FILE__) . '/../../classes/XAllegroController' . X13_ION_VERSION . '.php';
    
    class AdminXAllegroOverlayController extends XAllegroController
    {
        
        protected $_checkAccount = false;

        public function __construct()
        {
	        //Dla presty powyżej 1.7.0.0 inicjujemy dodatkowo obsługę tłumaczeń.
            if(Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->translator = Context::getContext()->getTranslator();
            }
	        
	        
            $this->table = 'xallegro_overlay';
            $this->className = 'XAllegroOverlay';
            $this->lang = false;
            
            $this->addRowAction('edit');
            $this->addRowAction('delete');
            $this->bulk_actions = array('delete' => array('text' => $this->l('Usuń zaznaczone'), 'confirm' => $this->l('Usunąć zaznaczone elementy?')));
            
            $this->fieldImageSettings = array(
                'name' => 'image',
                'dir' => 'overlay'
            );
            $this->imageType = 'png';
            
            $this->fields_list = array(
                'id_xallegro_overlay' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'width' => 20
                ),
                'name' => array(
                    'title' => $this->l('Nazwa ramki'),
                    'width' => 'auto'
                ),
                'default' => array(
                    'title' => $this->l('Domyślny'),
                    'active' => 'default',
                    'width' => 100,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                ),
                'active' => array(
                    'title' => $this->l('Aktywny'),
                    'active' => 'active',
                    'width' => 100,
                    'class' => 'fixed-width-sm',
                    'align' => 'center',
                    'type' => 'bool'
                )
            );
            
            parent::__construct();
        }
        
        public function initToolbarTitle()
        {
            if (in_array($this->display, array('edit', 'add'))) {
                $this->toolbar_scroll = false;
            }
            
            parent::initToolbarTitle();
        }
        
        public function processAdd()
        {
            parent::processAdd();
            
            $this->redirect_after = self::$currentIndex.'&'.$this->identifier.'='.$this->object->id.'&conf=3&update'.$this->table.'&token='.$this->token;
        }

        public function postProcess()
        {
            parent::postProcess();
            
			if((int)Tools::getValue('default') == 1) {
				$this->object->setDefault();
			}
            
        }

        public function renderList()
        {
            return parent::renderList();
        }

        public function renderForm()
        {
            $this->fields_form = array(
                'legend' => array(
                    'title' => $this->l('Podstawowe dane'),  
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_' . $this->table
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa'),
                        'name' => 'name',
                        'size' => 33,
                        'required' => true,
                    ),
                    array(           
                        'type' => 'file',
                        'label' => $this->l('Obrazek'),
                        'name' => 'image',
                        'display_image' => true,
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Domyślna ramka'),
                        'name' => 'default',
                        'required' => false,
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'desc' => $this->l('Automatycznie wybierana podczas wystawiania przedmiotów'),
                        'default_value' => 1
                    ),
                    array(
                        'type' => $this->bootstrap ? 'switch' : 'radio',
                        'label' => $this->l('Aktywne ramka'),
                        'name' => 'active',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        ),
                        'default_value' => 1
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz')
                )
            );
            
            if ($overlay = $this->loadObject(true)) {
                $image = ImageManager::thumbnail(_PS_IMG_DIR_.'overlay/'.$overlay->id.'.png', $this->table.'_'.(int)$overlay->id.'.'.$this->imageType, 2000, $this->imageType, true);

                $this->fields_form['input'][2]['files'][] = array(
                    'image' => $image,
                    'type' => 'image',
                    'size' => $image ? filesize(_PS_IMG_DIR_.'overlay/'.$overlay->id.'.png') / 1000 : false
                );
                
                $this->fields_form['input'] = array_merge($this->fields_form['input'], array(
                    array(
                        'type' => 'hidden',
                        'name' => 'X1'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'X2'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'Y1'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'Y2'
                    )
                ));
                
                $image = ImageManager::thumbnail(_PS_IMG_DIR_.'overlay/'.$overlay->id.'.png', $this->table.'_'.(int)$overlay->id.'.'.$this->imageType, 2000, $this->imageType, true);

                $this->fields_value['image'] = array(
                    'image' => $image,
                    'size' => $image ? filesize(_PS_IMG_DIR_.'overlay/'.$overlay->id.'.png') / 1000 : false
                );
            }
            
            return parent::renderForm();
        }
    }
