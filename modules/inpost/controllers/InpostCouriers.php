<?php
/**
 *
 * NOTICE OF LICENSE
 *
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class InpostCouriers extends Module
{
    public $name = 'inpost';
    public $keys = ['date_label_generation', 'order_id', 'parcel_no', 'label_no', 'label_downloaded'];
    const FILE = 'inpostcouriers';
    public function __construct()
    {
        parent::__construct();
    }

    public function resetFilters()
    {
        if (Tools::isSubmit('submitResetinpostCouriers')) {
            $_POST['inpostCouriersFilter_courier_id'] = '';
            $_POST['inpostCouriersFilter_courier_status'] = '';
            $_POST['local_inpostCouriersFilter_courier_from'][0] = '';
            $_POST['local_inpostCouriersFilter_courier_from'][1] = '';
            $_POST['inpostCouriersFilter_courier_from'][0] = '';
            $_POST['inpostCouriersFilter_courier_from'][1] = '';
            $_POST['inpostCouriersFilter_courier_to'][0] = '';
            $_POST['inpostCouriersFilter_courier_to'][1] = '';
            $_POST['inpostCouriersFilter_dispatch_point'] = '';
            $_POST['inpostCouriersFilter_courier_api_id'] = '';
            $_POST['inpostCouriersFilter_created_at'][0] = '';
            $_POST['inpostCouriersFilter_created_at'][1] = '';
        }
    }
    public function renderCouriersActions()
    {
        return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/couriers_actions.tpl');
    }
    public function defaultSelectedFilters()
    {
        if (!Tools::getValue('inpostCouriersFilter_created_at')) {
            $_POST['inpostCouriersFilter_created_at'][0] = date('Y-m-d');
            $_POST['inpostCouriersFilter_created_at'][1] = '';
            $_POST['submitFilter'] = '';
        }

    }
    public function generateTableCouriers()
    {

        $this->fields_list = array(
            'created_at' => array(
                'title' => $this->l('Created at', self::FILE),
                'type' => 'datetime'
            ),
            'courier_api_id' => array(
                'title' => $this->l('Courier number', self::FILE),
            ),
            'courier_status' => array(
                'title' => $this->l('Status', self::FILE),
            ),
            'courier_from' => array(
                'title' => $this->l('Date from picking', self::FILE),
                'type' => 'datetime',
            ),
            'courier_to' => array(
                'title' => $this->l('Date to picking', self::FILE),
                'type' => 'datetime',
            ),
            'dispatch_point' => array(
                'title' => $this->l('Dispatch point', self::FILE),
            )

        );

        $helper = new HelperList();

        $helper->actions = array('edit');
        $helper->orderBy = Tools::getValue('inpostCouriersOrderby');
        $helper->orderWay = Tools::strtoupper(Tools::getValue('inpostCouriersOrderway'));

        $helper->identifier = 'courier_api_id';
        $helper->show_toolbar = false;
        $helper->title = 'Couriers';
        $helper->table = 'inpostCouriers';
        $helper->shopLinkType = '';
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&couriers';

        $this->defaultSelectedFilters();
        $this->resetFilters();


        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $page = (int)Tools::getValue('submitFilterCouriers');
        } else {
            $page = (int)Tools::getValue('submitFilterinpostCouriers');
        }

        if (!$page) {
            $page = 1;
        }

        $pagination = (int)Tools::getValue('inpostCouriers_pagination', 50);

        $start = ($pagination * $page) - $pagination;


        $contents = $this->getListContent(
            $helper->orderBy,
            $helper->orderWay,
            $pagination,
            $start
        );

        foreach ($contents as &$content) {
            if (isset($content['dispatch_point_id'])) {
                $content['dispatch_point'] = Db::getInstance()->getValue('SELECT `name`
                  FROM '._DB_PREFIX_.'inpost_dispatch_points WHERE id='.(int)$content['dispatch_point_id']);
            }
        }
        $helper->listTotal= $this->getCouriersCount();
        $this->context->smarty->assign(array(
            'total' => $helper->listTotal
        ));
        if ($helper->listTotal > 0) {
            $helper->bulk_actions = array(
                'download_manifest' => array('text' => 'Download selected manifest'),
            );
        }
        return $helper->generateList($contents, $this->fields_list);

    }

    public function getListContent($orderBy = '', $orderWay = 'ASC', $limit = 50, $start = 0)
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."inpost_couriers iic
                  LEFT JOIN "._DB_PREFIX_."inpost_dispatch_points iidp ON iidp.id=iic.dispatch_point_id";

        $filtersCouriers = $this->filter();

        if ($filtersCouriers) {
            $sql .= ' '.$filtersCouriers;
        }

        if ($orderBy) {
            $sql.= ' order by '.$orderBy;
        }
        if ($orderWay) {
            $sql.= ' '.$orderWay;
        }
        if ($limit) {
            $sql .= ' LIMIT '.$limit;
        }
        if ($start) {
            $sql .= ' OFFSET '.$start;
        }

        $couriers = Db::getInstance()->executeS($sql);
        foreach ($couriers as &$courier) {
            $packages = InpostLabels::getCourierLabels($courier['courier_api_id']);
            $courier['packages'] = $packages;
        }

        return $couriers;
    }

    public function filter()
    {

        $filters = [];

        if (Tools::getValue('local_inpostCouriersFilter_courier_from')[0]) {
            $filters[] = 'iic.courier_from >= "'.Tools::getValue('local_inpostCouriersFilter_courier_from')[0].' 00:00:01"';
        }
        if (Tools::getValue('local_inpostCouriersFilter_courier_from')[1]) {
            $filters[] = 'iic.courier_from <= "'.Tools::getValue('local_inpostCouriersFilter_courier_from')[1].' 23:59:59"';
        }
        if (Tools::getValue('inpostCouriersFilter_courier_to')[0]) {
            $filters[] = 'iic.courier_to >= "'.Tools::getValue('inpostCouriersFilter_courier_to')[0].' 00:00:01"';
        }
        if (Tools::getValue('inpostCouriersFilter_courier_to')[1]) {
            $filters[] = 'iic.courier_to <= "'.Tools::getValue('inpostCouriersFilter_courier_to')[1].' 23:59:59"';
        }
        if (Tools::getValue('inpostCouriersFilter_created_at')[0]) {
            $filters[] = 'iic.created_at >= "'.Tools::getValue('inpostCouriersFilter_created_at')[0].' 00:00:01"';
        }
        if (Tools::getValue('inpostCouriersFilter_created_at')[1]) {
            $filters[] = 'iic.created_at <= "'.Tools::getValue('inpostCouriersFilter_created_at')[1].' 23:59:59"';
        }
        if (Tools::getValue('inpostCouriersFilter_courier_id')) {
            $filters[] = ' iic.courier_id = '.Tools::getValue('inpostCouriersFilter_courier_id');
        }
        if (Tools::getValue('inpostCouriersFilter_courier_status')) {
            $filters[] = ' iic.courier_status like "%'.Tools::getValue('inpostCouriersFilter_courier_status').'%"';
        }
        if (Tools::getValue('inpostCouriersFilter_dispatch_point')) {
            $filters[] = ' iidp.name like "%'.Tools::getValue('inpostCouriersFilter_dispatch_point').'%"';
        }

        if (Tools::getValue('inpostCouriersFilter_courier_api_id')) {
            $filters[] = 'iic.courier_api_id ='.Tools::getValue('inpostCouriersFilter_courier_api_id');
        }


        if ($filters) {
            return ' WHERE '.implode(' AND ', $filters);
        }
        return false;
    }

    public function getCouriersCount()
    {
        $sql = "SELECT COUNT(courier_id) FROM "._DB_PREFIX_."inpost_couriers iic LEFT JOIN "._DB_PREFIX_."inpost_dispatch_points iidp ON iidp.id=iic.dispatch_point_id";
        $this->defaultSelectedFilters();
        $filtersCouriers = $this->filter();

        if ($filtersCouriers) {
            $sql .= ' '.$filtersCouriers;
        }

        return Db::getInstance()->getValue($sql);
    }
    public function pagination($pagination_helper)
    {

        $pagination = 50;
        if (in_array((int)Tools::getValue('inpostCouriers_pagination'), $pagination_helper)) {
            $pagination = (int)Tools::getValue('inpostCouriers_pagination');
        } elseif (isset($this->context->cookie->inpost_packages_pagination) && $this->context->cookie->inpost_packages_pagination) {
            $pagination = $this->context->cookie->inpost_packages_pagination;
        }

        return $pagination;
    }
    public static function saveNewCourier($response)
    {
        $dispatchpointId = Db::getInstance()->getValue("SELECT id FROM "._DB_PREFIX_."inpost_dispatch_points where name = '".$response->dispatch_point_name."'");
        Db::getInstance()->insert('inpost_couriers', array(
            'courier_status' => $response->status,
            'created_at' => date('Y-m-d H:i:s', strtotime($response->created_at)),
            'courier_from' => $response->dispatch_date->from,
            'courier_to' =>$response->dispatch_date->to,
            'courier_api_id' => $response->id,
            'dispatch_point_id' => $dispatchpointId,
            'reference' => $response->reference,
            'comment' => $response->comment
        ));
    }
    public static function saveNewCourierX($response, $courier_api_id)
    {
        $dispatchpointId = Db::getInstance()->getValue("SELECT id FROM "._DB_PREFIX_."inpost_dispatch_points where x_border_id = '".$response->dispatch_point_id."'");
        Db::getInstance()->insert('inpost_couriers', array(

            'created_at' => date('Y-m-d H:i:s', strtotime($response->created_on)),
            'courier_from' => $response->date_time_from,
            'courier_to' =>$response->date_time_to,
            'courier_api_id' => $courier_api_id,
            'dispatch_point_id' => $dispatchpointId,
            'comment' => $response->comment,
            'x_border' => true
        ));
    }
    public static function getCourierByCourierApiId($courier_api_id)
    {
        if ($courier_api_id) {
            return Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."inpost_couriers
                                                WHERE courier_api_id =".$courier_api_id);
        }

        return false;
    }
}
