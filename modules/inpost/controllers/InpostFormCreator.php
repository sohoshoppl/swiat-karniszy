<?php
/**
 *
 * NOTICE OF LICENSE
 *
 */
if (!defined('_PS_VERSION_')) {
    exit;
}


require_once(dirname(__FILE__) . '/InpostPackages.php');
class InpostFormCreator extends Module
{
    const FILE = 'inpostformcreator';

    public function __construct()
    {
        $this->name = 'inpost';
        parent::__construct();
    }


    public function accountInfoForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Account information', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' =>$this->l('E-mail', self::FILE),
                        'name' => 'INPOST_LOGIN',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Token', self::FILE),
                        'name' => 'INPOST_PASSWORD',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('API URL', self::FILE),
                        'name' => 'EE_INPOST_API_URL',
                        'required' => true
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'INPOST_API_URL_POLAND',
                        'required' => true
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'INPOST_API_URL_FRANCE',
                        'required' => true
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'INPOST_API_URL_ITALY',
                        'required' => true
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'INPOST_COUNTRY',
                        'required' => true,
                        'label' =>$this->l('Select country', self::FILE),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 0,
                                    'name' =>$this->l('Select country', self::FILE)
                                ),
                                array(
                                    'id' => 1,
                                    'name' =>$this->l('Poland', self::FILE)
                                ),
                                array(
                                    'id' => 2,
                                    'name' =>$this->l('France', self::FILE)
                                ),
                                array(
                                    'id' => 3,
                                    'name' =>$this->l('Italy', self::FILE)
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),

                ),
                'submit' => array(
                    'title' =>$this->l('Save', self::FILE),
                )
            ),
        );
    }

    public function availableServicesForm()
    {
        $paymentModules = Module::getPaymentModules();
        $payments = [];

        foreach ($paymentModules as $payment) {
            $moduleName = Module::getInstanceByName($payment['name']);
            $payments[] = array(
                'id' => $payment['name'],
                'name' => $payment['name'] .' - '. $moduleName->displayName,
                'val' => 1
            );
        }
        $data = array();
        $data[] =  array(
            'id' => 'PARCELS_LOCKERS',
            'name' => $this->l('Parcel locker', self::FILE),
            'val' => '1'
        );
        $data[] = array(
            'id' => 'RETURNS',
            'name' => $this->l('Returns', self::FILE),
            'val' => '1'
        );
        if (Configuration::get('INPOST_COUNTRY') == 1) {
            $data[] = array(
                'id' => 'CROSSBORDER',
                'name' => $this->l('CrossBorder', self::FILE),
                'val' => '1'
            );
        }

        if (Configuration::get('INPOST_COUNTRY') == 1 || Configuration::get('INPOST_COUNTRY') == 3) {
            $data[] = array(
                'id' => 'PARCELS_LOCKERS_COD',
                'name' => $this->l('Parcel locker COD', self::FILE),
                'val' => '1'
            );
        }

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Select available functions', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'checkbox',
                        'name' => 'INPOST_AVAILABLE_SERVICES',
                        'values' => array(
                            'query' => $data,

                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
//                    array(
//                        'type' => 'text',
//                        'label' => $this->l('IBAN number', self::FILE),
//                        'name' => 'INPOST_PARCERLS_LOCKERS_COD_IBAN',
//                        'class' => 'test',
//                        'required' => false,
////                        'desc' =>   $ibanDescription,
//                    ),

                    array(
                        'type' => 'checkbox',
                        'name' => 'INPOST_PARCEL_LOCKERS_COD_PAYMENT',
                        'class' => 'parcel_locker_cod_payment',
                        'values' => array(
                            'query' => $payments,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'INPOST_PARCEL_LOCKERS_PAYMENT',
                        'class' => 'parcel_locker_payment',
                        'values' => array(
                            'query' => $payments,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'INPOST_X_BORDER_PAYMENT',
                        'class' => 'xborder_payment',
                        'values' => array(
                            'query' => $payments,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Return service URL', self::FILE),
                        'name' => 'INPOST_PARCERLS_LOCKERS_RETURNS_URL',
                        'required' => false,
                        'class' => 'inpost_return_url'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Return service additional description', self::FILE),
                        'name' => 'INPOST_PARCERLS_LOCKERS_RETURNS_DESCRIPTION',
                        'required' => false,
                        'class' => 'inpost_return_description'
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', self::FILE),
                )
            ),
        );
    }
    public function senderAddressForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Sending packages', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('IBAN number', self::FILE),
                        'name' => 'INPOST_PARCERLS_LOCKERS_COD_IBAN',
                        'class' => 'test2',
                        'required' => false
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', self::FILE),
                )
            ),
        );
    }
    public function defaultParcelSizeForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Default parcel size', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'radio',
                        'name' => 'INPOST_DEFAULT_PARCEL_SIZE',

                        'values' => array(
                            array(
                                'id' => 'A',
                                'value' => 'A',
                                'label' => $this->l('Size A: 8 x 38 x 64 cm', self::FILE)
                            ),
                            array(
                                'id' => 'B',
                                'value' => 'B',
                                'label' => $this->l('Size B: 19 x 38 x 64 cm', self::FILE)
                            ),
                            array(
                                'id' => 'C',
                                'value' => 'C',
                                'label' => $this->l('Size C: 41 x 38 x 64 cm', self::FILE)
                            ),
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save', self::FILE),
                )
            ),
        );
    }

    public function defaultOrderStatusForm()
    {
        $fieldToRender = array();
        $fieldToRender[] = array(
            'id' => '',
            'value' => '',
            'name' => '',
        );

        foreach (OrderStateCore::getOrderStates($this->context->language->id) as $status) {
            $fieldToRender[] = array(
                'id' => $status['id_order_state'],
                'value' => $status['id_order_state'],
                'name' => $status['name']
            );
        }

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Statuses', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'name' => 'INPOST_DEFAULT_STATUS_AFTER_LABEL_GENERATION',
                        'label' => $this->l('Order state after label generation', self::FILE),
                        'options' => array(
                            'query' =>$fieldToRender,
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'INPOST_DEFAULT_STATUS_AFTER_PARCEL_DELIVERY', //do wybrania pozniej
                        'label' => $this->l('Order state after parcel delivery', self::FILE),
                        'options' => array(
                            'query' =>$fieldToRender,
                            'id' => 'id',
                            'name' => 'name',
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save', self::FILE),
                )
            ),
        );
    }

    public function renderSenderAddressForm()
    {
        $this->context->smarty->assign(
            InpostModel::getSenderData()
        );
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/1.5/sender_address.tpl');
        } else {
            return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/sender_address.tpl');
        }

    }

    public function loadPriceRules()
    {
        if (Configuration::get('INPOST_PLFS')) {
            $parcelsLockersFreeShip = number_format(Configuration::get('INPOST_PLFS'), 2, '.', '');
        } else {
            $parcelsLockersFreeShip = '';
        }
        if (Configuration::get('INPOST_PLCOSFS')) {
            $parcelsLockersCodFreeShip = number_format(
                Configuration::get('INPOST_PLCOSFS'),
                2,
                '.',
                ''
            );
        } else {
            $parcelsLockersCodFreeShip = '';
        }
        $this->context->smarty->assign(
            array(
                'price_rules' => InpostModel::getAllPriceRules(),
                'weight_rules' => InpostModel::getAllWeightRules(),
                'price_rules_cod' => InpostModel::getAllPriceRulesCod(),
                'weight_rules_cod' => InpostModel::getAllWeightRulesCod(),
                'selected' => InpostModel::getSelectedPriceRule(),
                'flat_price' => InpostModel::parcelsLockersGetFlatPrice(),
                'flat_price_cod' => InpostModel::parcelsLockersCodGetFlatPrice(),
                'PARCELS_LOCKERS_FREE_SHIPPING' => $parcelsLockersFreeShip,
                'PARCELS_LOCKERS_COD_FREE_SHIPPING' => $parcelsLockersCodFreeShip,
                'INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS' => Configuration::get('INPOST_ASPL'),
                'INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD' => Configuration::get('INPOST_ASPLCOD'),
                'INPOST_CUSTOMER_REFERENCE' => Configuration::get('INPOST_CUSTOMER_REFERENCE'),
                 'currency' => new Currency(Configuration::get('PS_CURRENCY_DEFAULT')),
                'tax_rules_groups' => TaxRulesGroup::getTaxRulesGroups(true),
                'selected_tax' => Configuration::get('INPOST_STI'),
                'cart_value_percent_cod' => Configuration::get('INPOST_PERCENT_CART_VALUE_COD')
            )
        );
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/1.5/price_rules.tpl');
        } else {
            return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/price_rules.tpl');
        }

    }
    public function loadXBorderPriceRules()
    {

        $this->context->smarty->assign(
            array(
                'INPOST_AVAILABLE_SERVICES_CROSSBORDER' => Configuration::get('INPOST_ASC'),
                'filter_smarty' => array(
                    'cross_border_receiver_country' => Tools::getValue('cross_border_receiver_country', ''),
                    'cross_border_sender_country' => Tools::getValue('cross_border_sender_country', '')
                ),
                'crossborder_prices' => InpostModel::getXBorderPrices(),
                'filter_url' =>'index.php?controller=AdminModules&token='.Tools::getValue('token').'&configure=inpost&prices'

            )
        );

        return $this->context->smarty->fetch(INPOST_TPL_DIR.'admin/crossborder/price_rules.tpl');

    }
    public function leadingAndMenu($module_url, $disabled)
    {
        $packages = new InpostPackages();
        $labels = new InpostLabels();
        $configSelected = false;
        if ((!Tools::isSubmit('packages')
                && !Tools::isSubmit('labels')
                && !Tools::isSubmit('couriers')
                && !Tools::isSubmit('dispatch_points')
                && !Tools::isSubmit('prices'))
                || Tools::isSubmit('configuration')) {
            $configSelected = true;
        }
        $a = array(
            'configuration' => array(
                'name' => $this->l('Configuration', self::FILE),
                'href' => $module_url . '&configuration',
                'active' => false,
                'imgclass' => 'icon-cog',
                'selected' => $configSelected,
                'disabled' => (bool)$disabled,
                'count' => false
            ),
            'packages' => array(
                'name' => $this->l('Parcels', self::FILE),
                'href' => $module_url . '&packages',
                'active' => false,
                'imgclass' => 'icon-cube',
                'selected' => Tools::isSubmit('packages'),
                'disabled' => (bool)$disabled,
                'count' => $packages->getPackagesCount()
            ),
            'labels' => array(
                'name' => $this->l('Labels', self::FILE),
                'href' => $module_url . '&labels',
                'active' => false,
                'imgclass' => 'icon-barcode',
                'selected' => Tools::isSubmit('labels'),
                'disabled' => (bool)$disabled,
                'count' => $labels->getLabelsCount()
            ),

        );

        if ((int)Configuration::get('INPOST_COUNTRY') === 1) {
            $a['couriers'] = array(
                'name' => $this->l('Manifests', self::FILE),
                self::FILE,
                'href' => $module_url . '&couriers',
                'active' => false,
                'imgclass' => 'icon-file',
                'selected' => Tools::isSubmit('couriers'),
                'disabled' => (bool)$disabled,
                'count' => ''
            );

        }
        if (InpostDispatchPoints::isActive()) {
            $a['dispatch_points'] = array(
                'name' => $this->l('Dispatch points', self::FILE),
                'href' => $module_url . '&dispatch_points',
                'active' => false,
                'imgclass' => 'icon-map-marker',
                'selected' => Tools::isSubmit('dispatch_points'),
                'disabled' => (bool)$disabled,
                'count' => false
            );
        }
        $a['prices'] = array(
            'name' => $this->l('Price rules', self::FILE),
            'href' => $module_url . '&prices',
            'active' => false,
            'imgclass' => 'icon-tags',
            'selected' => Tools::isSubmit('prices'),
            'disabled' => (bool)$disabled,
            'count' => false
        );

        return $a;
    }

    public function dispatchPointForm()
    {
        $countries = Country::getCountries($this->context->language->id);
        $countryToSelect = [];
        foreach ($countries as $country) {
            $countryToSelect[] = [
                'id' => $country['id_country'],
                'name' => $country['name']
            ];
        }

        $fields = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Add new dispatch point', self::FILE),
                    'icon' => 'icon-user'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Name', self::FILE),
                        'name' => 'DISPATCH_NAME',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Postal code', self::FILE),
                        'name' => 'DISPATCH_POSTCODE',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('City', self::FILE),
                        'name' => 'DISPATCH_CITY',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Street', self::FILE),
                        'name' => 'DISPATCH_STREET',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Building No.', self::FILE),
                        'name' => 'DISPATCH_BUILDING_NO',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Flat No. ', self::FILE),
                        'name' => 'DISPATCH_FLAT_NO',
                        'required' => false
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Phone', self::FILE),
                        'name' => 'DISPATCH_PHONE_NO',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('E-mail', self::FILE),
                        'name' => 'DISPATCH_EMAIL',
                    ),

                    array(
                        'type' => 'text',
                        'label' =>$this->l('Comment', self::FILE),
                        'name' => 'DISPATCH_COMMENTS',
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$this->l('Office hours', self::FILE),
                        'name' => 'DISPATCH_OFFICE_HOURS',
                        'desc' => (new InpostHelper())->htmlAssign('desc', array(
                            'eg' => $this->l('eg. 9am - 5am', self::FILE),
                            'required' => $this->l('Required field', self::FILE)
                        ))
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'DISPATCH_HREF'
                    ),
                ),
                'submit' => array(
                    'title' =>$this->l('Save', self::FILE),
                )
            ),
        );

        return $fields;
    }
}
