{**
*
* NOTICE OF LICENSE
*
*}
{if $total > 0}
    <div id="actions_couriers" style="display: none;">
        {*<button class="btn btn-success" style="margin-left: 10px;" onclick="sendBulkAction($(this).closest('form').get(0), 'download_manifests');">*}
            {*{l s='Download selected manifests' mod='inpost'}*}
        {*</button>*}
    </div>

    {literal}
        <script type="text/javascript">
            $('#actions_couriers').insertAfter('.btn-group.bulk-actions.dropup');
            $('.btn-group.bulk-actions.dropup').css('float', 'left');
            $('input[name="inpostCouriersBox[]"]').click(function(){
                if ($(this).is(':checked'))
                    $('#actions_couriers').show();
                else
                    $('#actions_couriers').hide();
            })
            $('.bulk-actions .dropdown-menu a').click(function(){
                $('input[name="inpostCouriersBox[]"]').each(function(){
                    if ($(this).is(':checked'))
                        $('#actions_couriers').show();
                    else
                        $('#actions_couriers').hide();
                })
            })
        </script>

    {/literal}
{/if}