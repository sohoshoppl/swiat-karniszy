{**
*
* NOTICE OF LICENSE
*
*}
<div class="panel" id="fieldset_2_2">

    <div class="panel-heading">
        <i class="icon-user"></i> {l s='Defult parcel size' mod='inpost'}
    </div>
    <div class="form-wrapper">

        <div class="form-group">


            <div class="col-lg-9 col-lg-offset-3">

                <div class="radio ">
                    <label><input type="radio" name="INPOST_DEFAULT_PARCEL_SIZE" id="A" value="A" checked="checked">Rozmiar:
                        A</label>
                </div>
                <div class="radio ">
                    <label><input type="radio" name="INPOST_DEFAULT_PARCEL_SIZE" id="B" value="B">Rozmiar: B</label>
                </div>
                <div class="radio ">
                    <label><input type="radio" name="INPOST_DEFAULT_PARCEL_SIZE" id="C" value="C">Rozmiar: C</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" value="1" id="configuration_form_submit_btn_2" name="btnSubmit"
                class="btn btn-default pull-right">
            <i class="process-icon-save"></i> Zapisz
        </button>
    </div>

</div>