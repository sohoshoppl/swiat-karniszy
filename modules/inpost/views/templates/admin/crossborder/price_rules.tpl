{**
*
* NOTICE OF LICENSE
*
*}
{if $INPOST_AVAILABLE_SERVICES_CROSSBORDER}
<div class="alert alert-warning" id="inpostCouriers-empty-filters-alert" style="display:none;">Wypełnij przynajmniej
    jedno pole aby wyszukiwać w tej liście.
</div>
<form method="post" action="" class="form-horizontal clearfix" id="form-inpostCouriers">
    <input type="hidden" name="x_border_prices" value="1">

    <input type="hidden" name="page" value="1">
    <input type="hidden" name="selected_pagination" value="50">

    <div class="panel col-lg-12">
        <div class="panel-heading">
            {l s='CrossBorder prices' mod='inpost'}
        </div>
        <style>
            @media (max-width: 992px) {
                .table-responsive-row td:nth-of-type(1):before {
                    content: "Courier id";
                }

                .table-responsive-row td:nth-of-type(2):before {
                    content: "Status";
                }

                .table-responsive-row td:nth-of-type(3):before {
                    content: "Date from";
                }

                .table-responsive-row td:nth-of-type(4):before {
                    content: "Date from";
                }
            }
        </style>
        <div class="table-responsive-row clearfix">
            <table class="table inpostCouriers">
                <thead>
                <tr class="nodrag nodrop">
                    <!-- <th class="">

						<span class="title_box">{l s='Sender Country' mod='inpost'}
                            <a href="{$filter_url}&OrderBy=from_country&OrderWay=desc&token={getAdminToken tab='AdminModules'}"><i
                                        class="icon-caret-down"></i></a>
                            <a href="{$filter_url}&OrderBy=from_country&OrderWay=asc&token={getAdminToken tab='AdminModules'}"><i
                                        class="icon-caret-up"></i></a>
                        </span>
                    </th> -->
                    <th class="">
						<span class="title_box">{l s='Receiver Country' mod='inpost'}
                            <a href="{$filter_url}&OrderBy=to_country&OrderWay=desc&token={getAdminToken tab='AdminModules'}"><i
                                        class="icon-caret-down"></i></a>
                            <a href="{$filter_url}&OrderBy=to_country&OrderWay=asc&token={getAdminToken tab='AdminModules'}"><i
                                        class="icon-caret-up"></i></a>
                            </span>
                    </th>
                    <th class="">
						<span class="title_box">{l s='Receiver Method' mod='inpost'}</span>
                    </th>
                    <!-- <th class="">
                        <span class="title_box">{l s='Sender Method' mod='inpost'}</span>
                    </th> -->
                    <th class="">
                        <span class="title_box">{l s='1 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='2 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='5 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='10 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='15 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='20 KG' mod='inpost'}</span>
                    </th>
                    <th>
                        <span class="title_box">{l s='25 KG' mod='inpost'}</span>
                    </th>
                </tr>
                </thead>
                
                <tbody>
                {foreach $crossborder_prices as $x_price}
                    <tr class="odd" id="price{$x_price.id|escape:'htmlall':'UTF-8'}">
                        <!-- <td class="pointer text-center">{$x_price.from_country|escape:'htmlall':'UTF-8'}</td> -->
                        <td class="pointer text-center">{$x_price.to_country|escape:'htmlall':'UTF-8'}</td>
                        <td class="pointer text-center">{$x_price.recipient_method|escape:'htmlall':'UTF-8'}</td>
                        <!-- <td class="pointer text-center">{$x_price.sender_method|escape:'htmlall':'UTF-8'}</td> -->
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][1]" type="text" value="{$x_price.weight.1|escape:'htmlall':'UTF-8'}">
                        </td>
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][2]" type="text" value="{$x_price.weight.2|escape:'htmlall':'UTF-8'}">
                        </td>
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][5]" type="text" value="{$x_price.weight.5|escape:'htmlall':'UTF-8'}">
                        </td>
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][10]" type="text"
                                                   value="{$x_price.weight.10|escape:'htmlall':'UTF-8'}"></td>
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][15]" type="text"
                                                   value="{$x_price.weight.15|escape:'htmlall':'UTF-8'}"></td>
                        <td class="pointer"><input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][20]" type="text"
                                                   value="{$x_price.weight.20|escape:'htmlall':'UTF-8'}"></td>
                        <td class="pointer">

                          {if $x_price.recipient_method|strtoupper == "DO PACZKOMATU"}
                          <input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][25]" type="text"
                                                   value="NIEDOSTĘPNE" disabled="disabled">
                         {else}
                         <input name="type[{$x_price.id|escape:'htmlall':'UTF-8'}][25]" type="text"
                                                  value="{$x_price.weight.25|escape:'htmlall':'UTF-8'}">
                          {/if}

                         </td>
                    </tr>
                {/foreach}

                </tbody>

            </table>
        </div>
        <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
        </div>
        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn_1" name="btnSubmitCorssBorder"
                    class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='inpost'}
            </button>
        </div>
    </div>


</form>

{/if}
