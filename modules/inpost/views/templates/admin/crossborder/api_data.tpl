{**
*
* NOTICE OF LICENSE
*
*}
<div class="modal fade" id="crossBorderModal" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">CrossBorder</h4>
            </div>

            <div class="modal-body form-horizontal">
                <form id="x_border_data" method="POST" action="">
                    <div class="alert alert-warning x_alert" style="display: none;">

                    </div>
                    <input type="hidden" value="x_border_data" name="action">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Login</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="x_login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Haslo</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" name="x_password">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" id="check_x_border_data">Check data</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    var inpost_ajax_request = '{$inpost_ajax_request}';
</script>
{literal}
<script type="text/javascript">
    $(function() {
        $('#check_x_border_data').click(function (e) {
            e.preventDefault();
            $(this).text('Checking data...');
            $('.x_alert').text('').hide();
            $.ajax({
                type: "POST",
                async: true,
                url: inpost_ajax_request,
                dataType: "json",
                global: false,
                data: $('#x_border_data').serialize(),
                success: function (resp) {

                    if (resp.error == 1)
                    {
                        $('.x_alert').text('Wrong data').show();
                        $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').attr('checked', false);
                        $('#check_x_border_data').text('Check data');
                    }
                    else
                    {
                        $('#crossBorderModal').modal('hide');
                        $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').attr('checked', true);
                        $('#check_x_border_data').text('Check data');
                    }



                }
            })
        })
    })

</script>
{/literal}
