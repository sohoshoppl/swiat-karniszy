{**
*
* NOTICE OF LICENSE
*
*}
{if $filterLabelGenerated != '1' && $total > 0}
    <div id="actions_packages" style="display: none">
        <button class="btn btn-success" style="margin-left: 10px;" onclick="if (confirm(generateLabelsTranslations))sendBulkAction($(this).closest('form').get(0), 'generate_sticker');">
            {l s='Generate labels for selected packages' mod='inpost'}
        </button>
        <Br />
        <button class="btn btn-danger" style="margin: 10px 0px 0px 10px;" onclick="if (confirm(cancelParcels))sendBulkAction($(this).closest('form').get(0), 'cancel_parcel');">
            {l s='Cancel selected parcels' mod='inpost'}
        </button>
    </div>
    <script>
        var inpost_ajax_request = '{$inpost_ajax_request}';
        var generateLabelsTranslations = "{l s='Generate labels for selected packages?' mod='inpost'}";
        var cancelParcels = "{l s='Cancel the selected parcel?' mod='inpost'}";
        var showtext  = "{l s='Show' mod='inpost'}";
        $('.to_hide').hide();
        $('input[name="inpost_packagesFilter_parcel_no"]').parent().hide();
        {literal}
            $('#actions_packages').insertAfter('.btn-group.bulk-actions.dropup');
            $('.btn-group.bulk-actions.dropup').css('float', 'left');
            $('input[name="inpost_packagesBox[]"]').click(function(){
                showAndHidePackagesActions($(this))
            })
            $('.bulk-actions .dropdown-menu a').click(function(){
                $('input[name="inpost_packagesBox[]"]').each(function(){
                    showAndHidePackagesActions($(this))
                })
            })
        $('table.inpost_packages tbody tr td.label_generated_class a').each(function(){
            var $this = $(this);
            if ($this.children('i.icon-remove.hidden').length == 1)
            {
                tr = $this.parents('tr');
                $(tr).children('td:last').addClass('changeTextToShow')
            }
        })
        $('.changeTextToShow div a').html('<i class="icon-pencil"></i> '+showtext);
        function showAndHidePackagesActions(checkEL)
        {
            if (checkEL.is(':checked') || $('input[name="inpost_packagesBox[]"]:checked').length > 0)
                $('#actions_packages').show();
            else
                $('#actions_packages').hide();
        }
        showAndHidePackagesActions($('input[name="inpost_packagesBox[]"]'))
        {/literal}
    </script>

{/if}
