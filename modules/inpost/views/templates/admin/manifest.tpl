{**
*
* NOTICE OF LICENSE
*
*}
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>Protokół odebrania paczek</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body{
            font-size:14px;
            font-family:'Arial',arial,sans-serif;
            padding:0;
            margin:0;
        }
        body{
            padding:30px;
            width:100%;
            box-sizing:border-box;
        }
        h1{
            font-size:12px;
            font-weight: normal;
            text-align: center;
        }
        h2{
            font-size:14px;

        }
        table{

            border-collapse:collapse;
            width:100%;
        }
        td,th{
            border:1px solid #000;
            padding:5px;
        }
        th{
            text-align: left;
        }
        td{
            text-align:center;
        }
        table.no-border td{
            border:0;
            vertical-align:top;
            text-align:left;
        }
        .no-border{
            margin-top:10px;
            font-size:12px;
        }
        .blank{
            border:0;
        }
        footer{
            margin-top:50px;
        {*position:absolute;
        bottom:20px;
        left:0;
        width:100%;
        box-sizing:border-box;
        padding:30px;*}
        }
        .col-50{
            width:50%;
            padding:0;
            box-sizing:border-box;
            float:left;
        }
        hr{
            border:0;
            margin:20px 0 10px;
            border-bottom:1px dotted #000;
        }
        .str{
            position:absolute;
            bottom:20px;
            right:50px;
            text-align: right;
        }

        ul{
            padding:20px;
            margin:20px;
        }
        table td.text-left{
            text-align:left;
            padding-left:10px;
        }
        .color{
            background:#f4efef;
            text-transform:uppercase;
        }
        ul li{
            list-style:none;
            padding-left:20px;
            margin-left:30px;
        }
        .list{
            padding:0;
            margin:0;
        }
        img{
            width:200px;
            height:auto;
        }

    </style>
</head>
<body>

<h1>Potwierdzenie odebrania paczek nr {$courier_api.courier_id|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'} <br />
    dla zamówienia Odbioru nr {$courier_api.courier_api_id|escape:'htmlall':'UTF-8'}
</h1>

<h2>Data {$smarty.now|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</h2>
<table>
    <thead>
    <tr>
        <th colspan="2">Oddział inpost</th>
        <th>Nadawca</th>
        <th>Adres Odbioru</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="2"></td>
        <td class="text-left">
            {$sender->email|escape:'htmlall':'UTF-8'}<br />
            {$sender->first_name|escape:'htmlall':'UTF-8'} {$sender->last_name|escape:'htmlall':'UTF-8'}<br />
            {$sender->address->city|escape:'htmlall':'UTF-8'}<br />
            {$sender->address->post_code|escape:'htmlall':'UTF-8'} {$sender->address->city|escape:'htmlall':'UTF-8'}, {$sender->address->province|escape:'htmlall':'UTF-8'}<br />
            Tel.: {$sender->phone|escape:'htmlall':'UTF-8'}<br />
        </td>
        <td class="text-left">
            {$dispatchPoint->email|escape:'htmlall':'UTF-8'}<br />
            {$dispatchPoint->agency_name|escape:'htmlall':'UTF-8'}<br />
            {$dispatchPoint->address->street|escape:'htmlall':'UTF-8'} {$dispatchPoint->address->building_no|escape:'htmlall':'UTF-8'} {$dispatchPoint->address->flat_no|escape:'htmlall':'UTF-8'}<br />
            {$dispatchPoint->address->post_code|escape:'htmlall':'UTF-8'} {$dispatchPoint->address->city|escape:'htmlall':'UTF-8'}<br />
            Tel.: {$dispatchPoint->phone|escape:'htmlall':'UTF-8'}<br />
        </td>
    </tr>
    <tr>
        <td colspan="4" class="text-left">
            Uwagi:{$courier_api.comment|escape:'htmlall':'UTF-8'}<br />
            {if $courier_api.courier_from && $courier_api.courier_to && $courier_api.courier_from != '0000-00-00 00:00:00'}
                Preferowany czas odbioru od:{$courier_api.courier_from|escape:'htmlall':'UTF-8'} do:{$courier_api.courier_to|escape:'htmlall':'UTF-8'}
            {/if}

        </td>
    </tr>
    <tr>
        <td>Typ</td>
        <td>Kod Paczki</td>
        <td>Nr Referencyjny</td>
        <td>Odbiorca</td>
    </tr>
    {assign var="sizeA" value=0}
    {assign var="sizeB" value=0}
    {assign var="sizeC" value=0}
        {foreach $packagesInfo as $package}
            {if $package.api->size == 'A'}
                {$sizeA++|escape:'htmlall':'UTF-8'}
            {/if}
            {if $package.api->size == 'B'}
                {$sizeB++|escape:'htmlall':'UTF-8'}
            {/if}
            {if $package.api->size == 'C'}
                {$sizeC++|escape:'htmlall':'UTF-8'}
            {/if}
            <tr>
                <td>{$package.api->size|escape:'htmlall':'UTF-8'}</td>
                <td>
                    <img src="{$package.barcode|escape:'htmlall':'UTF-8'}"><br />
                    {$package.api->id|escape:'htmlall':'UTF-8'}
                </td>
                <td>
                    {$package.api->customer_reference|escape:'htmlall':'UTF-8'}
                </td>
                <td>
                    {$package.api->receiver_email|escape:'htmlall':'UTF-8'}
                </td>
            </tr>
        {/foreach}

    </tbody>
</table>
<footer>
    <table>
        <tr>
            <td>Paczka gabarytu A</td>
            <td>Paczka gabarytu B</td>
            <td>Paczka gabarytu C</td>
            <td class="blank"></td>
            <td class="color">Suma</td>
        </tr>
        <tr>
            <td>{$sizeA|escape:'htmlall':'UTF-8'}</td>
            <td>{$sizeB|escape:'htmlall':'UTF-8'}</td>
            <td>{$sizeC|escape:'htmlall':'UTF-8'}</td>
            <td class="blank"></td>
            <td class="color"> {math equation="x + y + z" x=$sizeA y=$sizeB z=$sizeC}</td>
        </tr>
    </table>
    <div class="col-50">
        <table class="no-border">
            <tr>
                <td>
                    <input type="checkbox" class="checkbox" />
                </td>
                <td>
                    sprawdzono numery wszystkich paczek<br />
                    zgadza sie / nie zgadza sie - Uwagi<br />
                    <hr />
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" class="checkbox" />
                </td>
                <td>
                    nie dokonano weryfikacji liczby i numerów przesyłek - rzeczywista liczba sztuk zostanie potwierdzona przez InPost dopiero po ich rejestracji w systemie sledzenia<br />
                    Tylko przesyłki zarejestrowane w systemie sledzenia uwaza sie za nadane<br />
                    Uwagi
                    <hr />
                    <hr />
                    Podpis kuriera
                </td>
            </tr>
        </table>
    </div>
    <div class="col-50">
        <table class="no-border">
            <tr>
                <td><input type="checkbox" class="checkbox" /></td>
                <td>
                    sprawdzono numery wszystkich paczek<br />
                    zgadza sie / nie zgadza sie - Uwagi<br />
                    <hr />
                    <hr />
                    Podpis i pieczęć nadawcy
                </td>
            </tr>
        </table>

    </div>
    <div class="str">

    </div>
</footer>
</body>
</html>
