{**
*
* NOTICE OF LICENSE
*
*}
<form id="configuration_form" class="defaultForm form-horizontal" action="" method="post" enctype="multipart/form-data" novalidate="">
    <input type="hidden" name="priceRules" value="1">

    <div class="panel q-panel" id="price_rules">
        <div class="panel-heading">
            <i class="icon-user"></i> {l s='Parcel Lockers' mod='inpost'}
        </div>
        <div class="form-wrapper">

            {if !$INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS && ! $INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD}
                <div class="alert alert-danger">
                    {l s='Select available services in configuration form' mod='inpost'}
                </div>
            {/if}
            <div class="form-group">
                <div class="col-lg-3">
                    <label class="control-label">
                        {l s='Select tax for shipping methods' mod='inpost'}
                    </label>
                    <select class="form-control" name="tax_shipping_prices">
                        <option value="-1">{l s='No tax' mod='inpost'}</option>
                        {foreach from=$tax_rules_groups item=tax_rules_group}
                            <option value="{$tax_rules_group.id_tax_rules_group|escape:'htmlall':'UTF-8'}" {if $selected_tax == $tax_rules_group.id_tax_rules_group} selected="selected" {/if}>
                                {$tax_rules_group['name']|htmlentitiesUTF8|escape:'htmlall':'UTF-8'}
                            </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            {if $INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS}
                <div class="form-group">
                    <div class="col-lg-12">
                        <h2 style="margin: 20px;">{l s='Parcel Locker' mod='inpost'}</h2>
                    </div>
                    <div class="alert alert-info">
                        {l s='Prices are defined in %s' sprintf=$currency->iso_code mod='inpost'}
                        <br />
                        {l s='Prices are defined including selected tax' mod='inpost'}
                    </div>
                </div>
                <hr />

                {include file="./price_rules_parcels_lockers.tpl"}
            {/if}
            {if $INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD}
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="checkbox">
                            <h2 style="margin: 20px;">{l s='Parcel Locker COD' mod='inpost'}</h2>
                        </div>
                        <div class="alert alert-info">
                            {l s='Prices are defined in %s' sprintf=$currency->iso_code mod='inpost'}
                            <br />
                            {l s='Prices are defined including selected tax' mod='inpost'}
                        </div>
                    </div>
                </div>
                <hr />

                {include file="./price_rules_parcels_lockers_cod.tpl"}
            {/if}
        </div>

        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn_1" name="btnSubmit"
                    class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='inpost'}
            </button>
        </div>

    </div>
</form>