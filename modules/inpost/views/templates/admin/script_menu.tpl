{**
*
* NOTICE OF LICENSE
*
*}
{if $new_route}
    <div class="row" id="dashboard_info_new_route">
        <div class="col-lg-12">
            <div class="alert alert-info">
                {l s='New Cross Border route is now Available. Check it in the InPost module settings' mod='inpost'}
                <button class="confirm_new_route btn btn-success">{l s='Ok' mod='inpost'}</button>
            </div>
        </div>
    </div>
{/if}


<script type="text/javascript">

    var idTabModuleInpost = {$id|escape:'htmlall':'UTF-8'};
    var tokenInpost = '{getAdminToken tab='AdminModules'}';
    var inpost_ajax_request = '{$inpost_ajax_request}';

</script>
{literal}
    <script type="text/javascript">
        if (idTabModuleInpost > 0) {
            $(function () {
                href = $('*[data-submenu="' + idTabModuleInpost + '"] a');
                newHref = "index.php?controller=AdminModules&configure=inpost&token="+tokenInpost
                href.attr('href', newHref)
            })
        }
        $(function(){
            if ($('#dashboard').length > 0)
            {
                $('#dashboard').prepend($('#dashboard_info_new_route'))
            }
            $('.confirm_new_route').click(function(){

                $.ajax({
                    type: "POST",
                    async: true,
                    url: inpost_ajax_request,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    global: false,
                    data:  {'action': 'confirm_route'},
                    success: function (resp)
                    {
                        $('#dashboard_info_new_route').hide();
                    },
                    error: function()
                    {
                        alert('Something went wrong');
                    }
                });
            })
        })
    </script>
    <style type="text/css">
        .icon-Inpost:before {
            background-image: url("/modules/inpost/views/fonts/inpost-black.svg");
            content: '';
            background-size: 32px 20px;
            width: 50px;
            height: 15px;
            display: inline-block;
            background-repeat: no-repeat;
        }
    </style>
{/literal}

