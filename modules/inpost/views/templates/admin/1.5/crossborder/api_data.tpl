{**
*
* NOTICE OF LICENSE
*
*}
<div class="modal fade" id="crossBorderModal" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">CrossBorder</h4>
            </div>

            <div class="modal-body form-horizontal">
                <form id="x_border_data" method="POST" action="">
                    <div class="alert alert-warning x_alert" style="display: none;">

                    </div>
                    <input type="hidden" value="x_border_data" name="action">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Login</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="x_login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Haslo</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" name="x_password">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" id="check_x_border_data">Check data</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    var inpost_ajax_request = '{$inpost_ajax_request}';
</script>
{literal}
<script type="text/javascript">
    $(function() {
        $('#check_x_border_data').click(function (e) {
            e.preventDefault();
            $(this).text('Checking data...');
            $('.x_alert').text('').hide();
            $.ajax({
                type: "POST",
                async: true,
                url: inpost_ajax_request,
                dataType: "json",
                global: false,
                data: $('#x_border_data').serialize(),
                success: function (resp) {

                    if (resp.error == 1)
                    {
                        $('.x_alert').text('Wrong data').show();
                        $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').attr('checked', false);
                        $('#check_x_border_data').text('Check data');
                    }
                    else
                    {
                        $('#crossBorderModal').hide();
                        $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').attr('checked', true);
                        $('#check_x_border_data').text('Check data');
                    }



                }
            })
        })
    })

</script>

<style type="text/css">

    .modal-open {
        overflow: hidden;
    }
    .modal {
        display: none;
        overflow: hidden;
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1050;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }
    .modal.fade .modal-dialog {
        -webkit-transform: translate(0, -25%);
        -ms-transform: translate(0, -25%);
        -o-transform: translate(0, -25%);
        transform: translate(0, -25%);
        -webkit-transition: -webkit-transform 0.3s ease-out;
        -o-transition: -o-transform 0.3s ease-out;
        transition: transform 0.3s ease-out;
    }
    .modal.in .modal-dialog {
        -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
        -o-transform: translate(0, 0);
        transform: translate(0, 0);
    }
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
    }
    .modal-dialog {
        position: relative;
        width: auto;
        margin: 10px;
    }
    .modal-content {
        position: relative;
        background-color: #ffffff;
        border: 1px solid #999999;
        border: 1px solid rgba(0, 0, 0, 0.2);
        border-radius: 6px;
        -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
        box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        outline: 0;
    }
    .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        background-color: #000000;
    }
    .modal-backdrop.fade {
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .modal-backdrop.in {
        opacity: 0.5;
        filter: alpha(opacity=50);
    }
    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
    }
    .modal-header .close {
        margin-top: -2px;
    }
    .modal-title {
        margin: 0;
        line-height: 1.42857143;
    }
    .modal-body {
        position: relative;
        padding: 15px;
    }
    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }
    .modal-footer .btn + .btn {
        margin-left: 5px;
        margin-bottom: 0;
    }
    .modal-footer .btn-group .btn + .btn {
        margin-left: -1px;
    }
    .modal-footer .btn-block + .btn-block {
        margin-left: 0;
    }
    .modal-scrollbar-measure {
        position: absolute;
        top: -9999px;
        width: 50px;
        height: 50px;
        overflow: scroll;
    }
    @media (min-width: 768px) {
        .modal-dialog {
            width: 600px;
            margin: 100px auto;
        }
        .modal-content {
            -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
        }
        .modal-sm {
            width: 300px;
        }
    }
    @media (min-width: 992px) {
        .modal-lg {
            width: 900px;
        }
    }
    .clearfix:before,
    .clearfix:after,
    .modal-header:before,
    .modal-header:after,
    .modal-footer:before,
    .modal-footer:after {
        content: " ";
        display: table;
    }
    .clearfix:after,
    .modal-header:after,
    .modal-footer:after {
        clear: both;
    }
    .center-block {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    .pull-right {
        float: right !important;
    }
    .pull-left {
        float: left !important;
    }
    .hide {
        display: none !important;
    }
    .show {
        display: block !important;
    }
    .invisible {
        visibility: hidden;
    }
    .text-hide {
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }
    .hidden {
        display: none !important;
    }
    .affix {
        position: fixed;
    }
</style>
{/literal}