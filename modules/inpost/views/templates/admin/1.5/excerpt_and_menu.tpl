{**
*
* NOTICE OF LICENSE
*
*}
<script type="text/javascript">
    var assign_payment_methods_text = "{l s='Assign payment methods' mod='inpost'}"
    var from_text = "{l s='from' mod='inpost'}"
    var to_text = "{l s='to' mod='inpost'}"
    var cost_text = "{l s='cost' mod='inpost'}"

</script>
<div class=" row">
    <div class = "q-info clearfix">
        <div class="col-sm-4 ">
            <img src="{$q_img_dir|escape:'htmlall':'UTF-8'}inpost-logo.png" alt="logo" class="img-responsive" />
        </div>
        <div class="col-sm-8 ">
            <p class="inpost_desc without" style="display:{if $inpost_country != false}none;{/if}">
                InPost is an international network of fully automated parcel lockers that are accessible 24/7, meaning no more queues or waiting in, enabling customers to collect, send and return parcels at their convenience. InPost's lockers are located at a variety of safe and secure locations such as supermarkets, petrol stations and transport hubs.
                Using an InPost Parcel Locker is very easy and intuitive. An online shopper selects the InPost Parcel Lockers 23/7 at the e-shop’s check-out and chooses his preferred locker, on his way to home or work. Once the parcel is delivered, the customer receives a text message with an opening code. After scanning it at the locker, the locker opens and the parcel is ready for collection.
                This is that easy!
            </p>
            <p class="inpost_desc pl" style="display:{if $inpost_country != 1}none;{/if}">
                Paczkomaty InPost błyskawicznie dostarczają paczki!
                Klienci wolą Paczkomaty od kuriera. Dzięki nim oszczędzają. Sami decydują, kiedy i gdzie odbierają przesyłki.

                Dzięki InPost można: nadać, odebrać, a także zwrócić paczkę zarówno do Paczkomatu, jak i na adres domowy.
                98% paczek jest dostarczanych już dzień po nadaniu.
            </p>

            <p class="inpost_desc fr" style="display:{if $inpost_country != 2}none;{/if}">
                Les Abricolis InPost sont des consignes automatiques, disponibles 24h/24 et 7j/7 ou en horaires étendus. Pour récupérer son colis, le client doit composer son numéro de téléphone et son code secret, ou tout simplement scanner le code QR reçu par sms dès l’arrivée de son colis. Grâce à cette solution pratique, vos clients peuvent récupérer leurs colis en seulement quelques secondes, sans faire la queue, où et quand ils le souhaitent. Le suivi de colis est très facile grâce à la communication par e-mail, notification par sms, et via site d’InPost. Les Abricolis InPost 24h/24 et 7j/7 sont situés sur le trajet quotidien des utilisateurs : près d’un supermarché, d’une station-service ou encore dans une zone commerçante. Nos consignes sont dotées de 3 tailles de casiers : A) 80x380x640 mm B) 190x380x640 mm C) 380x380x640 mm. Veuillez trouver plus d’informations sur www.inpost24.fr.
            </p>
            <p class="inpost_desc it" style="display:{if $inpost_country != 3}none;{/if}">
                INPOST è una rete internazionale di Lockers (Caselle Postali) automatizzati ed accessibili 24/7, che consentono il ritiro, l’invio e la restituzione dei pacchi senza più code né attese. I Lockers INPOST sono dislocati in luoghi sicuri e comodi come supermercati, distributori di benzina, stazioni metropolitane e ferroviarie.
                Utilizzare un INPOST Parcel Locker è molto semplice ed intuitivo. Lo “shopper online” seleziona il locker  INPOST 24/7 al momento del check-out e sceglie l’indirizzo preferito  in cui il locker è situato (p. es. sulla via per recarsi a lavoro o di casa). Una volta che il pacco viene riposto nel Locker, l’utente riceve un SMS con un codice PIN ed un codice QR che provvederà poi rispettivamente ad inserire o far scansionare attraverso la tastiera o il lettore QR di cui tutte le macchine sono dotate. Fatto ciò lo sportello del Locker corrispondente di aprirà e sarà possibile ritirare il pacco in circa 5 secondi!
                Utile, facile e divertente !
            </p>
        </div>
    </div>
</div>

<nav class="navbar navbar-inverse menu_inpost clearfix" style="background-color: #2e3641;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#q-big-navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation1.5</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
        </div>
        <div id="q-big-navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                {foreach $menu_tabs as $tab}
                    <li class="{if $tab.selected == 1}active{/if}">
                        <a {if $tab.disabled == 0} href="{$tab.href|escape:'htmlall':'UTF-8'}" {/if}><i class="{$tab.imgclass|escape:'htmlall':'UTF-8'}"></i>{$tab.name|escape:'htmlall':'UTF-8'} {if $tab.count}<span class="badge">{$tab.count|escape:'htmlall':'UTF-8'}</span>{/if}</a>
                    </li>
                {/foreach}
            </ul>
        </div>                  
    </div>
</nav>


{if !$config_filled}
    <div class="alert alert-danger">
        {l s='Before continuing fill the data' mod='inpost'}
        <br />
        {$error|escape:'htmlall':'UTF-8'}
    </div>
{/if}
            
                
{literal}
<style type="text/css">
    .menu_inpost li a:hover{
        color: #fff!important;
        background-color: #3b4551!important;
    }

    .menu_inpost {
        border-radius: 0!important;        
    }

    .menu_inpost li a i {
        padding-right: 10px;
    }
    .menu_inpost li a {
        text-transform: uppercase;
    }
</style>
{/literal}