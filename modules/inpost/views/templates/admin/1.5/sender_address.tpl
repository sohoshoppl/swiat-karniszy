{**
*
* NOTICE OF LICENSE
*
*}
<fieldset id="fieldset_4">
    <legend>
        <i class="icon-user"></i> {l s='Sending packages' mod='inpost'}
    </legend>
    <div class="form-wrapper">
        <div class="form-group">
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Street' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_STREET" id="" value="{$INPOST_SENDER_STREET|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Building No.' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_BUILDING_NO" id="" value="{$INPOST_SENDER_BUILDING_NO|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Flat No.' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_FLAT_NO" id="" value="{$INPOST_SENDER_FLAT_NO|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Postal code' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_POSTCODE" id="" value="{$INPOST_SENDER_POSTCODE|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='City' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_CITY" id="" value="{$INPOST_SENDER_CITY|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Contact phone' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_PHONE" id="" value="{$INPOST_SENDER_PHONE|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='E-mail' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_SENDER_EMAIL" id="" value="{$INPOST_SENDER_EMAIL|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Customer Reference' mod='inpost'}
                </label>
                <div class="col-lg-9 ">
                    <input type="text" name="INPOST_CUSTOMER_REFERENCE" id="" value="{$INPOST_CUSTOMER_REFERENCE|escape:'htmlall':'UTF-8'}" class="">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label class="control-label"> {l s='Default parcel locker' mod='inpost'}: </label>
                <hr />
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Select' mod='inpost'}
                </label>
                <div class="col-lg-9">
                    {if $INPOST_SENDER_PARCEL_LOCKER}
                        {assign var="selectedDefault" value="&selected={$INPOST_SENDER_PARCEL_LOCKER|escape:'htmlall':'UTF-8'}"}
                    {else}
                        {assign var="selectedDefault" value=""}
                    {/if}

                    <div class="col-lg-11">
                        <select name="INPOST_SENDER_PARCEL_LOCKER">
                            {foreach $parcels_lockers as $parcel}
                                <option {if $parcel->id == $INPOST_SENDER_PARCEL_LOCKER}selected="selected"{/if} value="{$parcel->id|escape:'htmlall':'UTF-8'}">{$parcel->id|escape:'htmlall':'UTF-8'}, {$parcel->address->city|escape:'htmlall':'UTF-8'}, {$parcel->address->street|escape:'htmlall':'UTF-8'}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-lg-1">
                    <a class="btn btn-primary" href="#" onclick="openMap(); return false;">{l s='Map' mod='inpost'}</a>
                    </div>
                    <script type="text/javascript" src="{$INPOST_GEOWIDGET_URL|escape:'htmlall':'UTF-8'}dropdown.php?dropdown_name=INPOST_SENDER_PARCEL_LOCKER{$selectedDefault|escape:'htmlall':'UTF-8'}"></script>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <label class="control-label">
                    {l s='Default sending options' mod='inpost'}
                </label>
                <hr />
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 ">
                    {l s='Method of giving' mod='inpost'}
                </label>
                <div class="col-lg-9">
                    <select name="INPOST_DEFAULT_SEND_METHOD">
                        <option value="0"> ---- </option>
                        <option value="parcel_locker" {if $INPOST_DEFAULT_SEND_METHOD == 'parcel_locker'}selected="selected"{/if} >{l s='From parcel locker' mod='inpost'}</option>
                        <option value="dispatch_point"{if $INPOST_DEFAULT_SEND_METHOD == 'dispatch_point'}selected="selected"{/if} >{l s='From dispatch point' mod='inpost'}</option>
                    </select>
                </div>
            </div>

            {if $dispatch_points}
                <div class="form-group">
                    <label class="control-label col-lg-2 ">
                        {l s='Dispatch point' mod='inpost'}
                    </label>
                    <div class="col-lg-9">
                        <select name="EE_INPOST_DEFAULT_DISPATCH_POINT">
                            <option value="0"> ---- </option>
                            {foreach from=$dispatch_points item=dispatch_point}
                                <option value="{$dispatch_point['id']}"{if $EE_INPOST_DEFAULT_DISPATCH_POINT == $dispatch_point['id']} selected="selected"{/if}>
                                    {$dispatch_point['name']}
                                </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            {/if}
        </div>
    </div>

    <div class="panel-footer">
        <button type="submit" value="1" id="configuration_form_submit_btn_3" name="btnSubmit" class="btn btn-default pull-right">
            <i class="process-icon-save"></i> {l s='Save' mod='inpost'}
        </button>
    </div>
</fieldset>