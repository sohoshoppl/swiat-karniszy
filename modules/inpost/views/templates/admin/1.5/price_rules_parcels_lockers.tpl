{**
*
* NOTICE OF LICENSE
*
*}
<div id="parcels_lockers" class="q-fix">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-info">
                {l s='Free shipping has the highest priority' mod='inpost'}
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-lg-2">{l s='Free shipping from' mod='inpost'}</label>
        <div class="col-lg-10">
            <input type="text" class="form-control" id="PARCELS_LOCKERS_FREE_SHIPPING" placeholder="00,00" name="PARCELS_LOCKERS_FREE_SHIPPING" value="{$PARCELS_LOCKERS_FREE_SHIPPING|escape:'htmlall':'UTF-8'}">
        </div>
    </div>
    <div class="row">
        <div class="radio col-lg-2">
            <label>
                <input class="selection" type="radio" name="PARCELS_LOCKERS_FLAT_PRICE_CHECKED" value="1" {if 'PARCELS_LOCKERS_FLAT_PRICE'|in_array:$selected}checked="checked"{/if}> {l s='Flat price' mod='inpost'}
            </label>
        </div>

        <div class="col-lg-10">
            <input type="text" class="form-control" name="PARCELS_LOCKERS_FLAT_PRICE" value="{$flat_price.cost|escape:'htmlall':'UTF-8'}" placeholder="00,00">
        </div>        
    </div>
    <div class="row">
        <div class="radio  col-lg-2">
            <label>
                <input class="selection" type="radio" name="PARCELS_LOCKERS_PRICE_RULES" value="1"
                       {if 'PRICE_RULES'|in_array:$selected}checked="checked"{/if}> {l s='Price Ranges' mod='inpost'}
            </label>
        </div>

        <div class="col-lg-10">
            <div class="table-responsive">
                <table class="table q-table">
                    <tbody>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label class="control-label col-lg-4">{l s='price from' mod='inpost'}</label>

                                <div class="col-lg-8">
                                    <input type="text" class="form-control"
                                                             id="PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE"
                                                             placeholder="00,00"
                                                             name="PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label class="control-label col-lg-4">{l s='price to' mod='inpost'}</label>

                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE"
                                           placeholder="00,00" name="PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label class="control-label col-lg-4">{l s='cost' mod='inpost'}</label>

                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE"
                                           placeholder="00,00" name="PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE">
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="alert alert-info alertParcelLockers" style="display: none">
                {l s='All fields are required' mod='inpost'}
            </div>
            <div class="btn btn-default" id="add_new_price_rule"
                 style="margin-top: 10px;">{l s='Add range' mod='inpost'}</div>


            <div class="table-responsive">
                <table class="table table-bordered table_prices_rules q-table">
                    <tbody>
                        {if $price_rules}
                            {foreach $price_rules as $price_rule_key => $price_rule}
                                <tr class="price" data-id="{$price_rule_key|escape:'htmlall':'UTF-8'}">
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='price from' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_FROM_0" value="{$price_rule['price_from']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_PRICE_RULES_FROM[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='price to' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_TO_0"  value="{$price_rule['price_to']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_PRICE_RULES_TO_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-5">{l s='cost' mod='inpost'}</label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_COST_0" value="{$price_rule['cost']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_PRICE_RULES_COST_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="last">
                                        <button type="button" class="delete">x</button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <div class="radio">
                <label>
                    <input class="selection" type="radio" name="PARCELS_LOCKERS_WEIGHT_RULES"
                           {if 'WEIGHT_RULES'|in_array:$selected}checked="checked"{/if} value="1"> {l s='Weight ranges products in cart' mod='inpost'}
                </label>
            </div>
        </div>
        <div class="col-lg-10">
            <div class="table-responsive">
                <div class="alert alert-danger">
                    {l s='Please make sure that you defined the weight in products' mod='inpost'}
                </div>
                <table class="table q-table">
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='weight from' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_RULES_FROM_DEFINE" placeholder="00,00" name="PARCELS_LOCKERS_WEIGHT_RULES_FROM_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='weight to ' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_RULES_TO_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_WEIGHT_RULES_TO_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='cost' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_RULES_COST_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_WEIGHT_RULES_COST_DEFINE">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="alert alert-info alertParcelLockersWeight" style="display: none">
                {l s='All fields are required' mod='inpost'}
            </div>
            <div class="btn btn-default" id="add_new_weight_rule" style="margin-top: 10px;">{l s='Add range' mod='inpost'}</div>
            <div class="table-responsive">
                <table class="table table-bordered table_weight_rules q-table">
                    <tbody>
                        {if $weight_rules}
                            {foreach $weight_rules as $weight_rule_key => $weight_rule}
                                <tr data-id="{$weight_rule_key|escape:'htmlall':'UTF-8'}">
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='weight from' mod='inpost'}</label>
                                            <div class="col-lg-8"><input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_FROM_0"
                                                                         value="{$weight_rule['price_from']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_WEIGHT_RULES_FROM[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='weight to ' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_TO_0" value="{$weight_rule['price_to']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_WEIGHT_RULES_TO_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='cost' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_WEIGHT_COST_0" value="{$weight_rule['cost']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_WEIGHT_RULES_COST_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="last">
                                        <button type="button" class="delete">x</button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
            </div>
            <hr/>
        </div>
    </div>
</div>