{**
*
* NOTICE OF LICENSE
*
*}
{if $total > 0 }
    <div id="actions_packages" class="q-actions" style="display: none">
        {*<input type="submit" class="button" name="submitBulkdownload_labelsinpostLabels" value="{l s='Download selected labels' mod='inpost'}">*}
        {*<Br />*}
        {if $inpost_country == 1 && $label_send == 0}
            <button class="btn btn-danger" type="button"  id="order_courier">
                {l s='Order the courier' mod='inpost'}
            </button>
        {/if}
        <div class="q-form" id="date_courier" style="display: none">
            <div class="inner">
                <input type="text" name="date_from" placeholder="{l s='Reception date' mod='inpost'}" class="datepickerInpost form-control">

                <select name="hour_from" class="form-control">
                    <option>{l s='Select reception hour from' mod='inpost'}</option>
                    {for $var=0 to 23}
                        <option value="{if $var < 10}0{/if}{$var|escape:'htmlall':'UTF-8'}:00">{$var|escape:'htmlall':'UTF-8'}:00</option>
                    {/for}

                </select>
                <div style="margin: 5px;"></div>
                <select name="hours_to" class="form-control">
                    <option>{l s='Select reception hour to' mod='inpost'}</option>
                    {for $var=0 to 23}
                        <option value="{if $var < 10}0{/if}{$var|escape:'htmlall':'UTF-8'}:00">{$var|escape:'htmlall':'UTF-8'}:00</option>
                    {/for}
                </select>
                <div style="margin: 5px;"></div>
                <input type="text" name="reference" placeholder="Reference" class="form-control">
                <input type="text" name="comments" placeholder="Komentarz" class="form-control">

                <button class="btn btn-danger" name="order_courier_confirm" onclick="sendBulkAction($(this).closest('form').get(0), 'order_courier');">{l s='Submit courier order' mod='inpost'}</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var inpost_ajax_request = '{$inpost_ajax_request}';
        var packages = "{l s='The shipments: ' mod='inpost'}"
        var need_to_text = "{l s=' sent with use of the parcel locker cannot be picked up by the InPost courier, please delete the shipments from the list and place the order once again:' mod='inpost'}"
    </script>
    {literal}
    <script type="text/javascript">
        $('#actions_packages').insertAfter('.inpostLabels+p');
        $('.btn-group.bulk-actions.dropup').css('float', 'left');

        $('#order_courier').click(function(){
            $('.errroPack').remove()

            $.ajax({
                type: "POST",
                async: true,
                dataType: "json",
                url: inpost_ajax_request,
                global: false,
                data:  $('input[name="inpostLabelsBox[]"]:checked').serialize() + "&action=check_order_courier",
                success: function (resp)
                {
                    html = ''
                    if (resp)
                    {
                        for(i=0;i<resp.length;i++)
                        {
                            if (resp[i].tracking_number) {
                                html += resp[i].tracking_number+' ';
                            } else {
                                html += resp[i].parcel_no+' ';
                            }
                        }
                        $('<div class="alert alert-danger errroPack">').insertBefore($('#form-inpostLabels'));
                        $('.errroPack').text(packages+' ' +html+ need_to_text);
                        //$('#actions_packages').hide();
                    }
                    else
                    {
                        $('#date_courier').show();
                    }


                    $btn.button('reset');
                },
                error: function(xhr, stat, error)
                {
                    alert('Unexpected error');
                }
            });

        })
        $('input[name="inpostLabelsBox[]"]').click(function(){
            if ($(this).is(':checked') || $('input[name="inpostLabelsBox[]"]:checked').length > 0)
                $('#actions_packages').show();
            else
                $('#actions_packages').hide();
        })
        $('.bulk-actions .dropdown-menu a').click(function(){
            $('input[name="inpostLabelsBox[]"]').each(function(){
                if ($(this).is(':checked'))
                    $('#actions_packages').show();
                else
                    $('#actions_packages').hide();
            })
        })
        $(function(){
            $('.datepickerInpost').datepicker();
            if ($('input[name="inpostLabelsBox[]"]:checked').length > 0)
                $('#actions_packages').show();
        })
    </script>
    {/literal}
{/if}