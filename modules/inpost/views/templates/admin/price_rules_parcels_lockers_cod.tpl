{**
*
* NOTICE OF LICENSE
*
*}
<div id="parcels_lockers_cod">
    <div class="form-group">
        <div class="alert alert-info">
            {l s='Free shipping has the highest priority' mod='inpost'}
        </div>
        <label class="col-lg-2">{l s='Free shipping from' mod='inpost'}</label>
        <div class="col-lg-10">
            <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_FREE_SHIPPING" placeholder="00,00"
                   name="PARCELS_LOCKERS_COD_FREE_SHIPPING" value="{$PARCELS_LOCKERS_COD_FREE_SHIPPING|escape:'htmlall':'UTF-8'}">
        </div>
    </div>
    <div class="row">
        <label class="col-lg-2">
            {l s='% of shopping cart' mod='inpost'}
        </label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="PERCENT_OF_CART_VALUE" value="{$cart_value_percent_cod}" placeholder="0%">
            <span class="help-block">
                {l s='Example: "Flat price" is set to 10.00, "percent of the shopping cart" is set at 20%. The worth of the cart is 10.00. The total shipping cost will be 10 * 120 / 100 = 12.' mod='inpost'}
            </span>
        </div>
    </div>
    <div class="row">
        <div class="radio col-lg-2">
            <label>
                <input class="selection" type="radio" name="PARCELS_LOCKERS_COD_FLAT_PRICE_CHECKED" value="1" {if 'PARCELS_LOCKERS_COD_FLAT_PRICE'|in_array:$selected}checked="checked"{/if}> {l s='Flat price' mod='inpost'}
            </label>
        </div>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="PARCELS_LOCKERS_COD_FLAT_PRICE" value="{$flat_price_cod.cost|escape:'htmlall':'UTF-8'}" placeholder="00,00">
        </div>
    </div>

    <div class="row">
        <div class="radio col-lg-2">
            <label>
                <input class="selection" type="radio" name="PARCELS_LOCKERS_COD_PRICE_RULES" value="1"
                       {if 'PRICE_RULES_COD'|in_array:$selected}checked="checked"{/if}> {l s='Price ranges' mod='inpost'}
            </label>
        </div>

        <div class="col-lg-10">
            <div class="table-responsive">
                <table class="table q-table">
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='price from' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='price to' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">{l s='cost' mod='inpost'}</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="alert alert-info alertParcelLockersCod" style="display: none;">
                {l s='All fields are required' mod='inpost'}
            </div>
            <div class="btn btn-default" id="add_new_price_rule_cod" style="margin-top: 10px;">{l s='Add range' mod='inpost'}</div>
            <div class="table-responsive">
                <table class="table q-table table_prices_rules_cod">
                    <tbody>
                        {if $price_rules_cod}
                            {foreach $price_rules_cod as $price_rule_key => $price_rule}
                                <tr class="price" data-id="{$price_rule_key|escape:'htmlall':'UTF-8'}">
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='from' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_FROM_0" value="{$price_rule['price_from']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_PRICE_RULES_FROM[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">{l s='to' mod='inpost'}</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_TO_0"  value="{$price_rule['price_to']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_PRICE_RULES_TO_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-5">{l s='cost' mod='inpost'}</label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_PRICE_RULES_COST_0" value="{$price_rule['cost']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_PRICE_RULES_COST_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="last">
                                        <button type="button" class="delete">x</button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="radio col-lg-2">
            <label>
                <input class="selection" type="radio" name="PARCELS_LOCKERS_COD_WEIGHT_RULES"
                       {if 'WEIGHT_RULES_COD'|in_array:$selected}checked="checked"{/if} value="1"> {l s='Weight ranges products in cart' mod='inpost'}
            </label>
        </div>
        <div class="col-lg-10">
            <div class="table-responsive">
                <div class="alert alert-danger">
                    {l s='Please make sure that you defined the weight in products' mod='inpost'}
                </div>
                <table class="table q-table">
                    <tbody>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-6">{l s='weight from' mod='inpost'}</label>
                                    <div class="col-lg-6">
                                        <input style="width: 100%;" type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-6">{l s='weight to' mod='inpost'}</label>

                                    <div class="col-lg-6">
                                        <input style="width: 100%;" type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_DEFINE">
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label class="control-label col-lg-6">{l s='cost' mod='inpost'}</label>

                                    <div class="col-lg-6">
                                        <input style="width: 100%;" type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_DEFINE"
                                               placeholder="00,00" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_DEFINE">
                                    </div>
                                </div>
                            </td>
                            {*<td>
                                <div class="form-group">
                                    <label class="control-label col-lg-10">{l s='% of cart value' mod='inpost'}</label>

                                    <div class="col-lg-2">
                                        <input style="width: 100%" type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT_DEFINE"
                                               placeholder="0" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT_DEFINE">
                                    </div>

                                </div>
                            </td>*}
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="alert alert-info alertParcelLockersCodWeight" style="display: none;">
                {l s='All fields are required' mod='inpost'}
            </div>
            <div class="btn btn-default" id="add_new_weight_rule_cod" style="margin-top: 10px;">{l s='Add range' mod='inpost'}</div>

            <div class="table-responsive">
                <table class="table q-table table-bordered table_weight_rules_cod">

                    <tbody>
                        {if $weight_rules_cod}
                            {foreach $weight_rules_cod as $weight_rule_key => $weight_rule}
                                <tr data-id="{$weight_rule_key|escape:'htmlall':'UTF-8'}">
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-6">{l s='weight from' mod='inpost'}</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM_0"
                                                       value="{$weight_rule['price_from']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-6">{l s='weight to' mod='inpost'}</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_0"
                                                       value="{$weight_rule['price_to']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_[]">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label class="control-label col-lg-6">{l s='cost' mod='inpost'}</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_0"
                                                       value="{$weight_rule['cost']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_[]">
                                            </div>
                                        </div>
                                    </td>
                                    {*<td>*}
                                        {*<div class="form-group">*}
                                            {*<label class="control-label col-lg-10">{l s='% of cart value' mod='inpost'}</label>*}
                                            {*<div class="col-lg-2">*}
                                                {*<input type="text" class="form-control" id="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT_0"*}
                                                       {*value="{$weight_rule['cart_value']|escape:'htmlall':'UTF-8'}" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT[]">*}
                                            {*</div>*}
                                        {*</div>*}
                                    {*</td>*}
                                    <td>
                                        <button type="button" class="delete">x</button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
                <hr/>
            </div>
        </div>
    </div>
</div>
