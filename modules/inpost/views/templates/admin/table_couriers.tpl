{**
*
* NOTICE OF LICENSE
*
*}
<div class="alert alert-warning" id="inpostCouriers-empty-filters-alert" style="display:none;">Wypełnij przynajmniej
    jedno pole aby wyszukiwać w tej liście.
</div>
<form method="post" action="" class="form-horizontal clearfix" id="form-inpostCouriers">
    <input type="hidden" name="couriers" value="1">

    <input type="hidden" id="submitFilterinpostCouriers" name="submitFilterinpostCouriers" value="0">
    <input type="hidden" name="page" value="1">
    <input type="hidden" name="selected_pagination" value="50">

    <div class="panel col-lg-12">
        <div class="panel-heading">
            Couriers
        </div>
        <style>
            @media (max-width: 992px) {
                .table-responsive-row td:nth-of-type(1):before {
                    content: "Courier id";
                }

                .table-responsive-row td:nth-of-type(2):before {
                    content: "Status";
                }

                .table-responsive-row td:nth-of-type(3):before {
                    content: "Date from";
                }

                .table-responsive-row td:nth-of-type(4):before {
                    content: "Date from";
                }
            }
        </style>
        <div class="table-responsive-row clearfix">
            <table class="table inpostCouriers">
                <thead>
                <tr class="nodrag nodrop">
                    <th class="center fixed-width-xs"></th>
                    <th class="">

						<span class="title_box">Courier id
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=courier_id&inpostCouriersOrderWay=desc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-down"></i></a>
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=courier_id&inpostCouriersOrderWay=asc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-up"></i></a>
                        </span>
                    </th>
                    <th class="">
						<span class="title_box">Status
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=status&inpostCouriersOrderWay=desc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-down"></i></a>
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=status&inpostCouriersOrderWay=asc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-up"></i></a>
                            </span>
                    </th>
                    <th class="">
						<span class="title_box">Date from
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=date_from&inpostCouriersOrderWay=desc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-down"></i></a>
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=date_from&inpostCouriersOrderWay=asc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-up"></i></a>
                        </span>
                    </th>
                    <th class="">
						<span class="title_box">Date to
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=date_to&inpostCouriersOrderWay=desc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-down"></i></a>
                            <a href="{$filter_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&inpostCouriersOrderBy=date_to&inpostCouriersOrderWay=asc&token=&token={getAdminToken tab='AdminModules'}"><i class="icon-caret-up"></i></a>
                        </span>
                    </th>
                    <th class="">
						<span class="title_box">

                        </span>
                    </th>
                    <th></th>
                </tr>
                <tr class="nodrag nodrop filter row_hover">
                    <th class="text-center">
                        --
                    </th>
                    {*{$full_url|escape:'htmlall':'UTF-8'|escape:'htmlall':'UTF-8'}&PackagesOrderBy=id_order&PackagesOrderWay=desc">*}
                    <th>
                        <input type="text" class="filter" name="inpostCouriersFilter_courier_id" value="{$filter_smarty.inpostCouriersFilter_courier_id|escape:'htmlall':'UTF-8'}"
                               style="cursor: auto; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat;">
                    </th>
                    <th>
                        <input type="text" class="filter" name="inpostCouriersFilter_courier_status" value="{$filter_smarty.inpostCouriersFilter_courier_status|escape:'htmlall':'UTF-8'}">
                    </th>
                    <th>
                        <div class="date_range row">
                            <div class="input-group fixed-width-md center">
                                <input type="text" class="filter datepicker date-input form-control"
                                       id="local_inpostCouriersFilter_courier_from_0" value="{$filter_smarty.inpostCouriersFilter_courier_from_0|escape:'htmlall':'UTF-8'}"
                                       name="local_inpostCouriersFilter_courier_from[0]" placeholder="{l s='From' mod='inpost'}">
                                <input type="hidden" id="inpostCouriersFilter_courier_from_0"
                                       name="inpostCouriersFilter_courier_from[0]" value="{$filter_smarty.inpostCouriersFilter_courier_from_0|escape:'htmlall':'UTF-8'}">
											<span class="input-group-addon">
												<i class="icon-calendar"></i>
											</span>
                            </div>
                            <div class="input-group fixed-width-md center">
                                <input type="text" class="filter datepicker date-input form-control"
                                       id="local_inpostCouriersFilter_courier_from_1" value="{$filter_smarty.inpostCouriersFilter_courier_from_1|escape:'htmlall':'UTF-8'}"
                                       name="local_inpostCouriersFilter_courier_from[1]" placeholder="{l s='To' mod='inpost'}">
                                <input type="hidden" id="inpostCouriersFilter_courier_from_1"
                                       name="inpostCouriersFilter_courier_from[1]" value="{$filter_smarty.inpostCouriersFilter_courier_from_1|escape:'htmlall':'UTF-8'}">
											<span class="input-group-addon">
												<i class="icon-calendar"></i>
											</span>
                            </div>
                            <script>
                                $(function () {
                                    var dateStart = parseDate($("#inpostCouriersFilter_courier_from_0").val());
                                    var dateEnd = parseDate($("#inpostCouriersFilter_courier_from_1").val());
                                    $("#local_inpostCouriersFilter_courier_from_0").datepicker({ dateFormat: 'yy-mm-dd' });
                                    $("#local_inpostCouriersFilter_courier_from_1").datepicker({ dateFormat: 'yy-mm-dd' });
                                    if (dateStart !== null) {
                                        $("#local_inpostCouriersFilter_courier_from_0").datepicker("setDate", dateStart);
                                    }
                                    if (dateEnd !== null) {
                                        $("#local_inpostCouriersFilter_courier_from_1").datepicker("setDate", dateEnd);
                                    }
                                });
                            </script>
                        </div>
                    </th>
                    <th>
                        <div class="date_range row">
                            <div class="input-group fixed-width-md center">
                                <input type="text" class="filter datepicker date-input form-control"
                                       id="local_inpostCouriersFilter_courier_to_0"
                                       name="local_inpostCouriersFilter_courier_to[0]" placeholder="{l s='From' mod='inpost'}">
                                <input type="hidden" id="inpostCouriersFilter_courier_to_0"
                                       name="inpostCouriersFilter_courier_to[0]" value="{$filter_smarty.inpostCouriersFilter_courier_to_0|escape:'htmlall':'UTF-8'}">
											<span class="input-group-addon">
												<i class="icon-calendar"></i>
											</span>
                            </div>
                            <div class="input-group fixed-width-md center">
                                <input type="text" class="filter datepicker date-input form-control"
                                       id="local_inpostCouriersFilter_courier_to_1"
                                       name="local_inpostCouriersFilter_courier_to[1]" placeholder="{l s='To' mod='inpost'}">
                                <input type="hidden" id="inpostCouriersFilter_courier_to_1"
                                       name="inpostCouriersFilter_courier_to[1]" value="{$filter_smarty.inpostCouriersFilter_courier_to_1|escape:'htmlall':'UTF-8'}">
											<span class="input-group-addon">
												<i class="icon-calendar"></i>
											</span>
                            </div>
                            <script>
                                $(function () {
                                    var dateStart = parseDate($("#inpostCouriersFilter_courier_to_0").val());
                                    var dateEnd = parseDate($("#inpostCouriersFilter_courier_to_1").val());
                                    $("#local_inpostCouriersFilter_courier_to_0").datepicker({ dateFormat: 'yy-mm-dd' });
                                    $("#local_inpostCouriersFilter_courier_to_1").datepicker({ dateFormat: 'yy-mm-dd' });
                                    if (dateStart !== null) {
                                        $("#local_inpostCouriersFilter_courier_to_0").datepicker("setDate", dateStart);
                                    }
                                    if (dateEnd !== null) {
                                        $("#local_inpostCouriersFilter_courier_to_1").datepicker("setDate", dateEnd);
                                    }
                                });
                            </script>
                        </div>
                    </th>
                    <th class="">
						<span class="title_box">

                        </span>
                    </th>
                    <th class="actions">
                        <span class="pull-right">

                            <button type="submit" id="submitFilterButtoninpostCouriers" name="submitFilter"
                                    class="btn btn-default" data-list-id="inpostCouriers">
                                <i class="icon-search"></i> Szukaj
                            </button>
                            {if $filters_enabled}
                                <button type="submit" name="submitResetinpostCouriers" class="btn btn-warning">
                                    <i class="icon-eraser"></i> Wyczyść
                                </button>
                            {/if}
                        </span>
                    </th>
                </tr>
                </thead>

                <tbody>
                {foreach $couriers as $courier}
                    <tr class="odd" id="courier{$courier.courier_id|escape:'htmlall':'UTF-8'}">
                        <td class="pointer"></td>
                        <td class="pointer">{$courier.courier_id|escape:'htmlall':'UTF-8'}</td>
                        <td class="pointer">{$courier.courier_status|escape:'htmlall':'UTF-8'}</td>
                        <td class="pointer">{$courier.courier_from|escape:'htmlall':'UTF-8'}</td>
                        <td class="pointer">{$courier.courier_to|escape:'htmlall':'UTF-8'}</td>
                        <td class="pointer">
                            Expand
                        </td>
                        <td class="text-right">
                            <div class="btn-group pull-right">
                                <a href="{$manifest_url|escape:'htmlall':'UTF-8'}{$courier.courier_api_id|escape:'htmlall':'UTF-8'}&token={getAdminToken tab='AdminModules'}" title="{l s='Download manifest' mod='inpost'}" class="edit btn btn-default">
                                    <i class="icon-pencil"></i> {l s='Download manifest' mod='inpost'}
                                </a>
                            </div>
                        </td>
                    </tr>
                    <tr class="hideMe" style="display: none;">
                        <td colspan="7">
                            <div style="padding: 20px;">
                                {foreach $courier.packages as $package}
                                    <a target="_blank" style="border:1px solid gray;padding:5px;" href="{$filter_url|escape:'htmlall':'UTF-8'}&packages&parcel_no={$package.package_id|escape:'htmlall':'UTF-8'}&updateinpost_packages&token={getAdminToken tab='AdminModules'}">{$package.package_id|escape:'htmlall':'UTF-8'}</a>
                                {/foreach}
                            </div>
                        </td>
                    </tr>
                {/foreach}

                </tbody>

            </table>
        </div>
        <div class="row">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6">
                <div class="pagination">
                    Wyświetl
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        50
                        <i class="icon-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:void(0);" class="pagination-items-page" data-items="20" data-list-id="inpostCouriers">20</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="pagination-items-page" data-items="50" data-list-id="inpostCouriers">50</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="pagination-items-page" data-items="100" data-list-id="inpostCouriers">100</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="pagination-items-page" data-items="300" data-list-id="inpostCouriers">300</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="pagination-items-page" data-items="1000" data-list-id="inpostCouriers">1000</a>
                        </li>
                    </ul>
                    / 55 wynik(i)
                    <input type="hidden" id="inpostCouriers-pagination-items-page" name="inpostCouriers_pagination" value="50">
                </div>
                <script type="text/javascript">
                    $('.pagination-items-page').on('click',function(e){
                        e.preventDefault();
                        $('#'+$(this).data("list-id")+'-pagination-items-page').val($(this).data("items")).closest("form").submit();
                    });
                </script>
                <ul class="pagination pull-right">
                    <li class="disabled">
                        <a href="javascript:void(0);" class="pagination-link" data-page="1" data-list-id="inpostCouriers">
                            <i class="icon-double-angle-left"></i>
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="javascript:void(0);" class="pagination-link" data-page="0" data-list-id="inpostCouriers">
                            <i class="icon-angle-left"></i>
                        </a>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="pagination-link" data-page="1" data-list-id="inpostCouriers">1</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="pagination-link" data-page="2" data-list-id="inpostCouriers">2</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="pagination-link" data-page="2" data-list-id="inpostCouriers">
                            <i class="icon-angle-right"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="pagination-link" data-page="2" data-list-id="inpostCouriers">
                            <i class="icon-double-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <script type="text/javascript">
                    $('.pagination-link').on('click',function(e){
                        e.preventDefault();

                        if (!$(this).parent().hasClass('disabled'))
                            $('#submitFilter'+$(this).data("list-id")).val($(this).data("page")).closest("form").submit();
                    });
                </script>
            </div>
        </div>
    </div>


</form>
{literal}
<script type="text/javascript">

    $('tr[id^="courier').click(function(){
        $(this).next('.hideMe').slideToggle(100);

    })
</script>

{/literal}