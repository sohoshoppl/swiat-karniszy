{**
*
* NOTICE OF LICENSE
*
*}
<div id="dispatch_points" class="q-actions">
    <button class="btn btn-danger"
            onclick="if (confirm('{l s='Delete selected dispatch points?' mod='inpost'}'))sendBulkAction($(this).closest('form').get(0), 'delete_dispatch_points');">
        {l s='Delete selected' mod='inpost'}
    </button>
</div>
{literal}
    <script type="text/javascript">
        $('#dispatch_points').insertAfter('.btn-group.bulk-actions.dropup');
        $('.btn-group.bulk-actions.dropup').css('float', 'left');
        if ($(".list-empty-msg").length == 1)
            $('#dispatch_points .btn.btn-danger').hide();

    </script>
{/literal}