{**
*
* NOTICE OF LICENSE
*
*}

<div id="EE_INPOST_MODULE_HOOK_AFTER_CARRIER">
    <script type="text/javascript">
        if(typeof EE_INPOST_MODULE_SETTINGS == 'undefined') {
            var EE_INPOST_MODULE_SETTINGS = {};
        }

        EE_INPOST_MODULE_SETTINGS.crossborderCourier = {$modulesId.CROSSBORDER_COURIER|escape:'htmlall':'UTF-8'};
        EE_INPOST_MODULE_SETTINGS.crossborderParcel = {$modulesId.CROSSBORDER_PARCEL|escape:'htmlall':'UTF-8'};
        EE_INPOST_MODULE_SETTINGS.inpost_x_border_desc_text = '{$descriptions.X_BORDER|escape:'htmlall':'UTF-8'}';
        EE_INPOST_MODULE_SETTINGS.inpost_x_border_courier_desc_text = '{$descriptions.X_BORDER_COURIER|escape:'htmlall':'UTF-8'}';
        EE_INPOST_MODULE_SETTINGS.cityUser = '{$cityUser}';
        EE_INPOST_MODULE_SETTINGS.countryUser = '{$countryUser}';
        EE_INPOST_MODULE_SETTINGS.inpostAjaxScript = '{$ajaxScript}';
        EE_INPOST_MODULE_SETTINGS.PS_ORDER_PROCESS_TYPE = {$PS_ORDER_PROCESS_TYPE};
        $ = jQuery;
    </script>

    <div class="row" style="display:none;" id="select_parcel_cross_container">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="control-label col-md-2" style="padding: 20px 0;width:10%;text-align:right;" for="select_parcel_cross">
                    {l s='Selected machine:' mod='inpost'}
                </label>
                <div class="col-md-10">
                    <a class="button btn btn-default button-medium" id="select_parcel_cross"
                       >&nbsp;
                        <p>{if !$selected_machine_cross}{l s='Select machine' mod='inpost'}{/if}{if $selected_machine_cross}{$selected_machine_cross->code|escape:'htmlall':'UTF-8'} {$selected_machine_cross->address->zip_code|escape:'htmlall':'UTF-8'} {$selected_machine_cross->address->city|escape:'htmlall':'UTF-8'} {$selected_machine_cross->address->street|escape:'htmlall':'UTF-8'} {$selected_machine_cross->address->building_number|escape:'htmlall':'UTF-8'}{/if}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;display:none;" id="phone_no_parcel_cross">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="control-label col-md-2" style="padding: 20px 0;width:10%;text-align:right;"
                       for="phone_parcel">
                    {l s='Phone No.:' mod='inpost'}
                </label>
                <div class="row">
                    <div class="col-xs-5 col-md-3">
                        <div class="validate-field">
                            <input type="text" class="form-control required" name="phone_parcel_cross" id="phone_parcel_cross" style="height:50px;font-size: 18px;"
                                   value="{if $phone_no}{$phone_no|escape:'htmlall':'UTF-8'}{/if}">
                            <span aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-3">
                        <button class="btn btn-default btn-lg" id="phone_parcel_button" style="height:50px;">{l s='Check the phone number' mod='inpost'}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="selected_parcel_cross"
           value="{if $selected_machine_cross}{$selected_machine_cross->id|escape:'htmlall':'UTF-8'}{/if}">

    <script type="text/javascript" src="{$jsFile}"></script>
</div>