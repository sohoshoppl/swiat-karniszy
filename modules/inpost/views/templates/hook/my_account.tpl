{**
*
* NOTICE OF LICENSE
*
*}
{if $return_url}
    <li class="lnk_wishlist bootstrap">
        <a class="label-tooltip" data-placement="right" href="{$return_url|escape:'htmlall':'UTF-8'}"
           title="{l s=$return_description mod='inpost'}" data-toggle="tooltip">
            <i class="icon-truck"></i>
            <span>{l s='Returns' mod='inpost'}</span>
        </a>
    </li>
{/if}
{literal}
    <script type="text/javascript">
        $(function () {
            $('.label-tooltip').tooltip();
        })
    </script>
    <style>
        .tooltip {
            position: absolute;
            z-index: 1070;
            display: block;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-style: normal;
            font-weight: normal;
            letter-spacing: normal;
            line-break: auto;
            line-height: 1.42857143;
            text-align: left;
            text-align: start;
            text-decoration: none;
            text-shadow: none;
            text-transform: none;
            white-space: normal;
            word-break: normal;
            word-spacing: normal;
            word-wrap: normal;
            font-size: 12px;
            opacity: 0;
            filter: alpha(opacity=0);
        }

        .tooltip.in {
            opacity: 0.9;
            filter: alpha(opacity=90);
        }


        .tooltip.right {
            margin-left: 3px;
            padding: 0 5px;
        }


        .tooltip-inner {
            max-width: 200px;
            padding: 3px 8px;
            color: #ffffff;
            text-align: center;
            background-color: #000000;
            border-radius: 4px;
        }

        .tooltip-arrow {
            position: absolute;
            width: 0;
            height: 0;
            border-color: transparent;
            border-style: solid;
        }

        .tooltip.right .tooltip-arrow {
            top: 50%;
            left: 0;
            margin-top: -5px;
            border-width: 5px 5px 5px 0;
            border-right-color: #000000;
        }
    </style>
{/literal}