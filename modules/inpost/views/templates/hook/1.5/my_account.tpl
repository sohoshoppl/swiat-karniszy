{**
*
* NOTICE OF LICENSE
*
*}
{if $return_url}
    <li class="lnk_wishlist">
        <a 	href="{$return_url|escape:'htmlall':'UTF-8'}" title="{l s='Returns' mod='inpost'}">
            <i class="icon-truck"></i>
            <img src="{$img_dir|escape:'htmlall':'UTF-8'}icon/return.gif" class="icon" />
            <span>{l s='Returns' mod='inpost'}</span>
        </a>
    </li>
{/if}