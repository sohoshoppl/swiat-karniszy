{**
*
* NOTICE OF LICENSE
*
*}
<fieldset style="margin-top: 10px;">
    <legend><img class="q-img-small" style="width: 20px;" src="{$q_img_dir|escape:'htmlall':'UTF-8'}inpost-logo.png" />InPost</legend>
	<form id="inposT_order2" class="bootstrap">

		{if $errors}
			{foreach $errors as $error}
				<div class="alert alert-warning">
					{$error|escape:'htmlall':'UTF-8'}
				</div>
			{/foreach}
		{/if}
		<input type="hidden" name="shipping_cod" value="{$shipping_cod|escape:'htmlall':'UTF-8'}">
		<input type="hidden" name="action" value="" id="actionAjax">
		<input type="hidden" name="id_order" value="{$order->id|escape:'htmlall':'UTF-8'}">
		<input type="hidden" name="x_border_order" value="{$x_border|escape:'htmlall':'UTF-8'}">
		{if $show_parcel !== false}
			<input type="hidden" name="parcel_id" value="{$show_parcel|escape:'htmlall':'UTF-8'}">
		{/if}

		<div id="inpost_order">
			<div class="panel">
				<div class="panel-heading q-heading">

					<div class="panel-heading-action"></div>
				</div>
				{if $packages}
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<table class="table q-table-packages">
								<thead style="background-color: #108512;color: #fff;">
									<tr>
										<th>{l s='Shipping number' mod='inpost'}</th>
										<th>{l s='Status' mod='inpost'}</th>
										<th class="text-center">{l s='Action' mod='inpost'}</th>

									</tr>
								</thead>
								<tbody>
									{foreach $packages as $package}
										<tr>

											<td>{$package.parcel_no|escape:'htmlall':'UTF-8'}</td>
											<td>{if isset($PARCEL_STATUSES[$package.status|lower])}{$PARCEL_STATUSES[$package.status|lower]|escape:'htmlall':'UTF-8'}{else}{$package.status|escape:'htmlall':'UTF-8'}{/if}</td>
											<td class="text-center">
												<a class="btn btn-default" href="?tab=AdminOrders&id_order={$order->id|escape:'htmlall':'UTF-8'}&vieworder&show_parcel={$package.parcel_no|escape:'htmlall':'UTF-8'}&token={getAdminToken tab='AdminOrders'}"><i class="icon-eye"></i> {l s='Show' mod='inpost'}</a>
											</td>
										</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3 text-right">
							<a class="btn btn-primary align_right" href="?tab=AdminOrders&id_order={$order->id|escape:'htmlall':'UTF-8'}&add_new_package&vieworder&token={getAdminToken tab='AdminOrders'}">{l s='Add new package' mod='inpost'}</a>
						</div>
					</div>
				{/if}
				<div class="show_more_div" style="{if $packages && !$add_new_package && !$show_parcel}display: none;{/if}">
					<div class="row">
						<hr />
						<div class="col-lg-12">
							<div class="panel panel-sm clearfix">
								<div class="panel-heading">
									<i class="icon-user"></i>
									<h4>{l s='Sender:' mod='inpost'}</h4>
								</div>
								<div class="panel-body">
									<div class="form-horizontal">
										{if $inpost_country == 1}
											<div class="form-group">
												<label class="col-lg-2 control-label required">{l s='Method of giving' mod='inpost'}</label>
												<div class="col-lg-8">
													<select class="form-control" name="method_giving">
														<option value="0">{l s='Select giving method' mod='inpost'}</option>
														<option value="parcel_locker" {if $sender.INPOST_SENDER_PARCEL_LOCKER && $show_parcel != false}selected="selected"{/if} >{l s='From parcel locker' mod='inpost'}</option>
														<option value="dispatch_point" {if $sender.INPOST_SENDER_DISPATCH_POINT}selected="selected"{/if}>{l s='From dispatch point' mod='inpost'}</option>
													</select>
												</div>
											</div>
											<div class="form-group" id="dispatch_method" style="display:{if $sender.INPOST_SENDER_DISPATCH_POINT}block;{else}none;{/if}">
												<label class="col-lg-2 control-label">{l s='Dispatch point' mod='inpost'}</label>
												<div class="col-lg-8">
													<select class="form-control" name="dispatch_points_sender">
														<option value="-1">{l s='Select' mod='inpost'}</option>
														{if isset($dispatch_points)}
															{foreach $dispatch_points as $point}
																{if $point.status == 'ACTIVE'}
																	<option {if $sender.INPOST_SENDER_DISPATCH_POINT == $point.id} selected="selected" {/if} value="{$point.id|escape:'htmlall':'UTF-8'}">{$point.name|escape:'htmlall':'UTF-8'}</option>
																{/if}
															{/foreach}
														{/if}
													</select>
												</div>
											</div>

											<div id="parcel_method" class="form-group">
												<label class="col-lg-2 control-label required">{l s='Source parcel locker' mod='inpost'}</label>
												<div class="col-lg-7">
													<input type="text" class="form-control" name="sender[source_machine_id]" value="{$sender.INPOST_SENDER_PARCEL_LOCKER|escape:'htmlall':'UTF-8'}">
												</div>
												<div class="col-lg-1">
												   <a class="btn btn-primary openMapB" onclick="openMap('sender');
														return false;">{l s='Map' mod='inpost'}</a>
												</div>
											</div>
										{/if}
										<div class="form-group">

											<label class="col-lg-2 control-label">{l s='Company name' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[company_name]" value="{$sender.INPOST_SENDER_COMPANY_NAME|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='Firstname' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[firstname]" value="{$sender.INPOST_SENDER_FIRSTNAME|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='Lastname' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[lastname]" value="{$sender.INPOST_SENDER_LASTNAME|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label required">{l s='Street' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[street]" value="{$sender.INPOST_SENDER_STREET|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label required">{l s='Building No.' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[building_no]" value="{$sender.INPOST_SENDER_BUILDING_NO|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='Flat No.' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[flat_no]" value="{$sender.INPOST_SENDER_FLAT_NO|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label required">{l s='Postal code' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[postcode]" value="{$sender.INPOST_SENDER_POSTCODE|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label required">{l s='City' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="sender[city]" value="{$sender.INPOST_SENDER_CITY|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										{*<div class="form-group">*}
                                        {*<label class="col-lg-2 control-label">Kraj</label>*}

                                        {*<div class="col-lg-8">*}
										{*<select class="form-control" name="country" id="sender_country">*}
										{*{foreach $countries as $country}*}
										{*<option value="{$country.id_country|escape:'htmlall':'UTF-8'}">{$country.name|escape:'htmlall':'UTF-8'}</option>*}
										{*{/foreach}*}
										{*</select>*}
                                        {*</div>*}
                                        {*<div class="col-lg-2">*}

                                        {*</div>*}
										{*</div>*}
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='E-mail' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="email" class="form-control" name="sender[email]" value="{$sender.INPOST_SENDER_EMAIL|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='Phone' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="email" class="form-control" name="sender[phone]" value="{$sender.INPOST_SENDER_PHONE|escape:'htmlall':'UTF-8'}">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">{l s='Reference customer' mod='inpost'}</label>
											<div class="col-lg-8">
												<input type="text" class="form-control" id="customerReference" name="sender[customer_reference]" value="{$sender.INPOST_CUSTOMER_REFERENCE|escape:'htmlall':'UTF-8'}">
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="panel panel-sm clearfix">
								<div class="panel-heading">
									<i class="icon-user"></i>
									<h4>{l s='Receiver:' mod='inpost'}</h4>
								</div>

								<div class="panel-body">
									<div class="form-horizontal">
										{if $show_parcel === false}
											<div class="form-group">
												<label class="col-lg-3 control-label">{l s='Receiver address' mod='inpost'}</label>
												<div class="col-lg-7">
													<select class="form-control" name="shipping_address_inpost">
														{foreach from=$customer_addresses item=address}
															<option value="{$address['id_address']|escape:'htmlall':'UTF-8'}"
																	{if $address['id_address'] == $order->id_address_invoice}
																		selected="selected"
																	{/if}>
																{$address['alias']|escape:'htmlall':'UTF-8'} -
																{$address['address1']|escape:'htmlall':'UTF-8'}
																{$address['postcode']|escape:'htmlall':'UTF-8'}
																{$address['city']|escape:'htmlall':'UTF-8'}
																{if !empty($address['state'])}
																	{$address['state']|escape:'htmlall':'UTF-8'}
																{/if},
																{$address['country']|escape:'htmlall':'UTF-8'}
															</option>
														{/foreach}
													</select>
												</div>
												<div class="col-lg-2">
													<button class="btn btn-default" type="button" id="changeShippingAddress"><i class="icon-refresh"></i> {l s='change' mod='inpost'}</button>
												</div>
											</div>
											<div class="margin20"></div>
										{/if}
										{foreach from=$customer_addresses item=address}
											{if $show_parcel !== false || $address['id_address'] == $order->id_address_invoice || $customer_addresses|count == 1}

												<input type="hidden" name="receiver[id_customer]" value="{$address.id_customer|escape:'htmlall':'UTF-8'}">
												<div class="form-group">
													<label class="col-lg-3 control-label company-l required">{l s='Company name' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" value="{$address.company|escape:'htmlall':'UTF-8'}" class="form-control company-r" name="receiver[company_name]">
														<span id="helpBlock" class="help-block">{l s='Company name is required when firstname and lastname are blank' mod='inpost'}</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label firstname-l required">{l s='Firstname' mod='inpost'} </label>
													<div class="col-lg-7">
														<input type="text" value="{$address.firstname|escape:'htmlall':'UTF-8'}" class="form-control" name="receiver[firstname]">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label lastname-l required">{l s='Lastname' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" value="{$address.lastname|escape:'htmlall':'UTF-8'}" class="lastname-r form-control" name="receiver[lastname]">
														<span id="helpBlock" class="help-block">{l s='Lastname and firstname is required when company name is blank' mod='inpost'}</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label required">{l s='Street' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[street]" value="{$address.street|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label required">{l s='Building No.' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[building_no]" value="{$address.building_no|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">{l s='Flat No.' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[flat_no]" value="{$address.flat_no|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label required">{l s='Postal code' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" value="{$address.postcode|escape:'htmlall':'UTF-8'}" class="form-control" name="receiver[postcode]">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label required">{l s='City' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[city]" value="{$address.city|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">{l s='Country' mod='inpost'}</label>
													<div class="col-lg-7">
														<select class="form-control" name="receiver[country]">
															{foreach $countries as $country}
																<option {if $address.id_country == $country.id_country} selected="selected" {/if} value="{$country.id_country|escape:'htmlall':'UTF-8'}">{$country.name|escape:'htmlall':'UTF-8'}</option>
															{/foreach}
														</select>
													</div>
													<div class="col-lg-2">

													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">{l s='E-mail' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="email" class="form-control" name="receiver[email]" value="{$address.email|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">{l s='Phone' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[phone]" value="{$address.phone|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label required">{l s='Mobile phone' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[phone_mobile]" value="{$address.phone_mobile|escape:'htmlall':'UTF-8'}">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label {if !x_border}required{/if}">{l s='Parcel locker' mod='inpost'}</label>
													<div class="col-lg-7">
														<input type="text" class="form-control" name="receiver[parcel_locker]" value="{if $selected_parcel && !isset($address.parcel_locker)}{$selected_parcel|escape:'htmlall':'UTF-8'}{/if}{if isset($address.parcel_locker)}{$address.parcel_locker|escape:'htmlall':'UTF-8'}{/if}">
													</div>
													<script type="text/javascript" src="{$INPOST_GEOWIDGET_URL|escape:'htmlall':'UTF-8'}dropdown.php"></script>
													<script type="text/javascript">
													var geo_url = '{$INPOST_GEOWIDGET_URL|escape:'htmlall':'UTF-8'}';
													var geo_url_receiver = '{$geowidget_receiver_url|escape:'htmlall':'UTF-8'}';
													</script>

													{literal}
														<script type="text/javascript">
															function user_function(name, objMachine, openIndex) {
																var address = name.split(';');
																if (address[4] == 'receiver')
																	$('input[name="receiver[parcel_locker]"').val(address[0]);
																if (address[4] == 'sender')
																	$('input[name="sender[source_machine_id]"').val(address[0]);
															}
															$(function () {
																$('#openMapReceiver').click(function () {
																	$.getScript(geo_url_receiver + '/dropdown.php?user_function=user_function', function () {
																		openMap('receiver');
																	});
																});

																$('.openMapB').click(function () {
																	$.getScript(geo_url + '/dropdown.php?user_function=user_function', function () {
																		openMap('sender');
																	});
																});
															})
														</script>

													{/literal}
													<div class="col-lg-1">
													   <a class="btn btn-primary" onclick="openMap('receiver');
																	return false;">{l s='Map' mod='inpost'}</a>
													</div>
												</div>
											{/if}
										{/foreach}
									</div>
								</div>
							</div>
						</div>
					</div>
					{if !$x_border}
						<div class="row">
							<hr />
							<div class="col-lg-11 col-lg-offset-1">
								<div class="form-group">
									<label class="col-lg-2 control-label q-text-right required" style="margin-top:10px;">{l s='Parcel size' mod='inpost'}</label>
									<div class="col-lg-10">
										<div class="row">
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="A" {if $size_parcel === 'A'}checked="checked"{/if}>
													{l s='Size A: 8 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
										</div>
										<div class="row">
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="B" {if $size_parcel === 'B'}checked="checked"{/if}>
													{l s='Size B: 19 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
										</div>
										<div class="row">
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="C" {if $size_parcel === 'C'}checked="checked"{/if}>
													{l s='Size C: 41 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					{/if}
					{if $x_border}
						<div class="row">
							<hr />
							<div class="col-lg-11 col-lg-offset-1">
								<div class="form-group">
									<label class="col-lg-2 control-label q-text-right" style="margin-top:10px;">{l s='Parcel weight' mod='inpost'}</label>
									<div class="col-lg-1">
										<select class="form-control" name="parcel_weight_unit">
											{if $show_parcel === false}
												<option name="kg" selected="selected">{l s='KG' mod='inpost'}</option>
												<option name="g">{l s='G' mod='inpost'}</option>
											{else}
												<option name="kg" {if $package_db.declared_weight_unit == 'kg'} selected="selected" {/if}>{l s='KG' mod='inpost'}</option>
												<option name="g"{if $package_db.declared_weight_unit == 'g'} selected="selected" {/if}>{l s='G' mod='inpost'}</option>
											{/if}
										</select>
									</div>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="parcel_weight" value="{if $show_parcel === false}{$order->getTotalWeight()|escape:'htmlall':'UTF-8'}{else}{$package.declared_weight_amount|escape:'htmlall':'UTF-8'}{/if}">
									</div>


								</div>
							</div>
						</div>
						{if $x_border}
							<div class="row">
								<hr />
								<div class="col-lg-11 col-lg-offset-1 form-horizontal">
									<div class="form-group">
										<label class="col-lg-2 control-label q-text-right" style="margin-top:10px;">{l s='Package size unit' mod='inpost'}</label>
										<div class="col-lg-3">
											<select class="form-control" name="parcel_size_unit">
												{if $show_parcel === false}
													<option name="mm">{l s='mm' mod='inpost'}</option>
													<option name="cm" selected="selected">{l s='cm' mod='inpost'}</option>
													<option name="m">{l s='m' mod='inpost'}</option>
												{else}
													<option name="mm" {if $package_db.declared_dimensions_unit == 'mm'} selected="selected" {/if}>{l s='mm' mod='inpost'}</option>
													<option name="cm"{if $package_db.declared_dimensions_unit == 'cm'} selected="selected" {/if}>{l s='cm' mod='inpost'}</option>
													<option name="m"{if $package_db.declared_dimensions_unit == 'm'} selected="selected" {/if}>{l s='m' mod='inpost'}</option>
												{/if}
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label q-text-right">{l s='Package height' mod='inpost'}</label>
										<div class="col-lg-3">
											<input type="text" class="form-control" name="parcel_height" value="{if $package_db}{$package_db.declared_dimensions_height|escape:'htmlall':'UTF-8'}{else}{$sizes.height|escape:'htmlall':'UTF-8'}{/if}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label q-text-right" style="margin-top:10px;">{l s='Package length' mod='inpost'}</label>
										<div class="col-lg-3">
											<input type="text" class="form-control" name="parcel_length" value="{if $package_db}{$package_db.declared_dimensions_length|escape:'htmlall':'UTF-8'}{else}{$sizes.length|escape:'htmlall':'UTF-8'}{/if}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label q-text-right" style="margin-top:10px;">{l s='Package width' mod='inpost'}</label>
										<div class="col-lg-3">
											<input type="text" class="form-control" name="parcel_width" value="{if $package_db}{$package_db.declared_dimensions_width|escape:'htmlall':'UTF-8'}{else}{$sizes.width|escape:'htmlall':'UTF-8'}{/if}">
										</div>
									</div>
								</div>
							</div>
						{else}
							<div class="row">
								<hr />
								<div class="col-lg-11 col-lg-offset-1">
									<div class="form-group">
										<label class="col-lg-2 control-label q-text-right" style="margin-top:10px;">{l s='Parcel size' mod='inpost'}</label>
										<div class="col-lg-10">
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="A" {if $size_parcel === 'A'}checked="checked"{/if}>
													{l s='Size A: 8 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="B" {if $size_parcel === 'B'}checked="checked"{/if}>
													{l s='Size B: 19 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="parcel_size" id="optionsRadios1" value="C" {if $size_parcel === 'C'}checked="checked"{/if}>
													{l s='Size C: 41 x 38 x 64 cm' mod='inpost'}
												</label>
											</div>
										</div>

									</div>
								</div>
							</div>
						{/if}
					{/if}
					<div class="row">
						<hr />
						<div class="col-lg-11 col-lg-offset-1">
							<div class="form-group">
								<label class="col-lg-2 control-label q-text-right">
									<input type="checkbox" name="parcel_insurance" style="bottom:-2px" {if $insurance_amount} checked="checked" {/if}>
									{l s='Insurance' mod='inpost'}
								</label>
								<div class="col-lg-10">
									<select class="form-control q-max-width" name="parcel_insurance_amount">
										<option>---</option>
										{foreach $insurances as $insurance}
											<option {if $insurance_amount == $insurance->level} selected="selected" {/if}value="{$insurance->level|escape:'htmlall':'UTF-8'}">{l s='Insurance amount: %s' sprintf=$insurance->level mod='inpost'}{$currency|escape:'htmlall':'UTF-8'} | {l s='Cost of insurance: %s' sprintf=$insurance->price mod='inpost'}{$currency|escape:'htmlall':'UTF-8'}</option>
										{/foreach}
									</select>
								</div>
							</div>

						</div>
					</div>
					{if !$x_border}
						<div class="row">
							<hr />
							<div class="col-lg-11 col-lg-offset-1">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="order_comments" class="col-lg-2 control-label q-text-right">{l s='Comments' mod='inpost'}</label>
										<div class="col-lg-10">
											<textarea rows="5" cols="50" id="order_comments" name="order_comments" class="form-control">{$comments|escape:'htmlall':'UTF-8'}</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					{/if}
					{if $show_parcel === false}
						<hr />
						<div class="row">
							<div class="col-lg-12">
								<div class="center-block text-center">
									<button type="button" id="create_parcel" class="btn btn-primary">{l s='CREATE PARCEL' mod='inpost'}</button>
									<button type="button" id="create_parcel_and_label" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{l s='Create a label is obligatory to pay' mod='inpost'}">{l s='CREATE PARCEL AND PRINT LABEL' mod='inpost'}</button>
									<i class="icon icon-info"  data-toggle="tooltip" data-placement="top" title="{l s='Create a label is obligatory to pay' mod='inpost'}"></i>
								</div>
							</div>
							<br />
							<div class="col-lg-12" style="margin-top: 20px;">
								<div class="alert alert-danger error" style="display: none;">

								</div>
							</div>
						</div>
					{else}
						<br />
						{if !$x_border}
							{if $package_db['package_id']}
								<div class="alert alert-info">
									{l s='Label generated: ' mod='inpost'}
									<i class="icon-ok icon-ok-circle"></i>
								</div>
							{elseif $package_db['status'] == 'created'}
								<div class="col-lg-12">
									<div class="col-lg-12">
										<div class="center-block text-center">
											<button type="button" id="create_label" class="btn btn-primary">{l s='Generate and download a label' mod='inpost'}</button>
											<button type="button" id="update_parcel" class="btn btn-primary">{l s='Update customer reference and parcel size' mod='inpost'}</button>
										</div>
									</div>

								</div>
							{/if}
						{else}
							Xborder download label
						{/if}
						<div class="col-lg-12" style="margin-top: 20px;">
							<div class="alert alert-danger error" style="display: none;">

							</div>
						</div>
					{/if}
					<div class="row">
						<div class="col-lg-2 col-lg-offset-5 text-center resultDiv">

						</div>
					</div>

				</div>
			</div>
		</div>
	</form>

	<script>
	var inpost_ajax_request = '{$inpost_ajax_request}';
	var show_parcel_info = '{$show_parcel|escape:'htmlall':'UTF-8'}';
	var add_new_package = '{$add_new_package|escape:'htmlall':'UTF-8'}';
	$(function () {
		firstname = $('.firstname-r').val();
		lastname = $('.lastname-r').val();
		company = $('.company-r').val();

		if (firstname || lastname)
		{
			$('.company-l').removeClass('required')
		}
		if (company)
		{
			$(".lastname-l").removeClass('required');
			$(".firstname-l").removeClass('required');
		}
	})
	</script>

	<style type="text/css">
		label.required:before {
			content: "*";
			color: red;
			font-size: 14px;
			position: relative;
			line-height: 12px;
		}
	</style>
</fieldset>
