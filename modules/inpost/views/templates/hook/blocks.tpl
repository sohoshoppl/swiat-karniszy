{if $br}<br />{/if}

{if $hookPaymentHide}
    <script type="text/javascript">
        $(function() {
            $("#HOOK_PAYMENT").hide();
        });
    </script>
{/if}

{if $hookPaymentShow}
    <script type="text/javascript">
        $(function() {
            $("#HOOK_PAYMENT").show();
        });
    </script>
{/if}

{if $manifestPDFFooter}<div style="text-align: right;">{literal}Strona {PAGENO} z {nb}</div>{/literal}{/if}

{if $desc}
    {$eg}<br /><p style="color: #f13340" class="required"><sup>*</sup>-{$required}</p>
{/if}

{if $validateOnePageCheckoutError}
    <div id="EE_HOOK_PAYMENT_TOP_INPOST_ERROR">
        <p class="warning parcel-locker-not-chosen">{$errorText}</p>
    </div>
{/if}