{**
*
* NOTICE OF LICENSE
*
*}
<div id="EE_INPOST_MODULE_HOOK_AFTER_CARRIER">
    <script type="text/javascript" src="{$INPOST_GEOWIDGET_URL|escape:'htmlall':'UTF-8'}dropdown.php?user_function=user_function"></script>
    <script type="text/javascript">
        {literal}
        var EE_INPOST_MODULE_SETTINGS = {
        {/literal}
            'parcelsLockers': {$modulesId.PARCELS_LOCKERS|escape:'htmlall':'UTF-8'},
            'parcelsLockersCod': {$modulesId.PARCELS_LOCKERS_COD|escape:'htmlall':'UTF-8'},
            'inpostParcelLockerDescText': '{$descriptions.PARCELS_LOCKERS|escape:'htmlall':'UTF-8'}',
            'inpostParcelLockerCodDescText': '{$descriptions.PARCELS_LOCKERS_COD|escape:'htmlall':'UTF-8'}',
            'cityUser': '{$cityUser}',
            'inpostAjaxScript': '{$ajaxScript}',
            'PS_ORDER_PROCESS_TYPE': {$PS_ORDER_PROCESS_TYPE},
            'INPOST_COUNTRY': {$INPOST_COUNTRY}
        {literal}
        };
        {/literal}

        {*var parcelsLockers = {$modulesId.PARCELS_LOCKERS|escape:'htmlall':'UTF-8'};*}
        {*var parcelsLockersCod = {$modulesId.PARCELS_LOCKERS_COD|escape:'htmlall':'UTF-8'};*}
        {*var inpost_parcel_locker_desc_text = '{$descriptions.PARCELS_LOCKERS|escape:'htmlall':'UTF-8'}';*}
        {*var inpost_parcel_locker_cod_desc_text = '{$descriptions.PARCELS_LOCKERS_COD|escape:'htmlall':'UTF-8'}';*}
        {*var inpost_x_border_desc_text = '{$descriptions.X_BORDER|escape:'htmlall':'UTF-8'}';*}
        {*var inpost_x_border_courier_desc_text = '{$descriptions.X_BORDER_COURIER|escape:'htmlall':'UTF-8'}';*}
        {*var crossborderCourier = {$modulesId.CROSSBORDER_COURIER|escape:'htmlall':'UTF-8'};*}
        {*var crossborderParcel = {$modulesId.CROSSBORDER_PARCEL|escape:'htmlall':'UTF-8'};*}
        {*var cityUser = '{$cityUser}';*}
        {*var PS_ORDER_PROCESS_TYPE = {$PS_ORDER_PROCESS_TYPE};*}
        {*var inpost_ajax_script = '{$ajaxScript}';*}
    </script>
    {if $errors_inpost}
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger">
                    {$errors_inpost}
                </div>
            </div>
        </div>
    {/if}
    <div class="row" style="display:none;" id="select_parcel_container">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="control-label col-md-2" style="padding: 20px 0;width:10%;text-align:right;" for="phone_parcel">
                    {l s='Selected machine:' mod='inpost'}
                </label>
                <div class="col-md-10">
                    <a class="button btn btn-default button-medium" id="select_parcel"
                       style="">&nbsp;
                        <p>{if !$selected_machine}{l s='Select machine' mod='inpost'}{/if}{if $selected_machine}{$selected_machine->id} {$selected_machine->address->city|escape:'htmlall':'UTF-8'} {$selected_machine->address->street|escape:'htmlall':'UTF-8'} {$selected_machine->address->building_no|escape:'htmlall':'UTF-8'} {$selected_machine->address->flat_no|escape:'htmlall':'UTF-8'}{/if}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;display:none;" id="phone_no_parcel">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="control-label col-md-2" style="padding: 20px 0;width:10%;text-align:right;" for="phone_parcel">
                    {l s='Phone No.:' mod='inpost'}
                </label>
                <div class="row">
                    <div class="col-xs-5 col-md-3">
                        <div class="validate-field">
                            <input type="text" class="form-control required" name="phone_parcel_locker" id="phone_parcel" style="height:50px;font-size: 18px;"
                                   value="{if $phone_no}{$phone_no|escape:'htmlall':'UTF-8'}{/if}">
                            <span aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-3">
                        <button class="btn btn-default btn-lg" id="phone_parcel_button" style="height:50px;">{l s='Check the phone number' mod='inpost'}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="selected_parcel"
           value="{if $selected_machine}{$selected_machine->id|escape:'htmlall':'UTF-8'}{/if}">


    <script type="text/javascript" src="{$jsFile}"></script>
</div>