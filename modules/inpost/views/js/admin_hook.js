/**
 *
 * NOTICE OF LICENSE
 *
 */
$ = jQuery;
$(function () {

	$('#inposT_order2').insertAfter('.panel.kpi-container')
	$('.show_more').click(function () {
		$('.show_more_div').show();
	})
	if (add_new_package) {
		$('.show_more_div').addClass('in');
	}
	//$('[data-toggle="tooltip"]').tooltip()

	$('input[name="parcel_insurance"]').on('change', function () {
		if ($('input[name="parcel_insurance"]:checked').val() == 0) {
			$('.insurance_visibility').show();
		} else {
			$('.insurance_visibility').hide();
		}
	});
	$('#parcel_insurance').prop('checked', true).trigger('change');
	$('#create_label').click(function () {
		$('#update_parcel').attr('disabled', 'disabled')
	});


	$('#create_parcel, #create_parcel_and_label, #create_label, #update_parcel, #download_label_x').click(function () {
		$('#actionAjax').val($(this).attr('id'));
		$('#create_parcel, #create_parcel_and_label').attr('disabled', 'disabled');
		$('.alert-danger.error').hide();

		$.ajax({
			type: "POST",
			async: true,
			url: inpost_ajax_request,
			dataType: "jsonp",
			jsonpCallback: 'callback',
			global: false,
			data: $('#inposT_order2').serialize(),
			success: function (resp) {


				if (resp['url']) {
					window.location.replace(resp['url']);
				}
				if (resp.id) {
					window.location.reload();
				}

				if (resp.error == 1) {
					$('.alert-danger.error').text('');
					for (var key in resp) {
						if (key != 'error') {
							$('.alert-danger.error').append(resp[key] + '<br />');
							$('.alert-danger.error').show();
							$('#create_parcel, #create_parcel_and_label').attr('disabled', false);
							$("html, body").animate({scrollTop: $('.alert-danger.error').offset().top - 300}, 1000);
						}

					}
				} else {
					if ($('input[name=x_border_order]').val() == 1) {
						window.location.reload();
					} else {
						if (resp['url']) {
							window.location.replace(resp['url']);
							setTimeout(function () {
								$("html, body").animate({scrollTop: $('#inpost_order').offset().top - 100}, 1000);
								window.location.reload(true);
							}, 3000)
						}
						if (resp.id) {
							window.location.reload();
						}
					}
				}
				$('#create_parcel, #create_parcel_and_label').attr('disabled', false);
			},
			error: function (xhr, status, error) {
				var msg = '';
				if (error) {
					msg = error + "\n";
				}
				if (xhr.responseText) {
					msg += xhr.responseText + "\n";
				} else {
					msg += 'Unexpected Error' + "\n";
				}
				if (xhr.readyState) {
					msg += xhr.readyState;
				}
				if (status) {
					msg += status;
				}
				alert(msg);
				$('#create_parcel, #create_parcel_and_label').attr('disabled', false);
			}
		});
	})

	$('#changeShippingAddress').click(function () {
		$('#changeShippingAddress i').addClass('loading');
		$.ajax({
			type: "GET",
			async: true,
			url: inpost_ajax_request,
			dataType: "jsonp",
			jsonpCallback: 'callback',
			global: false,
			data: {order_id: id_order, action: 'get_address', address_id: $('select[name="shipping_address_inpost"]').val()},
			success: function (resp) {


				for (var key in resp) {
					var obj = resp[key];
					$('input[name="receiver[' + key + ']"]').val(obj);

				}
				setTimeout(function () {
					$('#changeShippingAddress i').removeClass('loading');
				}, 100)

			},
			error: function () {

				alert('Error')
			}
		});
	})
	if (show_parcel_info) {
		$('#inpost_order input:not(#customerReference)').attr('readOnly', true);
		$('#inpost_order select').attr('disabled', 'disabled');
		$('#inpost_order input[type="checkbox"]').attr('disabled', 'disabled');
		$('.openMapB').attr('disabled', 'disabled');
		$("html, body").animate({scrollTop: $('.show_more_div').offset().top - 100}, 1000);
	}

	$('select[name=method_giving]').change(function () {
		if ($(this).val() == 'parcel_locker') {
			$('#parcel_method').show();
			$('#dispatch_method').hide();
			$('select[name=dispatch_points_sender]').attr('disabled', true);
			$('input[name="sender[source_machine_id]"]').attr('disabled', false);
		}
		if ($(this).val() == 'dispatch_point') {
			$('#dispatch_method').show();
			$('#parcel_method').hide();
			$('input[name="sender[source_machine_id]"]').attr('disabled', true);
			$('select[name=dispatch_points_sender]').attr('disabled', false);
		}
	}).trigger('change');
})


function generateMap()
{

	sheepla.api_key = '2wz57e5nb7ew9iz0ae7g8j07lr498kg0';
	jQuery(document).ready(function () {
		jQuery('#sheepla_method').checked = true;
		var map = sheepla.widget.call('crossborderMap', {
			country: 'SK',
			postal_code: '',
			city: '',
			street: '',
			building_number: '',
			target: "#openMapReceiver",
			mapTarget: '#content'
		});
		map.em.attach('after.pop_select', function (ctx, params) {
			$('#select_parcel_cross').show();
			$('#select_parcel_cross p').text(params.selectedPopCode + ' ' + params.selectedPopAddress);
			$('.sheepla-map-trigger-selected-point').hide();
			$('#select_parcel_cross').show();
		});
	});
	$(".sheepla-map-trigger-element").attr('type', 'button');
	$('#select_parcel_cross').click(function () {
		$(".sheepla-map-trigger-element").click()
	})
	$('.sheepla-map-trigger-selected-point').hide();
}
$(function () {
	//$('[data-toggle="tooltip"]').tooltip();
});
