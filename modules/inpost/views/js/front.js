/**
 *
 * NOTICE OF LICENSE
 *
 */

$('#phone_parcel_button').click(function (e) {
    e.preventDefault();

    var phone_no = $('#phone_parcel').val();
    $.ajax({
        type: "POST",
        url: EE_INPOST_MODULE_SETTINGS.inpostAjaxScript,
        dataType: "json",
        data: {action: 'save_phone_no', phone_no: phone_no},
        success: function (resp) {
            validatePhoneParcelField(resp);
            refreshOnePagePaymentSection(resp);
        },
        error: function () {

        }
    });

    return false;
});

$('#phone_parcel').on('change paste keyup', function () {
    $("#HOOK_PAYMENT").hide();
    validatePhoneParcelField('hide');
});

function user_function(value) {
    var address = value.split(';');
    if (address) {
        $('#select_parcel_container').css('display', 'block');
    }
    $('#select_parcel p').text(address[0] + ' ' + address[1] + ' ' + address[2] + ' ' + address[3]);
    $('input[name="selected_parcel"]').val(address[0]);

    $('#select_parcel_container').css('display', 'block');
    $('#phone_no_parcel').css('display', 'block');

    $.ajax({
        type: "POST",
        async: true,
        url: EE_INPOST_MODULE_SETTINGS.inpostAjaxScript,
        dataType: "json",
        global: false,
        data:  { action: 'save_parcel_locker', parcel_locker: address[0], phone_no: $('#phone_parcel').val() },
        success: function (resp)
        {
            refreshOnePagePaymentSection(resp);
        },
        error: function () {
            alert('Unexpected error');
        }
    });
    selectMachineFunction();

}

function selectedDelivery(valF, openMapI) {
    if (typeof openMapI == 'undefined') {
        openMapI = true;
    }
    if (typeof valF !== 'undefined') {
        var parcelLockersSelected = valF.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockers;
        var parcelLockersCodSelected = valF.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockersCod;
        if (parcelLockersSelected) {
            var cod = 'FALSE';
        }
        if (parcelLockersCodSelected) {
            var cod = 'TRUE';
        }

        // Current country is France. false and FALSE causing different behaviour in that case
        if(EE_INPOST_MODULE_SETTINGS.INPOST_COUNTRY == 2) {
            // Hide COD
            var cod = 'false';
        }

        // console.log('cod '+ cod);

        if (parcelLockersCodSelected || parcelLockersSelected) {
            // setTimeout(function () {
                $('#select_parcel_container').css('display', 'block');
                $('#phone_no_parcel').css('display', 'block');
            // }, 1000) //timeout is needed - carriers is loading via ajax
            if (openMapI) {
                openMap({
                    cod: cod,
                    town: EE_INPOST_MODULE_SETTINGS.cityUser
                });
            }
            selectMachineFunction();
        } else {
            selectMachineFunction();
        }
    }
}

function selectMachineFunction() {
    $('#select_parcel_container').click(function () {
        selectedDelivery($("input.delivery_option_radio:checked").val())
    })
}

function hackOnePageCheckoutWithBeforeCarrier() {
    if(EE_INPOST_MODULE_SETTINGS.PS_ORDER_PROCESS_TYPE == 1) {
        var element = $("#HOOK_BEFORECARRIER").find("#EE_INPOST_MODULE_HOOK_AFTER_CARRIER");
        if (element.length) {
            element.appendTo('#extra_carrier');
            $('#extra_carrier').show();

            return true;
        }
    }

   return false;
}

function refreshOnePagePaymentSection(resp)
{
    if(EE_INPOST_MODULE_SETTINGS.PS_ORDER_PROCESS_TYPE == 1) {
        $("#EE_HOOK_PAYMENT_TOP_INPOST_ERROR").remove();
        if(typeof(resp.error) != 'undefined') {
            $("#HOOK_PAYMENT").hide();
            $("#HOOK_TOP_PAYMENT").append(resp.error);
        } else {
            $("#HOOK_PAYMENT").show();
        }
    }
}

function validatePhoneParcelField(status)
{
    var validate_field_icon = $('.validate-field').find('span');
    if(status == 'hide') {
        validate_field_icon
            .removeClass('icon-remove-sign validate-field--invalid')
            .removeClass('icon-ok-sign validate-field--valid');
    } else if(status === true) {
        validate_field_icon
            .removeClass('icon-remove-sign validate-field--invalid')
            .addClass('icon-ok-sign validate-field--valid');
    } else {
        validate_field_icon
            .removeClass('icon-ok-sign validate-field--valid')
            .addClass('icon-remove-sign validate-field--invalid');
    }
}

$(function () {

    hackOnePageCheckoutWithBeforeCarrier();

    $('input.delivery_option_radio').each(function () {
        if ($(this).val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockers) {
            var element = $(this).parent().parent().parent();
			if(!element.hasClass('inpost_parcel_locker_desc')) {
				element.addClass('inpost_parcel_locker_desc');
				if (typeof EE_INPOST_MODULE_SETTINGS.inpostParcelLockerDescText != 'undefined') {
					if (element.is('td')) {
						element = element.parents('tbody');
					}
					$(element.find('td')[2]).append(EE_INPOST_MODULE_SETTINGS.inpostParcelLockerDescText);
				}
			}
        }
        if ($(this).val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockersCod) {
            var element = $(this).parent().parent().parent();
			if(!element.hasClass('inpost_parcel_locker_cod_desc')) {
				element.addClass('inpost_parcel_locker_cod_desc');
				if (typeof EE_INPOST_MODULE_SETTINGS.inpostParcelLockerCodDescText != 'undefined') {
					if (element.is('td')) {
						element = element.parents('tbody');
					}
					$(element.find('td')[2]).append(EE_INPOST_MODULE_SETTINGS.inpostParcelLockerCodDescText);
				}
			}
        }
    });

    $("input.delivery_option_radio").click(function () {
        var valF = $(this).val();
        selectedDelivery(valF, true);
        selectMachineFunction();
    });

    var valA = $("input.delivery_option_radio:checked").val();
    selectedDelivery(valA, false);


    selectMachineFunction();
    $('.delivery_option').click(function () {
        $('.delivery_option span').removeClass('checked');
        var input = $(this).find('input');

        // if (input.val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockers) {
        //     openMap({
        //         cod: 'FALSE',
        //         town: EE_INPOST_MODULE_SETTINGS.cityUser
        //     });
        // }
        // if (input.val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.parcelsLockersCod) {
        //     openMap({
        //         cod: 'TRUE',
        //         town: EE_INPOST_MODULE_SETTINGS.cityUser
        //     });
        // }
        input.attr('checked', true);
		input.change();
        input.parent('span').addClass('checked');
    });
});
