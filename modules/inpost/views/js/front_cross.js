/**
 *
 * NOTICE OF LICENSE
 *
 */

$ = jQuery;
var generatedMap = false;
$('#phone_parcel_cross').blur(function() {

    var phone_no = $(this).val();
    $.ajax({
        type: "POST",
        url: EE_INPOST_MODULE_SETTINGS.inpostAjaxScript,
        dataType: "json",
        data: {action: 'save_phone_no', phone_no: phone_no},
        success: function (resp)
        {
            crossRefreshOnePagePaymentSection(resp);
        },
        error: function()
        {

        }
    })
});

function generateMap()
{
        generatedMap = true
        if ($(".sheepla-map-trigger-element").length > 1) {
            $('.sheepla-widget-fullscreen').last().remove();
            $('.sheepla-map-trigger').last().remove();
        }


        sheepla.api_key = '6w4rlxhwibn6u2o2imu8exyzshd430t0';
        jQuery(document).ready(function () {
            jQuery('#sheepla_method').checked = false;
            var map = sheepla.widget.call('crossborderMap', {
                country: EE_INPOST_MODULE_SETTINGS.countryUser,
                postal_code: '',
                city: '',
                street: '',
                building_number: '',
                target: "#select_parcel_cross",
                mapTarget: '#page'
            });
            map.em.attach('after.pop_select', function (ctx, params) {
                $('#select_parcel_cross_container').show();
                $('#phone_no_parcel_cross').show();
                $('#select_parcel_cross p').text(params.selectedPopCode + ' ' + params.selectedPopAddress);
                $.ajax({
                    type: "POST",
                    async: true,
                    url: EE_INPOST_MODULE_SETTINGS.inpostAjaxScript,
                    dataType: "json",
                    global: false,
                    data:  { action: 'save_parcel_locker', parcel_locker: params.selectedPopCode, phone_no: $('#phone_parcel_cross').val() },
                    success: function (resp)
                    {
                        crossRefreshOnePagePaymentSection(resp);
                    },
                    error: function()
                    {
                        alert('Unexpected error');
                    }
                });
                $('.sheepla-map-trigger-selected-point').hide();
                $('#select_parcel_cross_container').show();
                $('#phone_no_parcel_cross').show();
            });
        });
        $("#select_parcel_cross_container").unbind('click');
        $('#select_parcel_cross_container').one("click", function () {
            $(".sheepla-map-trigger-element").attr('type', 'button');
            $(".sheepla-map-trigger-element").click();
        });

        $('.sheepla-map-trigger-selected-point').hide();
        $('#phone_no_parcel_cross').hide();

}

$('#phone_no_parcel_cross').blur(function() {
    var phone_no = $(this).val();
    $.ajax({
        type: "POST",
        url: EE_INPOST_MODULE_SETTINGS.inpostAjaxScript,
        dataType: "json",
        data: {action: 'save_phone_no', phone_no: phone_no},
        success: function (resp)
        {
            crossRefreshOnePagePaymentSection(resp);
        },
        error: function()
        {

        }
    })
});

function selectedDeliveryCross(valF)
{


    if (!generatedMap)
    {
        generateMap();
    }

    var crossBorderParcelSelected = valF.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderParcel;
    var crossborderCourierSelected = valF.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderCourier;

    if (crossBorderParcelSelected) {
        if ($('.sheepla-widget-fullscreen.sheepla-widget-fullscreen-visible').length == 0) {
            // $(".sheepla-map-trigger-element").click();
        }
        $('#select_parcel_cross_container').show();
        $('#phone_no_parcel_cross').show();
    } else {
        $('#select_parcel_cross_container').hide();
    }
    if (crossborderCourierSelected || crossBorderParcelSelected)
    {
        setTimeout(function(){
            $('#phone_no_parcel_cross').show();
        }, 1000)

    }
}
function generateDescriptions()
{
    $('.delivery_option_radio input').each(function(){
        if ($(this).val().slice(0,-1) == EE_INPOST_MODULE_SETTINGS.crossborderCourier) {
            $(this).parent().parent().parent().addClass('inpost_parcel_locker_desc');
            if (typeof EE_INPOST_MODULE_SETTINGS.inpost_x_border_courier_desc_text != 'undefined') {
                if ($('.inpost_parcel_locker_desc + td + td p').length < 1)
                $('.inpost_parcel_locker_desc + td + td').append('<p>'+EE_INPOST_MODULE_SETTINGS.inpost_x_border_courier_desc_text+'</p>');
            }
        }
        if ($(this).val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderParcel) {
            $(this).parent().parent().parent().addClass('inpost_parcel_locker_cod_desc');
            if (typeof EE_INPOST_MODULE_SETTINGS.inpost_x_border_desc_text != 'undefined') {
                if ($('.inpost_parcel_locker_cod_desc + td + td p').length < 1)
                    $('.inpost_parcel_locker_cod_desc + td + td').append('<p>'+EE_INPOST_MODULE_SETTINGS.inpost_x_border_desc_text+'</p>');
            }
        }
    })
}

function crossHackOnePageCheckoutWithBeforeCarrier() {
    if(EE_INPOST_MODULE_SETTINGS.PS_ORDER_PROCESS_TYPE == 1) {
        var element = $("#HOOK_BEFORECARRIER").find("#EE_INPOST_MODULE_HOOK_AFTER_CARRIER");
        if (element.length) {
            element.appendTo('#extra_carrier');
            $('#extra_carrier').show();

            return true;
        }
    }

    return false;
}

function crossRefreshOnePagePaymentSection(resp)
{
    if(EE_INPOST_MODULE_SETTINGS.PS_ORDER_PROCESS_TYPE == 1) {
        $("#EE_HOOK_PAYMENT_TOP_INPOST_ERROR").remove();
        if(typeof(resp.error) != 'undefined') {
            $("#HOOK_PAYMENT").hide();
            $("#HOOK_TOP_PAYMENT").append(resp.error);
        } else {
            $("#HOOK_PAYMENT").show();
        }
    }
}

$(function () {
    generateDescriptions();

    crossHackOnePageCheckoutWithBeforeCarrier();

    $("input.delivery_option_radio").click(function () {
        var valF = $(this).val();
        selectedDeliveryCross(valF)
    })
    var valA = $("input.delivery_option_radio:checked").val();
    var crossborderCourierSelected = valA.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderCourier;

    if (crossborderCourierSelected)
    {
        $('#phone_no_parcel_cross').show();
    }
    selectedDeliveryCross(valA);
    $('.delivery_option').click(function () {
        var input = $(this).find('input');
        if (input.val().slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderParcel) {
            selectedDeliveryCross(input.val());
        }
    });

    $("input.delivery_option_radio").click(function () {

        var valF = $(this).val();
        var crossborderCourierSelected = valF.slice(0, -1) == EE_INPOST_MODULE_SETTINGS.crossborderCourier;

        if (crossborderCourierSelected)
        {
            $('#phone_no_parcel_cross').show();
        }
    })
});
