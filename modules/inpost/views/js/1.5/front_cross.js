/**
 *
 * NOTICE OF LICENSE
 *
 */
$ = jQuery;

function generateMap()
{
    if ($(".sheepla-map-trigger-element").length == 0)
    {
        sheepla.api_key = '2wz57e5nb7ew9iz0ae7g8j07lr498kg0';
        jQuery(document).ready(function () {
            jQuery('#sheepla_method').checked = false;
            var map = sheepla.widget.call('crossborderMap',{
                country: EE_INPOST_MODULE_SETTINGS.countryUser,
                postal_code: '',
                city: '',
                street: '',
                building_number: '',
                target: "#select_parcel_cross",
                mapTarget: '#page'
            });
            map.em.attach('after.pop_select', function (ctx, params) {
                $('#select_parcel_cross').show();

                $('#select_parcel_cross p').text(params.selectedPopCode + ' '+ params.selectedPopAddress);
                $('.sheepla-map-trigger-selected-point').hide();
                $('#select_parcel_cross').show();
                $('#phone_no_parcel_cross').show();
            });
        });
        $(".sheepla-map-trigger-element").prop('type', 'button');
        $('#select_parcel_cross').click(function () {
            $(".sheepla-map-trigger-element").click()
        })
        $('.sheepla-map-trigger-selected-point').hide();
    }
}

function selectedDeliveryCross(valF)
{
    generateMap();
    var crossBorderParcelSelected = valF.match(EE_INPOST_MODULE_SETTINGS.crossborderParcel);
    var crossborderCourierSelected = valF.match(EE_INPOST_MODULE_SETTINGS.crossborderCourier);
    if (crossBorderParcelSelected) {
        if ($('.sheepla-widget-fullscreen.sheepla-widget-fullscreen-visible').length == 0) {
            $(".sheepla-map-trigger-element").click();
        }
        $('#select_parcel_cross').show();
        $('#phone_no_parcel_cross').show();
    } else {
        $('#select_parcel_cross').hide();

    }

    if (crossborderCourierSelected || crossBorderParcelSelected)
    {
        setTimeout(function(){
            $('#phone_no_parcel_cross').show();
        }, 1000)

    }
    else
    {

        $('#phone_no_parcel_cross').hide();
    }
}

$(function () {
    $("input.delivery_option_radio").click(function () {
        var valF = $(this).val();
        selectedDeliveryCross(valF)
    })


    var valA = $("input.delivery_option_radio:checked").val();
    selectedDeliveryCross(valA);
    var crossborderCourierSelected = valA.match(EE_INPOST_MODULE_SETTINGS.crossborderCourier);
    if (crossborderCourierSelected)
    {
        $('#phone_no_parcel_cross').show();
    }

    $('.delivery_option').click(function () {
        var input = $(this).find('input');

        if (input.val().match(EE_INPOST_MODULE_SETTINGS.crossborderParcel) || input.val().match(EE_INPOST_MODULE_SETTINGS.crossborderCourier)) {
            selectedDeliveryCross(input.val());
        }
    })
    $("input.delivery_option_radio").click(function () {
        var valF = $(this).val();
        var crossborderCourierSelected = valF.match(EE_INPOST_MODULE_SETTINGS.crossborderCourier);

        if (crossborderCourierSelected)
        {
            $('#phone_no_parcel_cross').show();
        }
    })
})


