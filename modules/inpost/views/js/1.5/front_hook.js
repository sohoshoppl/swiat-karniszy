/**
 *
 * NOTICE OF LICENSE
 *
 */

$(function () {
    if (return_url) {
        $('.history_detail a').click(function () {
            $(window).scroll(function () {
                if ($('#returnButton').length == 0) {
                    var returnElement = '<a id="returnButton" style="margin-left:10px;position: relative;top: -8px;float:right;"href="'+return_url+'" class="button exclusive"><span>'+return_text+'<i class="icon-truck right"></i></span></a>';
                    $(returnElement).insertBefore($("#block-order-detail input[name='submitReorder']"));
                }
            });

        })
    }

})