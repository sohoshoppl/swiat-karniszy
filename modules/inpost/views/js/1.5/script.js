/**
 *
 * NOTICE OF LICENSE
 *
 */
$(function () {

    $('#fieldset_4').insertAfter('#fieldset_3');
    $('<br />').insertAfter('#fieldset_3');

    $("input[name^=INPOST_PARCEL_LOCKERS_COD_PAYMENT]").closest('.margin-form').addClass('parcel_locker_cod');
    $("input[name^=INPOST_PARCEL_LOCKERS_PAYMENT]").closest('.margin-form').addClass('parcel_locker');
    $("input[name^=INPOST_X_BORDER_PAYMENT_]").closest('.margin-form').addClass('x_border');
    $('input[name^=INPOST_PARCERLS_LOCKERS_RETURNS_URL]').closest('.margin-form').addClass('parcel_returns');
    $('input[name^=INPOST_PARCERLS_LOCKERS_RETURNS_DESCRIPTION]').closest('.margin-form').addClass('parcel_returns');
    $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.margin-form').addClass('parcel_locker_cod')
    $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.margin-form').siblings('label').first().addClass('parcel_locker_cod');
    $('.parcel_locker').insertAfter($('label[for=INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS]'))
    $('.x_border').insertAfter($('label[for=INPOST_AVAILABLE_SERVICES_CROSSBORDER]'))
    $('.parcel_returns').insertAfter($('label[for=INPOST_AVAILABLE_SERVICES_RETURNS]'))
    $('input[name=INPOST_DEFAULT_PARCEL_SIZE]').closest('.margin-form').addClass('default_parcel_size');

    $('.default_parcel_size label').css('float', 'none');
    aaa = $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.margin-form').siblings('label')[1]
    $(aaa).insertAfter($('#INPOST_PARCERLS_LOCKERS_RETURNS_URL'));
    aaab = $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.margin-form').siblings('label')[1]
    $(aaab).insertAfter($('#INPOST_PARCERLS_LOCKERS_RETURNS_DESCRIPTION'));

    if (!$('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD').is(':checked')) {
        $('.parcel_locker_cod').hide();
    }

    if (!$('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS').is(':checked')) {
        $('.parcel_locker').hide();
    }

    if (!$('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').is(':checked')) {
        $('.x_border').hide();
    }

    if (!$('#INPOST_AVAILABLE_SERVICES_RETURNS').is(':checked')) {
        $('.parcel_returns').hide();
    }

    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD').change(function () {
        if ($(this).is(':checked')) {
            $('.parcel_locker_cod').show();
            if ($('#INPOST_PARCERLS_LOCKERS_COD_IBAN').val() == '') {
                $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.form-group').addClass('has-error')
            } } else {
            $('.parcel_locker_cod').hide();
            }
    })


    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS').change(function () {
        if ($(this).is(':checked')) {
            $('.parcel_locker').show();
        } else {
            $('.parcel_locker').hide();
        }
    })
    $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').change(function () {
        if ($(this).is(':checked')) {
            $('.x_border').show();
        } else {
            $('.x_border').hide();
        }
    })

    $('#INPOST_AVAILABLE_SERVICES_RETURNS').change(function () {
        if ($(this).is(':checked')) {
            $('.parcel_returns').show();
        } else {
            $('.parcel_returns').hide();
        }
    })
    $('#INPOST_COUNTRY').change(function () {
        $('#EE_INPOST_API_URL').closest('.form-group').show();
        $('.inpost_desc').hide();
        val = $(this).val();
        var varF = '';
        switch (val) {
            case '1':
                varF = $('#INPOST_API_URL_POLAND').val();
                $('.inpost_desc.pl').show();
                break;
            case '2':
                varF = $('#INPOST_API_URL_FRANCE').val();
                $('.inpost_desc.fr').show();
                break;
            case '3':
                varF = $('#INPOST_API_URL_ITALY').val();
                $('.inpost_desc.it').show();
                break;
            default:
                $('.inpost_desc.without').show();
                break;

        }

        $('#EE_INPOST_API_URL').val(varF)
    });


    $('#add_new_price_rule').click(function () {
        $('.alertParcelLockers').hide();
        var priceFrom = $('#PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE').val();
        $('#PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE').val('');
        $('#PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE').val('');
        $('#PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE').val('');
        if (!priceFrom && !priceTo && !cost) {
            $('.alertParcelLockers').show();
            return;
        }

        $('.table_prices_rules tbody').append(
            '<tr class="price">' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+from_text+'</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_PRICE_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+to_text+'</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_PRICE_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">'+cost_text+'</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_PRICE_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })


    $('#add_new_weight_rule').click(function () {
        $('.alertParcelLockersWeight').hide();
        var priceFrom = $('#PARCELS_LOCKERS_WEIGHT_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_WEIGHT_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_WEIGHT_RULES_COST_DEFINE').val();
        if (!priceFrom && !priceTo && !cost) {
            $('.alertParcelLockersWeight').show();
            return;
        }
        $('.table_weight_rules tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+from_text+'</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_WEIGHT_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+to_text+'</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_WEIGHT_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">'+cost_text+'</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_WEIGHT_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })


    $('#add_new_price_rule_cod').click(function () {
        $(".alertParcelLockersCod").hide();
        var priceFrom = $('#PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE').val();
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE').val('');
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE').val('');
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE').val('');
        if (!priceFrom && !priceTo && !cost) {
            $(".alertParcelLockersCod").show();
            return;
        }
        $('.table_prices_rules_cod tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">od</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">do</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">kosztuje</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })

    $('#add_new_weight_rule_cod').click(function () {
        $(".alertParcelLockersCodWeight").hide();
        var priceFrom = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_DEFINE').val();
        var percentDefine = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT_DEFINE').val();
        if (!priceFrom && !priceTo && !cost) {
            $(".alertParcelLockersCodWeight").show();
            return;
        }
        $('.table_weight_rules_cod tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">waga od</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">waga do</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">kosztuje</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            /*'<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-10">%wartości koszyka</label>' +
            '<div class="col-lg-2">' +
            '<input type="text" class="form-control" value="'+percentDefine+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT[]">' +
            '</div>' +
            '</div>' +
            '</td>' +*/
            '<td>' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })



    assignDelete();
    /**
     * Delete rules
     */
    function assignDelete()
    {
        $('button.delete').click(function (e) {
            e.preventDefault();
            var id =$(this).closest('tr').remove();
        })
    }

    if (!$('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').is(':checked')) {
        $('.xborder').hide();
    }
    $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').change(function () {
        if ($(this).is(':checked')) {
            $('.xborder').show();
            $('#crossBorderModal').show();

            $(document).click(function (event) {
                if ($("#crossBorderModal .modal-content").find($(event.target)).length == 0 || $(event.target).attr('class') == 'close') {
                    $('#crossBorderModal').hide(); }

            });

        } else {
            $('.xborder').hide();
        }
    })

    $('select[name=method_giving]').change(function () {
        if ($(this).val() == 'parcel_locker') {
            $('#parcel_method').show();
            $('#dispatch_method').hide();
            $('select[name=dispatch_points_sender]').attr('disabled', true);
            $('input[name="sender[source_machine_id]"]').attr('disabled', false);
        }
        if ($(this).val() == 'dispatch_point') {
            $('#dispatch_method').show();
            $('#parcel_method').hide();
            $('input[name="sender[source_machine_id]"]').attr('disabled', true);
            $('select[name=dispatch_points_sender]').attr('disabled', false);
        }
    })

    $('#parcels_lockers .selection').on('change', function () {
        $('#parcels_lockers .selection').not(this).prop('checked', false);
    });

    $('#parcels_lockers_cod .selection').on('change', function () {
        $('#parcels_lockers_cod .selection').not(this).prop('checked', false);
    });
})

function sendBulkAction(form, action)
{
    String.prototype.splice = function (index, remove, string) {
        return (this.slice(0, index) + string + this.slice(index + Math.abs(remove)));
    };

    var form_action = $(form).attr('action');

    if (form_action.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ') == '') {
        return false; }

    if (form_action.indexOf('#') == -1) {
        $(form).attr('action', form_action + '&' + action); } else {
        $(form).attr('action', form_action.splice(form_action.lastIndexOf('&'), 0, '&' + action)); }

    $(form).submit();
}
