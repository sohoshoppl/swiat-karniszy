/**
 *
 * NOTICE OF LICENSE
 *
 */
$(function () {
    if (return_url) {
        $('.history_detail a').click(function () {
            $(window).scroll(function () {
                if ($('#returnButton').length == 0) {
                    $('#submitReorder').append('<a id="returnButton" style="float: right; margin-top:10px;clear:both;"href="'+return_url+'" class="exclusive button btn btn-default button-medium pull-right"><span>'+return_text+'<i class="icon-truck right"></i></span></a>')
                }
            });

        })
    }

})