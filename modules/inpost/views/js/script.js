/**
 *
 * NOTICE OF LICENSE
 *
 */
$(function () {

    $('#parcels_lockers .selection').on('change', function () {
        $('#parcels_lockers .selection').not(this).prop('checked', false);
    });

    $('#parcels_lockers_cod .selection').on('change', function () {
        $('#parcels_lockers_cod .selection').not(this).prop('checked', false);
    });
    $('#configuration_form.defaultForm.form-horizontal').on('submit',function (e) {
        if ($('#parcels_lockers_cod .selection:checked').length > 1) {
            alert('You can only check one type of Parcels Lockers COD price shipping');
            e.preventDefault();
        }
        if ($('#parcels_lockers .selection:checked').length > 1) {
            alert('You can only check one type of Parcels Lockers price shipping');
            e.preventDefault();
        }
    })

    $('.inpostCouriers td').each(function () {
        $(this).attr('onclick', '')
    })

    $('.parcel_locker_payment').closest('.form-group').addClass('parcel_locker').insertAfter($('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS').closest('.checkbox'))
    $('.parcel_locker_payment').closest('.col-lg-9').removeClass('col-lg-offset-3');
    $('.parcel_locker_cod_payment').closest('.form-group').addClass('parcel_locker_cod');
    $('.xborder_payment').closest('.form-group').addClass('xborder').insertAfter($('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').closest('.checkbox'));
    $('.xborder_payment').closest('.col-lg-9').removeClass('col-lg-offset-3');
    $('.inpost_return_description').closest('.form-group').addClass('inpost_return_description_form').insertAfter($('#INPOST_AVAILABLE_SERVICES_RETURNS').closest('.checkbox'))
    $('.inpost_return_description').closest('.col-lg-9').removeClass('col-lg-offset-3');
    $('.inpost_return_url').closest('.form-group').addClass('inpost_return_url_form').insertAfter($('#INPOST_AVAILABLE_SERVICES_RETURNS').closest('.checkbox'))
    $('.inpost_return_url').closest('.col-lg-9').removeClass('col-lg-offset-3');
    if ($('#EE_INPOST_API_URL').val() === '') {
        $('#EE_INPOST_API_URL').closest('.form-group').hide();
    }

    $('#INPOST_AVAILABLE_SERVICES_RETURNS').change(function () {
        if ($(this).is(':checked')) {
            $('.inpost_return_url_form').show();
            $('.inpost_return_description_form').show();
        } else {
            $('.inpost_return_url_form').hide();
            $('.inpost_return_description_form').hide();
        }
    })
    if (!$('#INPOST_AVAILABLE_SERVICES_RETURNS').is(':checked')) {
        $('.inpost_return_url_form').hide();
        $('.inpost_return_description_form').hide();
    }
    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD').change(function () {
        if ($(this).is(':checked')) {
            $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.form-group').show();
            $('.parcel_locker_cod').show();
            if ($('#INPOST_PARCERLS_LOCKERS_COD_IBAN').val() == '') {
                $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.form-group').addClass('has-error')
            } } else {
            $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.form-group').hide();
            $('.parcel_locker_cod').hide();
            }
    })
    $("<h4>"+assign_payment_methods_text+"</h4>").insertBefore(".parcel_locker .col-lg-9");
    $("<h4>"+assign_payment_methods_text+"</h4>").insertBefore(".xborder .col-lg-9");
    $("<h4 class='col-lg-9 col-lg-offset-3'>"+assign_payment_methods_text+"</h4>").insertBefore(".parcel_locker_cod .col-lg-9");
    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS').change(function () {
        if ($(this).is(':checked')) {
            $('.parcel_locker').show();

        } else {
            $('.parcel_locker').hide();
        }
    })
    if (!$('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').is(':checked')) {
        $('.xborder').hide();
    }
    $('#INPOST_AVAILABLE_SERVICES_CROSSBORDER').change(function () {
        if ($(this).is(':checked')) {
            $('.xborder').show();
            $('#crossBorderModal').modal('show');
        } else {
            $('.xborder').hide();
        }
    })

    $('table.inpostCouriers tbody tr td:last-child div a').each(function () {
        $(this).text('Download manifest')
    })

    $('#INPOST_COUNTRY').change(function () {
        $('#EE_INPOST_API_URL').closest('.form-group').show();

        $('.inpost_desc').hide();
        val = $(this).val();
        var varF = '';
        switch (val) {
            case '1':
                varF = $('#INPOST_API_URL_POLAND').val();
                $('.inpost_desc.pl').show();
                break;
            case '2':
                varF = $('#INPOST_API_URL_FRANCE').val();
                $('.inpost_desc.fr').show();
                break;
            case '3':
                varF = $('#INPOST_API_URL_ITALY').val();
                $('.inpost_desc.it').show();
                break;
            default:
                $('.inpost_desc.without').show();
                break;

        }

        $('#EE_INPOST_API_URL').val(varF)
    });

    if (!$('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD').is(':checked')) {
        $('#INPOST_PARCERLS_LOCKERS_COD_IBAN').closest('.form-group').hide();
        $('.parcel_locker_cod').hide();
    }
    if (!$('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS').is(':checked')) {
        $('.parcel_locker').hide();
    }



    $('#price_rules').insertAfter('#fieldset_1_1');

    $('#add_new_price_rule').click(function () {
        $('.alertParcelLockers').hide();
        var priceFrom = $('#PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE').val();
        $('#PARCELS_LOCKERS_PRICE_RULES_FROM_DEFINE').val('');
        $('#PARCELS_LOCKERS_PRICE_RULES_TO_DEFINE').val('');
        $('#PARCELS_LOCKERS_PRICE_RULES_COST_DEFINE').val('');
        if (!priceFrom && !priceTo && !cost) {
            $('.alertParcelLockers').show();
            return;
        }

        $('.table_prices_rules tbody').append(
            '<tr class="price">' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+from_text+'</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_PRICE_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+to_text+'</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_PRICE_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">'+cost_text+'</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_PRICE_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })

    $('#add_new_weight_rule').click(function () {
        $('.alertParcelLockersWeight').hide();
        var priceFrom = $('#PARCELS_LOCKERS_WEIGHT_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_WEIGHT_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_WEIGHT_RULES_COST_DEFINE').val();
        if (!priceFrom && !priceTo && !cost) {
            $('.alertParcelLockersWeight').show();
            return;
        }
        $('.table_weight_rules tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+from_text+'</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_WEIGHT_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">'+to_text+'</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_WEIGHT_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">'+cost_text+'</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_WEIGHT_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })


    $('#add_new_price_rule_cod').click(function () {
        $(".alertParcelLockersCod").hide();
        var priceFrom = $('#PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE').val();
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_FROM_DEFINE').val('');
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_TO_DEFINE').val('');
        $('#PARCELS_LOCKERS_COD_PRICE_RULES_COST_DEFINE').val('');
        if (!priceFrom && !priceTo && !cost) {
            $(".alertParcelLockersCod").show();
            return;
        }
        $('.table_prices_rules_cod tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">od</label>' +
            '<div class="col-lg-8"> ' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_FROM[]">' +
            '</div>' +
            '</div>                                ' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-4">do</label>                                    ' +
            '<div class="col-lg-8">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-5">kosztuje</label>' +
            '<div class="col-lg-7">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_COD_PRICE_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td class="last">' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })

    $('#add_new_weight_rule_cod').click(function () {
        $(".alertParcelLockersCodWeight").hide();
        var priceFrom = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM_DEFINE').val();
        var priceTo = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_DEFINE').val();
        var cost = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_DEFINE').val();
        var percentDefine = $('#PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT_DEFINE').val();
        if (!priceFrom && !priceTo && !cost) {
            $(".alertParcelLockersCodWeight").show();
            return;
        }
        $('.table_weight_rules_cod tbody').append(
            '<tr>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">waga od</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+priceFrom+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_FROM[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">waga do</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+priceTo+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_TO_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="form-group">' +
            '<label class="control-label col-lg-6">kosztuje</label>' +
            '<div class="col-lg-6">' +
            '<input type="text" class="form-control" value="'+cost+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_COST_[]">' +
            '</div>' +
            '</div>' +
            '</td>' +
            //'<td>' +
            //'<div class="form-group">' +
            //'<label class="control-label col-lg-10">%wartości koszyka</label>' +
            //'<div class="col-lg-2">' +
            //'<input type="text" class="form-control" value="'+percentDefine+'" name="PARCELS_LOCKERS_COD_WEIGHT_RULES_PERCENT[]">' +
            //'</div>' +
            //'</div>' +
            //'</td>' +
            '<td>' +
            '<button type="button" class="delete">x</button>' +
            '</td>' +
            '</tr>'
        );
        assignDelete();
    })



    assignDelete();
    /**
     * Delete rules
     */
    function assignDelete()
    {
        $('button.delete').click(function (e) {
            e.preventDefault();
            var id =$(this).closest('tr').remove();
        })
    }


    /**
     * Show tab
     */
    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_PRICE').change(function () {
        if ($(this).is(':checked')) {
            $('#parcels_lockers').show();

        } else {
            $('#parcels_lockers').hide();
        }
    })
    $('#INPOST_AVAILABLE_SERVICES_PARCELS_LOCKERS_COD_PRICE').change(function () {
        if ($(this).is(':checked')) {
            $('#parcels_lockers_cod').show();

        } else {
            $('#parcels_lockers_cod').hide();
        }
    })
    /*--------------layout modification---------*/
    /*$('.table-responsive-row').each(function(){
       $(this).removeClass('table-responsive-row');
       $(this).addClass('table-responsive');
    });*/


})
