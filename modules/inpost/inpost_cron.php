<?php
/**
 *
 * NOTICE OF LICENSE
 *
 */
error_reporting(0);
ini_set('display_errors', 'off');
include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/controllers/InpostModel.php');
include_once(dirname(__FILE__) . '/controllers/InpostConnector.php');
include_once(dirname(__FILE__) . '/controllers/CrossBorderConnector.php');
include_once(dirname(__FILE__) . '/controllers/InpostPackages.php');

if (isset($_GET['secure_key'])) {
    $secureKey = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
    if (!empty($secureKey) && $secureKey === $_GET['secure_key']) {
        $api = new InpostConnector();
        $x_api = new CrossBorderConnector();
        //Synchronize X Border routes
        $fieldsX = $x_api->getUserRoutesServices();
        InpostModel::importAvailableCrossBorderTypes($fieldsX, true);

        //synchronize package statuses
        $packages = InpostModel::getAllPackagesForCron();

        if ($packages) {
            foreach ($packages as $package) {
                $parcel = $api->getParcel($package['parcel_no']);

                if (property_exists($parcel, 'status_code') || !property_exists($parcel, 'status')) {
                    if ($parcel->status_code == 404) {
                        $parcel = $x_api->getShipment($package['parcel_no']);

                        if ($parcel) {
                            InpostPackages::changeParcelStatus($package['parcel_no'], $parcel->status->code, $parcel->tracking_number);
                            if (strtoupper($parcel->status->code) == 'DELIVERED') {
                                  InpostModel::changeStatusAfterDelivery($package['order_id']);
                            }
                            continue;
                        }
                    }
                }

                if ($parcel && !property_exists($parcel, 'status_code') && $parcel->status !== $package['status']) {
                    if (strtoupper($parcel->status) == 'DELIVERED') {
                        InpostModel::changeStatusAfterDelivery($package['order_id']);
                    }
                    InpostPackages::changeParcelStatus($package['parcel_no'], $parcel->status);
                }

            }
        }
    }
}

return true;
