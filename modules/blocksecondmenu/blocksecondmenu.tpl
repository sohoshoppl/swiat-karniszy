{if $MENU != ''}
<!-- \modules\blocksecondmenu\blocksecondmenu.tpl -->
<!-- Menu -->
<div id="block_second_menu">
    <div  class="row">
        <div class="col-xs-12 col-lg-7">
            <div class="sf-contener-second clearfix">
                <div class="cat-title">
                    {l s="Menu" mod="blocksecondmenu"}
                </div>                
                <ul class="sf-menu-second clearfix menu-content">
                    {$MENU}
                    {if $MENU_SEARCH}
                    <li class="sf-search noBack" style="float:right">
                        <form id="searchbox" action="{$link->getPageLink('search')|escape:'html'}" method="get">
                            <p>
                                <input type="hidden" name="controller" value="search" />
                                <input type="hidden" value="position" name="orderby"/>
                                <input type="hidden" value="desc" name="orderway"/>
                                <input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
                            </p>
                        </form>
                    </li>
                    {/if}
                </ul>
            </div>
        </div>            
            <div id="block_top_menu_contact" class="hidden-xs hidden-sm hidden-md col-lg-5">
                {l s="masz pytania?" mod="blocktopmenu"}
                <span>
                    <i class="icon-phone">
                    </i>
                    <a href="tel:{$shop_phone}">{$shop_phone}</a>
                </span>
                <span>
                    <i class="icon-envelope">
                    </i>
                    <a href="mailto:{$shop_email}">{$shop_email}</a>
                </span>
            </div>

    </div>
</div>
<!--/ Menu -->
{/if}

<script src="{$content_dir}themes/swiat-karniszy/js/hidden.js" type="text/javascript"></script>
<div id="hidden">
</div>