<?php

class sohoshopaddressstickerspdfController extends ModuleAdminController {

    protected $module_name = 'sohoshopaddressstickers';
    public $bootstrap = true;
    public $custom_smarty;

    public function __construct() {
        $this->context = Context::getContext();
        $this->custom_smarty = new Smarty();
        $this->custom_smarty->setTemplateDir(_PS_MODULE_DIR_ . $this->module_name . '/views/templates/admin/');

        parent::__construct();
    }

    public function renderList() {
        $id_order = (int) Tools::getValue('id_order');
        if ($id_order) {
            $order = new Order($id_order);


            $delivery_address = new Address((int) $order->id_address_delivery);
            $tpl = $this->custom_smarty->createTemplate('stickers.tpl');
            $tpl->assign(
                    array(
                        'delivery_address' => $delivery_address,
            ));
            $content = $tpl->fetch();
            $pdf_renderer = new PDFGenerator( (bool)Configuration::get('PS_PDF_USE_CACHE'), 'P');
            $pdf_renderer->setFontForLang(Context::getContext()->language->iso_code);
            $pdf_renderer->createHeader($content);
            $pdf_renderer->writePage();
            $pdf_renderer->render('test.pdf', 'I');
            /*
              I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the “Save as” option on the link generating the PDF.
              D: send to the browser and force a file download with the name given by name.
              F: save to a local server file with the name given by name.
              S: return the document as a string. name is ignored.
              FI: equivalent to F + I option
              FD: equivalent to F + D option

             */
        } else {
            //return $this->context->link->getAdminLink('AdminOrders');
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminOrders'));
        }
    }

}

?>