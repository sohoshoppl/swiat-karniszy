<?php

class sohoshopaddressstickers extends Module {

    function __construct() {
        $this->name = 'sohoshopaddressstickers';
        $this->author = 'SohoShop.pl';
        $this->version = '0.1';
        $this->module_key = '';
        $this->dir = '/modules/sohoshopaddressstickers/';
        parent::__construct();
        $this->displayName = $this->l('Naklejki adresowe');
        $this->description = $this->l('Moduł generuje pdf z naklejkami adresowymi');
        $this->tab = 'shipping_logistics';
        $this->tabClassName = 'sohoshopaddressstickerspdf';
    }

    function install() {
        if (!parent::install() || !$this->registerHook('displayAdminOrderContentOrder') )
            return false;
     
        if (!$id_tab) {
            $tab = new Tab();
            $tab->class_name = $this->tabClassName;
            $tab->id_parent = -1;
            $tab->module = $this->name;
            $languages = Language::getLanguages();
            foreach ($languages as $language)
                $tab->name[$language['id_lang']] = $this->displayName;
            $tab->add();
        }
        return true;
    }

    public function uninstall() {
        if (parent::uninstall() == false) {
            return false;
        }
        $id_tab = Tab::getIdFromClassName($this->tabClassName);
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        return true;
    }
    
    public function hookDisplayAdminOrderContentOrder($params) {

        $html = '<a class="btn btn-default" target="_blank" href="'.$this->context->link->getAdminLink('sohoshopaddressstickerspdf'). '&id_order=' . $params['order']->id.'" style="position: absolute; top: -117px; right: 10px; ">'
                . ' <i class="icon-print"></i> '.$this->l('Etykiety adresowe').''
                . '</a>';
        return $html;
    }    


}

?>