$(document).on("click", "#addPackageToWaitRoom", function (e) {
    e.preventDefault();
    if (confirm(confirmTXT)) {
        var query = 'id_order='+id_order+'&action=addPackageToWaitRoom&'+$('#sohoshopgls-add-package form').serialize();
        console.log(query);
        $.ajax({
            type: 'POST',
            url: sohoshopgls_ajax_request,
            cache: false,
            dataType: 'json',
            data: query,
            success: function (data) {
                console.log(data);
                if (data.result)
                {
                    $('#sohoshopgls-add-package').append('<br><div class="alert alert-success">'+okTXT+'<a href="'+przygotowalniaLink+'" target="_blank">"Przygotowalni"</a></div>');
                } else{
                    var err = '';
                    for (i = 0; i < data.length; i++) { 
                        if(data[i] !== 'undefined' && data[i] !== null){
                            err += data[i] + "<br>";
                        }
                    }
                    $('#sohoshopgls-add-package').append('<br><div class="alert alert-danger">'+err+'</div>');
                }
           
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                
                //jAlert("Impossible to add the product to the cart.\n\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
            }
        });
    }
})