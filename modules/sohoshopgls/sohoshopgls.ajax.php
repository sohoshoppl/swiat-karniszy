<?php
/**
 * 2016 Eengine Sp. z o.o.
 *
 * NOTICE OF LICENSE
 *
 */
include_once(dirname(__FILE__).'/../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../init.php');
include_once(dirname(__FILE__).'/sohoshopgls.php');
include_once(dirname(__FILE__).'/SohoShopGLSModel.php');

$sohoshopgls = new sohoshopgls;
$response = '';

switch (Tools::getValue('action')) {
    case 'addPackageToWaitRoom':
        $order = new Order(Tools::getValue('id_order'));
        if((int)Tools::getValue('COD') == 1){
            $sohoshopgls->setCOD(Tools::getValue('cod_amount')); 
        }
        
        //to do walidacja
        $sohoshopgls->setResiverAddress(Tools::getValue('rphone'), Tools::getValue('rcontact'), Tools::getValue('rstreet'), Tools::getValue('rzipcode'), Tools::getValue('rcity'), Tools::getValue('rname1'), Tools::getValue('rname2'), 'PL', Tools::getValue('rname3'));
        $sohoshopgls->setSenderAddress(Tools::getValue('street'), Tools::getValue('zipcode'), Tools::getValue('city'), Tools::getValue('name1'), Tools::getValue('name2'), 'PL', Tools::getValue('name3'));
        $sohoshopgls->setPackageDetails($order->reference,Tools::getValue('notes'));
        $sohoshopgls->addParcel(Tools::getValue('weight'),$order->reference);
        $sohoshopgls->startSession();
        $SohoShopGLSModel_id = (int)$sohoshopgls->addPackageToWaitRoom();
        if( $SohoShopGLSModel_id != 0 ){
            $SohoShopGLSModel = new SohoShopGLSModel($SohoShopGLSModel_id);
            $SohoShopGLSModel->id_order = $order->id;
            $SohoShopGLSModel->update();
        }
        $sohoshopgls->getLabels();
        $sohoshopgls->endSession();
        if(is_array($sohoshopgls->error)){
            $response = $sohoshopgls->error;    
        }else{
            $response = array('result' => 'OK');            
        }
    break;    
}

die(Tools::jsonEncode($response));
/*


$inpost = Module::getInstanceByName('inpost');
$response = '';
$conn = new InpostConnector();
$x_border_conn = new CrossBorderConnector();
switch (Tools::getValue('action')) {
    case 'get_address':
        $response = $inpost->getUserAddress(Tools::getValue('address_id'));
        break;
    case 'get_dispatch_point':
        $response = InpostDispatchPoints::getDispatchPointById(Tools::getValue('dispatch_id'));
        $response['phone'] = $response['phone_no'];

        break;
    case 'update_parcel':
        $response = $conn->updateParcel();
        break;
    case 'create_label':
        $result = $conn->createAndDownloadManifest();
        if (is_array($result)) {
            $response = $result;
        } else {

            $inpost->changeStatuses($result, Tools::getValue('id_order'));
            InpostPackages::changeParcelStatus($result->id, $result->status);

            $response = ['url' =>
                            "/modules/inpost/inpost.ajax.php?action=download_manifest&id_manifest={$result->id}&token="
                            .Tools::getValue('token')];
        }
        break;
    case 'create_parcel':
        if ((int)Tools::getValue('x_border_order') === 1) {
            $response = $x_border_conn->createParcel();
        } else {
            $response = $conn->createParcel(Tools::getValue('id_order'));
        }
        break;
    case 'create_parcel_and_label':
        $result = $conn->createParcelAndPrintManifest();

        if (is_array($result)) {
            $response = $result;
        } else {
            $inpost->changeStatuses($result, Tools::getValue('id_order'));
            InpostPackages::changeParcelStatus($result->id, $result->status);
            $response = ['url' =>
                "/modules/inpost/inpost.ajax.php?action=download_manifest&id_manifest={$result->id}&token="
                .Tools::getValue('token')];
        }
        break;
    case 'download_manifest':
        $inpost->downloadSticker(Tools::getValue('id_manifest'));
        break;

    case 'insurance_check':
        break;
    case 'check_order_courier':
        $packages = Tools::getValue('inpostLabelsBox');

        $response = InpostModel::checkNeedSendFromMachine($packages);

        break;
    case 'x_border_data':
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            Configuration::updateGlobalValue('INPOST_X_BORDER_LOGIN', Tools::getValue('x_login'));
            Configuration::updateGlobalValue('INPOST_X_BORDER_PASSWORD', Tools::getValue('x_password'));
        } else {
            Configuration::updateValue('INPOST_X_BORDER_LOGIN', Tools::getValue('x_login'));
            Configuration::updateValue('INPOST_X_BORDER_PASSWORD', Tools::getValue('x_password'));
        }

        $x_border_conn = new CrossBorderConnector();
        $token = $x_border_conn->getToken();
        if (property_exists($token, 'access_token')) {
            $response = true;
        } else {
            $response = [
                'error' => 1,
                0 => 'Error'
            ];
        }

        break;
    case 'download_label_x':
        break;
    case 'confirm_route':
        $response = Configuration::deleteByName('X_BORDER_NEW_ROUTE');
        break;
    case 'save_parcel_locker':
        $phoneNo = Tools::getValue('phone_no');
        $parcelLocker = Tools::getValue('parcel_locker');
        $cartId = Context::getContext()->cart->id;

        $fields = array(
            'parcel_locker' => $parcelLocker,
            'cart_id' => $cartId,
            'phone_no' => $phoneNo,
            'x_border' => ''
        );

        $response = InpostModel::connectParcelLockerWithCart($fields);

        $validate = (new Inpost())->validateOnePageCheckout();
        if($validate !== true && isset($validate['error'])) {
            $response = array('error' => $validate['error']);
        }

//        if ((int)Configuration::get('PS_ORDER_PROCESS_TYPE') === 1) {
//            $parcelLocker = InpostModel::getSelectedParcelForCart($cartId);
//            if (!($parcelLocker)) {
//                $response = array('error' => '<p class="warning parcel-locker-not-chosen">Wybierz paczkomat</p>');
//            } else {
//                $phonePattern = '/[^0-9\+]/';
//                $phoneNo = preg_replace($phonePattern, '', $phoneNo);
//
//                if(!$phoneNo) {
//                    $response = array('error' => '<p class="warning parcel-locker-not-chosen">Podaj numer telefonu</p>');
//                } else {
//                    $inpostCountry = Configuration::get('INPOST_COUNTRY');
//
//                    if ($inpostCountry == 1) {
//                        $preg_phone = preg_match(Inpost::POLAND_PHONE, $phoneNo, $matches);
//                    } elseif ($inpostCountry == 2) {
//                        $preg_phone = preg_match(Inpost::FRANCE_PHONE, $phoneNo, $matches);
//                    } elseif ($inpostCountry == 3) {
//                        $preg_phone = preg_match(Inpost::ITALY_PHONE, $phoneNo, $matches);
//                    }
//
//                    if (!$preg_phone) {
//                        $response = array('error' => '<p class="warning parcel-locker-not-chosen">Podany numer telefonu jest nieprawidłowy</p>');
//                    }
//                }
//            }
//        }

        break;

    case 'save_phone_no':
        $context = Context::getContext();
		$cart = $context -> cart;
		$carrierId = $cart->id_carrier;

        $cartId = $cart->id;
        $phoneNo = Tools::getValue('phone_no');

        $fields = array(
            'cart_id' => $cartId,
            'phone_no' => $phoneNo
        );

        $response = InpostModel::addPhoneNoToParcelLockerOrder($fields);

        $validate = (new Inpost())->validateOnePageCheckout();
        if($validate !== true && isset($validate['error'])) {
            $response = array('error' => $validate['error']);
        }

//		if ((int)Configuration::get('PS_ORDER_PROCESS_TYPE') === 1 && Inpost::isCarrierAParcelLocker($carrierId)) {
//			$parcelLocker = InpostModel::getSelectedParcelForCart($cartId);
//			if (!($parcelLocker)) {
//                $response = array('error' => '<p class="warning parcel-locker-not-chosen">Wybierz paczkomat</p>');
//			} else {
//                $phonePattern = '/[^0-9\+]/';
//                $phoneNo = preg_replace($phonePattern, '', $phoneNo);
//
//                if(!$phoneNo) {
//                    $response = array('error' => '<p class="warning parcel-locker-not-chosen">Podaj numer telefonu</p>');
//                } else {
//                    $inpostCountry = Configuration::get('INPOST_COUNTRY');
//
//                    if ($inpostCountry == 1) {
//                        $preg_phone = preg_match(Inpost::POLAND_PHONE, $phoneNo, $matches);
//                    } elseif ($inpostCountry == 2) {
//                        $preg_phone = preg_match(Inpost::FRANCE_PHONE, $phoneNo, $matches);
//                    } elseif ($inpostCountry == 3) {
//                        $preg_phone = preg_match(Inpost::ITALY_PHONE, $phoneNo, $matches);
//                    }
//
//                    if (!$preg_phone) {
//                        $response = array('error' => '<p class="warning parcel-locker-not-chosen">Podany numer telefonu jest nieprawidłowy</p>');
//                    }
//                }
//            }
//		}

        break;
}
if ($response) {
    if (Tools::getValue('callback')) {
        die(Tools::getValue('callback') . "(" . Tools::jsonEncode($response) . ")");
    } else {
        die(Tools::jsonEncode($response));
    }
}
*/