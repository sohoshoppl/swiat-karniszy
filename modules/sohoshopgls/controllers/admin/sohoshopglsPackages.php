<?php

/**
 * SohoshopGLS module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://sohoshop.pl
 *
 * http://sohoshop.pl
 */
class sohoshopglsPackagesController extends ModuleAdminController {

    public function __construct() {
        $this->table = 'sohoshopgls_shipment';
        $this->className = 'SohoShopGLSModel';
        $this->module = 'sohoshopgls';
        $this->lang = false;
        $this->bootstrap = true;
        $this->explicitSelect = true;
        $this->context = Context::getContext();
        $this->_defaultOrderBy = 'id_sohoshopgls_shipment';
        $this->_defaultorderWay = 'DESC';
        $this->addRowAction('pdf_label');



        $this->tpl_list_vars['icon'] = 'icon-folder-close';
        $this->tpl_list_vars['title'] = $this->l('Paczki w "przygotowalni"');

        $this->fields_list = array(
            'id_sohoshopgls_shipment' => array(
                'title' => $this->l('ID'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true,
            ),
            'shipment_number' => array(
                'title' => $this->l('Numer przesyłki'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'reciver_name' => array(
                'title' => $this->l('Adresat'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'postcode' => array(
                'title' => $this->l('Kod pocztowy'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'city' => array(
                'title' => $this->l('Miasto'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true
            ),
            'addres' => array(
                'title' => $this->l('Adres'),
                'type' => 'text',
                'orderby' => true,
                'filter' => true,
                'search' => true
            )
        );

        parent::__construct();
    }

    public function initToolbar() {
        parent::initToolbar();
    }

    public function renderList() {


        return parent::renderList();
    }

    public function renderView() {
        return $this->renderList();
    }

    public function displayPdf_labelLink($token = null, $id, $name = null) {
        $tpl = $this->createTemplate('helpers/list/list_action_supplier_order_receipt.tpl');
        if (!array_key_exists('List przewozowy', self::$cache_lang))
            self::$cache_lang['List przewozowy'] = $this->l('List przewozowy', 'Helper');

        $tpl->assign(array(
            'href' => $this->context->link->getAdminLink('sohoshopglsPackages') . '&getPdfLabel=1' . '&' . $this->identifier . '=' . $id,
            'action' => self::$cache_lang['List przewozowy'],
            'id' => $id
        ));

        return $tpl->fetch();
    }

    public function postProcess() {
        // If id_order is sent, we instanciate a new Order object
        
        if (Tools::isSubmit('id_sohoshopgls_shipment') && Tools::getValue('id_sohoshopgls_shipment') > 0) {
            
            if (Tools::isSubmit('getPdfLabel')) {
                $SohoShopGLSModel = new SohoShopGLSModel(Tools::getValue('id_sohoshopgls_shipment'));
                header("Content-type:application/pdf");

                // It will be called downloaded.pdf
                header("Content-Disposition:attachment;filename='List_przewozowy.pdf'");

                // The PDF source is in original.pdf
                readfile(_PS_BASE_URL_."/modules/sohoshopgls/pdf/".(string)$SohoShopGLSModel->shipment_number.".pdf");                
            }
        }
    }

}
