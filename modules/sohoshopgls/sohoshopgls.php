<?php

/**
 * SohoshopGLS module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
if (!defined('_PS_VERSION_'))
    exit;


include_once(dirname(__FILE__) . '/SohoShopGLSModel.php');

class sohoshopgls extends CarrierModule {

    public $baseUrlAjax;
    public $token;
    public $error;
    public $module_url;
    private $auth = array(
        'user_name' => '',
        'user_password' => ''
    );
    private $pickup_id;
    private $shipment_id;
    private $sopClient;
    private $session;
    private $sendaddr = array(
        'name1' => '',
        'name2' => '',
        'name3' => '',
        'country' => '',
        'zipcode' => '',
        'city' => '',
        'street' => '');
    private $srv_bool = array(
        'cod' => 0,
        'cod_amount' => '',
        'exw' => 0,
        'rod' => 0,
        'pod' => 0,
        'exc' => 0,
        'ident' => 0,
        'daw' => 0,
        'ps' => 0,
        'pr' => 0,
        's10' => 0,
        's12' => 0,
        'sat' => 0,
        'ow' => 0,
        'srs' => 0);
    private $package = array(
        'rname1' => '',
        'rname2' => '',
        'rname3' => '',
        'rcountry' => '',
        'rzipcode' => '',
        'rcity' => '',
        'rstreet' => '',
        'rphone' => '',
        'rcontact' => '',
        'date' => '',
        'references' => '',
        'notes' => '',
        'quantity' => '',
        'weight' => '',
        'date' => '',
        'pfc' => '',
        'sendaddr' => '',
        'srv_bool' => '',
        'srv_ade' => '',
        'srv_daw' => '',
        'srv_ident' => '',
        'srv_ppe' => '',
        'parcels' => array()
    );

    public function __construct() {
        $this->name = 'sohoshopgls';
        $this->tab = 'shipping_logistics';
        $this->version = '0.1';
        $this->author = 'sohoshop.pl';

        $this->bootstrap = true;
        parent::__construct();

        $this->token = Tools::getAdminTokenLite('AdminModules');
        $this->baseUrlAjax = $this->getBaseURL() . 'modules/sohoshopgls/sohoshopgls.ajax.php?token=' . $this->token;
        if (defined('_PS_ADMIN_DIR_')) {
            $this->baseUrlAjax = 'index.php?token=' . $this->token . '&controller=AdminModules&configure=sohoshopgls&module_name=sohoshopgls&adminAjax=true';
        }

        $this->displayName = $this->l('SohoSHOP GLS');
        $this->description = $this->l('Zamawianie kuriera na podstawie zamówień.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_path = _PS_MODULE_DIR_ . $this->name . '/';

        $this->module_url = 'index.php?controller=AdminModules&token=' . Tools::getValue('token') . '&configure=' . $this->name;
    }

    public function install() {
        Configuration::updateValue('SOHOSHOP_GLS_API_URL', 'https://ade-test.gls-poland.com/adeplus/pm1/ade_webapi.php?wsdl');
        Configuration::updateValue('SOHOSHOP_GLS_LOGIN', 'SWISTAK');
        Configuration::updateValue('SOHOSHOP_GLS_PASS', 'sw_api_51');
        $tabNames = array();
        foreach (Language::getLanguages(false) as $lang) {
            $tabNames[$lang['id_lang']] = 'GLS - SohoShop';
        }
        $tab = new Tab();
        $tab->class_name = "sohoshopglsPackages";
        $tab->name = $tabNames;
        $tab->module = $this->name;
        $tab->id_parent = 0;
        $tab->add();
        if (!parent::install() || !$this->registerHook('adminOrder') || !SohoShopGLSModel::createTables())
            return false;

        return true;
    }

    public function uninstall() {
        if (
                !parent::uninstall() || !SohoShopGLSModel::DropTables()
        )
            return false;

        $idtabs = array(
            Tab::getIdFromClassName("sohoshopglsPackages")
        );
        foreach ($idtabs as $tabid) {
            if ($tabid) {
                $tab = new Tab($tabid);
                $tab->delete();
            }
        }

        return true;
    }

    public function getContent() {

        if (Tools::getValue('adminAjax')) {
            require_once 'sohoshopgls.ajax.php';
        }

        if (Tools::isSubmit('settings')) {
            Configuration::updateValue('SOHOSHOP_GLS_NAME1', Tools::getValue('name1'));
            Configuration::updateValue('SOHOSHOP_GLS_NAME2', Tools::getValue('name2'));
            Configuration::updateValue('SOHOSHOP_GLS_NAME3', Tools::getValue('name3'));
            Configuration::updateValue('SOHOSHOP_GLS_ZIPCODE', Tools::getValue('zipcode'));
            Configuration::updateValue('SOHOSHOP_GLS_STREET', Tools::getValue('street'));
            Configuration::updateValue('SOHOSHOP_GLS_CITY', Tools::getValue('city'));
            Configuration::updateValue('SOHOSHOP_GLS_LOGIN', Tools::getValue('sohoshopgls_login'));
            Configuration::updateValue('SOHOSHOP_GLS_PASS', Tools::getValue('sohoshopgls_password'));
            Configuration::updateValue('SOHOSHOP_GLS_API_URL', Tools::getValue('sohoshopgls_webserviceurl'));
        }
        $this->context->controller->addCSS($this->_path . 'views/css/style.css');
        return $this->renderForm();
    }

    private function renderForm() {
        $fields_form[] = array(
            'form' => array(
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Adres URL webserwisu'),
                        'name' => 'sohoshopgls_webserviceurl',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Login'),
                        'name' => 'sohoshopgls_login',
                        'required' => true
                    ),
                    array(
                        'type' => 'password',
                        'label' => $this->l('Hasło'),
                        'name' => 'sohoshopgls_password',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 1'),
                        'name' => 'name1',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 2'),
                        'name' => 'name2'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 3'),
                        'name' => 'name3'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kod pocztowy'),
                        'name' => 'zipcode',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Miasto'),
                        'name' => 'city',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Ulica'),
                        'name' => 'street',
                        'required' => true
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Zapisz ustawienia'),
                    'name' => $this->l('settings')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function getConfigFieldsValues() {
        return array(
            'sohoshopgls_webserviceurl' => Tools::getValue('sohoshopgls_webserviceurl', Configuration::get('SOHOSHOP_GLS_API_URL')),
            'sohoshopgls_login' => Tools::getValue('sohoshopgls_login', Configuration::get('SOHOSHOP_GLS_LOGIN')),
            'sohoshopgls_password' => Tools::getValue('sohoshopgls_password', Configuration::get('SOHOSHOP_GLS_PASS')),
            'name1' => Tools::getValue('name1', Configuration::get('SOHOSHOP_GLS_NAME1')),
            'name2' => Tools::getValue('name2', Configuration::get('SOHOSHOP_GLS_NAME2')),
            'name3' => Tools::getValue('name3', Configuration::get('SOHOSHOP_GLS_NAME3')),
            'zipcode' => Tools::getValue('zipcode', Configuration::get('SOHOSHOP_GLS_ZIPCODE')),
            'street' => Tools::getValue('street', Configuration::get('SOHOSHOP_GLS_STREET')),
            'city' => Tools::getValue('city', Configuration::get('SOHOSHOP_GLS_CITY'))
        );
    }

    public function getConfigFieldsValuesSender() {
        return array(
            'name1' => Tools::getValue('name1', Configuration::get('SOHOSHOP_GLS_NAME1')),
            'name2' => Tools::getValue('name2', Configuration::get('SOHOSHOP_GLS_NAME2')),
            'name3' => Tools::getValue('name3', Configuration::get('SOHOSHOP_GLS_NAME3')),
            'zipcode' => Tools::getValue('zipcode', Configuration::get('SOHOSHOP_GLS_ZIPCODE')),
            'street' => Tools::getValue('street', Configuration::get('SOHOSHOP_GLS_STREET')),
            'city' => Tools::getValue('city', Configuration::get('SOHOSHOP_GLS_CITY'))
        );
    }

    public function getConfigFieldsValuesReciver() {
        $order = new Order(Tools::getValue('id_order'));
        $address = new Address($order->id_address_delivery);
        return array(
            'rname1' => Tools::getValue('rname1', $address->firstname),
            'rname2' => Tools::getValue('rname2', $address->lastname),
            'rname3' => Tools::getValue('rname3', $address->company),
            'rzipcode' => Tools::getValue('rzipcode', $address->postcode),
            'rcity' => Tools::getValue('rcity', $address->city),
            'rstreet' => Tools::getValue('rstreet', $address->address1),
            'rphone' => Tools::getValue('rphone', ($address->phone_mobile != '') ? $address->phone_mobile : $address->phone),
            'rcontact' => Tools::getValue('rcontact', $address->firstname . ' ' . $address->lastname)
        );
    }

    public function getOrderShippingCost($cart, $shipping_cost) {
        return false;
    }

    public function getOrderShippingCostExternal($params) {
        return false;
    }

    public function getSoapClient() {
        $this->sopClient = new SoapClient(Configuration::get('SOHOSHOP_GLS_API_URL'), array('encoding' => 'UTF-8'));
        $this->sopClient->soap_defencoding = 'utf-8';
        $this->sopClient->decode_utf8 = FALSE;
    }

    public function setAuth() {
        $this->auth['user_name'] = Configuration::get('SOHOSHOP_GLS_LOGIN');
        $this->auth['user_password'] = Configuration::get('SOHOSHOP_GLS_PASS');
    }

    /**
     * Rozpoczęcie sesji w GLS api 
     * 
     */
    public function startSession() {
        $this->getSoapClient();
        $this->setAuth();
        try {
            $result = $this->sopClient->__soapCall('adeLogin', $this->auth);
            $this->session = $result->session;
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }
    }

    /**
     * Zakończenie sesji w GLS api 
     * 
     */
    public function endSession() {
        try {
            $aCMsg = array('session' => $this->session);
            $result = $this->sopClient->__soapCall('adeLogout', $aCMsg);
            $this->session = $result->session;
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }
    }

    /**
     * Ustawia dane adresata 
     * @param string $phone Telefon kontaktowy
     * @param string $contact Osoba do kontaktu
     * @param string $street Ulica ,numer budynku i ewentualnie numer lokalu
     * @param string $zip Kod pocztowy
     * @param string $city Miasto
     * @param string $name1 Pierwsza część nazwy adresata np. imię 
     * @param string $name2 Druga część nazwy adresata np. nazwisko 
     * @param string $country Kod kraju adresata np. PL
     * @param string $name3 Trzecia część nazwy adresata np. firma 
     * */
    public function setResiverAddress($phone, $contact, $street, $zip, $city, $name1, $name2 = '', $country = 'PL', $name3 = '') {
        $this->package['rname1'] = $name1;
        $this->package['rname2'] = $name2;
        $this->package['rname3'] = $name3;
        $this->package['rcountry'] = $country;
        $this->package['rzipcode'] = $zip;
        $this->package['rcity'] = $city;
        $this->package['rstreet'] = $street;
        $this->package['rphone'] = $phone;
        $this->package['rcontact'] = $contact;
    }

    /**
     * Ustawia dane adresata 
     * @param string $phone Telefon kontaktowy
     * @param string $contact Osoba do kontaktu
     * @param string $street Ulica ,numer budynku i ewentualnie numer lokalu
     * @param string $zip Kod pocztowy
     * @param string $city Miasto
     * @param string $name1 Pierwsza część nazwy adresata np. imię 
     * @param string $name2 Druga część nazwy adresata np. nazwisko 
     * @param string $country Kod kraju adresata np. PL
     * @param string $name3 Trzecia część nazwy adresata np. firma 
     * */
    public function setSenderAddress($street, $zip, $city, $name1, $name2 = '', $country = 'PL', $name3 = '') {
        $this->sendaddr['rname1'] = $name1;
        $this->sendaddr['rname2'] = $name2;
        $this->sendaddr['rname3'] = $name3;
        $this->sendaddr['rcountry'] = $country;
        $this->sendaddr['rzipcode'] = $zip;
        $this->sendaddr['rcity'] = $city;
        $this->sendaddr['rstreet'] = $street;
    }

    /**
     * Ustawia dane paczki:  
     * @param string $references  Referencje (pole to jest drukowane na etykietach, zazwyczaj podaje się w tym polu skrócony opis zawartości paczki, nr zamównienia etc.)
     * @param string $notes Uwagi
     * @param string $date Data nadania, jeśli brak zostanie wstawiona aktualna data [YYYY-MM-DD]
     * 
     */
    public function setPackageDetails($references = '', $notes = '', $date = '') {
        $this->package['date'] = $date;
        $this->package['references'] = $references;
        $this->package['notes'] = $notes;
    }

    /**
     * Do ustawienia płatnosći przy odbiorze:
     * @param string $cod_amount kwota COD
     * Jest jeszcze możliwość ustawiania innych opcji, ale na razie z tego nie korzystamy:
     * @param byte $exw  (ExWorks-Service) - Płaci odbiorca
     * @param byte $rod  (DocumentReturn-Servic) - Zwrot dokumentów
     * @param byte $pod  (ProofOfDelivery-Service) - Potwierdzenie dostawy
     * @param byte $exc  (Exchange-Service) - Wymiana towaru
     * @param byte $ident  (Ident-Service) - Zwrot podpisanej umowy, po uprzedniej identyfikacji odbiorcy przez kuriera
     * @param byte $daw  (DeliveryAtWork-Service) - Doręczenie paczki do rąk własnych odbiorcy lub zdefiniowanego miejsca w firmie
     * @param byte $ps  (Pick&Ship-Service) - Odbiór paczki spod dowolnego adresu i dostarczenie jej do odbiorcy
     * @param byte $pr  (Pick&Return-Service) - Odbiór paczki spod dowolnego adresu i dostarczenie jej do firmy/osoby zlecającej usługę
     * @param byte $s10  (10:00-Service) - Dostawa do godziny 10:00
     * @param byte $s12 (12:00-Service) - Dostawa do godziny 12:00
     * @param byte $sat (Saturday-Service) - Dostawa w sobotę
     * @param byte $ow  (Odbiór własny) - Odbiór paczki w filii GLS Poland
     * @param byte $srs  (ShopReturn-Service) - Zwrot paczki w dowolnym punkcie ParcelShop
     */
    public function setCOD($cod_amount) {
        $this->$srv_bool['cod'] = 1;
        $this->$srv_bool['cod_amount'] = $cod_amount;
    }

    /**
     * Dodawanie paczki do przesyłki
     * @param string $reference opis paczki
     * @param string $weight  waga paczki
     */
    public function addParcel($weight = '', $reference = '') {
        $parcel = array('number' => '', 'reference' => $reference, 'weight' => $weight, 'srv_bool' => '', 'srv_ade' => '');
        $this->package['parcels'][] = $parcel;
    }

    /**
     * Dodaje przesyłkę do poczekalni (w przesyłce może być wiele paczek)
     */
    public function addPackageToWaitRoom() {

        $this->package['srv_bool '] = $this->srv_bool;
        $this->package['sendaddr '] = $this->sendaddr;
        $aCMsg = array('session' => $this->session, 'consign_prep_data' => $this->package);
        try {
            $result = $this->sopClient->__soapCall('adePreparingBox_Insert', $aCMsg);
            $this->shipment_id = $result->id;
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }
        if ((int) $this->shipment_id != 0) {
            $SohoShopGLSModel = new SohoShopGLSModel();
            $SohoShopGLSModel->shipment_number = (int) $this->shipment_id;
            $SohoShopGLSModel->reciver_name = $this->package['rname1'] . ' ' . $this->package['rname2'] . ' ' . $this->package['rname3'];
            $SohoShopGLSModel->postcode = $this->package['rzipcode'];
            $SohoShopGLSModel->city = $this->package['rcity'];
            $SohoShopGLSModel->addres = $this->package['rstreet'];
            $SohoShopGLSModel->add();
        }

        return $SohoShopGLSModel->id;
    }

    /**
     * Po wprowadzeniu do przygotowalni kompletu danych na temat przesyłek należy wygenerować tzw. Potwierdzenie nadania. Na Potwierdzeniu nadania mają się tym samym znaleźć te przesyłki z przygotowalni, które chcemy nadać. Wygenerowanie takiego potwierdzenia jest procesem nieodwracalnym.
     */
    public function createPickUp() {
        $aCMsg = array('session' => $this->session, 'consigns_ids' => array($this->shipment_id), 'desc' => 'Potwierdzenie nadania z PrestaShop');
        try{
            $result = $this->sopClient->__soapCall('adePickup_Create', $aCMsg);
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }       
        $this->pickup_id = $result->id;
    }

    /**
     * generowanie pdf z listami przewozowymi, można ustawiać tryby wydruku, ale narazie to jest nie obsłużone, drukujemy w rogu A4
     */
    public function getLabels() {

        $aCMsg = array('session' => $this->session, 'id' => $this->shipment_id, 'mode' => 'one_label_on_a4_lt_pdf');
        try{
            $result = $this->sopClient->__soapCall('adePreparingBox_GetConsignLabels', $aCMsg);
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }           
        $pdf_decoded = base64_decode ($result->labels);
        //Write data back to pdf file
        $pdf = fopen(_PS_MODULE_DIR_."sohoshopgls/pdf/".(string)$this->shipment_id.".pdf","w");
        fwrite ($pdf,$pdf_decoded);
        //close output file
        fclose ($pdf);        
    }

    /**
     * generowanie pdf z potwierdzeniem nadania
     */
    public function getReceipt() {
        $aCMsg = array('session' => $this->session, 'id' => $this->pickup_id, 'mode' => 'condensed');
        try{
            $result = $this->sopClient->__soapCall('adePickup_GetReceipt', $aCMsg);
        } catch (SoapFault $e) {
            $this->error[] = $this->getErrorDesc($e->faultcode);
        }           
        return $result->receipt;
    }

    /**
     * 
     * @todo bcv
     */
    public function hookDisplayAdminOrder($params) {
        $this->context->controller->addJS($this->_path . 'views/js/hook_order.js');
        $this->smarty->assign('sohoshopgls_ajax_request', $this->baseUrlAjax);
        $this->smarty->assign('id_order', Tools::getValue('id_order'));
        $this->smarty->assign('senderForm', $this->renderSenderForm());
        $this->smarty->assign('reciverForm', $this->renderReciverForm());
        $this->smarty->assign('sohoshopgls_przygotowalniaLink', $this->context->link->getAdminLink('sohoshopglsPackages', false));
        $this->smarty->assign('propertiesForm', $this->renderPackagePropertiesForm());
        return $this->display(__FILE__, 'views/templates/hook/order.tpl');
    }

    public function renderSenderForm() {

        $fields_form[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Nadawca')
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 1'),
                        'name' => 'name1',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 2'),
                        'name' => 'name2'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 3'),
                        'name' => 'name3'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kod pocztowy'),
                        'name' => 'zipcode',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Miasto'),
                        'name' => 'city',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Ulica'),
                        'name' => 'street',
                        'required' => true
                    ),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValuesSender(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function renderReciverForm() {

        $fields_form[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Adresat')
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 1'),
                        'name' => 'rname1',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 2'),
                        'name' => 'rname2',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Nazwa 3'),
                        'name' => 'rname3',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kod Pocztowy'),
                        'name' => 'rzipcode',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Miasto'),
                        'name' => 'rcity',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Ulica'),
                        'name' => 'rstreet',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Telefon kontaktowy'),
                        'name' => 'rphone',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Osoba do kontaktu'),
                        'name' => 'rcontact',
                        'required' => true
                    ),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValuesReciver(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function renderPackagePropertiesForm() {

        $fields_form[] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Opcje paczki')
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Pobranie za towar'),
                        'name' => 'COD',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Kwota pobrania'),
                        'name' => 'cod_amount',
                        'class' => 'fixed-width-md'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Waga paczki'),
                        'name' => 'weight',
                        'class' => 'fixed-width-md',
                        'required' => true
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Uwagi'),
                        'name' => 'notes'
                    )
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            //'fields_value' => $this->getConfigFieldsValuesProperties(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm($fields_form);
    }

    public function getBaseURL($addURI = true) {
        $domainSSL = Tools::getShopDomainSsl(true);
        $domainNOSSL = Tools::getShopDomain(true);

        if (Configuration::get('PS_SSL_ENABLED') && $domainSSL) {
            $domain = $domainSSL;
        } else {
            $domain = $domainNOSSL;
        }

        $base_uri = '';

        if ($addURI) {
            $base_uri = constant('__PS_BASE_URI__');
            if (!strlen($base_uri) || substr($base_uri, -1) != '/') {
                $base_uri = '/';
            }
        }

        return $domain . $base_uri;
    }
    
    /**
     * dodanie opisów błędów na podstawie kodów
     * @param string $error_code kod błędu
     */
    public function getErrorDesc($error_code) {  
        $errors_desc = array(
            'err_user_incorrect_username_password' => 'Niepoprawna nazwa użytkownika i/lub hasło.',
            'err_user_permissions_problem' => 'Problem z uprawnieniami użytkownika. Taki przypadek wymaga kontaktu w firmą GLS.',
            'err_user_blocked' => 'Konto użytkownika w systemie jest zablokowane. Nie ma możliwości zalogowania się do systemu.',
            'err_user_webapi_blocked' => 'Użytkownik nie posiada dostępu do usług WebAPI.',
            'err_sess_create_problem' => 'Problem z procesem autoryzacji. Taki przypadek wymaga kontaktu w firmą GLS.',
            'err_sess_not_found' => 'Identyfikator sesji nie został znaleziony. Uzyskanie nowego identyfikatora możliwe jest tylko za pomocą funkcji logowania do systemu.',
            'err_sess_expired' => 'Ważność identyfikatora sesji wygasła. Aplikacja kliencka próbowała komunikować się z systemem po upływie czasu określonym w Wytycznych dla aplikacji.',
            'err_user_insufficient_permissions' => 'Użytkownik nie posiada odpowiednich uprawnień, aby wykonać podaną metodę.',
            'err_cons_weight_is_not_a_number' => 'Waga przesyłki nie jest liczbą.',
            'err_cons_weight_is_less_than_zero' => 'Waga pojedynczej paczki nie może być mniejsza od 0,01kg (10g).',
            'err_cons_weight_of_parcel_is_to_large' => 'Waga paczki jest za duża. Informacje o wagach maksymalnych dostępne są metodą adeParcel_GetMaxWeights.',
            'err_cons_number_of_parcels_is_incorrect' => 'Liczba paczek w przesyłce jest nieprawidłowa.',
            'err_date_is_invalid' => 'Data nadania jest nieprawidłowa',
            'err_date_is_holiday' => 'Data nadania jest nieprawidłowa, ponieważ przypada w dzień wolny od pracy (święto).',
            'err_date_must_indicate_at_least_the_next_working_day' => 'Data nadania musi wskazywać następny dzień roboczy',
            'err_cons_receiver_name1_empty' => 'Nazwa 1 odbiorcy jest pusta.',
            'err_cons_receiver_country_empty' => 'Kod kraju odbiorcy jest pusty.',
            'err_cons_receiver_country_code_is_invalid' => 'Kod kraju odbiorcy jest niepoprawny.',
            'err_cons_receiver_zipcode_empty' => 'Kod pocztowy obiorcy jest pusty.',
            'err_cons_receiver_zipcode_is_invalid' => 'Kod pocztowy odbiorcy jest niepoprawny.',
            'err_cons_receiver_zipcode_does_not_correspond_to_city_name' => 'Kod pocztowy odbiorcy nie odpowiada podanej nazwie miejscowości odbioru.',
            'err_cons_receiver_zipcode_format_is_invalid' => 'Format kod pocztowego odbiorcy jest nieprawidłowy.',
            'err_cons_receiver_zipcode_temporarily_unsupported' => 'Kod pocztowy odbiorcy jest tymczasowo nieobsługiwany.',
            'err_cons_receiver_ireland_county_city_not_found' => 'Dane adresowe Irlandii - w danych nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_receiver_zipcode_indicates_dependent_territory' => 'Kod pocztowy odbiorcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_receiver_city_empty' => 'Miejscowość odbiorcy jest pusta.',
            'err_cons_receiver_street_empty' => 'Ulica odbiorcy jest pusta.',
            'err_cons_receiver_phone_passed_after_sms_isnt_correct' => 'Numer telefonu komórkowego odbiorcy podany po frazie "SMS:" nie jest poprawny.',
            'err_cons_receiver_phone_is_invalid' => 'Numer telefonu komórkowego odbiorcy jest niepoprawny (występuje tylko w wersja irlandzkiej).',
            'err_cons_receiver_address_not_exist_in_addressbook' => 'Dane adresowe nie występuja w książce adresowej (korporacja)',
            'err_cons_references_contain_invalid_data' => 'Referencje zawierają nieprawidłowe dane (korporacja).',
            'err_cons_srv_amount_of_cod_is_incorrect' => 'Kwota usługi COD jest nieprawidłowa.',
            'err_cons_srv_amount_of_cod_is_too_large' => 'Kwota usługi COD jest za duża. Maksymalną wartość można uzyskać metodą adeServices_GetMaxCOD.',
            'err_cons_srv_for_countries_other_than_pl_are_not_available' => 'Przynajmniej jedna z usług jest niedostępna dla odbiorców poza granicami Polski.',
            'err_cons_srv_pspr_is_not_realized_for_given_country' => 'Usługa PS i/lub PR nie jest realizowana dla podanego kraju.',
            'err_cons_srv_pspr_delivery_address_must_be_in_poland' => 'Usługa PS i/lub PR adres doręczenia musi znajdować się w Polsce.',
            'err_cons_srv_ps_for_given_countries_can_not_be_realized' => 'Usługa PS nie może zostać zrealizowana dla podanych państw.',
            'err_cons_srv_10_unavailable_for_given_country_and_postal_code' => 'Usługa dostawy do 10:00 nie jest dostępna dla podanego kodu kraju i kodu pocztowego.',
            'err_cons_srv_12_unavailable_for_given_country_and_postal_code' => 'Usługa dostawy do 12:00 nie jest dostępna dla podanego kodu kraju i kodu pocztowego.',
            'err_cons_srv_sat_unavailable_for_given_country_and_postal_code' => 'Usługa dostawy w sobotę nie jest dostępna dla podanego kodu kraju i kodu pocztowego.',
            'err_cons_srv_cod_service_is_unavailable' => 'Usługa COD nie jest dostępna.',
            'err_cons_srv_exw_service_is_unavailable' => 'Usługa EXW nie jest dostępna.',
            'err_cons_srv_rod_service_is_unavailable' => 'Usługa ROD nie jest dostępna.',
            'err_cons_srv_pod_service_is_unavailable' => 'Usługa POD nie jest dostępna.',
            'err_cons_srv_exc_service_is_unavailable' => 'Usługa EXC nie jest dostępna.',
            'err_cons_srv_ident_service_is_unavailable' => 'Usługa IDENT nie jest dostępna.',
            'err_cons_srv_daw_service_is_unavailable' => 'Usługa DAW nie jest dostępna.',
            'err_cons_srv_ps_service_is_unavailable' => 'Usługa PS nie jest dostępna.',
            'err_cons_srv_pr_service_is_unavailable' => 'Usługa PR nie jest dostępna.',
            'err_cons_srv_10_service_is_unavailable' => 'Usługa dostawy do 10:00 nie jest dostępna.',
            'err_cons_srv_12_service_is_unavailable' => 'Usługa dostawy do 12:00 nie jest dostępna.',
            'err_cons_srv_sat_service_is_unavailable' => 'Usługa dostawy w sobotę nie jest dostępna.',
            'err_cons_srv_pyo_service_is_unavailable' => 'Usługa odbioru własnego nie jest dostępna.',
            'err_cons_srv_srs_service_is_unavailable' => 'Usługa ShopReturn nie jest dostępna.',
            'err_cons_srv_standard_no_services' => 'Przesyłka bez usług (serwisów)',
            'err_cons_srv_incorrect_combination_of_services' => 'Nieprawidłowa kombinacja serwisów.',
            'err_cons_srv_daw_first_and_last_name_empty' => 'Usługa DAW - Pole imię i nazwisko jest puste.',
            'err_cons_srv_exc_sender_name1_empty' => 'Usługa EXC - Imię i nazwisko nadawcy jest puste.',
            'err_cons_srv_exc_sender_country_empty' => 'Usługa EXC - Kod kraju nadawcy jest pusty.',
            'err_cons_srv_exc_sender_zipcode_empty' => 'Usługa EXC - Kod pocztowy nadawcy jest pusty.',
            'err_cons_srv_exc_sender_city_empty' => 'Usługa EXC - Miejscowość nadawcy jest pusta.',
            'err_cons_srv_exc_sender_street_empty' => 'Usługa EXC - Ulica nadawcy jest pusta.',
            'err_cons_srv_exc_sender_country_code_is_invalid' => 'Usługa EXC - Kod kraju nadawcy jest nieprawidłowy.',
            'err_cons_srv_exc_sender_zipcode_is_invalid' => 'Usługa EXC - Kod pocztowy nadawcy jest nieprawidłowy.',
            'err_cons_srv_exc_sender_zipcode_does_not_correspond_to_city_name' => 'Usługa EXC - Kod pocztowy nadawcy nie odpowiada podanej nazwie miejscowości nadawcy.',
            'err_cons_srv_exc_sender_zipcode_format_is_invalid' => 'Usługa EXC - Format kod pocztowego nadawcy jest nieprawidłowy.',
            'err_cons_srv_exc_sender_zipcode_temporarily_unsupported' => 'Usługa EXC - Kod pocztowy nadawcy jest tymczasowo nieobsługiwany.',
            'err_cons_srv_exc_sender_ireland_county_city_not_found' => 'Usługa EXC - Dane adresowe Irlandii - w danych nadawcy nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_exc_sender_zipcode_indicates_dependent_territory' => 'Usługa EXC - Podany kod pocztowy nadawcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_exc_rec_name1_empty' => 'Usługa EXC - Imię i nazwisko odbiorcy jest puste.',
            'err_cons_srv_exc_rec_country_empty' => 'Usługa EXC - Kod kraju odbiorcy jest pusty.',
            'err_cons_srv_exc_rec_zipcode_empty' => 'Usługa EXC - Kod pocztowy odbiorcy jest pusty.',
            'err_cons_srv_exc_rec_city_empty' => 'Usługa EXC - Miejscowość odbiorcy jest pusta.',
            'err_cons_srv_exc_rec_street_empty' => 'Usługa EXC - Ulica odbiorcy jest pusta.',
            'err_cons_srv_exc_rec_country_code_is_invalid' => 'Usługa EXC - Kod kraju odbiorcy jest nieprawidłowy.',
            'err_cons_srv_exc_rec_zipcode_is_invalid' => 'Usługa EXC - Kod pocztowy odbiorcy jest pusty.',
            'err_cons_srv_exc_rec_zipcode_does_not_correspond_to_city_name' => 'Usługa EXC - Kod pocztowy odbiorcy nie odpowiada podanej nazwie miejscowości odbiorcy.',
            'err_cons_srv_exc_rec_zipcode_format_is_invalid' => 'Usługa EXC - Format kod pocztowego odbiorcy jest nieprawidłowy.',
            'err_cons_srv_exc_rec_zipcode_temporarily_unsupported' => 'Usługa EXC - Kod pocztowy odbiorcy jest tymczasowo nieobsługiwany.',
            'err_cons_srv_exc_rec_ireland_county_city_not_found' => 'Usługa EXC - Dane adresowe Irlandii - w danych odbiorcy nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_exc_rec_zipcode_indicates_dependent_territory' => 'Usługa EXC - Podany kod pocztowy odbiorcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_exc_weight_is_not_a_number' => 'Usługa EXC - Waga przesyłki nie jest liczbą.',
            'err_cons_srv_exc_weight_is_less_than_zero' => 'Usługa EXC - Waga przesyłki jest mniejsza niż zero. Waga pojedynczej paczki nie może być mniejsza od 0,01kg (10g).',
            'err_cons_srv_exc_weight_of_parcel_is_to_large' => 'Usługa EXC - Waga paczki jest za duża. ',
            'err_cons_srv_ident_firstlastname_empty' => 'Usługa IDENT - Pole imię i nazwisko jest puste.',
            'err_cons_srv_ident_country_empty' => 'Usługa IDENT - Kod kraju jest pusty.',
            'err_cons_srv_ident_zipcode_empty' => 'Usługa IDENT - Kod pocztowy jest pusty.',
            'err_cons_srv_ident_city_empty' => 'Usługa IDENT - Miejscowość jest pusta.',
            'err_cons_srv_ident_street_empty' => 'Usługa IDENT - Ulica jest pusta.',
            'err_cons_srv_ident_datebirth_is_invalid' => 'Usługa IDENT - Data urodzenia jest pusta.',
            'err_cons_srv_ident_identity_card_number_empty' => 'Usługa IDENT - Numer dokumentu potwierdzającego tożsamość jest pusty.',
            'err_cons_srv_ident_identity_document_type_is_invalid' => 'Usługa IDENT - Typ dokumentu potwierdzającego tożsamość jest niepoprawny.',
            'err_cons_srv_ident_country_code_is_invalid' => 'Usługa IDENT - Kod kraju jest nieprawidłowy.',
            'err_cons_srv_ident_zipcode_is_invalid' => 'Usługa IDENT - Kod pocztowy jest nieprawidłowy.',
            'err_cons_srv_ident_zipcode_does_not_correspond_to_city_name' => 'Usługa IDENT - Kod pocztowy nie odpowiada podanej nazwie miejscowości.',
            'err_cons_srv_ident_zipcode_format_is_invalid' => 'Usługa IDENT - Format kod pocztowego jest nieprawidłowy.',
            'err_cons_srv_ident_ireland_county_city_not_found' => 'Usługa IDENT - Dane adresowe Irlandii - w danych nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_ident_zipcode_indicates_dependent_territory' => 'Usługa IDENT - Podany kod pocztowy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_ps_sender_name1_empty' => 'Usługa PS - Imię i nazwisko nadawcy jest puste.',
            'err_cons_srv_ps_sender_country_empty' => 'Usługa PS - Kod kraju nadawcy jest pusty.',
            'err_cons_srv_ps_sender_zipcode_empty' => 'Usługa PS - Kod pocztowy nadawcy jest pusty.',
            'err_cons_srv_ps_sender_city_empty' => 'Usługa PS - Miejscowość nadawcy jest pusta.',
            'err_cons_srv_ps_sender_street_empty' => 'Usługa PS - Ulica nadawcy jest pusta.',
            'err_cons_srv_ps_sender_country_code_is_invalid' => 'Usługa PS - Kod kraju nadawcy jest nieprawidłowy.',
            'err_cons_srv_ps_sender_zipcode_is_invalid' => 'Usługa PS - Kod pocztowy nadawcy jest nieprawidłowy.',
            'err_cons_srv_ps_sender_zipcode_does_not_correspond_to_city_name' => 'Usługa PS - Kod pocztowy nadawcy nie odpowiada podanej nazwie miejscowości nadawcy.',
            'err_cons_srv_ps_sender_zipcode_format_is_invalid' => 'Usługa PS - Format kod pocztowego nadawcy jest nieprawidłowy.',
            'err_cons_srv_ps_sender_zipcode_temporarily_unsupported' => 'Usługa PS - Kod pocztowy nadawcy jest tymczasowo nieobsługiwany.',
            'err_cons_srv_ps_sender_ireland_county_city_not_found' => 'Usługa PS - Dane adresowe Irlandii - w danych nadawcy nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_ps_sender_zipcode_indicates_dependent_territory' => 'Usługa PS - Podany kod pocztowy nadawcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_ps_rec_name1_empty' => 'Usługa PS - Imię i nazwisko odbiorcy jest puste.',
            'err_cons_srv_ps_rec_country_empty' => 'Usługa PS - Kod kraju odbiorcy jest pusty.',
            'err_cons_srv_ps_rec_zipcode_empty' => 'Usługa PS - Kod pocztowy odbiorcy jest pusty.',
            'err_cons_srv_ps_rec_city_empty' => 'Usługa PS - Miejscowość odbiorcy jest pusta.',
            'err_cons_srv_ps_rec_street_empty' => 'Usługa PS - Ulica odbiorcy jest pusta.',
            'err_cons_srv_ps_rec_country_code_is_invalid' => 'Usługa PS - Kod kraju odbiorcy jest nieprawidłowy.',
            'err_cons_srv_ps_rec_zipcode_is_invalid' => 'Usługa PS - Kod pocztowy odbiorcy jest nieprawidłowy.',
            'err_cons_srv_ps_rec_zipcode_does_not_correspond_to_city_name' => 'Usługa PS - Kod pocztowy odbiorcy nie odpowiada podanej nazwie miejscowości odbiorcy.',
            'err_cons_srv_ps_rec_zipcode_format_is_invalid' => 'Usługa PS - Format kod pocztowego odbiorcy jest nieprawidłowy.',
            'err_cons_srv_ps_rec_zipcode_temporarily_unsupported' => 'Usługa PS - Kod pocztowy odbiorcy jest tymczasowo nieobsługiwany.',
            'err_cons_srv_ps_rec_ireland_county_city_not_found' => 'Usługa PS - Dane adresowe Irlandii - w danych odbiorcy nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_ps_sender_zipcode_indicates_dependent_territory' => 'Usługa PS - Podany kod pocztowy odbiorcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_ps_weight_is_not_a_number' => 'Usługa PS - Waga przesyłki nie jest liczbą.',
            'err_cons_srv_ps_weight_is_less_than_zero' => 'Usługa PS - Waga przesyłki jest mniejsza niż zero. Waga pojedynczej paczki nie może być mniejsza od 0,01kg (10g).',
            'err_cons_srv_ps_weight_of_parcel_is_to_large' => 'Usługa PS - Waga paczki jest za duża. ',
            'err_cons_srv_srs_sendback_name1_empty' => 'Usługa SRS - Pole Nazwa 1 odsyłającego towar jest puste.',
            'err_cons_srv_srs_sendback_country_empty' => 'Usługa SRS - Kod kraju odsyłającego towar jest pusty.',
            'err_cons_srv_srs_sendback_zipcode_empty' => 'Usługa SRS - Kod pocztowy odsyłającego towar jest pusty.',
            'err_cons_srv_srs_sendback_city_empty' => 'Usługa SRS - Miejscowość odsyłającego towar jest pusta.',
            'err_cons_srv_srs_sendback_street_empty' => 'Usługa SRS - Ulica odsyłającego towar jest pusta.',
            'err_cons_srv_srs_sendback_country_code_is_invalid' => 'Usługa SRS - Kod kraju odsyłającego towar jest nieprawidłowy.',
            'err_cons_srv_srs_sendback_country_code_not_supported new' => 'Usługa SRS - Kod kraju odsyłającego towar nie jest obsługiwany.',
            'err_cons_srv_srs_sendback_zipcode_is_invalid' => 'Usługa SRS - Kod pocztowy odsyłającego towar jest nieprawidłowy.',
            'err_cons_srv_srs_sendback_zipcode_does_not_correspond_to_city_name' => 'Usługa SRS - Kod pocztowy odsyłającego towar nie odpowiada podanej nazwie miejscowości odsyłającego towar.',
            'err_cons_srv_srs_sendback_zipcode_format_is_invalid' => 'Usługa SRS - Format kod pocztowego odsyłającego towar jest nieprawidłowy.',
            'err_cons_srv_srs_sendback_ireland_county_city_not_found' => 'Usługa SRS - Dane adresowe Irlandii - w danych odsyłającego towar nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_srs_sendback_zipcode_indicates_dependent_territory' => 'Usługa SRS - Podany kod pocztowy odsyłającego towar wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_srs_rec_name1_empty' => 'Usługa SRS - Imię i nazwisko odbiorcy jest puste.',
            'err_cons_srv_srs_rec_country_empty' => 'Usługa SRS - Kod kraju odbiorcy jest pusty.',
            'err_cons_srv_srs_rec_zipcode_empty' => 'Usługa SRS - Kod pocztowy odbiorcy jest pusty.',
            'err_cons_srv_srs_rec_city_empty' => 'Usługa SRS - Miejscowość odbiorcy jest pusta.',
            'err_cons_srv_srs_rec_street_empty' => 'Usługa SRS - Ulica odbiorcy jest pusta.',
            'err_cons_srv_srs_rec_country_code_is_invalid' => 'Usługa SRS - Kod kraju odbiorcy jest nieprawidłowy.',
            'err_cons_srv_srs_rec_country_code_not_supported new' => 'Usługa SRS - Kod kraju odbiorcy nie jest obsługiwany.',
            'err_cons_srv_srs_rec_zipcode_is_invalid' => 'Usługa SRS - Kod pocztowy odbiorcy jest nieprawidłowy.',
            'err_cons_srv_srs_rec_zipcode_does_not_correspond_to_city_name' => 'Usługa SRS - Kod pocztowy odbiorcy nie odpowiada podanej nazwie miejscowości odbiorcy.',
            'err_cons_srv_srs_rec_zipcode_format_is_invalid' => 'Usługa SRS - Format kod pocztowego odbiorcy jest nieprawidłowy.',
            'err_cons_srv_srs_rec_ireland_county_city_not_found' => 'Usługa SRS - Dane adresowe Irlandii - w danych odbiorcy nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_srv_srs_rec_zipcode_indicates_dependent_territory' => 'Usługa SRS - Podany kod pocztowy odbiorcy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_srv_srs_weight_is_not_a_number' => 'Usługa SRS - Waga przesyłki nie jest liczbą.',
            'err_cons_srv_srs_weight_is_less_than_zero' => 'Usługa SRS - Waga przesyłki jest mniejsza niż zero. Waga pojedynczej paczki nie może być mniejsza od 0,01kg (10g).',
            'err_cons_srv_srs_weight_of_parcel_is_to_large' => 'Usługa SRS - Waga paczki jest za duża. ',
            'err_cons_parcel_numbers_problem ' => 'Problem z numeracją paczek. Dotyczy paczkek z usługami PS/PR.',
            'err_cons_pfc_invalid' => 'Identyfikator MPK jest niepoprawny. Błęd ten występuje zazwyczaj podczas próby użycia identyfikatora MPK niewystępującego na dozwolonej liście MPK. W przypadku kiedy metoda adePfc_GetStatus zwraca wartość 3, błąd ten może być spowodowany przez zbyt długi ciąg znaków przekazywany przez pole pfc (maks. 8 znaków).',
            'err_cons_sender_address_name1_empty' => 'Adres nadawcy: Pole Nazwa 1 jest puste.',
            'err_cons_sender_address_country_empty' => 'Adres nadawcy: Kod kraju jest pusty.',
            'err_cons_sender_address_zipcode_empty' => 'Adres nadawcy: Kod pocztowy jest pusty.',
            'err_cons_sender_address_city_empty' => 'Adres nadawcy: Miejscowość jest pusta.',
            'err_cons_sender_address_street_empty' => 'Adres nadawcy: Ulica jest pusta.',
            'err_cons_sender_address_country_code_is_invalid' => 'Adres nadawcy: Kod kraju jest nieprawidłowy.',
            'err_cons_sender_address_zipcode_is_invalid' => 'Adres nadawcy: Kod pocztowy jest nieprawidłowy.',
            'err_cons_sender_address_zipcode_does_not_correspond_to_city_name' => 'Adres nadawcy: Kod pocztowy nie odpowiada podanej nazwie miejscowości.',
            'err_cons_sender_address_zipcode_format_is_invalid' => 'Adres nadawcy: Format kod pocztowego jest nieprawidłowy.',
            'err_cons_sender_address_ireland_county_city_not_found' => 'Adres nadawcy z Irlandii - w danych nie znaleziono nazwy miasta lub nazwy hrabstwa (county).',
            'err_cons_sender_address_zipcode_indicates_dependent_territory' => 'Adres nadawcy: Podany kod pocztowy wskazuje na terytorium zależne. Należy podać prawidłowy kod kraju tego terytorium zależnego.',
            'err_cons_sender_address_invalid' => 'Adres nadawcy jest nieprawidłowy czyli nie występuje na liście adresów dozwolonych.',
            'err_sess_not_found' => 'Identyfikator sesji nie został znaleziony. Uzyskanie nowego identyfikatora możliwe jest tylko za pomocą funkcji logowania do systemu.',
            'err_sess_expired' => 'Ważność identyfikatora sesji wygasła. Aplikacja kliencka próbowała komunikować się z systemem po upływie czasu określonym w Wytycznych dla aplikacji.',
            'err_user_insufficient_permissions' => 'Użytkownik nie posiada odpowiednich uprawnień, aby wykonać podaną metodę.',
            'err_user_debt_collection_lock' => 'Użytkownik nie może dodawać nowych paczek z powodu blokady windykacyjnej.',
            'err_cons_not_found' => 'Nie znaleziono przesyłki.',
            'err_invalid_identifier' => 'Niepoprawny identyfikator przesyłki. ',
            'err_cons_no_label' => 'Przesyłka nie posiada etykiet (aktywne usługi PS, PR)',
            'err_cons_parcel_label_problem' => 'Problem z wygenerowaniem etykiety. ',
            'err_sess_not_found' => 'Identyfikator sesji nie został znaleziony. Uzyskanie nowego identyfikatora możliwe jest tylko za pomocą funkcji logowania do systemu.',
            'err_sess_expired' => 'Ważność identyfikatora sesji wygasła. Aplikacja kliencka próbowała komunikować się z systemem po upływie czasu określonym w Wytycznych dla aplikacji.',
            'err_user_insufficient_permissions' => 'Użytkownik nie posiada odpowiednich uprawnień, aby wykonać podaną metodę.',
            'err_sess_not_found' => 'Identyfikator sesji nie został znaleziony. Uzyskanie nowego identyfikatora możliwe jest tylko za pomocą funkcji logowania do systemu.',
            'err_sess_expired' => 'Ważność identyfikatora sesji wygasła. Aplikacja kliencka próbowała komunikować się z systemem po upływie czasu określonym w Wytycznych dla aplikacji.',
            'err_user_insufficient_permissions' => 'Użytkownik nie posiada odpowiednich uprawnień, aby wykonać podaną metodę.'            
        );
        return $errors_desc[$error_code];
    }  

}

?>