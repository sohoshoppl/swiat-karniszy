<?php

/**
 * SohoshopGLS module
 *
 * @author    SohoSHOP
 * @copyright Copyright (c) 2017 SohoSHOP
 * @license   http://SohoSHOP.pl
 *
 * http://SohoSHOP.pl
 */
/*
 * @todo poprawić z potrzeby GLS, można zgapić z smartblog lub blockcms
 */
class SohoShopGLSModel extends ObjectModel {

    public $id_shipment;
    public $shipment_number;
    public $id_order;
    public $reciver_name;
    public $postcode;
    public $city;
    public $addres;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'sohoshopgls_shipment',
        'primary' => 'id_sohoshopgls_shipment',
        'fields' => array(
            'id_sohoshopgls_shipment' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'shipment_number' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_order' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'reciver_name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),
            'postcode' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 10),
            'city' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100),
            'addres' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255)
        ),
    );

    public static function createTables() {
        return (
                SohoShopGLSModel::createShipmentTable()
                );
    }

    public static function dropTables() {
        $sql = 'DROP TABLE `' . _DB_PREFIX_ . 'sohoshopgls_shipment`';

        return Db::getInstance()->execute($sql);
    }

    public static function createShipmentTable() {
        $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sohoshopgls_shipment`(
			`id_sohoshopgls_shipment` int(10) unsigned NOT NULL auto_increment,
			`shipment_number` int(10) unsigned,
                        `id_order` int(10) unsigned,
			`reciver_name` varchar(255),
                        `postcode` varchar(10),
                        `city` varchar(100),
                        `addres` varchar(255),
			PRIMARY KEY (`id_shipment`)
			) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        return Db::getInstance()->execute($sql);
    }

}
