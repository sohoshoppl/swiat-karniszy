With ORBITVU SELF HOSTED PRODUCT TELEPORTING - Prestashop you are able to:

* Upload presentations directly from your Alphashot Editor or via Prestashop's back office (ovus or zip)

* Automatically or manually link your `ORBITVU` presentations to your Prestashop's products

* Show/hide and reorder presentations content items in product gallery

* Display all items (Prestashop product images and yours: 360° presentations and 2D photos) in product view gallery


The module is PHP application developed for Prestashop official module requirements.


Requirements
============

* This module works only with Prestashop store.

* Min PHP 5.3.x

* Extensions required: php-curl, php-zip


Installation
============

Setup:


1. **Using .zip file**

* Download the .zip file from your ORBITVU SUN account [http://orbitvu.co/market/products](http://orbitvu.co/market/products)

* Unpack the .zip file and copy it's content into your Prestashop path **PrestashopPath/modules**

* Login to your Prestashop backoffice and go to the Plugins tab

* Activate `ORBITVU SELF HOSTED` plugin

* That's it! 


Quick Start
===========

To use the module, you **don't need** `ORBITVU SUN` account. Presentations are stored on your server where Prestashop shop is installed.
Go straight to the `Usage` section.


Usage
=====

**Catalog** -> **Products** -> **Edit Product** -> **Orbitv SH tab**
* Linking presentations manually

* Enabling/disabling items

* Changing items order

**Modules** -> **Orbitvu SH** -> **Configure** -> **Cron jobs**
* Auto linking presentations comparing product's SKU with presentation's SKU

* Auto synchronizing presentations with local files

**Modules** -> **Orbitvu SH** -> **Configure**
* All plugin settings


Migrating from 1.0.x, 1.1.x, 1.2.x to 2.0.x, 2.1.x
============
Upgrade procedure movie: [https://vimeo.com/196262552/b8b59df236](https://vimeo.com/196262552/b8b59df236)

In order to migrate from old core version, please follow this steps:

1. Create a backup of your store.

2. Disable old module version (1.0.x, 1.1.x, 1.2.x). DO NOT UNINSTALL/DELETE IT!!

3. Download the newest version (>= 2.0.0) of a module from your ORBITVU SUN account [http://orbitvu.co/market/products](http://orbitvu.co/market/products)

4. **Modules** -> **Add a new module** -> choose downloaded new module file and upload it.

5. In file **PrestashopPath/override/controllers/admin/AdminProductsController.php** delete constructor method ("__construct()") with its body.

6. Install new module and go to its configuration page.

7. Above module configuration page you should see "Migration tool". If all necessary requirements are ok, then click "start migration".

8. Once migration is finished, please go to the product's edit page and check if all presentations are available and properly linked.

9. If everything is fine, you may uninstall and delete old module.


Updating module (versions >= 2.0.0)
============

In order to update module to the newest version, please follow this steps:


1. Disable current version of module

2. Download the newest version of module from your ORBITVU SUN account [http://orbitvu.co/market/products](http://orbitvu.co/market/products)

3. Unpack the .zip file and copy (overwrite) it's content into your Prestashop path **PrestashopPath/modules/orbitvushn**

4. Enable `ORBITVU SH` plugin (all update tasks will be automatically performed in the background during activation)

5. Enjoy the newest version **:)**