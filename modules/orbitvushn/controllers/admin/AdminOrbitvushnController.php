<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class AdminOrbitvushnController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->display = 'view';
        parent::__construct();

        if (!$this->module->active) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
        }
    }

    public function initProcess()
    {
        parent::initProcess();
    }

    /**
     * Creates a json response
     * @param $data
     */
    private function response($data)
    {
        if (!isset($data['refresh'])) {
            $data['refresh'] = 0;
        }
        if (!isset($data['status'])) {
            $data['status'] = 1;
        }
        header("Content-Type: application/json; charset=utf-8");
        die(Tools::jsonEncode($data));
    }

    /**
     * Validates each ajax call
     */
    private function validateAjaxCall()
    {
        $ovSettings = OrbitvuShDbdriver::getDbSettings();
        $refresh = 0;

        if (!Tools::getIsset('multi_link') || (bool)Tools::getValue('multi_link') != (bool)$ovSettings['multi_link']) {
            $refresh = 1;
        }

        if ($refresh) {
            return $this->response(array(
                'refresh' => $refresh,
                'status' => 0,
                'message' => html_entity_decode($this->l("Refreshing the page. Some options are not valid or have been changed"))
            ));
        }
    }

    /**
     * Generate new url for specific batch action
     */
    public function ajaxProcessGenerateBatchActionUrl()
    {
        $newKey = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH'].date('Y-m-d H:i:s').' '.mt_rand(1000, 10000)));
        $ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? true : false;
        $url = '';
        $db = new OrbitvuShDbdriver(DB::getInstance());
        if (Tools::getValue('type') == 'auto_synch_key') {
            $db->updateSettings('auto_synch_key', $newKey);
            $url = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $newKey, 'ov_action' => 'auto_synch'), $ssl);
        } elseif (Tools::getValue('type') == 'auto_link_key') {
            $db->updateSettings('auto_link_key', $newKey);
            $url = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $newKey, 'ov_action' => 'auto_link'), $ssl);
        } elseif (Tools::getValue('type') == 'auto_sku_key') {
            $db->updateSettings('auto_sku_key', $newKey);
            $url = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $newKey, 'ov_action' => 'auto_sku'), $ssl);
        } elseif (Tools::getValue('type') == 'remote_upload_key') {
            $db->updateSettings('remote_upload_key', $newKey);
            $url = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $newKey, 'ov_action' => 'auth'), $ssl);
        }
        return $this->response(array(
            "status" => true,
            "data" => array("url" => $url, 'new_key' => $newKey),
            "message" =>html_entity_decode($this->l("Url has been changed!")),
        ));
    }

    /**
     * Links one presentation to the product
     */
    public function ajaxProcesslinkPresentation()
    {
        $this->validateAjaxCall();

        $idPresentation = Tools::getIsset('id_presentation') ? (int)Tools::getValue('id_presentation') : false;
        $idProduct = Tools::getIsset('id_product') ? (int)Tools::getValue('id_product') : false;

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $presentation = $db->getPresentationById($idPresentation);
        $product = new Product($idProduct);

        if (!$presentation || !Validate::isLoadedObject($product)) {
            return $this->response(array(
                "status" => false,
                "message" => html_entity_decode($this->l("Error occurred! Presentation or product has not been found. Please wait! <strong>Refreshing the page...</strong>")),
            ));
        }

        $orderItem = 10;
        $orderPresentation = 10;

        if ($noPresentations = $db->countProductPresentations($idProduct)) {
            $orderPresentation = ($noPresentations * 10) + 10;
            if ($noItems = $db->countProductItems($idProduct)) {
                $orderItem = ($noItems * 10) + 10;
            }
        }

        //create product presentation
        $productPresentation = array_merge($presentation, array(
            'id_product' => $idProduct,
            'id_presentation' => $idPresentation,
            'linked_hash' => $presentation['cache_hash'],
            'order' => $orderPresentation,
            'active' => 1
        ));

        $fullProductPresentation = $db->linkPresentation($product, $productPresentation, $this->context, $orderItem);

        return $this->response(array(
            "status" => true,
            "message" => sprintf(html_entity_decode($this->l("Presentation <strong>%s</strong> has been linked")), $productPresentation['ov_name']),
            "data" => array("presentation" => $fullProductPresentation)
        ));
    }

    /**
     * copy image as original Prestashop image
     */
    public function ajaxProcessCopyImage()
    {
        $this->validateAjaxCall();


        $active = Tools::getValue('active');
        $idProductPresentationItem = Tools::getIsset('id_product_presentation_item') ? (int)Tools::getValue('id_product_presentation_item') : false;
        $db = new OrbitvuShDbdriver(DB::getInstance());
        $item = $db->parseItems($db->getProductPresentationItem($idProductPresentationItem));
        $product = new Product($item['id_product']);
        if ($db->copyAsOriginal($item, !(int)count($product->getImages($this->context->language->id, $this->context)) ? true : false)) {
            if ($item['ov_type'] == 'ov2d') { //if Orbitvu 2d Image, we have disable it to prevent duplicated images
                $db->deactivateItem($item['id_product_presentation_item']);
                $active = 0;
            }
        }
        return $this->response(array(
            'status' => 1,
            'data' => array('active' => $active),
            'message' => html_entity_decode($this->l('Image has been copied to original Prestashop images. You may want to refresh the page to see the results.'))
        ));
    }

    public function ajaxProcessDeletePresentation()
    {
        $this->validateAjaxCall();
        $idPresentation = Tools::getIsset('id_presentation') ? (int)Tools::getValue('id_presentation') : false;

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $presentation = $db->getPresentationById($idPresentation);
        if (!$presentation) {
            return $this->response(array(
                "status" => false,
                "message" => html_entity_decode($this->l("Error occurred! Presentation or product has not been found. Please wait! <strong>Refreshing the page...</strong>")),
            ));
        }

        $idsProductsLinked = $db->getLinkedIdsProductsByPresentationId($presentation['id_presentation']);
        $db->deletePresentaionByFolderName($presentation['folder_name']);
        //clean up ordering for products which were linked to deleted presentation
        foreach ($idsProductsLinked as $pr) {
            $db->cleanUpOrdering($pr['id_product']);
        }
        //delete presentationPath folder if exists
        $presentationPath = ORBITVU_PRESENTATIONS_PATH.$presentation['folder_name'];
        if (file_exists($presentationPath)) {
            OrbitvuShDir::rrmdir($presentationPath);
        }

        return $this->response(array(
            "status" => true,
            "message" => sprintf(html_entity_decode($this->l("Presentation <strong>%s</strong> has been deleted")), $presentation['ov_name']),
            "data" => array("presentation" => $presentation)
        ));
    }

    public function ajaxProcessClearUnlinkHistory()
    {
        $db = new OrbitvuShDbdriver(DB::getInstance());
        $db->clearUnlinkHistory();

        return $this->response(array(
            'status' => 1,
            'message' => html_entity_decode($this->l('Unlink history has been successfully cleared.'))
        ));
    }

    /**
     * Updates items order
     */
    public function ajaxProcessUpdateItemsPosition()
    {
        $this->validateAjaxCall();

        $idPresentation = Tools::getIsset('id_presentation') ? (int)Tools::getValue('id_presentation') : false;
        $idProduct = Tools::getIsset('id_product') ? (int)Tools::getValue('id_product') : false;

        if ($idPresentation === false || $idProduct === false) {
            return $this->response(array(
                "status" => 0,
                "message" => sprintf(html_entity_decode($this->l("Error occurred! Presentation <strong>%s</strong> or product %s not found")), $idPresentation, $idProduct),
            ));
        }
        $items = Tools::getValue('items');
        $db = new OrbitvuShDbdriver(DB::getInstance());
        foreach ($items as $item) {
            $db->updateItemOrder($item['id_product_presentation_item'], $item['order']);
        }
        return $this->response(array(
            "status" => true,
            "message" => html_entity_decode($this->l("Items order has been updated"))
        ));
    }

    /**
     * Unlinks one presentation from the product
     */
    public function ajaxProcessUnlinkPresentation()
    {
        $this->validateAjaxCall();

        $idPresentation = Tools::getIsset('id_presentation') ? (int)Tools::getValue('id_presentation') : false;
        $idProduct = Tools::getIsset('id_product') ? (int)Tools::getValue('id_product') : false;

        if ($idPresentation === false || $idProduct === false) {
            return $this->response(array(
                "status" => 0,
                "message" => sprintf(html_entity_decode($this->l("Error occurred! Presentation <strong>%s</strong> or product %s not found")), $idPresentation, $idProduct),
            ));
        }

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $p = $db->getProductPresentation($idProduct, $idPresentation);
        $db->unlinkProductPresentation($idProduct, $idPresentation);
        $db->cleanUpOrdering($idProduct);
        $db->addProductPresentationHistory($idProduct, $idPresentation);

        return $this->response(array(
            "status" => true,
            "message" => sprintf(html_entity_decode($this->l("Presentation <strong>%s</strong> has been unlinked")), isset($p['ov_name']) ? $p['ov_name'] : ''),
            "data" => array("id_presentation" => $idPresentation),
        ));
    }

    /**
     * Actiaves|Deactivates presentation item
     */
    public function ajaxProcessActivateDeactivateItem()
    {
        $this->validateAjaxCall();

        $idProductPresentationItem = Tools::getIsset('id_product_presentation_item') ? (int)Tools::getValue('id_product_presentation_item') : false;
        $active = Tools::getIsset('active') ? !(int)Tools::getValue('active') : 0;

        $db = new OrbitvuShDbdriver(DB::getInstance());
        if (!$active) {
            $db->deactivateItem($idProductPresentationItem);
        } else {
            $db->activateItem($idProductPresentationItem);
        }

        return $this->response(array(
            'status' => 1,
            'data' => array('active' => (int)$active),
            'message' => $active ? html_entity_decode($this->l("Activated")) : html_entity_decode($this->l("Deactivated"))
        ));
    }

    /**
     * Actiaves|Deactivates presentation all presentation items
     */
    public function ajaxProcessActivateDeactivateAllItems()
    {
        $this->validateAjaxCall();
        $idPresentation = Tools::getIsset('id_presentation') ? (int)Tools::getValue('id_presentation') : false;
        $idProduct = Tools::getIsset('id_product') ? (int)Tools::getValue('id_product') : false;
        $active = Tools::getIsset('active') ? (int)Tools::getValue('active') : 0;

        if ($idPresentation === false || $idProduct === false) {
            return $this->response(array(
                "status" => 0,
                "message" => sprintf(html_entity_decode($this->l("Error occurred! Presentation <strong>%s</strong> or product %s not found")), $idPresentation, $idProduct),
            ));
        }

        $db = new OrbitvuShDbdriver(DB::getInstance());
        if ($active) {
            $db->activateAllItems($idProduct, $idPresentation);
        } else {
            $db->deactivateAllItems($idProduct, $idPresentation);
        }

        return $this->response(array(
            'status' => 1,
            'data' => array('active' => $active),
            'message' => $active ? $this->l("All items have been activated") : $this->l("All items have been deactivated")
        ));
    }

    /**
     * Perform upload ovus files
     */
    public function ajaxProcessUploadPresentation()
    {
        $this->validateAjaxCall();

        if (count($_FILES) > 0) {
            if (isset($_FILES['file']['error']) && $_FILES['file']['error']) {
                return $this->response(array(
                    "status" => 0,
                    "message" => OrbitvuShDir::uploadCodeToMessage($_FILES['file']['error'])
                ));
            } else {
                $_FILES['file']['name'] = urldecode($_FILES['file']['name']);
                $oldmask = umask(0);
                $ovusPath = ORBITVU_PRESENTATIONS_PATH.$_FILES['file']['name'];
                $ext = pathinfo($ovusPath, PATHINFO_EXTENSION);
                if (!in_array(Tools::strtolower($ext), array('ovus', 'zip'))) {
                    umask($oldmask);
                    return $this->response(array(
                        "status" => 0,
                        "message" => html_entity_decode($this->l('That file format is not allowed. Only ".zip" and ".ovus" files are allowed.'))
                    ));
                } elseif (!copy($_FILES['file']['tmp_name'], $ovusPath)) {
                    umask($oldmask);
                    return $this->response(array(
                        "status" => 0,
                        "message" => html_entity_decode($this->l('Error occurred! Unable to move file.'))
                    ));
                } else {
                    umask($oldmask);
                    return $this->response(array(
                        "status" => 1,
                        "data" => array("filename" => $_FILES['file']['name']),
                        "message" => html_entity_decode($this->l('Success!<br><strong>Installing presentation...</strong>'))
                    ));
                }
            }
        } else {
            return $this->response(array(
                "status" => 0,
                "message" => html_entity_decode($this->l("Error occurred! Please try again."))
            ));
        }
    }

    public function ajaxProcessInstallPresentation()
    {
        $this->validateAjaxCall();

        $filename = Tools::getIsset('filename') ? Tools::getValue('filename') : '';
        if (!$filename) {
            return $this->response(array(
                "status" => 0,
                "message" => html_entity_decode($this->l("Error occurred! Installation failed. Wrong presentation name!"))
            ));
        }
        try {
            $localPresentationName = OrbitvuShDir::installPresentation($filename);
            $parsedPresentation = OrbitvuShDir::parseLocalPresentationContent($localPresentationName);
            if ($parsedPresentation) {
                $db = new OrbitvuShDbdriver(DB::getInstance());
                $db->addPresentation($parsedPresentation);
                //added since 2.1.1, automatic linking
                $idPresentation = $db->lastInsertId();
                $ovSettings = OrbitvuShDbdriver::getDbSettings();
                if ($ovSettings['automatic_link'] == 1 && $parsedPresentation['ov_sku']) {
                    $sku = trim($parsedPresentation['ov_sku']);
                    if (!empty($sku)) {
                        $prestaProduct = $db->getPrestaProductBySku($sku);
                        if ($prestaProduct) {
                            if (!$db->countProductPresentations($prestaProduct->id)) { //check if product has some linked Orbitvu presentation
                                $orderPresentation = 10;
                                //create product presentation
                                $productPresentation = array_merge(array(
                                    'id_product' => $prestaProduct->id,
                                    'id_presentation' => $idPresentation,
                                    'linked_hash' => $parsedPresentation['cache_hash'],
                                    'order' => $orderPresentation,
                                    'active' => 1
                                ), $db->parsePresentation($parsedPresentation)); //parsePresentation is necessary because we display this presentation

                                $fullProductPresentation = $db->linkPresentation($prestaProduct, $productPresentation, $this->context);

                                return $this->response(array(
                                    "status" => true,
                                    "message" => sprintf(html_entity_decode($this->l("Presentation <strong>%s</strong> has been successfully installed and linked")), $productPresentation['ov_name']),
                                    "data" => array(
                                        "filename" => $filename,
                                        "presentation" => $fullProductPresentation
                                    )
                                ));
                            }
                        }
                    }
                } //end added since 2.1.1, automatic linking
            }
            return $this->response(array(
                "status" => 1,
                "data" => array("filename" => $filename),
                "message" => html_entity_decode($this->l("Presentation has been successfully installed."))
            ));
        } catch (\Exception $e) {
            return $this->response(array(
                "status" => 0,
                "message" => $e->getMessage()
            ));
        }
    }

    /**
     * Pager
     */
    public function ajaxProcessPager()
    {
        $this->validateAjaxCall();

        $currentPage = Tools::getIsset('current_page') ? (int)Tools::getValue('current_page') : 0;
        $perPage = Tools::getIsset('per_page') ? (int)Tools::getValue('per_page') : 0;

        $currentPage = $currentPage < 1 ? 1 : $currentPage;
        $perPage = $perPage <= 0 ? 20 : $perPage;

        if (Tools::getIsset('clear') && Tools::getValue('clear')) {
            $currentPage = 1;
        }

        if (Tools::getIsset('direction') && Tools::getValue('direction') == 'ovnext') {
            $currentPage += 1;
        } else {
            if (Tools::getIsset('direction') && Tools::getValue('direction') == 'ovprev') {
                $currentPage -= 1;
            }
        }

        $shParams = array(
            'page_size' => $perPage,
            'ordering' => in_array(Tools::getIsset('sort') ? Tools::getValue('sort') : '', array('-name', 'name', '-sku', 'sku', '-create_date', 'create_date')) ? Tools::getValue('sort') : '-create_date',
            'current_page' => $currentPage,
        );

        $search = Tools::getIsset('search') ? strip_tags(trim(Tools::getValue('search'))) : '';
        if ($search != '') {
            $shParams['search'] = $search;
        }

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $results = $db->getPresentationsList($shParams);
        $totalPages = $results['count'] % $perPage ? (int)(floor($results['count'] / $perPage)) + 1 : (int)(floor($results['count'] / $perPage));
        $results['current_page'] = $currentPage;
        $results['per_page'] = $perPage;
        $results['total_items'] = $results['count'];
        $results['total_pages'] = $totalPages;
        $results['arrow_prev_disabled'] = $currentPage > 1 ? '' : 'orbitvu-pager-section-arrow-disabled';
        $results['arrow_next_disabled'] = (($currentPage == $totalPages) || ($totalPages == 0)) ? 'orbitvu-pager-section-arrow-disabled' : '';

        return $this->response(array(
            'status' => 1,
            'data' => $results
        ));
    }

    /**
     * Copy and link presentations from old module to the new one
     */
    public function ajaxProcessMigrate()
    {
        $maxExecutionTime = ini_get('max_execution_time');
        ini_set('max_execution_time', 3600);

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $ovSettings = OrbitvuShDbdriver::getDbSettings();

        if ($ovSettings['auto_synch_lock'] || $ovSettings['auto_link_lock']) {
            return $this->response(array(
                'status' => 0,
                'message' => html_entity_decode($this->l('Auto synchronizing or auto linking process is running. Please try again later.'))
            ));
        }

        //make sure that other linking and synchronizing processes don't spoil our migration
        OrbitvuShDbdriver::updateSettings('auto_synch_lock', 1);
        OrbitvuShDbdriver::updateSettings('auto_link_lock', 1);
        OrbitvuShDbdriver::updateSettings('migrate_progress_bar', 0);

        $oldPresentationsFolder = _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.'orbitvush'.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR;
        $oldPresentationsFolderNames = OrbitvuShDir::getLocalPresentationNames($oldPresentationsFolder);
        $noOldPresentations = count($oldPresentationsFolderNames);
        $migratedPresentations = array();
        $oldmask = umask(0);

        //Copy old presentations to the new module directory and install them in cache table
        foreach ($oldPresentationsFolderNames as $k => $oldPresentationFolderName) {
            if (!file_exists(ORBITVU_PRESENTATIONS_PATH.$oldPresentationFolderName)) { //if presnetation exists in new module, don't copy it
                OrbitvuShDir::recurseCopy($oldPresentationsFolder.$oldPresentationFolderName, ORBITVU_PRESENTATIONS_PATH.$oldPresentationFolderName);
                $newParsedPresentation = OrbitvuShDir::parseLocalPresentationContent($oldPresentationFolderName);
                if ($newParsedPresentation) {
                    //add presentation to cache table in new module
                    $db->addPresentation($newParsedPresentation);
                    $migratedPresentations[] = array(
                        'new_id' => $db->lastInsertId(),
                        'old_id' => $db->getOldModulePresentationId($oldPresentationFolderName)
                    );
                } else {
                    OrbitvuShDir::rrmdir(ORBITVU_PRESENTATIONS_PATH.$oldPresentationFolderName);
                }
            }
            OrbitvuShDbdriver::updateSettings('migrate_progress_bar', (80 * ($k + 1))/$noOldPresentations);
        }

        //link copied presentations to the products
        $linkedProducts = $db->getOldModuleProductsWithLinkedPresentations();
        $noLinkedProducts = count($linkedProducts);
        $i = 1;
        foreach ($linkedProducts as $idProduct => $linkedPresentations) {
            if (Validate::isLoadedObject(new Product((int)$idProduct, false, (int)Configuration::get('PS_LANG_DEFAULT')))) {
                foreach ($linkedPresentations as $idPresentation => $items) {
                    if ($migrated = $this->isMigrated($idPresentation, $migratedPresentations)) {
                        $newPresentation = $db->getPresentationById($migrated['new_id']);
                        if ($newPresentation) {
                            $newPresentationParsedContent = Tools::jsonDecode($newPresentation['parsed_content'], true);
                            $orderNewItem = 10;
                            $orderNewPresentation = 10;
                            if ($noPresentations = $db->countProductPresentations($idProduct)) {
                                $orderNewPresentation = ($noPresentations * 10) + 10;
                                if ($noItems = $db->countProductItems($idProduct)) {
                                    $orderNewItem = ($noItems * 10) + 10;
                                }
                            }
                            //create product presentation
                            $productNewPresentation = array_merge($newPresentation, array(
                                'id_product' => $idProduct,
                                'id_presentation' => $migrated['new_id'],
                                'linked_hash' => $newPresentation['cache_hash'],
                                'order' => $orderNewPresentation,
                                'active' => 1
                            ));
                            $db->addProductPresentation($productNewPresentation);

                            //add items
                            foreach ($newPresentationParsedContent as $key => $type) {
                                if (($key == "images2d" || $key == "sequence360") && is_array($type) && count($type)) {
                                    foreach ($type as $newItem) {
                                        foreach ($items as $oldItem) {
                                            if (($newItem['ov_type'] == 'ov360' && $oldItem['ov_type'] == $newItem['ov_type'] && !$oldItem['ov_name']) ||
                                                ($newItem['ov_type'] == 'ov2d' && $newItem['ov_type'] == $oldItem['ov_type'] && $newItem['ov_thumbnail_file'] == $oldItem['ov_name'])
                                            ) {
                                                $db->addProductPresentationItem(array(
                                                    'id_product' => $idProduct,
                                                    'id_presentation' => $migrated['new_id'],
                                                    'ov_name' => $newItem['ov_name'],
                                                    'ov_type' => $newItem['ov_type'],
                                                    'ov_thumbnail_file' => $newItem['ov_thumbnail_file'],
                                                    'order' => $oldItem['order'] + $orderNewItem,
                                                    'active' => $oldItem['active']
                                                ));
                                            }
                                        }
                                    }
                                }
                            }
                            $db->cleanUpOrdering($idProduct);
                        }
                    }
                }
            }
            //When presentations are copied (we have already progress bar at 80%)
            OrbitvuShDbdriver::updateSettings('migrate_progress_bar', 80 + ((20 * $i)/$noLinkedProducts));
            $i++;
        }
        umask($oldmask);

        OrbitvuShDbdriver::updateSettings('auto_synch_lock', 0);
        OrbitvuShDbdriver::updateSettings('auto_link_lock', 0);
        OrbitvuShDbdriver::updateSettings('migrate_progress_bar', 0);

        ini_set('max_execution_time', $maxExecutionTime);

        return $this->response(array(
            'status' => 1,
            'message' => html_entity_decode($this->l('Migration has been successfully finished. Please verify if presentations are present and linked to proper products.'))
        ));
    }

    public function ajaxProcessMigrateProgress()
    {
        $ovSettings = OrbitvuShDbdriver::getDbSettings();
        return $this->response(array(
            'status' => 1,
            'data' => array('percentage' => round($ovSettings['migrate_progress_bar'], 0)),
            'message' => html_entity_decode($this->l('Migrating...'))
        ));
    }

    /**
     * @param $idPresentation
     * @param $migratedPresentations
     * @return bool
     */
    private function isMigrated($idPresentation, $migratedPresentations)
    {
        foreach ($migratedPresentations as $presentation) {
            if ($presentation['old_id'] == $idPresentation) {
                return $presentation;
            }
        }
        return false;
    }
}
