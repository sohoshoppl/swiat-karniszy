<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class OrbitvushnRemoteModuleFrontController extends ModuleFrontController
{

    /**
     * Remote xml content to return for ALPHASHOT EDITOR
     *
     * @since    1.0.0
     * @access   private
     * @var      string $remote_xml xml content for Alphashot Editor.
     */
    private $remoteXml;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->display = 'view';

        parent::__construct();

        return $this->authorizeCronAndUpload();
    }

    /**
     * Returns xml response
     */
    private function responseXml()
    {
        header('Content-Type: text/xml; charset=utf-8');
        die($this->remoteXml->asXML());
    }

    /**
     * Encodes data to json
     * @param $data
     */
    private function response($data)
    {
        header('Content-Type: application/json');
        die(Tools::jsonEncode($data));
    }

    protected function autoSynch($cancel)
    {
        $ovSettings = OrbitvuShDbdriver::getDbSettings();

        //force cancel
        if ($cancel) {
            OrbitvuShDbdriver::updateSettings('auto_synch_lock', '0');
            return $this->response(array(
                "message" => $this->module->l("Auto synchronizing operation has been successfully canceled"),
                "success" => true
            ));
        }

        if (!$ovSettings['auto_synch_lock']) {
            OrbitvuShDbdriver::updateSettings('auto_synch_lock', '1');
            $db = new OrbitvuShDbdriver(DB::getInstance());
            $localPresentationNames = OrbitvuShDir::getLocalPresentationNames();
            $dbPresentationNames = $db->getPresentationFolderNames();
            //Delete db presentations when files are deleted from server
            foreach ($dbPresentationNames as $dbPresentationName) {
                //check every iteration if auto_synch has been broken or not
                $ovSettings = OrbitvuShDbdriver::getDbSettings(true);
                if (!$ovSettings['auto_synch_lock']) {
                    return $this->response(array(
                        "message" => $this->module->l("Presentations has been partially synchronized. Auto synchronizing process has been canceled by user."),
                        "success" => false
                    ));
                }

                $dbPresentation = $db->getPresentationByFolderName($dbPresentationName);
                $presentationPath = ORBITVU_PRESENTATIONS_PATH.$dbPresentation['folder_name'];
                $contentXmlPath = $presentationPath.DIRECTORY_SEPARATOR.'content2.xml';

                //delete
                if (!file_exists($presentationPath) || !file_exists($contentXmlPath)) { //we have to remove presentation from database and files - local files are corrupted or deleted
                    //first check if presestation is linked to any products
                    $idsProductsLinked = $db->getLinkedIdsProductsByPresentationId($dbPresentation['id_presentation']);
                    $db->deletePresentaionByFolderName($dbPresentationName);
                    //clean up ordering for products which were linked to deleted presentation
                    foreach ($idsProductsLinked as $pr) {
                        $db->cleanUpOrdering($pr['id_product']);
                    }
                    //delete presentationPath folder if exists
                    if (file_exists($presentationPath)) {
                        OrbitvuShDir::rrmdir($presentationPath);
                    }
                } else {
                    if (!OrbitvuShDir::hashIsEqual($dbPresentation['cache_hash'], $dbPresentation['folder_name'])) { //check state of db presentation
                        $parsedLocalPresentation = OrbitvuShDir::parseLocalPresentationContent($dbPresentationName);
                        $db->synchronizePresentation($dbPresentation['id_presentation'], $parsedLocalPresentation);
                    }
                }
            }
            //Add new presentations if new presentations folders exist
            foreach ($localPresentationNames as $localPresentationName) {
                //check every iteration if auto_synch has been broken or not
                $ovSettings = OrbitvuShDbdriver::getDbSettings(true);
                if (!$ovSettings['auto_synch_lock']) {
                    return $this->response(array(
                        "message" => $this->module->l("Presentations has been partially synchronized. Auto synchronizing process has been canceled by user."),
                        "success" => false
                    ));
                }
                if (!in_array($localPresentationName, $dbPresentationNames)) { //we have to add new presentation to database
                    $parsedPresentation = OrbitvuShDir::parseLocalPresentationContent($localPresentationName);
                    if ($parsedPresentation) {
                        $db->addPresentation($parsedPresentation);
                    }
                }
            }
            OrbitvuShDbdriver::updateSettings('auto_synch_lock', '0');
            return $this->response(array(
                "message" => $this->module->l("Presentations has been successfully synchronized"),
                "success" => true
            ));
        } else {
            return $this->response(array(
                "message" => $this->module->l("Auto synchronizing action is already running"),
                "success" => false
            ));
        }
    }

    protected function autoLink($cancel)
    {
        $ovSettings = OrbitvuShDbdriver::getDbSettings();

        //force cancel
        if ($cancel) {
            OrbitvuShDbdriver::updateSettings('auto_link_lock', '0');
            return $this->response(array(
                "message" => $this->module->l("Auto linking operation has been successfully canceled"),
                "success" => true
            ));
        }

        if (!$ovSettings['auto_link_lock']) {
            OrbitvuShDbdriver::updateSettings('auto_link_lock', '1');
            $db = new OrbitvuShDbdriver(DB::getInstance());
            $shPresentations = $db->getAllPresentations();
            if (count($shPresentations)) {
                $dbProductIds = $db->getAllPrestashopProductIds();
                foreach ($dbProductIds as $product) {
                    $ovSettings = OrbitvuShDbdriver::getDbSettings(true);
                    //check every iteration if auto_synch has been broken or not
                    if (!$ovSettings['auto_link_lock']) {
                        return $this->response(array(
                            "message" => $this->module->l("Presentations has been partially linked. Auto linking process has been canceled by user."),
                            "success" => false
                        ));
                    }
                    if (!$db->countProductPresentations($product['id_product'])) {
                        $prestaProduct = new Product($product['id_product']);
                        foreach ($shPresentations as $shPresentation) {
                            $shPresentation['ov_sku'] = trim($shPresentation['ov_sku']);
                            if ($shPresentation['ov_sku'] == $prestaProduct->reference && $shPresentation['ov_sku'] != '') {
                                if (!$db->wasUnlinked($product['id_product'], $shPresentation['id_presentation']) && (OrbitvuShDir::hashIsEqual($shPresentation['cache_hash'], $shPresentation['folder_name']))) {
                                    $orderPresentation = 10;
                                    //create product presentation
                                    $productPresentation = array_merge(array(
                                        'id_product' => $product['id_product'],
                                        'id_presentation' => $shPresentation['id_presentation'],
                                        'linked_hash' => $shPresentation['cache_hash'],
                                        'order' => $orderPresentation,
                                        'active' => 1
                                    ), $shPresentation);

                                    $db->linkPresentation($prestaProduct, $productPresentation, $this->context);
                                    break;
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
                OrbitvuShDbdriver::updateSettings('auto_link_lock', '0');
                return $this->response(array(
                    "message" => $this->module->l("Presentations has been successfully linked"),
                    "success" => true
                ));
            }
            OrbitvuShDbdriver::updateSettings('auto_link_lock', '0');
            return $this->response(array(
                "message" => $this->module->l("No SH presentations has been found while performing auto linking action."),
                "success" => false
            ));
        } else {
            return $this->response(array(
                "message" => $this->module->l("Auto linking action is already running"),
                "success" => false
            ));
        }
    }

    /**
     * Authorize before doing upload action
     *
     * @param $key
     */
    public function doRemoteAuthorizeAction($key)
    {
        $ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? true : false;

        //generate token
        $token = md5(md5(date('Y-m-d H:i:s') . ' ' . mt_rand(1000, 10000)));
        //save token in a cookie
        $this->context->cookie->orbitvu_token = $token;
        //set cookie for AE editor
        setcookie('orbitvu_random_cookie', $token, time() + 3600 * 3);
        //provide upload_url

        $upload_url = str_replace('&', '&amp;', $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $key, 'ov_action' => 'upload', 'ov_token' => $token), $ssl));

        $this->remoteXml->addChild('code', 0);
        $this->remoteXml->addChild('message', 'Authorization succeeded!');
        $this->remoteXml->addChild('data', $upload_url);
        return $this->responseXml();
    }

    /**
     * Perfrom remote upload action from Alphashot Editor
     */
    public function doRemoteUploadAction()
    {
        if (count($_FILES) > 0) {
            if (!isset($this->context->cookie->orbitvu_token) || ($this->context->cookie->orbitvu_token != Tools::getValue('ov_token'))) {
                $this->remoteXml->addChild('code', 2);
                $this->remoteXml->addChild('message', 'Authorization failed!');
                return $this->responseXml();
            }
            if (isset($_FILES['path']['error']) && $_FILES['path']['error']) {
                $this->remoteXml->addChild('code', 5);
                $this->remoteXml->addChild('message', OrbitvuShDir::uploadCodeToMessage($_FILES['path']['error']));
                return $this->responseXml();
            } else {
                $oldmask = umask(0);
                $ovusPath = ORBITVU_PRESENTATIONS_PATH.$_FILES['path']['name'];
                $ext = pathinfo($ovusPath, PATHINFO_EXTENSION);
                if (!in_array(Tools::strtolower($ext), array('ovus', 'zip'))) {
                    umask($oldmask);
                    $this->remoteXml->addChild('code', 1);
                    $this->remoteXml->addChild('message', 'Wrong file format!');
                    return $this->responseXml();
                } elseif (!copy($_FILES['path']['tmp_name'], $ovusPath)) {
                    umask($oldmask);
                    $this->remoteXml->addChild('code', 4);
                    $this->remoteXml->addChild('message', 'Upload error! Unable to move file');
                    return $this->responseXml();
                } else {
                    try {
                        $localPresentationName = OrbitvuShDir::installPresentation($_FILES['path']['name']);
                        $parsedPresentation = OrbitvuShDir::parseLocalPresentationContent($localPresentationName);
                        if ($parsedPresentation) {
                            $db = new OrbitvuShDbdriver(DB::getInstance());
                            $db->addPresentation($parsedPresentation);

                            //added since 2.1.1, automatic linking
                            $idPresentation = $db->lastInsertId();
                            $ovSettings = OrbitvuShDbdriver::getDbSettings();

                            if ($ovSettings['automatic_link'] == 1 && $parsedPresentation['ov_sku']) {
                                $sku = trim($parsedPresentation['ov_sku']);
                                if (!empty($sku)) {
                                    $prestaProduct = $db->getPrestaProductBySku($sku);
                                    if ($prestaProduct) {
                                        if (!$db->countProductPresentations($prestaProduct->id)) { //check if product has some linked Orbitvu presentation
                                            $orderPresentation = 10;
                                            //create product presentation
                                            $productPresentation = array_merge(array(
                                                'id_product' => $prestaProduct->id,
                                                'id_presentation' => $idPresentation,
                                                'linked_hash' => $parsedPresentation['cache_hash'],
                                                'order' => $orderPresentation,
                                                'active' => 1
                                            ), $parsedPresentation);

                                            $db->linkPresentation($prestaProduct, $productPresentation, $this->context);
                                        }
                                    }
                                }
                            } //end added since 1.1.1, automatic linking
                        }
                        $this->remoteXml->addChild('code', 0);
                        $this->remoteXml->addChild('message', 'Upload succeed!');
                        $this->remoteXml->addChild('data', false);
                        setcookie('orbitvu_random_cookie', '', time() - 3600);
                        setcookie('orbitvu_token', '', time() - 3600);
                        return $this->responseXml();
                    } catch (\Exception $e) {
                        umask($oldmask);
                        $this->remoteXml->addChild('code', 4);
                        $this->remoteXml->addChild('message', 'Upload error! Unable to install presentation');
                        return $this->responseXml();
                    }
                }
            }
        }
        $this->remoteXml->addChild('code', 3);
        $this->remoteXml->addChild('message', 'Wrong HTTP method!');
        return $this->responseXml();
    }

    /**
     * Action responsible for CRON jobs and upload from Alphashot Editor
     */
    public function authorizeCronAndUpload()
    {
        $action = Tools::getIsset('ov_action') ? Tools::getValue('ov_action') : '';
        $key = Tools::getIsset('ov_auth') ? Tools::getValue('ov_auth') : '';
        $cancel = Tools::getIsset('ov_cancel') ? (bool)Tools::getValue('ov_cancel') : false;
        $settings = OrbitvuShDbdriver::getDbSettings();

        if ($action == 'auto_synch' && (isset($settings['auto_synch_key']) && $settings['auto_synch_key'] == $key)) {
            $this->autoSynch($cancel);
        } elseif ($action == 'auto_link' && (isset($settings['auto_link_key']) && $settings['auto_link_key'] == $key)) {
            $this->autoLink($cancel);
        } elseif ($action == 'auth' || $action == 'upload') { //uploading presentations from Alphashot Editor
            $this->remoteXml = new SimpleXMLElement('<ovs_response/>');
            if ($settings['remote_upload_key'] != $key) {
                $this->remoteXml->addChild('code', 4);
                $this->remoteXml->addChild('message', 'Authorization URL is not complete, not valid or expired.');
                return $this->responseXml();
            }
            if ($action == 'upload') {
                return $this->doRemoteUploadAction();
            }
            return $this->doRemoteAuthorizeAction($key);
        }
    }
}
