<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class OrbitvuTemplate
{
    public $ovSettings;

    public $prestaSettings;

    public $ratio;

    public $product;

    public $galleryMainImageClass = '';

    public $galleryZoomerClass = '';

    public $galleryZoomerZoomClass = ''; //this is necessary when zoomer zoom fades in, there is a blink of unproper cursor

    public $lightboxDimensions = array();

    protected $prestaItems;

    protected $ovItems;

    protected $allItems = null;

    protected $has2d = null;

    protected $has360 = null;

    protected $hasTour = null;

    protected $hasVideo = null;

    protected $context;

    public function __construct($product, $context)
    {
        $this->product = $product;
        $this->context = $context;

        //Orbitvu plugin settings
        $this->ovSettings = OrbitvuShDbdriver::getDbSettings();

        /**************************************************************************************************
         * This is just in case someone just copied files without deactivating and activating again module.
         * This is important when new version contain some new options fields (like between 2.0.1
         * and 2.0.2 - the last one has 4 new fields or 2.0.2 and 2.1
         **************************************************************************************************/
        if (!array_key_exists('keep_original_enabled', $this->ovSettings)) {
            $this->ovSettings['keep_original_enabled'] = '1';
        }
        if (!array_key_exists('big_width', $this->ovSettings)) {
            $this->ovSettings['big_width'] = '1280px';
        }
        if (!array_key_exists('big_height', $this->ovSettings)) {
            $this->ovSettings['big_height'] = '960px';
        }
        if (!array_key_exists('medium_width', $this->ovSettings)) {
            $this->ovSettings['medium_width'] = '800px';
        }
        if (!array_key_exists('medium_height', $this->ovSettings)) {
            $this->ovSettings['medium_height'] = '600px';
        }


        if (!array_key_exists('zoom_magnifier_enabled', $this->ovSettings)) {
            $this->ovSettings['zoom_magnifier_enabled'] = '1';
        }
        if (!array_key_exists('lightbox_enabled', $this->ovSettings)) {
            $this->ovSettings['lightbox_enabled'] = '1';
        }
        if (!array_key_exists('lightbox_proportions', $this->ovSettings)) {
            $this->ovSettings['lightbox_proportions'] = '4/3';
        }
        /*if (!array_key_exists('lightbox_open_click', $this->ovSettings)) { removed in 2.1.0
            $this->ovSettings['lightbox_open_click'] = '0';
        }*/

        //added in 2.1.0
        if (!array_key_exists('zoom_magnifier_maxzoom_enabled', $this->ovSettings)) {
            $this->ovSettings['zoom_magnifier_maxzoom_enabled'] = '0';
        }
        if (!array_key_exists('zoom_magnifier_maxzoom_width', $this->ovSettings)) {
            $this->ovSettings['zoom_magnifier_maxzoom_width'] = '1800px';
        }
        if (!array_key_exists('zoom_magnifier_maxzoom_height', $this->ovSettings)) {
            $this->ovSettings['zoom_magnifier_maxzoom_height'] = '1500px';
        }

        if (!array_key_exists('lightbox_colorized_bg', $this->ovSettings)) {
            $this->ovSettings['lightbox_colorized_bg'] = '1';
        }
        if (!array_key_exists('lightbox_thumbnails_enabled', $this->ovSettings)) {
            $this->ovSettings['lightbox_thumbnails_enabled'] = '1';
        }
        if (!array_key_exists('lightbox_buttons_shadow_enabled', $this->ovSettings)) {
            $this->ovSettings['lightbox_buttons_shadow_enabled'] = '1';
        }
        if (!array_key_exists('lightbox_maxzoom_enabled', $this->ovSettings)) {
            $this->ovSettings['lightbox_maxzoom_enabled'] = '0';
        }
        if (!array_key_exists('lightbox_maxzoom_width', $this->ovSettings)) {
            $this->ovSettings['lightbox_maxzoom_width'] = '1800px';
        }
        if (!array_key_exists('lightbox_maxzoom_height', $this->ovSettings)) {
            $this->ovSettings['lightbox_maxzoom_height'] = '1500px';
        }
        /*********************************************** END **********************************************/

        //Count intrinsic ratio
        //$ratio = '75%'; //4:3 default
        $dim = $this->validateProportions($this->ovSettings['main_proportions']);
        $ratio = $dim['height'] * 100 / $dim['width'];
        $this->ratio = (string)$ratio.'%';

        //Count dimensions for lightbox slide
        if ($this->ovSettings['lightbox_enabled'] !== '0') {
            $dim = $this->validateProportions($this->ovSettings['lightbox_proportions']);
            $this->lightboxDimensions['width'] = '80vw'; //default max 80% of viewport vidth
            $this->lightboxDimensions['height'] = (($dim['height']/$dim['width']) * 80)."vw"; //$height/$width * 80
            $this->lightboxDimensions['max_width'] = (($dim['width']/$dim['height']) * 80)."vh"; //16/9 * 80
            $this->lightboxDimensions['max_height'] = '80vh'; //default max 80% of viewport height
        }

        //Prestashop's image settings
        $medium = Image::getSize(ImageType::getFormatedName('large')); //Presta's large image is our medium
        $large = Image::getSize(ImageType::getFormatedName('thickbox'));
        $thumbnail = Image::getSize(ImageType::getFormatedName('cart'));

        $this->prestaSettings = array(
            'medium_width' => $medium['width'],
            'medium_height' => $medium['height'],
            'large_width' => $large['width'],
            'large_height' => $large['height'],
            'thumbnail_width' => $thumbnail['width'],
            'thumbnail_height' => $thumbnail['height']
            //'shop_thumbnail' => WC_Admin_Settings::get_option('shop_thumbnail_image_size')
        );

        /**
         * Set proper cursor for product gllaery (depending lightbox/zoom magnifier enabled or disabled)
         */
        if ($this->ovSettings['zoom_magnifier_enabled'] == '0' && $this->ovSettings['lightbox_enabled'] !== '0') {
            $this->galleryMainImageClass = 'orbitvu-lightbox-cursor';
        }

        if ($this->ovSettings['zoom_magnifier_enabled'] !== '0' && $this->ovSettings['lightbox_enabled'] !== '0') {
            $this->galleryZoomerClass = 'orbitvu-lightbox-cursor';
            $this->galleryZoomerZoomClass = 'orbitvu-lightbox-cursor';
        }

        /**
         * Prepare max zoom for magnifier and lightbox
         */
        if ($this->ovSettings['zoom_magnifier_maxzoom_enabled'] === '1') {
            $this->ovSettings['zoom_magnifier_maxzoom_enabled'] = 1;
            $this->ovSettings['zoom_magnifier_maxzoom_width'] = (int)$this->ovSettings['zoom_magnifier_maxzoom_width'];
            $this->ovSettings['zoom_magnifier_maxzoom_height'] = (int)$this->ovSettings['zoom_magnifier_maxzoom_height'];
        } else {
            $this->ovSettings['zoom_magnifier_maxzoom_enabled'] = 0;
            $this->ovSettings['zoom_magnifier_maxzoom_width'] = 0;
            $this->ovSettings['zoom_magnifier_maxzoom_height'] = 0;
        }

        if ($this->ovSettings['lightbox_maxzoom_enabled'] === '1') {
            $this->ovSettings['lightbox_maxzoom_enabled'] = 1;
            $this->ovSettings['lightbox_maxzoom_width'] = (int)$this->ovSettings['lightbox_maxzoom_width'];
            $this->ovSettings['lightbox_maxzoom_height'] = (int)$this->ovSettings['lightbox_maxzoom_height'];
        } else {
            $this->ovSettings['lightbox_maxzoom_enabled'] = 0;
            $this->ovSettings['lightbox_maxzoom_width'] = 0;
            $this->ovSettings['lightbox_maxzoom_height'] = 0;
        }
    }


    /**
     * Validates if proportions of main image or lightbox image are proper
     *
     * @param $proportionsString
     * @return array
     */
    protected function validateProportions($proportionsString)
    {
        if (preg_match("/^\d+\/\d+$/", $proportionsString)) {
            list($width, $height) = explode("/", $proportionsString);
            if ($width == 0.0) { //dont allow width = 0
                $width = 4;
            }
            if ($height == 0.0) { //prevent division by zero
                $height = 3;
            }
        } else {
            $width = 4;
            $height = 3;
        }
        return array('width' => (int)$width, 'height' => (int)$height);
    }

    protected function getPrestaItems()
    {
        $prestaItems = $this->product->getImages((int)$this->context->language->id);
        $this->prestaItems = array();
        $i = 0;
        foreach ($prestaItems as $item) {
            $this->prestaItems[$i]['ov_id'] = $item['id_image'];
            $title = (Tools::getIsset($item['legend']) && !empty($item['legend'])) ? $item['legend'] : $this->product->name;
            $title = str_replace('"', '&#34;', $title);
            $title = str_replace('\'', '&#39;', $title);
            $this->prestaItems[$i]['ov_name'] = $title;
            $this->prestaItems[$i]['ov_alt'] = $title;
            $this->prestaItems[$i]['ov_type'] = 'ov2d-native';
            $this->prestaItems[$i]['ov_script_url'] = '';
            $this->prestaItems[$i]['view_url'] = '';
            $this->prestaItems[$i]['ov_resized_thumb'] = $this->context->link->getImageLink($this->product->link_rewrite, $item['id_image'], ImageType::getFormatedName('cart'));
            $this->prestaItems[$i]['ov_resized_medium'] = $this->context->link->getImageLink($this->product->link_rewrite, $item['id_image'], ImageType::getFormatedName('large'));
            $this->prestaItems[$i]['ov_resized_large'] = $this->context->link->getImageLink($this->product->link_rewrite, $item['id_image'], ImageType::getFormatedName('thickbox'));
            $i++;
        }

        return $this->prestaItems;
    }

    protected function getOvItems()
    {
        $ovdb = new OrbitvuShDbdriver(DB::getInstance());
        //Get Orbitvu items
        $this->ovItems = $ovdb->getProductPresentationItemsActive($this->product->id);
        foreach ($this->ovItems as $k => $ovItem) {
            $ovItem['ov_presentation_name'] = trim($ovItem['ov_presentation_name']);
            if ($ovItem['ov_type'] == 'ov2d') {
                $this->ovItems[$k]['ov_name'] = $ovItem['ov_presentation_name']." - 2D image";
                $this->ovItems[$k]['ov_alt'] = $ovItem['ov_presentation_name']." - 2D image";
            } elseif ($ovItem['ov_type'] == 'ov360') {
                $this->ovItems[$k]['ov_name'] = $ovItem['ov_presentation_name']." - 360° presentation";
                $this->ovItems[$k]['ov_alt'] = $ovItem['ov_presentation_name']." - 360° presentation";
            } elseif ($ovItem['ov_type'] == 'ovtour') {
                $this->ovItems[$k]['ov_name'] = $ovItem['ov_presentation_name']." - Orbittour";
                $this->ovItems[$k]['ov_alt'] = $ovItem['ov_presentation_name']." - Orbittour";
            }
            $this->ovItems[$k]['ov_resized_thumb'] = OrbitvuShDir::getItemFrontImage($ovItem['folder_name'], $ovItem, (int)$this->ovSettings['thumbnail_width'], (int)$this->ovSettings['thumbnail_height']);

            $this->ovItems[$k]['ov_resized_medium'] = OrbitvuShDir::getItemFrontImage($ovItem['folder_name'], $ovItem, (int)$this->ovSettings['medium_width'], (int)$this->ovSettings['medium_height'], 'best_fit');

            $this->ovItems[$k]['ov_resized_large'] = OrbitvuShDir::getItemFrontImage($ovItem['folder_name'], $ovItem, (int)$this->ovSettings['big_width'], (int)$this->ovSettings['big_height'], 'best_fit');

            /*$this->ovItems[$k]['ov_resized_medium'] = OrbitvuShDir::getItemFrontImage($ovItem['folder_name'], $ovItem, (int)$this->prestaSettings['medium_width'], (int)$this->prestaSettings['medium_height'], 'best_fit');

            $this->ovItems[$k]['ov_resized_large'] = OrbitvuShDir::getItemFrontImage($ovItem['folder_name'], $ovItem, (int)$this->prestaSettings['large_width'], (int)$this->prestaSettings['large_height'], 'best_fit');*/
        }
        $properItems = array();
        foreach ($this->ovItems as $item) {
            if ($item['ov_type'] == 'ov2d' && $this->ovSettings['show_2d']) {
                array_push($properItems, $item);
            } elseif ($item['ov_type'] == 'ov360' && $this->ovSettings['show_360']) {
                array_push($properItems, $item);
            } elseif ($item['ov_type'] == 'ovtour' && $this->ovSettings['show_orbittour']) {
                array_push($properItems, $item);
            }
        }
        return $properItems;
    }

    /**
     * @return array|null
     */
    public function getItems()
    {
        if ($this->allItems === null) {
            $pi = $this->getPrestaItems();
            $oi = $this->getOvItems();
            if ($this->ovSettings['thumbs_position'] === 'prepend') {
                $this->allItems = array_merge($oi ? $oi : array(), $pi ? $pi : array());
            } else {
                $this->allItems = array_merge($pi ? $pi : array(), $oi ? $oi : array());
            }
        }
        return $this->allItems;
    }

    /**
     * This is necessary for seo, because when src is empty (robotos doesn't execute js) of main image, seo tools return errors
     * @return string
     */
    public function getDefaultImage2dSrc()
    {
        $items = $this->getItems();
        foreach ($items as $item) {
            if ($item['ov_type'] == 'ov2d' || $item['ov_type'] == 'ov2d-native') {
                return $item['ov_resized_medium'];
            }
        }
        return '';
    }

    /**
     * Prestashop's specific
     */
    public function getDefaultSelectedItem()
    {
        $items = $this->getItems();

        if (isset($items[0])) {
            if ($items[0]['ov_type'] == 'ov360' || $items[0]['ov_type'] == 'ovtour' || $items[0]['ov_type'] == 'ovvideo') {
                return $items[0]['ov_type'].'-'.(isset($items[0]['id_product_presentation_item']) ? $items[0]['id_product_presentation_item'] : $items[0]['ov_id']);
            } else {
                $cover = Product::getCover($this->product->id);
                //check if product has combinations and find default one
                $attributesGroups = $this->product->getAttributesGroups($this->context->language->id);
                if (is_array($attributesGroups) && $attributesGroups) {
                    $combinationImages = $this->product->getCombinationImages($this->context->language->id);
                    foreach ($attributesGroups as $row) {
                        if ($row['default_on']) {
                            if (isset($combinationImages[$row['id_product_attribute']][0]['id_image'])) {
                                $cover = $combinationImages[$row['id_product_attribute']][0]['id_image'];
                                break;
                            }
                        }
                    }
                }
                if (isset($cover['id_image'])) {
                    $cover = $cover['id_image'];
                }
                foreach ($items as $item) {
                    if ($item['ov_type'] == 'ov2d-native' && $item['ov_id'] == $cover) {
                        return $item['ov_type'].'-'.$cover;
                    }
                }
            }
        }
        return -1;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function hasType($type)
    {
        $check_type = 'ov'.$type;
        $type = 'has'.$type;
        if ($this->{$type} === null) {
            $this->{$type} = false;
            foreach ($this->getItems() as $item) {
                if ($check_type == 'ov2d') {
                    if ($item['ov_type'] === 'ov2d-native' || $item['ov_type'] === 'ov2d' || $item['ov_type'] === 'ov2d-variation') {
                        $this->{$type} = true;
                        break;
                    }
                } else {
                    if ($item['ov_type'] === $check_type) {
                        $this->{$type} = true;
                        break;
                    }
                }
            }
        }
        return $this->{$type};
    }

    public function get360ScriptInjectFunction($item, $isLightbox = false)
    {
        $orbitvuer12Url = ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules/'.ORBITVU_PLUGIN_NAME."/_orbitvu_viewer/orbitvu12/orbitvuer12.swf";
        $expressInstallUrl =((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules/'.ORBITVU_PLUGIN_NAME."/_orbitvu_viewer/orbitvu12/expressInstall.swf";
        $ovusFolderUrl = ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules/'.ORBITVU_PLUGIN_NAME."/_orbitvu_presentations/".rawurlencode($item['folder_name']);

        //teaser
        $teaser = 'static';
        if ($this->ovSettings['teaser'] != 'config_xml') {
            $teaser = $this->ovSettings['teaser'];
        } else {
            //TODO Parse config.xml file
        }

        //gui_style
        $guiStyle = 0;
        if ($this->ovSettings['gui_style'] != 'config_xml') {
            if ($this->ovSettings['gui_style'] == 'round') {
                $guiStyle = 0;
            } elseif ($this->ovSettings['gui_style'] == 'square') {
                $guiStyle = 1;
            } elseif ($this->ovSettings['gui_style'] == 'round_rotation') {
                $guiStyle = 2;
            } elseif ($this->ovSettings['gui_style'] == 'square_rotation') {
                $guiStyle = 3;
            } elseif ($this->ovSettings['gui_style'] == 'no_buttons') {
                $guiStyle = 4;
            }
        } else {
            //TODO Parse config.xml file
        }

        $inject = "inject_orbitvu('".($isLightbox !== false ? 'ovContent-lightbox_' : 'ovContent-').$item['id_product_presentation_item']."', '".$orbitvuer12Url."', '".$expressInstallUrl."', {\n";
        $inject .= "ovus_folder: '".$ovusFolderUrl."',\n";
        $inject .= "content2: 'yes',\n";
        $inject .= "width: 'auto',\n";
        $inject .= "height: 'auto',\n";
        $inject .= "force_html5: '".(($this->ovSettings['viewer_mode'] == 'html5') ? 'yes' : 'no')."',\n";
        $inject .= "teaser: '".$teaser."',\n";
        $inject .= "gui_style: '".$guiStyle."',\n";


        //preload_width
        if ($this->ovSettings['preload_width'] != 0) {
            $inject .= "preload_width: '".(int)$this->ovSettings['preload_width']."',\n";
        }

        //preload_height
        if ($this->ovSettings['preload_height'] != 0) {
            $inject .= "preload_height: '".(int)$this->ovSettings['preload_height']."',\n";
        }

        //mousewheel
        if ($this->ovSettings['mousewheel'] == 0) {
            $inject .= "mousewheel: 'no',\n";
        } else {
            $inject .= "mousewheel: 'yes',\n";
        }

        //frame_rate
        if ($this->ovSettings['frame_rate'] != 0) {
            $inject .= "frame_rate: '".(int)$this->ovSettings['frame_rate']."'\n";
        }

        $inject .= "});";

        return $inject;
    }
}
