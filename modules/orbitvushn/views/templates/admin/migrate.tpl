{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

{if $smarty.const._PS_VERSION_ >= 1.6}
    <div class="alert alert-info">
        <button class="close" data-dismiss="alert" type="button">×</button>
{else}
    <div class="hint" id="hintAzConfig" style="display: block; margin-bottom: 20px;">
        <span style="float:right"><a id="hideWarn" href="javascript:void(0)" onclick="$('#hintAzConfig').remove()"><img src="../img/admin/close.png" alt="X"></a></span>
{/if}
<strong>{l s='We detected that old version (%s) of Orbitvu self hosted module is installed. You can easily migrate your presentations from old module to the new one.'|sprintf:$oldModule.version|escape:'htmlall':'UTF-8' mod='orbitvushn'}</strong>
<br><br><strong>{l s='Migration info:' mod='orbitvushn'}</strong><br>
{l s='Current free disk space: %s'|sprintf:$freeDiskSpaceMB|escape:'htmlall':'UTF-8' mod='orbitvushn'}<br>
{l s='Necessary free disk space: %s'|sprintf:$oldPresentationsSizeMB|escape:'htmlall':'UTF-8' mod='orbitvushn'}
{if ($freeDiskSpaceBytes > $oldPresentationsSizeBytes && $isWritable)}
    <br><br>{l s='Please note that this action may consume a lot of time!'  mod='orbitvushn'}
    <br><strong>{l s='In case something goes wrong, please be sure you made a backup of your shop first!' mod='orbitvushn'}</strong>
    <br><br><a style="cursor:pointer" id="ovmigrate" class="orbitvu-button orbitvu-button-no-margin">{l s='Start migration' mod='orbitvushn'}</a>
{else}
    <br><strong>{l s='You have too little space on you hard drive to perform migration or the folder is not writable!' mod='orbitvushn'}</strong></a>
{/if}

</div>
