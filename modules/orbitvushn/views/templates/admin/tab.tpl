{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

<div class="panel product-tab">
        <!-- TOOLBAR ORBITVU -->
        <div id="ovtoolbar" class="orbitvu-toolbar">

            {if ($orbitvu_connect->isConnected() && !$orbitvu_connect->isNewestVersion())}
            <div class="orbitvu-alert orbitvu-alert-info">
                {l s='Version' mod='orbitvushn'} <span class="orbitvu-new-version">{$orbitvu_connect->getRemoteNewestVersion()|escape:'htmlall':'UTF-8'}</span> {l s='of Orbitvu extension is out! Upgrade now from' mod='orbitvushn'} <span class="orbitvu-new-version">{$orbitvu_connect->getPluginVersion()|escape:'htmlall':'UTF-8'}</span> {l s='to' mod='orbitvushn'} <span class="orbitvu-new-version">{$orbitvu_connect->getRemoteNewestVersion()|escape:'htmlall':'UTF-8'}</span>. <span class="orbitvu-new-version"><a href="https://orbitvu.co/market/products" target="_blank">{l s='Download' mod='orbitvushn'}</a></span> {l s='the newest version from SUN cloud.' mod='orbitvushn'}
                {html_entity_decode($new_version_message|escape:'htmlall':'UTF-8')}
            </div>
            {/if}

            <div id="ovmessages-dynamic"></div>

            <div class="orbitvu-presentation-upload-container">
                <h2 class="orbitvu-header-2">{l s='Upload new presentations' mod='orbitvushn'}</h2>
                <div class="orbitvu-upload-top">
                    <div class="orbitvu-upload-files-container" id="ovupload-files-container"></div>
                    <div class="orbitvu-upload-left">
{l s='Upload new presentation (*.ovus):' mod='orbitvushn'}
                    </div>
                    <div class="orbitvu-upload-right">
                        <input type="file" multiple="multiple" id="ov-upload-input">
                    </div>
                </div>
                <div class="orbitvu-upload-bottom">
                    <div class="orbitvu-upload-left">
{l s='Upload new presentations from your Alphashot Editor:' mod='orbitvushn'}
                    </div>
                    <div class="orbitvu-upload-right">
                        <span class="orbitvu-remote-url-text">{l s='Enter this URL to your Alphashot Editor. Attention! Do not give this URL to anyone!' mod='orbitvushn'}</span>
                        <span class="orbitvu-remote-url">{$ov_settings.remote_upload_url|escape:'htmlall':'UTF-8'}</span>
                    </div>
                </div>
            </div>

            <!-- PRESENTATIONS LINKED CONTAINER -->
            <div id="ovpresentation-linked-list-container" class="orbitvu-presentation-linked-list-container orbitvu-hidden">
                <h2 class="orbitvu-header-2">{l s='Linked presentations' mod='orbitvushn'}</h2>
                <div id="ovlist-linked" class="orbitvu-presentation-linked-list"></div>
            </div>
            <!-- PRESENTATIONS LINKED CONTAINER ENDS -->


            <!-- PRESENTATIONS LIST CONTAINER -->
            <div class="orbitvu-presentation-list-container orbitvu-hidden" id="ovpresentation-list-container">
                <h2 class="orbitvu-header-2">{l s='Click on the presentation to link it with current product' mod='orbitvushn'}</h2>
                <div id="ovpager"></div>
                <div id="ovlist" class="orbitvu-presentation-list"></div>
            </div>
            <!-- PRESENTATIONS LIST CONTAINER ENDS -->

        </div>
        <!-- TOOLBAR ORBITVU ENDS -->
</div>
<script type="text/javascript">
    var orbitvuPresentationsLinked = {$linked_presentations}{* ARRAY, cannot escape*},
        orbitvuPresentationsNotLinked = {$sh_results}{* ARRAY, cannot escape*},
        orbitvuIdProduct = "{html_entity_decode($product->id|escape:'htmlall':'UTF-8')}",
        orbitvuSkuProduct = "{html_entity_decode($product->reference|escape:'htmlall':'UTF-8')}",
        orbitvuMultiLink = {html_entity_decode($ov_settings.multi_link|escape:'htmlall':'UTF-8')},
        orbitvuUploadErrorMessages = {html_entity_decode($upload_error_messages|escape:'htmlall':'UTF-8')};

    ORBITVUSH.app.initAjaxUrl('{html_entity_decode($orbitvu_ajax_url|escape:'htmlall':'UTF-8')}');
    ORBITVUSH.app.init(orbitvuPresentationsLinked, orbitvuPresentationsNotLinked, orbitvuIdProduct, orbitvuSkuProduct, orbitvuMultiLink, orbitvuUploadErrorMessages);
</script>