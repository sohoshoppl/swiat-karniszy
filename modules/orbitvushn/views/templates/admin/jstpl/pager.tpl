{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

{literal}
<script id="orbitvuPager" type="text/ractive">
    <!-- PAGER STARTS -->
    <div class="orbitvu-pager">
        {{#if is_blocked}}
            <div class="orbitvu-ajax-blocker"></div>
        {{elseif (is_blocked_permanently)}}
            <div class="orbitvu-ajax-blocker-permanent"></div>
        {{elseif (is_loading)}}
            <div class="orbitvu-ajax-blocker-loader"><div class="orbitvu-ajax-loader"></div><div class="orbitvu-ajax-blocker"></div></div>
        {{/if}}
        <div class="orbitvu-pager-section">
            <span class="orbitvu-pager-section-text">{/literal}{l s='Page:' mod='orbitvushn'}{literal}</span>
            <span id="ovprev" class="orbitvu-pager-section-arrow {{arrow_prev_disabled}}">&laquo;</span>
            <span class="orbitvu-pager-section-text" id="ovcurrent-page" data-current_page="{{current_page}}">{{current_page}}</span>
            <span id="ovnext" class="orbitvu-pager-section-arrow {{arrow_next_disabled}}">&raquo;</span>
            <span class="orbitvu-pager-section-text">of</span>
            <span id="ovtotal-pages" class="orbitvu-pager-section-text" data-total_pages="{{total_pages}}">{{total_pages}}</span>
        </div>
        <div class="orbitvu-pager-section">
            <span class="orbitvu-pager-section-text">{/literal}{l s='Per page:' mod='orbitvushn'}{literal}</span>
            <select class="orbitvu-pager-section-select" id="ovperpage">
                <option value="20" selected="selected">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="200">200</option>
            </select>
        </div>
        <div class="orbitvu-pager-section">
            <span class="orbitvu-pager-section-text">{/literal}{l s='Total items:' mod='orbitvushn'}{literal}</span>
            <span class="orbitvu-pager-section-text" id="ovtotal-items" data-total_items="{{total_items}}">{{total_items}}</span>
        </div>
        <div class="orbitvu-pager-section">
            <select class="orbitvu-pager-section-select" id="ovsort">
                <option selected="selected" value="-create_date">{/literal}{l s='Date' mod='orbitvushn'}{literal} &dArr;</option>
                <option value="create_date">{/literal}{l s='Date' mod='orbitvushn'}{literal} &uArr;</option>
                <option value="-sku">{/literal}{l s='SKU' mod='orbitvushn'}{literal} &dArr;</option>
                <option value="sku">{/literal}{l s='SKU' mod='orbitvushn'}{literal} &uArr;</option>
                <option value="-name">{/literal}{l s='Name' mod='orbitvushn'}{literal} &dArr;</option>
                <option value="name">{/literal}{l s='Name' mod='orbitvushn'}{literal} &uArr;</option>
            </select>
        </div>
        <div class="orbitvu-pager-section">
            <input type="text" class="orbitvu-pager-section-input" id="ovsearch" placeholder="{/literal}{l s='Presentation name or SKU' mod='orbitvushn'}{literal}">
            <button class="orbitvu-button orbitvu-button-no-margin" id="ovsearch-button">{/literal}{l s='Search' mod='orbitvushn'}{literal}</button>
        </div>
    </div>
    <!-- PAGER ENDS -->
</script>
{/literal}