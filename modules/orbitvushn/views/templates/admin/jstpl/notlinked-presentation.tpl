{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

{literal}
<script id="orbitvuPresentationNotLinked" type="text/ractive">
    <div class="orbitvu-presentation" id="orbitvu-presentation-{{id_presentation}}">
        {{#if is_blocked}}
            <div class="orbitvu-ajax-blocker"></div>
        {{elseif (is_blocked_permanently)}}
            <div class="orbitvu-ajax-blocker-permanent"></div>
        {{elseif (is_loading)}}
            <div class="orbitvu-ajax-blocker-loader"><div class="orbitvu-ajax-loader"></div><div class="orbitvu-ajax-blocker"></div></div>
        {{/if}}
        <div class="orbitvu-presentation-title" title="{{ov_name}}">{{ov_name}}</div>
        <div class="orbitvu-presentation-sku" title="{{ov_sku}}"><span class="orbitvu-presentation-sku-header">SKU: </span> <span>{{ov_sku}}</span></div>
        {{#if !different_hashes}}
            <div class="orbitvu-presentation-image">
                <img {{#if delete_ask}}class="orbitvu-deletion"{{/if}} src="{{admin_image_small}}" alt="{/literal}{l s='Thumbnail:' mod='orbitvushn'}{literal} {{ov_name}}" title="{/literal}{l s='Thumbnail:' mod='orbitvushn'}{literal} {{ov_name}}">
                <div class="orbitvu-delete-ask {{#if !delete_ask}}orbitvu-hidden{{/if}}">
                    Are you sure do you want to delete this presentation?
                    <div class="orbitvu-delete-ask-buttons">
                        <button data-id_presentation="{{id_presentation}}" class="orbitvu-button orbitvu-button-red orbitvu-button-no-margin ovdelete-yes" title="{/literal}{l s='Yes' mod='orbitvushn'}{literal}">{/literal}{l s='Yes' mod='orbitvushn'}{literal}</button>
                        <button data-id_presentation="{{id_presentation}}" class="orbitvu-button orbitvu-button-no-margin ovdelete-no" title="{/literal}{l s='No' mod='orbitvushn'}{literal}">{/literal}{l s='No' mod='orbitvushn'}{literal}</button>
                    </div>
                </div>
            </div>

            <div class="orbitvu-presentation-icons {{#if delete_ask}}orbitvu-hidden{{/if}}">
                {{#if has_2d}}
                    <span class="orbitvu-presentation-ov2d" title="{/literal}{l s='Presentation contains 2D images' mod='orbitvushn'}{literal}"></span>
                {{/if}}
                {{#if has_360}}
                    <span class="orbitvu-presentation-ov360" title="{/literal}{l s='Presentation contains 360° presentation' mod='orbitvushn'}{literal}"></span>
                {{/if}}
                {{#if has_orbittour}}
                    <span class="orbitvu-presentation-ovtour" title="{/literal}{l s='Presentation contains Orbittour' mod='orbitvushn'}{literal}"></span>
                {{/if}}
                {{#if has_video}}
                    <span class="orbitvu-presentation-ovvideo" title="{/literal}{l s='Presentation contains Video' mod='orbitvushn'}{literal}"></span>
                {{/if}}
            </div>
            <div class="orbitvu-presentation-buttons {{#if delete_ask}}orbitvu-hidden{{/if}}">
                <div class="orbitvu-presentation-delete">
                    <button data-id_presentation="{{id_presentation}}" class="orbitvu-button orbitvu-button-red orbitvu-button-no-margin ovdelete" title="{/literal}{l s='Delete presentation' mod='orbitvushn'}{literal}"><span class="orbitvu-button-icon orbitvu-button-icon-delete"></span></button>
                </div>
                <div class="orbitvu-presentation-link">
                    <button data-id_presentation="{{id_presentation}}" class="orbitvu-button orbitvu-button-no-margin ovlink" title="{/literal}{l s='Link presentation to the current product' mod='orbitvushn'}{literal}"><span class="orbitvu-button-icon orbitvu-button-icon-link"></span>{/literal}{l s='Link' mod='orbitvushn'}{literal}</button>
                </div>
            </div>
        {{else}}
            <div class="orbitvu-presentation-corrupted">
                <p class="orbitvu-presentation-corrupted-info">{/literal}{l s='This presentation is corrupted. Please synchronize presentations.' mod='orbitvushn'}{literal}</p>
                <p class="orbitvu-presentation-corrupted-info-instruction">{/literal}{l s='(Settings → Orbitvu SH → Cron Jobs → Auto synchronizing url)' mod='orbitvushn'}{literal}</p>
            </div>
        {{/if}}
    </div>
</script>
{/literal}