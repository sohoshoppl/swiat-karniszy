{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

{literal}
<script id="orbitvuPresentationLinked" type="text/ractive">
    <div class="orbitvu-presentation-dynamic-message" id="ovmessage-dynamic-{{id_presentation}}"></div>
    <div class="orbitvu-presentation-linked" data-id_presentation="{{id_presentation}}" id="ovpresentation-linked-{{id_presentation}}">
        {{#if is_blocked}}
            <div class="orbitvu-ajax-blocker"></div>
        {{elseif (is_blocked_permanently)}}
            <div class="orbitvu-ajax-blocker-permanent"></div>
        {{elseif (is_loading)}}
            <div class="orbitvu-ajax-blocker-loader"><div class="orbitvu-ajax-loader"></div><div class="orbitvu-ajax-blocker"></div></div>
        {{/if}}
        <div class="orbitvu-presentation-linked-title" title="{{ov_name}}">{{ov_name}}</div>
        <div class="orbitvu-presentation-linked-sku" title="{{ov_sku}}">
            <span class="orbitvu-presentation-sku-header">SKU:</span> <span id="ovpresentation-linked-sku-{{id_presentation}}">{{ov_sku}}</span>
        </div>
        <div class="orbitvu-presentation-linked-left">
            {{#if !different_hashes}}
                <div class="orbitvu-presentation-linked-image">
                    <img src="{{admin_image_big}}" alt="{/literal}{l s='Presentation preview thumbnail:' mod='orbitvushn'}{literal} {{ov_name}}" title="{/literal}{l s='Presentation preview thumbnail:' mod='orbitvushn'}{literal} {{ov_name}}">
                    <div class="orbitvu-presentation-icons">
                        {{#if has_2d != 0}}
                            <span class="orbitvu-presentation-ov2d" title="{/literal}{l s='Presentation contains 2D images' mod='orbitvushn'}{literal}"></span>
                        {{/if}}
                        {{#if has_360 != 0}}
                            <span class="orbitvu-presentation-ov360" title="{/literal}{l s='Presentation contains 360° presentation' mod='orbitvushn'}{literal}"></span>
                        {{/if}}
                        {{#if has_orbittour != 0}}
                            <span class="orbitvu-presentation-ovtour" title="{/literal}{l s='Presentation contains Orbittour' mod='orbitvushn'}{literal}"></span>
                        {{/if}}
                    </div>
                </div>
            {{/if}}
        </div>
        <div class="orbitvu-presentation-linked-right">
            <div class="orbitvu-presentation-linked-right-buttons">
                {{#if (different_hashes)}}
                    <div class="orbitvu-alert orbitvu-alert-danger">{/literal}{l s='This presentation is corrupted. Server files are changed or deleted. Please synchronize presentations:<br /><i>(Settings → Orbitvu SH → Cron Jobs → Auto synchronizing url)</i>' mod='orbitvushn'}{literal}</div>
                {{elseif sku_info}}
                    <div class="orbitvu-alert orbitvu-alert-info orbitvu-alert-different-sku">{/literal}{l s='Product\'s "Reference code" field is different than Presentation\'s SKU.' mod='orbitvushn'}{literal}</div>
                {{/if}}
                <button data-id_presentation="{{id_presentation}}" class="orbitvu-button orbitvu-button-red ovunlink"><span class="orbitvu-button-icon orbitvu-button-icon-unlink"></span>{/literal}{l s='Unlink the presentation' mod='orbitvushn'}{literal}</button>
                <button data-id_presentation="{{id_presentation}}" class="orbitvu-button ovactivateall"><span class="orbitvu-button-icon orbitvu-button-icon-activateall"></span>{/literal}{l s='Activate all items' mod='orbitvushn'}{literal}</button>
                <button data-id_presentation="{{id_presentation}}" class="orbitvu-button ovdeactivateall"><span class="orbitvu-button-icon orbitvu-button-icon-deactivateall"></span>{/literal}{l s='Deactivate all items' mod='orbitvushn'}{literal}</button>
            </div>
        </div>
        {{#if !different_hashes}}
            <div class="orbitvu-presentation-linked-bottom">
                <div class="orbitvu-presentation-linked-items-header">{/literal}{l s='Presentation items:' mod='orbitvushn'}{literal}</div>
                <div class="orbitvu-presentation-linked-items">
                    {{#each items:num}}
                        <div id="orbitvu-presentation-linked-item-container-{{id_product_presentation_item}}" class="orbitvu-presentation-linked-item-container {{#if (active == 0)}}orbitvu-presentation-linked-item-container-deactivated{{/if}}" data-order="{{(num * 10) + 10}}">
                            <div class="orbitvu-presentation-linked-item" data-id_product_presentation_item="{{id_product_presentation_item}}" data-order="{{order}}" data-ov_view_url="{{ov_view_url}}" data-ov_type="{{ov_type}}" data-active="{{active}}" data-admin_image="{{admin_image}}" data-ov_name="{{ov_name}}" data-ov_script_url="{{ov_script_url}}" data-ov_id="{{ov_id}}" data-date_added="{{date_added}}" data-date_updated="{{date_updated}}">
                                {{#if (this.is_blocked)}}
                                    <div class="orbitvu-ajax-blocker"></div>
                                {{elseif (this.is_loading)}}
                                    <div class="orbitvu-ajax-blocker-loader"><div class="orbitvu-ajax-loader"></div><div class="orbitvu-ajax-blocker"></div></div>
                                {{/if}}
                                <img class="orbitvu-presentation-linked-item-image" data-thumb_url={{admin_image}} src="{{admin_image}}" alt="{{ov_name}}" title="{{ov_name}}">
                                <div class="orbitvu-presentation-linked-item-actions">

                                    {{#if (this.active != 0)}}
                                        <span class="orbitvu-presentation-actions-deactivate" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Deactivate item' mod='orbitvushn'}{literal}"></span>
                                        <span class="orbitvu-presentation-actions-activate orbitvu-hidden" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Activate item' mod='orbitvushn'}{literal}"></span>
                                    {{else}}
                                        <span class="orbitvu-presentation-actions-deactivate orbitvu-hidden" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Deactivate item' mod='orbitvushn'}{literal}"></span>
                                        <span class="orbitvu-presentation-actions-activate" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Activate item' mod='orbitvushn'}{literal}"></span>
                                    {{/if}}
                                    <span class="orbitvu-presentation-actions-move" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Move item' mod='orbitvushn'}{literal}"></span>
                                    {{#if (ov_type == 'ov2d')}}
                                    <span class="orbitvu-presentation-actions-copy" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Copy to original images' mod='orbitvushn'}{literal}"></span>
                                    {{elseif (ov_type == 'ov360')}}
                                    <span class="orbitvu-presentation-actions-copy" data-id_product_presentation_item="{{id_product_presentation_item}}" title="{/literal}{l s='Copy one frame to original images' mod='orbitvushn'}{literal}"></span>
                                    {{/if}}
                                </div>
                                <div class="orbitvu-presentation-icons">
                                    {{#if (ov_type == 'ov2d')}}
                                        <span class="orbitvu-presentation-{{ov_type}}" title="{/literal}{l s='2D image' mod='orbitvushn'}{literal}"></span>
                                    {{elseif (ov_type == 'ov360')}}
                                        <span class="orbitvu-presentation-{{ov_type}}" title="{/literal}{l s='360° presentation' mod='orbitvushn'}{literal}"></span>
                                    {{elseif (ov_type == 'ovtour')}}
                                        <span class="orbitvu-presentation-{{ov_type}}" title="{/literal}{l s='Orbittour' mod='orbitvushn'}{literal}"></span>
                                    {{/if}}
                                </div>
                            </div>
                        </div>
                    {{/each}}
                </div>
            </div>
        {{/if}}
    </div>
</script>
{/literal}