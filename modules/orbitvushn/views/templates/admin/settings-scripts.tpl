{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

<script type="text/javascript">
    ORBITVUSH.ajax.initAjaxUrl('{html_entity_decode($orbitvu_ajax_url|escape:'htmlall':'UTF-8')}');
    ORBITVUSH.settings.init();
</script>