{*
* Orbitvu PHP  Orbitvu eCommerce administration
*
*  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
*  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code

*  @license   license.pdf
*}

<style type="text/css">

    .orbitvu-gallery-wrapper {
        width: {$template->ovSettings.gallery_width|escape:'html':'UTF-8'};
    }

    .orbitvu-gallery-fix {
        border: 1px solid {$template->ovSettings.main_border_color|escape:'html':'UTF-8'};
        padding: {$template->ovSettings.main_padding|escape:'html':'UTF-8'};
    }

    .orbitvu-gallery {
        padding-bottom: {$template->ratio|escape:'html':'UTF-8'}; /* 16:9 - default */
    }

    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs li.orbitvu-gallery-item {
        margin: {$template->ovSettings.thumbnail_margin|escape:'html':'UTF-8'};
    }

    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs li.orbitvu-gallery-item,
    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs li.orbitvu-gallery-item a {
        width: {$template->ovSettings.thumbnail_width|escape:'html':'UTF-8'};
        height: {$template->ovSettings.thumbnail_height|escape:'html':'UTF-8'};
    }

    /*No thumbnails scroll*/
    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs.orbitvu-image-thumbs-no-scroll {
        margin-left: -{$template->ovSettings.thumbnail_margin|escape:'htmlall':'UTF-8'};
        margin-right: -{$template->ovSettings.thumbnail_margin|escape:'htmlall':'UTF-8'};
    }
    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs.orbitvu-image-thumbs-no-scroll li.orbitvu-gallery-item:first-child,
    .orbitvu-gallery-product-views ul.orbitvu-image-thumbs.orbitvu-image-thumbs-no-scroll li.orbitvu-gallery-item.orbitvu-gallery-item-first {
        margin-left: {$template->ovSettings.thumbnail_margin|escape:'htmlall':'UTF-8'};
    }
    /*No thumbnails scroll ends*/

    .orbitvu-gallery-button {
        width: {$template->ovSettings.button_width|escape:'html':'UTF-8'};
        height: {$template->ovSettings.button_height|escape:'html':'UTF-8'};
    }

    #ovgo-next, #ovgo-back {
        top: calc(50% - {math equation="x/y" x=$template->ovSettings.button_height|intval y=2}px);
    }

    .orbitvu-gallery-view-image img.orbitvu-gallery-main-image.orbitvu-opacity-active {
        opacity: {$template->ovSettings.zoom_magnifier_image_opacity_hover|escape:'html':'UTF-8'};
    }

    .orbitvu-gallery-view-image .orbitvu-gallery-lenses {
        border: 1px solid {$template->ovSettings.zoom_magnifier_lenses_border_color|escape:'html':'UTF-8'};
        box-shadow: {$template->ovSettings.zoom_magnifier_lenses_box_shadow|escape:'html':'UTF-8'};
    }

    .orbitvu-gallery-zoomer-zoom {
        border: 1px solid {$template->ovSettings.zoom_magnifier_zoom_border_color|escape:'html':'UTF-8'};
        background: {$template->ovSettings.zoom_magnifier_zoom_background|escape:'html':'UTF-8'};
        box-shadow: {$template->ovSettings.zoom_magnifier_zoom_box_shadow|escape:'html':'UTF-8'};
    }

    /*Prestashop's specific - this has to be here because of quick view*/
    /*
    #image-block,
    #views_block {
        display: none !important;
    }*/
</style>
<div id="ovgallery-presta" style="display: none">
    {if (count($template->getItems()) > 0)}
    <div id="ovgallery-wrapper" class="orbitvu-gallery-wrapper">
      <div id="ovgallery-scroller" class="orbitvu-gallery-scroller">
            <div id="ovgallery-product-views" class="orbitvu-gallery-product-views">
                <ul id="ovimage-thumbs" class="orbitvu-image-thumbs {if ($template->ovSettings.scroll|escape:'htmlall':'UTF-8' != 'yes')}orbitvu-image-thumbs-no-scroll{/if}">
                    {foreach from=$template->getItems() key=k  item=item}
                    <li class="orbitvu-gallery-item">
                        <a class="orbitvu-gallery-item-link" data-big_src="{$item.ov_resized_large|escape:'htmlall':'UTF-8'}" data-src="{$item.ov_resized_medium|escape:'htmlall':'UTF-8'}" data-id="{if isset($item.id_product_presentation_item)}{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}{/if}" data-ov_type="{$item.ov_type|escape:'htmlall':'UTF-8'}" data-ov_slide_id="ovlightbox-slide-{$item.ov_type|escape:'htmlall':'UTF-8'}-{if isset($item.id_product_presentation_item)}{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}{else}{$item.ov_id|escape:'htmlall':'UTF-8'}{/if}" id="{$item.ov_type|escape:'htmlall':'UTF-8'}-{if isset($item.id_product_presentation_item)}{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}{else}{$item.ov_id|escape:'htmlall':'UTF-8'}{/if}">
                        <img class="lazyload" data-src="{$item.ov_resized_thumb|escape:'htmlall':'UTF-8'}" alt="{$item.ov_alt|escape:'htmlall':'UTF-8'}" title="{$item.ov_name|escape:'htmlall':'UTF-8'}">
                        <div class="orbitvu-icon {$item.ov_type|escape:'htmlall':'UTF-8'}"></div>
                        </a>
                    </li>
                    {/foreach}
                </ul>
                <button type="button" id="ovgo-back" aria-label="Previous" class="orbitvu-gallery-button orbitvu-hidden">
                    <svg width="24" height="30">
                        <polyline points="16 9 9 15 16 21" stroke="rgba(49,51,61,1)" stroke-width="3" stroke-linecap="butt"
                                  fill="none" stroke-linejoin="round"></polyline>
                    </svg>
                </button>
                <button type="button" id="ovgo-next" aria-label="Previous" class="orbitvu-gallery-button orbitvu-hidden">
                    <svg width="24" height="30">
                        <polyline points="9 9 16 15 9 21" points="" stroke="rgba(49,51,61,1)" stroke-width="3" stroke-linecap="butt"
                                  fill="none" stroke-linejoin="round"></polyline>
                    </svg>
                </button>
            </div>
        </div>        
        <div id="ovgallery-fix" class="orbitvu-gallery-fix">
            <div id="ovgallery" class="orbitvu-gallery">
                <div id="ovgallery-zoomer-zoom" class="orbitvu-gallery-zoomer-zoom orbitvu-hidden {$template->galleryZoomerZoomClass|escape:'htmlall':'UTF-8'}">
                    <div class="orbitvu-gallery-zoomer-zoom-inner">
                        <div id="ovspinner-zoomer-zoom-image" class="orbitvu-spinner">
                            <div class="orbitvu-spinner-inner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ovgallery-product-image" class="orbitvu-gallery-product-image">
                    {if $template->hasType('2d')}
                    <div id="ovgallery-view-image" class="orbitvu-gallery-view-image">
                        <div id="ovspinner-main-image" class="orbitvu-spinner">
                            <div class="orbitvu-spinner-inner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                        <img class="lazyload" id="ovgallery-main-image" class="orbitvu-gallery-main-image {$template->galleryMainImageClass|escape:'htmlall':'UTF-8'}" data-src="{$template->getDefaultImage2dSrc()|escape:'htmlall':'UTF-8'}" alt="{$template->product->name|escape:'html':'UTF-8'}" title="{$template->product->name|escape:'html':'UTF-8'}">
                        <div id="ovgallery-lenses" class="orbitvu-gallery-lenses orbitvu-hidden"></div>
                        <div id="ovgallery-zoomer" class="orbitvu-gallery-zoomer orbitvu-hidden {$template->galleryZoomerClass|escape:'htmlall':'UTF-8'}"></div>
                    </div>
                    {/if}
                    {if ($template->hasType('360'))}
                    <div id="ovgallery-view-360" class="orbitvu-gallery-view-360">
                        {foreach from=$template->getItems() key=k  item=item}
                            {if ($item.ov_type == 'ov360')}
                            <div class="ov360-item-container" id="ov360-item-container-{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}">
                                <div id="ovContent-{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}" class="orbitvu-container"></div>
                            </div>
                            {/if}
                        {/foreach}
                    </div>
                    {/if}
                </div>
            </div>
        </div>
  
        <!--Prestashop's specific -->
        <div id="ovdisplay-images" class="orbitvu-display-images orbitvu-hidden">
            <span class="orbitvu-display-images-text"><i class="orbitvu-icon-repeat"></i>{l s='Display all pictures' mod='orbitvushn'}</span>
        </div>
        <!-- Prestashop's specific ends -->
    </div>
    {else}
    <div id="ovgallery-wrapper" class="orbitvu-gallery-wrapper">
        <div id="ovgallery-fix" class="orbitvu-gallery-fix">
            <div id="ovgallery" class="orbitvu-gallery">
                <img class="lazyload" itemprop="image" data-src="{$img_prod_dir|escape:'htmlall':'UTF-8'}{$lang_iso|escape:'htmlall':'UTF-8'}-default-large_default.jpg" id="bigpic" alt="" title="{$template->product->name|escape:'html':'UTF-8'}" width="{$template->prestaSettings.large_width|escape:'htmlall':'UTF-8'}" height="{$template->prestaSettings.large_height|escape:'htmlall':'UTF-8'}"/>
            </div>
        </div>
    </div>
    {/if}
</div>
<!-- LIGHTBOX -->
{if ($template->ovSettings.lightbox_enabled !== '0')}

    <style type="text/css">
        {if ($template->ovSettings.lightbox_thumbnails_enabled !== '0')}
            .orbitvu-lightbox-slider {
                height: calc(100% - 80px);
            }

            .orbitvu-lightbox-button-prev,
            .orbitvu-lightbox-button-next {
                top: calc(50% - 75px);
            }
                {if ($template->ovSettings.lightbox_buttons_shadow_enabled !== '0')}
                    .orbitvu-lightbox-button:focus,
                    .orbitvu-lightbox-button:hover {
                        background-color: rgba(80, 80, 80, 0.3);
                    }
                {/if}
        {/if}
    </style>

    <div role="dialog" id="ovlightbox-overlay" class="orbitvu-lightbox-overlay {if ($template->ovSettings.lightbox_colorized_bg !== '0')}orbitvu-lightbox-overlay-bgcolorized{/if}">

        {if ($template->ovSettings['lightbox_colorized_bg'] !== '0')}
            <div id="ovlightbox-bgcolorized" class="orbitvu-lightbox-bgcolorized"></div>
        {/if}

        <div id="ovlightbox-zoomed" class="orbitvu-lightbox-zoomed">

        </div>

        <div id="ovlightbox-slider" class="orbitvu-lightbox-slider">
            {foreach from=$template->getItems() key=k  item=item}
                <div class="orbitvu-lightbox-slide" data-ov_slide_id="ovlightbox-slide-{$item.ov_type|escape:'htmlall':'UTF-8'}-{if (isset($item.id_product_presentation_item))}{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}{else}{$item.ov_id|escape:'htmlall':'UTF-8'}{/if}" data-ov_type="{$item.ov_type|escape:'htmlall':'UTF-8'}" data-id="{if (isset($item.id_product_presentation_item))}{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}{/if}" data-alt="{$item.ov_alt|escape:'htmlall':'UTF-8'}" data-title="{$item.ov_name|escape:'htmlall':'UTF-8'}" data-lightbox_src="{$item.ov_resized_large|escape:'htmlall':'UTF-8'}">
                    <div class="orbitvu-lightbox-slide-inner orbitvu-lightbox-slide-inner-{$item.ov_type|escape:'htmlall':'UTF-8'}">
                        {if ($item['ov_type'] == 'ov360')}
                            <div id="ovContent-lightbox_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}" class="orbitvu-container"></div>
                        {elseif ($item['ov_type'] == 'ovtour')}
                            <div id="orbittour-lightbox_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}" class="orbitvu-container"></div>
                        {elseif ($item['ov_type'] == 'ovvideo')}
                            <div id="ovvideo-lightbox_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}" class="orbitvu-video-container"></div>
                        {else}
                            <div class="orbitvu-spinner">
                                <div class="orbitvu-spinner-inner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
            {/foreach}
        </div>
        {if ($template->ovSettings.lightbox_thumbnails_enabled !== '0')}
            <div id="ovlightbox-thumbs" class="orbitvu-lightbox-thumbs">
                <div id="ovlightbox-thumbs-inner" class="orbitvu-lightbox-thumbs-inner orbitvu-no-buttons">
                    <div id="ovlightbox-thumbs-list-container" class="orbitvu-lightbox-thumbs-list-container">
                        <ul id="ovlightbox-thumbs-list" class="orbitvu-lightbox-thumbs-list">
                            {foreach from=$template->getItems() key=k  item=item}
                                <li data-ov_slide_id="ovlightbox-slide-{$item.ov_type|escape:'html':'UTF-8'}-{if (isset($item.id_product_presentation_item))}{$item.id_product_presentation_item|escape:'html':'UTF-8'}{else}{$item.ov_id|escape:'html':'UTF-8'}{/if}" data-ov_type="{$item.ov_type|escape:'html':'UTF-8'}" data-id="{if (isset($item.id_product_presentation_item))}{$item.id_product_presentation_item|escape:'html':'UTF-8'}{/if}"><img class="orbitvu-lightbox-thumbs-image lazyload" data-src="{$item.ov_resized_thumb|escape:'html':'UTF-8'}" alt="{$item.ov_alt|escape:'html':'UTF-8'}" title="{$item.ov_name|escape:'html':'UTF-8'}"></li>
                            {/foreach}
                        </ul>
                    </div>
                    <button type="button" id="ovlightbox-thumbs-button-prev" aria-label="Previous" class="orbitvu-lightbox-thumbs-button orbitvu-lightbox-thumbs-button-prev orbitvu-hidden">
                        <svg width="22" height="30">
                            <polyline points="15 5 5 15 15 25" stroke="rgba(255,255,255,0.5)" stroke-width="2" stroke-linecap="butt"
                                      fill="none" stroke-linejoin="round"></polyline>
                        </svg>
                    </button>
                    <button type="button" id="ovlightbox-thumbs-button-next" aria-label="Previous" class="orbitvu-lightbox-thumbs-button orbitvu-lightbox-thumbs-button-next orbitvu-hidden">
                        <svg width="22" height="30">
                            <polyline points="7 5 17 15 7 25" stroke="rgba(255,255,255,0.5)" stroke-width="2" stroke-linecap="butt"
                                      fill="none" stroke-linejoin="round"></polyline>
                        </svg>
                    </button>
                </div>
            </div>
        {/if}
        <button type="button" id="ovlightbox-prev-button" aria-label="Previous" class="orbitvu-lightbox-button orbitvu-lightbox-button-prev">
            <svg width="44" height="60">
                <polyline points="30 10 10 30 30 50" stroke="rgba(255,255,255,0.5)" stroke-width="2" stroke-linecap="butt"
                          fill="none" stroke-linejoin="round"></polyline>
            </svg>
        </button>
        <button type="button" id="ovlightbox-next-button" aria-label="Next" class="orbitvu-lightbox-button orbitvu-lightbox-button-next">
            <svg width="44" height="60">
                <polyline points="14 10 34 30 14 50" stroke="rgba(255,255,255,0.5)" stroke-width="2" stroke-linecap="butt"
                          fill="none" stroke-linejoin="round"></polyline>
            </svg>
        </button>
        <button type="button" id="ovlightbox-close-button" aria-label="Close" class="orbitvu-lightbox-button orbitvu-lightbox-button-close">
            <svg width="30" height="30">
                <g stroke="rgba(255,255,255,0.5)" stroke-width="2">
                    <line x1="5" y1="5" x2="25" y2="25"></line>
                    <line x1="5" y1="25" x2="25" y2="5"></line>
                </g>
            </svg>
        </button>
    </div>
{/if}
<!-- LIGHTBOX ENDS -->
<!-- Prestashop's specific -->
<script type="text/javascript">
    var $ovpImageBlock = $('#image-block2'),
        $ovpViewsBlock = $('#views_block2'),
        $ovgalleryPresta = $('#ovgallery-presta'),
        $orbitvuHtml = $ovgalleryPresta.html(), //copy plugins gallery html
        $ovgallery; //we have to assign this var after inserting plugin html to the Presetashop's gallery

    $ovgalleryPresta.remove(); //remove plugins gallery container
    $ovpImageBlock.css('display', 'none'); //hide native presta image block
    $ovpViewsBlock.css('display', 'none'); //hide native presta thumbnails block
    $ovpImageBlock.before($orbitvuHtml); //insert plugins gallery into Prestashop's gallery
    $ovgallery = $('#ovgallery'); //now we can assign ovgallery div

    //Preserve layout
    $('.new-box', $ovpImageBlock).prependTo($ovgallery).css('z-index', 9999);
    $('.sale-box', $ovpImageBlock).prependTo($ovgallery).css('z-index', 9999);
    $('.discount', $ovpImageBlock).prependTo($ovgallery).css('z-index', 9999);

    $ovpImageBlock.remove(); //delete Prestashop's gallery html
    $ovpViewsBlock.remove(); //delete Prestashop's gallery thumbnails html
</script>
<!-- Prestashop's specific ends -->

{if (count($template->getItems()) > 0)}
    <!-- Embeed viewer -->
    <script type="text/javascript" src="{$viewer_url|escape:'htmlall':'UTF-8'}swfobject.js"></script>
    <script type="text/javascript" src="{$viewer_url|escape:'htmlall':'UTF-8'}orbitvu.js"></script>

    {foreach from=$template->getItems() item=item}
        {if ($item.ov_type == 'ov360')}
            <script type="text/javascript">
                function inject_viewer_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}() {
                    {$template->get360ScriptInjectFunction($item)}{* ARRAY, cannot escape*}
                }
                {if ($template->ovSettings.lightbox_enabled !== '0')}
                    function inject_viewer_lightbox_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}() {
                        {$template->get360ScriptInjectFunction($item, 1)}{* ARRAY, cannot escape*}
                    }
                {/if}
            </script>
        {/if}
    {/foreach}

    <script type="text/javascript">
        var ovData = [],
            ovDefaultSelectedItemId = '{$template->getDefaultSelectedItem()|escape:'htmlall':'UTF-8'}'; //Prestashop's specific for combinations

        {foreach from=$template->getItems() item=item}
            {if ($item.ov_type == 'ov360') || ($item.ov_type == 'ovtour')}
                ovData.push({
                    id: '{$item.ov_type|escape:'htmlall':'UTF-8'}-{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}',
                    inject: function() {
                        if (typeof this.inject.counter == 'undefined') {
                            this.inject.counter = 0;
                        {if ($item.ov_type == 'ov360')}
                            inject_viewer_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}();
                        {/if}
                        }
                        this.inject.counter++;
                    },
                    {if ($template->ovSettings.lightbox_enabled !== '0')}
                    injectLightbox: function() {
                        if (typeof this.injectLightbox.counter == 'undefined') {
                            {if ($item.ov_type == 'ov360')}
                                inject_viewer_lightbox_{$item.id_product_presentation_item|escape:'htmlall':'UTF-8'}();
                            {/if}
                        }
                        this.injectLightbox.counter++;
                    }
                    {/if}
                });
            {/if}
        {/foreach}

        ORBITVUSH.gallery.init({
            presentationData: ovData,
            thumbs: {
                scrollEnabled: {if ($template->ovSettings.scroll == 'yes')}1{else}0{/if},
                hoverDelay: {$template->ovSettings.thumbnail_hover_delay|escape:'html':'UTF-8'}
            },
            zoomMagnifier: {
                enabled: {if ($template->ovSettings.zoom_magnifier_enabled !== '0')}1{else}0{/if},
                lensesVisible: {$template->ovSettings.zoom_magnifier_lenses|escape:'html':'UTF-8'},
                maxZoomEnabled: {$template->ovSettings.zoom_magnifier_maxzoom_enabled|escape:'html':'UTF-8'},
                maxZoomWidth: {$template->ovSettings.zoom_magnifier_maxzoom_width|escape:'html':'UTF-8'},
                maxZoomHeight: {$template->ovSettings.zoom_magnifier_maxzoom_height|escape:'html':'UTF-8'},
            },
            lightbox: {
                enabled: {if ($template->ovSettings.lightbox_enabled !== '0')}1{else}0{/if},
                colorizedBg: {$template->ovSettings.lightbox_colorized_bg|escape:'html':'UTF-8'},
                thumbnailsEnabled: {$template->ovSettings.lightbox_thumbnails_enabled|escape:'html':'UTF-8'},
                proportions: '{$template->ovSettings.lightbox_proportions|escape:'html':'UTF-8'}',
                maxZoomEnabled: {$template->ovSettings.lightbox_maxzoom_enabled|escape:'html':'UTF-8'},
                maxZoomWidth: {$template->ovSettings.lightbox_maxzoom_width|escape:'html':'UTF-8'},
                maxZoomHeight: {$template->ovSettings.lightbox_maxzoom_height|escape:'html':'UTF-8'},
            }
        }, ovDefaultSelectedItemId);

        /*****************************************************
         * Prestashop's specific (for combination attributes)
         *****************************************************/
        var $ovDisplayImages = $('#ovdisplay-images'),
            ovRefreshProductImagesLock,
            ovPrevIdProductAttribute = null,
            ovPsVersion = '{$orbitvu_ps_version|escape:'htmlall':'UTF-8'}';


        /**
         * Compare two versions (as PHP version_compare)
         * @param strV1
         * @param strV2
         * @returns number
         */
        function orbitvuCompVersions(strV1, strV2) {
            var nRes = 0
                    , parts1 = strV1.split('.')
                    , parts2 = strV2.split('.')
                    , nLen = Math.max(parts1.length, parts2.length);

            for (var i = 0; i < nLen; i++) {
                var nP1 = (i < parts1.length) ? parseInt(parts1[i], 10) : 0
                        , nP2 = (i < parts2.length) ? parseInt(parts2[i], 10) : 0;

                if (isNaN(nP1)) { nP1 = 0; }
                if (isNaN(nP2)) { nP2 = 0; }

                if (nP1 != nP2) {
                    nRes = (nP1 > nP2) ? 1 : -1;
                    break;
                }
            }

            return nRes;
        };

        /**
         * Override default Prestashop's function for combination attributes
         * @param id_product_attribute
         * @param ovrLock
         */
        function refreshProductImages(idProductAttribute, ovRefreshLock) {
            if (ovRefreshProductImagesLock && !ovRefreshLock) {
                //Prestashop 1.5
                if (orbitvuCompVersions(ovPsVersion, '1.6') == -1 && ovPrevIdProductAttribute == 0) {
                    // setTimeout(function() {
                    refreshProductImages(idProductAttribute, true);
                    // } , 1);
                    return;
                } else {
                    return;
                }
            }
            if (typeof(idProductAttribute) != 'undefined'){
                ovPrevIdProductAttribute = idProductAttribute;
            }
            ovRefreshProductImagesLock = true;

            setTimeout(function() {
                ovRefreshProductImagesLock = false;
            } , 500);
            idProductAttribute = parseInt(idProductAttribute);

            //we set combination
            if (idProductAttribute > 0 && typeof(combinationImages) != 'undefined' && typeof(combinationImages[idProductAttribute]) != 'undefined') {
                var $ovImageThumbs = $('#ovimage-thumbs'),
                    visibleIds = [],
                    $liItem,
                    $aItem,
                    thumbsObj;

                $ovImageThumbs.find('a.orbitvu-gallery-item-link').each(function() {
                    $liItem = $(this).closest('li.orbitvu-gallery-item');
                    $liItem.removeClass('orbitvu-gallery-item-first');

                    if ($(this).data('ov_type') != 'ov2d-native') {
                        $liItem.addClass('orbitvu-hidden');
                    } else {
                        var id = parseInt($(this).attr('id').replace('ov2d-native-', ''));
                        if (combinationImages[idProductAttribute].indexOf(id) !== -1) {
                            $liItem.removeClass('orbitvu-hidden');
                            visibleIds.push(id);
                        } else {
                            $liItem.addClass('orbitvu-hidden');
                        }
                    }
                });
                if (visibleIds.length) {
                    $aItem = $('a#ov2d-native-' + visibleIds[0]);
                    $aItem.closest('li.orbitvu-gallery-item').addClass('orbitvu-gallery-item-first');
                    ORBITVUSH.gallery.refreshView($aItem);
                    thumbsObj = ORBITVUSH.gallery.getThumbs();
                    thumbsObj.updateScroll();
                    $ovDisplayImages.removeClass('orbitvu-hidden');
                }
            } else {
                $ovDisplayImages.trigger('click');
            }
        }

        //Show all images (not only combination images)
        $ovDisplayImages.on('click', function() {
            var $ovImageThumbs = $('#ovimage-thumbs'), thumbsObj;

            $(this).addClass('orbitvu-hidden');
            $ovImageThumbs.find('li.orbitvu-gallery-item').each(function() {
                $(this).removeClass('orbitvu-hidden').removeClass('orbitv-gallery-item-first');
                $(this).find('a.orbitvu-gallery-item-link:first').removeClass('orbitvu-hidden');
            });
            thumbsObj = ORBITVUSH.gallery.getThumbs();
            thumbsObj.updateScroll();
            /*ORBITVUSH.scroller.restore();
            ORBITVUSH.scroller.dispatchButtonsVisibility();*/
        });
    </script>
{/if}
