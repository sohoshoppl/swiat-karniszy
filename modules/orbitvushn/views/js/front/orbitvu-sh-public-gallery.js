/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function($) {
    ORBITVUSH.namespace('gallery');
    ORBITVUSH.namespace('gallery.thumbs');
    ORBITVUSH.namespace('gallery.zoomMagnifier');
    ORBITVUSH.namespace('gallery.lightbox');
    ORBITVUSH.namespace('gallery.lightboxThumbs');

    ORBITVUSH.gallery = (function () {
        var
            _itemHoverTimeoutId,

            _settings = { //all initialization params
                spinnerFadeInTime: 1000,
            },

            _cssFilterSupport = true, //detects if css3 filter is supported

            _$dom, //DOM objects

            _thumbs = ORBITVUSH.gallery.thumbs = (function () {
                var _currentTranslateX = 0;

                /**
                 * Determines if buttons should be visible in thumbs list
                 * @returns {boolean}
                 * @private
                 */
                function _shouldEnableButtons() {
                    if (_$dom.ovGalleryScroller.outerWidth() < _$dom.ovImageThumbs.outerWidth()) {
                        return true;
                    }
                    return false;
                }

                return {
                    /**
                     * Scroll to next images
                     * @private
                     */
                    scrollNextAllThumbs: function() {
                        var tmpTranslateX = (_currentTranslateX - _$dom.ovGalleryScroller.outerWidth());
                        if ((Math.abs(tmpTranslateX) + _$dom.ovGalleryScroller.outerWidth()) >= _$dom.ovImageThumbs.outerWidth()) {
                            _$dom.ovGoNext.addClass('orbitvu-hidden');
                            tmpTranslateX = -(_$dom.ovImageThumbs.outerWidth() - _$dom.ovGalleryScroller.outerWidth());
                        }
                        _$dom.ovGoBack.removeClass('orbitvu-hidden');
                        _currentTranslateX = tmpTranslateX;
                        _$dom.ovImageThumbs.css('transform', 'translate3d(' + tmpTranslateX + 'px, 0, 0)');
                    },

                    /**
                     * Scroll to previous images
                     * @private
                     */
                    scrollPrevAllThumbs: function() {
                        var tmpTranslateX = (_currentTranslateX + _$dom.ovGalleryScroller.outerWidth());
                        if (tmpTranslateX >= 0) {
                            _$dom.ovGoBack.addClass('orbitvu-hidden');
                            tmpTranslateX = 0;
                        }
                        _$dom.ovGoNext.removeClass('orbitvu-hidden');
                        _currentTranslateX = tmpTranslateX;
                        _$dom.ovImageThumbs.css('transform', 'translate3d(' + tmpTranslateX + 'px, 0, 0)');
                    },

                    /**
                     * Updates thumbs list
                     */
                    updateScroll: function() {
                        var $active = _$dom.ovImageThumbs.find('li.orbitvu-active');
                        if ($active.length && _shouldEnableButtons()) {
                            var thumbW = $active.outerWidth(true),
                                index = $active.index(),
                                tmpTranslateX = (thumbW * index) - (_$dom.ovGalleryScroller.outerWidth()/2) + (thumbW/2);

                            if (tmpTranslateX < 0) {
                                tmpTranslateX = 0;
                            } else if ((_$dom.ovImageThumbs.outerWidth() - tmpTranslateX) < _$dom.ovGalleryScroller.outerWidth()) {
                                tmpTranslateX = tmpTranslateX - (_$dom.ovGalleryScroller.outerWidth() - (_$dom.ovImageThumbs.outerWidth() - tmpTranslateX));
                            }
                            tmpTranslateX = parseInt(tmpTranslateX); //to prevent image blurr on float translations
                            _currentTranslateX = -tmpTranslateX;
                            _$dom.ovImageThumbs.css('transform', 'translate3d(' + (-tmpTranslateX) + 'px, 0, 0)');

                            if (tmpTranslateX <= 0) {
                                _$dom.ovGoBack.addClass('orbitvu-hidden');
                            } else {
                                _$dom.ovGoBack.removeClass('orbitvu-hidden');
                            }

                            if (_$dom.ovImageThumbs.outerWidth() - tmpTranslateX <= _$dom.ovGalleryScroller.outerWidth()) {
                                _$dom.ovGoNext.addClass('orbitvu-hidden');
                            } else {
                                _$dom.ovGoNext.removeClass('orbitvu-hidden');
                            }
                        } else {
                            _$dom.ovImageThumbs.css('transform', 'translate3d(0, 0, 0)');
                            _$dom.ovGoBack.addClass('orbitvu-hidden');
                            _$dom.ovGoNext.addClass('orbitvu-hidden');
                        }
                    }
                }
            }()),

            _zoomMagnifier = ORBITVUSH.gallery.zoomMagnifier = (function () {
                var _lensesSizeFactor,
                    _zoomerZoomInside = false,
                    _initialWindowWidth = $(window).width(),
                    _ePageX, _ePageY;

                /**
                 * Counts zoomer div position
                 * @returns {{}}
                 * @private
                 */
                function _getZoomerZoomPosition() {
                    var pos = {}, mainImageDim = _$dom.ovGalleryMainImage[0].getBoundingClientRect();

                    pos.x = _$dom.ovGalleryMainImage.position().left + mainImageDim.width + 10;
                    pos.y = _$dom.ovGalleryMainImage.position().top;

                    if (_$dom.ovGalleryMainImage.offset().left + (2 * mainImageDim.width) + 10 >= $('body').outerWidth()) {
                        pos.x = _$dom.ovGalleryMainImage.position().left;
                        _zoomerZoomInside = true;
                    } else {
                        _zoomerZoomInside = false;
                    }

                    return pos;
                }


                /**
                 * Displays zoomer zoom popup
                 * @param width
                 * @param height
                 * @private
                 */
                function _showZoomerZoom(width, height) {
                    var position = _getZoomerZoomPosition();

                    if (!_$dom.ovGalleryZoomerZoom.find('#ovgallery-zoomer-zoom-image').length) {
                        _$dom.ovGalleryZoomerZoom.find('.orbitvu-gallery-zoomer-zoom-inner:first').append(_$dom.ovGalleryZoomerZoomImage);
                    }

                    if (_$dom.ovGalleryZoomerZoom.hasClass('orbitvu-hidden')) {
                        _$dom.ovGalleryZoomerZoom.removeClass('orbitvu-hidden');
                        _$dom.ovGalleryZoomerZoom.css({
                            width: 10 + 'px',
                            height: 10 + 'px',
                            left: (_$dom.ovGalleryMainImage.position().left + _$dom.ovGalleryMainImage[0].getBoundingClientRect().width/2) + 'px',
                            top: (_$dom.ovGalleryMainImage.position().top + _$dom.ovGalleryMainImage[0].getBoundingClientRect().height/2) + 'px'
                        });
                        _$dom.ovGalleryZoomerZoom.animate({
                            opacity: 1,
                            width: width + 'px',
                            height: height + 'px',
                            left: position.x + 'px',
                            top: position.y + 'px'
                        });
                        if (_zoomerZoomInside) {
                            _$dom.ovGalleryZoomerZoom.addClass('')
                        }
                    }
                }

                /**
                 * Sets initial lenses factor when big image is loaded in zoomerZoom
                 * @private
                 */
                function _setInitialLensesFactor() {
                    var wFactor,
                        hFactor;

                    if (_settings.zoomMagnifier.maxZoomEnabled && (_settings.zoomMagnifier.maxZoomWidth < _$dom.ovGalleryZoomerZoomImage[0].naturalWidth && _settings.zoomMagnifier.maxZoomHeight < _$dom.ovGalleryZoomerZoomImage[0].naturalHeight)) {
                        wFactor =  _$dom.ovGalleryMainImage[0].getBoundingClientRect().width/_settings.zoomMagnifier.maxZoomWidth;
                        hFactor =  _$dom.ovGalleryMainImage[0].getBoundingClientRect().height/_settings.zoomMagnifier.maxZoomHeight;
                    } else {
                        wFactor =  _$dom.ovGalleryMainImage[0].getBoundingClientRect().width/_$dom.ovGalleryZoomerZoomImage[0].naturalWidth;
                        hFactor =  _$dom.ovGalleryMainImage[0].getBoundingClientRect().height/_$dom.ovGalleryZoomerZoomImage[0].naturalHeight
                    }

                    _lensesSizeFactor = {
                        current: wFactor > hFactor ? wFactor : hFactor,
                        min: wFactor > hFactor ? wFactor : hFactor,
                        max: 1
                    }
                }

                /**
                 * Adds initial dimensions for lenses
                 * @private
                 */
                function _prepareLenses() {

                    if (!_lensesSizeFactor) { //it means that big image is not loaded yes
                        return false;
                    } else {
                        _$dom.ovGalleryLenses.css({
                            width: Math.round(_$dom.ovGalleryMainImage[0].getBoundingClientRect().width * _lensesSizeFactor.current) + 'px',
                            height: Math.round(_$dom.ovGalleryMainImage[0].getBoundingClientRect().height * _lensesSizeFactor.current) + 'px',
                        });

                        if (_settings.zoomMagnifier.lensesVisible) {
                            if (!_$dom.ovGalleryLenses.find('#ovgallery-lenses-image').length) {
                                _$dom.ovGalleryLenses.append(_$dom.ovGalleryLensesImage);
                            }
                            _$dom.ovGalleryLensesImage.attr('src', _$dom.ovGalleryMainImage.attr('src')).css({
                                width: _$dom.ovGalleryMainImage[0].getBoundingClientRect().width + 'px',
                                height: _$dom.ovGalleryMainImage[0].getBoundingClientRect().height + 'px'
                            });
                        }
                        return true;
                    }
                }

                /**
                 * Retrieves necessary data for leneses
                 * @param ePageX
                 * @param ePageY
                 * @returns {{pos_x: number, pos_y: number}|*}
                 * @private
                 */
                function _getLensesData(ePageX, ePageY) {
                    var mainImageOffset =  _$dom.ovGalleryMainImage.offset(),
                        mainImagePosition = _$dom.ovGalleryMainImage.position(),
                        mainImageSize = {
                            width: _$dom.ovGalleryMainImage[0].getBoundingClientRect().width,
                            height: _$dom.ovGalleryMainImage[0].getBoundingClientRect().height
                        },
                        lensesSize = {
                            width: _$dom.ovGalleryLenses[0].getBoundingClientRect().width,
                            height: _$dom.ovGalleryLenses[0].getBoundingClientRect().height,
                            half_width: _$dom.ovGalleryLenses[0].getBoundingClientRect().width/2,
                            half_height: _$dom.ovGalleryLenses[0].getBoundingClientRect().height/2
                        },
                        data = {
                            pos_x: 0,
                            pos_y: 0,
                            main_image_width: mainImageSize.width,
                            main_image_height: mainImageSize.height
                        };

                    if ((mainImageOffset.left  >= ePageX -1 && mainImageOffset.left <= ePageX + 1)
                        || (ePageX - lensesSize.half_width < mainImageOffset.left)) {
                        data.pos_x = mainImagePosition.left;
                    } else if ((mainImageOffset.left + mainImageSize.width >= ePageX -1 && mainImageOffset.left + mainImageSize.width <= ePageX + 1)
                        || (ePageX + lensesSize.half_width > mainImageOffset.left + mainImageSize.width)) {
                        data.pos_x = mainImagePosition.left + mainImageSize.width - lensesSize.width;
                    } else {
                        data.pos_x = ePageX - mainImageOffset.left + mainImagePosition.left - lensesSize.half_width
                    }

                    if ((mainImageOffset.top  >= ePageY -1 && mainImageOffset.top <= ePageY + 1)
                        || (ePageY - lensesSize.half_height < mainImageOffset.top)) {
                        data.pos_y = mainImagePosition.top;
                    } else if ((mainImageOffset.top + mainImageSize.height >= ePageY -1 && mainImageOffset.top + mainImageSize.height <= ePageY + 1)
                        || (ePageY + lensesSize.half_height > mainImageOffset.top + mainImageSize.height)) {
                        data.pos_y = mainImagePosition.top + mainImageSize.height - lensesSize.height;
                    } else {
                        data.pos_y = ePageY - mainImageOffset.top + mainImagePosition.top - lensesSize.half_height
                    }
                    return data;
                }

                /**
                 * Sets Zoomer image position and dimensions
                 */
                function _setZoomerImageData(data) {
                    var transformValue = 'translate3d(' + data.pos_x + 'px, ' + data.pos_y + 'px,0px)' +
                        '  scale(' + (data.width/_$dom.ovGalleryZoomerZoomImage[0].naturalWidth) + ')';
                    _$dom.ovGalleryZoomerZoomImage.css('transform', transformValue);

                    /*_$dom.ovGalleryZoomerZoomImage.css({
                     width: data.width + 'px',
                     height: data.height + 'px',
                     left: data.pos_x + 'px',
                     top: data.pos_y + 'px'
                     });*/
                }

                /**
                 * Retrieves data for zoomerZoom image
                 * @returns {{}}
                 * @private
                 */
                function _getZoomerZoomImageData() {
                    var factor, factor2, data = {}, mainImageDim = _$dom.ovGalleryMainImage[0].getBoundingClientRect();

                    if (!_lensesSizeFactor) { //it means that big image is not loaded yet
                        return {};
                    }

                    factor = (_$dom.ovGalleryZoomerZoomImage[0].naturalWidth * _lensesSizeFactor.current)/mainImageDim.width;
                    factor2 = (_$dom.ovGalleryZoomerZoomImage[0].naturalHeight * _lensesSizeFactor.current)/mainImageDim.height;
                    factor = (factor > factor2) ? factor : factor2;

                    //count Image proportions
                    data.width = _$dom.ovGalleryZoomerZoomImage[0].naturalWidth/factor;
                    data.height = _$dom.ovGalleryZoomerZoomImage[0].naturalHeight/factor;

                    /*data.pos_x = -(data.width/mainImageDim.width) * (_$dom.ovGalleryLenses.offset().left -_$dom.ovGalleryZoomer.offset().left);
                     data.pos_y = -(data.height/mainImageDim.height) * (_$dom.ovGalleryLenses.offset().top -_$dom.ovGalleryZoomer.offset().top);*/

                    data.xMiddle = (((data.width - mainImageDim.width)/2) + 1) * -1; //+1 because of border 1px in left and right in _$dom.ovGalleryZoomerZoom
                    data.yMiddle = (((data.height - mainImageDim.height)/2) + 1) * -1; //+1 because of border 1px _$dom.ovGalleryZoomerZoom top and bottom in _$dom.ovGalleryZoomerZoom

                    data.lensesOffsetLeftMiddle = (mainImageDim.width - _$dom.ovGalleryLenses.width())/2;
                    data.lensesOffsetTopMiddle = (mainImageDim.height - _$dom.ovGalleryLenses.height())/2;

                    data.lensesCurrentOffsetLeft = (_$dom.ovGalleryLenses.offset().left -_$dom.ovGalleryZoomer.offset().left);
                    data.lensesCurrentOffsetTop = (_$dom.ovGalleryLenses.offset().top -_$dom.ovGalleryZoomer.offset().top);

                    if (data.width <= mainImageDim.width) {
                        data.pos_x = data.xMiddle;
                    } else {
                        data.pos_x = (data.xMiddle * data.lensesCurrentOffsetLeft)/data.lensesOffsetLeftMiddle;
                    }

                    if (data.height <= mainImageDim.height) {
                        data.pos_y = data.yMiddle;
                    } else {
                        data.pos_y = (data.yMiddle * data.lensesCurrentOffsetTop)/data.lensesOffsetTopMiddle;
                    }

                    return data;
                }

                /**
                 * Inject zoomer zoom image and sets its initial position
                 * @private
                 */
                function _injectZommerZoomImage() {
                    var $a = _$dom.ovImageThumbs.find('a.orbitvu-active:first');

                    _$dom.ovSpinnerZoomerZoom.stop().hide();
                    _$dom.ovSpinnerZoomerZoom.fadeIn(_settings.spinnerFadeInTime);
                    _$dom.ovGalleryZoomerZoomImage.stop().addClass('orbitvu-dispatch');
                    _$dom.ovGalleryZoomerZoomImage.one('load', function() {
                        _$dom.ovSpinnerZoomerZoom.stop().hide();
                        $(this).removeClass('orbitvu-hidden');
                        _setInitialLensesFactor();
                        _zoomMagnifier.buildZoomer(_ePageX, _ePageY); //to display immediately lenses
                    }).attr('src', $a.data('big_src'));
                }

                return {
                    /**
                     * Adds initial dimensions and position for zoomer
                     * @private
                     */
                    prepareZoomer: function() {
                        if (_$dom.ovGalleryMainImage.length) {
                            _$dom.ovGalleryZoomer.css({
                                width: _$dom.ovGalleryMainImage[0].getBoundingClientRect().width + 'px',
                                height: _$dom.ovGalleryMainImage[0].getBoundingClientRect().height + 'px',
                                left: _$dom.ovGalleryMainImage.position().left + 'px',
                                top: _$dom.ovGalleryMainImage.position().top + 'px'
                            });
                            _$dom.ovGalleryZoomer.removeClass('orbitvu-hidden');
                        }
                    },

                    /**
                     * Builds and moves the zoomer and zoomer image
                     * @param lensesPosX
                     * @param lensesPoxY
                     * @private
                     */
                    buildZoomer: function(lensesPosX, lensesPosY) {
                        var lensesData, zoomerPosition = _$dom.ovGalleryZoomer.position(), lensesPosition, zoomerZoomImageData;

                        _ePageX = lensesPosX;
                        _ePageY = lensesPosY;

                        _showZoomerZoom(_$dom.ovGalleryMainImage[0].getBoundingClientRect().width, _$dom.ovGalleryMainImage[0].getBoundingClientRect().height);
                        if (_prepareLenses()) {
                            _$dom.ovGalleryLenses.removeClass('orbitvu-hidden');
                            lensesData = _getLensesData(lensesPosX, lensesPosY);
                            _$dom.ovGalleryLenses.css({
                                left: (_lensesSizeFactor.current == 1 ? _$dom.ovGalleryMainImage.position().left : lensesData.pos_x ) + 'px',
                                top: (_lensesSizeFactor.current == 1 ? _$dom.ovGalleryMainImage.position().top : lensesData.pos_y ) + 'px'
                            });
                            if (_settings.zoomMagnifier.lensesVisible) {
                                lensesPosition = _$dom.ovGalleryLenses.position();
                                _$dom.ovGalleryLensesImage.css({
                                    left: -(lensesPosition.left - zoomerPosition.left + 1) + 'px',
                                    top: -(lensesPosition.top - zoomerPosition.top + 1) + 'px'
                                });
                            }
                        }

                        if (_settings.zoomMagnifier.lensesVisible && !_zoomerZoomInside) {
                            _$dom.ovGalleryMainImage.addClass('orbitvu-opacity-active');
                        } else if (_settings.zoomMagnifier.lensesVisible && _zoomerZoomInside) {
                            _$dom.ovGalleryLenses.css('opacity', '0');
                        } else if (!_settings.zoomMagnifier.lensesVisible) {
                            _$dom.ovGalleryLenses.css('opacity', '0');
                        }

                        if (!_$dom.ovGalleryZoomerZoomImage.hasClass('orbitvu-dispatch')) {
                            _injectZommerZoomImage(lensesPosX, lensesPosY);
                        } else {
                            zoomerZoomImageData = _getZoomerZoomImageData();
                            if (zoomerZoomImageData) {
                                _setZoomerImageData(zoomerZoomImageData);
                            }
                        }
                    },

                    /**
                     * Hides zoomer
                     * @private
                     */
                    seekZoomer: function() {
                        _$dom.ovGalleryLenses.addClass('orbitvu-hidden').css('opacity', '1');
                        _$dom.ovGalleryZoomer.addClass('orbitvu-hidden');
                        _$dom.ovGalleryZoomerZoom.addClass('orbitvu-hidden').css('opacity', '0');
                        _$dom.ovGalleryZoomerZoomImage.addClass('orbitvu-hidden');
                        _$dom.ovGalleryZoomerZoomImage.removeClass('orbitvu-dispatch');
                        _$dom.ovGalleryZoomerZoomImage.attr('src', '');
                        _$dom.ovGalleryMainImage.removeClass('orbitvu-opacity-active');
                        _lensesSizeFactor = false;
                    },

                    /**
                     * Check whether zoomerZoom is inside main image
                     * @returns {boolean}
                     */
                    isZoomerZoomInside: function() {
                        return _zoomerZoomInside;
                    },

                    /**
                     * Sets proper Zoomer and lenses data on mousescroll
                     * @param zoomerPosition
                     * @param event
                     * @returns {boolean}
                     */
                    setZoomerAndLensesOnScroll: function(zoomerPosition, event) {
                        var lensesData, lensesPosition, zoomerZoomImageData;

                        if (!_lensesSizeFactor) {
                            return false;
                        }

                        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                            _lensesSizeFactor.current -= 0.05;
                        } else {
                            _lensesSizeFactor.current += 0.05;
                        }

                        if (_lensesSizeFactor.current >= _lensesSizeFactor.max) {
                            _lensesSizeFactor.current = _lensesSizeFactor.max;
                        }

                        if (_lensesSizeFactor.current < _lensesSizeFactor.min) {
                            _lensesSizeFactor.current = _lensesSizeFactor.min;
                        }

                        _$dom.ovGalleryLenses.css({
                            width: Math.round(_$dom.ovGalleryMainImage[0].getBoundingClientRect().width * _lensesSizeFactor.current) + 'px',
                            height: Math.round(_$dom.ovGalleryMainImage[0].getBoundingClientRect().height * _lensesSizeFactor.current) + 'px',
                        });

                        lensesData = _getLensesData(event.pageX, event.pageY);
                        _$dom.ovGalleryLenses.css({
                            left: (_lensesSizeFactor.current == 1 ? _$dom.ovGalleryMainImage.position().left : lensesData.pos_x ) + 'px',
                            top: (_lensesSizeFactor.current == 1 ? _$dom.ovGalleryMainImage.position().top : lensesData.pos_y ) + 'px'
                        });

                        if (_settings.zoomMagnifier.lensesVisible) {
                            lensesPosition = _$dom.ovGalleryLenses.position();
                            _$dom.ovGalleryLensesImage.css({
                                left: -(lensesPosition.left - zoomerPosition.left + 1) + 'px',
                                top: -(lensesPosition.top - zoomerPosition.top + 1) + 'px'
                            });
                        }
                        zoomerZoomImageData = _getZoomerZoomImageData();
                        if (zoomerZoomImageData) {
                            _setZoomerImageData(zoomerZoomImageData);
                        }
                    },

                    /**
                     * Retrieves initial window width
                     * @returns {*}
                     */
                    getInitialWindowWidth: function() {
                        return _initialWindowWidth;
                    },

                    /**
                     * Sets initial window width
                     * @param windowWidth
                     */
                    setInitialWindowWidth: function(windowWidth) {
                        _initialWindowWidth = windowWidth;
                    }

                }
            }()),

            _lightbox = ORBITVUSH.gallery.lightbox = (function () {

                //determines if lightbox is in zoom mode
                var _zoomedData = {
                        isZoomed: false, //checks whether lightbox image is zoomed
                        isZoomedImageMovable: false, //checks whether lightbox image is movable while zoomed
                        isClosing: false, //checks whether lightbox zoomed image is closing right now
                        $baseImage: null, //image which is clicked to open zoom
                        $image: $("<img>"), //zoomed image object
                        helper: {} //some helper data for calculations
                    }
                    ;

                /**
                 * Retrieves lightbox proportions
                 * @returns {{width: *, height: *}}
                 * @private
                 */
                function _countLightboxProportions() {
                    var tmpProportions = _settings.lightbox.proportions.split("/");
                    return {
                        width: tmpProportions[0],
                        height: tmpProportions[1]
                    }
                }

                /**
                 * Set the translate x axis to display proper image
                 * @param slideId
                 * @param shouldAnimate
                 * @private
                 */
                function _slideLightbox(slideId, shouldAnimate) {
                    //clear videos
                    _clearVideoLightbox();

                    //remove active from all slides
                    _$dom.ovLightboxSlider.find('.orbitvu-lightbox-slide').removeClass('orbitvu-active');

                    //check if should animate
                    if (shouldAnimate) {
                        _$dom.ovLightboxSlider.addClass('orbitvu-animate-slide');
                    } else {
                        _$dom.ovLightboxSlider.removeClass('orbitvu-animate-slide');
                    }

                    //find proper slide and display image or inject proper orbitvu for lightbox
                    _$dom.ovLightboxSlider.find('.orbitvu-lightbox-slide').each(function(i) {
                        var $slide = $(this);
                        if (slideId == $slide.data('ov_slide_id')) {
                            $slide.addClass('orbitvu-active');
                            if (_settings.lightbox.thumbnailsEnabled) {
                                var $lightboxThumbs = $('#ovlightbox-thumbs-list');
                                $lightboxThumbs.find('li').removeClass('orbitvu-active');
                                $lightboxThumbs.find('li').each(function() {
                                    if ($(this).data('ov_slide_id') == slideId) {
                                        $(this).addClass('orbitvu-active');
                                        _lightboxThumbs.updateScroll();
                                    }
                                });
                            }
                            _$dom.ovLightboxSlider.css('transform', 'translate3d(-' + (i * 100) + '%, 0px, 0px)');
                            if ($slide.data('ov_type') == 'ov360') {
                                _injectProperLightboxOrbitvu(slideId.replace('ovlightbox-slide-', ''));
                            } else {
                                if (shouldAnimate) {
                                    setTimeout(function() {
                                        _injectImage2dLightbox($slide);
                                    }, 400);
                                } else {
                                    _injectImage2dLightbox($slide);
                                }
                            }
                            setTimeout(function() {
                                _$dom.ovLightboxSlider.removeClass('orbitvu-animate-slide'); //to prevent transition from coming back from fullscreen
                            }, 400);
                            return false;
                        }
                        i++;
                    });
                }

                /**
                 * Stops and clears the video content
                 */
                function _clearVideoLightbox() {
                    var $activeSlide = _$dom.ovLightboxSlider.find('.orbitvu-lightbox-slide.orbitvu-active:first');
                    if ($activeSlide.length && $activeSlide.data('ov_type') == 'ovvideo') {
                        $activeSlide.find('.orbitvu-video-container:first').html('');
                    }
                }

                /**
                 * Take cares about injecting proper function to display presentation or tour in lightbox mode
                 * @param slideId
                 * @returns {boolean}
                 * @private
                 */
                function _injectProperLightboxOrbitvu(slideId) {
                    for(var i in _settings.presentationData) {
                        var item = _settings.presentationData[i];
                        if (item.id == slideId) {
                            item.injectLightbox();
                            if (_settings.lightbox.colorizedBg) {
                                $('a.orbitvu-gallery-item-link').each(function(){
                                    if ($(this).data('ov_slide_id') == 'ovlightbox-slide-' + slideId) {
                                        var $img = $('<img>'),
                                            src = $(this).data('big_src');
                                        $img.on('load', function() {
                                            _createColorizedBg(src);
                                        }).attr('src', src);
                                    }
                                });
                            }
                            return true;
                        }
                    }
                    return false;
                }

                /**
                 * Inject proper image to lightbox slide
                 * @param $slide
                 * @private
                 */
                function _injectImage2dLightbox($slide) {
                    var $spinnerSlide, $img, $slideInner;

                    if ($slide.find('.orbitvu-lightbox-image').length == 0) {
                        $spinnerSlide = $slide.find('.orbitvu-spinner:first');
                        $slideInner = $slide.find('.orbitvu-lightbox-slide-inner:first');
                        $spinnerSlide.stop(true, true).hide();
                        $spinnerSlide.fadeIn(_settings.spinnerFadeInTime);
                        $img = $('<img class="orbitvu-lightbox-image" alt="' + $slide.data('alt') + '" title="' + $slide.data('title') + '">');
                        $img.on('load', function() {
                            $spinnerSlide.stop(true, true).hide();
                            $slideInner.html($(this));
                            if (_settings.lightbox.colorizedBg) {
                                _createColorizedBg($slide.data('lightbox_src'));
                            }
                            //enable tap and mouseenter events
                            _bindImage2dZoomEvents($(this));
                        }).attr('src', $slide.data('lightbox_src'));
                    } else {
                        if (_settings.lightbox.colorizedBg) {
                            _createColorizedBg($slide.data('lightbox_src'));
                        }
                    }
                }

                /**
                 * Binds proper events to lightbox images for zooming purposes
                 * @param $img
                 * @private
                 */
                function _bindImage2dZoomEvents($img) {
                    //enable tap
                    $img.hammer({});
                    $img.on('tap', function(e) {
                        if (_isImage2dZoomable($(this))) {
                            _zoomedData.$baseImage = $(this),
                                _zoomedData.$image.attr('src', $(this).attr('src'));
                            _createImage2dZoom(e.gesture.center);
                        }
                    });

                    //add proper zoom cursor
                    $img.on('mouseenter', function() {
                        if (_isImage2dZoomable($img)) {
                            $(this).addClass('orbitvu-lightbox-cursor');
                        }
                    });

                    //remove zoom cursor
                    $img.on('mouseleave', function() {
                        $(this).removeClass('orbitvu-lightbox-cursor');
                    });
                }

                /**
                 * Checks whether lightbox image is zoomable
                 * @param $img
                 * @returns {boolean}
                 * @private
                 */
                function _isImage2dZoomable($img) {
                    var currentWidth = $img[0].getBoundingClientRect().width,
                        currentHeight = $img[0].getBoundingClientRect().height,
                        imageNaturalWidth = $img[0].naturalWidth,
                        imageNaturalHeight = $img[0].naturalHeight;

                    if ((currentWidth < imageNaturalWidth) || (currentHeight < imageNaturalHeight)) {
                        return true;
                    }
                    return false;
                }

                /**
                 * Calculates initial zoomed image data to create nice zoom transisiton
                 * @returns {*}
                 * @private
                 */
                function _calculateInitialZoomedImageData() {
                    return {
                        posX: -((_zoomedData.$baseImage[0].getBoundingClientRect().width - window.innerWidth)/2),
                        posY: -((_zoomedData.$baseImage[0].getBoundingClientRect().height - window.innerHeight)/2 + (_settings.lightbox.thumbnailsEnabled ? $('#ovlightbox-thumbs').outerHeight()/2 : 0)),
                        scaleX: _zoomedData.$baseImage[0].getBoundingClientRect().width/_zoomedData.$baseImage[0].naturalWidth,
                        scaleY: _zoomedData.$baseImage[0].getBoundingClientRect().height/_zoomedData.$baseImage[0].naturalHeight
                    }
                }

                /**
                 * Creates zoom effect transition for 2d image
                 * @param ev
                 * @private
                 */
                function _createImage2dZoom(ev) {
                    //create zoom
                    var data = _calculateInitialZoomedImageData(),
                        ratio,
                        ratioX,
                        ratioY,
                        factor,
                        zoomedWidth,
                        zoomedHeight,
                        scaleX = 1,
                        scaleY = 1,
                        cursorPosX = ev.x,
                        cursorPosY = ev.y;

                    $('body').addClass('orbitvu-lightbox-cursor-out');
                    _applyZoomedImageTransform(data); //add start transform
                    _zoomedData.$image.addClass('orbitvu-zoom-in');
                    _$dom.ovLightboxZoomed.html(_zoomedData.$image); //append big image to zoomed area
                    _$dom.ovLightboxSlider.addClass('orbitvu-visibility-hidden'); //hide lightbox slider, visibility hidden is necessary for calculations
                    _$dom.ovLightboxNextButton.addClass('orbitvu-hidden'); //hide next button orbitvu-hidden performs faster thatn visibility hidden
                    _$dom.ovLightboxPrevButton.addClass('orbitvu-hidden'); //hide prev button
                    _$dom.ovLightboxCloseButton.addClass('orbitvu-hidden'); //hide close button
                    if (_settings.lightbox.thumbnailsEnabled) {
                        $('#ovlightbox-thumbs').addClass('orbitvu-visibility-hidden'); //hide lightbox thumbnails
                    }

                    if (_settings.lightbox.maxZoomEnabled && (_settings.lightbox.maxZoomWidth < _zoomedData.$baseImage[0].naturalWidth && _settings.lightbox.maxZoomHeight < _zoomedData.$baseImage[0].naturalHeight)) {
                        ratioX = _zoomedData.$baseImage[0].naturalWidth/_settings.lightbox.maxZoomWidth;
                        ratioY = _zoomedData.$baseImage[0].naturalHeight/_settings.lightbox.maxZoomHeight;
                        ratio = (ratioX > ratioY) ? ratioX : ratioY;
                        zoomedWidth = _zoomedData.$baseImage[0].naturalWidth/ratio;
                        zoomedHeight = _zoomedData.$baseImage[0].naturalHeight/ratio;
                        scaleX = zoomedWidth/_zoomedData.$baseImage[0].naturalWidth;
                        scaleY = zoomedHeight/_zoomedData.$baseImage[0].naturalHeight;
                    } else {
                        zoomedWidth = _zoomedData.$baseImage[0].naturalWidth;
                        zoomedHeight = _zoomedData.$baseImage[0].naturalHeight;
                        scaleX = 1;
                        scaleY = 1;
                    }

                    factor = _lightbox.calculateFactor(zoomedWidth, zoomedHeight);

                    _zoomedData.helper.zoomedImageCurrentData = {
                        posX: window.innerWidth < zoomedWidth ? (cursorPosX/factor.x) : -((zoomedWidth - window.innerWidth)/2),
                        posY: window.innerHeight < zoomedHeight ? (cursorPosY/factor.y) : -((zoomedHeight - window.innerHeight)/2),
                        scaleX: scaleX,
                        scaleY: scaleY
                    };

                    //add end tranistion to make a nice zoom effect
                    setTimeout(function() {
                        _applyZoomedImageTransform(_zoomedData.helper.zoomedImageCurrentData);
                        setTimeout(function() {
                            _zoomedData.isZoomed = true;
                            _zoomedData.$image.removeClass('orbitvu-zoom-in');
                        }, 200); //remove transition on mousemove (css 200ms)
                    }, 50);
                }

                /**
                 * Applies css transform
                 * @param data
                 * @private
                 */
                function _applyZoomedImageTransform(data) {
                    _zoomedData.$image.css('transform', 'translate3d(' + data.posX + 'px, ' + data.posY + 'px, 0) scale(' + data.scaleX +', ' + data.scaleY + ')');
                }

                /**
                 * Calcualtes factor how much to move zoomed Image
                 * @param zoomedWidth
                 * @param zoomedHeight
                 * @returns {{x: number, y: number}}
                 * @private
                 */
                function _calculateFactor(zoomedWidth, zoomedHeight) {
                    var tmpCenterX = (window.innerWidth - zoomedWidth)/2;
                    var tmpCenterY = (window.innerHeight - zoomedHeight)/2;
                    var windowCenterX = window.innerWidth/2;
                    var windowCenterY = window.innerHeight/2;
                    return {
                        x: windowCenterX/tmpCenterX,
                        y: windowCenterY/tmpCenterY
                    }
                }

                /**
                 * Creates colorized background for image
                 * @param imageSrc
                 * @private
                 */
                function _createColorizedBg(imageSrc) {
                    if (_cssFilterSupport) {
                        var $bg = $('#ovlightbox-bgcolorized'),
                            $newimg = $('<img>'),
                            $img = $bg.find('img:first');

                        if ($img.length < 1) {
                            $newimg.attr('src', imageSrc);
                            $bg.html($newimg);
                        } else {
                            $img.attr('src', imageSrc);
                        }
                    } else {
                        var $bg = $('#ovlightbox-bgcolorized'),
                            $tmpImg = $('<img>').attr('src', imageSrc),
                            svgString = '' +
                                '<svg id="mySVG" width="' + $tmpImg[0].naturalWidth + '" height="' + $tmpImg[0].naturalHeight + '">' +
                                '<filter id="orbitvuBlur"><feGaussianBlur in="SourceGraphic" stdDeviation="20" /></filter>' +
                                '<image filter="url(#orbitvuBlur)" xlink:href="' + imageSrc + '" x="0" y="0" height="100%" width="100%"/>' +
                                '</svg>',
                            $newimg = $(svgString);

                        $bg.html($newimg);
                    }
                    _setColorizedBgPositon();
                }

                /**
                 * Sets proper position for colorized background when window resizes
                 * @private
                 */
                function _setColorizedBgPositon() {
                    var $bg = $('#ovlightbox-bgcolorized'),
                        $img = _cssFilterSupport ? $bg.find('img:first') : $bg.find('svg:first'),
                        windowWidth = window.innerWidth,
                        windowHeight = window.innerHeight,
                        imageWidth,
                        imageHeight,
                        pos = {};

                    if ($img.length) {
                        imageWidth = _cssFilterSupport ? $img[0].naturalWidth : $img.attr('width');
                        imageHeight = _cssFilterSupport ? $img[0].naturalHeight : $img.attr('height');

                        if (windowWidth < imageWidth) {
                            pos.left = -(imageWidth - windowWidth)/2 + "px";
                            pos.marginLeft = "0";
                            pos.marginRight = "0";
                        } else {
                            pos.left = 0 + 'px';
                            pos.marginLeft = "auto";
                            pos.marginRight = "auto";
                        }

                        if (windowHeight < imageHeight) {
                            pos.top = -(imageHeight - windowHeight)/2 + "px";
                            pos.marginTop = "0";
                            pos.marginBottom = "0";
                        } else {
                            pos.top = 0 + 'px';
                            pos.marginTop = "auto";
                            pos.marginBottom = "auto";
                        }
                    }
                    $img.css({
                        left: pos.left,
                        top: pos.top,
                        marginLeft: pos.marginLeft,
                        marginRight: pos.marginRight,
                        marginTop: pos.marginTop,
                        marginBottom: pos.marginBottom
                    });
                }

                return {
                    /**
                     * Show overlay and and proper lightbox image
                     * @param slideId
                     */
                    showLightbox: function(slideId) {
                        if ($('body').length) {
                            $('body').addClass('orbitvu-overflow-hidden');
                        }
                        if (_settings.lightbox.colorizedBg) {

                        }
                        _$dom.ovLightboxOverlay.addClass('orbitvu-lightbox-overlay-visible');
                        _zoomMagnifier.seekZoomer();
                        _slideLightbox(slideId, false);
                    },

                    /**
                     * Close lightbox
                     */
                    closeLightbox: function() {
                        if ($('body').length) {
                            $('body').removeClass('orbitvu-overflow-hidden');
                        }
                        _clearVideoLightbox();
                        _$dom.ovLightboxOverlay.removeClass('orbitvu-lightbox-overlay-visible');
                    },

                    /**
                     * Go to the next slide
                     */
                    goNextSlideLightbox: function() {
                        var $activeSlide = _$dom.ovLightboxSlider.find('.orbitvu-lightbox-slide.orbitvu-active:first');
                        if ($activeSlide.next('.orbitvu-lightbox-slide').length) {
                            _slideLightbox($activeSlide.next('.orbitvu-lightbox-slide').data('ov_slide_id'), true);
                        } else {
                            _$dom.ovLightboxSlider.addClass('orbitvu-bounce-from-right');
                            setTimeout(function() {
                                _$dom.ovLightboxSlider.removeClass('orbitvu-bounce-from-right');
                            }, 400);
                        }
                    },

                    /**
                     * Go to the previous slide
                     */
                    goPrevSlideLightbox: function() {
                        var $activeSlide = _$dom.ovLightboxSlider.find('.orbitvu-lightbox-slide.orbitvu-active:first');
                        if ($activeSlide.prev('.orbitvu-lightbox-slide').length) {
                            _slideLightbox($activeSlide.prev('.orbitvu-lightbox-slide').data('ov_slide_id'), true);
                        } else {
                            _$dom.ovLightboxSlider.addClass('orbitvu-bounce-from-left');
                            setTimeout(function() {
                                _$dom.ovLightboxSlider.removeClass('orbitvu-bounce-from-left');
                            }, 400);
                        }
                    },

                    /**
                     * Set proper lightbox slide dimensions
                     */
                    setLightboxSlideDimensions: function() {
                        var proportions = _countLightboxProportions();

                        if (_settings.lightbox.thumbnailsEnabled) {
                            $('.orbitvu-lightbox-slide-inner').css({
                                width: (0.8 * _$dom.ovLightboxSlider.outerWidth()) + 'px',
                                height: ((proportions.height/proportions.width) * (0.8 * _$dom.ovLightboxSlider.outerWidth())) + 'px',
                                "max-width": ((proportions.width/proportions.height) * (0.8 * _$dom.ovLightboxSlider.outerHeight())) + 'px',
                                "max-height": (0.8 * _$dom.ovLightboxSlider.outerHeight()) + 'px' //default max 80% of viewport height
                            });
                        } else {
                            $('.orbitvu-lightbox-slide-inner').css({
                                width: '80vw', //default max 80% of viewport vidth
                                height: ((proportions.height/proportions.width) * 80) + 'vw', //$height/$width * 80
                                maxWidth: ((proportions.width/proportions.height) * 80) + 'vh', //16/9 * 80
                                maxHeight: '80vh' //default max 80% of viewport height
                            });
                        }
                        if (_settings.lightbox.colorizedBg) {
                            _setColorizedBgPositon();
                        }
                    },

                    /**
                     * Wrapper for private method _slideLightbox
                     * @param slideId
                     * @param shouldAnimate
                     */
                    slideLightbox: function(slideId, shouldAnimate) {
                        _slideLightbox(slideId, shouldAnimate);
                    },

                    /**
                     * Centers zoomed image on window resize
                     */
                    centerImage2dZoomedPosition: function() {
                        if (_zoomedData.isZoomed) {
                            _zoomedData.helper.zoomedImageCurrentData.posX = (window.innerWidth - _zoomedData.$image[0].getBoundingClientRect().width)/2;
                            _zoomedData.helper.zoomedImageCurrentData.posY = (window.innerHeight - _zoomedData.$image[0].getBoundingClientRect().height)/2;
                            _applyZoomedImageTransform(_zoomedData.helper.zoomedImageCurrentData);
                        }
                    },

                    /**
                     * Calculates factor for moving zoomed image
                     * @param zoomedWidth
                     * @param zoomedHeight
                     * @returns {{x, y}|{x: number, y: number}}
                     */
                    calculateFactor: function(zoomedWidth, zoomedHeight) {
                        return _calculateFactor(zoomedWidth, zoomedHeight);
                    },

                    /**
                     * Sets proper position for zoomed image on mouse/touch move
                     * @param data
                     */
                    setImage2dZoomedPosition: function(data) {
                        if (_zoomedData.isZoomed) {
                            _zoomedData.helper.zoomedImageCurrentData.posX = data.posX;
                            _zoomedData.helper.zoomedImageCurrentData.posY = data.posY;
                            _zoomedData.helper.zoomedImageCurrentData.scaleX = data.scaleX;
                            _zoomedData.helper.zoomedImageCurrentData.scaleY = data.scaleY;
                            _applyZoomedImageTransform(_zoomedData.helper.zoomedImageCurrentData);
                        }
                    },

                    /**
                     * Checks wheter zoomed image 2d is movable
                     * @returns {boolean}
                     */
                    isZoomedImageMovable: function() {
                        if (_zoomedData.isZoomed) {
                            if (window.innerWidth < _zoomedData.$image[0].getBoundingClientRect.width || window.innerHeight < _zoomedData.$image[0].getBoundingClientRect().height) {
                                return true
                            }
                        }
                        return false;
                    },

                    /**
                     * Return zoomed Image 2d object data
                     * @returns {{isZoomed: boolean, isZoomedImageMovable: boolean, $baseImage: null, $image: (*|HTMLElement), helper: {}}}
                     */
                    getImage2dZoomedObj: function() {
                        return _zoomedData;
                    },

                    /**
                     * Closes zoomed Imaged 2d
                     */
                    closeZoomedImage2d: function() {
                        _zoomedData.$image.addClass('orbitvu-zoom-out');
                        _$dom.ovLightboxNextButton.removeClass('orbitvu-hidden');
                        _$dom.ovLightboxPrevButton.removeClass('orbitvu-hidden');
                        _$dom.ovLightboxCloseButton.removeClass('orbitvu-hidden');
                        if (_settings.lightbox.thumbnailsEnabled) {
                            $('#ovlightbox-thumbs').removeClass('orbitvu-visibility-hidden');
                        }
                        _zoomedData.isClosing = true;
                        //_lightbox.setLightboxSlideDimensions(); //this is necessary to prevent disappearing slide-inner
                        _applyZoomedImageTransform(_calculateInitialZoomedImageData());
                        setTimeout(function() {
                            $('body').removeClass('orbitvu-lightbox-cursor-out');
                            _$dom.ovLightboxSlider.removeClass('orbitvu-visibility-hidden');
                            _$dom.ovLightboxZoomed.html('');
                            _zoomedData = {
                                isZoomed: false, //checks whether lightbox image is zoomed
                                isZoomedImageMovable: false, //checks whether lightbox image is movable while zoomed
                                isClosing: false, //checks whether lightbox zoomed image is closing right now
                                $baseImage: null, //image which is clicked to open zoom
                                $image: $("<img>"), //zoomed image object
                                helper: {} //some helper data for calculations
                            }
                        }, 200);
                    }
                }
            }()),

            _lightboxThumbs = ORBITVUSH.gallery.lightboxThumbs = (function () {
                var _currentTranslateX = 0;

                /**
                 * Determines if buttons should be visible in lightbox thumbs list
                 * @returns {boolean}
                 * @private
                 */
                function _shouldEnableButtons() {
                    if (_$dom.ovLightboxThumbsList.outerWidth() > (_$dom.ovLightboxThumbsListContainer.outerWidth() + (_$dom.ovLightboxThumbsInner.hasClass('orbitvu-no-buttons') ? 0 : 60))) { //60 because buttons next and prev are 30px width
                        return true;
                    }
                    return false;
                }

                return {
                    /**
                     * Scroll to next images in thumbs list
                     * @private
                     */
                    scrollNextAllThumbs: function() {
                        var tmpTranslateX = (_currentTranslateX - _$dom.ovLightboxThumbsListContainer.outerWidth());
                        if ((Math.abs(tmpTranslateX) + _$dom.ovLightboxThumbsListContainer.outerWidth()) >= _$dom.ovLightboxThumbsList.outerWidth()) {
                            _$dom.ovLightboxThumbsButtonNext.addClass('orbitvu-hidden');
                            tmpTranslateX = -(_$dom.ovLightboxThumbsList.outerWidth() - _$dom.ovLightboxThumbsListContainer.outerWidth());
                        }
                        _$dom.ovLightboxThumbsButtonPrev.removeClass('orbitvu-hidden');
                        _currentTranslateX = tmpTranslateX;
                        _$dom.ovLightboxThumbsList.css('transform', 'translate3d(' + tmpTranslateX + 'px, 0, 0)');
                    },

                    /**
                     * Scroll to previous images in thumbs list
                     * @private
                     */
                    scrollPrevAllThumbs: function() {
                        var tmpTranslateX = (_currentTranslateX + _$dom.ovLightboxThumbsListContainer.outerWidth());
                        if (tmpTranslateX >= 0) {
                            _$dom.ovLightboxThumbsButtonPrev.addClass('orbitvu-hidden');
                            tmpTranslateX = 0;
                        }
                        _$dom.ovLightboxThumbsButtonNext.removeClass('orbitvu-hidden');
                        _currentTranslateX = tmpTranslateX;
                        _$dom.ovLightboxThumbsList.css('transform', 'translate3d(' + tmpTranslateX + 'px, 0, 0)');
                    },

                    /**
                     * Updates thumbs list of lightbox
                     */
                    updateScroll: function() {
                        var $active = _$dom.ovLightboxThumbsList.find('li.orbitvu-active');
                        if ($active.length && _shouldEnableButtons()) {
                            _$dom.ovLightboxThumbsInner.removeClass('orbitvu-no-buttons');
                            var thumbW = $active.outerWidth(true),
                                index = $active.index(),
                                tmpTranslateX = (thumbW * index) - (_$dom.ovLightboxThumbsListContainer.outerWidth()/2) + (thumbW/2);

                            if (tmpTranslateX < 0) {
                                tmpTranslateX = 0;
                            } else if ((_$dom.ovLightboxThumbsList.outerWidth() - tmpTranslateX) < _$dom.ovLightboxThumbsListContainer.outerWidth()) {
                                tmpTranslateX = tmpTranslateX - (_$dom.ovLightboxThumbsListContainer.outerWidth() - (_$dom.ovLightboxThumbsList.outerWidth() - tmpTranslateX));
                            }
                            _currentTranslateX = -tmpTranslateX;
                            _$dom.ovLightboxThumbsList.css('transform', 'translate3d(' + (-tmpTranslateX) + 'px, 0, 0)');

                            if (tmpTranslateX <= 0) {
                                _$dom.ovLightboxThumbsButtonPrev.addClass('orbitvu-hidden');
                            } else {
                                _$dom.ovLightboxThumbsButtonPrev.removeClass('orbitvu-hidden');
                            }

                            if (_$dom.ovLightboxThumbsList.outerWidth() - tmpTranslateX <= _$dom.ovLightboxThumbsListContainer.outerWidth()) {
                                _$dom.ovLightboxThumbsButtonNext.addClass('orbitvu-hidden');
                            } else {
                                _$dom.ovLightboxThumbsButtonNext.removeClass('orbitvu-hidden');
                            }
                        } else {
                            _$dom.ovLightboxThumbsInner.addClass('orbitvu-no-buttons');
                            _$dom.ovLightboxThumbsList.css('transform', 'translate3d(0, 0, 0)');
                            _$dom.ovLightboxThumbsButtonPrev.addClass('orbitvu-hidden');
                            _$dom.ovLightboxThumbsButtonNext.addClass('orbitvu-hidden');
                        }
                    }
                }
            }())
            ;

        /**
         * Refreshes the view inside gallery
         * @param $itemLink
         * @private
         */
        function _refreshView($itemLink) {
            _$dom.ovImageThumbs.find('li.orbitvu-gallery-item').removeClass('orbitvu-active');
            _$dom.ovImageThumbs.find('a.orbitvu-gallery-item-link').removeClass('orbitvu-active');

            $itemLink.addClass('orbitvu-active');
            $itemLink.closest('li.orbitvu-gallery-item').addClass('orbitvu-active');

            _removeActive();

            if (_$dom.ovGalleryLenses && !_$dom.ovGalleryLenses.hasClass('orbitvu-hidden')) {
                _zoomMagnifier.seekZoomer();
            }

            if ($itemLink.data('ov_type') == 'ov2d-native' || $itemLink.data('ov_type') == 'ov2d-variation' || $itemLink.data('ov_type') == 'ov2d') {
                _$dom.ovGalleryViewImage.addClass('orbitvu-active');
                _injectImage2d($itemLink);
            } else if ($itemLink.data('ov_type') == 'ov360') {
                _$dom.ovGalleryView360.addClass('orbitvu-active');
                $('#ov360-item-container-' + $itemLink.data('id')).addClass('orbitvu-active');
                _injectProperOrbitvu($itemLink);
            }
        }

        /**
         * Removes active from  image2d/360/tour container
         * @private
         */
        function _removeActive() {
            if (_$dom.ovGalleryViewImage.length) {
                _$dom.ovGalleryViewImage.removeClass('orbitvu-active');
            }

            if (_$dom.ovGalleryView360.length) {
                _$dom.ovGalleryView360.removeClass('orbitvu-active');
                _$dom.ovGalleryView360.find('.ov360-item-container').removeClass('orbitvu-active');
            }

            if (_$dom.ovGalleryViewTour.length) {
                _$dom.ovGalleryViewTour.removeClass('orbitvu-active');
                _$dom.ovGalleryViewTour.find('.orbittour-item-container').removeClass('orbitvu-active');
            }
        }



        /**
         * Take cares about injecting proper function to display presentation or tour
         * @param $itemLink
         * @returns {boolean}
         * @private
         */
        function _injectProperOrbitvu($itemLink) {
            for(var i in _settings.presentationData) {
                var item = _settings.presentationData[i];
                if (item.id == $itemLink.attr('id')) {
                    item.inject();
                    return true;
                }
            }
            return false;
        }

        function _injectImage2d($itemLink) {
            _$dom.ovGalleryMainImage.addClass('orbitvu-hidden');
            _$dom.ovGalleryMainImage.attr('src', '');

            _$dom.ovSpinnerMainImage.stop(true, true).hide();
            _$dom.ovSpinnerMainImage.fadeIn(_settings.spinnerFadeInTime);

            _$dom.ovGalleryMainImage.on('load', function() {
                _$dom.ovSpinnerMainImage.stop(true, true).hide();
                $(this).removeClass('orbitvu-hidden');
            }).attr('src', $itemLink.data('src'));


        }

        /**********************************
         ************** EVENTS ************
         **********************************/
        function _initEvents() {
            //Change item events
            var $a = $('a.orbitvu-gallery-item-link');

            //touchAction parameter allows to zoom
            $('#ovgallery-main-image, #ovgallery-zoomer, #ovgallery-zoomer-zoom').hammer({touchAction : 'auto'});

            //Items hover, click
            $a.on('mouseenter', function() {
                var $elem = $(this);

                if (!$elem.hasClass('orbitvu-active')) {
                    _itemHoverTimeoutId = setTimeout(function() {
                        _refreshView($elem);
                        $elem.addClass('orbitvu-active');
                    }, _settings.thumbs.hoverDelay);
                }
            }).on('mouseleave', function(){
                if (_itemHoverTimeoutId) {
                    clearTimeout(_itemHoverTimeoutId);
                }
            }).on('click', function(){
                var $elem = $(this);

                if (!$elem.hasClass('orbitvu-active')) {
                    if (_itemHoverTimeoutId) {
                        clearTimeout(_itemHoverTimeoutId);
                    }
                    _refreshView($elem);
                    $elem.addClass('orbitvu-active');
                }
            });

            //Enable thumbs scroll events
            if (_settings.thumbs.scrollEnabled) {
                //set initial scroll buttons visibility
                //_thumbs.dispatchButtonsVisibility();
                _thumbs.updateScroll();

                _$dom.ovGoNext.on('click', function () {
                    _thumbs.scrollNextAllThumbs();
                });
                _$dom.ovGoBack.on('click', function () {
                    _thumbs.scrollPrevAllThumbs();
                });

                //mobile events for scroll
                _$dom.ovGalleryScroller.hammer({}).bind("swipeleft", function() {
                    _thumbs.scrollNextAllThumbs();
                });
                _$dom.ovGalleryScroller.hammer({}).bind("swiperight", function() {
                    _thumbs.scrollPrevAllThumbs();
                });

                $(window).on('resize', function () {
                    _thumbs.updateScroll();
                });
            }

            //Enable zoom magnifier events
            if (_settings.zoomMagnifier.enabled) {
                //prepare zoomer
                _$dom.ovGalleryMainImage.on('mouseenter', function (e) {
                    e.stopPropagation();
                    _zoomMagnifier.prepareZoomer();
                });

                //stop propagation when mouse is in lenses area
                _$dom.ovGalleryLenses.on('mousemove mouseenter', function (e) {
                    e.stopPropagation();
                });

                //destroy zoomer
                $('#ovgallery-zoomer, #ovgallery-zoomer-zoom').on('mouseleave', function (e) {
                    if (_$dom.ovGalleryZoomerZoom.is(':animated')) {
                        var mainImageOffset = _$dom.ovGalleryMainImage.offset();
                        if (e.pageX < mainImageOffset.left || e.pageX > mainImageOffset.left + _$dom.ovGalleryMainImage[0].getBoundingClientRect().width ||
                            e.pageY < mainImageOffset.top || e.pageY > mainImageOffset.top + _$dom.ovGalleryMainImage[0].getBoundingClientRect().height) {
                            _$dom.ovGalleryZoomerZoom.stop();
                            _zoomMagnifier.seekZoomer();
                        } else {
                            return false;
                        }
                    }

                    if (_zoomMagnifier.isZoomerZoomInside() && ($(this).attr('id') == 'ovgallery-zoomer-zoom')) {
                        _zoomMagnifier.seekZoomer();
                    } else if (!_zoomMagnifier.isZoomerZoomInside() && $(this).attr('id') == 'ovgallery-zoomer') {
                        _zoomMagnifier.seekZoomer();
                    }
                });

                _$dom.ovGalleryFix.on('mouseleave', function () {
                    _$dom.ovGalleryZoomerZoom.stop();
                    _zoomMagnifier.seekZoomer();
                });

                _$dom.ovGalleryFix.on('mousemove', function (e) {
                    var mainImageOffset = _$dom.ovGalleryMainImage.offset();
                    if (_$dom.ovGalleryZoomerZoom.is(':animated')) {
                        if (e.pageX < mainImageOffset.left || e.pageX > mainImageOffset.left + _$dom.ovGalleryMainImage[0].getBoundingClientRect().width ||
                            e.pageY < mainImageOffset.top || e.pageY > mainImageOffset.top + _$dom.ovGalleryMainImage[0].getBoundingClientRect().height) {
                            _$dom.ovGalleryZoomerZoom.stop();
                            _zoomMagnifier.seekZoomer();
                        }
                    }
                });

                //Move lenses
                $('#ovgallery-zoomer, #ovgallery-zoomer-zoom').on('mousemove', function(e) {
                    e.stopPropagation();
                    if (!_zoomMagnifier.isZoomerZoomInside() && $(this).attr('id') == 'ovgallery-zoomer-zoom' && !_$dom.ovGalleryZoomerZoom.is(':animated')) {
                        _zoomMagnifier.seekZoomer();
                        return false;
                    }
                    _zoomMagnifier.buildZoomer(e.pageX, e.pageY);
                });

                //Lenses size change on mousescroll
                $('#ovgallery-zoomer, #ovgallery-zoomer-zoom').on('mousewheel DOMMouseScroll', function (e) {
                    e.preventDefault();
                    _zoomMagnifier.setZoomerAndLensesOnScroll($(this).position(), e);
                });

                //Change zoomer size and position on window resize
                $(window).on('resize', function () {
                    if ($(this).width() != _zoomMagnifier.getInitialWindowWidth()) {
                        _zoomMagnifier.seekZoomer();
                        _zoomMagnifier.prepareZoomer();
                        _zoomMagnifier.setInitialWindowWidth($(this).width());
                    }
                });
            }

            //Enable lightbox events
            if (_settings.lightbox.enabled) {
                _$dom.ovLightboxOverlay.on('touchmove', function(e){
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                });

                $('#ovgallery-main-image, #ovgallery-zoomer, #ovgallery-zoomer-zoom').bind('tap', function(e) {
                    e.stopPropagation();
                    _lightbox.showLightbox($('#ovimage-thumbs').find('.orbitvu-gallery-item-link.orbitvu-active:first').data('ov_slide_id'));
                });
                //Next slide
                _$dom.ovLightboxNextButton.on('click', function() {
                    _lightbox.goNextSlideLightbox();
                });

                //Previous slide
                _$dom.ovLightboxPrevButton.on('click', function() {
                    _lightbox.goPrevSlideLightbox();
                });

                //Added arrow controls
                $(document).on('keydown', function(e){
                    if (e.keyCode == 37) { //left
                        _lightbox.goPrevSlideLightbox();
                    } else if (e.keyCode == 39) { //right
                        _lightbox.goNextSlideLightbox();
                    }
                });

                if (_settings.lightbox.thumbnailsEnabled) {
                    $('img.orbitvu-lightbox-thumbs-image').on('click', function() {
                        _lightbox.slideLightbox($(this).closest('li').data('ov_slide_id'), false);
                    });
                }

                //Enable lightbox mobile events
                _$dom.ovLightboxSlider.hammer({drag_block_horizontal: true, drag_block_vertical: false});
                _$dom.ovLightboxSlider.on('swipeleft', function(ev) {
                    if (ev.target != _$dom.ovLightboxSlider[0]) { //preventing double swipe
                        return;
                    }
                    _lightbox.goNextSlideLightbox();
                });
                _$dom.ovLightboxSlider.on('swiperight', function(ev) {
                    if (ev.target != _$dom.ovLightboxSlider[0]) { //preventing double swipe
                        return;
                    }
                    _lightbox.goPrevSlideLightbox();
                });

                //Close lightbox
                $('#ovlightbox-close-button').on('click', function() {
                    _lightbox.closeLightbox();
                });
                $(document).on('keyup', function(e) {
                    if (e.keyCode === 27) {
                        _lightbox.closeLightbox();
                    }
                });

                //Set proper lightbox slide dimension on init
                _lightbox.setLightboxSlideDimensions();

                //Set proper lightbox slide dimension on window resize
                $(window).on('resize', function () {
                    _lightbox.setLightboxSlideDimensions();
                });

                //Lightbox thumbs events
                if (_settings.lightbox.thumbnailsEnabled) {
                    //block double tap iOS zoom
                    _$dom.ovLightboxThumbs.on('click', function(e) {
                        e.preventDefault();
                    });
                    _$dom.ovLightboxThumbsButtonPrev.on('click', function() {
                        _lightboxThumbs.scrollPrevAllThumbs();
                    });

                    _$dom.ovLightboxThumbsButtonNext.on('click', function() {
                        _lightboxThumbs.scrollNextAllThumbs();
                    });
                    _$dom.ovLightboxThumbsListContainer.hammer({drag_block_horizontal: true, drag_block_vertical: false});
                    _$dom.ovLightboxThumbsListContainer.on('swipeleft swiperight', function(ev) {
                        ev.preventDefault();
                        if (ev.target != _$dom.ovLightboxThumbsListContainer[0]) { //preventing double swipe
                            return;
                        }
                        if (ev.type == 'swipeleft') {
                            _lightboxThumbs.scrollNextAllThumbs();
                        } else if (ev.type == 'swiperight') {
                            _lightboxThumbs.scrollPrevAllThumbs();
                        }
                    });
                    $(window).on('resize', function () {
                        _lightboxThumbs.updateScroll();
                    });
                }

                //Zoom lightbox image
                _$dom.ovLightboxZoomed.on('mousemove', function(e) {
                    var zoomedObj = _lightbox.getImage2dZoomedObj(),
                        factor,
                        ratioX,
                        ratioY,
                        zoomedWidth,
                        zoomedHeight,
                        scaleX,
                        scaleY,
                        cursorPosX = e.pageX - $(window).scrollLeft(),
                        cursorPosY = e.pageY - $(window).scrollTop();
                    if (zoomedObj.isZoomed && !zoomedObj.isClosing) {
                        if (_lightbox.isZoomedImageMovable()) {
                            if (_settings.lightbox.maxZoomEnabled && (_settings.lightbox.maxZoomWidth < zoomedObj.$baseImage[0].naturalWidth && _settings.lightbox.maxZoomHeight < zoomedObj.$baseImage[0].naturalHeight)) {
                                ratioX = zoomedObj.$baseImage[0].naturalWidth/_settings.lightbox.maxZoomWidth;
                                ratioY = zoomedObj.$baseImage[0].naturalHeight/_settings.lightbox.maxZoomHeight;
                                ratio = (ratioX > ratioY) ? ratioX : ratioY;
                                zoomedWidth = zoomedObj.$baseImage[0].naturalWidth/ratio;
                                zoomedHeight = zoomedObj.$baseImage[0].naturalHeight/ratio;
                                scaleX = zoomedWidth/zoomedObj.$baseImage[0].naturalWidth;
                                scaleY = zoomedHeight/zoomedObj.$baseImage[0].naturalHeight;
                            } else {
                                zoomedWidth = zoomedObj.$baseImage[0].naturalWidth;
                                zoomedHeight = zoomedObj.$baseImage[0].naturalHeight;
                                scaleX = 1;
                                scaleY = 1;
                            }

                            factor = _lightbox.calculateFactor(zoomedWidth, zoomedHeight);

                            _lightbox.setImage2dZoomedPosition({
                                posX: window.innerWidth < zoomedWidth ? (cursorPosX/factor.x) : zoomedObj.helper.zoomedImageCurrentData.posX,
                                posY: window.innerHeight < zoomedHeight ? (cursorPosY/factor.y) : zoomedObj.helper.zoomedImageCurrentData.posY,
                                scaleX: scaleX,
                                scaleY: scaleY
                            });
                        }
                    }
                });

                //Move zoomed lightbox image for mobile devices image
                _$dom.ovLightboxZoomed.on('touchmove', function (e) {
                    var zoomedObj = _lightbox.getImage2dZoomedObj(), temp = {};
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    e = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                    if (typeof zoomedObj.helper.touchMoveData == "undefined") {
                        zoomedObj.helper.touchMoveData = {
                            startX: e.pageX,
                            offsetX: 0,
                            startY: e.pageY,
                            offsetY: 0,
                            scaleX: zoomedObj.helper.zoomedImageCurrentData.scaleX,
                            scaleY: zoomedObj.helper.zoomedImageCurrentData.scaleY
                        }
                    } else {
                        zoomedObj.$image.removeClass('orbitvu-transition');
                        zoomedObj.helper.touchMoveData = {
                            offsetX: zoomedObj.helper.touchMoveData.startX - e.pageX,
                            startX: e.pageX,
                            offsetY: zoomedObj.helper.touchMoveData.startY - e.pageY,
                            startY: e.pageY,
                            scaleX: zoomedObj.helper.zoomedImageCurrentData.scaleX,
                            scaleY: zoomedObj.helper.zoomedImageCurrentData.scaleY
                        };
                        temp.posX = zoomedObj.helper.zoomedImageCurrentData.posX - zoomedObj.helper.touchMoveData.offsetX;
                        temp.posY = zoomedObj.helper.zoomedImageCurrentData.posY - zoomedObj.helper.touchMoveData.offsetY;
                        var imageDim = zoomedObj.$image[0].getBoundingClientRect();

                        if (imageDim.width > window.innerWidth) {
                            if (temp.posX + imageDim.width <= window.innerWidth) {
                                temp.posX = window.innerWidth - imageDim.width;
                            } else if (temp.posX > 0) {
                                temp.posX = 0;
                            }
                        } else {
                            temp.posX = zoomedObj.helper.zoomedImageCurrentData.posX;
                        }

                        if (imageDim.height > window.innerHeight) {
                            if (temp.posY + imageDim.height <= window.innerHeight) {
                                temp.posY = window.innerHeight - imageDim.height;
                            } else if (temp.posY > 0) {
                                temp.posY = 0;
                            }
                        } else {
                            temp.posY = zoomedObj.helper.zoomedImageCurrentData.posY;
                        }

                        temp.scaleX = zoomedObj.helper.zoomedImageCurrentData.scaleX;
                        temp.scaleY = zoomedObj.helper.zoomedImageCurrentData.scaleY;
                        _lightbox.setImage2dZoomedPosition(temp);
                    }
                });
                //Restart touchmove
                _$dom.ovLightboxZoomed.on('touchend', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var zoomedObj = _lightbox.getImage2dZoomedObj();
                    zoomedObj.$image.addClass('orbitvu-transition'); //this is necessary for devices with mouse and touchscreen
                    zoomedObj.helper.touchMoveData = undefined;
                });

                //Close zoomed Image
                _$dom.ovLightboxZoomed.hammer({})
                _$dom.ovLightboxZoomed.on('tap', function(e) {
                    var zoomedObj = _lightbox.getImage2dZoomedObj();
                    if (zoomedObj.isZoomed) {
                        _lightbox.closeZoomedImage2d();
                    }
                });
                //set proper position for zoomed image
                $(window).on('resize', function() {
                    _lightbox.centerImage2dZoomedPosition();
                });
            }

        }

        /**
         * Detects if css3 filter property is supported
         * @param enableWebkit
         * @returns {boolean|*}
         * @private
         */
        function _css3FilterFeatureDetect(enableWebkit) {
            if(enableWebkit === undefined) {
                enableWebkit = false;
            }
            var el = document.createElement('div');
            el.style.cssText = (enableWebkit?'-webkit-':'') + 'filter: blur(2px)';
            var test1 = (el.style.length != 0);
            var test2 = (
                document.documentMode === undefined //non-IE browsers, including ancient IEs
                || document.documentMode > 9 //IE compatibility moe
            );
            return test1 && test2;
        }

        return {
            init: function(settings) {
                _settings = $.extend( true, _settings, settings);

                _$dom = {  //DOM objects
                    ovGalleryFix: $('#ovgallery-fix'),
                    ovGallery: $('#ovgallery'),
                    ovGalleryViewImage: $('#ovgallery-view-image'),
                    ovGalleryView360: $('#ovgallery-view-360'),
                    ovGalleryViewTour: $('#ovgallery-view-tour'),
                    ovGalleryMainImage: $('#ovgallery-main-image'),
                    ovSpinnerMainImage: $('#ovspinner-main-image'),
                    ovGalleryScroller: $('#ovgallery-scroller'),
                    ovImageThumbs: $('#ovimage-thumbs'),
                    ovGoBack: $('#ovgo-back'),
                    ovGoNext: $('#ovgo-next'),
                    ovGalleryZoomer: $('#ovgallery-zoomer'),
                    ovGalleryLenses: $('#ovgallery-lenses'),
                    ovGalleryLensesImage: $('<img id="ovgallery-lenses-image" class="orbitvu-gallery-lenses-image">'),
                    ovSpinnerZoomerZoom: $('#ovspinner-zoomer-zoom-image'),
                    ovGalleryZoomerZoom: $('#ovgallery-zoomer-zoom'),
                    ovGalleryZoomerZoomImage: $('<img id="ovgallery-zoomer-zoom-image" class="orbitvu-gallery-zoomer-zoom-image orbitvu-hidden">'),
                    ovLightboxOverlay: $('#ovlightbox-overlay'),
                    ovLightboxSlider: $('#ovlightbox-slider'),
                    ovLightboxPrevButton: $('#ovlightbox-prev-button'),
                    ovLightboxNextButton: $('#ovlightbox-next-button'),
                    ovLightboxCloseButton: $('#ovlightbox-close-button'),
                    ovLightboxZoomed: $('#ovlightbox-zoomed'),
                    ovLightboxThumbsButtonPrev: $('#ovlightbox-thumbs-button-prev'),
                    ovLightboxThumbsButtonNext: $('#ovlightbox-thumbs-button-next'),
                    ovLightboxThumbsInner: $('#ovlightbox-thumbs-inner'),
                    ovLightboxThumbsListContainer: $('#ovlightbox-thumbs-list-container'),
                    ovLightboxThumbsList: $('#ovlightbox-thumbs-list'),
                    ovLightboxThumbs: $('#ovlightbox-thumbs'),
                };

                var $liFirst = _$dom.ovImageThumbs.find('li.orbitvu-gallery-item:first'),
                    $aFirst = $liFirst.find('a.orbitvu-gallery-item-link:first');

                _cssFilterSupport = _css3FilterFeatureDetect(true);

                if (!_cssFilterSupport) {
                    _$dom.ovImageThumbs.addClass('orbitvu-no-filter');
                }

                //refresh first view
                /************************
                 * Prestashop's specific
                 ************************/
                //refresh first view
                if (typeof defaultSelectedItem == 'undefined' || defaultSelectedItem == -1) {
                    _refreshView($aFirst);
                } else {
                    _refreshView($('a#' + defaultSelectedItem));
                }
                /*****************************
                 * Prestashop's specific ends
                 *****************************/

                //initialize events
                _initEvents();
            },

            //Prestashop's specific
            getThumbs: function() {
                return _thumbs;
            },

            //Prestashop's specific
            refreshView: function($itemLink) {
                _refreshView($itemLink);
            }
        }
    }());
})(jQuery);