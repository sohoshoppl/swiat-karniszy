/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function ($) {
    'use strict';

    /****************************
     * SET THE NAMESPACE FUNCTION
     ****************************/
    var ORBITVUSH = ORBITVUSH || {};
    window['ORBITVUSH'] = ORBITVUSH;
    ORBITVUSH.namespace = function (ns_string) {
        var parts = ns_string.split('.'),
            parent = ORBITVUSH,
            i;
        // strip redundant leading global
        if (parts[0] === "ORBITVUSH") {
            parts = parts.slice(1);
        }
        for (i = 0; i < parts.length; i += 1) {
            // create a property if it doesn't exist
            if (typeof parent[parts[i]] === "undefined") {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    };

    ORBITVUSH.namespace('scroller');
    ORBITVUSH.scroller = (function () {

        var _speed = 500;

        function _countThumbsWidth($ul) {
            var width = 0;
            $ul.find('li').each(function () {
                width += $(this).outerWidth(true);
            });
            return width;
        }

        function _countStep($scroller_div, $ul) {
            var width = 0,
                scroller_div_width = $scroller_div.width();
            $ul.find('li').each(function () {
                width += $(this).outerWidth(true);
                if (width > scroller_div_width) {
                    width -= $(this).outerWidth(true);
                    return false;
                }
            });

            if (width <= 0) {
                width = scroller_div_width;
            } else {
                width += parseInt($ul.find('li:first').css('margin-right'));
            }
            return width;
        }


        function _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul) {
            var prev_display = 'none',
                next_display = 'none',
                m_left = parseInt($ul.css('margin-left')),
                abs_m_left = Math.abs(m_left),
                thumbs_width = _countThumbsWidth($ul),
                step = _countStep($scroller_div, $ul);

            //next button
            if ((thumbs_width > $scroller_div.width()) && (m_left == 0)) {
                next_display = 'block';
            } else if (step + abs_m_left < thumbs_width) {
                next_display = 'block';
            }
            $next.css('display', next_display);

            //prev button
            if (m_left < 0) {
                prev_display = 'block';
            }
            $next.css('display', next_display);
            $prev.css('display', prev_display);
        }


        return {
            init: function ($prev, $next, $scroller_div, $ul) {

                _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul);

                $next.on('click', function () {
                    var step = _countStep($scroller_div, $ul);
                    if (!$ul.is(':animated')) {
                        $ul.animate({
                            marginLeft: "-=" + step + "px"
                        }, _speed, 'swing', function () {
                            _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul);
                        });
                    }
                });

                $prev.on('click', function () {
                    var step = _countStep($scroller_div, $ul);
                    if (!$ul.is(':animated')) {
                        $ul.animate({
                            marginLeft: "+=" + step + "px"
                        }, _speed, 'swing', function () {
                            _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul);
                        });
                    }
                });

                $(window).on('resize', function () {
                    if (!$ul.is(':animated') && parseInt($ul.css('margin-left')) != 0) {
                        $ul.animate({
                            marginLeft: "0px"
                        }, _speed, 'swing', function () {
                            _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul);
                        });
                    } else {
                        _dispatchButtonsVisibility($prev, $next, $scroller_div, $ul);
                    }
                });
            },

            getSpeed: function() {
                return _speed;
            },

            setSpeed: function(speed) {
                _speed = speed;
            }
        }
    }());


    ORBITVUSH.namespace('items');
    ORBITVUSH.items = (function () {
        var _ovData,
            _$container2d,
            _$container360,
            _$containerTour,
            _$containerItem,
            _$mainImage;

        function _initEvents() {
            var $a = $(_ovData.itemLinkClass);

            $a.on('mouseenter click', function() {
                if (!$(this).hasClass('orbitvu_active')) {
                    _refreshView($(this));
                    $(this).addClass('orbitvu_active');
                }

            })
        }

        function _refreshView($itemLink) {
            _$containerItem.find('li' + _ovData.itemClass).removeClass('orbitvu_active');
            _$containerItem.find('a' + _ovData.itemLinkClass).removeClass('orbitvu_active');

            $itemLink.addClass('orbitvu_active');
            $itemLink.closest('li' + _ovData.itemClass).addClass('orbitvu_active');

            if (_$container2d.length) {
                _$container2d.removeClass('orbitvu_active');
            }

            if (_$container360.length) {
                _$container360.removeClass('orbitvu_active');
                _$container360.find('.ov360-item-container').removeClass('orbitvu_active');
            }

            if (_$containerTour.length) {
                _$containerTour.removeClass('orbitvu_active');
                _$containerTour.find('.orbittour-item-container').removeClass('orbitvu_active');
            }

            if ($itemLink.data('ov_type') == 'ov2d-native' || $itemLink.data('ov_type') == 'ov2d') {
                _$container2d.addClass('orbitvu_active');
                _$mainImage.attr('src', $itemLink.data('src'));
            } else if ($itemLink.data('ov_type') == 'ov360') {
                _$container360.addClass('orbitvu_active');
                $('#ov360-item-container-' + $itemLink.data('ov_uid')).addClass('orbitvu_active');
                _injectProper($itemLink);
            } else if ($itemLink.data('ov_type') == 'ovtour') {
                _$containerTour.addClass('orbitvu_active');
                $('#orbittour-item-container-' + $itemLink.data('ov_uid')).addClass('orbitvu_active');
                _injectProper($itemLink);
            }
        }

        function _injectProper($itemLink) {
            for(var i in _ovData.items) {
                var item = _ovData.items[i];
                if (item.id == $itemLink.attr('id')) {
                    item.inject();
                    return true;
                }
            }
            return false;
        }

        return {
            init: function(ovData) {
                _ovData = ovData;
                _$container2d = ovData.container2d;
                _$container360 = ovData.container360;
                _$containerTour = ovData.containerTour;
                _$containerItem = ovData.containerItem,
                _$mainImage = ovData.mainImage;

                var $li_first = _$containerItem.find('li' + ovData.itemClass + ':first'),
                    $a_first = $li_first.find('a' + ovData.itemLinkClass + ':first');

                _refreshView($a_first);
                _initEvents();
            },

            getData: function() {
                return _ovData;
            },
        }
    }());

})(jQuery);
