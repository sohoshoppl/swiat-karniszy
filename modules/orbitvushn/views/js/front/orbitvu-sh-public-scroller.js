/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function($) {
    ORBITVUSH.namespace('scroller');
    ORBITVUSH.scroller = (function () {

        var
            _$prev,
            _$next,
            _$scrollerDiv,
            _$ul,
            _speed = 500;

        /**
         * Counts thumbnails width
         * @returns {number}
         * @private
         */
        function _countThumbsWidth() {
            var width = 0;
            _$ul.find('li').each(function () {
                width += $(this).outerWidth(true);
            });
            return width;
        }

        /**
         * Counts the step we have to scroll
         * @returns {number}
         * @private
         */
        function _countStep() {
            var width = 0,
                scroller_div_width = _$scrollerDiv.width();
            _$ul.find('li').each(function () {
                width += $(this).outerWidth(true);
                if (width > scroller_div_width) {
                    width -= $(this).outerWidth(true);
                    return false;
                }
            });

            if (width <= 0) {
                width = scroller_div_width;
            } else {
                width += parseInt(_$ul.find('li:first').css('margin-right'));
            }
            return width;
        }


        /**
         * Checks wheter button prev or next should be visible and clickable
         * @param $prev
         * @param $next
         * @param $scroller_div
         * @param $ul
         * @private
         */
        function _dispatchButtonsVisibility() {
            var prev_display = 'none',
                next_display = 'none',
                m_left = parseInt(_$ul.css('margin-left')),
                abs_m_left = Math.abs(m_left),
                thumbs_width = _countThumbsWidth(),
                step = _countStep();

            //next button
            if ((thumbs_width > _$scrollerDiv.width()) && (m_left == 0)) {
                next_display = 'block';
            } else if (step + abs_m_left < thumbs_width) {
                next_display = 'block';
            }
            _$next.css('display', next_display);

            //prev button
            if (m_left < 0) {
                prev_display = 'block';
            }
            _$next.css('display', next_display);
            _$prev.css('display', prev_display);
        }

        function _restoreScroller() {
            if (!_$ul.is(':animated') && parseInt(_$ul.css('margin-left')) != 0) {
                _$ul.animate({
                    marginLeft: "0px"
                }, _speed, 'swing', function () {
                    _dispatchButtonsVisibility();
                });
            } else {
                _dispatchButtonsVisibility();
            }
        }

        /**
         * Scrolls the items to the left
         * @private
         */
        function _goPrev() {
            var step = _countStep();

            if (!_$ul.is(':animated')) {
                if (parseInt(_$ul.css('margin-left')) < 0 ) {
                    _$ul.animate({
                        marginLeft: "+=" + step + "px"
                    }, _speed, 'swing', function () {
                        _dispatchButtonsVisibility();
                    });
                }
            }
        }

        /**
         * Scrolls the items to the right
         * @private
         */
        function _goNext() {
            var step = _countStep(),
                thumbs_width = _countThumbsWidth(),
                m_left = parseInt(_$ul.css('margin-left')),
                abs_m_left = Math.abs(m_left);

            if (!_$ul.is(':animated')) {
                if (((thumbs_width > _$scrollerDiv.width()) && (m_left == 0)) || (step + abs_m_left < thumbs_width)) {
                    _$ul.animate({
                        marginLeft: "-=" + step + "px"
                    }, _speed, 'swing', function () {
                        _dispatchButtonsVisibility();
                    });
                }
            }
        }

        return {
            init: function ($prev, $next, $scroller_div, $ul) {

                //init html elements
                _$prev = $prev;
                _$next = $next;
                _$scrollerDiv = $scroller_div;
                _$ul = $ul;

                _dispatchButtonsVisibility();


                $next.on('click', function () {
                    _goNext();
                });
                $prev.on('click', function () {
                    _goPrev();
                });

                //mobile events for scroll
                _$scrollerDiv.hammer({}).bind("swipeleft", function() {
                    _goNext();
                });
                _$scrollerDiv.hammer({}).bind("swiperight", function() {
                    _goPrev();
                });


                $(window).on('resize', function () {
                    _restoreScroller();
                });
            },

            /**
             * Sets the speed of the scroller
             * @param speed
             */
            setSpeed: function(speed) {
                _speed = speed;
            },

            /************************
             * Prestashop's specific
             ************************/
            dispatchButtonsVisibility: function() {
                _dispatchButtonsVisibility();
            },

            restore: function() {
                _restoreScroller();
            }
            /*****************************
             * Prestashop's specific ends
             *****************************/
        }
    }());
})(jQuery);