/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function ($) {
    'use strict';
    ORBITVUSH.namespace('ORBITVUSH.ajax');
    ORBITVUSH.ajax = (function () {

        var _orbitvuAjaxUrl = '', //module admin ajax url
            _initStandardData = {} //data which always is sent to ajax request
            ;

        /**
         * Prints ORBITVUSH exception
         * @param message
         * @returns {string}
         */
        function orbitvuException(message) {
            return "ORBITVUSH EXCEPTION: " + message;
        }

        //public api
        return {
            /**
             * Initializes ajax url for controller
             * @param url
             */
            initAjaxUrl: function(url) {
                _orbitvuAjaxUrl = url;
            },

            /**
             * Initializes sandard data which is always sent to ajax request
             * @param data
             */
            initStandardData: function(data) {
                _initStandardData = data;
            },

            /**
             * @param action
             * @param data
             * @param callbackSuccess
             * @param callbackError
             * @param callbackBeforeAjax
             * @param callbackAfterAjax
             */
            ajaxCall: function (action, data, callbackBeforeAjax, callbackSuccess, callbackError, callbackAfterAjax) {
                if (!$.isPlainObject(data)) {
                    throw orbitvuException("Data should be an object created within new Object or {}");
                }
                $.extend(data, {action: action}, _initStandardData);

                if ($.isFunction(callbackBeforeAjax)) {
                    callbackBeforeAjax();
                }
                $.post(
                    _orbitvuAjaxUrl,
                    data,
                    function (response) {
                        if (response === 0) {
                            throw orbitvuException("Ajax route: '" + action + "' not found");
                        }
                        if (response.status) {
                            if ($.isFunction(callbackSuccess)) {
                                callbackSuccess(response);
                            }
                        } else {
                            if ($.isFunction(callbackError)) {
                                callbackError(response);
                            }
                        }
                        if ($.isFunction(callbackAfterAjax)) {
                            callbackAfterAjax(response);
                        }
                    },
                    'json'
                );
            },
            getAjaxUrl: function() {
                return _orbitvuAjaxUrl;
            },
            getStandardData: function() {
                return _initStandardData;
            }
        }
    }());
})(jQuery);