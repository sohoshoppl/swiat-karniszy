/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function ($) {
    'use strict';

    ORBITVUSH.namespace('ORBITVUSH.settings');
    ORBITVUSH.settings = (function () {

        var _$ovUl,
            _$ovLi,
            _$ovGenerateAutoSynchUrl,
            _$ovGenerateAutoLinkUrl,
            _$ovGenerateAutoSkuUrl,
            _$ovGenerateRemoteUploadUrl,
            _$ovClearUnlinkHistory,
            _$ovMigrate, //Prestashop's specific
            _progressBarStop = false, //Prestashop's specific
            //dependencies
            _orbitvuAjax = ORBITVUSH.ajax;

        /**
         * Check status of progress bar
         * @param $progressBar
         */
        function _progressBar($progressBar) {
            _orbitvuAjax.ajaxCall('migrateProgress', {}, function() {

                },
                function(response) {
                    if (!_progressBarStop) {
                        if($progressBar.length) {
                            if (response.data.percentage > 0) {
                                $progressBar.find('#ovmigrate-progress-title').html(response.data.percentage + '%');
                                $progressBar.find('#ovmigrate-progress-bar').css('width', response.data.percentage + '%');
                            }
                            setTimeout(function() {
                                _progressBar($progressBar);
                            }, 1000);
                        }
                    }
                }
            );
        }

        //public api
        return {
            /**
             * Initialization method
             * @param presentationsLinked
             * @param presentationsNotLinked
             * @param idProduct
             * @param skuProduct
             * @param multiLink
             * @param isDemo
             * @param isConnected
             */
            init: function() {
                _$ovUl = $('#ovadmin-tabs'),
                _$ovLi = $('.orbitvu-admin-tab'),
                _$ovGenerateAutoSynchUrl = $("#ovgenerate-auto-synch-url"),
                _$ovGenerateAutoLinkUrl = $("#ovgenerate-auto-link-url"),
                _$ovGenerateAutoSkuUrl = $("#ovgenerate-auto-sku-url"),
                _$ovGenerateRemoteUploadUrl = $("#ovgenerate-remote-upload-url"),
                _$ovClearUnlinkHistory = $("#ovclear-unlink-history"),
                _$ovMigrate = $("#ovmigrate"); //Prestashop's specific


                $('#remote_upload_url, #auto_synch_url, #auto_link_url').attr('readonly', true); //Prestashop specific
                $('#ovexecute_auto_synch_url').attr('href', $('#auto_synch_url').val()); //Prestashop specific
                $('#ovexecute_auto_link_url').attr('href', $('#auto_link_url').val()); //Prestashop specific

                _orbitvuAjax.initStandardData({
                    dataType: 'json',
                    ajax: true
                });

                //Generate new batch action key
                $('#ovgenerate-auto-synch-url, #ovgenerate-auto-link-url, #ovgenerate-remote-upload-url').on('click', function(e) {
                    var $loader = $('<div class="orbitvu-ajax-loader"></div>'),
                        $elem = $(this);
                    e.preventDefault();
                    _orbitvuAjax.ajaxCall('generateBatchActionUrl', {type: $elem.data('type') }, function() {
                        $elem.hide();
                        $elem.after($loader);
                    }, function(response) {
                        var $message = $('<div class="orbitvu-alert orbitvu-alert-success">' + response.message + '</div>');
                        $loader.remove();
                        if ($elem.data('type') == 'auto_synch_key') {
                            $('#auto_synch_url').val(response.data.url);
                            $('#ovexecute_auto_synch_url').attr('href', response.data.url);
                            $('#auto_synch_key').val(response.data.new_key);
                        } else if ($elem.data('type') == 'auto_link_key') {
                            $('#auto_link_url').val(response.data.url);
                            $('#ovexecute_auto_link_url').attr('href', response.data.url);
                            $('#auto_link_key').val(response.data.new_key);
                        } else if ($elem.data('type') == 'remote_upload_key') {
                            $('#remote_upload_url').val(response.data.url);
                            $('#remote_upload_key').val(response.data.new_key);
                        }
                        $elem.after($message);
                        setTimeout(function() {
                            $message.fadeOut(800, function() {
                                $elem.show();
                            });
                        }, 1000);
                    });
                });

                //clear unlink history
                _$ovClearUnlinkHistory.on('click', function(e) {
                    var $loader = $('<div class="orbitvu-ajax-loader"></div>'),
                        $elem = $(this);
                    e.preventDefault();
                    _orbitvuAjax.ajaxCall('clearUnlinkHistory', {}, function() {
                        $elem.hide();
                        $elem.after($loader);
                    }, function(response) {
                        var $message = $('<div class="orbitvu-alert orbitvu-alert-success">' + response.message + '</div>');
                        $loader.remove();
                        $elem.after($message);
                        setTimeout(function() {
                            $message.fadeOut(800, function() {
                                $elem.show();
                            });
                        }, 1000);
                    });
                });

                //migrate
                _$ovMigrate.on('click', function(e) {
                    var $loader = $('<div class="orbitvu-ajax-loader"></div>'),
                        $progressBar = $('<div class="orbitvu-migrate-progress"><div class="orbitvu-migrate-progress-title" id="ovmigrate-progress-title">0%</div><div class="orbitvu-migrate-progress-bar" id="ovmigrate-progress-bar"></div></div>'),
                        $elem = $(this);

                    e.preventDefault();
                    _orbitvuAjax.ajaxCall('migrate', {}, function() {
                        $elem.hide();
                        $elem.after($progressBar).after($loader);
                        _progressBarStop = false;
                        _progressBar($progressBar);
                    }, function(response) {
                        var $message = $('<div class="orbitvu-alert orbitvu-alert-success orbitvu-alert-no-margin-bottom">' + response.message + '</div>');
                        _progressBarStop = true;
                        $loader.remove();
                        $progressBar.remove();
                        $elem.after($message);
                        $elem.remove();
                    },
                    function(response) {
                        var $message = $('<div class="orbitvu-alert orbitvu-alert-danger orbitvu-alert-no-margin-bottom">' + response.message + '</div>');
                        _progressBarStop = true;
                        $loader.remove();
                        $progressBar.remove();
                        $elem.after($message);
                        setTimeout(function() {
                            $message.fadeOut(800, function() {
                                $elem.show();
                            });
                        }, 2000);
                    });
                });
            }
        }
    }());
})(jQuery);