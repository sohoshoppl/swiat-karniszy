/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

(function ($) {
    'use strict';

    ORBITVUSH.namespace('ORBITVUSH.app');
    ORBITVUSH.app = (function () {
            var
                //private properties
                _presentationsLinked = [],
                _presentationsNotLinked = [],
                //initialization flags
                _eventsLinkedInitialized = false,
                _eventsNotLinkedInitialized = false,
                _eventsPagerInitialized = false,
                _pagerInitialized = false,
                _pager = {},
                _idProduct,
                _skuProduct,
                _multiLink,
                _orbitvuUploadErrorMessages = {},
                _$ovpresentationLinkedListContainer,
                _$ovListLinked,
                _$ovpresentationListContainer,
                _$ovList,
                _$ovPager,
                _$ovUploadInput,
                _$ovUploadFilesContainer,

                //draggable
                _$draggableHook,
                _$draggableItem,
                _$draggableItemContainer,
                _$allItemsContainer,
                _isInDragState = false,
                _changeableItemId = false,

                //animations speed
                _messageTimeout = 2000,
                _fadeInSpeed = 400,
                _fadeOutSpeed = 400,
                _refreshTimeout= 500,

                //dependencies
                _orbitvuAjax = ORBITVUSH.ajax

                ;

        /**
         * Initializies linked presentations html content
         * @private
         */
        function _initPresentationsLinked(presentationsLinked) {
            _presentationsLinked.init(presentationsLinked);
        }

        /**
         * Initializes pager html content
         * @private
         */
        function _initPager() {

            _pagerInitialized = true;

            _pager.Ractive = new Ractive({
                el: '#ovpager',
                template: '#orbitvuPager',
                data: _pager.getData(),
                append: true
            });
        }

        /**
         * Initializes not linked presentations content
         * @private
         */
        function _initPresentationsNotLinked(presentationsNotLinked) {
            _presentationsNotLinked.init(presentationsNotLinked);
        }


        /* ********************************************************************************************************** */
        /* *********************************************** Helper functions ***************************************** */
        /* ********************************************************************************************************** */


        /**
         * Adds helper methods for pager object
         * @private
         */
        function _addPagerHelperMethods() {

            /**
             * Retrieves all pager data
             * @returns {*}
             */
            _pager.getData = function() {
                var total_pages;

                if (!this.Ractive) {
                    total_pages = (parseInt(_presentationsNotLinked.count) % 20) ? (Math.floor(_presentationsNotLinked.count / 20) + 1) : (Math.floor(_presentationsNotLinked.count / 20));
                    return {
                        total_pages: isNaN(total_pages) ? '' : total_pages,
                        total_items: isNaN(parseInt(_presentationsNotLinked.count)) ? '' : parseInt(_presentationsNotLinked.count),
                        arrow_prev_disabled: 'orbitvu-pager-section-arrow-disabled',
                        arrow_next_disabled: total_pages > 1 ? '' : 'orbitvu-pager-section-arrow-disabled',
                        current_page: 1,
                    }
                }
                return $.extend({}, this.Ractive.get(''), {
                    per_page: $('#ovperpage').val(),
                    sort: $('#ovsort').val(),
                    search: $('#ovsearch').val()
                }); //get all pager data from Ractive object + custom data
            }

            /**
             * Executes ajax request for pager
             * @param pData
             */
            _pager.callPager = function(pData) {
                var ractive = _pager.Ractive;
                _orbitvuAjax.ajaxCall('pager', pData,
                    function() {
                        ractive.set('is_loading', 1);
                        _presentationsLinked.blockAll();
                        _presentationsNotLinked.blockAll();
                    },
                    function(response) {
                        ractive.set('is_loading', 0);
                        ractive.set({
                            is_loading: 0,
                            total_items: response.data.total_items,
                            total_pages: response.data.total_pages,
                            per_page: response.data.per_page,
                            current_page: response.data.current_page,
                            arrow_prev_disabled: response.data.arrow_prev_disabled,
                            arrow_next_disabled: response.data.arrow_next_disabled,
                        });
                        _presentationsLinked.unblockAll();
                        _presentationsNotLinked.unblockAll();
                        _presentationsNotLinked.removeAll();
                        _presentationsNotLinked.init(response.data.results);
                    }
                );
            }
        }


        /**
         * Adds helper methods presentations linked objects
         * @private
         */
        function _addPerentationsLinkedHelperMethods() {

            /**
             * Initializes linked prezentations
             * @param presentationsLinked
             */
            _presentationsLinked.init = function(presentationsLinked) {
                for(var i = 0; i < presentationsLinked.length; i++) {
                    _presentationsLinked.parsePresentationData(presentationsLinked[i]);
                    presentationsLinked[i].Ractive = new Ractive({
                        el: '#ovlist-linked',
                        template: '#orbitvuPresentationLinked',
                        data: presentationsLinked[i],
                        append: true
                    });
                    _presentationsLinked.push(presentationsLinked[i]);
                }
            }

            /**
             * Adds new linked presentation
             * @param presentation
             */
            _presentationsLinked.addPresentation = function(presentation) {
                _presentationsLinked.parsePresentationData(presentation);
                presentation.Ractive = new Ractive({
                    el: '#ovlist-linked',
                    template: '#orbitvuPresentationLinked',
                    data: presentation,
                    append: true
                });
                _presentationsLinked.push(presentation);
            }

            /**
             * Returns presentations instance
             * @param idPresentation
             * @returns {*}
             */
            _presentationsLinked.getPresentationById = function(idPresentation) {
                for (var i = 0; i < _presentationsLinked.length; i++) {
                    if (_presentationsLinked[i].id_presentation == idPresentation) {
                        return _presentationsLinked[i];
                    }
                }
                return false;
            };

            /**
             * Removes one presentation from dom and array
             * @param idPresentation
             */
            _presentationsLinked.removePresentationById = function(idPresentation) {
                var index = null,
                    order = 10,
                    i = 0,
                    j = 0,
                    items;

                for (i = 0; i < _presentationsLinked.length; i++) {
                    if (_presentationsLinked[i].id_presentation == idPresentation) {
                        index = i;
                        break;
                    }
                }

                if (index !== null) {
                    _presentationsLinked[index].Ractive.teardown();
                    _presentationsLinked.splice(index, 1);
                    //fix items ordering after delete
                    for (i = 0; i < _presentationsLinked.length; i++) {
                        items = _presentationsLinked[i].Ractive.get('items');
                        for(j = 0; j < items.length; j++) {
                            _presentationsLinked[i].Ractive.set('items.' + j + '.order', order);
                            order += 10;
                        }
                    }
                }
            }

            /**
             * Blocks presentation
             * @param idPresentation
             */
            _presentationsLinked.block = function(idPresentation) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_blocked', 1);
                }
            };

            /**
             * Blocks presentation with loading icon
             * @param idPresentation
             */
            _presentationsLinked.blockLoading = function(idPresentation) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_loading', 1);
                }
            };

            /**
             * Blocks all presentations linked
             */
            _presentationsLinked.blockAll = function() {
                for (var i = 0; i < _presentationsLinked.length; i++) {
                    if (!_presentationsLinked[i].Ractive.get('is_blocked') && !_presentationsLinked[i].Ractive.get('is_blocked_permanently') && !_presentationsLinked[i].Ractive.get('is_loading')) {
                        _presentationsLinked[i].Ractive.set('is_blocked', 1);
                    }
                }
            };

            /**
             * Blocks presentation item with loading icon
             * @param idPresentation
             */
            _presentationsLinked.blockItemLoading = function(idPresentation, idProductPresentationItem) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    for(var i = 0; i < p.items.length; i++) {
                        if (p.items[i].id_product_presentation_item == idProductPresentationItem) {
                            p.Ractive.set('items.' + i + '.is_loading', 1);
                        }
                    }
                }
            };

            /**
             * Blocks presentation item with loading icon
             * @param idPresentation
             */
            _presentationsLinked.blockItemsAll = function(idPresentation) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    for(var i = 0; i < p.items.length; i++) {
                        if (!p.Ractive.get('items.' + i + '.is_blocked') && !p.Ractive.get('items.' + i + '.is_loading')) {
                            p.Ractive.set('items.' + i + '.is_blocked', 1);
                        }
                    }
                }
            };

            /**
             * Unblocks all presentations linked (except blocked permanently)
             */
            _presentationsLinked.unblockAll = function() {
                for (var i = 0; i < _presentationsLinked.length; i++) {
                    if (_presentationsLinked[i].Ractive.get('is_blocked') || _presentationsLinked[i].Ractive.get('is_loading')) {
                        _presentationsLinked[i].Ractive.set('is_blocked', 0);
                        _presentationsLinked[i].Ractive.set('is_loading', 0);
                    }
                }
            };

            /**
             * Blocks presentation item with loading icon
             * @param idPresentation
             */
            _presentationsLinked.unblockItemsAll = function(idPresentation) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    for(var i = 0; i < p.items.length; i++) {
                        if (p.Ractive.get('items.' + i + '.is_blocked') || p.Ractive.get('items.' + i + '.is_loading')) {
                            p.Ractive.set('items.' + i + '.is_loading', 0);
                            p.Ractive.set('items.' + i + '.is_blocked', 0);
                        }
                    }
                }
            };

            /**
             * Retrieves item key from an items array
             * @param idPresentation
             * @param idProductPresentationItem
             * @returns {*}
             */
            _presentationsLinked.getItemKey = function(idPresentation, idProductPresentationItem) {
                var p = _presentationsLinked.getPresentationById(idPresentation);
                if (p) {
                    for(var i = 0; i < p.items.length; i++) {
                        if (p.Ractive.get('items.' + i + '.id_product_presentation_item') == idProductPresentationItem) {
                            return 'items.' + i;
                        }
                    }
                }
                return false;
            };

            /**
             *
             * @param id_presentation
             * @param idProductPresentationItem
             * @param type green|red
             * @param message
             */
            _presentationsLinked.blinkItem = function(idProductPresentatonItem, type, message) {
                var tpl = '' +
                    '<div class="orbitvu-presentation-linked-item-container-' + type + '-blink">' +
                        '<div class="orbitvu-table-container">' +
                            '<span>' + message + '</span>' +
                        '</div>' +
                    '</div>',
                    $blink = $(tpl);

                $('#orbitvu-presentation-linked-item-container-' + idProductPresentatonItem).prepend($blink);
                $blink.fadeOut(_fadeOutSpeed + 500, function() {
                    $(this).remove();
                });
            }

            /**
             * Prints dynamic message
             * @param idPresentation
             * @param type danger|info|success
             * @param message
             * @param beforeShow
             * @param afterDisappear
             */
            _presentationsLinked.printDynamicMessage = function(idPresentation, type, message, beforeShow, afterDisappear) {
                var $messageContent = $('<div class="orbitvu-alert orbitvu-alert-' + type + '">' + message + '</div>'),
                    $message = $('#ovmessage-dynamic-' + idPresentation).length ? $('#ovmessage-dynamic-' + idPresentation) : $('#ovmessages-dynamic'),
                    scrollOffset,
                    $presta16PageHead = $('body').find('.page-head:first'); //Prestashop's specific

                if ($.isFunction(beforeShow)) {
                    beforeShow();
                }
                $message.append($messageContent.fadeIn(_fadeInSpeed));
                scrollOffset = parseInt($messageContent.offset().top) - parseInt($messageContent.outerHeight() + 15 + ($presta16PageHead.length ? $presta16PageHead.outerHeight() : 0));
                $('body').animate({
                    scrollTop: scrollOffset
                }, 500);
                setTimeout(function() {
                    $messageContent.fadeOut(_fadeOutSpeed, function() {
                        if ($.isFunction(afterDisappear)) {
                            $messageContent.remove();
                            afterDisappear();
                        }
                    })
                }, _messageTimeout);
            }

            /**
             * Prepares the presentation data to display on frontend
             * @param presentation
             * @returns {*}
             * @private
             */
            _presentationsLinked.parsePresentationData = function(presentation) {
                presentation.sku_info = (presentation.ov_sku !== _skuProduct && _skuProduct != '') ? 1 : 0;
                presentation.buttons_synch_disabled = '';
            }
        }


        /**
         * Adds helper methods for presentations not linked objects
         * @private
         */
        function _addPerentationsNotLinkedHelperMethods() {

            /**
             * Initializes
             * @param results
             */
            _presentationsNotLinked.init = function(presentationsNotLinked) {
                _presentationsNotLinked.count = presentationsNotLinked.count;
                for(var i = 0; i < presentationsNotLinked.length; i++) {
                    _presentationsNotLinked.parsePresentationData(presentationsNotLinked[i]);
                    presentationsNotLinked[i].Ractive = new Ractive({
                        el: '#ovlist',
                        template: '#orbitvuPresentationNotLinked',
                        data: presentationsNotLinked[i],
                        append: true
                    });
                    _presentationsNotLinked.push(presentationsNotLinked[i]);
                }
            }

            /**
             * Returns presentation not linked instance
             * @param idPresentation
             * @returns {*}
             */
            _presentationsNotLinked.getPresentationById = function(idPresentation) {
                for (var i = 0; i < _presentationsNotLinked.length; i++) {
                    if (_presentationsNotLinked[i].id_presentation == idPresentation) {
                        return _presentationsNotLinked[i];
                    }
                }
                return false;
            };

            /**
             * Blocks presentation not linked
             * @param idPresentation
             */
            _presentationsNotLinked.block = function(idPresentation) {
                var p = _presentationsNotLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_blocked', 1);
                }
            };

            /**
             * Blocks presentation not linked permanently
             * @param idPresentation
             */
            _presentationsNotLinked.blockPermanently = function(idPresentation) {
                var p = _presentationsNotLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_blocked', 0);
                    p.Ractive.set('is_blocked_permanently', 1);
                    p.Ractive.set('is_loading', 0);
                }
            };

            /**
             * Blocks presentation not linked with loading icon
             * @param idPresentation
             */
            _presentationsNotLinked.blockLoading = function(idPresentation) {
                var p = _presentationsNotLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_loading', 1);
                }
            };

            /**
             * Blocks all presentations not linked
             */
            _presentationsNotLinked.blockAll = function() {
                for (var i = 0; i < _presentationsNotLinked.length; i++) {
                    var item = _presentationsNotLinked[i];
                    if (!item.Ractive.get('is_blocked') && !item.Ractive.get('is_blocked_permanently') && !item.Ractive.get('is_loading')) {
                        item.Ractive.set('is_blocked', 1);
                    }
                }
            }

            /**
             * Unblocks all presentations not linked (except blocked permanently)
             */
            _presentationsNotLinked.unblockAll = function() {
                for (var i = 0; i < _presentationsNotLinked.length; i++) {
                    var item = _presentationsNotLinked[i];
                    if (item.Ractive.get('is_blocked') || item.Ractive.get('is_loading')) {
                        item.Ractive.set('is_blocked', 0);
                        item.Ractive.set('is_loading', 0);
                    }
                }
            }

            /**
             * Unblocks presentation not linked from permament
             * @param idPresentation
             */
            _presentationsNotLinked.unblockPermanently = function(idPresentation) {
                var p = _presentationsNotLinked.getPresentationById(idPresentation);
                if (p) {
                    p.Ractive.set('is_blocked', 0);
                    p.Ractive.set('is_blocked_permanently', 0);
                    p.Ractive.set('is_loading', 0);
                }
            };

            /**
             * Removes all not linked presentations from DOM
             */
            _presentationsNotLinked.removeAll = function() {
                for (var i = 0; i < _presentationsNotLinked.length; i++) {
                    var item = _presentationsNotLinked[i];
                    item.Ractive.teardown();
                }
                _presentationsNotLinked = [];
                _addPerentationsNotLinkedHelperMethods();
            }

            /**
             * Prepares the presentation data to display on frontend
             * @param presentation
             * @returns {*}
             * @private
             */
            _presentationsNotLinked.parsePresentationData = function(presentation) {
                for (var i = 0; i < _presentationsLinked.length; i++) {
                    if (_presentationsLinked[i].id_presentation == presentation.id_presentation) {
                        presentation.is_blocked_permanently = 1;
                        break;
                    }
                }
            }
        }


        /* ********************************************************************************************************** */
        /* ******************************************** Helper functions ends *************************************** */
        /* ********************************************************************************************************** */



        /* ********************************************************************************************************** */
        /* ************************************************** Events ************************************************ */
        /* ********************************************************************************************************** */


        /**
         * Initializes standard events
         * @private
         */
        function _initUploadEvents() {
            _$ovUploadInput.on('change', function() {
                var data = {};
                $.extend(data, {
                    action: 'uploadPresentation'
                }, _orbitvuAjax.getStandardData());

                $(this).simpleUpload(_orbitvuAjax.getAjaxUrl(), {
                    allowedExts: ["zip", "ovus"],
                    data: data,
                    start: function(file) {
                        //upload started
                        this.block = $('<div class="orbitvu-upload-file"></div>');
                        this.progressBar = $('<div class="orbitvu-upload-progress"></div>');
                        this.title = $('<div class="orbitvu-upload-title" title="' + file.name + '">' + file.name + '</div>');
                        this.progressBarPercentage = $('<div class="orbitvu-upload-progress-percentage">0%</div>');
                        this.block.append(this.progressBar).append(this.title).append(this.progressBarPercentage);
                        _$ovUploadFilesContainer.append(this.block);
                    },
                    progress: function(progress){
                        //received progress
                        this.progressBar.width(progress + "%");
                        this.progressBarPercentage.text(parseInt(progress) + "%");
                    },
                    success: function(response){
                        if (response.status) {
                            installPresentation(response, this);
                        } else {
                            var $errorDiv = $('<div class="orbitvu-upload-error">' + response.message + '</div>');
                            this.block.html($errorDiv);
                            setTimeout(function() {
                                $errorDiv.closest('div.orbitvu-upload-file').fadeOut(_fadeOutSpeed, function() {
                                    $(this).remove();
                                });
                            }, 5000)
                        }
                    },
                    error: function(error){
                        //upload failed
                        var message = error.message;
                        if (typeof _orbitvuUploadErrorMessages[error.name] !== "undefined") {
                            message = _orbitvuUploadErrorMessages[error.name];
                        }
                        var $errorDiv = $('<div class="orbitvu-upload-error"></div>').text(message);
                        this.block.html($errorDiv);
                        setTimeout(function() {
                            $errorDiv.closest('div.orbitvu-upload-file').fadeOut(_fadeOutSpeed, function() {
                                $(this).remove();
                            });
                        }, 5000);
                    }
                });
            });

            function installPresentation(response, simpleUpload) {
                simpleUpload.progressBar.fadeOut();
                simpleUpload.progressBarPercentage.fadeOut();
                simpleUpload.title.fadeOut(_fadeOutSpeed, function() {
                    var $installDiv = $('<div class="orbitvu-upload-install">' + response.message + '</div>'),
                        $loader = $('<div class="orbitvu-ajax-loader"></div>');

                    simpleUpload.block.append($installDiv).append($loader);
                    _orbitvuAjax.ajaxCall('installPresentation', {filename: response.data.filename},
                        null,
                        function(response) {
                            $installDiv.fadeOut(_fadeOutSpeed, function() {
                                var $successDiv = $('<div class="orbitvu-upload-success">' + response.message + '</div>'),
                                    pData;

                                simpleUpload.block.html($successDiv);
                                setTimeout(function() {
                                    $successDiv.closest('div.orbitvu-upload-file').fadeOut(_fadeOutSpeed, function() {
                                        $(this).remove();
                                    });
                                }, 5000);

                                if (!_pagerInitialized) {
                                    _initPager();
                                }
                                if (!_eventsPagerInitialized) {
                                    _initPagerEvents();
                                }
                                if (!_eventsNotLinkedInitialized) {
                                    _initPresentationsNotLinkedEvents();
                                }

                                if (typeof response.data.presentation !== "undefined") { //we link immediately uploaded Presentation
                                    _$ovpresentationLinkedListContainer.removeClass('orbitvu-hidden');
                                    _presentationsLinked.addPresentation(response.data.presentation);
                                    if (!_eventsLinkedInitialized) {
                                        _initPresentationsLinkedEvents();
                                    }
                                    if (!_multiLink) {
                                        _$ovpresentationListContainer.addClass('orbitvu-hidden');
                                    }
                                }

                                if (_multiLink || !_presentationsLinked.length) {
                                    pData = _pager.getData();
                                    pData.sort = '-create_date';
                                    pData.search = '';
                                    pData.clear = 1;
                                    $('#ovsort').val(pData.sort);
                                    $('#ovsearch').val(pData.search);
                                    _pager.callPager(pData);
                                    _$ovpresentationListContainer.removeClass('orbitvu-hidden');
                                }
                            });
                            $loader.fadeOut(_fadeOutSpeed);
                        },
                        function(data) {
                            $installDiv.fadeOut(_fadeOutSpeed, function() {
                                var $errorDiv = $('<div class="orbitvu-upload-error">' + data.message + '</div>');
                               simpleUpload.block.html($errorDiv);
                                setTimeout(function() {
                                    $errorDiv.closest('div.orbitvu-upload-file').fadeOut(_fadeOutSpeed, function() {
                                        $(this).remove();
                                    });
                                }, 5000);
                            });
                        }
                    );
                });
            }
        }

        /**
         * Add events for pager
         * @param callPager
         * @private
         */
        function _initPagerEvents() {

            _eventsPagerInitialized = true;

            //next|prev|search click
            _$ovPager.on('click', '#ovprev, #ovnext, #ovsearch-button', function(e) {
                var pData = _pager.getData();

                if ($(this).hasClass('orbitvu-pager-section-arrow-disabled')) {
                    return false;
                }

                if ($(this).attr('id') == 'ovsearch-button') {
                    pData.direction = null;
                    pData.clear = 1;
                    e.preventDefault();
                }

                if ($(this).attr('id') == 'ovprev' || $(this).attr('id') == 'ovnext') {
                    pData.direction = $(this).attr('id');
                    pData.clear = 0;
                }

                //callPager(pData);
                _pager.callPager(pData);
            });
            //per page, sort change
            _$ovPager.on('change', '#ovperpage, #ovsort', function(e) {
                var pData = _pager.getData();

                pData.direction = null;
                pData.clear = 1;

                //callPager(pData);
                _pager.callPager(pData);
            });

            //click enter when in search input
            _$ovPager.on('keydown', '#ovsearch', function(e) {
                var pData = _pager.getData();
                if (e.keyCode == 13) {
                    pData.clear = 1;
                    pData.direction = null;
                    //callPager(pData);
                    _pager.callPager(pData)
                    return false;
                }
            });
        }


        /**
         * Adds events for presentations linked
         * @private
         */
        function _initPresentationsLinkedEvents() {

            _eventsLinkedInitialized = true;

            /************************************
             * MOVE ITEMS HELPER FUNCTIONS START
             ************************************/
            /**
             * Retirevies item which has to be replaced with currently drag item
             * @returns {boolean}
             * @private
             */
            function _getChangeableItemId() {
                var changeableItems = _getChangeableItems(), changeableItem = false, properItems = {left: [], right: [], top: [], bottom: []}, squares = {};

                for(var i = 0; i < changeableItems.left.length; i++) {
                    var item = changeableItems.left[i], hor = 'left', vert = false;
                    if ($.inArray(item, changeableItems.top) > -1) {
                        vert = 'top';
                    } else if ($.inArray(item, changeableItems.bottom) > -1) {
                        vert = 'bottom';
                    }
                    if (hor && vert) {
                        if ($.inArray(item, properItems[hor]) == -1) {
                            properItems[hor].push(item);
                        }
                        if ($.inArray(item, properItems[vert]) == -1) {
                            properItems[vert].push(item);
                        }
                    }
                }

                for(var i = 0; i < changeableItems.right.length; i++) {
                    var item = changeableItems.right[i], hor = 'right', vert = false;
                    if ($.inArray(item, changeableItems.top) > -1) {
                        vert = 'top';
                    } else if ($.inArray(item, changeableItems.bottom) > -1) {
                        vert = 'bottom';
                    }
                    if (hor && vert) {
                        if ($.inArray(item, properItems[hor]) == -1) {
                            properItems[hor].push(item);
                        }
                        if ($.inArray(item, properItems[vert]) == -1) {
                            properItems[vert].push(item);
                        }
                    }
                }

                for(var i = 0; i < changeableItems.top.length; i++) {
                    var item = changeableItems.top[i], hor = false, vert = 'top';
                    if ($.inArray(item, changeableItems.left) > -1) {
                        hor = 'left';
                    } else if ($.inArray(item, changeableItems.right) > -1) {
                        hor = 'right';
                    }
                    if (hor && vert) {
                        if ($.inArray(item, properItems[hor]) == -1) {
                            properItems[hor].push(item);
                        }
                        if ($.inArray(item, properItems[vert]) == -1) {
                            properItems[vert].push(item);
                        }
                    }
                }

                for(var i = 0; i < changeableItems.bottom.length; i++) {
                    var item = changeableItems.bottom[i], hor = false, vert = 'bottom';
                    if ($.inArray(item, changeableItems.left) > -1) {
                        hor = 'left';
                    } else if ($.inArray(item, changeableItems.right) > -1) {
                        hor = 'right';
                    }
                    if (hor && vert) {
                        if ($.inArray(item, properItems[hor]) == -1) {
                            properItems[hor].push(item);
                        }
                        if ($.inArray(item, properItems[vert]) == -1) {
                            properItems[vert].push(item);
                        }
                    }
                }

                for (var i in properItems) {
                   for(var j in properItems[i]) {
                       var id = properItems[i][j];
                       if (!squares[id]) {
                           squares[id] = {};
                       }
                       if (i == 'left') {
                            squares[id].width = $('#' + id).position().left + _$draggableItemContainer.width() - _$draggableItem.position().left;
                       } else if (i == 'right') {
                           squares[id].width = _$draggableItemContainer.width() - (($('#' + id).position().left + _$draggableItemContainer.width()) - (_$draggableItem.position().left + _$draggableItemContainer.width()));
                       } else if (i == 'top') {
                           squares[id].height = $('#' + id).position().top + _$draggableItemContainer.height() - _$draggableItem.position().top;
                       } else if (i == 'bottom') {
                           squares[id].height = _$draggableItemContainer.height() - (($('#' + id).position().top + _$draggableItemContainer.height()) - (_$draggableItem.position().top + _$draggableItemContainer.height()));
                       }
                   }
                }

               //calculate the biggest area
               for (var i in squares) {
                   squares[i].size = squares[i].width * squares[i].height;
                   if (!changeableItem) {
                       changeableItem =  {
                           size: squares[i].size,
                           id: i
                       };
                   } else if (changeableItem.size < squares[i].size) {
                       changeableItem =  {
                           size: squares[i].size,
                           id: i
                       };
                   }
               }
               //return changeable item with biggest area
               return changeableItem ? changeableItem.id : false;
            }

            /**
             * Retrieves all items which are in the draggableItem area
             * @returns {{left: Array, right: Array, top: Array, bottom: Array}}
             * @private
             */
            function _getChangeableItems() {
                var changeableItems = {
                    left: [],
                    right: [],
                    top: [],
                    bottom: []
                }
                _$allItemsContainer.find('.orbitvu-presentation-linked-item-container').each(function() {
                    /*if ($(this).attr('id') == _$draggableItemContainer.attr('id')) {
                        return true;
                    }*/
                    if (_$draggableItem.position().left > $(this).position().left && _$draggableItem.position().left <= $(this).position().left + $(this).width()) {
                        if ($.inArray($(this).attr('id'), changeableItems.left) == -1) {
                            changeableItems.left.push($(this).attr('id'));
                        }
                    } else if (_$draggableItem.position().left + $(this).width() > $(this).position().left && _$draggableItem.position().left + $(this).width() <= $(this).position().left + $(this).width()) {
                        if ($.inArray($(this).attr('id'), changeableItems.right) == -1) {
                            changeableItems.right.push($(this).attr('id'));
                        }
                    }

                    if (_$draggableItem.position().top > $(this).position().top && _$draggableItem.position().top <= $(this).position().top + $(this).height()) {
                        if ($.inArray($(this).attr('id'), changeableItems.top) == -1 && ($.inArray($(this).attr('id'), changeableItems.left) > -1 || $.inArray($(this).attr('id'), changeableItems.right) > -1)) {
                            changeableItems.top.push($(this).attr('id'));
                        }
                    }
                    if (_$draggableItem.position().top + $(this).height() > $(this).position().top && _$draggableItem.position().top + $(this).height() <= $(this).position().top + $(this).height()) {
                        if ($.inArray($(this).attr('id'), changeableItems.bottom) == -1 && ($.inArray($(this).attr('id'), changeableItems.left) > -1 || $.inArray($(this).attr('id'), changeableItems.right) > -1)) {
                            changeableItems.bottom.push($(this).attr('id'));
                        }
                    }
                });
                return changeableItems;
            }

            /**
             * Calculates draggable item position according to the mouse cursor
             * @param e
             * @returns {{left: number, top: number}}
             * @private
             */
            function _getDraggablePosition(e) {
                return {
                    left: e.pageX - _$allItemsContainer.offset().left - _$draggableHook.position().left - (_$draggableHook.width()/2),
                    top: e.pageY - _$allItemsContainer.offset().top - _$draggableHook.position().top - (_$draggableHook.height()/2)
                }
            }

            /**
             * Searches which itemContainer is free
             * @returns {boolean}
             * @private
             */
            function _getFreeItemContainer() {
                var  $free = false;
                $('.orbitvu-presentation-linked-item-container ').each(function() {
                    if ($(this).children().length == 0) {
                        $free = $(this);
                        return false;
                    }
                });
                return $free;
            }


            /**
             * Searches for min and max order in presentation items
             * @param items
             * @returns {{min: *, max: *}}
             * @private
             */
            function _getMinMaxOrder(items) {
                var min = null,
                    max = null;

                for (var i in items) {
                    if (min == null) {
                        min = items[i].order;
                    } else if (min > items[i].order) {
                        min = items[i].order;
                    }

                    if (max == null) {
                        max = items[i].order;
                    } else if (max < items[i].order) {
                        max = items[i].order;
                    }
                }

                return {
                    min: min,
                    max: max
                }
            }

            /**
             * Updates item position and preforms ajax call
             * @private
             */
            function _updateItemsPosition() {
                var idPresentation = _$draggableItem.closest('.orbitvu-presentation-linked').data('id_presentation'),
                    p = _presentationsLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive,
                    ritems,
                    i = 0,
                    minMax,
                    diff;

                $('#ovpresentation-linked-' + idPresentation).find('.orbitvu-presentation-linked-item').each(function() {
                    $(this).parent().attr('id', 'orbitvu-presentation-linked-item-container-' + $(this).data('id_product_presentation_item'));
                });

                ritems = ractive.get('items');
                $('#ovpresentation-linked-' + idPresentation).find('.orbitvu-presentation-linked-item').each(function() {
                    var obj = {
                        active: $(this).parent().hasClass('orbitvu-presentation-linked-item-container-deactivated') ? 0 : 1,
                        id_product_presentation_item: $(this).data('id_product_presentation_item'),
                        order: $(this).data('order'),
                        ov_name: $(this).data('ov_name'),
                        admin_image: $(this).data('admin_image'),
                        ov_type: $(this).data('ov_type'),
                        date_added: $(this).data('date_added'),
                        date_updated: $(this).data('date_updated'),
                        ov_id: $(this).data('ov_id'),
                        ov_view_url: $(this).data('ov_view_url'),
                        ov_script_url: $(this).data('ov_script_url')
                    }
                    for (var j in obj) {
                        ritems[i][j] = obj[j];
                    }
                    i++;
                });
                ractive.set('items', null);
                ractive.set('items', ritems);
                minMax = _getMinMaxOrder(ritems);
                diff = minMax.min - 10;
                i = 0;
                $('#ovpresentation-linked-' + idPresentation).find('.orbitvu-presentation-linked-item-container').each(function() {
                    ractive.set('items.' + i + '.order', $(this).data('order') + diff);
                    i++;
                });

                _orbitvuAjax.ajaxCall('updateItemsPosition', {
                        items: ractive.get('items'),
                        id_presentation: idPresentation
                    },
                    function() {
                        _presentationsLinked.blockLoading(idPresentation);
                        _presentationsLinked.blockAll();
                        _presentationsNotLinked.blockAll();
                    },
                    function(response) {
                        _presentationsNotLinked.unblockAll();
                        _presentationsLinked.unblockAll();
                    }
                );
            }

            /**********************************
             * MOVE ITEMS HELPER FUNCTIONS END
             **********************************/
            //start or end dragging
            _$ovListLinked.on('mousedown touchstart', '.orbitvu-presentation-actions-move', function(e) {
                var $free;
                //prevent right mouse click
                if (e.which == 3) {
                    return false;
                }
                e.stopPropagation();

                //start dragging
                if (!_isInDragState) {
                    _$draggableHook = $(this);
                    _$draggableItem = $(this).closest('.orbitvu-presentation-linked-item');
                    _$draggableItemContainer = _$draggableItem.parent();
                    _$allItemsContainer = _$draggableItem.closest('.orbitvu-presentation-linked-items');
                    _$allItemsContainer.append(_$draggableItem);
                    _$draggableItem.addClass('orbitvu-draggable');
                    _$draggableItem.css({
                        left: (_$draggableItemContainer.position().left + 7) + 'px',
                        top: (_$draggableItemContainer.position().top + 7) + 'px'
                    })
                    _isInDragState = true;
                } else { //end dragging
                    if ((e.type == 'mousedown') && _changeableItemId) {
                        $free = _getFreeItemContainer();
                        if ($free) {
                            $free.append(_$draggableItem);
                            _$draggableItem.removeClass('orbitvu-draggable');
                            _$draggableItem.css({
                                left: 0,
                                top: 0
                            });
                            _isInDragState = false;
                            _updateItemsPosition();
                        }
                    }
                }
            });
            //move
            _$ovListLinked.on('mousemove touchmove', '.orbitvu-presentation-linked-items', function(e) {
                if (_isInDragState) {
                    if (e.type == 'touchmove') {
                        e.preventDefault();
                        e = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                    }
                    var draggablePosition = _getDraggablePosition(e),
                        $free = _getFreeItemContainer(),
                        changeableItemId,
                        $changeableItemContainer,
                        tempClass;

                    _$draggableItem.css({
                        left: draggablePosition.left + 'px',
                        top: draggablePosition.top + 'px'
                    });
                    changeableItemId = _getChangeableItemId();
                    if (changeableItemId) {
                        if (_changeableItemId === false || _changeableItemId != changeableItemId) {
                            _changeableItemId = changeableItemId;
                            if ($free) {
                                $changeableItemContainer = $('#' + changeableItemId);
                                $free.append($changeableItemContainer.find('.orbitvu-presentation-linked-item:first'));
                                $changeableItemContainer.attr('class');
                                tempClass = $changeableItemContainer.attr('class');
                                $changeableItemContainer.attr('class', $free.attr('class'));
                                $free.attr('class', tempClass);
                            }
                        }
                    }
                }
            });

            //activate|deactivate item
            _$ovListLinked.on('click', '.orbitvu-presentation-actions-activate, .orbitvu-presentation-actions-deactivate', function() {
                var idPresentation = $(this).closest('.orbitvu-presentation-linked').data('id_presentation'),
                    idProductPresentationItem = $(this).data('id_product_presentation_item'),
                    p = _presentationsLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive,
                    itemKey = _presentationsLinked.getItemKey(idPresentation, idProductPresentationItem);

                _orbitvuAjax.ajaxCall('activateDeactivateItem', ractive.get(itemKey),
                    function() {
                        ractive.set(itemKey + '.is_loading', 1);
                        _presentationsLinked.blockItemsAll(idPresentation);
                    },
                    function(response) {
                        ractive.set(itemKey + '.is_loading', 0);
                        _presentationsLinked.unblockItemsAll(idPresentation);
                        ractive.set(itemKey + '.active', response.data.active);
                        _presentationsLinked.blinkItem(idProductPresentationItem, response.data.active ? 'activated' : 'deactivated', response.message);
                    }
                );
            });

            //activate/deactivate all items
            _$ovListLinked.on('click', '.ovactivateall, .ovdeactivateall', function(e) {
                var idPresentation = $(this).data('id_presentation'),
                    p = _presentationsLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive,
                    active = 0;

                e.preventDefault();
                if ($(this).hasClass('ovactivateall')) {
                    active = 1;
                }

                _orbitvuAjax.ajaxCall('activateDeactivateAllItems', {
                        id_presentation: idPresentation,
                        active: active
                    },
                    function() {
                        _presentationsLinked.blockLoading(idPresentation);

                    },
                    function(response) {
                        var items = ractive.get('items');
                        for(var j = 0; j < items.length; j++) {
                            ractive.set('items.' + j + '.active', active);
                            //_presentationsLinked.blinkItem(items[j].id_product_presentation_item, response.data.active ? 'activated' : 'deactivated', response.message);
                        }
                        _presentationsLinked.unblockAll();
                        _presentationsLinked.printDynamicMessage(idPresentation, 'success', response.message);
                    }
                );
            });

            //copy to original images
            _$ovListLinked.on('click', '.orbitvu-presentation-actions-copy', function() {
                var idPresentation = $(this).closest('.orbitvu-presentation-linked').data('id_presentation'),
                    idProductPresentationItem = $(this).data('id_product_presentation_item'),
                    p = _presentationsLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive,
                    itemKey = _presentationsLinked.getItemKey(idPresentation, idProductPresentationItem);

                _orbitvuAjax.ajaxCall('copyImage', ractive.get(itemKey),
                    function() {
                        ractive.set(itemKey + '.is_loading', 1);
                        _presentationsLinked.blockItemsAll(idPresentation);
                    },
                    function(response) {
                        ractive.set(itemKey + '.is_loading', 0);
                        _presentationsLinked.unblockItemsAll(idPresentation);
                        ractive.set(itemKey + '.active', response.data.active);
                        _presentationsLinked.printDynamicMessage(idPresentation, 'success', response.message);
                    }
                );
            });

            //unlink presentation
            _$ovListLinked.on('click', '.ovunlink', function(e) {
                var idPresentation = $(this).data('id_presentation'),
                    pData;

                e.preventDefault();
                _orbitvuAjax.ajaxCall('unlinkPresentation', {
                        id_presentation: idPresentation
                    },
                    function() {
                        _presentationsLinked.blockLoading(idPresentation);
                        _presentationsLinked.blockAll();
                        _presentationsNotLinked.blockAll();
                        if (_pager.Ractive) {
                            _pager.Ractive.set('is_blocked', 1);
                        }
                    },
                    function(response) {
                        _presentationsLinked.removePresentationById(idPresentation);
                        _presentationsLinked.printDynamicMessage(idPresentation, 'success', response.message);
                        _presentationsNotLinked.unblockAll();
                        _presentationsNotLinked.unblockPermanently(idPresentation);
                        if (_pager.Ractive) {
                            _pager.Ractive.set('is_blocked', 0);
                        }
                        if (_$ovListLinked.find('.orbitvu-presentation-linked').length) {
                            _presentationsLinked.unblockAll();
                        } else {
                            _$ovpresentationLinkedListContainer.addClass('orbitvu-hidden');
                        }

                        if (!_pagerInitialized) {
                            _initPager();
                        }
                        if (!_eventsPagerInitialized) {
                            _initPagerEvents();
                        }
                        if (!_eventsNotLinkedInitialized) {
                            _initPresentationsNotLinkedEvents();
                        }
                        pData = _pager.getData();
                        _pager.callPager(pData);
                        if (_multiLink || !_presentationsLinked.length) {
                            _$ovpresentationListContainer.removeClass('orbitvu-hidden');
                        }

                    },
                    function(response) {
                        //TODO
                    }
                );
            });
        }


        /**
         * Adds events for presentations notlinked
         * @private
         */
        function _initPresentationsNotLinkedEvents() {

            _eventsNotLinkedInitialized = true;

            //link presentation
            _$ovList.on('click', '.ovlink', function(e) {
                var idPresentation = $(this).data('id_presentation'),
                    p = _presentationsNotLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive;

                e.preventDefault();
                _orbitvuAjax.ajaxCall('linkPresentation', {id_presentation: idPresentation},
                    function() {
                        _presentationsNotLinked.blockLoading(idPresentation);
                        _presentationsNotLinked.blockAll();
                        _pager.Ractive.set('is_blocked', 1);
                        _presentationsLinked.blockAll();
                    },
                    function(response) {
                        //remove hidden class if necessary
                        _$ovpresentationLinkedListContainer.removeClass('orbitvu-hidden');
                        _presentationsNotLinked.unblockAll();
                        _presentationsLinked.unblockAll();
                        _pager.Ractive.set('is_blocked', 0);
                        if (!_multiLink) {
                            _$ovpresentationListContainer.addClass('orbitvu-hidden');
                        }
                        _presentationsNotLinked.blockPermanently(idPresentation);
                        _presentationsLinked.addPresentation(response.data.presentation);
                        _presentationsLinked.printDynamicMessage(idPresentation, 'success', response.message);
                        if (!_eventsLinkedInitialized) {
                            _initPresentationsLinkedEvents();
                        }
                    },
                    function(response) {
                        var idPresentation = $(this).data('id_presentation'),
                            p = _presentationsNotLinked.getPresentationById(idPresentation),
                            ractive = p.Ractive;
                    }
                )
            });

            //delete presentation, step first
            _$ovList.on('click', '.ovdelete', function(e) {
                var idPresentation = $(this).data('id_presentation'),
                    p = _presentationsNotLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive;

                e.preventDefault();
                ractive.set('delete_ask', 1);
            });

            //delete presentation, step second
            _$ovList.on('click', '.ovdelete-yes, .ovdelete-no', function(e) {
                var idPresentation = $(this).data('id_presentation'),
                    p = _presentationsNotLinked.getPresentationById(idPresentation),
                    ractive = p.Ractive,
                    pData = _pager.getData();

                e.preventDefault();
                if ($(this).hasClass('ovdelete-no')) {
                    ractive.set('delete_ask', 0);
                } else if ($(this).hasClass('ovdelete-yes')) {
                    _orbitvuAjax.ajaxCall('deletePresentation', {id_presentation: idPresentation},
                        function() {
                            _presentationsNotLinked.blockLoading(idPresentation);
                            _presentationsNotLinked.blockAll();
                            _pager.Ractive.set('is_blocked', 1);
                            _presentationsLinked.blockAll();
                        },
                        function(response) {
                            _presentationsLinked.unblockAll();
                            _pager.Ractive.set('is_blocked', 0);
                            pData.clear = 1;
                            _pager.callPager(pData);
                        },
                        function(response) {

                        }
                    )
                }
            });
        }

        /* ********************************************************************************************************** */
        /* *********************************************** Events ends ********************************************** */
        /* ********************************************************************************************************** */

        //public api
        return {
            /**
             * Initialization of ajax url
             * @param url
             */
            initAjaxUrl: function(url) {
                _orbitvuAjax.initAjaxUrl(url);
            },

            /**
             * Initialization method
             * @param presentationsLinked
             * @param presentationsNotLinked
             * @param idProduct
             * @param skuProduct
             * @param multiLink
             */
            init: function(presentationsLinked, presentationsNotLinked, idProduct, skuProduct, multiLink, orbitvuUploadErrorMessages) {
                //data elements
                _idProduct = idProduct;
                _skuProduct = skuProduct;
                _multiLink = parseInt(multiLink),
                _orbitvuUploadErrorMessages = orbitvuUploadErrorMessages;

                //dom elements
                _$ovpresentationLinkedListContainer = $('#ovpresentation-linked-list-container');
                _$ovListLinked = $('#ovlist-linked');
                _$ovpresentationListContainer = $('#ovpresentation-list-container');
                _$ovList = $('#ovlist');
                _$ovPager = $('#ovpager');
                _$ovUploadInput = $('#ov-upload-input');
                _$ovUploadFilesContainer = $('#ovupload-files-container');

                //add helper methods
                _addPagerHelperMethods();
                _addPerentationsLinkedHelperMethods();
                _addPerentationsNotLinkedHelperMethods();

                //set standard ajax data which always is sent to ajax request
                _orbitvuAjax.initStandardData({
                    dataType: 'json',
                    ajax: true,
                    id_product: _idProduct,
                    sku_product: _skuProduct,
                    multi_link: _multiLink
                });
                //init upload events
                _initUploadEvents();

                //init linked presentations
                if (presentationsLinked.length !== 0) {
                    _$ovpresentationLinkedListContainer.removeClass('orbitvu-hidden');
                    _initPresentationsLinked(presentationsLinked);
                    _initPresentationsLinkedEvents();
                }

                //init pager and not linked presentations
                if (_multiLink !== 0 || presentationsLinked.length === 0) {
                    _$ovpresentationListContainer.removeClass('orbitvu-hidden');
                    presentationsNotLinked.results.count = presentationsNotLinked.count;
                    _initPresentationsNotLinked(presentationsNotLinked.results);
                    _initPresentationsNotLinkedEvents();
                    _initPager();
                    _initPagerEvents();
                }
            }
        }
    }());
})(jQuery);