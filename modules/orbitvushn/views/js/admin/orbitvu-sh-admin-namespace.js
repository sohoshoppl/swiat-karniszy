/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

var ORBITVUSH = ORBITVUSH || {};
window['ORBITVUSH'] = ORBITVUSH;
ORBITVUSH.namespace = function (ns_string) {
    var parts = ns_string.split('.'),
        parent = ORBITVUSH,
        i;
    // strip redundant leading global
    if (parts[0] === "ORBITVUSH") {
        parts = parts.slice(1);
    }
    for (i = 0; i < parts.length; i += 1) {
        // create a property if it doesn't exist
        if (typeof parent[parts[i]] === "undefined") {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};