2.1.2 (2017-12-19)
* Fixed zoom magnifier initial position for slow network connections

2.1.1 (2017-11-13)
* Added keyboard arrow control for lightbox
* Orbitvu Viewer update to the version 3.4.1
* Added automatic linking to the product by SKU while uploading Orbitvu presentation from Alphashot Editor or via back office.

2.1.0 (2017-06-21)
* Improved lightbox gallery (added colorized background, added lightbox thumbnails)
* Fixed swipe event in Lightbox
* Added option to enable/disable lightbox buttons shadow after hover
* Improved frontend product gallery (changed thumbnails design and scroll design, improved performance)
* Improved Zoom Magnifier
* Modified "Images 2d copy settings" (copy one frame from 360° presentation if no 2d Images are available)
* Added caption (alt, title) while copying Orbitvu images to native Prestashop images
* Added option to keep original image size for "Zoom/lightbox".
* Default combination fix
* Fix for settings update
* Viewer update

2.0.7 (2017-03-31)
* Fixed error in "Catalog->Products" when module is disabled

2.0.6 (2017-03-03)
* Fixed progress bar when performing migration from old module
* Improved validation of module settings for fields which may spoil frontend product gallery
* Viewer update to version 3.0.25

2.0.5 (2017-02-27)
* Added pinch zoom for 2d images
* Blocked scroll and zoom page fix for mobile events
* Fixed some js errors when 2d images are not present at all in frontend product gallery

2.0.4 (2017-02-15)
* Added possibility of disbaling Zoom Magnifier
* Added lightbox option (for images 2d, 360/3D presentations)
* Added option "Enable/Disable" all items of linked presentation
* Improved SEO of frontend product gallery
* Fixed Prestashop Validator errors for market purposes

2.0.3 (2017-01-20)
* Fixed changing items order (thumbnail bug)
* Fixed urls when presentation contains non-stardard symbols
* Fixed upload when ovus file contains non-standard symbols
* Fixed js error and thumbnails margins when "Thumbnails scroll" option is disabled
* Fixed activate/deactivate item
* Viewer update to 3.0.19

2.0.2 (2016-12-21)
* Orbitvu Viewer update to 3.0.17

2.0.1 (2016-12-21)
* Fixed migration info error when old plugin (< 2.0.0) is deleted or not present

2.0.0 (2016-12-17)
* Initial release of new core version