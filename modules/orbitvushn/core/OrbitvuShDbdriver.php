<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class OrbitvuShDbdriver
{

    /**
     * Default plugin settings
     * @var array
     */
    protected static $default_settings = array(
        'plugin_version' => ORBITVU_PLUGIN_VERSION,
        'remote_upload_key' => '',
        'automatic_link' => '0',
        'multi_link' => '0',
        //0 - do not copy anything, 1 - Copy first thumbnail, 2 - copy all 2d images
        'images2d_copy_type' => '0',
        'show_360' => '1',
        'show_2d' => '1',

        'viewer_mode' => 'html5',
        'gui_style' => 'config_xml',
        // round|square|round_rotation|square_rotation|no_buttons|sh
        'teaser' => 'config_xml',
        'preload_width' => '0',
        'preload_height' => '0',
        'mousewheel' => '1',
        'frame_rate' => '12',

        // Prestashop's specific (image sizes)
        'keep_original_enabled' => '0', //added since 2.1
        'big_width' => '1280px', //added since 2.1
        'big_height' => '960px', //added since 2.1
        'medium_width' => '800px', //added since 2.1
        'medium_height' => '600px', //added since 2.1
        'thumbnail_width' => '80px', //added since 2.1
        'thumbnail_height' => '80px', //added since 2.1

        // Gallery settings
        'thumbs_position' => 'append',
        'scroll' => 'yes',
        'gallery_width' => '100%',
        'main_proportions'  => '4/3',
        'main_border_color'  => '#ccc',
        'main_padding'  => '0',
        'thumbnail_margin' => '5px',
        //'thumbnail_padding' => '10px', removed since 2.1
        //'thumbnail_border_color' => 'transparent', removed since 2.1
        //'thumbnail_active_border_color' => '#ccc', removed since 2.1
        'thumbnail_hover_delay' => '400',
        'button_width' => '25px',
        'button_height' => '40px',
        //'button_opacity' => 0.5, removed since 2.1
        //'button_border_color' => '#ccc', removed since 2.1

        //zoom magnifier
        'zoom_magnifier_enabled' => '1',
        'zoom_magnifier_lenses' => '1',
        //'zoom_magnifier_lenses_size' => '25%', removed since 2.1, lenses size is counted dynamically
        'zoom_magnifier_lenses_border_color' => '#ccc',
        'zoom_magnifier_lenses_box_shadow' => '0 0 5px #ccc',
        'zoom_magnifier_image_opacity_hover' => '0.4',
        'zoom_magnifier_zoom_border_color' => '#ccc',
        'zoom_magnifier_zoom_box_shadow' => '0 0 5px #a4a5a6',
        'zoom_magnifier_zoom_background' => '#fff',
        'zoom_magnifier_maxzoom_enabled' => '0', //added since 2.1
        'zoom_magnifier_maxzoom_width' => '1800px', //added since 2.1
        'zoom_magnifier_maxzoom_height' => '1500px', //added since 2.1

        //lightbox
        'lightbox_enabled' => '1',
        'lightbox_proportions' => '4/3',
        //'lightbox_open_click' => '0', //0 - on thumbnails click, 1 - on main image click, 2 - on both click, removed since 2.1
        'lightbox_colorized_bg' => '1', //added since 2.1
        'lightbox_thumbnails_enabled' => '1', //added since 2.1
        'lightbox_buttons_shadow_enabled' => '1', //added since 2.1
        'lightbox_maxzoom_enabled' => '0', //added since 2.1
        'lightbox_maxzoom_width' => '1800px', //added since 2.1
        'lightbox_maxzoom_height' => '1500px', //added since 2.1

        //cron synch keys
        'auto_synch_key' => '',
        'auto_link_key' => '',
        'auto_synch_lock' => '0',
        'auto_link_lock' => '0',
    );

    protected static $db_settings = null;


    /**
     * @return array
     */
    public static function getDefaultSettings()
    {
        return static::$default_settings;
    }


    /**
     * @param bool|false $refresh
     * @return null
     */
    public static function getDbSettings($refresh = false)
    {
        if (null === static::$db_settings || $refresh) {
            static::$db_settings = unserialize(Configuration::get(ORBITVU_PLUGIN_NAME));
        }

        return static::$db_settings;
    }


    /**
     * @param $key
     * @param $value
     * @throws Exception
     */
    public static function updateSettings($key, $value)
    {
        $settings = static::getDbSettings();
        if (!$settings) {
            throw new \Exception("No settings in database");
        } else {
            static::$db_settings[$key] = $value;
            Configuration::updateValue(ORBITVU_PLUGIN_NAME, serialize(static::$db_settings));
        }
    }


    /**
     * @var DB
     */
    protected $db;


    /**
     * @var
     */
    protected $db_prefix;


    /**
     * OrbitvuShDbdriver constructor.
     * @param $db_link
     */
    public function __construct($db_link)
    {
        $this->db = $db_link;
        $this->db_prefix = _DB_PREFIX_;
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->db->Insert_ID();
    }

    /**
     * @param $sql
     * @param $values
     * @return mixed
     */
    public function query($sql, $values = array())
    {
        if ($values) {
            array_walk($values, function (&$val) {
                $val = pSQL($val);
            });
        }
        $sql = vsprintf($sql, $values);
        return $this->db->execute($sql);
    }


    /**
     * @param $sql
     * @param array $values
     * @return array
     * @throws PrestaShopDatabaseException
     */
    public function fetchAll($sql, $values = array())
    {
        if ($values) {
            array_walk($values, function (&$val) {
                $val = pSQL($val);
            });
            $sql = vsprintf($sql, $values);
        }

        return $this->db->executeS($sql);
    }


    /**
     * @param $sql
     * @param $values
     * @return mixed
     */
    public function fetchRow($sql, $values = array())
    {
        if ($values) {
            array_walk($values, function (&$val) {
                $val = pSQL($val);
            });
            $sql = vsprintf($sql, $values);
        }
        return $this->db->getRow($sql);
    }


    /**
     * Performs sql install script
     */
    public function sqlInstall()
    {

        //insert basic settings
        Configuration::updateValue(ORBITVU_PLUGIN_NAME, serialize(static::$default_settings));

        $remoteUploadKey = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH'].date('Y-m-d H:i:s').' '.mt_rand(1000, 10000)));
        $autoSynchKey = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH'].date('Y-m-d H:i:s').' '.mt_rand(1000, 10000)));
        $autoLinkKey = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH'].date('Y-m-d H:i:s').' '.mt_rand(1000, 10000)));

        static::getDbSettings(true); //this is necessary for "eanble" action for update sql. If not present "No settings in database" error returned!

        static::updateSettings('remote_upload_key', $remoteUploadKey);
        static::updateSettings('auto_synch_key', $autoSynchKey);
        static::updateSettings('auto_link_key', $autoLinkKey);

        //set proper image sizes, Prestashop's specific
        $medium = Image::getSize(ImageType::getFormatedName('large')); //Presta's large image is our medium
        $large = Image::getSize(ImageType::getFormatedName('thickbox'));
        $thumbnail = Image::getSize(ImageType::getFormatedName('cart'));
        static::updateSettings('big_width', ((int)$large['width']).'px');
        static::updateSettings('big_height', ((int)$large['height']).'px');
        static::updateSettings('medium_width', ((int)$medium['width']).'px');
        static::updateSettings('medium_height', ((int)$medium['height']).'px');
        static::updateSettings('thumbnail_width', ((int)$thumbnail['width']).'px');
        static::updateSettings('thumbnail_height', ((int)$thumbnail['height']).'px');

        //custom tables
        $sql = "CREATE TABLE IF NOT EXISTS `".$this->db_prefix."orbitvu_sh_presentation` (
                  `id_presentation` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
                  `folder_name` VARCHAR(255) NOT NULL COMMENT '',
                  `content` LONGTEXT NULL COMMENT '',
                  `parsed_content` LONGTEXT NULL COMMENT '',
                  `cache_hash` VARCHAR(45) NULL COMMENT '',
                  `ov_name` VARCHAR(255) NULL COMMENT '',
                  `ov_sku` VARCHAR(255) NULL COMMENT '',
                  `has_2d` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '',
                  `has_360` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '',
                  `has_orbittour` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '',
                  `has_video` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '',
                  `date_added` DATETIME NULL COMMENT '',
                  `date_updated` DATETIME NULL COMMENT '',
                  PRIMARY KEY (`id_presentation`)  COMMENT '',
                  UNIQUE INDEX `folder_name_UNIQUE` (`folder_name` ASC)  COMMENT '')
                ENGINE = InnoDB;";
        if (!$this->db->execute($sql)) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `".$this->db_prefix."orbitvu_sh_product_presentation` (
                  `id_product` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `id_presentation` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `linked_hash` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
                  `order` INT(11) NULL DEFAULT NULL COMMENT '',
                  `active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '',
                  `date_added` DATETIME NOT NULL COMMENT '',
                  `date_updated` DATETIME NOT NULL COMMENT '',
                  PRIMARY KEY (`id_product`, `id_presentation`)  COMMENT '',
                  INDEX `fk_wp_orbitvu_sh_product_presentation_wp_orbitvu_sh_present_idx` (`id_presentation` ASC)  COMMENT '',
                  CONSTRAINT `fk_wp_orbitvu_sh_product_presentation_wp_orbitvu_sh_presentat1`
                    FOREIGN KEY (`id_presentation`)
                    REFERENCES `".$this->db_prefix."orbitvu_sh_presentation` (`id_presentation`)
                    ON DELETE CASCADE
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;";
        if (!$this->db->execute($sql)) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `".$this->db_prefix."orbitvu_sh_product_presentation_item` (
                  `id_product_presentation_item` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
                  `id_product` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `id_presentation` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `ov_name` VARCHAR(255) NULL DEFAULT NULL COMMENT '',
                  `ov_type` ENUM('ov2d','ov360','ovtour','ovvideo') NOT NULL COMMENT '',
                  `ov_thumbnail_file` VARCHAR(255) NOT NULL COMMENT '',
                  `order` INT(11) NULL DEFAULT NULL COMMENT '',
                  `active` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '',
                  `date_added` DATETIME NOT NULL COMMENT '',
                  `date_updated` DATETIME NOT NULL COMMENT '',
                  PRIMARY KEY (`id_product_presentation_item`)  COMMENT '',
                  INDEX `fk_wp_orbitvu_sh_product_presentation_item_wp_orbitvu_sh_pr_idx` (`id_product` ASC, `id_presentation` ASC)  COMMENT '',
                  CONSTRAINT `fk_wp_orbitvu_sh_product_presentation_item_wp_orbitvu_sh_prod1`
                    FOREIGN KEY (`id_product` , `id_presentation`)
                    REFERENCES `".$this->db_prefix."orbitvu_sh_product_presentation` (`id_product` , `id_presentation`)
                    ON DELETE CASCADE
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB AUTO_INCREMENT = 77 DEFAULT CHARACTER SET = utf8;";
        if (!$this->db->execute($sql)) {
            return false;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `".$this->db_prefix."orbitvu_sh_product_presentation_history` (
                  `id_product_presentation_history` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
                  `id_product` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `id_presentation` BIGINT(20) UNSIGNED NOT NULL COMMENT '',
                  `unlink_date` DATETIME NOT NULL COMMENT '',
                  PRIMARY KEY (`id_product_presentation_history`)  COMMENT '')
                ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;";
        if (!$this->db->execute($sql)) {
            return false;
        }

        //install sample presentations
        if (file_exists(ORBITVU_PRESENTATIONS_PATH."maxi_3d_kit")) {
            $parsedPresentation = OrbitvuShDir::parseLocalPresentationContent("maxi_3d_kit");
            if ($parsedPresentation) {
                $this->addPresentation($parsedPresentation);
            }
        }

        if (file_exists(ORBITVU_PRESENTATIONS_PATH."samsung_gear_fit_black")) {
            $parsedPresentation = OrbitvuShDir::parseLocalPresentationContent("samsung_gear_fit_black");
            if ($parsedPresentation) {
                $this->addPresentation($parsedPresentation);
            }
        }

        return true;
    }

    public function sqlUninstall()
    {
        $this->db->execute('DROP TABLE IF EXISTS `'.$this->db_prefix.'orbitvu_sh_product_presentation_history`');
        $this->db->execute('DROP TABLE IF EXISTS `'.$this->db_prefix.'orbitvu_sh_product_presentation_item`');
        $this->db->execute('DROP TABLE IF EXISTS `'.$this->db_prefix.'orbitvu_sh_product_presentation`');
        $this->db->execute('DROP TABLE IF EXISTS `'.$this->db_prefix.'orbitvu_sh_presentation`');

        $tab = $this->db->getRow('SELECT * FROM `'.$this->db_prefix.'tab` WHERE `module` = "'.ORBITVU_PLUGIN_NAME.'"');
        if ($tab && isset($tab['id_tab'])) {
            $this->db->execute('DELETE FROM `'.$this->db_prefix.'tab` WHERE `id_tab` = '.pSQL($tab['id_tab']));
            $this->db->execute('DELETE FROM `'.$this->db_prefix.'tab_lang` WHERE `id_tab` = '.pSQL($tab['id_tab']));
        }
        return true;
    }

    public function sqlUpdate($old_version, $new_version)
    {
        $dir = _PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'sql-update';
        $files = scandir($dir);
        foreach ($files as $file) {
            if (strpos($file, 'sql-update-') !== false) {
                $ver = explode("-", str_replace(array('sql-update-', '.php'), '', $file));
                if (count($ver) == 2) {
                    if (version_compare($ver[0], $old_version) >= 0 && version_compare($ver[1], $new_version) <= 0) {
                        //do action update
                        $sql_file = $dir.DIRECTORY_SEPARATOR.$file;
                        if (file_exists($sql_file)) {
                            include_once $sql_file;
                        }
                    }
                }
            }
        }

        //Prestashop's specific - copy new Orbitvu Viewer version on module update
        if (version_compare($old_version, $new_version) == -1) {
            $tmpInstallDir = _PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_tmp_install'.DIRECTORY_SEPARATOR;
            $viewerPathZip = $tmpInstallDir."orbitvu12.zip";
            $viewerPathExtract = _PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR."_orbitvu_viewer";

            //save old mask
            $oldmask = umask(0);
            //update viewer
            if (!file_exists($viewerPathExtract)) {
                mkdir($viewerPathExtract, 0775, true);
            }
            if (is_writable($viewerPathExtract)) {
                $zip = new ZipArchive();
                if ($zip->open($viewerPathZip) === true) {
                    $zip->extractTo($viewerPathExtract);
                    $zip->close();
                }
            }
            //restore old mask
            umask($oldmask);
        }
        //Prestashop's  specific - copy new Orbitvu Viewer version on module update ends

        static::updateSettings('plugin_version', $new_version);
    }


    /**************************************************************
     * QUERIES NEEDED FOR MIGRATION FROM OLD MODULE TO THE NEW ONE
     **************************************************************/
    /**
     * Needed to check if migration from 1.0.x, 1.1.x, 1.2.x to 2.0.x is available
     *
     * @return mixed
     */
    public function getOldModuleCredentials()
    {
        $sql = "SELECT * FROM `".$this->db_prefix."module` WHERE name = 'orbitvush'";
        return $this->fetchRow($sql);
    }

    /**
     * Please note that this may return many ids because of cache bug in old module
     *
     * @param $presentationName
     * @return array
     */
    public function getOldModulePresentationId($presentationName)
    {
        $sql = "SELECT id as id_presentation FROM `".$this->db_prefix."orbitvu_sh_products_presentations_cache` WHERE `name` = '%s'";
        $val = $this->fetchRow($sql, array($presentationName));
        if ($val) {
            return $val['id_presentation'];
        }
        return false;
    }


    public function getOldModuleProductsWithLinkedPresentations()
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_products_presentations`";
        $products = $this->fetchAll($sql);
        $properProducts = array();
        foreach ($products as $product) {
            $key = $product['product_id'];
            $properProducts[$key] = array();
            if ($product['config']) {
                $obj = unserialize($product['config']);
                $i = count($obj->child_presentations) - 1;
                for ($i; $i >= 0; $i--) {
                    $childPresentationId = $obj->child_presentations[$i];
                    $properProducts[$key][$childPresentationId] = array();
                }
            }
            $properProducts[$key][$product['orbitvu_id']] = array();

            $items = $this->getOldModuleLinkedPresentationItems($product['id']); //this is not a product id but linkId...Gooosh!!
            foreach ($items as $item) {
                if (!$item['config'] || !property_exists(Tools::jsonDecode($item['config']), 'real_presentation_id')) {
                    $properProducts[$key][$product['orbitvu_id']][] = array(
                        'ov_type' => !$item['name'] ? 'ov360' : 'ov2d',
                        'ov_name' => $item['name'],
                        'active' => $item['status'] == 'active' ? 1 : 0,
                        'order' => count($properProducts[$key][$product['orbitvu_id']]) == 0 ? 10 : (count($properProducts[$key][$product['orbitvu_id']]) + 1) * 10
                    );
                } else {
                    $obj = Tools::jsonDecode($item['config']);
                    $properProducts[$key][$obj->real_presentation_id][] = array(
                        'ov_type' => !$item['name'] ? 'ov360' : 'ov2d',
                        'ov_name' => $item['name'],
                        'active' => $item['status'] == 'active' ? 1 : 0,
                        'order' => count($properProducts[$key][$obj->real_presentation_id]) == 0 ? 10 : (count($properProducts[$key][$obj->real_presentation_id]) + 1) * 10
                    );
                }
            }
        }
        return $properProducts;
    }

    public function getOldModuleLinkedPresentationItems($linkedId)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_products_presentations_items` WHERE `_presentations_id` = %d ORDER BY `priority` DESC";
        return $this->fetchAll($sql, array($linkedId));
    }

    /**
     * @param $presentationId
     * @return mixed
     */
    public function getOldModulePresentationFolderName($presentationId)
    {
        $sql = "SELECT `name` as folder_name FROM `".$this->db_prefix."orbitvu_sh_products_presentations_cache` WHERE id = %d";
        return $this->fetchRow($sql, array($presentationId));
    }
    /************************************************************
     * ********************* END OF QUERIES *********************
     ************************************************************/

    /**
     * Get Presentation folder names
     * @return array
     */
    public function getPresentationFolderNames()
    {
        $folderNames = array();
        $sql = "SELECT folder_name FROM `".$this->db_prefix."orbitvu_sh_presentation`";
        foreach ($this->fetchAll($sql) as $result) {
            $folderNames[] = $result['folder_name'];
        };

        return $folderNames;
    }


    /**
     * Get all cached presentations
     *
     * @return mixed
     */
    public function getAllPresentations()
    {
        $sql = "SELECT id_presentation, folder_name, parsed_content, cache_hash, ov_name, ov_sku FROM `".$this->db_prefix."orbitvu_sh_presentation`";

        return $this->fetchAll($sql);
    }

    /**
     * Delete specific presentation by folderName
     *
     * @param $folderName
     * @return mixed
     */
    public function deletePresentaionByFolderName($folderName)
    {
        $sql = "DELETE FROM `".$this->db_prefix."orbitvu_sh_presentation` where folder_name = '%s'";

        return $this->query($sql, array($folderName));
    }

    /**
     * Get presentation by folderName
     * Get presentation by folderName
     *
     * @param $folderName
     * @return mixed
     */
    public function getPresentationByFolderName($folderName)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_presentation` WHERE folder_name = '%s'";
        return $this->fetchRow($sql, array($folderName));
    }

    /**
     * Adds new presentation to the db table
     *
     * @param $data
     * @return mixed
     */
    public function addPresentation($data)
    {
        $sql = "INSERT INTO `".$this->db_prefix."orbitvu_sh_presentation` (
            folder_name,
            content,
            parsed_content,
            cache_hash,
            ov_name,
            ov_sku,
            has_2d,
            has_360,
            has_orbittour,
            has_video,
            date_added,
            date_updated
        ) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %d, %d, '%s', '%s')";

        $date = date('Y-m-d H:i:s');

        return $this->query(
            $sql,
            array(
                $data['folder_name'],
                $data['content'],
                $data['parsed_content'],
                $data['cache_hash'],
                $data['ov_name'],
                $data['ov_sku'],
                $data['has_2d'],
                $data['has_360'],
                $data['has_orbittour'],
                $data['has_video'],
                $date,
                $date
            )
        );
    }

    /**
     * @param $idPresentation
     * @param $data
     * @return mixed
     */
    public function updatePresentation($idPresentation, $data)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_presentation` SET
            `folder_name` = '%s',
            `content` = '%s',
            `parsed_content` = '%s',
            `cache_hash` = '%s',
            `ov_name` = '%s',
            `ov_sku` = '%s',
            `has_2d` = %d,
            `has_360` = %d,
            `has_orbittour` = %d,
            `has_video` = %d,
            `date_updated` = '%s'
            WHERE id_presentation = %d";

        $date = date('Y-m-d H:i:s');

        return $this->query(
            $sql,
            array(
                $data['folder_name'],
                $data['content'],
                $data['parsed_content'],
                $data['cache_hash'],
                $data['ov_name'],
                $data['ov_sku'],
                $data['has_2d'],
                $data['has_360'],
                $data['has_orbittour'],
                $data['has_video'],
                $date,
                $idPresentation
            )
        );
    }

    /**
     * Retreives presentations list
     *
     * @param array $params
     * @return mixed
     */
    public function getPresentationsList($params = array())
    {
        $params = array_merge(array('current_page' => 1, 'page_size' => 20, 'ordering' => '-create_date', 'search' => ''), $params);

        //First count items
        if ($params['search']) {
            $sql = "SELECT count(*) as no_presentations FROM `".$this->db_prefix."orbitvu_sh_presentation` WHERE (ov_name LIKE '%%%s%%' OR ov_sku LIKE '%%%s%%')";
            $no = $this->fetchRow($sql, array($params['search'], $params['search']));
        } else {
            $sql = "SELECT count(*) as no_presentations FROM `".$this->db_prefix."orbitvu_sh_presentation`";
            $no = $this->fetchRow($sql);
        }

        if ($params['ordering'] == 'create_date' || $params['ordering'] == '-create_date') {
            $params['ordering'] = 'date_added '.(($params['ordering'] == 'create_date') ? 'ASC' : 'DESC');
        } elseif ($params['ordering'] == 'sku' || $params['ordering'] == '-sku') {
            $params['ordering'] = 'ov_sku '.(($params['ordering'] == 'sku') ? 'ASC' : 'DESC');
        } elseif ($params['ordering'] == 'name' || $params['ordering'] == '-name') {
            $params['ordering'] = 'ov_name '.(($params['ordering'] == 'name') ? 'ASC' : 'DESC');
        } else {
            $params['ordering'] = 'date_added ASC';
        }

        if ($params['search']) {
            $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_presentation` WHERE (ov_name LIKE '%%%s%%' OR ov_sku LIKE '%%%s%%') ORDER BY ".$params['ordering']." LIMIT %d, %d";
            $presentations = $this->fetchAll($sql, array($params['search'], $params['search'], ($params['current_page'] - 1) * $params['page_size'], $params['page_size']));
        } else {
            $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_presentation` ORDER BY ".$params['ordering']." LIMIT %d, %d";
            $presentations = $this->fetchAll($sql, array(($params['current_page'] - 1) * $params['page_size'], $params['page_size']));
        }

        foreach ($presentations as $k => $p) {
            if (!OrbitvuShDir::hashIsEqual($p['cache_hash'], $p['folder_name'])) {
                $presentations[$k]['different_hashes'] = true; //it means that presentation in db is not in real state with local files.
                //we cannot perform synchronization from here, becuase it could interfere pagination (for example when presentation files are deleted)
            } else {
                $presentations[$k]['different_hashes'] = false;
                //let's parse it for display
                $presentations[$k] = $this->parsePresentation($p);
            }
        }

        $returnResults = array(
            'count' => $no['no_presentations'],
            'results' => $presentations
        );

        return $returnResults;
    }

    /**
     * @param $idPresentation
     * @return mixed
     */
    public function getPresentationById($idPresentation)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_presentation` WHERE id_presentation = %d";

        $presentation = $this->fetchRow($sql, array($idPresentation));
        if ($presentation) {
            return $this->parsePresentation($presentation);
        }
        return false;
    }


    /**
     * Parses presentation for display
     *
     * @param $presentation
     * @return mixed
     */
    public function parsePresentation($presentation)
    {
        $presentation['admin_image_small'] = OrbitvuShDir::getPresentationAdminImage($presentation['folder_name'], $presentation['parsed_content']);
        $presentation['admin_image_big'] = OrbitvuShDir::getPresentationAdminImage($presentation['folder_name'], $presentation['parsed_content'], 'big');
        $presentation['has_2d'] = $presentation['has_2d'] ? true : false;
        $presentation['has_360'] = $presentation['has_360'] ? true : false;
        $presentation['has_orbittour'] = $presentation['has_orbittour'] ? true : false;
        $presentation['has_video'] = $presentation['has_video'] ? true : false;
        return $presentation;
    }


    /**
     * Parses presentation array of items or one item for display
     *
     * @param $itemsOrItem
     * @return mixed
     */
    public function parseItems($itemsOrItem)
    {
        $idPresentation = isset($itemsOrItem[0]) ? $itemsOrItem[0]['id_presentation'] : false;
        if ($idPresentation !== false) {
            $presentation = $this->getPresentationById($idPresentation);
            foreach ($itemsOrItem as $k => $item) {
                $itemsOrItem[$k]['admin_image'] = OrbitvuShDir::getItemAdminImage($presentation['folder_name'], $item);
                $itemsOrItem[$k]['original_image'] = OrbitvuShDir::getItemOriginalImage($presentation['folder_name'], $item);
                $itemsOrItem[$k]['original_image_file_path'] = OrbitvuShDir::getItemOriginalImage($presentation['folder_name'], $item, true); //Presta specific
            }
        } else {
            $idPresentation = isset($itemsOrItem['id_presentation']) ? $itemsOrItem['id_presentation'] : false;
            if ($idPresentation !== false) {
                $presentation = $this->getPresentationById($idPresentation);
                $itemsOrItem['admin_image'] = OrbitvuShDir::getItemAdminImage($presentation['folder_name'], $itemsOrItem);
                $itemsOrItem['original_image'] = OrbitvuShDir::getItemOriginalImage($presentation['folder_name'], $itemsOrItem);
                $itemsOrItem['original_image_file_path'] = OrbitvuShDir::getItemOriginalImage($presentation['folder_name'], $itemsOrItem, true); //Presta specific
            }
        }
        return $itemsOrItem;
    }


    /**
     * This synchronizes all linked presentations which cache has been changed
     *
     * @param $idPresentation
     * @param $parsedLocalPresentation
     */
    public function synchronizePresentation($idPresentation, $parsedLocalPresentation)
    {
        $presentationsDb = $this->getPresentationsById($idPresentation);
        $parsedPresentationItemIds = array();
        $parsedPresentationItems = array();
        $parsedContent = Tools::jsonDecode($parsedLocalPresentation['parsed_content'], true);
        foreach ($parsedContent as $key => $type) {
            if (($key == "images2d" || $key == "sequence360") && is_array($type) && count($type)) {
                foreach ($type as $item) {
                    $parsedPresentationItemIds[] = $item['ov_internal_id'];
                    $parsedPresentationItems[] = $item;
                }
            }
        }

        foreach ($presentationsDb as $pr) {
            $itemIds = array();
            foreach ($pr['items'] as $item) {
                if ($item['id_presentation'] == $idPresentation) {
                    $fileName = pathinfo($item['ov_thumbnail_file'], PATHINFO_FILENAME); // filename without extension
                    $ext = pathinfo($item['ov_thumbnail_file'], PATHINFO_EXTENSION); // jpg|gif|png
                    $ovInternalId = $item['ov_type']."_".$fileName."_".$ext;
                    if (!in_array($ovInternalId, $parsedPresentationItemIds)) {
                        $this->deleteItem($item['id_product_presentation_item']);
                        $itemPathAdmin = ORBITVU_PRESENTATIONS_PATH.$parsedLocalPresentation['folder_name'].DIRECTORY_SEPARATOR."_orbitvu_resized_images".DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."items".DIRECTORY_SEPARATOR.$ovInternalId;
                        $itemPathFront = ORBITVU_PRESENTATIONS_PATH.$parsedLocalPresentation['folder_name'].DIRECTORY_SEPARATOR."_orbitvu_resized_images".DIRECTORY_SEPARATOR."front".DIRECTORY_SEPARATOR."items".DIRECTORY_SEPARATOR.$ovInternalId;
                        if (file_exists($itemPathAdmin)) {
                            OrbitvuShDir::rrmdir($itemPathAdmin);
                        }
                        if (file_exists($itemPathFront)) {
                            OrbitvuShDir::rrmdir($itemPathAdmin);
                        }
                    } else {
                        $itemIds[] = $ovInternalId;
                    }
                }
            }

            $this->cleanUpOrdering($pr['id_product']);

            //add items
            $addItems = array_diff($parsedPresentationItemIds, $itemIds);
            foreach ($parsedPresentationItems as $item) {
                if (in_array($item['ov_internal_id'], $addItems)) {
                    //let's count proper item order
                    $orderItem = 10;
                    $greaterPresentations = $this->getPresentationsByOrder($pr['id_product'], $pr['order']);
                    $lowerPresentations = $this->getPresentationsByOrder($pr['id_product'], $pr['order'], "<");
                    $result = $this->getPresentationItemMaxOrder($pr['id_product'], $pr['id_presentation']);
                    if ($result) { //presentation has other items
                        $orderItem = $result['order'] + 10;
                    } else { //presentation is empty
                        if ($lowerPresentations) {
                            foreach ($lowerPresentations as $lpr) {
                                foreach ($lpr['items'] as $litem) {
                                    $orderItem += 10;
                                }
                            }
                        }
                    }
                    if ($greaterPresentations) {
                        foreach ($greaterPresentations as $gpr) {
                            foreach ($gpr['items'] as $gitem) {
                                $this->updateItemOrder($gitem['id_product_presentation_item'], $gitem['order'] + 10);
                            }
                        }
                    }
                    //end count item order

                    $itemToAdd = array(
                        'id_product' => $pr['id_product'],
                        'id_presentation' => $pr['id_presentation'],
                        'ov_name' => $item['ov_name'],
                        'ov_type' => $item['ov_type'],
                        'ov_thumbnail_url' => $item['ov_thumbnail_url'],
                        'order' => $orderItem,
                        'active' => 1
                    );
                    $this->addProductPresentationItem($itemToAdd);
                }
            }
            //update hash for presentation
            $this->updateLinkedPresentationHash($pr['id_presentation'], $parsedLocalPresentation['cache_hash']);
        }
        //update presentation
        $this->updatePresentation($idPresentation, $parsedLocalPresentation);
        return $parsedLocalPresentation;
    }

    public function updateLinkedPresentationHash($idPresentation, $newHash)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation` SET `linked_hash` = '%s' WHERE id_presentation = %d";

        return $this->query($sql, array($newHash, $idPresentation));
    }

    /**
     * Count number of presentations linked to the product
     *
     * @param $id_product
     * @return mixed
     */
    public function countProductPresentations($id_product)
    {
        $sql = "SELECT count(*) as no_presentations FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_product = %d";

        $no = $this->fetchRow($sql, array($id_product));

        return $no['no_presentations'];
    }

    /**
     * @param $id_product
     * @return mixed
     */
    public function getProductPresentations($id_product)
    {
        $sql = "SELECT pp.*, p.* FROM `".$this->db_prefix."orbitvu_sh_product_presentation` pp
         INNER JOIN `".$this->db_prefix."orbitvu_sh_presentation` p ON pp.id_presentation = p.id_presentation WHERE pp.id_product = %d ORDER BY pp.`date_added` ASC";

        $presentations = $this->fetchAll($sql, array($id_product));
        foreach ($presentations as $k => $presentation) {
            if (!OrbitvuShDir::hashIsEqual($presentation['cache_hash'], $presentation['folder_name']) || !OrbitvuShDir::hashIsEqual($presentation['linked_hash'], $presentation['folder_name'])) {
                $presentations[$k]['different_hashes'] = true;
            } else {
                $presentations[$k]['different_hashes'] = false;
            }
        }
        return  $presentations;
    }


    /**
     * @param $id_product
     * @param $order
     * @param string $compare_sign
     * @return mixed
     */
    public function getPresentationsByOrder($id_product, $order, $compare_sign = ">")
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_product = %d AND `order` ".$compare_sign." %d ";

        $presentations =  $this->fetchAll($sql, array($id_product, $order));
        foreach ($presentations as $k => $presentation) {
            $presentations[$k]['items'] = $this->getItems($presentation['id_product'], $presentation['id_presentation']);
        }
        return $presentations;
    }

    /**
     * @param $id_product
     * @param $id_presentation
     * @return mixed
     */
    public function getPresentationItemMaxOrder($id_product, $id_presentation)
    {
        $sql = "SELECT `order` FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product = %d AND id_presentation = %d ORDER BY `order` DESC";

        return $this->fetchRow($sql, array($id_product, $id_presentation));
    }

    /**
     * @param $id_product
     */
    public function cleanUpOrdering($id_product)
    {
        $items = $this->getProductItems($id_product);
        $order = 10;
        foreach ($items as $item) {
            $this->updateItemOrder($item['id_product_presentation_item'], $order);
            $order += 10;
        }
    }

    /**
     * @param $id_item
     * @param $order
     * @return mixed
     */
    public function updateItemOrder($id_item, $order)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation_item` SET `order` = %d, date_updated = '%s' WHERE id_product_presentation_item = %d";

        return $this->query($sql, array($order, date("Y-m-d H:i:s"), $id_item));
    }

    /**
     * @param $id_presentation
     * @return mixed
     */
    public function getPresentationsById($id_presentation)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_presentation = %d ORDER BY `date_added` ASC";

        $presentations =  $this->fetchAll($sql, array($id_presentation));
        foreach ($presentations as $k => $presentation) {
            $presentations[$k]['items'] = $this->getProductItems($presentation['id_product']);
        }
        return $presentations;
    }

    /**
     * @param $id_product
     * @return mixed
     */
    public function getProductItems($id_product)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product = %d ORDER BY `order` ASC";

        return $this->fetchAll($sql, array($id_product));
    }

    /**
     * @param $id_product
     * @param $id_presentation
     * @return mixed
     */
    public function getItems($id_product, $id_presentation)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product = %d AND id_presentation = %d ORDER BY `order` ASC";

        return $this->fetchAll($sql, array($id_product, $id_presentation));
    }

    /**
     * Get products ids which are linked to the specific presentation id
     *
     * @param $idPresentation
     * @return mixed
     */
    public function getLinkedIdsProductsByPresentationId($idPresentation)
    {
        $sql = "SELECT id_product FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_presentation = %d ORDER BY `date_added` ASC";
        return $this->fetchAll($sql, array($idPresentation));
    }

    /**
     * @param $id_item
     * @return mixed
     */
    public function deleteItem($id_item)
    {
        $sql = "DELETE FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product_presentation_item = %d";

        return $this->query($sql, array($id_item));
    }

    /**
     * Count number of items linked to the product
     *
     * @param $id_product
     * @return mixed
     */
    public function countProductItems($id_product)
    {
        $sql = "SELECT count(*) as no_items FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product = %d";

        $no = $this->fetchRow($sql, array($id_product));

        return $no['no_items'];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addProductPresentation($data)
    {
        $sql = "INSERT INTO `".$this->db_prefix."orbitvu_sh_product_presentation` (
            id_product,
            id_presentation,
            linked_hash,
            `order`,
            active,
            date_added,
            date_updated
        ) VALUES (%d, %d, '%s', %d, %d, '%s', '%s')";

        $date = date('Y-m-d H:i:s');

        return $this->query(
            $sql,
            array(
                $data['id_product'],
                $data['id_presentation'],
                $data['linked_hash'],
                $data['order'],
                $data['active'],
                $date,
                $date
            )
        );
    }


    /**
     * @param $data
     * @return mixed
     */
    public function addProductPresentationItem($data)
    {
        $sql = "INSERT INTO `".$this->db_prefix."orbitvu_sh_product_presentation_item` (
            id_product,
            id_presentation,
            ov_name,
            ov_type,
            ov_thumbnail_file,
            `order`,
            active,
            date_added,
            date_updated
        ) VALUES (%d, %d, '%s', '%s', '%s', %d, %d, '%s', '%s')";

        $date = date('Y-m-d H:i:s');

        return $this->query(
            $sql,
            array(
                $data['id_product'],
                $data['id_presentation'],
                $data['ov_name'],
                $data['ov_type'],
                $data['ov_thumbnail_file'],
                $data['order'],
                $data['active'],
                $date,
                $date
            )
        );
    }

    /**
     * Added since 2.1.1, this method was created to avoid code duplication while linking the presentation in RemoteController and AdminController
     *
     * @param $prestaProduct
     * @param $productPresentation
     * @param $context
     * @param int $orderItem
     * @return array
     */
    public function linkPresentation($prestaProduct, $productPresentation, $context, $orderItem = 10)
    {
        $ovSettings = static::getDbSettings();
        $this->addProductPresentation($productPresentation);

        //add product presentation items
        $productPresentationItems = array();
        $i = 0;
        $parsedContent = json_decode($productPresentation['parsed_content'], true);
        foreach ($parsedContent as $key => $type) {
            if (($key == "images2d" || $key == "sequence360") && is_array($type) && count($type)) {
                foreach ($type as $item) {
                    $productPresentationItems[$i] = array(
                        'id_product' => $prestaProduct->id,
                        'id_presentation' => $productPresentation['id_presentation'],
                        'ov_name' => $item['ov_name'],
                        'ov_type' => $item['ov_type'],
                        'ov_thumbnail_file' => $item['ov_thumbnail_file'],
                        'order' => $orderItem,
                        'active' => 1
                    );
                    $this->addProductPresentationItem($productPresentationItems[$i]);
                    $productPresentationItems[$i]['id_product_presentation_item'] = $this->lastInsertId();
                    $orderItem += 10;
                    $i++;
                }
            }
        }

        $productPresentationItems = $this->parseItems($productPresentationItems);

        //copy 2D images as original
        if ($ovSettings['images2d_copy_type'] == 1 && !(int)count($prestaProduct->getImages($context->language->id, $context))) { //Copy first thumbnail without asking me (if product has no original images)
            $copied = false;
            foreach ($productPresentationItems as $k => $item) {
                if ($item['ov_type'] == 'ov2d') {
                    if ($this->copyAsOriginal($item, true)) {
                        $this->deactivateItem($item['id_product_presentation_item']);
                        $productPresentationItems[$k]['active'] = 0;
                        $copied = true;
                    }
                    break;
                }
            }
            if (!$copied) { //if no 2d images present, copy one frame from 360 presentation
                foreach ($productPresentationItems as $k => $item) {
                    if ($item['ov_type'] == 'ov360') {
                        $this->copyAsOriginal($item, true);
                        break;
                    }
                }
            }
        } elseif ($ovSettings['images2d_copy_type'] == 2) { //Always copy all Orbitvu 2D images
            $copied = false;
            foreach ($productPresentationItems as $k => $item) {
                if ($item['ov_type'] == 'ov2d') {
                    if ($this->copyAsOriginal($item, !(int)count($prestaProduct->getImages($context->language->id, $context)) ? true : false)) {
                        $this->deactivateItem($item['id_product_presentation_item']);
                        $productPresentationItems[$k]['active'] = 0;
                        $copied = true;
                    }
                }
            }
            if (!$copied) { //if no 2d images present, copy one frame from 360 presentations
                foreach ($productPresentationItems as $k => $item) {
                    if ($item['ov_type'] == 'ov360') {
                        $this->copyAsOriginal($item, !(int)count($prestaProduct->getImages($context->language->id, $context)) ? true : false);
                    }
                }
            }
        }

        return array_merge($productPresentation, array('items' => $productPresentationItems));
    }

    /**
     * @param $id_product
     * @param $id_presentation
     * @return mixed
     */
    public function unlinkProductPresentation($id_product, $id_presentation)
    {
        $sql = "DELETE FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_product = %d AND id_presentation = %d";
        $this->query($sql, array($id_product, $id_presentation));

        /**
         * Prestashop's specific - Prestashop's 1 click upgrade deletes foreign keys constraints...GOOOSH!!, so this is just in case
         */
        $sql = "DELETE FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product = %d AND id_presentation = %d";
        $this->query($sql, array($id_product, $id_presentation));
        /**
         * Prestashop's specific ends
         */

        return true;
    }

    /**
     * @param $id_product
     * @param $id_presentation
     * @return mixed
     */
    public function getProductPresentation($id_product, $id_presentation)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation` WHERE id_product = %d AND id_presentation = %d";

        return $this->fetchRow($sql, array($id_product, $id_presentation));
    }

    /**
     * @param $id_product
     * @param $id_presentation
     * @return mixed
     */
    public function addProductPresentationHistory($id_product, $id_presentation)
    {
        $sql = "INSERT INTO `".$this->db_prefix."orbitvu_sh_product_presentation_history` (
            id_product,
            id_presentation,
            unlink_date
        ) VALUES (%d, %d, '%s')";

        $date = date('Y-m-d H:i:s');

        return $this->query(
            $sql,
            array(
                $id_product,
                $id_presentation,
                $date
            )
        );
    }

    public function clearUnlinkHistory()
    {
        $sql = "DELETE FROM `".$this->db_prefix."orbitvu_sh_product_presentation_history` WHERE id_product_presentation_history > %d";
        return $this->query($sql, array(0));
    }

    /**
     * @param $id_item
     * @return mixed
     */
    public function getProductPresentationItem($id_item)
    {
        $sql = "SELECT * FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` WHERE id_product_presentation_item = %d ORDER BY `order` ASC";

        return $this->fetchRow($sql, array($id_item));
    }

    /**
     * @param $id_item
     * @return mixed
     */
    public function deactivateItem($id_item)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation_item` SET `active` = %d, date_updated = '%s' WHERE id_product_presentation_item = %d";

        return $this->query($sql, array(0, date("Y-m-d H:i:s"), $id_item));
    }

    /**
     * @param $id_item
     * @return mixed
     */
    public function activateItem($id_item)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation_item` SET `active` = %d, date_updated = '%s' WHERE id_product_presentation_item = %d";

        return $this->query($sql, array(1, date("Y-m-d H:i:s"), $id_item));
    }

    /**
     * @param $idProduct
     * @param $idPresentation
     * @return mixed
     */
    public function deactivateAllItems($idProduct, $idPresentation)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation_item` SET `active` = %d, date_updated = '%s' WHERE id_product = %d AND id_presentation = %d";

        return $this->query($sql, array(0, date("Y-m-d H:i:s"), $idProduct, $idPresentation));
    }

    /**
     * @param $idProduct
     * @param $idPresentation
     * @return mixed
     */
    public function activateAllItems($idProduct, $idPresentation)
    {
        $sql = "UPDATE `".$this->db_prefix."orbitvu_sh_product_presentation_item` SET `active` = %d, date_updated = '%s' WHERE id_product = %d AND id_presentation = %d";

        return $this->query($sql, array(1, date("Y-m-d H:i:s"), $idProduct, $idPresentation));
    }

    /**
     * @param $id_product
     * @return mixed
     */
    public function getProductPresentationItemsActive($id_product)
    {
        $sql = "SELECT pi.*, p.folder_name, p.ov_name as ov_presentation_name, p.ov_sku, p.has_2d, p.has_360, p.has_orbittour, p.has_video, p.date_added, p.date_updated FROM `".$this->db_prefix."orbitvu_sh_product_presentation_item` pi
        INNER JOIN `".$this->db_prefix."orbitvu_sh_product_presentation` pp ON pi.id_presentation = pp.id_presentation AND pi.id_product = pp.id_product
        INNER JOIN `".$this->db_prefix."orbitvu_sh_presentation` p ON pp.id_presentation = p.id_presentation
        WHERE pi.id_product = %d AND pi.`active` = 1 AND pi.`ov_thumbnail_file` != '' ORDER BY pi.`order` ASC";

        $items = $this->fetchAll($sql, array($id_product));
        return $items;
    }

    /**
     * @param $id_product
     * @return mixed
     */
    public function getProductPresentationMainImage($id_product)
    {
        $sql = "SELECT pp.id_presentation, p.parsed_content, p.folder_name, p.ov_name, p.ov_sku FROM `".$this->db_prefix."orbitvu_sh_product_presentation` pp
         INNER JOIN `".$this->db_prefix."orbitvu_sh_presentation` p ON pp.id_presentation = p.id_presentation WHERE pp.id_product = %d ORDER BY `order` ASC";

        return $this->fetchRow($sql, array($id_product));
    }

    /**
     * Get all Prestashop product ids (without grouped products)
     *
     * @return mixed
     */
    public function getAllPrestashopProductIds()
    {
        $sql = "SELECT p.id_product as id_product  FROM `".$this->db_prefix."product` p";

        return $this->fetchAll($sql);
    }

    /**
     * Retrieves Prestashop product by SKU
     *
     * @param $sku
     * @return null|Product
     */
    public function getPrestaProductBySku($sku)
    {
        $sql = "SELECT id_product FROM `".$this->db_prefix."product` WHERE reference = '%s'";
        $product = $this->fetchRow($sql, array($sku));
        if ($product && $product['id_product']) {
            $product = new Product($product['id_product']);
            if (Validate::isLoadedObject($product)) {
                return $product;
            }
        }
        return null;
    }

    /**
     * Check wheter the presentation was unlinked earlier
     *
     * @param $id_product
     * @param $id_presentation
     * @return bool
     */
    public function wasUnlinked($id_product, $id_presentation)
    {
        $sql = "SELECT count(*) as no_unlinked FROM `".$this->db_prefix."orbitvu_sh_product_presentation_history` WHERE id_product = %d AND id_presentation = %d";
        $no = $this->fetchRow($sql, array($id_product, $id_presentation));

        if ($no['no_unlinked'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $item2d
     * @param bool|false $isPostThumbnail
     * @return bool
     */
    public function copyAsOriginal($item2d, $isPostThumbnail = false)
    {
        $product = new Product((int)$item2d['id_product']);
        if (Validate::isLoadedObject($product) && file_exists($item2d['original_image_file_path'])) {
            //$fileOrUrl = $this->fileUrl($item2d['original_image_file_path']);
            $fileOrUrl = $item2d['original_image_file_path'];
            $shops = Shop::getShops(true, null, true);
            $image = new Image();
            $image->id_product = $product->id;
            $image->position = Image::getHighestPosition($product->id) + 1;
            $image->cover = $isPostThumbnail;
            $image->legend = $product->name;
            if (($image->validateFields(false, true)) === true &&
                ($image->validateFieldsLang(false, true)) === true && $image->add()
            ) {
                $image->associateTo($shops);
                $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
                $watermarkTypes = explode(',', Configuration::get('WATERMARK_TYPES'));
                $path = $image->getPathForCreation();
                //$oPath = $image->getImgPath();
                if (!ImageManager::checkImageMemoryLimit($fileOrUrl)) {
                    $image->delete();
                    return false;
                }
                $orbitvuImage = Tools::file_get_contents($item2d['original_image_file_path']);
                if (file_put_contents($tmpfile, $orbitvuImage)) {
                    ImageManager::resize($tmpfile, $path . '.jpg');
                    $imagesTypes = ImageType::getImagesTypes('products');
                    foreach ($imagesTypes as $imageType) {
                        ImageManager::resize($tmpfile, $path . '-' . Tools::stripslashes($imageType['name']) . '.jpg', $imageType['width'], $imageType['height']);
                        if (in_array($imageType['id_image_type'], $watermarkTypes)) {
                            Hook::exec('actionWatermark', array('id_image' => $image->id, 'id_product' => $product->id));
                        }
                    }
                } else {
                    unlink($tmpfile);
                    $image->delete();
                    return false;
                }
                unlink($tmpfile);
                return true;
            }
        }
        return false;
    }

    private function fileUrl($url)
    {
        $parts = parse_url($url);
        $path_parts = array_map('rawurldecode', explode('/', $parts['path']));

        if (isset($parts['scheme']) && isset($parts['host'])) {
            return
                $parts['scheme'] . '://' .
                $parts['host'] .
                implode('/', array_map('rawurlencode', $path_parts))
                ;
        }
        return implode('/', array_map('rawurlencode', $path_parts));
    }
}
