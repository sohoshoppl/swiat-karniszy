<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class OrbitvuShConnect
{

    /**
     * @var
     */
    private $api_version; //SH plugin for Prestashop - this is needed for SH API

    /**
     * @var
     */
    private $plugin_version; //Prestashop's plugin version

    /**
     * @var
     */
    protected $remote_newest_version = null; //newset version available on SH server

    /**
     * @var string
     */
    protected $api_url = 'https://orbitvu.co/api/ecommerce/';

    /**
     * @var bool
     */
    protected $is_connected = null;


    /**
     * OrbitvuShConnect constructor.
     * @param $api_version
     * @param $plugin_version
     */
    public function __construct($api_version, $plugin_version)
    {
        $this->api_version = $api_version;
        $this->plugin_version = $plugin_version;

        $response = $this->callApi('plugins/versions/latest');
        if (isset($response['version']) && $response['version']) {
            $this->remote_newest_version = $response['version'];
            $this->is_connected = true;
        } else {
            $this->is_connected = false;
        }
    }


    /**
     * @param $request
     * @param array $params
     * @param string $method
     * @param bool|false $forcePost
     * @return array|mixed|object
     */
    public function callApi($request, $params = array(), $method = 'get', $forcePost = false)
    {
        $request = $request.".json"; //set the json mode always

        /*
         * Set the access token
         */
        $header = array();
        $header[] = "Authorization: Token 1567f2b4a02a8bfc5d8aacf0f44b16157e149d29"; //TODO remove this and ask Maciek to remove token from SH modules in API
        $header[] = "Accept-User-Agent: ".$this->api_version;
        $header[] = "Accept-User-Agent-Version: ".$this->plugin_version;

        if (is_array($params)) {
            $keys = array_keys($params);
            $vals = array_values($params);
            for ($i = 0, $n = count($keys); $i < $n; $i++) {
                if ($i == 0) {
                    $request .= '?';
                } else {
                    $request .= '&';
                }

                $request .= $keys[$i].'='.$vals[$i];
            }
        }

        $c = curl_init();
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_URL, $this->api_url.$request);
        curl_setopt($c, CURLOPT_HTTPHEADER, $header);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($c, CURLOPT_TIMEOUT, 20);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if ('post' == Tools::strtolower($method)) {
            $params['_method'] = !$forcePost ? 'PATCH' : 'POST';
            curl_setopt($c, CURLOPT_POST, count($params));
            curl_setopt($c, CURLOPT_POSTFIELDS, $params);
        }

        $response = curl_exec($c);

        return Tools::jsonDecode($response, true);
    }


    /**
     * @param $request
     * @param array $params
     * @param string $method
     * @param bool|false $forcePost
     * @return array|bool|mixed|object
     */
    public function callSh($request, $params = array(), $method = 'get', $forcePost = false)
    {
        if (!$this->isConnected()) {
            return false;
        }

        return $this->callApi($request, $params, $method, $forcePost);
    }


    /**"
     * Checks whether the plugin is connected to SH (DEMO or STANDARD account)
     *
     * @return bool
     */
    public function isConnected()
    {
        return $this->is_connected;
    }



    /**
     * Returns newest remote version published on SH
     *
     * @return string
     */
    public function getRemoteNewestVersion()
    {
        return $this->remote_newest_version;
    }


    /**
     * Returns current plugin version
     *
     * @return string
     */
    public function getPluginVersion()
    {
        return $this->plugin_version;
    }


    /**
     * Checks if the current plugin is the newest version
     *
     * @return bool
     */
    public function isNewestVersion()
    {
        return (version_compare($this->plugin_version, $this->remote_newest_version) === -1) ? false : true;
    }

    /**
     * @param $presentation
     * @return mixed
     */
    protected function parsePresentation($presentation)
    {
        $has_2d = false;
        $has_360 = false;

        foreach ($presentation['presentationcontent_set'] as $k => $item) {
            if ($item['type'] == 1) {
                $has_360 = true;
                $presentation['presentationcontent_set'][$k]['type'] = 'ov360';
            }
            if ($item['type'] == 3) {
                $has_2d = true;
                $presentation['presentationcontent_set'][$k]['type'] = 'ov2d';
            }
        }

        $presentation['ov_name'] = $presentation['name'];
        $presentation['ov_sku'] = $presentation['sku'];

        $presentation['has_2d'] = $has_2d;
        $presentation['has_360'] = $has_360;

        return $presentation;
    }
}
