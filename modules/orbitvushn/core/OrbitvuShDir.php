<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class OrbitvuShDir
{
    public function __construct()
    {
    }

    /**
     * Get all folder names of presentations
     *
     * @return array
     */
    public static function getLocalPresentationNames($presentationsPath = false)
    {
        $presentations = array();
        foreach (new DirectoryIterator($presentationsPath ? $presentationsPath : ORBITVU_PRESENTATIONS_PATH) as $currentFile) {
            if ($currentFile->isDir() && !$currentFile->isDot()) {
                $presentations[] = $currentFile->getBasename();
            }
        }
        return $presentations;
    }

    /**
     * Creates from xml files content which is pushed to database
     *
     * @param $folderName
     * @return array|bool
     * @throws Exception
     */
    public static function parseLocalPresentationContent($folderName)
    {
        $dir = ORBITVU_PRESENTATIONS_PATH.$folderName;
        //$content = $dir.DIRECTORY_SEPARATOR.'content.xml'; //we do not use content - it's old format
        $content2 = $dir.DIRECTORY_SEPARATOR.'content2.xml';
        $meta = $dir.DIRECTORY_SEPARATOR.'meta.xml';
        $config = $dir.DIRECTORY_SEPARATOR.'config.xml';
        if (file_exists($dir) && file_exists($content2)) {
            $dbContentField = array(
                //'content' => file_exists($content) ? static::xmlToArray(Tools::file_get_contents($content)) : '',
                'content' => static::xmlToArray(Tools::file_get_contents($content2)),
                'meta' => file_exists($meta) ? static::xmlToArray(Tools::file_get_contents($meta)) : '',
                'config' => file_exists($config) ? static::xmlToArray(Tools::file_get_contents($config)) : ''
            );

            $dbData = array(
                'folder_name' => $folderName,
                'content' => Tools::jsonEncode($dbContentField),
                'parsed_content' => array(
                    'images2d' => array(),
                    'sequence360' => array(),
                    'orbittour' => array(),
                    'video' => array(),
                    'viewer_config' => array()
                ),
                'cache_hash' => md5(Tools::jsonEncode($dbContentField)),
                'ov_sku' => '',
                'has_2d' => 0,
                'has_360' => 0,
                'has_orbittour' => 0,
                'has_video' => 0
            );

            //get presentation name
            if (is_array($dbContentField['content']) && isset($dbContentField['content']['properties'])
                && isset($dbContentField['content']['properties']['property']) && isset($dbContentField['content']['properties']['property'][0])
                && !is_array($dbContentField['content']['properties']['property'][0])) {
                $dbData['ov_name'] = $dbContentField['content']['properties']['property'][0];
            } else {
                $dbData['ov_name'] = $folderName;
            }

            //get presentation sku
            if (is_array($dbContentField['meta']) && isset($dbContentField['meta']['sku']) && !is_array($dbContentField['meta']['sku'])) {
                $dbData['ov_sku'] = $dbContentField['meta']['sku'];
            } elseif (is_array($dbContentField['content']) && isset($dbContentField['content']['properties'])
                && isset($dbContentField['content']['properties']['property']) && isset($dbContentField['content']['properties']['property'][4])
                && !is_array($dbContentField['content']['properties']['property'][4])) {
                $dbData['ov_sku'] = $dbContentField['content']['properties']['property'][4];
            } else {
                $dbData['ov_sku'] = '';
            }


            //check if has 2d photos (we check only content2.xml)
            if (is_array($dbContentField['content']) && isset($dbContentField['content']['images']) && isset($dbContentField['content']['images']['img'])) {
                $xmlImages = $dbContentField['content']['images']['img'];
                if (isset($xmlImages[0])) {
                    foreach ($xmlImages as $xmlImage) {
                        if (isset($xmlImage['@attributes'])) {
                            $xmlImage = $xmlImage['@attributes'];
                            if (isset($xmlImage['name']) && file_exists($dir.DIRECTORY_SEPARATOR.'images2d'.DIRECTORY_SEPARATOR.$xmlImage['name'])) {
                                $fileName = pathinfo($xmlImage['name'], PATHINFO_FILENAME); // filename without extension
                                $ext = pathinfo($xmlImage['name'], PATHINFO_EXTENSION); // jpg|gif|png
                                $ovInternalId = "ov2d_".$fileName."_".$ext;
                                $dbData['parsed_content']['images2d'][] = array(
                                    'ov_internal_id' => $ovInternalId,
                                    'ov_name' => $dbData['ov_name'],
                                    'ov_type' => 'ov2d',
                                    'ov_thumbnail_file' => $xmlImage['name'],
                                    'width' => isset($xmlImage['width']) ? $xmlImage['width'] : '',
                                    'height' => isset($xmlImage['height']) ? $xmlImage['height'] : ''
                                );
                            }
                        }
                    }
                } else {
                    if (isset($xmlImages['@attributes'])) {
                        $xmlImage = $xmlImages['@attributes'];
                        if (isset($xmlImage['name']) && file_exists($dir.DIRECTORY_SEPARATOR.'images2d'.DIRECTORY_SEPARATOR.$xmlImage['name'])) {
                            $fileName = pathinfo($xmlImage['name'], PATHINFO_FILENAME); // filename without extension
                            $ext = pathinfo($xmlImage['name'], PATHINFO_EXTENSION); // jpg|gif|png
                            $ovInternalId = "ov2d_".$fileName."_".$ext;
                            $dbData['parsed_content']['images2d'][] = array(
                                'ov_internal_id' => $ovInternalId,
                                'ov_name' => $dbData['ov_name'],
                                'ov_type' => 'ov2d',
                                'ov_thumbnail_file' => $xmlImage['name'],
                                'width' => isset($xmlImage['width']) ? $xmlImage['width'] : '',
                                'height' => isset($xmlImage['height']) ? $xmlImage['height'] : ''
                            );
                        }
                    }
                }
                if (count($dbData['parsed_content']['images2d']) > 0) {
                    $dbData['has_2d'] = 1;
                }
            }

            //check sequence 360
            if (is_array($dbContentField['content']) && isset($dbContentField['content']['sequence3d']) && isset($dbContentField['content']['sequence3d']['scales'])
                && isset($dbContentField['content']['sequence3d']['scales']['scale'])) {
                if (isset($dbContentField['content']['sequence3d']['scales']['scale'][0])) {
                    foreach ($dbContentField['content']['sequence3d']['scales']['scale'] as $xmlSequence) {
                        if (isset($xmlSequence['@attributes']) && isset($xmlSequence['@attributes']['cols']) && isset($xmlSequence['@attributes']['rows'])) {
                            if ($xmlSequence['@attributes']['cols'] == 1 && $xmlSequence['@attributes']['rows'] == 1) {
                                $scale = str_replace('.', '', (string)$xmlSequence['@attributes']['value']);
                                break;
                            }
                        }
                    }
                } else {
                    $xmlSequence = $dbContentField['content']['sequence3d']['scales']['scale'];
                    if (isset($xmlSequence['@attributes']) && isset($xmlSequence['@attributes']['cols']) && isset($xmlSequence['@attributes']['rows'])) {
                        if ($xmlSequence['@attributes']['cols'] == 1 && $xmlSequence['@attributes']['rows'] == 1) {
                            $scale = str_replace('.', '', (string)$xmlSequence['@attributes']['value']);
                        }
                    }
                }

                if (isset($scale) && isset($dbContentField['content']['sequence3d']['images'])
                    && isset($dbContentField['content']['sequence3d']['images']['@attributes'])
                    && isset($dbContentField['content']['sequence3d']['images']['@attributes']['ext'])
                    && isset($dbContentField['content']['sequence3d']['images']['@attributes']['name'])) {
                    $ext = $dbContentField['content']['sequence3d']['images']['@attributes']['ext'];
                    $prefix = $dbContentField['content']['sequence3d']['images']['@attributes']['name'];
                    $seqImage = $prefix.'0_0_'.$scale.'_0_0.'.$ext;
                    if (file_exists($dir.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$seqImage)) {
                        $fileName = pathinfo($seqImage, PATHINFO_FILENAME); // filename without extension
                        $ovInternalId = "ov360_".$fileName."_".$ext;
                        $dbData['parsed_content']['sequence360'] = array(
                            array(
                                'ov_internal_id' => $ovInternalId,
                                'ov_name' => $dbData['ov_name'],
                                'ov_type' => 'ov360',
                                'ov_thumbnail_file' => $seqImage,
                            )
                        );
                        $dbData['has_360'] = 1;
                    }
                }
            }
            if (is_array($dbContentField['config']) && isset($dbContentField['config']['viewer-params'])) {
                foreach ($dbContentField['config']['viewer-params'] as $cfgKey => $cfgValue) {
                    if ($cfgKey == 'style') {
                        $dbData['parsed_content']['viewer_config']['style'] = $cfgValue;
                    }

                    if ($cfgKey == 'teaser') {
                        $dbData['parsed_content']['viewer_config']['teaser'] = $cfgValue;
                    }
                    //if more options can be set from config.xml in module settings add them below
                }
            }

            //parse to json
            $dbData['parsed_content'] = Tools::jsonEncode($dbData['parsed_content']);
            return $dbData;
        }
        return false;
    }

    /**
     * Parses xml content into array
     * @param $xmlContent
     * @return mixed
     * @throws Exception
     */
    public static function xmlToArray($xmlContent)
    {
        if (!class_exists('SimpleXMLElement')) {
            throw new Exception('Orbitvu: PHP extension SimpleXMLElement doesn\'t exist. Upgrade your PHP version to >= 5.0.1');
        }
        $xml = new SimpleXMLElement($xmlContent);
        $xml_array = unserialize(serialize(Tools::jsonDecode(Tools::jsonEncode((array) $xml), 1)));

        return $xml_array;
    }

    /**
     * Check whether the presentation has been changed on local file system
     *
     * @param $dbHash
     * @param $folderName
     * @return bool
     * @throws Exception
     */
    public static function hashIsEqual($dbHash, $folderName)
    {
        $dir = ORBITVU_PRESENTATIONS_PATH.$folderName;
        $content2 = $dir.DIRECTORY_SEPARATOR.'content2.xml';
        $meta = $dir.DIRECTORY_SEPARATOR.'meta.xml';
        $config = $dir.DIRECTORY_SEPARATOR.'config.xml';

        if (file_exists($dir) && file_exists($content2)) {
            $dirContent = array(
                //'content' => file_exists($content) ? static::xmlToArray(Tools::file_get_contents($content)) : '',
                'content' => static::xmlToArray(Tools::file_get_contents($content2)),
                'meta' => file_exists($meta) ? static::xmlToArray(Tools::file_get_contents($meta)) : '',
                'config' => file_exists($config) ? static::xmlToArray(Tools::file_get_contents($config)) : ''
            );
            if (strcmp(md5(Tools::jsonEncode($dirContent)), $dbHash) === 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * Get presentation main image (first 360 image, if no 360 then first 2d image)
     *
     * @param $folderName
     * @param $parsedContent
     * @return bool
     */
    private static function getPresentationMainImage($folderName, $parsedContent)
    {
        $parsedContent = $parsedContent ? Tools::jsonDecode($parsedContent, true) : false;
        $dir = ORBITVU_PRESENTATIONS_PATH.$folderName;

        if ($parsedContent) {
            if (isset($parsedContent['sequence360']) && isset($parsedContent['sequence360']) && isset($parsedContent['sequence360'][0])
                && isset($parsedContent['sequence360'][0]['ov_thumbnail_file'])
                && file_exists($dir.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$parsedContent['sequence360'][0]['ov_thumbnail_file'])) {
                return $dir.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$parsedContent['sequence360'][0]['ov_thumbnail_file'];
            } elseif (isset($parsedContent['images2d']) && isset($parsedContent['images2d'][0])
                && isset($parsedContent['images2d'][0]['ov_thumbnail_file'])
                && file_exists($dir.DIRECTORY_SEPARATOR.'images2d'.DIRECTORY_SEPARATOR.$parsedContent['images2d'][0]['ov_thumbnail_file'])) {
                return $dir.DIRECTORY_SEPARATOR.'images2d'.DIRECTORY_SEPARATOR.$parsedContent['images2d'][0]['ov_thumbnail_file'];
            }
        }
        return false;
    }

    /**
     * @param $folderName
     * @param $parsedContent
     * @param string $size
     * @return bool
     */
    public static function getPresentationAdminImage($folderName, $parsedContent, $size = 'small')
    {
        $mainImagePath = static::getPresentationMainImage($folderName, $parsedContent);
        if (!$mainImagePath) {
            return false;
        }

        $ext = pathinfo($mainImagePath, PATHINFO_EXTENSION);
        $checkName = basename($mainImagePath, '.'.$ext);
        $width = 90;
        $height = 90;
        if ($size == 'big') {
            $width = 140;
            $height = 140;
        }

        //save old mask
        $oldmask = umask(0);

        //check if directory exists
        $mainResizedPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR.'_orbitvu_resized_images'.DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."main";

        if (!file_exists($mainResizedPath)) {
            mkdir($mainResizedPath, 0775, true);
        }

        //check if directory is writable
        if (!is_writable($mainResizedPath)) {
            return false;
        }

        $properFile = $mainResizedPath.DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height.".".$ext;
        if (!file_exists($properFile)) {
            if (!static::resizeImage($mainImagePath, $properFile, $width, $height, 'supplement')) {
                //restore old mask
                umask($oldmask);
                return false;
            }
        }
        //restore old mask
        umask($oldmask);

        $imgPath = ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR.rawurlencode($folderName).DIRECTORY_SEPARATOR;
        $imgPath .= '_orbitvu_resized_images'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR."main".DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height.".".$ext;

        //return proper file
        return DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$imgPath;
    }

    /**
     * Returns item admin image (both for 2d and 360 items)
     *
     * @param $folderName
     * @param $item
     * @return bool|string
     */
    public static function getItemAdminImage($folderName, $item)
    {
        $itemPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR.($item['ov_type'] == 'ov2d' ? 'images2d' : 'images').DIRECTORY_SEPARATOR.$item['ov_thumbnail_file'];
        if (!file_exists($itemPath)) {
            return false;
        }

        $ext = pathinfo($itemPath, PATHINFO_EXTENSION);
        $checkName = basename($itemPath, '.'.$ext);
        $width = 68;
        $height = 68;

        //save old mask
        $oldmask = umask(0);

        //check if directory exists
        $itemResizedPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR."_orbitvu_resized_images".DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."items";

        if (!file_exists($itemResizedPath)) {
            mkdir($itemResizedPath, 0775, true);
        }

        //check if directory is writable
        if (!is_writable($itemResizedPath)) {
            return false;
        }

        $properFile = $itemResizedPath.DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height.".".$ext;
        if (!file_exists($properFile)) {
            if (!static::resizeImage($itemPath, $properFile, $width, $height, 'supplement')) {
                //restore old mask
                umask($oldmask);
                return false;
            }
        }

        //restore old mask
        umask($oldmask);

        $imgPath = ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR.rawurlencode($folderName).DIRECTORY_SEPARATOR;
        $imgPath .= '_orbitvu_resized_images'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR."items".DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height.".".$ext;

        //return proper file
        return DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$imgPath;
    }

    /**
     * @param $folderName
     * @param $item
     * @param $width
     * @param $height
     * @param string $resizeType
     * @return bool|string
     */
    public static function getItemFrontImage($folderName, $item, $width, $height, $resizeType = 'supplement')
    {
        $itemPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR.($item['ov_type'] == 'ov2d' ? 'images2d' : 'images').DIRECTORY_SEPARATOR.$item['ov_thumbnail_file'];
        if (!file_exists($itemPath)) {
            return false;
        }

        $ext = pathinfo($itemPath, PATHINFO_EXTENSION);
        $checkName = basename($itemPath, '.'.$ext);

        //save old mask
        $oldmask = umask(0);

        //check if directory exists
        $itemResizedPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR.'_orbitvu_resized_images'.DIRECTORY_SEPARATOR."front".DIRECTORY_SEPARATOR."items";

        if (!file_exists($itemResizedPath)) {
            mkdir($itemResizedPath, 0775, true);
        }

        //check if directory is writable
        if (!is_writable($itemResizedPath)) {
            return false;
        }

        $properFile = $itemResizedPath.DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height."_".$resizeType.".".$ext;
        if (!file_exists($properFile)) {
            if (!static::resizeImage($itemPath, $properFile, $width, $height, $resizeType)) {
                //restore old mask
                umask($oldmask);
                return false;
            }
        }

        //restore old mask
        umask($oldmask);

        $imgPath = ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR.rawurlencode($folderName).DIRECTORY_SEPARATOR;
        $imgPath .= '_orbitvu_resized_images'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR."items".DIRECTORY_SEPARATOR.$checkName."_".$width."_".$height."_".$resizeType.".".$ext;

        //return proper file
        return ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules'.DIRECTORY_SEPARATOR.$imgPath;
    }

    /**
     * @param $folderName
     * @param $item
     * @param bool|false $filePath - full path of file (Presta specific)
     * @return bool|string
     */
    public static function getItemOriginalImage($folderName, $item, $filePath = false)
    {
        $itemPath = ORBITVU_PRESENTATIONS_PATH.$folderName.DIRECTORY_SEPARATOR.($item['ov_type'] == 'ov2d' ? 'images2d' : 'images').DIRECTORY_SEPARATOR.$item['ov_thumbnail_file'];
        if (!file_exists($itemPath)) {
            return false;
        }
        $imgPath = ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR.($filePath ? $folderName : rawurlencode($folderName)).DIRECTORY_SEPARATOR;
        $imgPath .= ($item['ov_type'] == 'ov2d' ? 'images2d' : 'images').DIRECTORY_SEPARATOR.$item['ov_thumbnail_file'];

        if ($filePath) { //Presta specific
            return _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.$imgPath;
        }
        //return proper url
        return ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules'.DIRECTORY_SEPARATOR.$imgPath;
    }

    /**
     * Resizes giver image
     *
     * @param $source
     * @param $destination
     * @param $width
     * @param $height
     * @param null $action (supplement, crop or regular resize to best fit in given square)
     * @return bool
     * @throws Exception
     */
    public static function resizeImage($source, $destination, $width, $height, $action = null)
    {
        include_once("OrbitvuShSimpleimage.php");

        $newImg = new OrbitvuShSimpleimage($source);
        if ($action == 'supplement') {
            //resize source image
            $newImg->bestFit($width, $height);
            //create empty image with white background and proper size
            $whiteImage = new OrbitvuShSimpleimage(null, $width, $height, array(255, 255, 255));
            $whiteImage->overlay($newImg)->save($destination);
        } elseif ($action == 'crop') {
            $newImg->thumbnail($width, $height)->save($destination);
        } else {
            try {
                $newImg->bestFit($width, $height)->save($destination);
            } catch (Exception $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Recursively removes dir
     *
     * @param $dir
     * @return bool
     */
    public static function rrmdir($dir)
    {
        if (!file_exists($dir)) {
            return true;
        }
        if (!is_dir($dir) || is_link($dir)) {
            return unlink($dir);
        }
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            if (!static::rrmdir($dir . "/" . $item, false)) {
                chmod($dir . "/" . $item, 0777);
                if (!static::rrmdir($dir . "/" . $item, false)) {
                    return false;
                }
            };
        }
        return rmdir($dir);
    }

    public static function uploadCodeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }

    public static function installPresentation($filename)
    {
        $ovusPath = ORBITVU_PRESENTATIONS_PATH.$filename;
        if (!file_exists($ovusPath) || is_dir($ovusPath)) {
            throw new \Exception("Error occurred! Presentation file does not exist!");
        }
        $folderName = pathinfo($ovusPath, PATHINFO_FILENAME); // filename without extension
        $extractFilePath = ORBITVU_PRESENTATIONS_PATH.$folderName;
        if (file_exists($extractFilePath)) {
            $folderName .= uniqid();
            $extractFilePath = ORBITVU_PRESENTATIONS_PATH.$folderName;
        }
        static::unzip($ovusPath, $extractFilePath);
        return $folderName;
    }

    /**
     * Unzip the file
     *
     * @param $file
     * @param null $path
     * @return bool
     * @throws Exception
     */
    public static function unzip($file, $path = null)
    {
        if (!class_exists('ZipArchive')) {
            throw new \Exception('PHP extension ZipArchive doesn\'t exist.');
        }
        $zip = new \ZipArchive();
        $oldmask = umask(0);
        if ($res = $zip->open($file)) {
            if ($res === true) {
                //perform extract
                $zip->extractTo($path);
                $zip->close();

                //delete tmp zip file
                unlink($file);

                if (file_exists($file)) {
                    throw new \Exception('Cannot extract the file. Wrong permissions!');
                }
                umask($oldmask);
                return true;
            } else {
                throw new \Exception('Cannot extract the file. Not valid file format or directory.');
            }
        } else {
            throw new \Exception('Cannot extract the file. Unknown error.');
        }
    }

    /***************************************
     ******* Prestashop's specific *********
     ***************************************/
    /**
     * Copy directory with its content to the specified destination
     *
     * @param $src
     * @param $dst
     */
    public static function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    static::recurseCopy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Converts bytes into human readable size
     *
     * @param $bytes
     * @return string
     */
    public static function convertSize($bytes)
    {
        $type = array("Bytes", "KB", "MB", "GB", "TB");
        $counter = 0;
        while ($bytes >= 1024) {
            $bytes /= 1024;
            $counter++;
        }

        return ("".round($bytes, 2)." ".$type[$counter]);
    }

    public static function folderSize($dir)
    {
        $size = 0;
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $file) {
            $size += $file->getSize();
        }
        return $size;
    }
    /***************************************
     ***** End Prestashop's specific *******
     ***************************************/
}
