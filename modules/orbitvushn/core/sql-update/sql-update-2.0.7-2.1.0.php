<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

/**
 * @static OrbitvuShDbdriver
 */

$medium = Image::getSize(ImageType::getFormatedName('large')); //Presta's large image is our medium
$large = Image::getSize(ImageType::getFormatedName('thickbox'));
$thumbnail = Image::getSize(ImageType::getFormatedName('cart'));

static::updateSettings('keep_original_enabled', '0');
static::updateSettings('big_width', ((int)$large['width']).'px');
static::updateSettings('big_height', ((int)$large['height']).'px');
static::updateSettings('medium_width', ((int)$medium['width']).'px');
static::updateSettings('medium_height', ((int)$medium['height']).'px');
static::updateSettings('thumbnail_width', ((int)$thumbnail['width']).'px'); //this option doesn't have to be inserted because it existed in previous versions, but we update the width size
static::updateSettings('thumbnail_height', ((int)$thumbnail['height']).'px'); //this option doesn't have to be inserted because it existed in previous versions, but we update the height size


static::updateSettings('zoom_magnifier_maxzoom_enabled', '0');
static::updateSettings('zoom_magnifier_maxzoom_width', '1800px');
static::updateSettings('zoom_magnifier_maxzoom_height', '1500px');

static::updateSettings('lightbox_colorized_bg', '1');
static::updateSettings('lightbox_thumbnails_enabled', '1');
static::updateSettings('lightbox_buttons_shadow_enabled', '1');
static::updateSettings('lightbox_maxzoom_enabled', '0');
static::updateSettings('lightbox_maxzoom_width', '1800px');
static::updateSettings('lightbox_maxzoom_height', '1500px');
