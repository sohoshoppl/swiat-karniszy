<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

class AdminProductsController extends AdminProductsControllerCore
{
    public function __construct()
    {
        parent::__construct();

        /***********************************************************************
         * Copy this section into the end of constructor method of
         * /prestapath/override/controllers/admin/AdminProductsController.php
         * if you manually merge both files. When copying is done, please
         * remove constructor method from this file to pervent install error
         ***********************************************************************/
        $tmpSelect = trim($this->_select);
        if (Tools::substr($tmpSelect, -1) == ',') {
            $this->_select .= ' a.id_product as orbitvushn ';
        } else {
            $this->_select .= ' , a.id_product as orbitvushn ';
        }

        $this->fields_list['orbitvushn'] = array(
            'title' => $this->l('Orbitvu SH'),
            'align' => 'center',
            'callback' => 'orbitvushnColumnContent',
            'orderby' => false,
            'filter' => false,
            'search' => false,
        );
        /**************
         * End of copy
         **************/
    }

    /**
     * Display additional column in products grid (Add this method to the override)
     *
     * @param $idProduct
     * @return string
     */
    public function orbitvushnColumnContent($idProduct)
    {
        if (!defined('ORBITVU_PLUGIN_NAME')) {
            return;
        }

        include_once(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'OrbitvuShDbdriver.php');
        include_once(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'OrbitvuShDir.php');

        $db = new OrbitvuShDbdriver(DB::getInstance());
        $productPresentationImage = $db->getProductPresentationMainImage($idProduct);
        if ($productPresentationImage) {
            $productPresentationImage['admin_image_small'] = OrbitvuShDir::getPresentationAdminImage($productPresentationImage['folder_name'], $productPresentationImage['parsed_content']);
            if ($productPresentationImage['admin_image_small']) {
                $img = '<img src="'.$productPresentationImage['admin_image_small'].'" alt="'.$productPresentationImage['ov_name'].' ('.$productPresentationImage['ov_sku'].')" title="'.$productPresentationImage['ov_name'].' ('.$productPresentationImage['ov_sku'].')">';
            }
        }
        if (!isset($img)) {
            $img =  "-";
        }
        return $img;
    }
}
