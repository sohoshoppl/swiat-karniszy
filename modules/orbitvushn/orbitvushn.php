<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * Orbitvu PHP  Orbitvu eCommerce administration
 *
 *  @author    Orbitvu Sp. z o.o <dev@orbitvu.com>
 *  @copyright Orbitvu Sp. z o.o. is the owner of full rights to this code
 *  @license   license.pdf
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

define("ORBITVU_API_VERSION", "1.1.2"); //SH plugin for Prestashop

/***************************************************
 * DO NOT FORGET TO CHANGE IT IN A CONSTRUCTOR!!!! *
 ***************************************************/
define("ORBITVU_PLUGIN_VERSION", "2.1.2"); //Current plugin version

/***************************************************
 * DO NOT FORGET TO CHANGE IT IN A CONSTRUCTOR!!!! *
 ***************************************************/
define("ORBITVU_PLUGIN_NAME", "orbitvushn"); //DO NOT FORGET TO CHANGE IT IN A CONSTRUCTOR!!!!

define("ORBITVU_PRESENTATIONS_PATH", _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_orbitvu_presentations'.DIRECTORY_SEPARATOR);

class OrbitvuShn extends Module
{
    /**
     * @var OrbitvuShConnect
     */
    public $orbitvu_connect;

    protected $settings_html = '';

    public function __construct()
    {
        $this->name = 'orbitvushn'; //the same as ORBITVU_PLUGIN_NAME (can't assign constant because of Prestashop validator gooosh!!)
        $this->tab = 'others';
        $this->version = '2.1.2'; // the same as ORBITVU_PLUGIN_VERSION (can't assign constant because of Prestashop validator gooosh!!)
        $this->author = 'ORBITVU';
        $this->module_key = '6ca1af9999bbf8512eddca2e796b330b';
        $this->ps_versions_compliancy = array('min' => '1.4.999', 'max' => '1.6.999');
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('ORBITVU SELF HOSTED');
        $this->description = $this->l('Dynamic zoom, 360°/3D spins, product tours using ORBITVU imaging solution. Improve your customer\'s experience.');
        $this->confirmUninstall = $this->l('Are you sure about uninstalling these module?');

        include_once(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'OrbitvuShDbdriver.php');
        include_once(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'OrbitvuShConnect.php');
        include_once(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'OrbitvuShDir.php');
    }

    /**
     * Installs plugin
     *
     * @return bool
     */
    public function install()
    {
        //Prestashop's specific
        //install sample presentation and Orbitvu Viewer
        if (!class_exists('ZipArchive')) {
            return $this->context->controller->errors[] = $this->l('Class ZipArchive does not exist. Please install "php-zip" extension.');
        } else {
            $tmpInstallDir = _PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'_tmp_install'.DIRECTORY_SEPARATOR;
            $samplePresentationPathZip = $tmpInstallDir."samsung_gear_fit_black.zip";
            $viewerPathZip = $tmpInstallDir."orbitvu12.zip";

            $samplePresentationPathExtract = ORBITVU_PRESENTATIONS_PATH."samsung_gear_fit_black";
            $viewerPathExtract = _PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR."_orbitvu_viewer";

            //save old mask
            $oldmask = umask(0);

            //install sample presentation
            if (!file_exists($samplePresentationPathExtract)) {
                mkdir(ORBITVU_PRESENTATIONS_PATH."samsung_gear_fit_black", 0775, true);
            }
            if (is_writable($samplePresentationPathExtract)) {
                $zip = new ZipArchive();
                if ($zip->open($samplePresentationPathZip) === true) {
                    $zip->extractTo($samplePresentationPathExtract);
                    $zip->close();
                }
            }

            //install viewer
            if (!file_exists($viewerPathExtract)) {
                mkdir($viewerPathExtract, 0775, true);
            }
            if (is_writable($viewerPathExtract)) {
                $zip = new ZipArchive();
                if ($zip->open($viewerPathZip) === true) {
                    $zip->extractTo($viewerPathExtract);
                    $zip->close();
                }
            }

            //restore old mask
            umask($oldmask);
        }

        $db = new OrbitvuShDbdriver(DB::getInstance());

        if (!parent::install() or !$this->registerHook('displayAdminProductsExtra') or !$this->registerHook('actionAdminControllerSetMedia') or !$this->registerHook('displayFooterProduct') or !$db->sqlInstall()) {
            return false;
        }

        //---------------------------------------------------------------------------------------------------
        // Register admin controllers
        //---------------------------------------------------------------------------------------------------
        $tab = new Tab();
        $tab->active = 1;
        $tab->name = array();
        $tab->class_name = 'Admin'.Tools::ucfirst(ORBITVU_PLUGIN_NAME);

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = Tools::ucfirst(ORBITVU_PLUGIN_NAME).' controller';
        }
        $tab->id_parent = -1;
        $tab->module = ORBITVU_PLUGIN_NAME;
        $tab->add();

        return true;
    }


    public function uninstall()
    {
        $db = new OrbitvuShDbdriver(DB::getInstance());
        return (parent::uninstall() && $db->sqlUninstall() && $this->unregisterHook('displayAdminProductsExtra') && $this->unregisterHook('actionAdminControllerSetMedia') && $this->unregisterHook('displayFooterProduct'));
    }

    /**
     * Perform update action
     *
     * @param bool|false $forceAll
     */
    public function enable($forceAll = false)
    {
        parent::enable($forceAll);
        $db = new OrbitvuShDbdriver(DB::getInstance());
        $settings = OrbitvuShDbdriver::getDbSettings();
        if ($settings) {//this is necessary to prevent installation error
            $db->sqlUpdate($settings['plugin_version'], ORBITVU_PLUGIN_VERSION);
        }
    }

    /**
     * Creates connection with SH api
     */
    public function makeApiConnection()
    {
        if (null === $this->orbitvu_connect) {
            $this->orbitvu_connect = new OrbitvuShConnect(
                ORBITVU_API_VERSION,
                $this->version
            );
        }
    }

    /**
     * Hook responsible for displaying "ORBITVU SH" in product's edit page
     *
     * @return string
     */
    public function hookDisplayAdminProductsExtra()
    {
        $content = '';
        $content .= $this->getLinkedPresentationTemplate();
        $content .= $this->getNotLinkedPresentationTemplate();
        $content .= $this->getPagerTemplate();
        $content .= $this->getTabTemplate();
        return $content;
    }

    /**
     * Hook responsible for adding necessary scripts and css in product's edit page
     */
    public function hookActionAdminControllerSetMedia()
    {
        if ($this->context->controller->controller_name == 'AdminProducts' && Tools::getValue('id_product')) {
            $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-namespace.js');
            $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-ractive.min.js');
            $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-upload.min.js');
            $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-ajax.js');
            $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-app.js');
            $this->context->controller->addCSS($this->_path.'views'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin.css');
        }
    }


    public function hookDisplayFooterProduct($params)
    {
        include_once(_PS_MODULE_DIR_.ORBITVU_PLUGIN_NAME.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'product_gallery.php');

        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'orbitvu-sh-public-namespace.js');
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'orbitvu-sh-public-hammer.js');
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'orbitvu-sh-public-hammer-jquery.js');
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'orbitvu-sh-public-gallery.js');
        $this->context->controller->addCSS($this->_path.'views'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'orbitvu-sh-public.css');


        $this->smarty->assign(array(
            'orbitvu_ps_version' => _PS_VERSION_,
            'template' => new OrbitvuTemplate($params['product'], $this->context),
            'viewer_url' => ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? _PS_BASE_URL_SSL_ : _PS_BASE_URL_).__PS_BASE_URI__.'modules/'.ORBITVU_PLUGIN_NAME.'/_orbitvu_viewer/orbitvu12/'
        ));

        return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR.'product_gallery.tpl');
    }

    /**
     * Get "ORBITVU SH" tab template in product's edit page
     *
     * @return mixed
     */
    public function getTabTemplate()
    {
        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product'), false, (int)Configuration::get('PS_LANG_DEFAULT')))) {
            $db = new OrbitvuShDbdriver(DB::getInstance());
            $this->makeApiConnection();

            //number of linked presentations
            $noPresentations = $db->countProductPresentations($product->id);

            //plugin settings and default settings
            $ovSettings = $this->getSettingsValues();
            $defaultSettings = OrbitvuShDbdriver::getDefaultSettings();

            //linked presentations
            $linkedPresentations = $noPresentations ? $db->getProductPresentations($product->id) : array();

            //sh presentations
            $shResults = (!$noPresentations || $ovSettings['multi_link']) ? $db->getPresentationsList() : array();

            foreach ($linkedPresentations as $k => $p) {
                $linkedPresentations[$k] = $db->parsePresentation($linkedPresentations[$k]);
                $linkedPresentations[$k]['items'] = $db->parseItems($db->getItems($product->id, $p['id_presentation']));
            }

            $this->smarty->assign(array(
                'product' => $product,
                'orbitvu_connect' => $this->orbitvu_connect,
                'no_presentations' => $noPresentations, //number of linked presentations
                'ov_settings' => $ovSettings,
                'default_settings' => $defaultSettings,
                'linked_presentations' => Tools::jsonEncode($linkedPresentations),
                'sh_results' => Tools::jsonEncode($shResults),
                'orbitvu_ajax_url' => $this->context->link->getAdminLink('AdminOrbitvushn'),
                //'new_version_message' => sprintf(html_entity_decode($this->l('Version <span class="orbitvu-new-version">%s</span> of Orbitvu extension is out! Upgrade now from <span class="orbitvu-new-version">%s</span> to <span class="orbitvu-new-version">%s</span>. <span class="orbitvu-new-version"><a href="https://orbitvu.co/market/products" target="_blank">Download</a></span> the newest version from SUN cloud.')), $this->orbitvu_connect->getRemoteNewestVersion(), $this->orbitvu_connect->getPluginVersion(), $this->orbitvu_connect->getRemoteNewestVersion()),
                'upload_error_messages' => Tools::jsonEncode(array(
                    'InvalidFileExtensionError' =>  html_entity_decode($this->l('This file format is not allowed. Only ".zip" and ".ovus" files are allowed.')),
                    'InvalidFileTypeError' => html_entity_decode($this->l('This file format is not allowed. Only ".zip" and ".ovus" files are allowed.')),
                    'MaxFileSizeError' => html_entity_decode($this->l('This file is too big.')),
                    'HashError' => html_entity_decode($this->l('Unknown upload error.')),
                    'RequestError' => html_entity_decode($this->l('Could not get response from server.')),
                    'UnsupportedError' => html_entity_decode($this->l('Your browser does not support this upload method.')),
                    'InternalError' => html_entity_decode($this->l('There was an error uploading the file.'))
                ))
            ));
            return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'tab.tpl');
        }
    }

    public function getLinkedPresentationTemplate()
    {
        return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'jstpl'.DIRECTORY_SEPARATOR.'linked-presentation.tpl');
    }

    public function getNotLinkedPresentationTemplate()
    {
        return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'jstpl'.DIRECTORY_SEPARATOR.'notlinked-presentation.tpl');
    }

    public function getPagerTemplate()
    {
        return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'jstpl'.DIRECTORY_SEPARATOR.'pager.tpl');
    }

    /**
     * Display module config page
     *
     * @return string
     */
    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->validateSettingsForm();
        }

        $this->settings_html .= $this->renderMigrationInfo();
        $this->settings_html .= $this->renderSettingsForm();
        return $this->settings_html .= $this->initSettingsScripts();
    }

    /**
     * Init module settings js and css scripts
     *
     * @return mixed
     */
    public function initSettingsScripts()
    {
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-namespace.js');
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-ajax.js');
        $this->context->controller->addJS($this->_path.'views'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin-settings.js');
        $this->context->controller->addCSS($this->_path.'views'.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'orbitvu-sh-admin.css');

        $this->smarty->assign(array(
            'orbitvu_ajax_url' => $this->context->link->getAdminLink('AdminOrbitvushn'),
        ));
        return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'settings-scripts.tpl');
    }

    /**
     * Renders migration info
     */
    public function renderMigrationInfo()
    {
        $oldModule = $this->shouldMigrate();
        if ($oldModule) {
            $isWritable = is_writable(ORBITVU_PRESENTATIONS_PATH);
            $freeDiskSpaceBytes = disk_free_space(_PS_MODULE_DIR_);
            $oldPresentationsSizeBytes = OrbitvuShDir::folderSize(_PS_MODULE_DIR_.DIRECTORY_SEPARATOR.'orbitvush'.DIRECTORY_SEPARATOR.'_orbitvu_presentations') + (1024 * 1024 * 150); //+150MB of safety space

            $this->smarty->assign(array(
                'oldModule' => $oldModule,
                'freeDiskSpaceBytes' => $freeDiskSpaceBytes,
                'oldPresentationsSizeBytes' => $oldPresentationsSizeBytes,
                'freeDiskSpaceMB' => OrbitvuShDir::convertSize($freeDiskSpaceBytes),
                'oldPresentationsSizeMB' => OrbitvuShDir::convertSize($oldPresentationsSizeBytes),
                'isWritable' => $isWritable ? 1 : 0
            ));
            return $this->display(__FILE__, 'views'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'migrate.tpl');
        }
        return '';
    }

    /**
     * Renders module settings form
     *
     * @return mixed
     */
    public function renderSettingsForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Remote upload url'),
                        'name' => 'remote_upload_url',
                        'desc' => '<a class="orbitvu-generate-key" id="ovgenerate-remote-upload-url" data-type="remote_upload_key">Generate new url</a><br>'.$this->l('Enter this URL to your Alphashot Editor. You will be able to upload *.ovus files from your directly!')
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'remote_upload_key'
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Automatic linking'),
                        'desc' => $this->l('Automatically link Orbitvu presentation to the product by SKU while uploading it from Alphashot Editor or via back office.'),
                        'name' => 'automatic_link',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_automatic_link' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_automatic_link' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_automatic_link',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Multi linking'),
                        'desc' => $this->l('Allow linking multiple presentations to product'),
                        'name' => 'multi_link',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_multi_link' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_multi_link' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_multi_link',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Images 2D copy settings'),
                        'desc' => $this->l('This option allows to copy Orbitvu 2D images as original Prestashop images while linking the presentation.'),
                        'name' => 'images2d_copy_type',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_images2d_copy_type' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Don\'t do anything')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_images2d_copy_type' => '1',
                                    'name' => $this->l('Copy first Orbitvu 2D image (if product has no original images)')
                                ),
                                array(
                                    'id_images2d_copy_type' => '2',
                                    'name' => $this->l('Always copy all Orbitvu 2D images as original Prestashop images')
                                ),
                            ),
                            'id' => 'id_images2d_copy_type',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Show Orbitvu 360° presentation'),
                        'desc' => $this->l('360° Orbitvu presentations are visible in front gallery'),
                        'name' => 'show_360',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_show_360' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_show_360' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_show_360',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[General]</i> '.$this->l('Show Orbitvu 2d images'),
                        'desc' => $this->l('Orbitvu 2D images are visible in front gallery'),
                        'name' => 'show_2d',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_show_2d' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_show_2d' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_show_2d',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('HTML5 or Flash mode'),
                        'name' => 'viewer_mode',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_viewer_mode' => 'html5',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('HTML5 mode')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_viewer_mode' => 'flash',
                                    'name' => $this->l('Flash mode')
                                ),
                            ),
                            'id' => 'id_viewer_mode',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('GUI buttons style'),
                        'name' => 'gui_style',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_gui_style' => 'round',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Default round style')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_gui_style' => 'square',
                                    'name' => $this->l('Square style')
                                ),
                                array(
                                    'id_gui_style' => 'round_rotation',
                                    'name' => $this->l('Round style with rotation scroll')
                                ),
                                array(
                                    'id_gui_style' => 'square_rotation',
                                    'name' => $this->l('Square style with rotation scroll')
                                ),
                                array(
                                    'id_gui_style' => 'no_buttons',
                                    'name' => $this->l('No buttons at all')
                                ),
                                array(
                                    'id_gui_style' => 'config_xml',
                                    'name' => $this->l('Use Orbitvu setting from presentation (config.xml)')
                                ),
                            ),
                            'id' => 'id_gui_style',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('360° Presentation teaser'),
                        'name' => 'teaser',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_teaser' => 'autorotate',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Auto-rotation')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_teaser' => 'play',
                                    'name' => $this->l('Play button enables rotation')
                                ),
                                array(
                                    'id_teaser' => 'static',
                                    'name' => $this->l('No teaser (static presentation)')
                                ),
                                array(
                                    'id_teaser' => 'zoom',
                                    'name' => $this->l('Zoom in')
                                ),
                                array(
                                    'id_teaser' => 'config_xml',
                                    'name' => $this->l('Use Orbitvu setting from presentation (config.xml)')
                                )
                            ),
                            'id' => 'id_teaser',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('Viewer preload width'),
                        'name' => 'preload_width',
                        'desc' => $this->l('Force initial image preload width. By default initially loaded images fit container size and while zooming in larger images are loaded. With this option you can load bigger images immediately (default 0)')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('Viewer preload height'),
                        'name' => 'preload_height',
                        'desc' => $this->l('Force initial image preload height (default 0)')
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('Viewer mousewheel'),
                        'desc' => $this->l('Enable or disable presentation zoom on mousewheel'),
                        'name' => 'mousewheel',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_mousewheel' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_mousewheel' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_mousewheel',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Viewer]</i> '.$this->l('Auto rotation speed'),
                        'name' => 'frame_rate',
                        'desc' => $this->l('Auto rotation speed [frames/sec]')
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbs position'),
                        'desc' => $this->l('Append or prepend ORBITVU PHOTOS in front image gallery (to the begin or end of gallery thumbnail list)'),
                        'name' => 'thumbs_position',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_thumbs_position' => 'append',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Append')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_thumbs_position' => 'prepend',
                                    'name' => $this->l('Prepend')
                                ),
                            ),
                            'id' => 'id_thumbs_position',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Enable thumbnails scroll'),
                        'desc' => $this->l('If enabled, thumbnails are scrollable (added prev/next button). If disabled, thumbnails are displayed one after another without scroll'),
                        'name' => 'scroll',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_scroll' => 'no',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_scroll' => 'yes',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_scroll',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Gallery width'),
                        'name' => 'gallery_width'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Main image proportions'),
                        'name' => 'main_proportions',
                        'desc' => $this->l('Main image proportions (default: 4/3)')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Main image border color'),
                        'name' => 'main_border_color'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Main image padding'),
                        'name' => 'main_padding'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbnails margin'),
                        'name' => 'thumbnail_margin'
                    ),
                    /*array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbnails padding'),
                        'name' => 'thumbnail_padding'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbnails border color'),
                        'name' => 'thumbnail_border_color',
                        'desc' => $this->l('Enter \'transparent\' for no border')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbnails active border color'),
                        'name' => 'thumbnail_active_border_color'
                    ),*/
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Thumbnails hover delay (ms)'),
                        'name' => 'thumbnail_hover_delay',
                        'desc' => $this->l('Delay in milliseconds after hovering thumbnail in order to change main image')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Button prev/next width'),
                        'name' => 'button_width'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Button prev/next height'),
                        'name' => 'button_height'
                    ),
                    /*array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Button prev/next opacity'),
                        'name' => 'button_opacity'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Gallery]</i> '.$this->l('Button prev/next border color'),
                        'name' => 'button_border_color'
                    ),*/
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Keep original image sizes in zoom/lightbox.'),
                        'desc' => $this->l('If enabled, \'Zoom/lightbox image width\' and \'Zoom/lightbox image height\' are ignored.'),
                        'name' => 'keep_original_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_keep_original_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_keep_original_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_keep_original_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Zoom/lightbox image width'),
                        'name' => 'big_width',
                        'desc' => $this->l('Note! This option affects only Orbitvu 2D images. Native Prestashop product images dimensions are configured from \'Preferences -> Images\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Zoom/lightbox image height'),
                        'name' => 'big_height',
                        'desc' => $this->l('Note! This option affects only Orbitvu 2D images. Native Prestashop product images dimensions are configured from \'Preferences -> Images\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Medium image width'),
                        'name' => 'medium_width',
                        'desc' => $this->l('Note! This option affects only Orbitvu 2D images. Native Prestashop product images dimensions are configured from \'Preferences -> Images\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Medium image height'),
                        'name' => 'medium_height',
                        'desc' => $this->l('Note! This option affects only Orbitvu 2D images. Native Prestashop product images dimensions are configured from \'Preferences -> Images\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Thumbnails width'),
                        'name' => 'thumbnail_width'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Image dimensions]</i> '.$this->l('Thumbnails height'),
                        'name' => 'thumbnail_height'
                    ),


                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Zoom magnifier enabled'),
                        'desc' => $this->l('Show zoomed image after hovering main image.'),
                        'name' => 'zoom_magnifier_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_zoom_magnifier_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_zoom_magnifier_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_zoom_magnifier_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Display lenses'),
                        'desc' => $this->l('After hovering main image, lenses are shown in square. If disabled, no lenses are shown.'),
                        'name' => 'zoom_magnifier_lenses',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_zoom_magnifier_lenses' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_zoom_magnifier_lenses' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_zoom_magnifier_lenses',
                            'name' => 'name'
                        )
                    ),
                    /*array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Lenses size'),
                        'name' => 'zoom_magnifier_lenses_size',
                        'desc' => $this->l('Set the lenses size in percentage. For example \'25%\' size creates lenses \'25%\' width and \'25%\' height of main image dimensons. Max: \'80%\'')
                    ),*/
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Lenses border color'),
                        'name' => 'zoom_magnifier_lenses_border_color',
                        'desc' => $this->l('Type \'transparent\' if no border')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Lenses box shadow'),
                        'name' => 'zoom_magnifier_lenses_box_shadow',
                        'desc' => $this->l('Type \'none\' if no box shadow')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Mian image opacity'),
                        'name' => 'zoom_magnifier_image_opacity_hover',
                        'desc' => $this->l('Enter the opacity for main image when hovered. Type \'1\', if no opacity.')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Zoomed image border color'),
                        'name' => 'zoom_magnifier_zoom_border_color',
                        'desc' => $this->l('Type \'transparent\' if no border')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Zoomed image box shadow'),
                        'name' => 'zoom_magnifier_zoom_box_shadow',
                        'desc' => $this->l('Type \'transparent\' if no border')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Zoomed image background'),
                        'name' => 'zoom_magnifier_zoom_background'
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Enable max zoom'),
                        'desc' => $this->l('If max zoom is enabled then zoomed image in zoom magnifier won\'t be bigger than \'Max zoom width\' and \'Max zoom height\''),
                        'name' => 'zoom_magnifier_maxzoom_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_zoom_magnifier_maxzoom_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_zoom_magnifier_maxzoom_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_zoom_magnifier_maxzoom_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Max zoom width'),
                        'name' => 'zoom_magnifier_maxzoom_width',
                        'desc' => $this->l('For example: \'1800px\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Zoom magnifier]</i> '.$this->l('Max zoom height'),
                        'name' => 'zoom_magnifier_maxzoom_height',
                        'desc' => $this->l('For example: \'1500px\'')
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Lightbox enabled'),
                        'desc' => $this->l('Display photos in lightbox.'),
                        'name' => 'lightbox_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_lightbox_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Lightbox image proportions'),
                        'desc' => $this->l('Lightbox image proportions (default: 4/3)'),
                        'name' => 'lightbox_proportions'
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Lightbox colorized background'),
                        'desc' => $this->l('Lightbox background is colorized based on image colors'),
                        'name' => 'lightbox_colorized_bg',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_colorized_bg' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_colorized_bg' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_lightbox_colorized_bg',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Lightbox thumbnails'),
                        'desc' => $this->l('Display thumbnails in lightbox mode'),
                        'name' => 'lightbox_thumbnails_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_thumbnails_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_thumbnails_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_lightbox_thumbnails_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Lightbox buttons shadow'),
                        'desc' => $this->l('Shadow under button after hover'),
                        'name' => 'lightbox_buttons_shadow_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_buttons_shadow_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_buttons_shadow_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_lightbox_buttons_shadow_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Enable max zoom'),
                        'desc' => $this->l('If max zoom is enabled then zoomed image in lightbox won\'t be bigger than \'Max zoom width\' and \'Max zoom height\''),
                        'name' => 'lightbox_maxzoom_enabled',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_maxzoom_enabled' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('Disabled')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_maxzoom_enabled' => '1',
                                    'name' => $this->l('Enabled')
                                ),
                            ),
                            'id' => 'id_lightbox_maxzoom_enabled',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Max zoom width'),
                        'name' => 'lightbox_maxzoom_width',
                        'desc' => $this->l('For example: \'1800px\'')
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('Max zoom height'),
                        'name' => 'lightbox_maxzoom_height',
                        'desc' => $this->l('For example: \'1500px\'')
                    ),
                    /*array(
                        'type' => 'select',
                        'label' => '<i style="opacity: 0.5">[Lightbox]</i> '.$this->l('When should lightbox be opened'),
                        'desc' => $this->l('Choose the action which opens the lightbox.'),
                        'name' => 'lightbox_open_click',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_lightbox_open_click' => '0',       // The value of the 'value' attribute of the <option> tag.
                                    'name' => $this->l('On thumbnails click')    // The value of the text content of the  <option> tag.
                                ),
                                array(
                                    'id_lightbox_open_click' => '1',
                                    'name' => $this->l('On main image click')
                                ),
                                array(
                                    'id_lightbox_open_click' => '2',
                                    'name' => $this->l('On thumbnails and main image click')
                                ),
                            ),
                            'id' => 'id_lightbox_open_click',
                            'name' => 'name'
                        )
                    ),*/
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Cron jobs]</i> '.$this->l('Auto synchronizing url'),
                        'name' => 'auto_synch_url',
                        'desc' => '<a class="orbitvu-generate-key" id="ovgenerate-auto-synch-url" data-type="auto_synch_key">Generate new url</a><br>'.$this->l('Execute this url in order to synchronize presentations with their real state (keep presentations up to date with server files).').' <a target="_blank" href="" id="ovexecute_auto_synch_url"><span class="orbitvu-button orbitvu-button-5-margin-bottom"><span class="orbitvu-button-icon orbitvu-button-icon-sync"></span>'.$this->l('Click to execute manually').'</span></a>'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'auto_synch_key'
                    ),
                    array(
                        'type' => 'text',
                        'label' => '<i style="opacity: 0.5">[Cron jobs]</i> '.$this->l('Auto linking url'),
                        'name' => 'auto_link_url',
                        'desc' => '<a class="orbitvu-generate-key" id="ovgenerate-auto-link-url" data-type="auto_link_key">Generate new url</a><br>'.$this->l('Execute this url in order to automatically link Orbitvu SH presentations with products (by SKU - presentation\'s SKU == product\'s reference field). Presentations which have been unlinked earlier, won\'t be considered anymore until you clear the \'unlink history\'. This action affects only products with no presentations (multi linking option is ignored. It means only one presentation will be linked per product).').' <a target="_blank" href="" id="ovexecute_auto_link_url"><span class="orbitvu-button orbitvu-button-5-margin-bottom"><span class="orbitvu-button-icon orbitvu-button-icon-link"></span>'.$this->l('Click to execute manually').'</span></a> <a style="cursor: pointer" class="orbitvu-button orbitvu-button-red orbitvu-button-5-margin-bottom" id="ovclear-unlink-history"><span class="orbitvu-button-icon orbitvu-button-icon-unlink"></span>'.$this->l("Clear unlink history").'</a>'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'auto_link_key'
                    ),
                )
            )
        );
        if (_PS_VERSION_ >= 1.6) {
            $fields_form['form']['submit'] = array(
                'title' => $this->l('Save'),
            );
        } else {
            $fields_form['form']['submit'] = array(
                'title' => $this->l('Save'),
                'class' => $this->isPrestahop15only ? 'button' : ''
            );
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getSettingsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getSettingsValues()
    {
        $settings = OrbitvuShDbdriver::getDbSettings();
        $ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? true : false;

        $settings['remote_upload_url'] = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $settings['remote_upload_key'], 'ov_action' => 'auth'), $ssl);

        $settings['auto_synch_url'] = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $settings['auto_synch_key'], 'ov_action' => 'auto_synch'), $ssl);

        $settings['auto_link_url'] = $this->context->link->getModuleLink(ORBITVU_PLUGIN_NAME, 'remote', array('ov_auth' => $settings['auto_link_key'], 'ov_action' => 'auto_link'), $ssl);

        return $settings;
    }

    private function validateSettingsForm()
    {
        $currentSettings = OrbitvuShDbdriver::getDbSettings();
        $isError = false;

        foreach (OrbitvuShDbdriver::getDefaultSettings() as $option => $default_value) {
            if (in_array($option, array('auto_synch_lock', 'auto_link_lock', 'plugin_version'))) {
                OrbitvuShDbdriver::updateSettings($option, $currentSettings[$option]);
            } else {
                $formValue = pSQL(Tools::getValue($option, $default_value)); //protect against sql injection
                /**
                 * Validate fields which may spoil frontend gallery view
                 */
                switch ($option) {
                    case "preload_width":
                        if (!Validate::isUnsignedInt($formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Viewer preload width" field. Plese enter the proper number (0...n).'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "preload_height":
                        if (!Validate::isUnsignedInt($formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Viewer preload height" field. Plese enter the proper number (0...n).'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "frame_rate":
                        if (!Validate::isUnsignedInt($formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Auto rotation speed" field. Plese enter the proper number (0...n).'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "gallery_width":
                        if (!preg_match("/^\d+%$/", $formValue) && !preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Gallery width" field. Percentage or pixels allowed (sample values: 100%, 400px)'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "main_proportions":
                        if (!preg_match("/^\d+\/\d+$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Main image proportions" field. Plese enter the proper value: n/n.'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "thumbnail_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Thumbnail width" field. Type value in pixels like: 90px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "thumbnail_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Thumbnails height" field. Type value in pixels like: 90px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "medium_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Medium image width" field. Type value in pixels like: 800px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "medium_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Medium image height" field. Type value in pixels like: 600px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "big_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Zoom/lightbox image width" field. Type value in pixels like: 1280px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "big_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Zoom/lightbox image height" field. Type value in pixels like: 960px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "thumbnail_hover_delay":
                        if (!Validate::isUnsignedInt($formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Thumbnails hover delay (ms)" field. Plese enter the proper number (0...n).'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "button_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Button width" field. Type value in pixels like: 90px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else if ((int)$formValue < 24) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Buttons width. Minimum 24px!'));
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "button_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Button height" field. Type value in pixels like: 90px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else if ((int)$formValue < 30) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Button height. Minimum 30px!'));
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "zoom_magnifier_maxzoom_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Zoom magnifier max zoom width" field. Type value in pixels like: 1500px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "zoom_magnifier_maxzoom_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Zoom magnifier max zoom height" field. Type value in pixels like: 1800px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "lightbox_proportions":
                        if (!preg_match("/^\d+\/\d+$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Lightbox image proportions" field. Plese enter the proper value: n/n.'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "lightbox_maxzoom_width":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Lightbox max zoom width" field. Type value in pixels like: 1500px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    case "lightbox_maxzoom_height":
                        if (!preg_match("/^\d+px$/", $formValue)) {
                            $this->settings_html .= $this->displayError($this->l('Wrong value for "Lightbox max zoom height" field. Type value in pixels like: 1800px'));
                            OrbitvuShDbdriver::updateSettings($option, $default_value);
                            $isError = true;
                        } else {
                            OrbitvuShDbdriver::updateSettings($option, $formValue);
                        }
                        break;
                    default:
                        OrbitvuShDbdriver::updateSettings($option, $formValue);
                        break;
                }
            }
        }
        if (!$isError) {
            $this->settings_html .= $this->displayConfirmation($this->l('Settings updated'));
        }
    }

    /**
     * Detetecs if migration is available and if so, return all old module credentials with presentations folders
     *
     * @return bool|mixed
     */
    private function shouldMigrate()
    {
        $db = new OrbitvuShDbdriver(DB::getInstance());
        $oldModule = $db->getOldModuleCredentials();
        $oldModulePresentationsPath = _PS_MODULE_DIR_.DIRECTORY_SEPARATOR.'orbitvush'.DIRECTORY_SEPARATOR.'_orbitvu_presentations';
        $oldPresentationsFolderNames = array();
        if (file_exists($oldModulePresentationsPath)) {
            $oldPresentationsFolderNames = OrbitvuShDir::getLocalPresentationNames($oldModulePresentationsPath);
        }
        if ($oldModule && count($oldPresentationsFolderNames)) {
            return $oldModule;
        }
        return false;
    }
}
