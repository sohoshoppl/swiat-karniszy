<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{paragonfaktura}prestashop>cart_fb713d48bf28d7a0698f996014ee9b84'] = 'Typ dokumentu';
$_MODULE['<{paragonfaktura}prestashop>cart_466eadd40b3c10580e3ab4e8061161ce'] = 'Faktura';
$_MODULE['<{paragonfaktura}prestashop>cart_d92c25d41fcd9f8ab35545ef34b1e7ed'] = 'Paragon';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_0fa8243b1c655aeca443e389d73998b1'] = 'Faktura lub paragon';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_c4502a63a3103270682389b4689756bc'] = 'Ten moduł umożliwia podjęcie przez klienta wyboru dokumentu do składanego zamówienia.';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_466eadd40b3c10580e3ab4e8061161ce'] = 'Faktura';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_d92c25d41fcd9f8ab35545ef34b1e7ed'] = 'Paragon';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_c888438d14855d7d96a2724ee9c306bd'] = 'Ustawienia zapisane';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_c21f969b5f03d33d43e04f8f136e7682'] = 'domyślny';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_fb713d48bf28d7a0698f996014ee9b84'] = 'Typ dokumentu';
$_MODULE['<{paragonfaktura}prestashop>paragonfaktura_9bdbe93e3614c857376bed2e70ff67aa'] = 'Wybrany dokument';
