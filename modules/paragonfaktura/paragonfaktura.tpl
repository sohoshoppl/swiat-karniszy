<div id="formAddPaymentPanel" class="panel">
    <div class="panel-heading">
            <i class="icon-money"></i>
            {l s='Type of document' mod='paragonfaktura'}
    </div>
    <fieldset>
            <p>
                    {l s='Selected document' mod='paragonfaktura'}:
                    <span style="font-weight: bold;">{$type}</span>
            </p>
    </fieldset>
</div>