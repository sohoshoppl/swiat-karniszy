<style>
#pfform div.radio,
#pfform div.radio span,
#pfform div.radio input{
	vertical-align: middle;
}
#pfform div.radio:last-of-type{
	margin-left: 30px;
}
</style>
<form action="#" method="POST" name="pf" style="margin-bottom: 20px;" id="pfform">
	{*<h3 class="page-heading bottom-indent">{l s='Type of document' mod='paragonfaktura'}</h3>*}
        <div class="paroagonFakturaRadios">
            	<input type="radio" value="1" name="pfi" {if $type == 1} checked {/if}/><label>{l s='Invoice' mod='paragonfaktura'}</label>
                <input type="radio" value="2" name="pfi" {if $type == 2} checked {/if} style="margin-left: 30px;"/><label style="margin-left: 5px;">{l s='Bill' mod='paragonfaktura'}</label>
        </div>
	<input type="hidden" value="{$id_cart}" name="pf_id" id="pf_id"/>
</form>