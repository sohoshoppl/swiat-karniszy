/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$(document).ready(function(){

	if (typeof(homesliderkolekcje_speed) == 'undefined')
		homesliderkolekcje_speed = 500;
	if (typeof(homesliderkolekcje_pause) == 'undefined')
		homesliderkolekcje_pause = 3000;
	if (typeof(homesliderkolekcje_loop) == 'undefined')
		homesliderkolekcje_loop = true;
   
	var block_width = $('#homepage-slider-kolekcje').width() + 30.0;
   
   if(block_width >= 720){
		var homesliderkolekcje_width = block_width/2;
		homesliderkolekcje_width =homesliderkolekcje_width -30.0;       	
   }else{
		var homesliderkolekcje_width =block_width -30.0;       	   	
   }
	if (!!$.prototype.bxSlider)
		$('#homesliderkolekcje').bxSlider({
            slideMargin:30,
			useCSS: false,
            minSlides: 1,
			maxSlides: homesliderkolekcje_width,
			slideWidth: homesliderkolekcje_width ,
			infiniteLoop: homesliderkolekcje_loop,
			hidecontrolonend: true,
			pager: false,
			autoHover: true,
			auto: homesliderkolekcje_loop,
			speed: parseInt(homesliderkolekcje_speed),
			pause: homesliderkolekcje_pause,
			controls: true
		});

    $('#homepage-slider-kolekcje .homeslider-description').click(function () {
        window.location.href = $(this).prev('a').prop('href');
    });


});