<?php
//define('DEBUG', false);
//define('PS_SHOP_PATH', 'http://swiatkarniszy.dkonto.pl');
//define('PS_WS_AUTH_KEY', '3GVTXWHTTEJ3PDRGUWELP8WHKZIRWP98');
//require_once('PSWebServiceLibrary.php');
//$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
//$back = 'http://swiatkarniszy.dkonto.pl';

define('ALLOWED_STATUSES', array(18, 19, 20, 22, 30, 31, 32, 33, 34, 35)); //statusy zamówienia, które pozwalaja na modyfikacje   

function getOrder($id_order, $reference) {
    $id_order = (int) $id_order;
    $products = array();
    $order['obj'] = new Order($id_order);
    if ($order['obj']->reference == $reference /* AND status zamówienia to status allegro do edycji */) {
        $order['products'] = $order['obj']->getProducts();
        foreach ($order['products'] as &$product) {
            $product['attributes'] = getProductAttributes($product['product_id']);
            $product['colectionCode'] = getProductCollectionCode($product['product_id']);
        }
        return $order;
    } else {
        return false;
    }
}

function getProductCollectionCode($id_product) {
    foreach (Product::getFeaturesStatic((int) $id_product) as $product_feature) {
        if ($product_feature['id_feature'] == 22) {
            $feature_value = new FeatureValue((int) $product_feature['id_feature_value']);
        }
    }
    return $feature_value->value[1];
}

function getProductAttributes($id_product) {

    $product_attributes_by_group = array();

    $product_attributes = Db::getInstance()->executeS('
			SELECT DISTINCT a.`id_attribute`,a.code as code, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`
			FROM `' . _DB_PREFIX_ . 'attribute` a
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) Context::getContext()->language->id . ')
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl
				ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = ' . (int) Context::getContext()->language->id . ')
			LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac
				ON (a.`id_attribute` = pac.`id_attribute`)
			LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa
				ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
			' . Shop::addSqlAssociation('product_attribute', 'pa') . '
			' . Shop::addSqlAssociation('attribute', 'pac') . '
			WHERE pa.`id_product` = ' . (int) $id_product . ' Order by a.`id_attribute_group` ');


    foreach ($product_attributes as $product_attribute) {
        $product_attributes_by_group[$product_attribute['id_attribute_group']]['name'] = $product_attribute['group'];
        $attr_color = '';
        $bracket_attribute_name = explode("(", $product_attribute['attribute']);
        $attr_color = Tools::link_rewrite($bracket_attribute_name[1]);
        $attributes = array('id_attribute' => $product_attribute['id_attribute'], 'attr_name' => $bracket_attribute_name[0], 'attr_full_name' => $product_attribute['attribute'], 'color' => $attr_color, 'code' => $product_attribute['code']);
        $product_attributes_by_group[$product_attribute['id_attribute_group']]['attributes'][] = $attributes;
    }

    return $product_attributes_by_group;
}

function getProductWidthCollectionCode($product_id, $product_attribute_id) {
    // trzeba pobrać szerokośc na potrzeby kodu kolekcji
    $attributes = Product::getAttributesParams((int) $product_id, (int) $product_attribute_id);
    foreach ($attributes as $value) {
        if ($value['id_attribute_group'] == 5) {
            $attr_val = new Attribute($value['id_attribute']);
            return $attr_val->code;
        }
    }
    return false;
}

function getProductWidthIdAttr($product_id, $product_attribute_id) {
    // trzeba pobrać szerokośc na potrzeby kodu kolekcji
    $attributes = Product::getAttributesParams((int) $product_id, (int) $product_attribute_id);
    foreach ($attributes as $value) {
        if ($value['id_attribute_group'] == 5) {
            return $value['id_attribute'];
        }
    }
    return false;
}

function updateOrderDetail() {
    $id_order_detail = (int) Tools::getValue('id_order_detail', 0);
    if ($id_order_detail > 0) {
        $orderDetail = new OrderDetail($id_order_detail);
        $order = new Order($orderDetail->id_order);
        if (in_array($order->current_state, ALLOWED_STATUSES)) {
            $org_product_name = $orderDetail->product_name;
            $org_product_reference = $orderDetail->product_reference;
            $attribute_groups = Tools::getValue('attribute_group');

            //składanie nazwy i pobranie fragmentów kodów kolekcji
            //najpierw trzeba obciąć z nazwy końcówkę szerokością
            //$id_attrWidth = getProductWidthIdAttr($orderDetail->product_id, $orderDetail->product_attribute_id);
            //$attr = new Attribute($id_attrWidth);
            //$instructions_id .= ',' . $id_attrWidth;
            //$attr_val = new Attribute($id_attrWidth);
            //$attr_group = new AttributeGroup($attr_val->id_attribute_group);
            $org_product_name_exp = explode('długość', strip_tags($org_product_name));

            //później zaczynamy składanie nowej nazwy
            $prefix_pr_name .= ($attr_group->name[1] != '') ? '<div>' . $attr_group->name[1] . '</div>' : '';
            $prefix_pr_name .= ($attr_val->name[1] != '') ? '<div>' . $attr_val->name[1] . '</div>' : '';
            $new_product_name = $org_product_name_exp[0] . $prefix_pr_name;
            $prefix_pr_name = '';

            foreach ($attribute_groups as $key => $value) {
                $instructions_id .= "," . $value;
                $attr_group = new AttributeGroup($key);
                $prefix_pr_name .= ($attr_group->name[1] != '') ? '<div>' . $attr_group->name[1] . '</div>' : '';
                $attr_val = new Attribute($value);
                if($attr_group->id == 8){
                    $id_attribute = $value;
                }
                $prefix_pr_name .= ($attr_val->name[1] != '') ? '<div>' . $attr_val->name[1] . '</div>' : '';
                $codes[$key] = $attr_val->code; // do składania kodu kolekcji
            }
            $new_product_name = $new_product_name . $prefix_pr_name;
            //echo $new_product_name;
            $orderDetail->product_name = $new_product_name; //podmieniamy nazwe produktu w zamówieniu
            //składanie kodu koleckji
            $pr_col_code = getProductCollectionCode($orderDetail->product_id);
            $codeWidth = getProductWidthCollectionCode($orderDetail->product_id, $orderDetail->product_attribute_id);
            $tmp_code = $codes[13];
            $tmp_code = str_replace("[kolekcja]", $pr_col_code, $tmp_code);
            $kod_kolekcji = $tmp_code . $codeWidth . $codes[9] . $codes[6] . $codes[8];
            $orderDetail->product_reference = $kod_kolekcji; //podmieniamy kod produktu w zamówieniu
            
            $sql = "DELETE FROM " . _DB_PREFIX_ . "cart_product WHERE id_cart = " . (int) $order->id_cart . " AND id_product_attribute = " . (int) $orderDetail->product_attribute_id . " AND id_product = " . (int) $orderDetail->product_id . " AND id_shop = " . (int) $order->id_shop;
            Db::getInstance()->execute($sql);            
            
            $sql = 'SELECT pa.`id_product_attribute` FROM `'._DB_PREFIX_.'product_attribute` as pa LEFT JOIN  `'._DB_PREFIX_.'product_attribute_combination` as pac ON (pa.`id_product_attribute` = pac.`id_product_attribute`)   WHERE pa.`id_product` = ' . (int) $orderDetail->product_id . ' AND pac.`id_attribute` = '.(int)$id_attribute;
            $product_attribute_id = Db::getInstance()->getValue($sql);
            if($product_attribute_id != 0){
                $orderDetail->product_attribute_id = $product_attribute_id;    
            }
            $logMessage = 'Modyfikacja zamówienia złożonego przez allegro. Org nazwa produktu:' . $org_product_name . '  Org kod product:' . $org_product_reference;
            PrestaShopLogger::addLog($logMessage, 1, null, 'OrderDetail', $orderDetail->id); //dodajemy log o zmodyfikowaniu zamówienia
            if ($orderDetail->update()) {
                // dodanie/modyfikacji produktu koszyka
                $sql = "INSERT INTO " . _DB_PREFIX_ . "cart_product( id_cart, id_product, id_address_delivery, id_shop, id_product_attribute, instructions_id, code, date_add, quantity ) VALUES ( " . (int) $order->id_cart . ", " . (int) $orderDetail->product_id . ", " . (int) $order->id_address_delivery . ", " . (int) $order->id_shop . ", " . (int) $orderDetail->product_attribute_id . ",  '" . $instructions_id . "',  '" . $orderDetail->product_reference . "' ,'" . date("Y-m-d H:i:s") . "', " . (int) $orderDetail->product_quantity . " )";
                Db::getInstance()->execute($sql);
                //wpisanie id zamówień do tablicy, która przechowuje edytowane zamówienia z formularza, później jest to wykorzystywane przy wysyłaniu przypomnień
                $sql = 'REPLACE INTO `allegro_orders_done`(`id_order`) VALUES (' . (int) $order->id . ');';
                Db::getInstance()->execute($sql);
                return 1;
            }
        }
    }
    return 0;
}

function separateProduct() {
    $error = 1;
    $id_order_detail = (int) Tools::getValue('id_order_detail', 0);
    if ($id_order_detail > 0) {
            $error =0;
            $orderDetail = new OrderDetail($id_order_detail);
            $qnt = $orderDetail->product_quantity;
            $orderDetail->product_quantity = 1;
            if(!$orderDetail->update()){
                $error = 1;    
            }
            for ($i = 0; $i < $qnt-1; $i++) {
                $orderDetail->id =0;
                if(!$orderDetail->add()){
                    $error = 1;
                }    
            }
    }   
    return $error;
}

if (Tools::isSubmit('submit') && ($_SESSION['access'] == 1)) {
    $res = updateOrderDetail();
    $result = array('status' => $res);
    die(Tools::jsonEncode($result));
}
if (Tools::isSubmit('separate') && ($_SESSION['access'] == 1)) {
    $res = separateProduct();
    die(Tools::jsonEncode($res));
}
?>