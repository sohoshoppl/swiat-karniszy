<?php

define("DAYS_TO_REMAIND", 10);
define('ALLEGRO_STATUSES', '18,19,20,22' ); //statusy zamówienia, które pozwalaja na modyfikacje  
include(dirname(__FILE__) . '/../config/config.inc.php');
include(dirname(__FILE__) . '/../init.php');

if (Tools::getIsset('secure_key'))
{
	$secure_key = 'r7h3S2n7bSkv4QTv';
	if (!empty($secure_key) && $secure_key === Tools::getValue('secure_key'))
	{
            allegroRemind();
	}
}

        /*
 * wysłanie przypomnienia o konfiguracji karnisza zamówionego na allegro
 * 
 */

function allegroRemind($count = false) {
    /* pobieram id_zamówień z ostatnich 90 dni, które dostały przypomnienie lub zostały edytowane z formularza integracji allegro
     * te zamówienia nie powinny otrzymać przypomnienia
     */
    $sql1 = 'SELECT * FROM  `allegro_orders_done` WHERE DATE_SUB(CURDATE(),INTERVAL 90 DAY) <= date_add' ;
    $sql2 = 'SELECT * FROM  `allegro_remind_send` WHERE DATE_SUB(CURDATE(),INTERVAL 90 DAY) <= date_add';
    $r_1 = Db::getInstance()->executeS($sql1);
    $r_2 = Db::getInstance()->executeS($sql2);
    $excluded_orders = '';
    
    /*składam string dla zapytania sql*/
    foreach ($r_1 as $value){
        $excluded_orders .= $value['id_order'].',';
    }
    
    foreach ($r_2 as $value){
        $excluded_orders .= $value['id_order'].',';
    }  

    $excluded_orders = rtrim($excluded_orders,",");
    
    /* pobieram zamówienia i dane do przypomnień*/
    $sql = '
        SELECT o.id_order,o.reference,cu.email,cu.firstname,cu.lastname,o.date_add FROM ' . _DB_PREFIX_ . 'orders o '
            . 'LEFT JOIN ' . _DB_PREFIX_ . 'customer as cu ON (o.id_customer = cu.id_customer)'   
            . 'WHERE '
            . 'DATE_SUB(CURDATE(),INTERVAL ' .DAYS_TO_REMAIND. ' DAY) >= o.date_add '
            . 'AND id_order NOT IN ('.$excluded_orders.')'
            . 'AND current_state IN ('.ALLEGRO_STATUSES.')';

    $remiands = Db::getInstance()->executeS($sql);

    /*procedura wysyłania maili:*/
    foreach ($remiands as $remiand) {
        echo $remiand['email'].'<br>';
        $template_vars = array(
            '{email}' => $remiand['email'],
            '{lastname}' => $remiand['lastname'],
            '{firstname}' => $remiand['firstname'],
            '{id_order}' => $remiand['id_order'],
            '{order_name}' => $remiand['reference']
        );
        Mail::Send(1, 'allegro_remind', Mail::l('Świat karniszy - przypomnienie allegro', 1), $template_vars, $remiand['email'], $remiand['firstname'] . ' ' . $remiand['lastname'], null, null, null, null, _PS_MAIL_DIR_, false);
        $sql = 'REPLACE INTO `allegro_remind_send`(`id_order`) VALUES ('.(int)$remiand['id_order'].');';
        Db::getInstance()->execute($sql);
    }
    echo 'Wysłane';
}
