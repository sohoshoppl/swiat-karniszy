<?php
session_start();
include(dirname(__FILE__) . '/../config/config.inc.php');
include(dirname(__FILE__) . '/../init.php');
include('integracje_allegro.php');



/* widoki */ ?>
<html><head>
        <title>Świat karniszy</title>
        <link rel="icon" type="image/x-icon" href="/img/favicon.ico">
        <meta name="robots" content="noindex,nofollow" />
        <link rel="stylesheet" href="/integracja_allegro/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="/themes/swiat-karniszy/css/global.css" type="text/css" media="all">   
        <link rel="stylesheet" href="/themes/swiat-karniszy/css/modules/blocktopmenu/css/superfish-modified.css" type="text/css" media="all">        
        <link rel="stylesheet" href="/modules/blocksecondmenu/css/superfish-modified-second.css" type="text/css" media="all">
        <link rel="stylesheet" href="/js/jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" media="all">        
        <script type="text/javascript" src="/../js/jquery/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/js/jquery/plugins/fancybox/jquery.fancybox.js"></script>        
    </head>
    <body>

        <?php /* widok zamówienia i wybór produktów do edycji
         * powinno być w linku id_zamówienia(id) i referencja(ref)
         * sprawdzamy czy to się zgadza sprawdzamy status zamówienia jak ok to pobieramy produkty z zamówienia
         * i robimy linki do edycji poszczególnych produktów
         */ ?> 

        <?php
        if (isset($_GET['id']) && isset($_GET['ref'])) {
            $order = getOrder($_GET['id'], $_GET['ref']);
            if (in_array($order['obj']->current_state, ALLOWED_STATUSES)) {
                $_SESSION["access"] = 1;
                ?> 

                <div class="header-container">
                    <header id="header">
                        <div>
                            <div class="container">
                                <div class="row">
                                    <div id="header_logo">
                                        <a href="http://decorative.pl/" title="Świat Karniszy">
                                            <img class="logo img-responsive" src="http://decorative.pl/img/swiat-karniszy-logo-1498563538.jpg" alt="Świat Karniszy" width="264" height="53">
                                        </a>
                                    </div>
                                    <div id="block_second_menu">
                                        <div class="row">

                                            <div id="block_top_menu_contact" >
                                                masz pytania?
                                                <span>
                                                    <i class="icon-phone">
                                                    </i>
                                                    <a href="tel:+48 610 642 145   ">+48 610 642 145   </a>
                                                </span>
                                                <span>
                                                    <i class="icon-envelope">
                                                    </i>
                                                    <a href="mailto:biuro@swiatkarniszy.pl">biuro@swiatkarniszy.pl</a>
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="block_top_menu">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <section class="product-listing">
                    <h1>Zamówienie allegro</h1>
                    <p class="green">Prosimy o uzupełnienie produktów o dodatkowe dane.</p>
        <?php foreach ($order['products'] as $product) { ?>
                        <div class="product">
                            <form class="product-form">
                                <input type="hidden" name="id_order_detail" value="<?php echo $product['id_order_detail']; ?>">
                                <div class="row allegro-flex-center">
                                    <div class="col-xs-1"><h2>PRODUKT:</h2></div>
                                    <div class="col-xs-1"><h2><?php echo $product['product_quantity']; ?>szt.</h2></div>
                                    <div class="col-xs-8">
                                        <h3 class="product_name"><?php echo $product['product_name'] /* Tools::truncateString($product['product_name'],70) */; ?></h3>
                                    </div>
                                    <div class="col-xs-2">
            <?php if ($product['product_quantity'] > 1) { ?>
                                        <button id="separate _<?php echo $product['id_order_detail']; ?>" data-id_order_detail="<?php echo $product['id_order_detail']; ?>" type="button" name="separate" class="submit btn btn-primary pull-right" >Rozdziel</button>
                                        <?php } ?>
                                    </div>
                                </div>
            <?php if (sizeof($product['attributes']) > 0) { ?>
                                    <div class="row allegro-flex-center">
                                        <div class="col-xs-1"><h2>MOŻLIWE OPCJE:</h2></div>
                                        <div class="col-xs-9 text-center">
                <?php foreach ($product['attributes'] as $key => $attribute_group) { ?>
                                                <?php if ( ($key != 12)) { // aukcje z allegro powinny mieć już wybraną długość, 12 to awp, 13 to wspornik ?>
                                                    <div class="<?php if (/*$key == 13 ||*/ $key == 5) {
                                                        echo "unvisible";
                                                    } else {
                                                        echo "cell";
                                                    } ?>">
                                                        <div class="form-group">
                                                            <label for="select_group_<?php echo $key; ?>_<?php echo $product['product_id']; ?>_<?php echo $product['id_order_detail']; ?>"><?php echo $attribute_group['name']; ?></label>
                                                            <div class="select-box">
                                                                <select class="select_group_<?php echo $key; ?>" id="select_group_<?php echo $key; ?>_<?php echo $product['product_id']; ?>_<?php echo $product['id_order_detail']; ?>" data-product_id="<?php echo $product['product_id']; ?>" data-id_order_detail="<?php echo $product['id_order_detail']; ?>" data-attribute_group="<?php echo $key; ?>" name="attribute_group[<?php echo $key; ?>]" >
                                                                    <?php if (($key != 5)&&($key != 6) && ($key != 13)) { ?> <option value="0">wybierz</option>> <?php } //grupa 5 to długości, 13 to wspornik, 6 to kolor ?> 
                                                                    <?php foreach ($attribute_group['attributes'] as $attribute) { ?>
                                                                        <option value="<?php echo $attribute['id_attribute']; ?>" data-color_group="<?php echo $attribute['color']; ?>"><?php echo $attribute['attr_name']; ?></option>
                        <?php } ?>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                    </div>
                    <?php } ?>
                <?php } ?>
                                        </div>
                                        <div class="col-xs-2">
                                            <button id="submit_<?php echo $product['id_order_detail']; ?>" type="submit" name="submit" class=" pull-right submit btn btn-primary" >Zapisz</button>
                                        </div>
                                    </div>
            <?php } ?>
                            </form>
                            <div class="unvisible">
                                <div id ="pop-up_<?php echo $product['product_id']; ?>_<?php echo $product['id_order_detail']; ?>" >
            <?php foreach ($product['attributes'] as $key => $attribute_group) { ?>
                                    
                                        <div class="pop-up-content" id="img-group_<?php echo $key; ?>">
                                            <h3>Wybierz <?php echo $attribute_group['name']; ?> swojego karnisza.   </h3>
                                            <p><?php if (($key == 9) ) { ?> <b style="color:red;">Do zawieszek "cichych" jest dopłata +2zł.</b><br> Płatności proszę dokonać przelewem na nasze konto: 0000000000000.<br>  <?php } ?> 
                                                <?php if (($key == 13) ) { ?><b style="color:red;">Do wsporników sufitowych jest dopłata +2zł.</b><br> Płatności proszę dokonać przelewem na nasze konto: 0000000000000.<br>  <?php } ?> 
                                                Skontaktuj się z nami, jeżeli nie znalazłeś interesującej cię opcji. Wykonujemy także personalizowane karnisze!</p>
                <?php foreach ($attribute_group['attributes'] as $attribute) { ?>
                    <?php if (($key != 5) && ($key != 12)) { //grupa 5 to długości, aukcje z allegro powinny mieć już wybraną długość  ?>
                                                    <div data-product_id="<?php echo $product['product_id']; ?>" data-id_order_detail="<?php echo $product['id_order_detail']; ?>" data-id_attribute="<?php echo $attribute['id_attribute']; ?>" data-id_groupe="<?php echo $key; ?>" class="img-group-item <?php echo $attribute['color']; ?>">
                                                        <img src="/img/co/<?php echo $attribute['id_attribute']; ?>.jpg" alt="<?php echo $attribute['attr_name']; ?>" />
                                                        <span><?php echo $attribute['attr_name']; ?></span>                                       
                                                    </div>  
                                            <?php } ?> 
                                        <?php } ?>                                
                                        </div>
            <?php } ?>
                                </div>
                            </div>
                        </div>
                <?php } ?>        
                </section>
            <?php
            } else {
                echo 'Nie można modyfikować tego zamówienia !!!';
                unset($_SESSION['access']);
                session_destroy();
            }
        } else {
            echo 'Zły link !!!';
            unset($_SESSION['access']);
            session_destroy();
        }
        ?> 


        <script>
            $(document).ready(function () {

                $('.product-form').each(function (e) {
                    var product_name = $(this).find('.product_name').text();
                    product_name = product_name.toLowerCase(product_name);
                    var product_name_splitted = product_name.split("długość");
                    var str = product_name_splitted[1].replace(/^\D+/g, '');
                    var dl = parseInt(str);
                    $(this).find('.select_group_5 option:contains('+dl+')').attr('selected','selected');
					if($(this).find('.select_group_13 option').length < 2){
						$(this).find('.select_group_13').parent().parent().hide();	
					}
                });
                $('.img-group-item').click(function (e) {
                    var group = parseInt($(this).data('id_groupe'));
                    var attr = $(this).data('id_attribute');
                    var product_id = $(this).data('product_id');
                    var id_order_detail = $(this).data('id_order_detail');
                    if (group == 6) {
                        $('select').not('.select_group_' + group).not('.select_group_13').not('.select_group_5').each(function(){
                            if( $(this).data('id_order_detail') == id_order_detail){
                                $(this).val(0);
                            }
                        });
                    }
                    $('#select_group_' + group + '_' + product_id + '_' + id_order_detail).val(attr);
                    $.fancybox.close();
                });

                $('.product-form select').mousedown(function (e) {
                    e.preventDefault();
                    $('.pop-up-content').hide();
                    var group = $(this).data('attribute_group');
                    var product_id = $(this).data('product_id');
                    var id_order_detail = $(this).data('id_order_detail');

                    $.fancybox({
                        'type': 'inline',
                        'href': '#pop-up_' + product_id + '_' + id_order_detail,
                        beforeShow: function () {
                            $('.product-form select').hide();
                            $('#pop-up_' + product_id + '_' + id_order_detail + ' #img-group_' + group).show();
                            colors();
                        },
                        beforeClose: function () {
                            $('.product-form select').show();
                        }
                    })
                })

                $('button[name=separate]').click(function (e) {
                    if (confirm('Produkt zostanie rozdzielony na kilka produktów po 1 sztuce. Tej operacji nie będzie można cofnąć. Czy kontynuować rozdzielanie?')) {
                        var id_order_detail= $(this).data('id_order_detail');
                        $.ajax({
                            type: 'POST',
                            headers: {"cache-control": "no-cache"},
                            url: 'index.php?rand=' + new Date().getTime(),
                            async: false,
                            cache: false,
                            dataType: "json",
                            data: 'id_order_detail='+id_order_detail+'&separate=true',
                            success: function (jsonData) {
                                console.log(jsonData);
                                if(jsonData == 0){
                                    location.reload();     
                                }else{
                                    alert('Coś poszło nie tak...');
                                }
                            }
                        });
                    }
                });

                
                $('.product-form').submit(function (e) {
                    var form = $(this);
                    var error = 0;
                    e.preventDefault();
                    /*$('select').each(function(){
                     var v = parseInt($(this).val());
                     if(v == 0){
                     alert('Nie można zapisać zmian, ponieważ są nie wybrane opcje.');
                     error =1;
                     return false;
                     }
                     });*/
                    if (!error) {
                        var data_form = $(this).serialize();
                        $.ajax({
                            type: 'POST',
                            headers: {"cache-control": "no-cache"},
                            url: 'index.php?rand=' + new Date().getTime(),
                            async: false,
                            cache: false,
                            dataType: "json",
                            data: data_form + '&submit=true',
                            success: function (jsonData) {
                                if (jsonData.status) {
                                    $(form).find('.submit').removeClass('submitted-error');
                                    $(form).find('.submit').addClass('submitted-ok');
                                    $(form).find('.submit').text('Zapisano');
                                } else {
                                    $(form).find('.submit').removeClass('submitted-ok');
                                    $(form).find('.submit').addClass('submitted-error');
                                    $(form).find('.submit').text('Problem !');
                                }
                            }
                        });
                        return false;
                    }
                });
            });

            function colors() {
                $('.select_group_6 option:selected').each(function () {
                    var color = $(this).data('color_group');
                    var product_id = $(this).parent().data('product_id');
                    var id_order_detail = $(this).parent().data('id_order_detail');

                    $('#pop-up_' + product_id + '_' + id_order_detail + ' #img-group_8').each(function () {
                        $(this).find('.img-group-item').hide();
                        $(this).find('.' + color).css('display', 'inline-block');
                    });
                    $('#pop-up_' + product_id + '_' + id_order_detail + ' #img-group_9').each(function () {
                        $(this).find('.img-group-item').hide();
                        $(this).find('.' + color).css('display', 'inline-block');
                    });
                    $('#pop-up_' + product_id + '_' + id_order_detail + ' #img-group_10').each(function () {
                        $(this).find('.img-group-item').hide();
                        $(this).find('.' + color).css('display', 'inline-block');
                    });
                    $('#pop-up_' + product_id + '_' + id_order_detail + ' #img-group_13').each(function () {
                        $(this).find('.img-group-item').hide();
                        $(this).find('.' + color).css('display', 'inline-block');
                    });
                });




            }
        </script>
    </body></html>
