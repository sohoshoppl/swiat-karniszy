<?php
session_start();
?>
<html><head>
        <title>Świat karniszy</title>
        <link rel="stylesheet" href="/integracja_allegro/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">        
        <script type="text/javascript" src="/../js/jquery/jquery-1.11.0.min.js"></script>
    </head>
    <body>
        <?php
        include(dirname(__FILE__).'/../config/config.inc.php');
        include(dirname(__FILE__).'/../init.php');        
        //define('DEBUG', false);
        //define('PS_SHOP_PATH', 'http://swiatkarniszy.dkonto.pl');
        //define('PS_WS_AUTH_KEY', '3GVTXWHTTEJ3PDRGUWELP8WHKZIRWP98');
        //require_once('PSWebServiceLibrary.php');
        //$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
        $back = 'http://swiatkarniszy.dkonto.pl';
        if (($_SESSION["access"] == 1) && isset($_GET['id_product']) && ($_GET['step'] == '2')) {
            $order = new Order( $_SESSION["id_order"] );
            $back = '/integracja_allegro?id='.$order->id.'&ref='.$order->reference;
            $id_product = (int)$_GET['id_product'];
            
            //pobranie kodu produktu do składania kodu kolekcji
            foreach(Product::getFeaturesStatic($id_product) as $product_feature){
                if( $product_feature['id_feature'] == 22){
                    $feature_value = new FeatureValue((int)$product_feature['id_feature_value']);
                }
            }
            $kod_karnisza = $feature_value->value[1];
 
            //pobranie i pogrupowanie atrybutów produktu
            
            $product_attributes = Db::getInstance()->executeS('
			SELECT DISTINCT a.`id_attribute`,a.code as code, a.`id_attribute_group`, al.`name` as `attribute`, agl.`name` as `group`
			FROM `'._DB_PREFIX_.'attribute` a
			LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
			LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl
				ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = '.(int)Context::getContext()->language->id.')
			LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac
				ON (a.`id_attribute` = pac.`id_attribute`)
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
			'.Shop::addSqlAssociation('product_attribute', 'pa').'
			'.Shop::addSqlAssociation('attribute', 'pac').'
			WHERE pa.`id_product` = '.(int)$id_product);            
            
            $product_attributes_by_group = array();
            foreach ($product_attributes as $product_attribute){
                $product_attributes_by_group[$product_attribute['id_attribute_group']]['name'] = $product_attribute['group'];
                $attr_color = '';
                $bracket_attribute_name = explode("(",$product_attribute['attribute']);
                $attr_color = Tools::link_rewrite($bracket_attribute_name[1] );
                $attributes = array('id_attribute'=> $product_attribute['id_attribute'], 'attribute' => $product_attribute['attribute'], 'color' => $attr_color, 'code' => $product_attribute['code'] );
                $product_attributes_by_group[$product_attribute['id_attribute_group']]['attributes'][] = $attributes;
            }

            
            ?>
                
                <section>
                    <h2>Formularz do budowania kolekcji karnisza</h2>
                    <form>
                    
            <?php
            foreach($product_attributes_by_group as $key => $attribute_group){
                if( ($key != 5) && ($key != 12) ){ //grupa 5 to długości, aukcje z allegro powinny mieć już wybraną długość
                    echo '<div class="row">';
                    echo '<h3 class="col-xs-4">'.$attribute_group["name"].'</h3>';
                    echo '<div class="selects col-xs-8">';
                    echo '<select id="select_group_'.$key.'">';
                    foreach($attribute_group['attributes'] as $attribute){
                        echo '<option data-color_group="'.$attribute['color'].'" data-code="'.$attribute['code'].'">'.$attribute['attribute'].'</option>';    
                    }
                    echo "</select>";
                    echo '</div>';
                    echo '</div>';
                }
            }
            ?>
                        <button type="button" id="back">Anuluj</button>
                        <input type="submit" value="Zapisz" />
                    </form>
                </section>
            <?php
            echo "<section>";
            echo "<h2>Poglądowe zdjęcia elementów</h2>";
            foreach($product_attributes_by_group as $key => $attribute_group){
                if( ($key != 5) && ($key != 12) ){ //grupa 5 to długości, aukcje z allegro powinny mieć już wybraną długość
                    echo '<h4 class="clear">'.$attribute_group["name"].'</h4>';
                    echo '<div id="img_group_'.$key.'" class="imgs">';
                    foreach($attribute_group['attributes'] as $attribute){
                        echo '<div data-color_group="'.$attribute['color'].'">';
                        echo '<span>'.$attribute['attribute'].'</span><br>';
                        echo '<img src="/img/co/'.$attribute['id_attribute'].'.jpg" alt="'.$attribute['attribute'].'" />';
                        echo "</div>";
                    }
                    echo '</div>';                    
                }
            }
            echo "</section>";            
            
        }
        /*
         * pierwsze wejście użytkownika, powinno być w linku id_zamówienia(id) i referencja(ref)
         * sprawdzamy czy to się zgadza sprawdzamy status zamówienia jak ok to pobieramy produkty z zamówienia
         * i robimy linki do edycji poszczególnych produktów
         */ elseif (isset($_GET['id']) && isset($_GET['ref'])) {
                $order = new Order((int)$_GET['id']);
            if ( $order->reference == $_GET['ref'] /* AND status zamówienia to status allegro do edycji */) {
                $products = $order -> getProducts();
                ?> 
                <header><h1>Zamówienie allegro</h1></header>
                <section class="product-listing">
                    <h2>Prosimy o uzupełnienie produktów o dodatkowe dane.</h2>
                    <ol>
                        <?php
                        foreach ($products as $product) {//2. formularz do uzupełniania danych produktów
                            echo "<li>{$product['product_name']}";
                            if ( $product['product_attribute_id'] != 0) {
                                echo " <a href=/integracja_allegro?id_product=" . $product['product_id'] . "&step=2>uzupełnij</a> ";
                            }
                            echo "</li>";
                        }
                        /* widok zamówienia i wybór produktów do edycji */
                        ?>
                    </ol>
                </section>
                <?php
                $_SESSION["access"] = 1;
                $_SESSION["id_order"] = $order->id;
                $_SESSION["products"] = $products;
            }
        } else {
            echo "<header><h2>Coś poszło nie tak...</h2></header>";
            unset($_SESSION['access']); 
            unset($_SESSION['order']); 
            unset($_SESSION['products']); 
            session_destroy();
        }
        ?>
        <script>
$(document).ready(function(){
    //colors();
    //$(document).on('change','#select_group_6', colors);
    $('#back').click(function(e){
        e.preventDefault();
        window.location.href = '<?php echo $back ?>';
    });

});

function colors(){
    var color = $('#select_group_6 option:selected').data('color_group');
    $('.imgs:not(#img_group_6) div').each(function(){
        var elem_color = $(this).data('color_group');
        if(elem_color != color){
            $(this).hide();   
        }else{
            $(this).show();     
        }
    });
}                
    </script>
    </body></html>
